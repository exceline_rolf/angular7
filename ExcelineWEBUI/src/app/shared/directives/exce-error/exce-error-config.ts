import {Injectable} from '@angular/core';

@Injectable()
export class ExceErrorConfig {
  placement: 'top' | 'bottom' | 'left' | 'right' = 'top';
  triggers = 'click';
  container: string;
}