import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class UsScrollService {
    reloadScroll: EventEmitter<any> = new EventEmitter();

    constructor() { }

    reloadScrollBar() {
        this.reloadScroll.emit();
    }


}
