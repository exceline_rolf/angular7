import { ExceToolbarService } from '../../../../modules/common/exce-toolbar/exce-toolbar.service';
import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  OnChanges
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'error-window',
  host: { '[class]': '"popover exce-validate exce-validate-" + placement', 'role': 'tooltip', '[id]': 'id' },
  templateUrl: './error-window.component.html',
  styleUrls: ['./error-window.component.scss']
})
export class ErrorWindowComponent implements OnDestroy {


  @Input() placement: 'top' | 'bottom' | 'left' | 'right' = 'top';

  private title: string;
  get $title(): string {
    return this.title;
  }
  @Input('title') set $title(value: string) {
    this.title = value;
    if (this.title && this.title.trim().length > 0) {
      this.translateGerSubscription = this.translate.get(this.title.trim()).subscribe((val: string) => {
        this.translatedMsg = val;
      });
    }
  }

  @Input() id: string;

  public translatedMsg: string;
  private translateGerSubscription: any;

  constructor(
    private exceToolbarService: ExceToolbarService,
    private translate: TranslateService
  ) {

    // this.exceToolbarService.langUpdated.subscribe(
    //   (lang) => {

    //     this.translate.use(this.exceToolbarService.getSelectedLanguage().id); // set the langusge for the ng2 translater
    //     // to get the messages translated
    //   }
    // );
  }

  ngOnDestroy(): void {
    if (this.translateGerSubscription) {
      this.translateGerSubscription.unsubscribe();
    }
  }


}
