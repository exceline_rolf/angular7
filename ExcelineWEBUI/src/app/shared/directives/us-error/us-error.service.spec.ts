import { TestBed, inject } from '@angular/core/testing';

import { UsErrorService } from './us-error.service';

describe('UsErrorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UsErrorService]
    });
  });

  it('should be created', inject([UsErrorService], (service: UsErrorService) => {
    expect(service).toBeTruthy();
  }));
});
