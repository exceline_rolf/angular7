import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsErrorComponent } from './us-error.component';

describe('UsErrorComponent', () => {
  let component: UsErrorComponent;
  let fixture: ComponentFixture<UsErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
