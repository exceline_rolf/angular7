import { isString } from '../directives/exce-error/util/util';
import * as moment from 'moment';
export class Extension {
    public static IsNorwayTelephoneNoValid(mobilePhoneNo: string, mobilePhoneNoPrifix: string): number {
        const mobileReg = '^[0-9]\d*$';
        let validResult = this.PrefixNumberValid(mobilePhoneNoPrifix);
        if (validResult === 0 && mobilePhoneNo !== 'undefined' && mobilePhoneNo) {
            if (!mobilePhoneNo.trim().match(mobileReg)) {
                validResult = 2;
            } else {
                if (mobilePhoneNoPrifix === '+47') {
                    if (!(mobilePhoneNo.length === 8)) {
                        validResult = 2;
                    }
                } else if (!(mobilePhoneNo.trim().length <= 12)) {
                    validResult = 2;
                }
            }
        }
        return validResult;
    }

    public static PrefixNumberValid(mobilePhoneNoPrifix: string): number {
        let validResult = 0;
        if (mobilePhoneNoPrifix !== 'undefined' && mobilePhoneNoPrifix && mobilePhoneNoPrifix.length >= 2) {
            if (this.IsNumberDigitsValid(mobilePhoneNoPrifix.substring(1, mobilePhoneNoPrifix.length))) {

            } else {
                validResult = 1;
            }
        } else {
            validResult = 1;
        }
        return validResult;
    }

    private static IsNumberDigitsValid(imputNumber: string): boolean {
        const codeNumber = '@"^[0-9]\d*$';
        let isNumberValid = true;
        if (imputNumber.match(codeNumber)) {
            isNumberValid = false;
        }
        return isNumberValid;
    }

    public static GetFormatteredDate(date: any): any {
        if (date) {
            const selectedDate = new Date(date);
            let formatteredDate: any;
            formatteredDate = { year: selectedDate.getFullYear(), month: selectedDate.getMonth() + 1, day: selectedDate.getDate() }
            return formatteredDate;
        } else {
            return;
        }

    }

    public static createGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            // tslint:disable-next-line:no-bitwise
            const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
    /* Returns a left padded string
    num is the raw (unpadded) data from the user, and size is how many zeros to pad */
    public static pad(num: number, size: number): string {
      let s: string = num + '';
      while (s.length < size) {
        s = '0' + s;
      }
      return s;
    }

    public static getNumberOfDaysInMonth(year: number, month: number): number {
      // adding 1 because date is 0
      return moment(year + '-' + (month + 1), 'YYYY-MM').daysInMonth();
    }
/**
 * @description Transform the dates timestamp into noon (12:00:00)
 * @returns A T-seperated string on the format {YYYY-MM-DDT12:00:00}
 * @param date Input string
  */
    public static setNoonTransform(date: Date) {
      const q = this.GetFormatteredDate(date);
      q.month = (q.month < 10 ) ? this.pad(q.month, 2) : q.month;
      q.day = (q.day < 10 ) ? this.pad(q.day, 2) : q.day;
      return (q.year + '-' + q.month + '-' + q.day + 'T' + '12:00:00');
    }
}

