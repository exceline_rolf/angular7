import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ExceMemberService } from '../../../modules/membership/services/exce-member.service'

@Component({
  selector: 'member-notification-detail',
  templateUrl: './member-notification-detail.component.html',
  styleUrls: ['./member-notification-detail.component.scss']
})
export class MemberNotificationDetailComponent implements OnInit {

  @Input() rowItem: any;
  @Input() modalReference: any;
  @Input() isNotificationDetail: boolean;
  @Output() onDoneOrIgnore: EventEmitter<number> = new EventEmitter<number>();

  private isButtonsVisible = true;
  private messageIcon: string;
  private severityIcon: string;

  constructor(private exceMemberService: ExceMemberService) { }

  ngOnInit() {
  }

  updateNotificationStatus(button) {
    switch (button) {
      case 'DONE':
        this.exceMemberService.updateNotificationStatus({ selectedIds: [this.rowItem.Id], status: 4 }).subscribe
          (result => {
            if (result.Data) {
              this.onDoneOrIgnore.emit();
              this.closeForm();
            } else {
            }
          })
        break
      case 'IGNORE':
        this.exceMemberService.updateNotificationStatus({ selectedIds: [this.rowItem.Id], status: 3 }).subscribe
          (result => {
            if (result.Data) {
              this.onDoneOrIgnore.emit();
              this.closeForm();
            } else {
            }
          })
        break
      default:
        break
    }
  }

  closeForm() {
    this.modalReference.close();
  }

}
