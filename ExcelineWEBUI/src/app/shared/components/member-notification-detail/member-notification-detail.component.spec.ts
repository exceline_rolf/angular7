import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberNotificationDetailComponent } from './member-notification-detail.component';

describe('MemberNotificationDetailComponent', () => {
  let component: MemberNotificationDetailComponent;
  let fixture: ComponentFixture<MemberNotificationDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberNotificationDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberNotificationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
