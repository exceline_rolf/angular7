import { Directive, Output, Input, EventEmitter } from '@angular/core';

@Directive({
  selector: '[text]',
  host: {
    '(input)': 'changeInput($event.target.value)'
  }
})
export class TextDirective {

  @Output('newValue') newValue = new EventEmitter<any>();
  @Input('text') text: any;
  @Input('rg') rg: any;

  changeInput(value: string) {
    if (this.rg === undefined) {
      this.newValue.emit(value);
    } else {
      const numeric = parseFloat(value)
      if (!isNaN(numeric) && numeric >= 0 && numeric <= this.rg) {
        this.newValue.emit(
          {
            v: numeric,
            rg: this.rg
          });
      }
    }
  }
}

