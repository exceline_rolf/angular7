import { Component, OnChanges, Directive, Input, Output, ViewContainerRef, ElementRef, EventEmitter, OnInit, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { ExceColorPickerService } from './exce-color-picker.service';
import { Rgba, Hsla, Hsva, SliderPosition, SliderDimension } from './classes';
import { NgModule, Compiler, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DialogComponent } from './dialog/dialog.component';
import { TextDirective } from './text.directive';
import { SliderDirective } from './slider.directive';
// import { SharedModule } from '../../shared.module';

// import template from './templates/default/color-picker.template';
// import styles from './templates/default/color-picker.style';

@Directive({
    selector: '[colorPicker]',
    host: {
        '(input)': 'changeInput($event.target.value)',
        '(click)': 'onClick()'
    }
})

export class ExceColorPickerComponent implements OnInit, OnChanges {
    @Input('colorPicker') colorPicker: string;
    @Output('colorPickerChange') colorPickerChange = new EventEmitter<string>(true);
    @Input('cpToggle') cpToggle: boolean;
    @Output('cpToggleChange') cpToggleChange = new EventEmitter<boolean>(true);
    @Input('cpPosition') cpPosition = 'bottom';
    @Input('cpPositionOffset') cpPositionOffset = '0%';
    @Input('cpPositionRelativeToArrow') cpPositionRelativeToArrow = false;
    @Input('cpOutputFormat') cpOutputFormat = 'hex';
    @Input('cpPresetLabel') cpPresetLabel = 'Preset colors';
    @Input('cpPresetColors') cpPresetColors: Array<string>;
    @Input('cpCancelButton') cpCancelButton = false;
    @Input('cpCancelButtonClass') cpCancelButtonClass = 'cp-cancel-button-class';
    @Input('cpCancelButtonText') cpCancelButtonText = 'Cancel';
    @Input('cpDisableHsla') cpDisableHsla = false;
    @Input('cpOKButton') cpOKButton = false;
    @Input('cpOKButtonClass') cpOKButtonClass = 'cp-ok-button-class';
    @Input('cpOKButtonText') cpOKButtonText = 'OK';
    @Output('cpOKButtonClick') cpOKButtonClick = new EventEmitter<string>(true);
    @Input('cpFallbackColor') cpFallbackColor = '#fff';
    @Input('cpHeight') cpHeight = 'auto';
    @Input('cpWidth') cpWidth = '235px';
    @Input('cpIgnoredElements') cpIgnoredElements: any = [];
    @Input('cpDialogDisplay') cpDialogDisplay = 'popup';
    @Input('cpSaveClickOutside') cpSaveClickOutside = true;
    @Input('cpAlphaChannel') cpAlphaChannel = 'hex6';
    @Input('cpleftAlign') cpleftAlign: string;
    @Input('cpAlign') cpTopAlign: string;



    private dialog: any;
    private created: boolean;
    private ignoreChanges = false;


    constructor(
        private compiler: Compiler,
        private vcRef: ViewContainerRef,
        private el: ElementRef,
        private resolver: ComponentFactoryResolver,
        private service: ExceColorPickerService
    ) {
        this.created = false;
    }

    ngOnChanges(changes: any): void {
        if (changes.cpToggle) {
            if (changes.cpToggle.currentValue) { this.openDialog(); }
            if (!changes.cpToggle.currentValue && this.dialog) { this.dialog.closeColorPicker(); }
        }
        if (changes.colorPicker) {
            if (this.dialog && !this.ignoreChanges) {
                if (this.cpDialogDisplay === 'inline') {
                    this.dialog.setInitialColor(changes.colorPicker.currentValue);
                }
                this.dialog.setColorFromString(changes.colorPicker.currentValue, false);

            }
            this.ignoreChanges = false;
        }
    }

    ngOnInit() {
        let hsva = this.service.stringToHsva(this.colorPicker);
        if (hsva === null) { hsva = this.service.stringToHsva(this.colorPicker, true); }
        if (hsva == null) {
            hsva = this.service.stringToHsva(this.cpFallbackColor);
        }
        this.colorPickerChange.emit(this.service.outputFormat(hsva, this.cpOutputFormat, this.cpAlphaChannel === 'hex8'));

    }

    onClick() {
        if (this.cpIgnoredElements.filter((item: any) => item === this.el.nativeElement).length === 0) {
            this.openDialog();
        }
    }

    openDialog() {
        if (!this.created) {
            this.created = true;
            // this.compiler.compileModuleAndAllComponentsAsync(SharedModule)
            //     .then(factory => {
            //         const compFactory = factory.componentFactories.find(x => x.componentType === DialogComponent);
            //         const injector = ReflectiveInjector.fromResolvedProviders([], this.vcRef.parentInjector);
            //         const cmpRef = this.vcRef.createComponent(compFactory, 0, injector, []);
            //         cmpRef.instance.setDialog(this, this.el, this.colorPicker, this.cpPosition, this.cpPositionOffset,
            //             this.cpPositionRelativeToArrow, this.cpOutputFormat, this.cpPresetLabel, this.cpPresetColors,
            //             this.cpCancelButton, this.cpCancelButtonClass, this.cpCancelButtonText,
            //             this.cpOKButton, this.cpOKButtonClass, this.cpOKButtonText, this.cpHeight, this.cpWidth,
            //             this.cpIgnoredElements, this.cpDialogDisplay, this.cpSaveClickOutside, this.cpAlphaChannel, this.cpDisableHsla);
            //         this.dialog = cmpRef.instance;
            //     });

            const compFactory = this.resolver.resolveComponentFactory(DialogComponent);
            const injector = Injector.create([], this.vcRef.parentInjector);
            const cmpRef = this.vcRef.createComponent(compFactory, 0, injector, []);
            cmpRef.instance.setDialog(this, this.el, this.colorPicker, this.cpPosition, this.cpPositionOffset,
                this.cpPositionRelativeToArrow, this.cpOutputFormat, this.cpPresetLabel, this.cpPresetColors,
                this.cpCancelButton, this.cpCancelButtonClass, this.cpCancelButtonText,
                this.cpOKButton, this.cpOKButtonClass, this.cpOKButtonText, this.cpHeight, this.cpWidth,
                this.cpIgnoredElements, this.cpDialogDisplay, this.cpSaveClickOutside, this.cpAlphaChannel, this.cpDisableHsla);
            this.dialog = cmpRef.instance;
        } else if (this.dialog) {
            this.dialog.openDialog(this.colorPicker);
        }
    }

    colorChanged(value: string, ignore: boolean = true) {
        this.ignoreChanges = ignore;
        this.colorPickerChange.emit(value)
    }

    okButtonClick(value: string) {
        this.cpOKButtonClick.emit(value);
    }

    changeInput(value: string) {
        this.dialog.setColorFromString(value, true);
    }

    toggle(value: boolean) {
        this.cpToggleChange.emit(value);
    }
}

