import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsBreadcrumbComponent } from './us-breadcrumb.component';

describe('UsBreadcrumbComponent', () => {
  let component: UsBreadcrumbComponent;
  let fixture: ComponentFixture<UsBreadcrumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsBreadcrumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsBreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
