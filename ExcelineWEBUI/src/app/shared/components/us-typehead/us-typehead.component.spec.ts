import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsTypeheadComponent } from './us-typehead.component';

describe('UsTypeheadComponent', () => {
  let component: UsTypeheadComponent;
  let fixture: ComponentFixture<UsTypeheadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsTypeheadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsTypeheadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
