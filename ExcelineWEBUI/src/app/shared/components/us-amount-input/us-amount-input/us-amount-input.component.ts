import { Component, OnInit, ViewChild, ElementRef, Input, forwardRef, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ExceToolbarService } from '../../../../modules/common/exce-toolbar/exce-toolbar.service';

export const AMOUNT_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => UsAmountInputComponent),
  multi: true
};

@Component({
  selector: 'us-amount-input',
  templateUrl: './us-amount-input.component.html',
  styleUrls: ['./us-amount-input.component.scss'],
  providers: [AMOUNT_VALUE_ACCESSOR]
})

export class UsAmountInputComponent implements OnInit, ControlValueAccessor {

  @ViewChild('input')
  input: ElementRef;
  locale: string;
  originalValue: any;
  @Input() disabled = false;
  @Input() hideClose = true;

  @Output() valueChange = new EventEmitter<string>();
  @Output() valueBlur = new EventEmitter<string>();
  @Output() clear = new EventEmitter<string>();
  @Output() enterKey = new EventEmitter<string>();

  changeCallback = (data: any) => { };
  touchCallback = () => { };

  constructor(
    private toolbarService: ExceToolbarService
  ) {
    if (this.toolbarService.getSelectedLanguage().id === 'no') {
      this.locale = 'en-US';
    } else {
      this.locale = 'en-US';
    }
  }

  ngOnInit() {
    // **Language selecton and subscription - dateformat and amountconverter uses this **
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
          this.input.nativeElement.value = this.format(this.originalValue);
        }
      }
    );
    // **END**

    // this.toolbarService.langUpdated.subscribe(
    //   lang => {
    //     if (lang.id === 'no') {
    //       this.locale = 'en-US';
    //     } else {
    //       this.locale = 'en-US';
    //     }
    //     this.input.nativeElement.value = this.format(this.originalValue);
    //   }
    // );
  }

  onChange(event) {
    this.originalValue = event.target.value;
    this.changeCallback(event.target.value);
    this.valueChange.emit(event.target.value);
  }

  writeValue(amount: any) {
    this.originalValue = amount;
    this.input.nativeElement.value = this.format(amount);
  }

  registerOnChange(fn: any) {
    this.changeCallback = fn;
  }

  registerOnTouched(fn: any) {
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

  format(value: number | string) {
    let amount = value ? value.toString() : '0';
    if (this.locale === 'nb-NO') {
      amount = amount.replace(/\s/g, '');
      amount = amount.replace(/,/g, '');
    } else {
      amount = amount.replace(/\s/g, '');
      amount = amount.replace(/,/g, '');
      amount = amount.replace(/,/g, '.');
    }

    if (Number.isNaN(Number(amount))) {
      amount = '0';
    }
    return new Intl.NumberFormat(this.locale, { minimumFractionDigits: 2 }).format(Number(amount));
  }

  insertAt(src: string, index: number, str: string) {
    return (src.substr(0, index) + str + src.substr(index));
  }

  clearAmount() {
    this.writeValue(0);
    this.changeCallback(0);
    this.clear.emit();
  }

  onBlur(value) {
    this.writeValue(value);
    this.valueBlur.emit(value)
  }

  enterKeyEvent(value) {
    this.enterKey.emit(value)

  }
}
