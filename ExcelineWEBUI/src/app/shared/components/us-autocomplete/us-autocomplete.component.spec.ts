import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsAutocompleteComponent } from './us-autocomplete.component';

describe('UsAutocompleteComponent', () => {
  let component: UsAutocompleteComponent;
  let fixture: ComponentFixture<UsAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsAutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
