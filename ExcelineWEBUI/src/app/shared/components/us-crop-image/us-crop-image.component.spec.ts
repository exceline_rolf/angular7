import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsCropImageComponent } from './us-crop-image.component';

describe('UsCropImageComponent', () => {
  let component: UsCropImageComponent;
  let fixture: ComponentFixture<UsCropImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsCropImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsCropImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
