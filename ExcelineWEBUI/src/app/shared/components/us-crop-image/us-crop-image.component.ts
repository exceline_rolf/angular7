import { Component, Input, Renderer2, ViewChild, ElementRef, Output, EventEmitter, Type, AfterViewInit, OnChanges, OnDestroy, SimpleChanges, OnInit } from '@angular/core';
import { ImageCropper } from './imageCropper';
import { CropperSettings } from './cropperSettings';
import { Exif } from './exif';
import { Bounds } from './model/bounds';
import { CropPosition } from './model/cropPosition';
import { constructDependencies } from '@angular/core/src/di/reflective_provider';
import { PreloaderService } from '../../services/preloader.service';

@Component({
    selector: 'us-crop-image',
    templateUrl: './us-crop-image.component.html',
    styleUrls: ['./us-crop-image.component.scss']
})
export class UsCropImageComponent implements AfterViewInit, OnChanges, OnDestroy {

    @ViewChild('cropcanvas', undefined) cropcanvas: ElementRef;

    @Input('settings') public settings: CropperSettings;
    @Input('image') public image: any;
    @Input('inputImage') public inputImage: any;
    @Input() public cropper: ImageCropper;
    @Input() public cropPosition: CropPosition;
    @Input('getOnlyCameraInput') public getOnlyCameraInput: boolean;
    @Input('getOnlyFileInput') public getOnlyFileInput: boolean;
    @Output() public cropPositionChange: EventEmitter<CropPosition> = new EventEmitter<CropPosition>();
    @Output() public onCrop: EventEmitter<any> = new EventEmitter();

    public croppedWidth: number;
    public croppedHeight: number;
    public intervalRef: number;
    public raf: number;
    public renderer: Renderer2;
    public windowListener: EventListenerObject;

    hideCanvas: any;

    // for the camera capturing image
    public closePlayer = false;
    public streaming = false;
    public player = null;
    public canvas = null;
    public context = null;

    private isCropPositionUpdateNeeded: boolean;

    constructor(
        renderer: Renderer2,
        // private postService:GeneralService
    ) {
        this.renderer = renderer;
    }

    /**
     * functions for the web cam capture are declatered below
     * /////////////////////////////////////////////////////////////////////////////////////////////
     */


    // for the web cam
    startWebcam() {
        PreloaderService.showPreLoader();
        this.streaming = false;
        this.player = <HTMLVideoElement>document.getElementById('video');

        const constraints = {
            video: true,
            audio: false
        };

        navigator.mediaDevices.getUserMedia(constraints)
            .then((stream) => {
                this.player.srcObject = stream;
                this.player.play();
                PreloaderService.hidePreLoader();
                this.streaming = true;
            })
            .catch((error) => {
            });
    }

    stopWebcam() {
        this.player = <HTMLVideoElement>document.getElementById('video');
        this.player.srcObject.getVideoTracks().forEach(track => track.stop());

    }

    image1;
    snapshot() {
        // this.player = <HTMLVideoElement>document.getElementById("video");
        this.canvas = <HTMLCanvasElement>document.getElementById("cropcanvas");
        this.context = this.canvas.getContext('2d');

        // need to run the video before capturing
        // const constraints = {
        //     video: true,
        // };
        // navigator.mediaDevices.getUserMedia(constraints)
        //     .then((stream) => {
        //         this.player.srcObject = stream;
        //         this.player.play();
        //     });

        // Draw the video frame to the canvas.
        this.context.drawImage(this.player, 0, 0, this.canvas.width, this.canvas.height);

        // convert the canvas image to data format utf8 to display in the canvas
        this.image1 = this.canvas.toDataURL('image/png');

        // to give the image to cropper
        this.filefromWebCam(this.image1);
        this.closePlayer = true;
        this.stopWebcam();
    }

    testing() {
        this.filefromWebCam(this.image1);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////

    public ngAfterViewInit(): void {
        let canvas: HTMLCanvasElement = this.cropcanvas.nativeElement;
        if (!this.settings) {
            this.settings = new CropperSettings();
        }

        if (this.settings.cropperClass) {
            this.renderer.setAttribute(canvas, 'class', this.settings.cropperClass);
        }

        if (!this.settings.dynamicSizing) {
            this.renderer.setAttribute(canvas, 'width', this.settings.canvasWidth.toString());
            this.renderer.setAttribute(canvas, 'height', this.settings.canvasHeight.toString());
        } else {
            this.windowListener = this.resize.bind(this);
            window.addEventListener('resize', this.windowListener);
        }

        if (!this.cropper) {
            this.cropper = new ImageCropper(this.settings);
        }

        this.cropper.prepare(canvas);
        if (this.getOnlyCameraInput) {
            this.startWebcam();
        }
    }

    // only called in page loading
    public ngOnChanges(changes: SimpleChanges): void {
        if (this.isCropPositionChanged(changes)) {
            this.cropper.updateCropPosition(this.cropPosition.toBounds());
            if (this.cropper.isImageSet()) {
                let bounds = this.cropper.getCropBounds();
                this.image.image = this.cropper.getCroppedImageHelper().src;
                this.onCrop.emit(bounds);
            }
            this.updateCropBounds();
        }

        if (changes.inputImage) {
            this.setImage(changes.inputImage.currentValue);
        }
    }

    public ngOnDestroy() {
        if (this.settings.dynamicSizing && this.windowListener) {
            window.removeEventListener('resize', this.windowListener);
        }
    }

    public onTouchMove(event: TouchEvent): void {
        this.cropper.onTouchMove(event);
    }

    public onTouchStart(event: TouchEvent): void {
        this.cropper.onTouchStart(event);
    }

    public onTouchEnd(event: TouchEvent): void {
        this.cropper.onTouchEnd(event);
        if (this.cropper.isImageSet()) {
            this.image.image = this.cropper.getCroppedImageHelper().src;
            this.onCrop.emit(this.cropper.getCropBounds());
            this.updateCropBounds();
        }
    }

    public onMouseDown(event: MouseEvent): void {
        this.cropper.onMouseDown(event);
    }

    public onMouseUp(event: MouseEvent): void {
        if (this.cropper.isImageSet()) {
            this.cropper.onMouseUp(event);
            this.image.image = this.cropper.getCroppedImageHelper().src;
            this.onCrop.emit(this.cropper.getCropBounds());
            this.updateCropBounds();
        }
    }

    public onMouseMove(event: MouseEvent): void {
        this.cropper.onMouseMove(event);
    }

    // the function fires at any file change
    public fileChangeListener($event: any) {
        if ($event.target.files.length === 0) return;

        let file: File = $event.target.files[0];
        if (this.settings.allowedFilesRegex.test(file.name)) {
            let image: any = new Image();
            let fileReader: FileReader = new FileReader();

            fileReader.addEventListener('loadend', (loadEvent: any) => {
                image.addEventListener('load', () => {
                    this.setImage(image);
                });
                image.src = loadEvent.target.result;
            });

            fileReader.readAsDataURL(file);
        }
    }

    // the function to capture external captured images - from web cam
    public filefromWebCam(imageSrc: string) {
        let image: any = new Image();
        let fileReader: FileReader = new FileReader();
        this.setImage(image);
        image.src = imageSrc;
    }

    private resize() {
        // debugger;
        let canvas: HTMLCanvasElement = this.cropcanvas.nativeElement;
        this.settings.canvasWidth = canvas.offsetWidth;
        this.settings.canvasHeight = canvas.offsetHeight;
        this.cropper.resizeCanvas(canvas.offsetWidth, canvas.offsetHeight, true);
    }

    public reset(): void {
        this.cropper.reset();
        this.renderer.setAttribute(this.cropcanvas.nativeElement, 'class', this.settings.cropperClass);
        this.image.image = this.cropper.getCroppedImageHelper().src;
    }

    public setImage(image: HTMLImageElement, newBounds: any = null) {
        this.renderer.setAttribute(this.cropcanvas.nativeElement, 'class', `${this.settings.cropperClass} ${this.settings.croppingClass}`);
        this.raf = window.requestAnimationFrame(() => {
            if (this.raf) {
                window.cancelAnimationFrame(this.raf);
            }
            if (image.naturalHeight > 0 && image.naturalWidth > 0) {

                image.height = image.naturalHeight;
                image.width = image.naturalWidth;

                window.cancelAnimationFrame(this.raf);
                this.getOrientedImage(image, (img: HTMLImageElement) => {
                    if (this.settings.dynamicSizing) {
                        let canvas: HTMLCanvasElement = this.cropcanvas.nativeElement;
                        this.settings.canvasWidth = canvas.offsetWidth;
                        this.settings.canvasHeight = canvas.offsetHeight;
                        this.cropper.resizeCanvas(canvas.offsetWidth, canvas.offsetHeight, false);
                    }

                    this.cropper.setImage(img);
                    if (this.cropPosition && this.cropPosition.isInitialized()) {
                        this.cropper.updateCropPosition(this.cropPosition.toBounds());
                    }

                    this.image.original = img;
                    let bounds = this.cropper.getCropBounds();
                    this.image.image = this.cropper.getCroppedImageHelper().src;

                    if (!this.image) {
                        this.image = image;
                    }

                    if (newBounds != null) {
                        bounds = newBounds;
                        this.cropper.setBounds(bounds);
                        this.cropper.updateCropPosition(bounds);
                    }
                    this.onCrop.emit(bounds);
                });
            }
        });
    }

    private isCropPositionChanged(changes: SimpleChanges): boolean {
        if (this.cropper && changes['cropPosition'] && this.isCropPositionUpdateNeeded) {
            return true;
        } else {
            this.isCropPositionUpdateNeeded = true;
            return false;
        }
    }

    private updateCropBounds(): void {
        let cropBound: Bounds = this.cropper.getCropBounds();
        this.cropPositionChange.emit(new CropPosition(cropBound.left, cropBound.top, cropBound.width, cropBound.height));
        this.isCropPositionUpdateNeeded = false;
    }

    private getOrientedImage(image: HTMLImageElement, callback: Function) {
        let img: any;

        Exif.getData(image, function () {
            let orientation = Exif.getTag(image, 'Orientation');

            if ([3, 6, 8].indexOf(orientation) > -1) {
                let canvas: HTMLCanvasElement = document.createElement('canvas'),
                    ctx: CanvasRenderingContext2D = <CanvasRenderingContext2D>canvas.getContext('2d'),
                    cw: number = image.width,
                    ch: number = image.height,
                    cx: number = 0,
                    cy: number = 0,
                    deg: number = 0;

                switch (orientation) {
                    case 3:
                        cx = -image.width;
                        cy = -image.height;
                        deg = 180;
                        break;
                    case 6:
                        cw = image.height;
                        ch = image.width;
                        cy = -image.height;
                        deg = 90;
                        break;
                    case 8:
                        cw = image.height;
                        ch = image.width;
                        cx = -image.width;
                        deg = 270;
                        break;
                    default:
                        break;
                }

                canvas.width = cw;
                canvas.height = ch;
                ctx.rotate(deg * Math.PI / 180);
                ctx.drawImage(image, cx, cy);
                img = document.createElement('img');
                img.width = cw;
                img.height = ch;
                img.addEventListener('load', function () {
                    callback(img);
                });
                img.src = canvas.toDataURL('image/png');
            } else {
                img = image;
                callback(img);
            }
        });
    }

}
