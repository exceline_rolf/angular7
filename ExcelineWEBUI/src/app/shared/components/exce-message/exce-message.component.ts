import { Component, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { ExceMessageService } from './exce-message.service';
import { UsbModal } from '../../../shared/components/us-modal/us-modal';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-exce-message',
  templateUrl: './exce-message.component.html',
  styleUrls: ['./exce-message.component.scss'],
})

export class ExceMessageComponent implements OnDestroy {
  private destroy$ = new Subject<void>();
  private msgType: string;
  private usMessage: any;
  private IsVisibleYes: boolean;
  private IsVisibleNo: boolean;
  private IsVisibleOk: boolean;
  private IsVisibleCancel: boolean;
  private msgBoxId: any;
  private translatedMsg: string;
  private translateGerSubscription: any;
  private optionalData: any;

  msgTitle: string;
  msgBody: string;
  param1: any;


  @ViewChild('MessageBox') messageBox: ElementRef;
  private errorMsgIcon = false;

  constructor(
    private messageService: ExceMessageService,
    private usbModel: UsbModal,
    private translate: TranslateService
  ) {
    this.messageService.componentMethodCalled$.pipe(
      takeUntil(this.destroy$)
    ).subscribe((data) => {
      this.msgTitle = data.msgTitle;
      this.msgBody = data.msgBody;
      this.msgType = data.msgType;
      this.msgBoxId = data.msgBoxId;
      this.optionalData = data.optionalData;
      this.IsVisibleYes = data.IsVisibleYes;
      this.IsVisibleOk = data.IsVisibleOk;

      if (this.msgTitle && this.msgTitle.trim().length > 0) {
        this.translate.get(this.msgTitle.trim()).pipe(
          takeUntil(this.destroy$)
          ).subscribe((val: string) => {
          this.msgTitle = val;
        });
      }

      if (this.msgBody && this.msgBody.trim().length > 0) {
        this.translate.get(this.msgBody.trim()).pipe(
          takeUntil(this.destroy$)
          ).subscribe((val: string) => {
          this.msgBody = val;
        });
      }

      if (this.msgType === 'ERROR') {
        this.errorMsgIcon = true;
      }

      if (this.msgType === 'SUCCSESS') {
        this.errorMsgIcon = false; }

      if (this.msgType === 'CONFIRM') {
        this.IsVisibleYes = false;
        this.IsVisibleNo = false;
        this.IsVisibleOk = true;
        this.IsVisibleCancel = true;
        this.errorMsgIcon = false;

        if (this.usMessage) {
          this.usMessage.close();
          this.usMessage = usbModel.open(this.messageBox, { width: '500' });
        } else {
          this.usMessage = usbModel.open(this.messageBox, { width: '500' });
        }
      } else if (this.msgType === 'WARNING' || 'ERROR' || 'SUCCESS') {
        this.IsVisibleYes = true;
        this.IsVisibleNo = true;
        this.IsVisibleOk = false;
        this.IsVisibleCancel = true;

        if (this.usMessage) {
          this.usMessage.close();
          this.usMessage = usbModel.open(this.messageBox, { width: '500' });
        } else {
          this.usMessage = usbModel.open(this.messageBox, { width: '500' });
        }
      } else {
      }
    });
  }

  btnYesHandler() {
    this.usMessage.close();
    this.messageService.yesClickHandler(this.msgBoxId, this.optionalData);
  }

  btnNoHandler() {
    this.usMessage.close();
    this.messageService.noClickHandler(this.msgBoxId);
  }

  btnOkHandler() {
    this.usMessage.close();
    this.messageService.okClickHandler(this.msgBoxId);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
