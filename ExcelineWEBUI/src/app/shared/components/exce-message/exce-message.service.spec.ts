import { TestBed, inject } from '@angular/core/testing';

import { ExceMessageService } from './exce-message.service';

describe('ExceMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceMessageService]
    });
  });

  it('should be created', inject([ExceMessageService], (service: ExceMessageService) => {
    expect(service).toBeTruthy();
  }));
});
