import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../shared/services/common-http.service';
import { ConfigService } from '@ngx-config/core';
import { Observable } from 'rxjs';

@Injectable()
export class ExceEntityselectionService {
  private memberApiURL: string;

  constructor(private commonHttpService: CommonHttpService,
    private config: ConfigService) {
      this.memberApiURL = this.config.getSettings('EXCE_API.MEMBER');

    }

    getBranches(branchId: number): Observable<any> {
      const url = this.memberApiURL + '/GetBranches?branchId=' + branchId
      return this.commonHttpService.makeHttpCall(url, {
        method: 'GET',
        auth: true
      })
    }

    getMemberBasicInfo(memberId: number): Observable<any> {
      const url = this.memberApiURL + '/GetMemberBasicInfo?memberId=' + memberId
      return this.commonHttpService.makeHttpCall(url, {
        method: 'GET',
        auth: true
      })
    }

}
