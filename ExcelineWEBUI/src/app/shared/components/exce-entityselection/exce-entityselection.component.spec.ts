import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceEntityselectionComponent } from './exce-entityselection.component';

describe('ExceEntityselectionComponent', () => {
  let component: ExceEntityselectionComponent;
  let fixture: ComponentFixture<ExceEntityselectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceEntityselectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceEntityselectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
