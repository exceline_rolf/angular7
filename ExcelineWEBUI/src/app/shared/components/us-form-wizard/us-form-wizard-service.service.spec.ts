import { TestBed, inject } from '@angular/core/testing';

import { UsFormWizardServiceService } from './us-form-wizard-service.service';

describe('UsFormWizardServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UsFormWizardServiceService]
    });
  });

  it('should be created', inject([UsFormWizardServiceService], (service: UsFormWizardServiceService) => {
    expect(service).toBeTruthy();
  }));
});
