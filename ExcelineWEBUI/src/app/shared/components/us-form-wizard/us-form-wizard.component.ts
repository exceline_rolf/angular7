import { Component, Output, Input, EventEmitter, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { WizardStepComponent } from './wizard-step/wizard-step.component';
import { asTextData } from '@angular/core/src/view';

@Component({
  selector: 'us-form-wizard',
  templateUrl: './us-form-wizard.component.html',
  styleUrls: ['./us-form-wizard.component.scss']
})
export class UsFormWizardComponent implements AfterContentInit {
  @ContentChildren(WizardStepComponent)
  wizardSteps: QueryList<WizardStepComponent>;

  private _steps: Array<WizardStepComponent> = [];
  private _isCompleted = false;
  public _prevStepVisible = true;
  public _nextStepVisible = true;

  @Input() nextStepDisabled = false;
  @Input() preStepDisabled = false;
  @Input() isContractVisible = true;
  @Input() isSaveVisibale = true;
  @Input() skipNumber;
  @Input() showNext = true;

  @Output() onStepChanged: EventEmitter<WizardStepComponent> = new EventEmitter<WizardStepComponent>();
  @Output() skiptheStep

  oneStepOnly: any;

  constructor() { }

  ngAfterContentInit() {
    this.wizardSteps.forEach(step => this._steps.push(step));
    this.steps[0].isActive = true;
    // this.isSaveVisibale =
  }

  set saveBtnVisiblestatus(step: WizardStepComponent) {

  }

  set setOneStepOnly(val) {
    this.oneStepOnly = val
    if (val === true) {
      this._steps.forEach(step => {
        if (Number(step.number) > 1) {
          step.hidden = true;
        }
      });
    } else if (val === false) {
      this._steps.forEach(step => {
        if (Number(step.number) > 1) {
          step.hidden = false;
        }
      });
    }
  }

  get steps(): Array<WizardStepComponent> {
    return this._steps.filter(step => !step.hidden);
  }

  get isCompleted(): boolean {
    return this._isCompleted;
  }

  get activeStep(): WizardStepComponent {
    return this.steps.find(step => step.isActive);
  }

  set activeStep(step: WizardStepComponent) {
    if (step.number === '2' || step.number === '3' || step.number === '4') {
      // this.isSaveVisibale = false;
    } else {
      // this.isSaveVisibale = true;
    }

    if (step !== this.activeStep && !step.isDisabled) {
      this.activeStep.isActive = false;
      step.isActive = true;
      this.onStepChanged.emit(step);
    }
  }

  public get activeStepIndex(): number {
    return this.steps.indexOf(this.activeStep);
  }

  get hasNextStep(): boolean {
    return this.activeStepIndex < this.steps.length - 1;
  }

  get hasPrevStep(): boolean {
    return this.activeStepIndex > 0;
  }

  set nextStepVisible(val) {
    this._nextStepVisible = val;
  }

  set backStepVisible(val) {
    this._prevStepVisible = val;
  }

  // public goToStep(step: WizardStepComponent): void {
  //   if (!this.isCompleted) {
  //     this.activeStep = step;
  //   }
  // }

  public next(): void {
    let skipHappend = false;
    if (this.hasNextStep) {
      let nextStep: WizardStepComponent;
      nextStep = this.steps[this.activeStepIndex + 1];
      if (Number(nextStep.number) === Number(this.skipNumber)) {
        nextStep = this.steps[this.activeStepIndex + 2];
        skipHappend = true;
      }
      if (nextStep.isValid) {
        this.activeStep.onNext.emit();
      }
      nextStep.isDisabled = this.nextStepDisabled;
      this.activeStep = nextStep;
    }
    if (this.hasPrevStep) {
      let prevStep: WizardStepComponent;
      prevStep = this.steps[this.activeStepIndex - 1];
      this._steps[this.activeStepIndex - 1].isDisabled = true;
    }
    if (skipHappend) {
      let prevStep: WizardStepComponent;
      prevStep = this.steps[this.activeStepIndex - 2];
      this._steps[this.activeStepIndex - 2].isDisabled = true;
    }
  }

  public previous(): void {
    let skipHappend = false;
    if (this.hasPrevStep) {
      let prevStep: WizardStepComponent;
      prevStep = this.steps[this.activeStepIndex - 1];
      if (Number(prevStep.number) === Number(this.skipNumber)) {
        prevStep = this.steps[this.activeStepIndex - 2];
        skipHappend = true;
      }
      this.activeStep.onPrev.emit();
      prevStep.isDisabled = this.preStepDisabled;
      this.activeStep = prevStep;
      this._steps[this.activeStepIndex + 1].isDisabled = true;
    }
    if (skipHappend) {
      let prevStep: WizardStepComponent;
      prevStep = this.steps[this.activeStepIndex + 2];
      this._steps[this.activeStepIndex + 2].isDisabled = true;
    }
  }

  public complete(): void {
    this.activeStep.onComplete.emit();
    this._isCompleted = true;
  }

  public errorState(activeStepIndex: number): void {
    const nextStep: WizardStepComponent = this.steps[activeStepIndex];
    nextStep.isDisabled = false;
    this.activeStep = nextStep;
  }

  public cancel(): void {
    this.activeStep.onCancel.emit();
  }

  public openNewMemberContract(): void {
    this.activeStep.onCompleteWithContract.emit();
    this._isCompleted = true;
    // if (this.selectedMember.BranchId != this._branchID) {
    //   this.messageService.openMessageBox('CONFIRM',
    //     {
    //       messageTitle: 'Exceline',
    //       messageBody: 'MEMBERSHIP.ContractHomeGymChange'
    //     });
    // } else {
    //   console.log(test)
      // this.addContract();
    }


  // addContract() {
  //   if (this.selectedMember.BirthDate && this.selectedMember.Role == 1 && this.selectedMember.BirthDate != new Date(1900, 1, 1)) {
  //     if (this.selectedMember.Age >= 18 || this.selectedMember.GuardianId != -1) {
  //       this.router.navigate(['/membership/card/' + this.selectedMember.Id + '/' + this.selectedMember.BranchId + '/' + MemberRole[this.selectedMember.Role] + '/contracts/register-contract'])
  //     } else {
  //       this.messageService.openMessageBox('WARNING',
  //         {
  //           messageTitle: 'Exceline',
  //           messageBody: 'MEMBERSHIP.MsgCannotSignUp'
  //         });
  //     }
  //   } else {
  //     this.router.navigate(['/membership/card/' + this.selectedMember.Id + '/' + this.selectedMember.BranchId + '/' + MemberRole[this.selectedMember.Role] + '/contracts/register-contract'])
  //   }
  // }

  }
