import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FollowUp } from 'app/shared/SystemObjects/Common/FollowUp';
import { ExceFollowupCommonService } from 'app/shared/components/exce-followup/exce-followup-common.service';
import { EntitySelectionType, ColumnDataType } from 'app/shared/enums/us-enum';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { ExceAddFollowUpService } from 'app/shared/components/exce-followup/exce-add-follow-up.service';
import { Cookie } from 'ng2-cookies/src/services';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { ControlBase } from 'app/shared/components/exce-followup/Control/ControlBase';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { Extension } from 'app/shared/Utills/Extensions';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

const now = new Date();

@Component({
  selector: 'follow-up-template',
  templateUrl: './exce-follow-up-template.component.html',
  styleUrls: ['./exce-follow-up-template.component.scss']
})
export class ExceFollowUpTemplateComponent implements OnInit, OnDestroy, AfterViewInit {
  private destroy$ = new Subject<void>();
  private followupTemplateData: any;


  public followupTemplate: any;

  fixedFollowupTemplate: any;
  selectFixFolCatSubs: Subscription;

  @Input() followUpDetail: any;
  @Input() isAddNew: boolean;
  @Input() dataUpdateMember: any;
  @Input() controls: ControlBase<any>[] = [];
  public isNextFollowUp = false
  form: FormGroup;
  public templateForm: FormGroup;
  questions = [];
  entitySelectionType: string;
  selectedFollowUp: any;
  isEmpSelected: boolean;
  isRoleSelected: boolean;
  empSearchType: string;
  columns: any;
  auotocompleteItems = [{ id: 'NAME', name: 'NAME :', isNumber: false }, { id: 'ID', name: 'Id :', isNumber: true }]
  items = [];
  roleItems = [];
  itemCount: number;
  roleItemCount: number;
  memberSelectionView: any;
  isStartTime: boolean;
  extendedFieldsList: any[];
  isFollowUp = true;
  formSubmited = false;
  formDy: FormGroup;
  payLoad = '';


  formErrors = {
    'Name': '',
    'AssignedEmpName': '',
    'Role:': ''
  };

  validationMessages = {
    'Name': {
      'required': 'MEMBERSHIP.StartDateReq'
    },
    'AssignedEmpName': {
      'required': 'MEMBERSHIP.AssignedEmpNameReq'
    },
    'RoleType': {
      'required': 'MEMBERSHIP.RoleReq'
    }
  };

  constructor(
    private fb: FormBuilder,
    private followUpService: ExceAddFollowUpService,
    private followUpCommonService: ExceFollowupCommonService,
    private translateService: TranslateService,
    private modalService: UsbModal
  ) {}

  ngOnInit() {
    this.formDy = this.fb.group({})
    this.templateForm = this.fb.group({
      'Id': [null],
      'Name': [null, [Validators.required]],
      'TaskCategoryName': [null],
      'TaskCategoryId': [null],
      'PlannedDate': [null],
      'CompletedDate': [null],
      'StartDate': [null],
      'EndDate': [null],
      'StartTime': [null],
      'EndTime': [null],
      'PhoneNo': [null],
      'AutomatedSMS': [null],
      'Description': [null],
      'FoundDate': [null],
      'ReturnDate': [null],
      'DueDate': [null],
      'DueTime': [null],
      'NumOfDays': [null],
      'AutomatedEmail': [null],
      'Comment': [null],
      'IsNextFollowUp': [null],
      'IsPopUp': [null],
      'AssignedEmpName': [null],
      'AssignedEmpId': [null],
      'CompletedEmpId': [null],
      'CompletedEmpName': [null],
      'EntRoleId': [null],
      'RoleType': [null],
      'Sms': [null]

    });
    this.templateForm.statusChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(val => {
      UsErrorService.onValueChanged(this.templateForm, this.formErrors, this.validationMessages)
    });
    this.setFollowUpType('EMP');

    if (this.followUpCommonService.FollowUpTemplate) {
      this.followupTemplate = this.followUpCommonService.FollowUpTemplate.template;
      this.followupTemplateData = this.followUpCommonService.FollowUpTemplate.data;

      this.setFollowupFormData();
    }

    this.followUpCommonService.selectFollowupCategory.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      res => {
        if (res.data) {
          this.followupTemplate = res.template;
          this.followupTemplateData = res.data;
          this.setFollowupFormData(res);
        }
      });
  }

  closeEmployeeSelectionView() {
    this.memberSelectionView.close();
  }

  ngAfterViewInit() {

/*     if (this.followUpCommonService.FollowUpTemplate) {
      this.followupTemplate = this.followUpCommonService.FollowUpTemplate.template;
      console.log(this.followupTemplate)
      this.followupTemplateData = this.followUpCommonService.FollowUpTemplate.data;

      this.setFollowupFormData();
    }

    this.selectFolCatSubs = this.followUpCommonService.selectFollowupCategory.pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        if (res.data) {
          this.followupTemplate = res.template;
          this.followupTemplateData = res.data;
          this.setFollowupFormData(res);
        }
      }); */
  }

  private setFollowupFormData(res?) {
    if (res) {
      this.followupTemplateData = res.data;
    }
    if (this.followupTemplateData) {
      this.isNextFollowUp = this.followupTemplateData.IsNextFollowUpValue
      this.templateForm.patchValue({
        'Id': this.followupTemplateData.Id,
        'Name': this.isAddNew ? '' : this.followupTemplateData.Name,
        'TaskCategoryName': this.followupTemplateData.TaskCategoryName,
        'TaskCategoryId': this.followupTemplateData.TaskCategoryId,
        'PlannedDate': this.followupTemplateData.PlannedDate,
        'CompletedDate': this.followupTemplateData.CompletedDate,
        'StartDate': this.followupTemplateData.StartDate,
        'EndDate': this.followupTemplateData.EndDate,
        'StartTime': this.followupTemplateData.StartTime,
        'EndTime': this.followupTemplateData.EndTime,
        'PhoneNo': this.followupTemplateData.PhoneNo,
        'AutomatedSMS': this.followupTemplateData.AutomatedSMS,
        'Description': this.followupTemplateData.Description,
        'FoundDate': this.followupTemplateData.FoundDate,
        'ReturnDate': this.followupTemplateData.ReturnDate,
        'DueDate': this.followupTemplateData.DueDate,
        'DueTime': this.followupTemplateData.DueTime,
        'NumOfDays': this.followupTemplateData.NumOfDays,
        'AutomatedEmail': this.followupTemplateData.AutomatedEmail,
        'Comment': this.followupTemplateData.Comment,
        'IsNextFollowUp': this.followupTemplateData.IsNextFollowUpValue,
        'IsPopUp': this.followupTemplateData.IsPopUp,
        'AssignedEmpName': this.followupTemplateData.AssignedEmpName,
        'AssignedEmpId': this.followupTemplateData.AssignedEmpId,
        'CompletedEmpId': this.followupTemplateData.CompletedEmpId,
        'CompletedEmpName': this.followupTemplateData.CompletedEmpName,
        'EntRoleId': this.followupTemplateData.EntRoleId,
        'RoleType': this.followupTemplateData.RoleType,
        'Sms': this.followupTemplateData.Sms
      });
    }
  }

  onCheckboxChange(event) {
    if (event.target.checked) {
      this.formDy.patchValue({ [event.target.id]: true });
    } else {
      this.formDy.patchValue({ [event.target.id]: false });
    }
  }

  public getSaveData() {
    const followUpForm = this.templateForm;
    const extendedField = [];
    this.formSubmited = true;
    if (followUpForm.valid) {
      if (this.extendedFieldsList) {
        for (let i = 0; i < this.extendedFieldsList.length; i++) {
          const selectedField = this.formDy.get(this.extendedFieldsList[i].Title);
          let selectedFieldValue;
          if (selectedField !== null) {
            selectedFieldValue = selectedField.value;
          }
          extendedField.push({ Key: this.extendedFieldsList[i].Title, Value: selectedFieldValue })
        }
      }
    followUpForm.value.ExtendedFieldsList = extendedField;
    followUpForm.value.PlannedDate.date.month = (followUpForm.value.PlannedDate.date.month < 10 ) ? Extension.pad(followUpForm.value.PlannedDate.date.month, 2) : followUpForm.value.PlannedDate.date.month;
    followUpForm.value.PlannedDate.date.day = (followUpForm.value.PlannedDate.date.day < 10 ) ? Extension.pad(followUpForm.value.PlannedDate.date.day, 2) : followUpForm.value.PlannedDate.date.day;
    return followUpForm;
    } else {
      UsErrorService.validateAllFormFields(this.templateForm, this.formErrors, this.validationMessages);
      return null
    }
  }

  public setDisabledField(val: boolean) {
    this.isFollowUp = val;
  }

  setFollowUpType(type: string) {
    this.formSubmited = false;
    if (type === 'EMP') {
      this.templateForm.get('AssignedEmpName').setValidators(Validators.required);
      this.templateForm.get('AssignedEmpName').updateValueAndValidity();
      this.templateForm.get('RoleType').setValidators(null);
      this.templateForm.get('RoleType').updateValueAndValidity();
      this.isEmpSelected = true;
      this.isRoleSelected = false;
    } else if (type === 'ROL') {
      this.templateForm.get('RoleType').setValidators(Validators.required);
      this.templateForm.get('RoleType').updateValueAndValidity();
      this.templateForm.get('AssignedEmpName').setValidators(null);
      this.templateForm.get('AssignedEmpName').updateValueAndValidity();
      this.isEmpSelected = false;
      this.isRoleSelected = true;
    }
    this.templateForm.statusChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(_ => UsErrorService.onValueChanged(this.templateForm, this.formErrors, this.validationMessages));
  }

  panelVisibility() {
    if (this.selectedFollowUp.AssignedEmpId != null && this.selectedFollowUp.AssignedEmpId > 0) {
      this.isEmpSelected = true;
    } else if (this.selectedFollowUp.EntRoleId != null && this.selectedFollowUp.EntRoleId > 0) {
      this.isRoleSelected = true;
    } else {
      this.isEmpSelected = true;
    }
  }

  validateFollowUpTemplate() {
    this.formSubmited = true;
    if (this.templateForm.valid) {
      return true;
    } else {
      UsErrorService.validateAllFormFields(this.templateForm, this.formErrors, this.validationMessages);
      return false;
    }
  }

  newMemberModal(content: any, type: string) {
    this.empSearchType = type;
    this.entitySelectionType = EntitySelectionType[EntitySelectionType.EMP];
    this.columns = [{ property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'CustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ];

    this.followUpService.getGymEmployees('', -1).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      result => {
        if (result) {
          this.items = result.Data;
          this.itemCount = result.Data.length;
          this.memberSelectionView = this.modalService.open(content, { width: '1000' });
        } else {
        }
      },
      error => {
      });
  }

  searchDeatail(val: any) {
    this.followUpService.getGymEmployees(val.searchVal, -1).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result) {
          this.items = result.Data;
          this.itemCount = result.Data.length;
        }
      });
  }
  selectedRowItem(item) {
    if (this.empSearchType === 'ASSIGNEMP') {
      this.templateForm.patchValue({
        AssignedEmpId: item.Id,
        AssignedEmpName: item.Name
      });
    } else if (this.empSearchType === 'COMPLETEDEMP') {
      this.templateForm.patchValue({
        CompletedEmpId: item.Id,
        CompletedEmpName: item.Name
      });
    }
    this.memberSelectionView.close();
  }

  roleModal(content: any) {
    this.followUpService.getEmpRole().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.roleItems = result.Data;
        this.roleItemCount = result.Data.length;
        this.memberSelectionView = this.modalService.open(content, { width: '1000' });
      } else {
      }
      });
  }

  roleDoubleClick(rowEvent) {
    this.templateForm.patchValue({
      EntRoleId: rowEvent.row.item.Id,
      RoleType: rowEvent.row.item.RoleName
    });
    this.memberSelectionView.close();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.memberSelectionView) {
      this.memberSelectionView.close();
    }
  }
}
