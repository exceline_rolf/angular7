import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceFollowUpTemplateComponent } from './exce-follow-up-template.component';

describe('ExceFollowUpTemplateComponent', () => {
  let component: ExceFollowUpTemplateComponent;
  let fixture: ComponentFixture<ExceFollowUpTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceFollowUpTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceFollowUpTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
