import { Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { ExceAddFollowUpService } from 'app/shared/components/exce-followup/exce-add-follow-up.service';
import { Cookie } from 'ng2-cookies/src/services';
import { ExceFollowupCommonService } from 'app/shared/components/exce-followup/exce-followup-common.service';
import { FormGroup, FormBuilder, AbstractControl, Validators, ValidationErrors, ValidatorFn, FormControl } from '@angular/forms';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { error } from 'util';
import { Subscription } from 'rxjs';
import { McBasicInfoService } from 'app/modules/membership/member-card/mc-basic-info/mc-basic-info.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'fixed-follow-up',
  templateUrl: './exce-fixed-follow-up.component.html',
  styleUrls: ['./exce-fixed-follow-up.component.scss']
})
export class ExceFixedFollowUpComponent implements OnInit, OnDestroy {
  FollowUpDetailList = [];
  BranchId: any;
  MemberId: any;
  followUpCategoryListData: any[] = [];
  fixedFollowupTemplateData: any;
  fixedFollowupTemplate: any;

  loggedBranchId: any;
  categoryList: any[];
  selectedFollowUpDetail: any;
  isMobileNumberValid: any;
  isTemplateDisable: boolean;
  followUpFixedForm: FormGroup;
  selectedCategoryTitle: any;
  taskCategoryObj: any;
  isSms: boolean;
  isEmail: boolean;
  myFormSubmited: boolean;
  isMobileBlur: boolean;
  isEmailBlur: boolean;
  followUpDetailList: any[];

  @Input() isEnabledFollowUp: boolean;
  @Input() Mobile: string;
  @Input() Email: string;

  @Output() closed = new EventEmitter();
  @Output() emailBlurEvent = new EventEmitter();
  @Output() mobileBlurEvent = new EventEmitter();

  @Output() newFollowUpOrder = new EventEmitter<{}>();

  @ViewChild('followUpTemplate') public followUpTemplate;

  followUpCategoryList: any[] = [];
  destroy$: Subject<boolean>;

  formErrors = {
    'Mobile': '',
    'Email': ''
  };

  validationMessages = {
    'Mobile': {
      'required': 'Mobile is required.',
      'phoneNumber': 'invalid phone number',
      'mobileTaken': 'Mobile exit'
    },
    'Email': {
      'required': 'email is required.',
      'email': 'Please enter valid email address'
    }
  };
  addFixedFollowUpToView: {
    MemberId: number;
    FollowUpTemplateId: number;
    FollowUpDetailList: any[];
    SmsPhoneNo: number;
    SmsPrefix: any;
    EmailAddress: any;
    BranchId: number;
    IsInsert: boolean;
    Id: number;
  };

  constructor(
    private fb: FormBuilder,
    private followUpService: ExceAddFollowUpService,
    private followUpCommonService: ExceFollowupCommonService,
    private exceMessageService: ExceMessageService,
    private basicInfoService: McBasicInfoService,
    private translate: TranslateService
  ) {
    this.followUpFixedForm = this.fb.group({
      // 'Id': [null],
      'followUpCategory': [this.selectedFollowUpDetail],
      'title': [this.selectedCategoryTitle],
      'Mobile': [this.Mobile],
      'Email': [this.Email],
      'MobilePrefix': [null]
    });
  }

  ngOnInit() {
    this.destroy$ = new Subject<boolean>();
    this.loggedBranchId = JSON.parse(Cookie.get('selectedBranch')).BranchId;
    this.basicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      member => {
        if (member) {
          this.MemberId = member.Id;
          this.BranchId = member.BranchId;
        }
      }, null, null);

    this.followUpService.GetFollowUpTask().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
        if (result) {
          this.followUpCategoryList = result.Data;
          this.taskCategoryObj = result.Data;
          this.taskCategoryObj.forEach(i => {
            this.followUpCategoryListData.push({
              'FollowUpId': 1, 'TaskCategoryId': i.ExcelineTaskCategory.Id, 'TaskCategoryName': i.ExcelineTaskCategory.Name
              , 'NumOfDays': i.NumberOfDays, 'IsNextFollowUpValue': i.IsNextFollowUpValue, 'Name': i.ExcelineTaskCategory.Name, 'CreatedUser': '',
              'CreatedDateTime': new Date(), 'PlannedDate': {
                date: {
                  year: new Date().getFullYear(),
                  month: new Date().getMonth() + 1,
                  day: new Date().getDate()
                }
              }, 'CompletedDate': null, 'IsStartTime': i.ExcelineTaskCategory.IsStartTime
              , 'StartTime': i.ExcelineTaskCategory.IsStartTime ? '8:30' : null, 'IsEndTime': i.ExcelineTaskCategory.IsEndTime
              , 'EndTime': i.ExcelineTaskCategory.IsEndTime ? '10:30' : null, 'AssignedEmpName': '', 'AssignedEmpId': 0,
              'CompletedEmpName': '', 'CompletedEmpId': 0, 'RoleType': '', 'EntRoleId': 0, 'Comment': '', 'Discription': '', 'DueDate': null,
              'DueTime': null, 'FoundDate': null, 'IsPopUp': false, 'Phoneno': '', 'ReturnDate': null, 'Sms': '', 'StartDate': null,
              'EndDate': null, 'AutomatedEmail': '', 'ExtendedFieldsList': null, 'IsAutomatedEmail': i.ExcelineTaskCategory.IsAutomatedEmail, 'IsAutomatedSMS': i.ExcelineTaskCategory.IsAutomatedSMS
            })
          });

          this.followUpService.addFollowupCategoryChange.next(this.selectedFollowUpDetail);
          for (let i = 0; i < result.Data.length; i++) {
            const selectedCategory = result.Data[i].ExcelineTaskCategory;
            this.categoryList = selectedCategory;
          }
          this.selectedFollowUpDetail = this.followUpCategoryList[0];
        }
      }, null, null);
      this.followUpFixedForm.valueChanges.pipe(
        takeUntil(this.destroy$)
        ).subscribe(data => {
        if (data.followUpCategory == null) {
          this.selectedCategoryTitle = '';
        } else {
          this.selectedCategoryTitle = data.followUpCategory.TaskCategoryName;
        }
      });
  }


  // when followup category changed
  followUpChangeEvent(followupData) {
    if (followupData === undefined) {
      return
    }
    const data = this.followUpCategoryListData.find(x => x.TaskCategoryId === followupData.ExcelineTaskCategory.Id);
    if (followupData) {
      this.followUpCommonService.FollowUpTemplate = {
        template: followupData.ExcelineTaskCategory,
        data: data
      };
    }
    this.followUpCommonService.selectFollowupCategory.next(followupData.ExcelineTaskCategory);
    if (followupData.ExcelineTaskCategory.IsAutomatedEmail === true && followupData.ExcelineTaskCategory.IsAutomatedEmail === true) {
      this.isSms = true;
      this.isEmail = true;
    } else {
      this.isSms = false;
      this.isEmail = false;
    }

  }

  fullValueChange(event: any) {
    this.isMobileNumberValid = event.numberType === 1 && event.valid;
  }

  mobileNumberValidator(control: AbstractControl, type: string) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (!this.isMobileNumberValid) {
          resolve({
            'phoneNumber': true
          })
        } else {
          resolve(null);
        }
      }, 10);
    });
  }

  private emailValidator(control: AbstractControl): ValidationErrors {
    if (!control.value) {
      return null;
    }
    return Validators.email(control);
  }


  mobileBlur(val: any, type: string) {
    this.isMobileBlur = true;
    if (this.followUpFixedForm.controls['Mobile'].errors == null || (!this.followUpFixedForm.controls['Mobile'].errors.required &&
      !this.followUpFixedForm.controls['Mobile'].errors.phoneNumber)) { // email is exits check after valid email
      this.followUpService.validateMobile(this.followUpFixedForm.value.Mobile, this.followUpFixedForm.value.MobilePrefix).subscribe(
        result => {
          if (result) {
            const outPutval = result.Data.split(':')[0].trim()
            if (Number(outPutval) === 0) {
              const memberName = result.Data.split(':')[1].trim()
              const errorMsg = memberName + ' ' + 'has same mobile number'

              this.followUpFixedForm.controls['Mobile'].setErrors({
                'mobileTaken': true
              });
              this.formErrors.Mobile = errorMsg;
            } else {
              this.mobileBlurEvent.emit({ Mobile: this.followUpFixedForm.value.Mobile, MobilePrefix: this.followUpFixedForm.value.MobilePrefix });

            }
          }
        });
    }
  }

  mobileFocus() {
    this.isMobileBlur = false
  }

  emailBlur() {
    this.isEmailBlur = true;
    if (this.followUpFixedForm.controls['Email'].errors == null) {
      this.emailBlurEvent.emit(this.followUpFixedForm.value.Email)
    }
  }

  emailFocus() {
    this.isEmailBlur = false;
  }


  // save followup
  saveFollowUp(value) {
    const fixedFollowUpData = this.followUpTemplate.getSaveData(); // Getting whole followUp-template data
    if (fixedFollowUpData !== null) {
    this.myFormSubmited = true;
    const fixedFollowUpValue = fixedFollowUpData.value;
    fixedFollowUpValue.ActiveStatus = true;
    fixedFollowUpValue.PlannedDate = fixedFollowUpValue.PlannedDate.date.year + '-' + fixedFollowUpValue.PlannedDate.date.month + '-' + fixedFollowUpValue.PlannedDate.date.day + 'T00:00:00';
    this.FollowUpDetailList.push(fixedFollowUpValue);
    const addFixedFollowUpList = [{
      MemberId: this.MemberId,
      FollowUpTemplateId: value.followUpCategory.FollowUpTemplateId,
      FollowUpDetailList: this.FollowUpDetailList,
      SmsPhoneNo: value.Mobile,
      SmsPrefix: value.MobilePrefix,
      EmailAddress: value.Email,
      BranchId: this.BranchId,
      IsInsert: true
    }];
    this.followUpService.saveFollowUp(addFixedFollowUpList).pipe(
      takeUntil(this.destroy$)
      ).subscribe(res => {
      if (res.Data) {
        const Id = res.Data;
        this.addFixedFollowUpToView = {
          MemberId: this.MemberId,
          FollowUpTemplateId: value.followUpCategory.FollowUpTemplateId,
          FollowUpDetailList: this.FollowUpDetailList,
          SmsPhoneNo: value.Mobile,
          SmsPrefix: value.MobilePrefix,
          EmailAddress: value.Email,
          BranchId: this.BranchId,
          IsInsert: true,
          Id: Id
        }
      }
    }, null, () => {
        this.newFollowUpOrder.emit(this.addFixedFollowUpToView);
    });
    }
  }

  setFollowUpDetail(item: any) {
    const followUpList = [];
    this.followUpDetailList.forEach(x => { followUpList.push(x) });
    for (let i = 0; i < this.followUpDetailList.length; i++) {
      if ((item.Id <= 0 && this.followUpDetailList[i].Id === item.Id) ||
        (item.Id > 0 && item.Id === this.followUpDetailList[i].Id)) {
        followUpList[i] = item;
      }
      if (this.followUpDetailList[i].Id <= 0 && this.followUpDetailList[i].IsStartTime && this.followUpDetailList[i].IsEndTime
        && this.followUpDetailList[i].StartTime != null && this.followUpDetailList[i].EndTime != null) {
        followUpList[i].IsShowInCalandar = true;
      }

      if (this.followUpDetailList[i].AssignedEmpId === 0 && this.followUpDetailList[i].EntRoleId === 0) {
        followUpList[i].AssignedEmpId = item.AssignedEmpId;
        followUpList[i].AssignedEmpName = item.AssignedEmpName;
        followUpList[i].EntRoleId = item.EntRoleId;
        followUpList[i].RoleType = item.RoleType;
      }
    }
    this.followUpDetailList = followUpList;

  }

  closeView() {
    this.closed.emit();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }


}
