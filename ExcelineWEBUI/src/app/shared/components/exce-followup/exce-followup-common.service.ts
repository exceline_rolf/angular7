import { Injectable, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ControlBase } from 'app/shared/components/exce-followup/Control/ControlBase';
import { TextboxControl } from 'app/shared/components/exce-followup/Control/TextboxControl';
import { CheckboxControl } from 'app/shared/components/exce-followup/Control/CheckboxControl';
import { DatepickerControl } from 'app/shared/components/exce-followup/Control/DatepickerControl';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';

@Injectable()
export class ExceFollowupCommonService {
  followUp: any;
  taskCategory: any;
  private isTemplateVisible: boolean;

  public changeFollowupCategory = new Subject<any>();

  public templatesListLoaded = new Subject<any>();

  set IsTemplateVisible(isTemplateVisible) {
    this.isTemplateVisible = isTemplateVisible;
    this.templatesListLoaded.next();
  }

  get IsTemplateVisible() {
    return this.isTemplateVisible;
  }

  public followupDataUpdateEvent = new Subject<any>();
  private selectedFollowupMember: any;

  set SelectedFollowupMember(selectedFollowupMember) {
    this.followupDataUpdateEvent.next(selectedFollowupMember);
    this.selectedFollowupMember = selectedFollowupMember;
  }
  get SelectedFollowupMember() {
    return this.selectedFollowupMember;
  }

  private followUpTemplate: any;
  get FollowUpTemplate() {
    return this.followUpTemplate;
  }
  set FollowUpTemplate(followUpTemplate) {
    this.followUpTemplate = followUpTemplate;
    this.selectFollowupCategory.next(followUpTemplate);
  }

  ///////////////////////////////////////

  // private fixedFollowUpTemplate: any;
  // get FixedFollowUpTemplate() {
  //   return this.fixedFollowUpTemplate;
  // }
  // set FixedFollowUpTemplate(fixedFollowUpTemplate) {
  //   this.fixedFollowUpTemplate = fixedFollowUpTemplate;
  //   this.fixedSelectedFollowupCategory.next(fixedFollowUpTemplate);
  // }
  // fixedSelectedFollowupCategory = new Subject<any>();

  //////////////////////////////////////////



  followUpSelectEvent: EventEmitter<any> = new EventEmitter();
  taskCategorySelectEvent: EventEmitter<any> = new EventEmitter();
  selectFollowupCategory = new Subject<any>();


  constructor() { }

  setFollowUp(folowUp: any) {
    this.followUp = folowUp;
    this.followUpSelectEvent.emit(this.followUp);
  }

  setExcelineTaskCategory(taskcategory: any) {
    this.taskCategory = taskcategory;
    this.taskCategorySelectEvent.emit(this.taskCategory);
  }

  toFormGroup(controls: ControlBase<any>[]) {
    const group: any = {};

    controls.forEach(question => {
      group[question.key] = question.required ? new FormControl(question.value || '', Validators.required)
        : new FormControl(question.value || '');
    });
    return new FormGroup(group);
  }

  getControls(ExtendedFieldsList: any, ExtendedFieldValueList: any) {
    let controls: ControlBase<any>[];
    controls = [];

    if (ExtendedFieldsList && ExtendedFieldsList.length > 0) {

      for (let i = 0; i < ExtendedFieldsList.length; i++) {
        const item = ExtendedFieldsList[i];
        let val: any = null;
        if (ExtendedFieldValueList !== null && ExtendedFieldValueList !== undefined) {
          val = ExtendedFieldValueList.find(x => x.Key === item.Title).Value;
        }

        if (item.FieldType === 0 || item.FieldType === 1) {
          controls.push(new TextboxControl({
            key: item.Title,
            label: item.Title,
            type: 'textbox',
            value: val,
            required: true,
            order: 1
          }))
        }
        if (item.FieldType === 2) {
          controls.push(
            new CheckboxControl({
              key: item.Title,
              label: item.Title,
              type: 'checkbox',
              value: val === null ? 'false' : val,
              required: true,
              order: 3
            }),
          )
        }
        if (item.FieldType === 3) {
          controls.push(
            new DatepickerControl({
              key: item.Title,
              label: item.Title,
              type: 'datepicker',
              value: val,
              required: true,
              order: 3
            }),
          )
        }
      }
    }

    //  questions = [
    //    new DropdownQuestion({
    //      key: 'brave',
    //      label: 'Bravery Rating',
    //      options: [
    //        {key: 'solid',  value: 'Solid'},
    //        {key: 'great',  value: 'Great'},
    //        {key: 'good',   value: 'Good'},
    //        {key: 'unproven', value: 'Unproven'}
    //      ],
    //      order: 3
    //    }),
    //    new TextboxQuestion({
    //      key: 'firstName',
    //      label: 'First name',
    //      value: 'Bombasto',
    //      required: true,
    //      order: 1
    //    }),
    //    new TextboxQuestion({
    //      key: 'emailAddress',
    //      label: 'Email',
    //      type: 'email',
    //      order: 2
    //    }),
    //    new CheckboxControl({
    //     key: 'agree',
    //     label: 'I Agree',
    //     type: 'checkbox',
    //     value: true,
    //     required: true,
    //     order: 3
    //   }),
    //   new Datepicker({
    //     key: 'ReturnDate',
    //     label: 'ReturnDate',
    //     type: 'datepicker',
    //     required: true,
    //     order: 4
    //   })
    // ];
    return controls.sort((a, b) => a.order - b.order);
  }


}
