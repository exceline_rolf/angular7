import { Injectable } from '@angular/core';
import { CommonHttpService } from 'app/shared/services/common-http.service';
import { ConfigService } from '@ngx-config/core';
import { Subject, Observable } from 'rxjs';
import { ExceLoginService } from 'app/modules/login/exce-login/exce-login.service';

@Injectable()
export class ExceAddFollowUpService {
  private branchId: number;
  private userApiURL: string;
  private memberApiURL: string;
  isDisableFollowupProfileList: boolean;

  public addFollowupCategoryChange = new Subject<any>();

  get IsDisableFollowupProfileList(): boolean {
    return this.isDisableFollowupProfileList;
  }

  set IsDisableFollowupProfileList(isDisableFollowupProfileList) {
    this.isDisableFollowupProfileList = isDisableFollowupProfileList;
  }

  constructor(
    private commonHttpService: CommonHttpService,
    private config: ConfigService,
    private exceLoginService: ExceLoginService
  ) {
    this.userApiURL = this.config.getSettings('EXCE_API.COMMON');
    this.memberApiURL = this.config.getSettings('EXCE_API.MEMBER');
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }

  getFollowUpTemplates(branchId: number): Observable<any> {
    const url = this.memberApiURL + '/GetFollowUpTemplates?branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getFollowUpTaskByTemplateIds(followUpTemplateId: number): Observable<any> {
    const url = this.memberApiURL + '/GetFollowUpTaskByTemplateIds?followUpTemplateId=' + followUpTemplateId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }
  getGymEmployees(searchText: string, roleId: number): Observable<any> {
    const url = this.memberApiURL + '/GetGymEmployees?branchId=' + this.branchId + '&searchText=' + searchText + '&roleId=' + roleId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getEmpRole(): Observable<any> {
    const url = this.memberApiURL + '/GetEmpRole';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  saveFollowUp(followUpList: any): Observable<any> {
    const url = this.memberApiURL + '/SaveFollowUp'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: followUpList
    });
  }

  validateMobile(mobile: string, mobilePrifix: string): Observable<any> {
    const url = this.memberApiURL + '/ValidateMobile?mobile=' + mobile + '&mobilePrefix=' + mobilePrifix;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetFollowUpTask(): Observable<any> {
    const url = this.memberApiURL + '/GetFollowUpTask';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

}
