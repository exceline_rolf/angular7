import { TestBed, inject } from '@angular/core/testing';

import { ExceFollowupCommonService } from './exce-followup-common.service';

describe('ExceFollowupCommonService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceFollowupCommonService]
    });
  });

  it('should be created', inject([ExceFollowupCommonService], (service: ExceFollowupCommonService) => {
    expect(service).toBeTruthy();
  }));
});
