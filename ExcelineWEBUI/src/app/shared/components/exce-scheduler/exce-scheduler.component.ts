import { NgModule, Component, ElementRef, OnDestroy, DoCheck, OnChanges, Input, Output, EventEmitter, IterableDiffers, OnInit, AfterViewChecked, SimpleChanges, TemplateRef } from '@angular/core';
import { CommonModule } from '@angular/common';
declare var jQuery: any;

@Component({
    selector: 'exce-scheduler',
    templateUrl: './exce-scheduler.component.html',
    styleUrls: ['./exce-scheduler.component.scss']
})
export class ExceSchedulerComponent implements DoCheck, OnDestroy, OnInit, OnChanges, AfterViewChecked {
    @Input() buttonText: any;
    @Input() buttonIcons: any;
    @Input() theme: any;
    @Input() resources: any[];
    @Input() resourceAreaWidth: string;
    @Input() resourceLabelText: string;
    @Input() slotLabelFormat: any = 'H:mm';
    @Input() titleFormat: any;
    @Input() eventBackgroundColor: any
    @Input() events: any[];
    @Input() header: any;
    @Input() views: any;
    @Input() style: any;
    @Input() styleClass: string;
    @Input() rtl: boolean;
    @Input() weekends: boolean;
    @Input() hiddenDays: number[];
    @Input() fixedWeekCount: boolean;
    @Input() weekNumbers: boolean;
    @Input() businessHours: any;
    @Input() height: any;
    @Input() contentHeight: any;
    @Input() aspectRatio = 1.35;
    @Input() eventLimit: any;
    @Input() defaultDate: any;
    @Input() selectable: boolean;
    @Input() editable: boolean;
    @Input() navLinks: boolean;
    @Input() droppable: boolean;
    @Input() eventStartEditable: boolean;
    @Input() eventDurationEditable: boolean;
    @Input() defaultView = 'month';
    @Input() allDaySlot = true;
    @Input() refetchResourcesOnNavigate = true;
    @Input() allDayText = 'all-day';
    @Input() slotDuration: any = '00:10:00';
    @Input() slotLabelInterval: any;
    @Input() snapDuration: any;
    @Input() scrollTime: any = '06:00:00';
    @Input() minTime: any = '00:00:00';
    @Input() maxTime: any = '24:00:00';
    @Input() slotEventOverlap = true;
    @Input() nowIndicator: boolean;
    @Input() dragRevertDuration = 500;
    @Input() dragOpacity = .75;
    @Input() dragScroll = true;
    @Input() eventOverlap: any;
    @Input() eventConstraint: any;
    @Input() constraint: any;
    @Input() locale: string;
    @Input() timezone: boolean | string = false;
    @Input() timeFormat: string | null = 'H(:mm)';
    @Input() eventRender: Function;
    @Input() dayRender: Function;
    @Input() options: any;
    @Input() groupByResource: any;
    @Output() onDayClick: EventEmitter<any> = new EventEmitter();
    @Output() onSelect: EventEmitter<any> = new EventEmitter();
    @Output() onDayRightClick: EventEmitter<any> = new EventEmitter();
    @Output() onBackgroundRightClick: EventEmitter<any> = new EventEmitter();
    @Output() onEventRightclick: EventEmitter<any> = new EventEmitter();
    @Output() onDrop: EventEmitter<any> = new EventEmitter();
    @Output() onEventClick: EventEmitter<any> = new EventEmitter();
    @Output() onEventMouseover: EventEmitter<any> = new EventEmitter();
    @Output() onEventMouseout: EventEmitter<any> = new EventEmitter();
    @Output() onEventDragStart: EventEmitter<any> = new EventEmitter();
    @Output() onEventDragStop: EventEmitter<any> = new EventEmitter();
    @Output() onEventDrop: EventEmitter<any> = new EventEmitter();
    @Output() onEventResizeStart: EventEmitter<any> = new EventEmitter();
    @Output() onEventResizeStop: EventEmitter<any> = new EventEmitter();
    @Output() onEventResize: EventEmitter<any> = new EventEmitter();
    @Output() onViewRender: EventEmitter<any> = new EventEmitter();
    @Output() onViewDestroy: EventEmitter<any> = new EventEmitter();
    @Output() onRemoveEvent: EventEmitter<any> = new EventEmitter();

    initialized: boolean;
    stopNgOnChangesPropagation: boolean;
    differ: any;
    schedule: any;
    config: any;

    constructor(public el: ElementRef, differs: IterableDiffers) {
        this.differ = differs.find([]).create(null);
        this.initialized = false;
    }

    ngOnInit() {
        this.config = {
            resources: this.resources,
            resourceAreaWidth: this.resourceAreaWidth,
            resourceLabelText: this.resourceLabelText,
            titleFormat: this.titleFormat,
            slotLabelFormat: this.slotLabelFormat,
            eventBackgroundColor: this.eventBackgroundColor,
            views: this.views,
            theme: this.theme,
            refetchResourcesOnNavigate: this.refetchResourcesOnNavigate,
            buttonIcons: this.buttonIcons,
            header: this.header,
            buttonText: this.buttonText,
            isRTL: this.rtl,
            firstDay: 1,
            weekends: this.weekends,
            hiddenDays: this.hiddenDays,
            fixedWeekCount: this.fixedWeekCount,
            weekNumbers: this.weekNumbers,
            businessHours: this.businessHours,
            height: this.height,
            contentHeight: this.contentHeight,
            aspectRatio: this.aspectRatio,
            eventLimit: this.eventLimit,
            defaultDate: this.defaultDate,
            locale: this.locale,
            timezone: this.timezone,
            timeFormat: this.timeFormat,
            editable: this.editable,
            navLinks: this.navLinks,
            selectable: this.selectable,
            droppable: this.droppable,
            eventStartEditable: this.eventStartEditable,
            eventDurationEditable: this.eventDurationEditable,
            defaultView: this.defaultView,
            allDaySlot: this.allDaySlot,
            allDayText: this.allDayText,
            slotDuration: this.slotDuration,
            slotLabelInterval: this.slotLabelInterval,
            snapDuration: this.snapDuration,
            scrollTime: this.scrollTime,
            minTime: this.minTime,
            maxTime: this.maxTime,
            slotEventOverlap: this.slotEventOverlap,
            nowIndicator: this.nowIndicator,
            dragRevertDuration: this.dragRevertDuration,
            dragOpacity: this.dragOpacity,
            dragScroll: this.dragScroll,
            eventOverlap: this.eventOverlap,
            eventConstraint: this.eventConstraint,
            constraint: this.constraint,
            eventRender: this.eventRender,
            groupByResource: this.groupByResource,
            dayRender: this.dayRender,
            dayClick: (date, jsEvent, view, resource) => {
                this.onDayClick.emit({
                    'date': date,
                    'jsEvent': jsEvent,
                    'view': view,
                    'resource': resource ? resource.id : '(no resource)'
                });
            },
            select: (date, start, end, jsEvent, view, resource) => {
                this.onSelect.emit({
                    'date': date,
                    'start': start,
                    'end': end,
                    'jsEvent': jsEvent,
                    'view': view,
                    'resource': resource ? resource.id : '(no resource)'
                });
            },
            dayRightClick: (date, jsEvent, view, resource) => {
                    this.onDayRightClick.emit({
                        'element': this,
                        'date': date,
                        'jsEvent': jsEvent,
                        'view': view,
                        'resource': resource ? resource.id : '(no resource)'
                    });
            },
            eventRightclick: (date, jsEvent, view, resource) => {
                this.onEventRightclick.emit({
                    'element': this,
                    'date': date,
                    'jsEvent': jsEvent,
                    'view': view,
                    'resource': resource ? resource.id : '(no resource)'
                });
            },
            drop: (date, jsEvent, ui, resourceId) => {
                this.onDrop.emit({
                    'date': date,
                    'jsEvent': jsEvent,
                    'ui': ui,
                    'resourceId': resourceId
                });
            },
            eventClick: (calEvent, jsEvent, view) => {
              // check if delete X on booking is pressed
              if (jsEvent.target.className !== 'icon-close float-right fc-close') {
                this.onEventClick.emit({
                    'calEvent': calEvent,
                    'jsEvent': jsEvent,
                    'view': view
                });
              } else {
                this.onRemoveEvent.emit({
                  'calEvent': calEvent,
                  'jsEvent': jsEvent,
                  'view': view
              });
              }
            },
            removeEvent: (calEvent, jsEvent, view) => {
                this.onRemoveEvent.emit({
                    'calEvent': calEvent,
                    'jsEvent': jsEvent,
                    'view': view
                });
            },
            eventMouseover: (calEvent, jsEvent, view) => {
                this.onEventMouseover.emit({
                    'calEvent': calEvent,
                    'jsEvent': jsEvent,
                    'view': view
                });
            },
            eventMouseout: (calEvent, jsEvent, view) => {
                this.onEventMouseout.emit({
                    'calEvent': calEvent,
                    'jsEvent': jsEvent,
                    'view': view
                });
            },
            eventDragStart: (event, jsEvent, ui, view) => {
                this.onEventDragStart.emit({
                    'event': event,
                    'jsEvent': jsEvent,
                    'view': view
                });
            },
            eventDragStop: (event, jsEvent, ui, view) => {
                this.onEventDragStop.emit({
                    'event': event,
                    'jsEvent': jsEvent,
                    'view': view
                });
            },
            eventDrop: (event, delta, revertFunc, jsEvent, ui, view) => {
                    this.updateEvent(event);
                    this.onEventDrop.emit({
                        'event': event,
                        'delta': delta,
                        'revertFunc': revertFunc,
                        'jsEvent': jsEvent,
                        'view': view
                    });
            },
            eventResizeStart: (event, jsEvent, ui, view) => {
                this.onEventResizeStart.emit({
                    'event': event,
                    'jsEvent': jsEvent,
                    'view': view
                });
            },
            eventResizeStop: (event, jsEvent, ui, view) => {
                this.onEventResizeStop.emit({
                    'event': event,
                    'jsEvent': jsEvent,
                    'view': view
                });
            },
            eventResize: (event, delta, revertFunc, jsEvent, ui, view) => {
                this.updateEvent(event);

                this.onEventResize.emit({
                    'event': event,
                    'delta': delta,
                    'revertFunc': revertFunc,
                    'jsEvent': jsEvent,
                    'view': view
                });
            },
            viewRender: (view, element) => {
                this.onViewRender.emit({
                    'view': view,
                    'element': element
                });
            },
            viewDestroy: (view, element) => {
                this.onViewDestroy.emit({
                    'view': view,
                    'element': element
                });
            }
        };

        if (this.options) {
            for (let prop in this.options) {
                this.config[prop] = this.options[prop];
            }
        }
    }

    ngAfterViewChecked() {
        if (!this.initialized && this.el.nativeElement.offsetParent) {
            this.initialize();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.schedule) {
            const options = {};
            for (const change in changes) {
                if (change !== 'events') {
                    options[change] = changes[change].currentValue;
                }
            }

            if (Object.keys(options).length) {
                this.schedule.fullCalendar('option', options);
            }
        }
    }

    initialize() {
        this.schedule = jQuery(this.el.nativeElement.children[0]);
        this.schedule.fullCalendar(this.config);
        this.schedule.fullCalendar('addEventSource', this.events);
        this.initialized = true;
    }

    ngDoCheck() {
        const changes = this.differ.diff(this.events);

        if (this.schedule && changes) {
            this.schedule.fullCalendar('removeEventSources');
            this.schedule.fullCalendar('addEventSource', this.events);
        }
    }

    ngOnDestroy() {
        jQuery(this.el.nativeElement.children[0]).fullCalendar('destroy');
        this.initialized = false;
        this.schedule = null;
    }

    // removeEvent(event) {
    //     this.schedule.fullCalendar('removeEventSources', event);
    // }

    gotoDate(date: any) {
        this.schedule.fullCalendar('gotoDate', date);
    }

    prev() {
        this.schedule.fullCalendar('prev');
    }

    next() {
        this.schedule.fullCalendar('next');
    }

    prevYear() {
        this.schedule.fullCalendar('prevYear');
    }

    nextYear() {
        this.schedule.fullCalendar('nextYear');
    }

    today() {
        this.schedule.fullCalendar('today');
    }

    incrementDate(duration: any) {
        this.schedule.fullCalendar('incrementDate', duration);
    }

    changeView(viewName: string, startDate?: Date, endDate?: Date) {
        this.schedule.fullCalendar('changeView', viewName, {
            start: startDate,
            end: endDate
        });
    }

    getDate() {
        return this.schedule.fullCalendar('getDate');
    }

    getView() {
        return this.schedule.fullCalendar('getView').type;
    }

    getCalendarDateRange() {
        const view = this.schedule.fullCalendar('getCalendar').view;
        const start = view.start._d;
        const end = view.end._d;
        const dates = { start: start, end: end };
        return dates;
    }

    refetchEvents() {
        this.schedule.fullCalendar('refetchEvents');
    }

    updateEvents(events) {
        this.schedule.fullCalendar('updateEvents', events);

    }

    refetchResources() {
        this.schedule.fullCalendar('refetchResources');
    }

    findEvent(id: string) {
        let event;
        if (this.events) {
            for (let e of this.events) {
                if (e.id === id) {
                    event = e;
                    break;
                }
            }
        }
        return event;
    }

    updateEvent(event: any) {
        const sourceEvent = this.findEvent(event.id);
        if (sourceEvent) {
            sourceEvent.start = event.start.format();
            if (event.end) {
                sourceEvent.end = event.end.format();
            }
        }
    }
}
