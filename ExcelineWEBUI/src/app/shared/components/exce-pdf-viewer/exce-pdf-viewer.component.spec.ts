import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcePdfViewerComponent } from './exce-pdf-viewer.component';

describe('ExcePdfViewerComponent', () => {
  let component: ExcePdfViewerComponent;
  let fixture: ComponentFixture<ExcePdfViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExcePdfViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcePdfViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
