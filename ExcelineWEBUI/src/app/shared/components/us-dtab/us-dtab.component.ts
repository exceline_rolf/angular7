import { Component, OnInit, Input } from '@angular/core';
import { BasicContent } from './basic-content';

@Component({
  selector: 'dtab',
  template: '',
  styleUrls: ['./us-dtab.component.scss']
})
export class UsDtabComponent {
  @Input() title: string;
  @Input() contentRef: BasicContent;
  active = false;
}
