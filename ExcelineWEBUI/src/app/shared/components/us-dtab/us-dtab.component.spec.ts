import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsDtabComponent } from './us-dtab.component';

describe('UsDtabComponent', () => {
  let component: UsDtabComponent;
  let fixture: ComponentFixture<UsDtabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsDtabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsDtabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
