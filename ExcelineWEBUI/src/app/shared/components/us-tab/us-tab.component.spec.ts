import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsTabComponent } from './us-tab.component';

describe('UsTabComponent', () => {
  let component: UsTabComponent;
  let fixture: ComponentFixture<UsTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
