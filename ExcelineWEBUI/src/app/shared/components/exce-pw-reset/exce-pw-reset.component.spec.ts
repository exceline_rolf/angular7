import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcePwResetComponent } from './exce-pw-reset.component';

describe('ExcePwResetComponent', () => {
  let component: ExcePwResetComponent;
  let fixture: ComponentFixture<ExcePwResetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExcePwResetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcePwResetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
