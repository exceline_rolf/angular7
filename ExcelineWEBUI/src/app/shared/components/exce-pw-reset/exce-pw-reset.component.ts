import { Component, Injectable, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service'
import { TreeviewItem, TreeviewConfig, DownlineTreeviewItem, TreeviewHelper } from 'app/shared/components/us-checkbox-tree';
import { CommonEncryptionService } from 'app/shared/services/common-Encryptor-Decryptor';
import { USSAdminService } from '../../../modules/uss-admin/services/uss-admin-service'
@Component({
  selector: 'exce-pw-reset',
  templateUrl: './exce-pw-reset.component.html',
  styleUrls: ['./exce-pw-reset.component.scss']
})
export class ExcePwResetComponent implements OnInit {
  public userSettingsForm: any;
  public isSubmitChangePw = false;


  @Input() selectedUser: any;
  @Output() canceled = new EventEmitter();
  @Output() saveSuccess = new EventEmitter();

  formErrors = {
    // 'oldPassword': '',
    'newPassowrd': '',
    'confirmPassowrd': '',
  };

  validationMessages = {

    // 'oldPassword': { 'required': 'Date is required.', 'oldPwNotMatching': 'Please Confirm Password' },
    'newPassowrd': { 'required': 'Date is required.', },
    'confirmPassowrd': { 'required': 'Date is required.', 'pwDontMatch': 'Please retype password correctly' },

  };


  constructor(private ussAdminService: USSAdminService,
    private commonEncryptionService: CommonEncryptionService,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {

    this.userSettingsForm = this.fb.group({
      // 'oldPassword': [null, [Validators.required]],
      'newPassowrd': [null, [Validators.required]],
      'confirmPassowrd': [null, [Validators.required]],
    });
  }

  // PwValidator(value) {
  //   this.ussAdminService.ValidateUser({ 'Password': this.commonEncryptionService.encryptValue(value.oldPassword) }).subscribe(result => {
  //     if (result.Data) {
  //       if (result.Data === "0") {
  //         this.userSettingsForm.controls['oldPassword'].setErrors({
  //           'oldPwNotMatching': false,
  //         });
  //       }

  //       if (value.confirmPassowrd !== value.newPassowrd) {
  //         this.userSettingsForm.controls['confirmPassowrd'].setErrors({
  //           'pwDontMatch': false,
  //         });
  //       }

  //       this.submitPwChange(value.newPassowrd);
  //     }
  //   });
  // }

  submitPwChange(value: any) {
    if (value.newPassowrd !== value.confirmPassowrd) {
      this.userSettingsForm.controls['confirmPassowrd'].setErrors({
          'pwDontMatch': false,
        });
        // UsErrorService.validateAllFormFields(this.userSettingsForm, this.formErrors, this.validationMessages);
        // this.isSubmitChangePw = true;
        // return;
    }
    this.isSubmitChangePw = true;
    UsErrorService.validateAllFormFields(this.userSettingsForm, this.formErrors, this.validationMessages);
    if (this.userSettingsForm.valid) {
      this.EditPw(value.newPassowrd)
    }
  }



  EditPw(newPassword: string) {
    this.selectedUser.passowrd = this.commonEncryptionService.encryptValue(newPassword);
    this.selectedUser.IsLoginFirstTime = true;
    this.ussAdminService.EditUSPUserBasicInfo({'Password': this.commonEncryptionService.encryptValue(newPassword), 'isPasswdChange': true, 'uspUser': this.selectedUser }).subscribe(result => {
      if (result.Data) {
        this.saveSuccess.emit();
      }
    })
  }

  canceledMethod() {
    this.canceled.emit();
  }
}
