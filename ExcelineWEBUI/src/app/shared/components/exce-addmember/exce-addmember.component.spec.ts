import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceAddmemberComponent } from './exce-addmember.component';

describe('ExceAddmemberComponent', () => {
  let component: ExceAddmemberComponent;
  let fixture: ComponentFixture<ExceAddmemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceAddmemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceAddmemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
