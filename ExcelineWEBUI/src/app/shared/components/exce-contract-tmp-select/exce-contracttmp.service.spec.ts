import { TestBed, inject } from '@angular/core/testing';

import { ExceContracttmpService } from './exce-contracttmp.service';

describe('ExceContracttmpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceContracttmpService]
    });
  });

  it('should be created', inject([ExceContracttmpService], (service: ExceContracttmpService) => {
    expect(service).toBeTruthy();
  }));
});
