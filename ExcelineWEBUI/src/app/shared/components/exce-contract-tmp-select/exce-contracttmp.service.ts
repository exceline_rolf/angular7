import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../services/common-http.service';
import { ConfigService } from '@ngx-config/core';
import { Observable } from 'rxjs';

@Injectable()
export class ExceContracttmpService {
  private _memberApiURL: string;
  constructor(private commonHttpService: CommonHttpService, private config: ConfigService) {
    this._memberApiURL = this.config.getSettings('EXCE_API.MEMBER');
  }

  getContractTemplates(branchId: number, contractType: number): Observable<any> {
    const url = this._memberApiURL + '/Contract/GetContractTemplates?branchId=' + branchId + '&contractType=' + contractType;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }
}
