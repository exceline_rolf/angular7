import { Component, OnInit, Output , EventEmitter, OnDestroy} from '@angular/core';
import { ExceContracttmpService } from './exce-contracttmp.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { ExceLoginService } from '../../../modules/login/exce-login/exce-login.service';
import { DataTableResource } from 'app/shared/components/us-data-table/tools/data-table-resource';
import { PreloaderService } from '../../services/preloader.service';
import { DataTableFilterPipe } from 'app/shared/components/us-data-table/tools/data-table-filter-pipe';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'exce-contract-tmp-select',
  templateUrl: './exce-contract-tmp-select.component.html',
  styleUrls: ['./exce-contract-tmp-select.component.scss']
})
export class ExceContractTmpSelectComponent implements OnInit, OnDestroy {
  private _loggedGym: number;
  private _contracttype = 0;
  private destroy$ = new Subject<void>();

  public _contractTemplates: any;
  public locale: string;

  @Output() selectTempateEvent: EventEmitter<any>  = new EventEmitter();

  names = {
    IntTemplateNumber: 'Nummer',
    PackageName: 'Navn',
    ContractTypeValue: 'Kategori',
    InStock: 'På lager',
    NumOfVisits: 'Klipp',
    NumOfInstallments: 'Order',
    NoOfMonths: 'Måneder',
    StartUpItemPrice: 'Varepris',
    ServicePrice: 'Månedspris',
    PackagePrice: 'Totalpris',
    EndDate: 'Salg til'
  }

  colNamesNo = {
    IntTemplateNumber: 'Nummer',
    PackageName: 'Navn',
    ContractTypeValue: 'Kategori',
    InStock: 'På lager',
    NumOfVisits: 'Klipp',
    NumOfInstallments: 'Order',
    NoOfMonths: 'Måneder',
    StartUpItemPrice: 'Varepris',
    ServicePrice: 'Månedspris',
    PackagePrice: 'Totalpris',
    EndDate: 'Salg til'
  }

  colNamesEn = {
    IntTemplateNumber: 'Number',
    PackageName: 'Name',
    ContractTypeValue: 'Category',
    InStock: 'Stock status',
    NumOfVisits: 'Punches',
    NumOfInstallments: 'Orders',
    NoOfMonths: 'Months',
    StartUpItemPrice: 'Item price',
    ServicePrice: 'Monthly price',
    PackagePrice: 'Total price',
    EndDate: 'Sale end'
  }

  templateDefs: any;
  gridApi: any;

  constructor(
    private templateService: ExceContracttmpService,
    private loginService: ExceLoginService,
    private toolbarService: ExceToolbarService) {
    if (this.toolbarService.getSelectedLanguage().id === 'no') {
      this.locale = 'nb-NO';
    } else {
      this.locale = 'en-US';
    }
  }

  ngOnInit() {
    this._loggedGym = this.loginService.SelectedBranch.BranchId;
    PreloaderService.showPreLoader();
    this.templateService.getContractTemplates(this._loggedGym, this._contracttype).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        PreloaderService.hidePreLoader();
        if (result) {
          // this._originalTemplateList =  result.Data;
          this._contractTemplates = result.Data;
          // this._ItemCount = Number(this._contractTemplates.length);
          // this._itemResource = new DataTableResource(result.Data);
          // this._itemResource.count().then(count => this._ItemCount = count);
        }
      }
    );

    // **Language selecton and subscription - dateformat and amountconverter uses this **
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
    // **END**

    this.templateDefs = [
      {headerName: this.names.IntTemplateNumber, field: 'TemplateNumber', width: 70, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.PackageName, field: 'PackageName', width: 70, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.ContractTypeValue, field: 'ContractType', suppressFilter: true, width: 70, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        return params.data.ContractTypeValue.Name;
      }},
      {headerName: this.names.EndDate, field: 'EndDate', width: 70, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, suppressFilter: true,
      cellRenderer: function(params) {
        if (params.value) {
          const d = params.value.split('T');
          const dDate = d[0].split('-');
          return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
        } else {
          return
        }
      }},
      {headerName: this.names.NumOfVisits, field: 'NumOfVisits', width: 70, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, suppressFilter: true},
      {headerName: this.names.NumOfInstallments, field: 'NumOfInstallments', width: 70, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, suppressFilter: true},
      {headerName: this.names.NoOfMonths, field: 'NoOfMonths', width: 70, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, suppressFilter: true},
      {headerName: this.names.StartUpItemPrice, field: 'StartUpItemPrice', width: 70, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, suppressFilter: true},
      {headerName: this.names.ServicePrice, field: 'ServicePrice', width: 70, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, suppressFilter: true},
      {headerName: this.names.PackagePrice, field: 'PackagePrice', width: 70, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, suppressFilter: true},

    ]
  }

  onGridReady(params) {
    this.gridApi = params.api;
    // this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    this.gridApi.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  selectTemplate(selectedTemplate: any) {
    this.selectTempateEvent.emit(selectedTemplate.data);
  }

  // filterTemplates(numberValue: any, nameValue: any, type: any) {
  //   if (type === 'NUMBER') {
  //     if (numberValue) {
  //       this._contractTemplates = this._filterPipe.transform('TemplateNumber', 'TEXT', this._originalTemplateList, numberValue);
  //     }
  //     if (nameValue) {
  //       this._contractTemplates = this._filterPipe.transform('PackageName', 'TEXT', this._contractTemplates, nameValue);
  //     }
  //   } else if (type === 'NAME') {
  //     if (nameValue) {
  //       this._contractTemplates = this._filterPipe.transform('PackageName', 'TEXT', this._originalTemplateList, nameValue);
  //     }
  //     if (numberValue) {
  //       this._contractTemplates = this._filterPipe.transform('TemplateNumber', 'TEXT', this._contractTemplates, numberValue);
  //     }
  //   }

  //   if (!numberValue && !nameValue) {
  //     this._contractTemplates = this._originalTemplateList;
  //   }
  // }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
