import { Component, OnInit, Input } from '@angular/core';
import { trigger, transition, animate, style, state} from '@angular/animations';

@Component({
  selector: 'exce-fade-in-n-out',
  templateUrl: './exce-fade-in-n-out.component.html',
  styleUrls: ['./exce-fade-in-n-out.component.scss'],
  animations: [
    trigger('fade', [
      state('visible', style({
        opacity: 1
      })),
      state('hide', style({
        opacity: 0,
        backgroundColor: 'transparent'
      })),
      transition('hide => visible', [
        animate('500ms ease-in')
      ]),
      transition('visible => hide', [
        animate('1000ms ease-in')
      ])
    ])
  ]
})
export class ExceFadeInNOutComponent implements OnInit {
  @Input() type: string;
  @Input() isShow: boolean;
  @Input() information = '';

  constructor() { }

  ngOnInit() {

  }

}
