import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceFadeInNOutComponent } from './exce-fade-in-n-out.component';

describe('ExceFadeInNOutComponent', () => {
  let component: ExceFadeInNOutComponent;
  let fixture: ComponentFixture<ExceFadeInNOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceFadeInNOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceFadeInNOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
