import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ExcePostalcodeService } from 'app/shared/components/exce-postalcode/exce-postalcode.service';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';

@Component({
  selector: 'exce-postalcode',
  templateUrl: './exce-postalcode.component.html',
  styleUrls: ['./exce-postalcode.component.scss']
})
export class ExcePostalcodeComponent implements OnInit {
  config: any;
  public postalDataForm: FormGroup;
  myFormSubmited = false;
  isPostalCodeBlur = false;
  @Output() addPostalCodeEvent = new EventEmitter();
  @Output() cancelPostalAreaModal = new EventEmitter();
  formErrors = {
    'postalCode': '',
    'postalName': ''
  };

  validationMessages = {
    'postalCode': {
      'required': 'postalCode is required.',
      'minlength': 'postalCode must be at least 3 characters long.',
      'requiredTrue': 'postalCode exit'
    },
    'postalName': {
      'required': 'postalName is required.',
    }
  };

  constructor(
    private fb: FormBuilder,
    private postalService: ExcePostalcodeService
  ) {
  }

  ngOnInit() {
    this.postalDataForm = this.fb.group({
      'postalCode': [null, [Validators.required, Validators.minLength(3)]],
      'postalName': [null, Validators.required],
      'population': [null],
      'houseHold': [null]
    });
    this.postalDataForm.statusChanges.subscribe(_ => UsErrorService.onValueChanged(this.postalDataForm, this.formErrors, this.validationMessages));
  }

  // postalCodeBlur(val: any) {
  //   console.log(val);
  //   this.isPostalCodeBlur = true;
  //   if (this.postalDataForm.controls['postalCode'].valid) { // email is exits check after valid email
  //     const isPostalCodeExits = this.postalService.checkPostalCodeExits(val);
  //     if (isPostalCodeExits) {
  //       this.postalDataForm.controls['postalCode'].setErrors({
  //         'postalCodeTaken': true
  //       });
  //     } else {
  //       //  this.memberForm.controls['Email'].setErrors(
  //       //   null
  //       // );
  //     }
  //   }
  // }

  postalCodeFocus() {
    this.isPostalCodeBlur = false
  }

  savePostalData() {
    this.myFormSubmited = true;
    if (this.postalDataForm.valid) {
      const postalData = this.postalDataForm.value;
      console.log(postalData);
      this.postalService.savePostalArea(postalData).subscribe(result => {
        if (result) {
          console.log(result.Data)
          if (result.Data > 0) {
            console.log('save postal code');
            console.log(postalData);
            this.addPostalCodeEvent.emit(postalData);

            // this.postalService.sentPostalData(postalData);

            // this.postalDataForm.patchValue({ PostPlace: postalData.postalCode });
            // this.postalDataForm.patchValue(postalData.postalName);
          } else {
            console.log('This postal code is exists..!');
            this.postalDataForm.controls['postalCode'].setErrors({ 'postalCodeTaken': true });
          }
        }
      });
      this.closenewPostalAreaView();
    } else {
      UsErrorService.validateAllFormFields(this.postalDataForm, this.formErrors, this.validationMessages);
    }
  }

  closenewPostalAreaView() {
    this.cancelPostalAreaModal.emit();
  }

}
