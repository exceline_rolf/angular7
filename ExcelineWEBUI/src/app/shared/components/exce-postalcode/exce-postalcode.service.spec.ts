import { TestBed, inject } from '@angular/core/testing';

import { ExcePostalcodeService } from './exce-postalcode.service';

describe('ExcePostalcodeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExcePostalcodeService]
    });
  });

  it('should be created', inject([ExcePostalcodeService], (service: ExcePostalcodeService) => {
    expect(service).toBeTruthy();
  }));
});
