import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsIconButtonComponent } from './us-icon-button.component';

describe('UsIconButtonComponent', () => {
  let component: UsIconButtonComponent;
  let fixture: ComponentFixture<UsIconButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsIconButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsIconButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
