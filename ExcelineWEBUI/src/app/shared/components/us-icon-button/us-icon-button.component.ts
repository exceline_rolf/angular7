import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'us-icon-button',
  templateUrl: './us-icon-button.component.html',
  styleUrls: ['./us-icon-button.component.scss']
})
export class UsIconButtonComponent implements OnInit {
  @Input() iconClass: string;
  @Input() customBtnColor= '';
  @Input() labelText: string;
  @Input() iconBgClass: string;
  @Input() isDisabled: boolean;
  @Input() toolTip = '';
  @Input() hidden = false;

  private divClass;
  constructor() {
  }

  ngOnInit() {

  }

}
