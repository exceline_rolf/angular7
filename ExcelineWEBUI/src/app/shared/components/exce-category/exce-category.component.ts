import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { UsbModal } from '../../../shared/components/us-modal/us-modal';
import { ExceCategoryService } from './exce-category.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';

@Component({
  selector: 'exce-category',
  templateUrl: './exce-category.component.html',
  styleUrls: ['./exce-category.component.scss']
})
export class ExceCategoryComponent implements OnInit {

  @Input() CategoryTypeCode: string;
  @Input() branchId: number;
  @Input() isShowColorPicker = true;
  @Output() closed = new EventEmitter();
  @Output() saved = new EventEmitter();

  categoryTypes: any;
  public addNewCategoryForm: FormGroup;
  private selectedCategoryType: any;
  currentColor: any;

  formErrors = {
    'CategoryName': ''
  };

  validationMessages = {
    'CategoryName': {
      'required': 'COMMON.AddCategoryValidateName'
    }
  };

  constructor(
    private fb: FormBuilder,
    private exceCategoryService: ExceCategoryService
  ) {
    this.addNewCategoryForm = fb.group({
      TypeId: [this.selectedCategoryType],
      CategoryName: [null],
      Description: [null],
      Color: [null]
    });
  }

  ngOnInit() {
    this.exceCategoryService.getCategoryTypes(this.CategoryTypeCode)
      .subscribe(
        result => {
          if (result) {
            this.categoryTypes = result.Data;
            this.selectedCategoryType = this.categoryTypes[0];
          }
        },
        error => {
          console.log(error);
        });

    this.addNewCategoryForm.statusChanges.subscribe(_ => {
      UsErrorService.onValueChanged(this.addNewCategoryForm, this.formErrors, this.validationMessages)
    });
  }

  // To add new category
  addNewCategory(value) {

    if (value.CategoryName === null) {
      this.addNewCategoryForm.controls['CategoryName'].setErrors({ 'required': true });
    }

    if (this.addNewCategoryForm.valid) {
      value.ActiveStatus = true;
      value.BranchId = this.branchId;
      value.TypeId = this.selectedCategoryType.Id;
      value.Code = value.CategoryName + Math.floor(Math.random() * 10000000000);
      value.Color = this.currentColor;
      value.Name = value.CategoryName;

      this.exceCategoryService.saveCategory(value).subscribe(result => {
        value.Id = result.Data;
        this.saved.emit(value);
      },
        error => {
          console.log(error);
        });
      this.resetForm();
    } else {
      UsErrorService.validateAllFormFields(this.addNewCategoryForm, this.formErrors, this.validationMessages);
    }
  }

  // to reset the form fields
  resetForm() {
    this.addNewCategoryForm.reset();
    this.currentColor = '#fff';
  }

  // to get the current color
  colorChange(color) {
    this.currentColor = color;
  }

  closeView() {
    this.closed.emit();
  }

}
