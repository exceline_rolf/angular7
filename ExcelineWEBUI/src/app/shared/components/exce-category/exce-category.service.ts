import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../shared/services/common-http.service';
import { ConfigService } from '@ngx-config/core';
import { Observable } from 'rxjs';
import { ExceLoginService } from 'app/modules/login/exce-login/exce-login.service';

@Injectable()
export class ExceCategoryService {
  private userApiURL: string;
  private branchId: number;

  constructor(
    private commonHttpService: CommonHttpService,
    private config: ConfigService,
    private exceLoginService: ExceLoginService
  ) {
    this.userApiURL = this.config.getSettings('EXCE_API.MEMBER');
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }

  getCategoryTypes(name: string): Observable<any> {
    const url = this.userApiURL + '/GetCategoryTypes?name=' + name + '&&branchId=' + this.branchId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  saveCategory(newCategory: any): Observable<any> {
    const url = this.userApiURL + '/SaveCategory'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: newCategory
    });
  }

}
