import { TestBed, inject } from '@angular/core/testing';

import { ExceCategoryService } from './exce-category.service';

describe('ExceCategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceCategoryService]
    });
  });

  it('should be created', inject([ExceCategoryService], (service: ExceCategoryService) => {
    expect(service).toBeTruthy();
  }));
});
