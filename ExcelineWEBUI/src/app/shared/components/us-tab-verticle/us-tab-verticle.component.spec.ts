import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsTabVerticleComponent } from './us-tab-verticle.component';

describe('UsTabVerticleComponent', () => {
  let component: UsTabVerticleComponent;
  let fixture: ComponentFixture<UsTabVerticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsTabVerticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsTabVerticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
