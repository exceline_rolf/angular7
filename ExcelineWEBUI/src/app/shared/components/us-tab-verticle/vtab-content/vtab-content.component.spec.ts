import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VtabContentComponent } from './vtab-content.component';

describe('VtabContentComponent', () => {
  let component: VtabContentComponent;
  let fixture: ComponentFixture<VtabContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VtabContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VtabContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
