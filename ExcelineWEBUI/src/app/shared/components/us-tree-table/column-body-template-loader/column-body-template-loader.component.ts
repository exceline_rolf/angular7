import { Component, OnInit, OnChanges, OnDestroy, Input, EmbeddedViewRef, ViewContainerRef, SimpleChanges } from '@angular/core';

@Component({
  selector: 'us-tree-column-body-loader',
  template: ``
})
export class ColumnBodyTemplateLoaderComponent implements OnInit, OnChanges, OnDestroy {

  @Input() column: any;

  @Input() rowData: any;

  @Input() rowIndex: number;

  view: EmbeddedViewRef<any>;

  constructor(public viewContainer: ViewContainerRef) { }

  ngOnInit() {
    this.view = this.viewContainer.createEmbeddedView(this.column.bodyTemplate, {
      '\$implicit': this.column,
      'rowData': this.rowData,
      'rowIndex': this.rowIndex
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!this.view) {
      return;
    }

    if ('rowIndex' in changes) {
      this.view.context.rowIndex = changes['rowIndex'].currentValue;
    }
  }

  ngOnDestroy() {
    this.view.destroy();
  }
}
