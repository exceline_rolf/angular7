import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnBodyTemplateLoaderComponent } from './column-body-template-loader.component';

describe('ColumnBodyTemplateLoaderComponent', () => {
  let component: ColumnBodyTemplateLoaderComponent;
  let fixture: ComponentFixture<ColumnBodyTemplateLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColumnBodyTemplateLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnBodyTemplateLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
