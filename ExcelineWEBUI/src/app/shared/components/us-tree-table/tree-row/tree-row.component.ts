import { Component, OnInit, Input, Inject, forwardRef } from '@angular/core';
import { UsTreeTableComponent } from '../us-tree-table.component';
import { TreeNode } from '../tree-node';


@Component({
  selector: '[usTreeRow]',
  templateUrl: './tree-row.component.html',
  styleUrls: ['./tree-row.component.scss']
})
export class TreeRowComponent implements OnInit {
  @Input() node: TreeNode;

  @Input() parentNode: TreeNode;

  @Input() level = 0;

  @Input() labelExpand = 'Expand';

  @Input() labelCollapse = 'Collapse';

  constructor( @Inject(forwardRef(() => UsTreeTableComponent)) public treeTable: UsTreeTableComponent) { }

  ngOnInit() {
    this.node.parent = this.parentNode;
  }

  toggle(event: Event) {
    if (this.node.expanded) {
      this.treeTable.onNodeCollapse.emit({ originalEvent: event, node: this.node });
    } else {
      this.treeTable.onNodeExpand.emit({ originalEvent: event, node: this.node });
    }
    this.node.expanded = !this.node.expanded;

    event.preventDefault();
  }

  isLeaf() {
    return this.node.leaf === false ? false : !(this.node.children && this.node.children.length);
  }

  isSelected() {
    return this.treeTable.isSelected(this.node);
  }

  onRowClick(event: MouseEvent) {
    this.treeTable.onRowClick(event, this.node);
  }

  onRowRightClick(event: MouseEvent) {
    this.treeTable.onRowRightClick(event, this.node);
  }

  rowDblClick(event: MouseEvent) {
    this.treeTable.onRowDblclick.emit({ originalEvent: event, node: this.node });
  }

  onRowTouchEnd() {
    this.treeTable.onRowTouchEnd();
  }

  resolveFieldData(data: any, field: string): any {
    if (data && field) {
      if (field.indexOf('.') === -1) {
        return data[field];
      } else {
        const fields = field.split('.');
        let value = data;
        for (let i = 0, len = fields.length; i < len; ++i) {
          value = value[fields[i]];
        }
        return value;
      }
    } else {
      return null;
    }
  }

}
