import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsTreeTableComponent } from './us-tree-table.component';

describe('UsTreeTableComponent', () => {
  let component: UsTreeTableComponent;
  let fixture: ComponentFixture<UsTreeTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsTreeTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsTreeTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
