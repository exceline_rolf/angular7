import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnHeaderTemplateLoaderComponent } from './column-header-template-loader.component';

describe('ColumnHeaderTemplateLoaderComponent', () => {
  let component: ColumnHeaderTemplateLoaderComponent;
  let fixture: ComponentFixture<ColumnHeaderTemplateLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColumnHeaderTemplateLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnHeaderTemplateLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
