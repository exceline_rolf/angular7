import { Pipe, PipeTransform } from '@angular/core';
import { TranslateDataService } from './translate-data.service';
import { MetaDataService } from './meta-data.service';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  constructor(private _translate: TranslateDataService, private _metaDataService: MetaDataService) {}

  transform(dataArray: any[], filterBy ?: Array<{Header: string, value: any, dataType?: string}>): any {
    let arrayToReturn = new Array<any>();
    let currencyHeaders = this._metaDataService.getCurrencyHeaders();
    let dateHeaders = this._metaDataService.getDateHeader();
    let objectProperties = Object.getOwnPropertyNames(dataArray);

    if (dataArray === undefined || (filterBy === undefined || filterBy === null) ) {
      return dataArray;
    }else if (dataArray !== undefined && (filterBy !== undefined && filterBy !== null)) {

      // set the data type of the filerBy array
      for (var index2 = 0; index2 < filterBy.length; index2++) { // loop for filerBy array
        for (var index3 = 0; index3 < currencyHeaders.length; index3++) { // loop for currencyHeaders
          if(filterBy[index2].Header === currencyHeaders[index3]){ // identify the currency data type headers
            filterBy[index2].dataType = 'currency';
          }
        }

        for (var index4 = 0; index4 < dateHeaders.length; index4++) {
          if(filterBy[index2].Header === dateHeaders[index4]) { // identify the date data type headers
            filterBy[index2].dataType = 'date';
          }
        }
      }

      // // fill the array to return depending on the filterBy array data
      // for (var index5 = 0; index5 < dataArray.length; index5++) { // iteration for the dataArray
      //   for (var index6 = 0; index6 < filterBy.length; index6++) { // iteration for the filerBy Array
      //     if(filterBy[index6].dataType !== undefined && filterBy[index6].dataType === 'currency') {
      //       // let tempConverted =  this._translate.translateCurrency(dataArray[index5][filterBy[index6].Header]);
      //       let tempAvailability = dataArray[index5][filterBy[index6].Header].toString().includes(filterBy[index6].value.toString().toUpperCase());
      //       if( tempAvailability ) {
      //         arrayToReturn.push(dataArray[index5]);
      //       }
      //     }
      //   }
      // }

      // let tempAvailability;
      // for (var index5 = 0; index5 < dataArray.length; index5++) {
      //   for (var index6 = 0; index6 < filterBy.length; index6++) {
      //     debugger;
      //     if(filterBy[index6].value !== '' && filterBy[index6].value !== undefined && filterBy[index6].value !== null ) {
      //       if(dataArray[index5][filterBy[index6].Header].toString().toUpperCase().includes(filterBy[index6].value.toString().toUpperCase())){
      //         tempAvailability = true;
      //       }else{
      //         tempAvailability = false;
      //         break;
      //       }
      //     }else{
      //       arrayToReturn = dataArray;
      //     return;
      //     }
      //   }
      //   if(tempAvailability){
      //     arrayToReturn.push(dataArray[index5])
      //   }
      // }

      // checking if a null value is entered
      let count = 0 ;
      for (var index5 = 0; index5 < filterBy.length; index5++) {
        if(filterBy[index5].value === null || filterBy[index5].value === undefined || filterBy[index5].value === '' || filterBy[index5].value === ' ' ){
          count++;
        }
      }
      if(count === filterBy.length) {
        arrayToReturn = dataArray;
        return arrayToReturn;
      }

      // to include the objects that contain the input things
      let tempAvailability = false;
      let available = false;
      for (var index6 = 0; index6 < dataArray.length; index6++) {
        for (var index7 = 0; index7 < filterBy.length; index7++) {
          if(filterBy[index7].dataType === 'date') {  // if filtering by the date property
            let originalDataObject = this._metaDataService.getSingleObjFromID(dataArray[index6][this._metaDataService.getIdHeader()]);
            let originalDate = originalDataObject[filterBy[index7].Header];
            let translatedDate = this._translate.translateDateWithIntl(originalDate);
            if (filterBy[index7].value !== null && filterBy[index7].value !== undefined) {
              tempAvailability = translatedDate.toString().toUpperCase().includes(filterBy[index7].value.toString().toUpperCase());
              if (tempAvailability) {
                available = true;
              } else {
                available = false;
                break;
              }
            }

          }else { // if the data type of the filtering is not date
            if (filterBy[index7].value !== null && filterBy[index7].value !== undefined) {
              tempAvailability = dataArray[index6][filterBy[index7].Header].toString().toUpperCase().includes(filterBy[index7].value.toString().toUpperCase());
              if (tempAvailability) {
                available = true;
              } else {
                available = false;
                break;
              }
            }
          }
        }
        if(available){
          arrayToReturn.push(dataArray[index6]);
        }
      }

      return arrayToReturn;
    }

    return null;
  }

}
