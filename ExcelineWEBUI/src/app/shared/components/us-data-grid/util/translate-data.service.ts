import { Injectable } from '@angular/core';
import { MetaDataService } from './meta-data.service';
import { DatePickerObject } from './date-picker-data';

@Injectable()
export class TranslateDataService {

  constructor(private _metaDataService: MetaDataService) { }

// this method expects a dataObject Array with all the unnecessary fields removed and plane metaDataObjectArray
  public translateDataObject(dataObj: any[], metaDataObj: any[], language: string) {
    // let returnDataObj = dataObj;

    let returnDataObj = Object.assign({}, dataObj);
    returnDataObj = Object.keys(returnDataObj).map(value => returnDataObj[value]);
    for (var index = 0; index < returnDataObj.length; index++) {
      returnDataObj[index] = Object.assign({}, dataObj[index] );
    }

    if (returnDataObj !== undefined && returnDataObj.length !== 0 && returnDataObj !== undefined && returnDataObj.length !== 0) {

      // let currencyHeaders = new Array<string>();  // this will store all the headers that bear currency values
      // let dateHeaders = new Array<string>();  // this will store all the headers that will bear date values

      // for (var index = 0; index < metaDataObj.length; index++) {
      //   if (metaDataObj[index].dataType === 'currency') {
      //     currencyHeaders.push(metaDataObj[index].HeaderName);
      //   }
      //   if (metaDataObj[index].dataType === 'date') {
      //     dateHeaders.push(metaDataObj[index].HeaderName);
      //   }
      // }

      let currencyHeaders = this._metaDataService.getCurrencyHeaders();
      let dateHeaders = this._metaDataService.getDateHeader();

      for (var index2 = 0; index2 < returnDataObj.length; index2++) {
        for (var index3 = 0; index3 < currencyHeaders.length; index3++) {
          returnDataObj[index2][currencyHeaders[index3]] = this.translateCurrency(returnDataObj[index2][currencyHeaders[index3]], language);
        }
        for (var index4 = 0; index4 < dateHeaders.length; index4++) {
          returnDataObj[index2][dateHeaders[index4]] = this.translateDate(returnDataObj[index2][dateHeaders[index4]], language);
        }
      }
    }
    return returnDataObj;
  }

  public translateDate(date: any, languageCode?: string) {
    if(languageCode === undefined){
      languageCode = this._metaDataService.getCurrentLanguage();
    }
    // if(!(date instanceof Date)){
    //   date = new Date(date);
    // }
    // var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    const options = { year: 'numeric', month: 'numeric', day: 'numeric' };
    const returnDate = new Intl.DateTimeFormat(languageCode, options).format(date);
    // return returnDate;

    // due to usage of the new datepicker the date format need to be changed
    return new DatePickerObject(date.getFullYear(), date.getMonth() + 1, date.getDate());
  }

  public translateDateWithIntl(date:any,languageCode?:string):string{
    if(languageCode === undefined){
      languageCode = this._metaDataService.getCurrentLanguage();
    }
    // if(!(date instanceof Date)){
    //   date = new Date(date);
    // }
    // var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    const options = { year: 'numeric', month: '2-digit', day: '2-digit' };
    const returnDate = new Intl.DateTimeFormat(languageCode, options).format(date);
    return returnDate;
  }

  public translateCurrency(money: any, languageCode?: string){
    if(languageCode === undefined){
      languageCode = this._metaDataService.getCurrentLanguage();
    }
    return new Intl.NumberFormat(languageCode).format(money);
  }

}
