import { Component, OnInit, Input, Output, EventEmitter, ElementRef, Injectable, ViewChildren } from '@angular/core';
import { MetaDataService } from './util/meta-data.service';
import { TranslateDataService } from './util/translate-data.service';
import { I18n, DatePickerData } from './util/date-picker-data';
import { NgbDatepickerI18n, NgbDatepickerConfig, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap'; // used for date picker
import { NgbDateFrParserFormatterService } from './util/ngb-date-fr-parser-formatter.service';
import { FilterPipe } from './util/filter.pipe';
import { ValidatorService } from './util/validator.service';

@Component({
  selector: 'app-us-data-grid',
  templateUrl: './us-data-grid.component.html',
  styleUrls: ['./us-data-grid.component.scss'],
  providers: [
    I18n,
    { provide: NgbDatepickerI18n, useClass: DatePickerData }, // for the date picker
    { provide: NgbDateParserFormatter, useClass: NgbDateFrParserFormatterService } // for the date picker date formatting
  ]
})
export class USDataGridComponent implements OnInit {

  viewingTableData: any[];  // full data object
  filterBy: any[];  // headers for filtering
  filterByCopy: any[];  // used as id in filtering data collection array
  tableHeadersViewed: string[];  // table headers to view (these corrospond to the viewingTableData variable)
  currentLanguage: string; // selected language (from the parent component)
  // tableHeadersServiceObject:string[];  // commented as the original metadata was used in table data viewing as id
  filterObjectArray: Array<{ Header: string, value: any, dataType?: string }>;
  dataObjectCopy: any[];  // used for the filtering pipe as parameter
  sortingToggler: Array<{ Header: Headers, Toggler: number }>;
  dataInputToggler: Array<{ DataObj: Object, Header: string, Toggler: number }>;
  showDatePickerInput: boolean; // to toggle the date and normal date inputs

  @Input() dataObject: any[];
  @Input() tableHeaderMetaData: any[];
  @Input() set translatedHeaders(translatedHeadersrs: string[]) {
    this.tableHeadersViewed = Object.assign({}, translatedHeadersrs);
    this.tableHeadersViewed = Object.keys(this.tableHeadersViewed).map(value => this.tableHeadersViewed[value]);
  }
  @Output() onColoumSelect = new EventEmitter<any>();
  @Output() onHeaderSelect = new EventEmitter<any>();
  @Output() languageHeaders = new EventEmitter<string[]>();
  @Output() filterHeaders = new EventEmitter<string[]>();
  @Input() set translatedFilters(translatedFilters: string[]) {
    this.filterBy = translatedFilters;
  }
  @Input() set langSelected(langSelected: string) {
    this.currentLanguage = langSelected;
    this._i18n.language = this.currentLanguage; // for the date picker to get ready
    this._metaDataService.setCurrentLanguage(langSelected);
    if (!(this._metaDataService.getDataObj() === undefined)) {
      this.getDataTranslated();
    }
  }
  @Output() updatedObject = new EventEmitter<any>();
  @Input() addDeleteButton: boolean;
  @Input() pageLimit: number;
  @Input() columnType: string;

  @ViewChildren('viewDataValue') dataView: any; // to reffer the dateView DOM text reference variable

  constructor(
    private _metaDataService: MetaDataService,
    private _translateDataService: TranslateDataService,
    private _filter: FilterPipe,
    private _i18n: I18n,  // for the data picker
    private _eref: ElementRef, // for the date picker to close on click someware else
    private _validator: ValidatorService
  ) { }

  ngOnInit() {
    if (this.tableHeaderMetaData !== undefined || this.tableHeaderMetaData !== null) {
      // sort metadata array in order
      this.tableHeaderMetaData.sort((a, b) => a.orderNo > b.orderNo ? 1 : a.orderNo > b.orderNo ? -1 : 0);
      this.extractData();
    }

    if (this.dataObject !== undefined || this.dataObject !== null) {
      this.arrange_data();
    }

    // set arrenged this.dataObject to a new istance of object
    this._metaDataService.setDataObj(this.dataObject);

    // set tableHeaders to a new instance object
    this._metaDataService.setHeaders(this.tableHeadersViewed);

    // get the service instance of headers obeject to use as view ids that will not subject to any changes
    // this.tableHeadersServiceObject = this._metaDataService.getHeaders(); // commeted this as i replaced that with the original metadata object

    // send the headers to parent class
    this.languageHeaders.emit(this._metaDataService.getHeaders());
    // send filtering headers to parent class
    this.filterHeaders.emit(this.filterBy);


    this.storeHeaderDataDefinitions();

    // get the data of the table translated
    this.getDataTranslated();

    // initialize the array to hold the sorting data
    this.sortingToggler = new Array<{Header,Toggler}>();

    // to show the data input field
    this.showDatePickerInput = false;
  }



  filterInput(anything?:any){
    this.dataObject = this._filter.transform(this.dataObjectCopy,anything);
  }

  storeHeaderDataDefinitions() {
    if (this.tableHeaderMetaData !== undefined && this.tableHeaderMetaData.length !== 0) {
      let currencyHeaders = new Array<string>();  // this will store all the headers that bear currency values
      let dateHeaders = new Array<string>();  // this will store all the headers that will bear date values

      for (var index = 0; index < this.tableHeaderMetaData.length; index++) {
        if (this.tableHeaderMetaData[index].dataType === 'currency') {
          currencyHeaders.push(this.tableHeaderMetaData[index].HeaderName);
        }
        if (this.tableHeaderMetaData[index].dataType === 'date') {
          dateHeaders.push(this.tableHeaderMetaData[index].HeaderName);
        }
      }
      this._metaDataService.setDateHeader(dateHeaders);
      this._metaDataService.setCurrencyHeader(currencyHeaders);
    }
  }

  getDataTranslated() { // this method will translate all the data object values as defined by meta data
    // the dataObject below is used to view in the html page
    this.dataObject = this._translateDataService.translateDataObject(this._metaDataService.getDataObj(),
                                                                    this.tableHeaderMetaData, this.currentLanguage);

    // happned to keep a copy of dataobject for filtering purposes
    this.dataObjectCopy = this._translateDataService.translateDataObject(this._metaDataService.getDataObj(),
                                                                        this.tableHeaderMetaData, this.currentLanguage);
  }

  extractData() {
    this.tableHeadersViewed = new Array<string>();
    this.filterBy = new Array<string>();
    this.filterByCopy = new Array<string>();

    // initialize the array to collect the filtering data
    this.filterObjectArray = new Array<{Header, value, dataType?}>();
    for (var index = 0; index < this.tableHeaderMetaData.length; index++) {
      // inserting the needed table headers to appear in the table
      this.tableHeadersViewed.push(this.tableHeaderMetaData[index].HeaderName);
      if (this.tableHeaderMetaData[index].showInFilter) {
        // inserting the needed headers to appear in the filterings
        this.filterBy.push(this.tableHeaderMetaData[index].HeaderName);
        this.filterObjectArray.push({Header:this.tableHeaderMetaData[index].HeaderName,value:null});
      }
    }
  }

  arrange_data() {
    // get all available headers
    var allHeaders = Object.getOwnPropertyNames(this.dataObject[0]);
    var keepIt = false;

    // remove the headers that do not need to appear
    for (var index1 = 0; index1 < allHeaders.length; index1++) {
      keepIt=false;
      for (var index2 = 0; index2 < this.tableHeadersViewed.length; index2++) {
        if (this.tableHeadersViewed[index2] === allHeaders[index1]) {
          keepIt = true;
          continue;
        }
      }
      if (!keepIt) {
        this.dataObject.forEach(dataObj => {
          delete dataObj[allHeaders[index1]];
        });
      }
    }

  }

  sort(header: Headers) {
    var availability = false;
    if (this.sortingToggler.length <= 0) {  // this condition is expected to fire in the first ever click on any sorting buttons
      this.sortingToggler.push({Header: header, Toggler: 1}); // toggler will be 1 to get sorted in ascending order
      this.sortInAccending(header);
    }else if (this.sortingToggler.length > 0) { // this is expected after having clicked more than one
      for (var index = 0; index < this.sortingToggler.length; index++) {

        if (this.sortingToggler[index].Header === header) { // if the header is clicked at least once to sort things
            availability = true;
          if(this.sortingToggler[index].Toggler === 1 ) {
            this.sortingToggler[index].Toggler = -1;  // set the Toggler to -1 to go to else in next click
            this.sortInDescending(header);
          }else {
            this.sortingToggler[index].Toggler = 1 ;
            this.sortInAccending(header);
          }
          break;
        }else{
          availability = false;
        }
      }
      if(!availability){  // if the header is clicked not as the first soring click
        this.sortingToggler.push({Header: header, Toggler: 1});
        this.sortInAccending(header);
      }
    }

  }

  sortInAccending(header: Headers) {
    if (header.dataType === 'number' || header.dataType === 'currency' || header.dataType === 'date') {
      this.dataObject.sort(( a, b ) =>
        // tslint:disable-next-line:max-line-length
        this._metaDataService.getSingleObjFromID(a[this._metaDataService.getIdHeader()])[header.HeaderName] >  this._metaDataService.getSingleObjFromID(b[this._metaDataService.getIdHeader()])[header.HeaderName] ? 1 : 
       // tslint:disable-next-line:max-line-length
       this._metaDataService.getSingleObjFromID(a[this._metaDataService.getIdHeader()])[header.HeaderName] <  this._metaDataService.getSingleObjFromID(b[this._metaDataService.getIdHeader()])[header.HeaderName] ? -1 : 0);  
    }else {
    this.dataObject.sort(( a, b ) => a[header.HeaderName].toString() > b[header.HeaderName].toString() ? 1 : a[header.HeaderName].toString() < b[header.HeaderName].toString() ? -1 : 0);
    }
  }

  sortInDescending(header: Headers) {
    if(header.dataType === 'number' || header.dataType === 'currency' || header.dataType === 'date'){
      this.dataObject.sort(( a, b ) => 
        // tslint:disable-next-line:max-line-length
        this._metaDataService.getSingleObjFromID(a[this._metaDataService.getIdHeader()])[header.HeaderName] >  this._metaDataService.getSingleObjFromID(b[this._metaDataService.getIdHeader()])[header.HeaderName] ? -1 : 
        // tslint:disable-next-line:max-line-length
        this._metaDataService.getSingleObjFromID(a[this._metaDataService.getIdHeader()])[header.HeaderName] <  this._metaDataService.getSingleObjFromID(b[this._metaDataService.getIdHeader()])[header.HeaderName] ? 1 : 0);
    }else {
      this.dataObject.sort(( a, b ) => a[header.HeaderName].toString() > b[header.HeaderName].toString() ? -1 : a[header.HeaderName].toString() < b[header.HeaderName].toString() ? 1 : 0);
    }
  }

  sortFunction(header:Headers,a?:any){
    let valueA:any;
    valueA = this._metaDataService.getSingleObjFromID(a[this._metaDataService.getIdHeader()])[header.HeaderName];
    return valueA;
  }

  showHideTextSup(dataObj, header, iObj) { // this function is used to enable the text input field show
    var availabile = false;
    if (this.dataObject[iObj].Toggler === undefined) {
      // adding a new proprety to this.dataObject to know what property is updated of what object
      this.dataObject[iObj].Toggler = new Array<{Header: string; value: any; isInputDone: boolean}>();
      this.dataObject[iObj].Toggler.push({Header: header, value: null, isInputDone: false});

    }else if (this.dataObject[iObj].Toggler.length >= 0) {
      for (var index = 0; index < this.dataObject[iObj].Toggler.length; index++) {
        if (this.dataObject[iObj].Toggler[index].Header === header) {
          availabile = true;
          break;
        }else {
          availabile = false;
        }
      }
      if (!availabile) {
        this.dataObject[iObj].Toggler.push({Header: header, value: null, isInputDone: false});
      }
    }
    return false;
  }

// the function below is to update the data input from the user
  updateDataObj(header: Headers, index: number, dataObject: any, value: any, event: any) {

    if (this._validator.Validator(header, value, dataObject) !== null) {
      this.dataObject[index][header.HeaderName] = this._validator.Validator(header, value, dataObject).dataToDisplay;
      let testValueOriginalFormat = this._metaDataService.updateDataObject(dataObject, header, this._validator.Validator(header, value, dataObject).dataInOriginalFormat);
      this.updatedObject.emit(testValueOriginalFormat);
    } else{
      event.target.value = this.dataObject[index][header.HeaderName];
      // this.dataView.nativeElement.value = this.dataObject[index][header.HeaderName];
    }
  }


  test(any?: any, anyDP?: any): string {
    if (any) {
    }
     if (anyDP) {
    }else {
    }
    return 'testing....';
  }

  getView(data: any): string {
    return 'working.......';
  }

}

export interface Headers {
  HeaderName: string;
  showInFilter: true;
  dataType: string;
  isEditable: boolean;
  canSort: boolean;
  orderNo: number;
  inputType: string;
}

class DatePickerToggler {
  private readonly HeaderName: string;
  private readonly IsDatePickerOpen: boolean;

  constructor(headerName: string, datePickerToken: boolean) {
    this.HeaderName = headerName;
    this.IsDatePickerOpen = datePickerToken;
  }

  public get headerName(): string {
    return this.HeaderName;
  }

  public get isDatePickerToken(): boolean {
    return this.IsDatePickerOpen
  }
}

export class DatePickerObject {
  public readonly year: number;
  public readonly month: number;
  public readonly day: number;
  constructor(yearPara,monthPara,dayPara){
    this.year = yearPara;
    this.month = monthPara;
    this.day = dayPara;
  }
}

