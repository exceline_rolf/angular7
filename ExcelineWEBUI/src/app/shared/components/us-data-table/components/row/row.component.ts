import {
    Component, Input, Inject, forwardRef, Output, EventEmitter, OnDestroy
} from '@angular/core';
import { UsDataTableComponent } from '../../us-data-table.component';

@Component({
    selector: '[dataTableRow]',
    templateUrl: './row.component.html',
    styleUrls: ['./row.component.scss']
})
export class DataTableRow implements OnDestroy {
    _this = this; // FIXME is there no template keyword for this in angular 2?
    @Input() item: any;
    @Input() index: number;
    expanded: boolean;
    // row selection:
    _selected: boolean;

    @Output() selectedChange = new EventEmitter();
    constructor(@Inject(forwardRef(() => UsDataTableComponent)) public dataTable: UsDataTableComponent) { }

    get selected() {
        return this._selected;
    }

    set selected(selected) {
        this._selected = selected;
        this.selectedChange.emit(selected);
    }

    get displayIndex() {
        if (this.dataTable.pagination) {
            return this.dataTable.displayParams.offset + this.index + 1;
        } else {
            return this.index + 1;
        }
    }

    getTooltip() {
        if (this.dataTable.rowTooltip) {
            return this.dataTable.rowTooltip(this.item, this, this.index);
        }
        return '';
    }

    ngOnDestroy() {
        this.selected = false;
    }

}
