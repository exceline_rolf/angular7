import { TestBed, inject } from '@angular/core/testing';

import { ExceBreadcrumbService } from './exce-breadcrumb.service';

describe('ExceBreadcrumbService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceBreadcrumbService]
    });
  });

  it('should be created', inject([ExceBreadcrumbService], (service: ExceBreadcrumbService) => {
    expect(service).toBeTruthy();
  }));
});
