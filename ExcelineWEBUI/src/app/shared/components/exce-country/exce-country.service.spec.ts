import { TestBed, inject } from '@angular/core/testing';

import { ExceCountryService } from './exce-country.service';

describe('ExceCountryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceCountryService]
    });
  });

  it('should be created', inject([ExceCountryService], (service: ExceCountryService) => {
    expect(service).toBeTruthy();
  }));
});
