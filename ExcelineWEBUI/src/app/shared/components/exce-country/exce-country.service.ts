import { Injectable } from '@angular/core';
import { CommonHttpService } from 'app/shared/services/common-http.service';
import { ConfigService } from '@ngx-config/core';
import { Observable } from 'rxjs';

@Injectable()
export class ExceCountryService {
  private memberApiURL: string;
  constructor(private commonHttpService: CommonHttpService, private config: ConfigService) {
    this.memberApiURL = this.config.getSettings('EXCE_API.MEMBER');
  }

  addCountryDetails(body: any): Observable<any> {
    const url = this.memberApiURL + '/AddCountryDetails';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

}
