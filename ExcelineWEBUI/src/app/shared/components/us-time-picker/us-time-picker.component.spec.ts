import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsTimePickerComponent } from './us-time-picker.component';

describe('UsTimePickerComponent', () => {
  let component: UsTimePickerComponent;
  let fixture: ComponentFixture<UsTimePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsTimePickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsTimePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
