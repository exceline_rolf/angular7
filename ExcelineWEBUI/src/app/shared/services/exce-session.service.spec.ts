import { TestBed, inject } from '@angular/core/testing';

import { ExceSessionService } from './exce-session.service';

describe('ExceSessionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceSessionService]
    });
  });

  it('should be created', inject([ExceSessionService], (service: ExceSessionService) => {
    expect(service).toBeTruthy();
  }));
});
