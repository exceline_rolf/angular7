import { Injectable } from '@angular/core';

@Injectable()
export class PreloaderService {

  static showPreLoader() {
    document.getElementById('preloader').style.display = 'block';
  }
  static hidePreLoader() {
    document.getElementById('preloader').style.display = 'none';
  }

}
