import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'amountConverter', pure: false
})
export class AmountConverterPipe implements PipeTransform {

  transform(value: number | string, locale?: string): string {
    let amount = value ? value.toString() : '0';

    if (locale === 'nb-NO') {
      amount = amount.replace(/\s/g, '');
      amount = amount.replace(/,/g, '');
    } else {
      amount = amount.replace(/\s/g, '');
      amount = amount.replace(/,/g, '.');
      // amount = amount.replace(/,/g, '.');
    }
    // amount = amount.replace(/\s/g, '');
    // amount = amount.replace(/,/g, '');
    // amount = amount.replace(/\./g, '');

    // amount = this.insertAt(amount, (amount.length - 2), '.')

    return new Intl.NumberFormat(locale, {
      minimumFractionDigits: 2
    }).format(Number(amount));
  }

  insertAt(src: string, index: number, str: string) {
    return (src.substr(0, index) + str + src.substr(index));
  }

}
