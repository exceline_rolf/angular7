import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
// import 'rxjs/add/operator/map'

@Injectable()
export class SharedService {
  private languageSelected: string;
  private _token = false;
  private userBranches: any;
  constructor() { }

  public getLanguageSelected(): string {
    return this.languageSelected;
  }

  public setLanguageSelected(lan: string) {
    this.languageSelected = lan;
  }

  get token() {
    return this._token;
  }

  set token(val) {
    this._token = val;
  }

  public setUserSelectedBranches(userBranches): void {
    this.userBranches = userBranches;
  }

  public getUserSelectedBranches(): any {
      return this.userBranches;
    }


}
