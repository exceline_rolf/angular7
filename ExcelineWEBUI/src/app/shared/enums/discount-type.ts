export enum DiscountType {
    GROUP = 0,
    ACTIVITY = 1,
    SHOP = 2,
    NONE = 3,
    OTHER = 4,
}
