export class PostalArea {
    public postalCode: string;
    public postalName: string;
    public population: number;
    public houseHold: number;
}
