import { TestBed, inject } from '@angular/core/testing';

import { ExceClassService } from './exce-class.service';

describe('ExceClassService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceClassService]
    });
  });

  it('should be created', inject([ExceClassService], (service: ExceClassService) => {
    expect(service).toBeTruthy();
  }));
});
