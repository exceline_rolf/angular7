import { Injectable } from '@angular/core';
import { ConfigService } from '@ngx-config/core';
import { Observable } from 'rxjs';

import { ExceLoginService } from 'app/modules/login/exce-login/exce-login.service';
import { CommonHttpService } from 'app/shared/services/common-http.service';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { Cookie } from 'ng2-cookies/src/services';

@Injectable()
export class ExceClassService {

  adminServiceUrl: any;
  classApiURL: any;

  branchId: number;
  culture: any;

  currentUser: any;

  set CurrentUser(user) {
    this.currentUser = user;
  }

  get CurrentUser() {
    if (this.config.getSettings('ENV') === 'DEV') {
      return JSON.parse(Cookie.get('currentUser'));
    } else {
      return this.currentUser;
    }
  }

  constructor(
    private commonHttpService: CommonHttpService,
    private config: ConfigService,
    private exceLoginService: ExceLoginService,
    private exceToolbarService: ExceToolbarService
  ) {
    this.classApiURL = this.config.getSettings('EXCE_API.CLASS');
    this.adminServiceUrl = this.config.getSettings('EXCE_API.ADMIN');
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
    this.culture = this.exceToolbarService.getSelectedLanguage().culture;
    this.exceToolbarService.langUpdated.subscribe(
      res => {
        this.culture = res.culture;
      }
    )
  }

  GetClassTypes(): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetClassTypesByBranch?branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SearchClass(params: any): Observable<any> {
    const url = this.classApiURL + 'SearchClass?category='
      + params.category + '&searchText=' + params.searchText;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SearchClassByStartDate(params: any): Observable<any> {
    const url = this.classApiURL + 'SearchClass?category='
      + params.category + '&searchText=' + params.searchText + '&startDate=' + params.startDate;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SearchClassByDateRange(params: any): Observable<any> {
    const url = this.classApiURL + 'SearchClass?category='
      + params.category + '&searchText=' + params.searchText
      + '&startDate=' + params.startDate
      + '&endDate=' + params.endDate;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetNextClassId(): Observable<any> {
    const url = this.classApiURL + 'GetNextClassId';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SaveClass(body: any): Observable<any> {
    body.BranchId = this.branchId;
    body.Culture = this.culture;
    body.ExcelineClass.BranchId = this.branchId;
    // body.ExcelineClass.CreatedUser = this.CurrentUser.username;
    // body.ExcelineClass.ModifiedUser = this.CurrentUser.username;

    const url = this.classApiURL + 'SaveClass';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  UpdateClass(body: any): Observable<any> {
    const url = this.classApiURL + 'UpdateClass';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  DeleteClass(params: any): Observable<any> {
    const url = this.classApiURL + 'DeleteClass?classId=' + params.classId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetClasses(params: any): Observable<any> {
    const url = this.classApiURL + 'GetClasses?className='
      + params.className + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetMembersByActiveTimeId(params: any): Observable<any> {
    const url = this.classApiURL + 'GetMembersByActiveTimeId?activeTimeId='
      + params.activeTimeId + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetScheduleItemsByClass(params: any): Observable<any> {
    const url = this.classApiURL + 'GetScheduleItemsByClass?classId='
      + params.classId + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetScheduleItemsByClassIds(body: any): Observable<any> {
    const url = this.classApiURL + 'GetScheduleItemsByClassIds';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  GetEntityActiveTimes(body: any): Observable<any> {
    body.BranchId = this.branchId;
    const url = this.classApiURL + 'GetEntityActiveTimes';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  GetExcelineClassActiveTimes(params: any): Observable<any> {
    const url = this.classApiURL + 'GetExcelineClassActiveTimes?startTime='
      + params.startTime + '&entNO=' + params.entNO +
      '&endTime=' + params.endTime + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetInstructorsForClass(params: any): Observable<any> {
    const url = this.classApiURL + 'GetInstructorsForClass?classId='
      + params.classId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetResources(params: any): Observable<any> {
    const url = this.classApiURL + 'GetResources?searchText='
      + params.searchText + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetResourceForClass(params: any): Observable<any> {
    const url = this.classApiURL + 'GetResourceForClass?classId='
      + params.classId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetClassSchedule(params: any): Observable<any> {
    const url = this.classApiURL + 'GetClassSchedule?classId='
      + params.classId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetExcelineClassIdByName(params: any): Observable<any> {
    const url = this.classApiURL + 'GetExcelineClassIdByName?className='
      + params.className;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }


  CheckActiveTimeOverlapWithClass(body: any): Observable<any> {
    body.Culture = this.culture;
    const url = this.classApiURL + 'CheckActiveTimeOverlapWithClass';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  SaveScheduleItem(body: any): Observable<any> {
    body.BranchId = this.branchId;
    body.Culture = this.culture;
    const url = this.classApiURL + 'SaveScheduleItem';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  UpdateCalenderActiveTimes(body: any): Observable<any> {
    body.BranchId = this.branchId;
    const url = this.classApiURL + 'UpdateCalenderActiveTimes';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  DeleteScheduleItem(body: any): Observable<any> {
    const url = this.classApiURL + 'DeleteScheduleItem';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  DeleteSchedule(body: any): Observable<any> {
    const url = this.classApiURL + 'DeleteSchedule';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  SaveActiveTimes(body: any): Observable<any> {
    body.BranchId = this.branchId;
    body.Culture = this.culture;
    const url = this.classApiURL + 'SaveActiveTimes';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  UpdateTimeTableScheduleItems(body: any): Observable<any> {
    body.BranchId = this.branchId;
    body.Culture = this.culture;
    const url = this.classApiURL + 'UpdateTimeTableScheduleItems';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  UpdateClassCalendarActiveTime(body: any): Observable<any> {
    const url = this.classApiURL + 'UpdateClassCalendarActiveTime';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }


  UpdateSeasonEndDateWithClassActiveTimes(params: any): Observable<any> {
    const url = this.classApiURL + 'UpdateSeasonEndDateWithClassActiveTimes?seasonId='
      + params.seasonId + '&endDate=' + params.endDate + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  DeleteClassActiveTime(params: any): Observable<any> {
    const url = this.classApiURL + 'DeleteClassActiveTime?actTimeId='
      + params.actTimeId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }


  UpdateClassActiveTime(body: any): Observable<any> {
    const url = this.classApiURL + 'UpdateClassActiveTime';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  GetActiveTimesForClassScheduleItem(params: any): Observable<any> {
    const url = this.classApiURL + 'GetActiveTimesForClassScheduleItem?scheduleItemId='
      + params.scheduleItemId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetListOrCalendarInitialViewByUser(): Observable<any> {
    const url = this.classApiURL + 'GetListOrCalendarInitialViewByUser';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetGymSettings(params: any): Observable<any> {
    const url = this.classApiURL + 'GetGymSettings?gymSettingType=' + params.gymSettingType
      + '&systemName=' + params.systemName
      + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetSeasonAndClassInfo(branchId: number, seasonId: number): Observable<any> {
    const url = this.classApiURL + 'GetSeasonAndClassInfo?branchId=' + branchId + '&seasonId=' + seasonId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }


}
