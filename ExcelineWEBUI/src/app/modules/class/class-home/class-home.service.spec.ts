import { TestBed, inject } from '@angular/core/testing';

import { ClassHomeService } from './class-home.service';

describe('ClassHomeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClassHomeService]
    });
  });

  it('should be created', inject([ClassHomeService], (service: ClassHomeService) => {
    expect(service).toBeTruthy();
  }));
});
