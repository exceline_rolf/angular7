import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class ClassHomeService {

  private classActiveTimes?: any[];
  private classTypeList: any[];
  private classIds: any[];
  private excelineClassList: any[];
  private instructorList: any[];
  private resourceRecords: any;

  public classActiveTimesLoaded = new Subject<any>();
  public headerFilter = new Subject<any>();
  public instructorsSet = new Subject<any>();

  public get ClassActiveTimes(): any[] {
    return this.classActiveTimes;
  }

  public set ClassActiveTimes(value: any[]) {
    this.classActiveTimes = value;
    this.classActiveTimesLoaded.next(this.classActiveTimes);
  }

  public get ClassTypeList(): any[] {
    return this.classTypeList;
  }

  public set ClassTypeList(value: any[]) {
    this.classTypeList = value;
  }

  public get ClassIds(): any[] {
    return this.classIds;
  }

  public set ClassIds(value: any[]) {
    this.classIds = value;
  }

  public get ExcelineClassList(): any[] {
    return this.excelineClassList;
  }

  public set ExcelineClassList(value: any[]) {
    this.excelineClassList = value;
  }

  public get InstructorList(): any[] {
    return this.instructorList;
  }

  public set InstructorList(value: any[]) {
    this.instructorList = value;
    this.instructorsSet.next(this.instructorList)
  }

  public get ResourceRecords(): any {
    return this.resourceRecords;
  }

  public set ResourceRecords(value: any) {
    this.resourceRecords = value;
  }

  constructor() { }

}
