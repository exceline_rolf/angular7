import { PreloaderService } from '../../../shared/services/preloader.service';
import { ClassHomeService } from './class-home.service';
import { Component, OnInit } from '@angular/core';

import { AdminService } from '../../admin/services/admin.service';
import { ExceClassService } from '../../class/services/exce-class.service';
import { ExceMemberService } from '../../membership/services/exce-member.service';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';

@Component({
  selector: 'app-class-home',
  templateUrl: './class-home.component.html',
  styleUrls: ['./class-home.component.scss']
})
export class ClassHomeComponent implements OnInit {

  isLoadCalandarView: boolean;
  classLevelList: any;
  classTypeList: any;
  classGroupList: any;
  instructorList: any;
  keyWordList: any;
  resourceRecords: any;
  classIds: any[];
  filterCriteria: any = {
    keyWord: '',
    instructor: '',
    group: '',
    type: '',
    level: '',
    location: ''
  }
  message: string;
  type: string;
  isShowInfo: boolean;

  constructor(
    private exceClassService: ExceClassService,
    private adminService: AdminService,
    private exceMemberService: ExceMemberService,
    private classHomeService: ClassHomeService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'CLASS.GroupTimerC',
      url: '/class'
    });

    this.isLoadCalandarView = false;
  }

  ngOnInit() {
    this.adminService.getCategories('CLASSGROUP').subscribe(
      res => {
        this.classGroupList = res.Data;
      }, err => {
        console.log(err);
      });

    this.adminService.getCategories('CLASSLEVEL').subscribe(
      res => {
        this.classLevelList = res.Data;
      }, err => {
        console.log(err);
      });

    this.adminService.getCategories('KEYWORD').subscribe(
      res => {
        this.keyWordList = res.Data;
      }, err => {
        console.log(err);
      });

    this.exceClassService.GetResources({ searchText: '' }).subscribe(
      res => {
        this.resourceRecords = res.Data;
        this.classHomeService.ResourceRecords = this.resourceRecords;
      }, err => {
        console.log(err);
      });

    this.exceClassService.GetClassTypes().subscribe(
      classTypes => {
        this.classTypeList = classTypes.Data;
        this.classHomeService.ClassTypeList = this.classTypeList;
        this.exceClassService.GetClasses({ className: '' }).subscribe(
          classes => {
            this.classIds = [];
            classes.Data.forEach(clsss => {
              this.classIds.push(clsss.Id)
            });
            this.classHomeService.ExcelineClassList = classes.Data;
            this.classHomeService.ClassIds = this.classIds;
            this.exceClassService.GetEntityActiveTimes({
              StartDate: new Date(),
              EndDate: new Date(),
              EntityList: this.classIds,
              EntityRoleType: 'CLS'
            }).subscribe(
              activeTimes => {
                activeTimes.Data.forEach(times => {
                  const type = this.classTypeList.filter(x => x.Id === times.ClassTypeId)
                  times.ClassType = type[0];
                  times.Color = type[0].Colour;
                  const now = new Date();
                  const startDateTime = new Date(times.StartDateTime);
                  if (startDateTime > now) {
                    times.IsDeleted = true;
                  }
                });

                this.classHomeService.ClassActiveTimes = activeTimes.Data;
              }, err => {

              });
          }, err => {
            console.log(err);
          });
      }, err => {
        console.log(err);
      });



    this.exceClassService.GetListOrCalendarInitialViewByUser().subscribe(
      res => {
      }, err => {
        console.log(err);
      });

    this.exceClassService.GetGymSettings({ gymSettingType: 'OTHER' }).subscribe(
      res => {
      }, err => {
        console.log(err);
      });

    this.instructorList =  this.classHomeService.InstructorList;

    this.classHomeService.instructorsSet.subscribe(instructors => {
      this.instructorList = instructors;
    })
  }

  handleInformation(infoEvent): void {
    // Recieves information from nested components and display at parent level. Used for instances where information is to be shown, but modal is closed simultaniously.
    this.type = infoEvent.type;
    this.message = infoEvent.message;
    this.isShowInfo = true;
    setTimeout(() => {
      this.isShowInfo = false;
    }, 4000);
  }
  changeView(isLoadCalandarView) {
    PreloaderService.showPreLoader();
    this.exceClassService.GetEntityActiveTimes({
      StartDate: new Date(),
      EndDate: new Date(),
      EntityList: this.classIds,
      EntityRoleType: 'CLS'
    }).subscribe(
      activeTimes => {
        activeTimes.Data.forEach(times => {
          const type = this.classTypeList.filter(x => x.Id === times.ClassTypeId)
          times.ClassType = type[0];
          times.Color = type[0].Colour;

          const now = new Date();
          const startDateTime = new Date(times.StartDateTime);
          if (startDateTime > now) {
            times.IsDeleted = true;
          }
        });

        this.classHomeService.ClassActiveTimes = activeTimes.Data;
        this.isLoadCalandarView = isLoadCalandarView;
        PreloaderService.hidePreLoader();
      }, err => {
        PreloaderService.hidePreLoader();
      });
  }

  onTypeChange(value) {
    this.filterCriteria.type = value;
    this.classHomeService.headerFilter.next(this.filterCriteria);
  }

  onInstructorChange(value) {
    this.filterCriteria.instructor = value;
    this.classHomeService.headerFilter.next(this.filterCriteria);
  }

  onKeywordChange(value) {
    this.filterCriteria.keyWord = value;
    this.classHomeService.headerFilter.next(this.filterCriteria);
  }

  onGroupChange(value) {
    this.filterCriteria.group = value;
    this.classHomeService.headerFilter.next(this.filterCriteria);
  }

  onLevelChange(value) {
    this.filterCriteria.level = value;
    this.classHomeService.headerFilter.next(this.filterCriteria);
  }

  onLocationChange(value) {
    this.filterCriteria.location = value;
    this.classHomeService.headerFilter.next(this.filterCriteria);
  }

}
