import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { ExceClassService } from '../../services/exce-class.service';
import { UsErrorService } from '../../../../shared/directives/us-error/us-error.service';
import { ClassHomeService } from '../class-home.service';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { ColumnDataType, EntitySelectionType, DayOfWeek } from '../../../../shared/enums/us-enum';
import { UsbModalRef } from '../../../../shared/components/us-modal/modal-ref';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { Component, OnInit, OnDestroy, EventEmitter, Output, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryTypes } from 'app/shared/enums/category-types';
import * as _ from 'lodash';
import * as moment from 'moment';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { IMyDpOptions } from 'app/shared/components/us-date-picker/interfaces';
import { ManageSeasonsService } from 'app/modules/admin/manage-seasons/manage-seasons.service';

@Component({
  selector: 'add-new-calss',
  templateUrl: './add-new-calss.component.html',
  styleUrls: ['./add-new-calss.component.scss']
})
export class AddNewCalssComponent implements OnInit, OnDestroy {
  isDeleted: boolean;
  confMsgSub: Subscription;
  translatedMsgs: any;
  scheduleBody: any;
  model: any;
  currentSeason: any;
  @Input() startTime?: any;
  @Input() fromDate: any;
  @Input() toDate: any;
  @Input() isDisableDateChange: boolean;
  @Input() isDisableToTimeChange: boolean;
  @Input() isFixed: boolean;
  @Input() day: number;
  @Input() isNewBooking: boolean;
  @Input() activeTimeDetail: any;
  @Input() scheduleType: string;
  @Output() saveNewClassEvent = new EventEmitter();
  @Output() cancelAddingNewCls = new EventEmitter();

  private isvalidSchedulItem: boolean;
  public classInfoForm: FormGroup;
  public scheduleForm: FormGroup;

  selectedClassType: any;
  resourceIdList: any = [];
  instructorIdList: any = [];
  timeDuration = 0;
  endTime: string;
  selectedLocations: any[] = [];
  selectedInstructors: any[] = [];
  classColor: any;
  resourceRecords: any;
  InstructorList: any[];
  classTypeList: any;
  itemCount: any;
  entityItems: any[] = [];
  entitySelectionModel: UsbModalRef;
  entitySearchType: string;
  multipleSelect: boolean;
  clsInfoFormSubmited: boolean;
  classCategoryList: any[] = [];
  entityIdForSeason: number;

  columnsToShow = ['LastName', 'FirstName', 'CustId'];

  columns = [
    { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'ClassGroup', header: 'ClassGroup', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'ClassLevel', header: 'ClassLevel', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
  ];

  auotocompleteItems = [
    { id: 'Name', name: 'Name :', isNumber: false },
    { id: 'ClassGroup', name: 'Class Group :', isNumber: false },
    { id: 'ClassLevel', name: 'Class Level :', isNumber: false },
    { id: 'ClassCategory', name: 'Class Category :', isNumber: false },
    { id: 'ClassKeywords', name: 'Class Keywords :', isNumber: false }
  ];

  formErrors = {
    'Name': ''
  }

  validationMessages = {
    'Name': { 'required': 'CLASS.SelectClassType' }
  }

  toDatePickerOptions: IMyDpOptions;

  invalidDateInterval: IMyDpOptions;


  constructor(
    private exceClassService: ExceClassService,
    private classHomeService: ClassHomeService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private exceMessageService: ExceMessageService,
    private manageSeasonsService: ManageSeasonsService,
    private modalService: UsbModal,
    private translate: TranslateService
  ) {
    this.isDeleted = false;
    this.entitySearchType = 'CLA';
    this.multipleSelect = false;

    this.confMsgSub = this.exceMessageService.yesCalledHandle.subscribe((value) => {
      this.yesHandler();
    });

    translate.get([
      'CLASS.Instructor',
      'CLASS.Resource',
      'CLASS.ScheduleOverlapWithInstructor',
      'CLASS.ScheduleOverlapWithResource',
      'CLASS.ClassEntityProcessMsg'
    ]).subscribe((res) => {
      this.translatedMsgs = res;
    });

  }

  ngOnInit() {
    this.entityIdForSeason = Number(this.route.snapshot.params['Id']);
    this.classInfoForm = this.fb.group({
      Name: [null, [Validators.required]],
      Code: [{ value: '', disabled: true }],
      Group: [{ value: '', disabled: true }],
      TimeDuration: [{ value: '', disabled: true }],
      ClassLevel: [{ value: '', disabled: true }],
      ClassKeywordString: [{ value: '', disabled: true }],
      TimeCategory: [{ value: '', disabled: true }]
    });

    this.scheduleForm = this.fb.group({
      Day: [{ value: this.day, disabled: this.isFixed }],
      StartTime: [null],
      EndTime: [{ value: null, disabled: this.isDisableToTimeChange }],
      MaxNoOfBooking: [null],
      WeekType: [{ value: 2, disabled: this.isFixed }],
      FromDate: [{ value: this.fromDate, disabled: this.isDisableDateChange }],
      ToDate: [{ value: this.toDate, disabled: this.isDisableDateChange }]
    });


    if (!this.isNewBooking) {
      this.classColor = this.activeTimeDetail.color;
      this.classInfoForm.patchValue({
        Name: this.activeTimeDetail.ClassType.Name,
        Code: this.activeTimeDetail.ClassType.Code,
        Group: this.activeTimeDetail.ClassType.ClassGroup,
        TimeDuration: this.activeTimeDetail.ClassType.TimeDuration,
        ClassLevel: this.activeTimeDetail.ClassType.ClassLevel,
        ClassKeywordString: this.activeTimeDetail.ClassType.ClassKeywordString,
        TimeCategory: this.activeTimeDetail.ClassType.TimeCategory
      });

      this.classCategoryList = this.activeTimeDetail.ClassType.ClassCategoryList;
      this.classColor = this.activeTimeDetail.ClassType.Colour;

      this.scheduleForm.patchValue({
        MaxNoOfBooking: this.activeTimeDetail.ClassType.MaxNoOfBookings,
      });
      this.setTimes(this.startTime, this.activeTimeDetail.ClassType.TimeDuration);
      this.timeDuration = this.activeTimeDetail.ClassType.TimeDuration;
      this.selectedClassType = this.activeTimeDetail.ClassType;

      this.selectedInstructors = this.activeTimeDetail.InstructorList;
      if (this.selectedInstructors) {
        this.selectedInstructors.forEach(ins => {
          ins.Name = ins.DisplayName;
        });
      } else {
        this.selectedInstructors = [];
      }

      this.selectedLocations = this.activeTimeDetail.ResourceLst;
      if (this.selectedLocations) {
        this.selectedLocations.forEach(loc => {
          loc.Name = loc.DisplayName;
        });
      } else {
        this.selectedLocations = [];
      }

      const now1 = new Date();
      const startDateTime = new Date(this.activeTimeDetail.StartDateTime);
      if (startDateTime > now1) {
        this.isDeleted = true;
      }

      this.toDatePickerOptions =  {
        disableSince: { year: startDateTime.getFullYear(), month: startDateTime.getMonth() + 1, day: startDateTime.getDate()}
      };

    } else {

      const startD = this.scheduleForm.get('FromDate').value

      const useDate = new Date(startD.date.year, startD.date.month - 1, startD.date.day)


      if (this.isFixed) {
        this.toDate = { year: startD.date.year, month: startD.date.month, day: startD.date.day };
        this.scheduleForm.patchValue({
          ToDate: { date: this.toDate },
        });

        useDate.setDate(useDate.getDate() + 1)
        const tempDate = new Date(startD.date.year, startD.date.month - 1, startD.date.day);
        tempDate.setDate(tempDate.getDate() - 1)
        this.toDatePickerOptions =  {
          disableUntil: { year: tempDate.getFullYear(), month: tempDate.getMonth() + 1, day: tempDate.getDate()},
          disableSince: { year: useDate.getFullYear(), month: useDate.getMonth() + 1, day: useDate.getDate()}
        };
      } else {
        useDate.setDate(useDate.getDate() - 1)
        this.toDatePickerOptions =  {
        disableUntil: { year: useDate.getFullYear(), month: useDate.getMonth() + 1, day: useDate.getDate()}
        };
      }

    }


    this.classInfoForm.statusChanges.subscribe(__ => {
      UsErrorService.onValueChanged(this.classInfoForm, this.formErrors, this.validationMessages)
    });
    if (this.classHomeService.ExcelineClassList.length > 0) {
      this.classHomeService.ExcelineClassList.forEach(item => {
        if (item.Id === this.entityIdForSeason) {
          this.currentSeason = item
          const fromDateMax = new Date(this.currentSeason.Schedule.StartDate)
          const toDateMax = new Date(this.currentSeason.Schedule.EndDate)
          this.invalidDateInterval = {
            disableUntil: {year: fromDateMax.getFullYear(), month: fromDateMax.getMonth() + 1 , day: fromDateMax.getDate() - 1 },
            disableSince: {year: toDateMax.getFullYear(), month: toDateMax.getMonth() + 1 , day: toDateMax.getDate() + 1  }
          }
        }
      });
    }
    // if (this.classHomeService.ExcelineClassList.length > 0) {
    //   this.classHomeService.ExcelineClassList.forEach(item => {
    //     console.log(item)
    //     const now = new Date();
    //     const startDate = new Date(item.Schedule.StartDate);
    //     const endDate = new Date(item.Schedule.EndDate);
    //     if (startDate <= now) {
    //       if (endDate && endDate >= now) {
    //         this.currentSeason = item;
    //         return false;
    //       } else if (!endDate) {
    //         this.currentSeason = item;
    //         return false;
    //       }
    //     }
    //   });
    // }
  }

  ngOnDestroy(): void {
    if (this.entitySelectionModel) {
      this.entitySelectionModel.close();
    }
    if (this.confMsgSub) {
      this.confMsgSub.unsubscribe();
    }
  }

  closeEntView() {
    this.entitySelectionModel.close();
  }

  openClassTypeSelection(content) {
    this.entitySelectionModel = this.modalService.open(content, { width: '1000' });
    this.columns = [
      { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'ClassGroup', header: 'ClassGroup', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'ClassLevel', header: 'ClassLevel', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ];

    this.auotocompleteItems = [
      { id: 'Name', name: 'Name :', isNumber: false },
      { id: 'ClassGroup', name: 'Class Group :', isNumber: false },
      { id: 'ClassLevel', name: 'Class Level :', isNumber: false },
      { id: 'ClassCategory', name: 'Class Category :', isNumber: false },
      { id: 'ClassKeywords', name: 'Class Keywords :', isNumber: false }
    ];
    this.multipleSelect = false;
    this.entitySearchType = 'CLA';

  }

  searchDeatail(val: any) {
    this.entityItems = [];
    const searchArray = val.searchVal.split(':');

    switch (val.entitySelectionType) {
      case EntitySelectionType[EntitySelectionType.CLA]:
        this.classTypeList = this.classHomeService.ClassTypeList
        if (searchArray.length > 1) {
          const searchType = searchArray[0].trim();
          switch (searchType) {
            case 'Name':
              this.entityItems = this.classTypeList.filter(x => x.Name.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()));
              break;
            case 'ClassGroup':
              this.entityItems = this.classTypeList.filter(x => x.ClassGroup.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()));

              break;
            case 'ClassLevel':
              this.entityItems = this.classTypeList.filter(x => x.ClassLevel.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()));

              break;
            case 'ClassCategory':
              this.classTypeList.forEach(categoryType => {
                const items = categoryType.ClassCategoryStringList.filter(x => x.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()));
                if (items.length > 0) {
                  this.entityItems.push(categoryType);
                }
              });
              break;
            case 'ClassKeywords':
              this.classTypeList.forEach(categoryType => {
                if (categoryType.ClassKeywordString) {
                  const items = categoryType.ClassKeywordString.split(',').filter(x => x.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()))
                  if (items.length > 0) {
                    this.entityItems.push(categoryType);
                  }
                }
              });
              break;
            default:
              this.entityItems = this.classTypeList;
              break;
          }
        } else {
          this.entityItems = this.classTypeList;
        }
        this.entityItems = _.orderBy(this.entityItems, 'Name');
        break;
      case EntitySelectionType[EntitySelectionType.INS]:
        this.InstructorList = this.classHomeService.InstructorList;
        if (searchArray.length > 1) {
          const searchType = searchArray[0].trim();
          switch (searchType) {
            case 'Name':
              this.entityItems = this.InstructorList.filter(x => x.Name.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()));
              break;
            case 'Id':
              this.entityItems = this.InstructorList.filter(x => x.CustId.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()));

              break;
            default:
              this.entityItems = this.InstructorList;
              break;

          }
        } else {
          this.entityItems = this.InstructorList;
        }
        this.entityItems = _.orderBy(this.entityItems, 'Name');
        if (this.selectedInstructors.length > 0) {
          this.selectedInstructors.forEach(ins => {
            this.entityItems = this.entityItems.filter(x => x.Id !== ins.Id);
          });
        }
        break;
      case EntitySelectionType[EntitySelectionType.RES]:
        this.resourceRecords = this.classHomeService.ResourceRecords;
        if (searchArray.length > 1) {
          const searchType = searchArray[0].trim();
          switch (searchType) {
            case 'Name':
              this.entityItems = this.resourceRecords.filter(x => x.Name.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()));
              break;
            case 'Id':
              this.entityItems = this.resourceRecords.filter(x => x.Id === Number(searchArray[1]));

              break;
            default:
              this.entityItems = this.resourceRecords;
              break;

          }
        } else {
          this.entityItems = this.resourceRecords;
        }
        this.entityItems = _.orderBy(this.entityItems, 'Name');
        if (this.selectedLocations.length > 0) {
          this.selectedLocations.forEach(ins => {
            this.entityItems = this.entityItems.filter(x => x.Id !== ins.Id);
          });
        }
        break;
    }

  }

  selecteItem(event) {
    switch (this.entitySearchType) {
      case 'CLA':
        this.classInfoForm.patchValue({
          Name: event.Name,
          Code: event.Code,
          Group: event.ClassGroup,
          TimeDuration: event.TimeDuration,
          ClassLevel: event.ClassLevel,
          ClassKeywordString: event.ClassKeywordString,
          TimeCategory: event.TimeCategory
        });
        this.classCategoryList = event.ClassCategoryList;
        this.classColor = event.Colour;

        this.scheduleForm.patchValue({
          MaxNoOfBooking: event.MaxNoOfBookings,
        });
        this.setTimes(this.startTime, event.TimeDuration);
        this.timeDuration = event.TimeDuration;
        this.selectedClassType = event;
        break;
      default:
        break;
    }
    this.entitySelectionModel.close();
  }

  setTimes(startTime, duration) {
    const date = moment(new Date()).format('YYYY-MM-DD');
    this.endTime = (moment(date + ' ' + startTime).add(duration, 'minute')).format('HH:mm');
    this.scheduleForm.patchValue({
      StartTime: startTime,
      EndTime: this.endTime
    });
  }

  private getTimePickerDateVal(time) {
    const date = moment(new Date()).format('YYYY-MM-DD');
    return (moment(date + ' ' + time)).format('YYYY-MM-DD HH:mm');
  }

  inputFieldChanged(event) {
    this.setTimes(event, this.timeDuration);
  }

  selectMultipleItems(event) {
    switch (this.entitySearchType) {
      case 'INS':
        event.forEach(ins => {
          this.selectedInstructors.push(ins);
        });
        break;
      case 'RES':
        event.forEach(loc => {
          this.selectedLocations.push(loc);
        });
        break;

      default:
        break;
    }
    this.entitySelectionModel.close();
  }

  openInstructorSelection(content) {
    this.multipleSelect = true;
    this.entitySelectionModel = this.modalService.open(content, { width: '1000' });
    this.columns = [
      { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'CustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ];

    this.auotocompleteItems = [
      { id: 'Id', name: 'Id :', isNumber: true },
      { id: 'Name', name: 'Name :', isNumber: false }
    ];
    this.entitySearchType = 'INS';

  }

  OpenLocationSelection(content) {
    this.multipleSelect = true;
    this.entitySelectionModel = this.modalService.open(content, { width: '1000' });
    this.columns = [
      { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ];

    this.auotocompleteItems = [
      { id: 'Id', name: 'Id :', isNumber: true },
      { id: 'Name', name: 'Name :', isNumber: false }
    ];
    this.entitySearchType = 'RES';

  }

  removeInstructor(item) {
    this.selectedInstructors = this.selectedInstructors.filter(x => x.Id !== item.Id);
  }

  removeLocation(item) {
    this.selectedLocations = this.selectedLocations.filter(x => x.Id !== item.Id);
  }

  saveNewClass() {
    this.clsInfoFormSubmited = true;
    if (this.classInfoForm.valid) {

      if (this.selectedInstructors) {
        this.selectedInstructors.forEach(ins => {
          this.instructorIdList.push(ins.Id);
        });
      }

      if (this.selectedInstructors) {
        this.selectedLocations.forEach(res => {
          this.resourceIdList.push(res.Id);
        });
      }
      let fixedSeason;
      if (this.isFixed) {
        this.manageSeasonsService.ClassList.forEach(season => {
          const fDate = { year: Number(season.Schedule.StartDate.split('T')[0].split('-')[0]),
                          month: Number(season.Schedule.StartDate.split('T')[0].split('-')[1]),
                          day: Number(season.Schedule.StartDate.split('T')[0].split('-')[2])
                        }
          const eDate = { year: Number(season.Schedule.EndDate.split('T')[0].split('-')[0]),
                          month: Number(season.Schedule.EndDate.split('T')[0].split('-')[1]),
                          day: Number(season.Schedule.EndDate.split('T')[0].split('-')[2])
                        }
          const fFormDate = this.scheduleForm.getRawValue().FromDate.date;
          const eFormDate = this.scheduleForm.getRawValue().ToDate.date;

          const jsFromDate = new Date(fDate.year, fDate.month - 1, fDate.day)
                        console.log(new Date(fDate.year, fDate.month - 1, fDate.day) <= new Date(fFormDate.year, fFormDate.month - 1, fFormDate.day) &&
                        new Date(eDate.year, eDate.month - 1, eDate.day) >= new Date(eFormDate.year, eFormDate.month - 1, eFormDate.day))

          const condi = new Date(fDate.year, fDate.month - 1, fDate.day) <= new Date(fFormDate.year, fFormDate.month - 1, fFormDate.day) &&
          new Date(eDate.year, eDate.month - 1, eDate.day) >= new Date(eFormDate.year, eFormDate.month - 1, eFormDate.day);

            if (condi) {
              fixedSeason = season
            }

        });

      }

      const fromDate =
        this.scheduleForm.getRawValue().FromDate.date.year + '-' +
        this.scheduleForm.getRawValue().FromDate.date.month + '-' +
        this.scheduleForm.getRawValue().FromDate.date.day;

      const toDate =
        this.scheduleForm.getRawValue().ToDate.date.year + '-' +
        this.scheduleForm.getRawValue().ToDate.date.month + '-' +
        this.scheduleForm.getRawValue().ToDate.date.day;


        console.log('I am saving with this SeasonId: ', this.currentSeason.Schedule.Id)
      this.scheduleBody = {
        ClassTypeId: this.selectedClassType.Id,
        ClassType: this.selectedClassType,
        StartDate: fromDate,
        EndDate: toDate,
        StartTime: this.getTimePickerDateVal(this.scheduleForm.getRawValue().StartTime),
        EndTime: this.getTimePickerDateVal(this.scheduleForm.getRawValue().EndTime),
        MaxNoOfBooking: this.scheduleForm.getRawValue().MaxNoOfBooking,
        InstructorIdList: this.instructorIdList,
        ResourceIdList: this.resourceIdList,
        IsFixed: this.isFixed,
        Occurrence: this.scheduleType,
        Day: DayOfWeek[this.scheduleForm.getRawValue().Day],
        Week: 0,
        Month: 0,
        Year: 0,
        WeekType: this.scheduleForm.getRawValue().WeekType,
        ScheduleId: this.isFixed ? fixedSeason.Schedule.Id : this.currentSeason.Schedule.Id
      }
      if (this.instructorIdList.length > 0 || this.resourceIdList.length > 0) {
        this.exceClassService.CheckActiveTimeOverlapWithClass(this.scheduleBody).subscribe(
          res => {
            const sTextArray = res.Data.split(':');

            if (sTextArray.length > 0) {
              if (Number(sTextArray[0]) === 1) {
                this.SaveScheduleItem(this.scheduleBody);
              } else if (Number(sTextArray[0]) === -2) {
                let entityMsg = '';
                if (sTextArray.length > 1) {

                  const entityArray1 = sTextArray[1].split(',');

                  if (entityArray1[0] === 'INS') {
                    entityMsg = this.translatedMsgs['CLASS.Instructor'] + ':' + entityArray1[1];
                  } else if (entityArray1[0] === 'RES') {
                    entityMsg = this.translatedMsgs['CLASS.Resource'] + ':' + entityArray1[1];
                  }

                  if (sTextArray.length > 2) {
                    const entityArray2 = sTextArray[2].split(',');
                    if (entityArray2[0] === 'RES') {
                      entityMsg = entityMsg + ' ' + this.translatedMsgs['CLASS.Resource'] + ':' + entityArray2[1];
                    }
                  }

                  const confirmMsg = entityMsg + ' ' + this.translatedMsgs['CLASS.ClassEntityProcessMsg'];
                  this.model = this.exceMessageService.openMessageBox('CONFIRM',
                    {
                      messageTitle: 'Exceline',
                      messageBody: confirmMsg
                    }
                  );

                }
              }
            }
          }, err => {
            console.log(err);
          }
        );
      } else {
        this.SaveScheduleItem(this.scheduleBody);
      }

    } else {
      UsErrorService.validateAllFormFields(this.classInfoForm, this.formErrors, this.validationMessages);
    }
  }

  private yesHandler() {
    this.SaveScheduleItem(this.scheduleBody);
  }

  private SaveScheduleItem(body): void {
    console.log('Ble jeg kjørt? ', body )
    this.saveNewClassEvent.emit(body);
  }

  closeNewClsModel() {
    this.cancelAddingNewCls.emit();
  }

  fromDateChanged(event) {
    const date = event.date
    const fromDate = new Date(date.year, date.month - 1, date.day)
    const toDate = new Date(this.scheduleForm.value.ToDate.date.year,
      this.scheduleForm.value.ToDate.date.month - 1,
      this.scheduleForm.value.ToDate.date.day)
    if (fromDate > toDate) {
      this.toDate = { year: date.year, month: date.month, day: date.day };
      this.scheduleForm.patchValue({
        ToDate: { date: this.toDate },
      });
    } else if (this.isFixed) {
      this.toDate = { year: date.year, month: date.month, day: date.day };
      this.scheduleForm.patchValue({
        ToDate: { date: this.toDate },
      });
    }

    if (this.isFixed) {
      this.scheduleForm.patchValue({
        Day: fromDate.getDay()
      })
      fromDate.setDate(fromDate.getDate() + 1)
      const date2 = event.date
      const tempDate = new Date(date.year, date.month - 1, date.day)
      tempDate.setDate(tempDate.getDate() - 1)
      this.toDatePickerOptions =  {
        disableUntil: { year: tempDate.getFullYear(), month: tempDate.getMonth() + 1, day: tempDate.getDate()},
        disableSince: { year: fromDate.getFullYear(), month: fromDate.getMonth() + 1, day: fromDate.getDate()}
      };
    } else {
      fromDate.setDate(fromDate.getDate() - 1)
      this.toDatePickerOptions =  {
        disableUntil: { year: fromDate.getFullYear(), month: fromDate.getMonth() + 1, day: fromDate.getDate()}
      };
    }
  }

}
