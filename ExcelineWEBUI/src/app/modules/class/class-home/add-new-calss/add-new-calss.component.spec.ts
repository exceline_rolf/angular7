import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewCalssComponent } from './add-new-calss.component';

describe('AddNewCalssComponent', () => {
  let component: AddNewCalssComponent;
  let fixture: ComponentFixture<AddNewCalssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewCalssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewCalssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
