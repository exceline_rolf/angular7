import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

import { UssAdminRoutingModule } from './uss-admin-routing.module';
import { UssAdminRoutingComponents } from './uss-admin-routing.module';
import { UserManagementComponent } from './user-settings/user-management/user-management.component';
import { RoleManagementComponent } from './user-settings/role-management/role-management.component';
import { UserDetailsComponent } from './user-settings/user-management/user-details/user-details.component';
import { UserRoleComponent } from './user-settings/role-management/user-role/user-role.component';
import { USSAdminService } from './services/uss-admin-service'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

// export function createTranslateLoader(http: HttpClient) {
//   return new TranslateHttpLoader(http, './assets/i18n/modules/uss-admin/', '.json');
// }

export function translateLoader(http: HttpClient) {

  return new MultiTranslateHttpLoader(http, [
    { prefix: './assets/i18n/', suffix: '.json' },
    { prefix: './assets/i18n/modules/admin/', suffix: '.json' },
    { prefix: './assets/i18n/modules/uss-admin/', suffix: '.json' }
  ]);
}

export class MultiTranslateHttpLoader implements TranslateLoader {

  constructor(private http: HttpClient,
    public resources: { prefix: string, suffix: string }[] = [
      {
        prefix: '/assets/i18n/',
        suffix: '.json'
      }
    ]) { }

  /**
   * Gets the translations from the server
   * @param lang
   * @returns {any}
   */
  public getTranslation(lang: string): any {

    return forkJoin(this.resources.map(config => {
      return this.http.get(`${config.prefix}${lang}${config.suffix}`);
    })).pipe(
      map(response => {
        return response.reduce((a, b) => {
          return Object.assign(a, b);
        });
      })
    )
  }
}

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UssAdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (translateLoader),
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    UssAdminRoutingComponents,
    UserManagementComponent,
    RoleManagementComponent,
    UserDetailsComponent,
    UserRoleComponent

  ],
  providers: [USSAdminService,
  ]
})
export class UssAdminModule { }
