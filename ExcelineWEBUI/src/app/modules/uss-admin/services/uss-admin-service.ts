import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../shared/services/common-http.service';
import { ConfigService } from '@ngx-config/core';
import { Observable } from 'rxjs';
import { ExceLoginService } from '../../../modules/login/exce-login/exce-login.service';

@Injectable()
export class USSAdminService {
    categoryObj;
    private adminServiceUrl: string;
    private branchId: number;
    constructor(private commonHttpService: CommonHttpService, private config: ConfigService, private exceLoginService: ExceLoginService) {
        this.adminServiceUrl = this.config.getSettings('EXCE_API.USSADMIN');
        this.branchId = this.exceLoginService.SelectedBranch.BranchId;
        this.exceLoginService.branchSelected.subscribe(
            (branch) => {
                this.branchId = branch.BranchId;
            }
        );

    }

    GetUspUsers(searchText: string, isActive: boolean): Observable<any> {
        const url = this.adminServiceUrl + 'GetUspUsers?searchText=' + searchText + '&&isActive=' + isActive
        return this.commonHttpService.makeHttpCall(url, {
            method: 'GET',
            auth: true
        })
    }

    SearchUSPEmployees(searchText: string, isActive: boolean): Observable<any> {
        const url = this.adminServiceUrl + 'SearchUSPEmployees?searchText=' + searchText + '&&isActive=' + isActive
        return this.commonHttpService.makeHttpCall(url, {
            method: 'GET',
            auth: true
        })
    }

    GetHardwareProfiles(branchId: number): Observable<any> {
        const url = this.adminServiceUrl + 'GetHardwareProfiles?branchId=' + branchId
        return this.commonHttpService.makeHttpCall(url, {
            method: 'GET',
            auth: true
        })
    }

    isUserAdmin(): Observable<any> {
      const url = this.adminServiceUrl + 'IsUserAdmin';
      return this.commonHttpService.makeHttpCall(url, {
          method: 'GET',
          auth: true
      })
    }

    GetBranches(): Observable<any> {
        const url = this.adminServiceUrl + 'GetBranches'
        return this.commonHttpService.makeHttpCall(url, {
            method: 'GET',
            auth: true
        })
    }

    GetUserSelectedBranches(userName: string): Observable<any> {
        const url = this.adminServiceUrl + 'GetUserSelectedBranches?userName=' + userName
        return this.commonHttpService.makeHttpCall(url, {
            method: 'GET',
            auth: true
        })
    }

    GetUSPRoles(searchText: string, isActive: boolean): Observable<any> {
        const url = this.adminServiceUrl + 'GetUSPRoles?searchText=' + searchText + '&&isActive=' + isActive
        return this.commonHttpService.makeHttpCall(url, {
            method: 'GET',
            auth: true
        })
    }

    CheckCardNumberAvilability(cardNumber: string): Observable<any> {
        const url = this.adminServiceUrl + 'CheckCardNumberAvilability?cardNumber=' + cardNumber
        return this.commonHttpService.makeHttpCall(url, {
            method: 'GET',
            auth: true
        })
    }

    GetModulesFeaturesOperations(loggedUser: string): Observable<any> {
        const url = this.adminServiceUrl + 'GetModulesFeaturesOperations?loggedUser=' + loggedUser
        return this.commonHttpService.makeHttpCall(url, {
            method: 'GET',
            auth: true
        })
    }

    GetModulesFeaturesOperationsConstrained(loggedUser: string): Observable<any> {
      const url = this.adminServiceUrl + 'GetModulesFeaturesOperationsConstrained?loggedUser=' + loggedUser
      return this.commonHttpService.makeHttpCall(url, {
          method: 'GET',
          auth: true
      })
  }

    GetSubOperationsForUser(userName: string): Observable<any> {
        const url = this.adminServiceUrl + 'GetSubOperationsForUser?userName=' + userName
        return this.commonHttpService.makeHttpCall(url, {
            method: 'GET',
            auth: true
        })
    }

    GetSubOperationsForRole(roleId: string): Observable<any> {
        const url = this.adminServiceUrl + 'GetSubOperationsForRole?roleId=' + roleId
        return this.commonHttpService.makeHttpCall(url, {
            method: 'GET',
            auth: true
        })
    }

    GetDefaultUspRole(loggedUser: string): Observable<any> {
        const url = this.adminServiceUrl + 'GetDefaultUspRole?loggedUser=' + loggedUser
        return this.commonHttpService.makeHttpCall(url, {
            method: 'GET',
            auth: true
        })
    }

    AddEditUSPUsers(addeditUsp: any): Observable<any> {
        const url = this.adminServiceUrl + 'AddEditUSPUsers'
        return this.commonHttpService.makeHttpCall(url, {
            method: 'POST',
            auth: true,
            body: addeditUsp
        });
    }

    AddEditUSPRole(addeditUspRole: any): Observable<any> {
        const url = this.adminServiceUrl + 'AddEditUSPRole'
        return this.commonHttpService.makeHttpCall(url, {
            method: 'POST',
            auth: true,
            body: addeditUspRole
        });
    }

    SaveSubOperations(saveSubOp: any): Observable<any> {
        const url = this.adminServiceUrl + 'SaveSubOperations'
        return this.commonHttpService.makeHttpCall(url, {
            method: 'POST',
            auth: true,
            body: saveSubOp
        });
    }

    ValidateUser(pword: any): Observable<any> {
        const url = this.adminServiceUrl + 'ValidateUser'
        return this.commonHttpService.makeHttpCall(url, {
            method: 'POST',
            auth: true,
            body: pword
        });
    }

    EditUSPUserBasicInfo(basicInfo: any): Observable<any> {
        const url = this.adminServiceUrl + 'EditUSPUserBasicInfo'
        return this.commonHttpService.makeHttpCall(url, {
            method: 'POST',
            auth: true,
            body: basicInfo
        });
    }

ssss
}
