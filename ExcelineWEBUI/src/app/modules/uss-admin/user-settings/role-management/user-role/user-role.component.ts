import { Component, Injectable, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../../../membership/services/exce-member.service'
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { USSAdminService } from '../../../services/uss-admin-service'
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { TreeNode } from 'app/shared/components/us-tree-table/tree-node';
import { UsErrorService } from '../../../../../shared/directives/us-error/us-error.service'
import { TreeviewItem, TreeviewConfig, DownlineTreeviewItem, TreeviewHelper } from 'app/shared/components/us-checkbox-tree';
import { CommonEncryptionService } from 'app/shared/services/common-Encryptor-Decryptor';
import * as _ from 'lodash';
import { Dictionary } from 'lodash';
import * as CryptoJS from 'crypto-js';
import { ExceBreadcrumbService } from '../../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

// import CryptoJS = require('crypto-js');
const now = new Date();


@Injectable()
export class ProductTreeviewConfig extends TreeviewConfig {
  isShowAllCheckBox = true;
  isShowFilter = true;
  isShowCollapseExpand = false;
  maxHeight = 500;
}

@Component({
  selector: 'app-user-role',
  templateUrl: './user-role.component.html',
  styleUrls: ['./user-role.component.scss']

})

export class UserRoleComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private gymCode: any;
  private branchId: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private modalReference: any;
  formSubmited = false;
  subItems = [];
  subCount = 0;
  private subResource: any = 0;
  private rowItem: any;
  private username: string;
  rolesForm: any;
  private filteredEmp: any;
  private moduleList: any;
  private subOpList: any;
  private rowSubItem: any;
  moduleFeatureItems = []


  private selectedOpList: any = [];





  @Input() selectedItem: any;
  @Input() isAddNew: true;
  @Output() canceled = new EventEmitter();
  @Output() saveSuccess = new EventEmitter();

  formErrors = {
    'Description': '',
    'RoleName': '',
    'IsActive': '',
  };

  validationMessages = {

    'Description': { 'required': 'value required.', },
    'RoleName': { 'required': 'value required.', },
    'IsActive': { 'required': 'value required.', },


  };

  auotocompleteItems = [{ id: 'NAME', name: 'NAME :', isNumber: false }, { id: 'ID', name: 'ID :', isNumber: false }]

  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };

  constructor(private exceMemberService: ExceMemberService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private ussAdminService: USSAdminService,
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService,
    private commonEncryptionService: CommonEncryptionService,

  ) {
    this.username = this.exceLoginService.CurrentUser.username
    this.gymCode = this.username.split('/')[0]
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );


  }

  ngOnInit() {
    this.rolesForm = this.fb.group({
      'Description': [null],
      'RoleName': [null, [Validators.required]],
      'IsActive': [null],
    });


    this.getModuleFetures()
    this.subOperations(1);
    if ((!this.isAddNew) && (this.selectedItem)) {
      this.rolesForm.patchValue(this.selectedItem)
    }



  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.modalReference) {
      this.modalReference.close();
    }
  }


  subOperations(value?) {
    if (this.isAddNew) {
      this.ussAdminService.GetSubOperationsForRole(value).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        this.subOpList = result.Data
        this.subResource = new DataTableResource(this.subOpList);
        this.subResource.count().then(count => this.subCount = count);
        this.subItems = this.subOpList;
        this.subCount = Number(result.Data.length);
      })


    } else {
      this.ussAdminService.GetSubOperationsForRole(this.selectedItem.Id).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        if (result) {
          this.subOpList = result.Data
          this.subResource = new DataTableResource(this.subOpList);
          this.subResource.count().then(count => this.subCount = count);
          this.subItems = this.subOpList;
          this.subCount = Number(result.Data.length);

        }
      })

    }

  }

  getModuleFetures(value?) {
    // if (this.isAddNew) {
    this.ussAdminService.GetModulesFeaturesOperations(this.username).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.moduleList = result.Data
        this.loadTree()
      }
    })

  }


  loadTree() {
    let selectedOpList = []
    if (!this.isAddNew) {
      this.selectedItem.OperationList.forEach(element => {
        selectedOpList.push(element.ID)
      });
    }
    this.moduleFeatureItems = []
    this.moduleList.forEach(element => {
      let jsm
      if (!this.isAddNew) {
        if (this.selectedItem.ModuleList.filter(x => x === element.ModuleId).length > 0) {
          jsm = new TreeviewItem({ text: element.DisplayName, value: element.ModuleId, checked: true, propertyVal: 'MODULE' })
        } else {
          jsm = new TreeviewItem({ text: element.DisplayName, value: element.ModuleId, checked: false, propertyVal: 'MODULE' })
        }
      } else {

        jsm = new TreeviewItem({ text: element.DisplayName, value: element.ModuleId, checked: true, propertyVal: 'MODULE' })

      }
      let childMod = []
      let childModOp = []


      element.Operations.forEach(elementOp => {
        if (!this.isAddNew) {
          if (selectedOpList.filter(x => x === elementOp.OperationId).length > 0) {
            childModOp.push(new TreeviewItem({ text: elementOp.DisplayName, value: elementOp.OperationId, checked: true, propertyVal: 'OPERATION' }))
          } else {
            childModOp.push(new TreeviewItem({ text: elementOp.DisplayName, value: elementOp.OperationId, checked: false, propertyVal: 'OPERATION' }))
          }
        } else {

          childModOp.push(new TreeviewItem({ text: elementOp.DisplayName, value: elementOp.OperationId, checked: true, propertyVal: 'OPERATION' }))

        }
      });

      element.Features.forEach(elementf => {
        let jsf
        if (!this.isAddNew) {
          if (this.selectedItem.FeatureList.filter(x => x === elementf.FeatureId).length > 0) {
            jsf = new TreeviewItem({ text: elementf.DisplayName, value: elementf.FeatureId, checked: true, propertyVal: 'FEATURE' })
          } else {
            jsf = new TreeviewItem({ text: elementf.DisplayName, value: elementf.FeatureId, checked: false, propertyVal: 'FEATURE' })
          }

        } else {
          jsf = new TreeviewItem({ text: elementf.DisplayName, value: elementf.FeatureId, checked: true, propertyVal: 'FEATURE' })

        }
        let childF = []
        elementf.Operations.forEach(elementO => {
          if (!this.isAddNew) {
            if (selectedOpList.filter(x => x === elementO.OperationId).length > 0) {
              childF.push(new TreeviewItem({ text: elementO.DisplayName, value: elementO.OperationId, checked: true, propertyVal: 'OPERATION' }))
            } else {
              childF.push(new TreeviewItem({ text: elementO.DisplayName, value: elementO.OperationId, checked: false, propertyVal: 'OPERATION' }))
            }
          } else {

            childF.push(new TreeviewItem({ text: elementO.DisplayName, value: elementO.OperationId, checked: true, propertyVal: 'OPERATION' }))

          }
        });

        if (childF.length > 0) {
          jsf.children = childF
        }
        childMod.push(jsf)
      });
      if (childModOp.length > 0) {
        childMod.push(new TreeviewItem({ text: '', value: '', children: childModOp, propertyVal: '' }))
      }
      if (childMod.length > 0) {
        jsm.children = childMod
      }
      this.moduleFeatureItems.push(jsm)
    });
  }



  closePage() {
    this.canceled.emit()
  }


  reloadItems(params): void {
  }


  openSubOp(content, item) {
    this.rowSubItem = item;
    this.modalReference = this.modalService.open(content, { width: '600' });
  }


  submitUserForm(value) {
    this.formSubmited = true;
    UsErrorService.validateAllFormFields(this.rolesForm, this.formErrors, this.validationMessages);
    if (this.rolesForm.valid) {
      this.saveUsp(value)
    }
  }

  saveUsp(value) {
    let moduleListForRole = []
    let featureListForRole = []
    let operationListForRole = []

    if (this.moduleFeatureItems) {
      this.moduleFeatureItems.forEach(elementM => {
        if (elementM.checked) {
          moduleListForRole.push(elementM.value)
        }
        if (elementM.children) {
          elementM.children.forEach(elementF => {
            if (elementF.checked && elementF.propertyVal === 'FEATURE') {
              featureListForRole.push(elementF.value)
            }
            if (elementF.children) {
              elementF.children.forEach(elementO => {
                if (elementO.checked && elementO.propertyVal === 'OPERATION') {
                  operationListForRole.push({ 'ID': elementO.value, 'Status': true })
                }
              });
            }
          });
        }
      })
    }

    if (this.isAddNew) {
      value.ModuleList = moduleListForRole
      value.OperationList = operationListForRole
      value.FeatureList = featureListForRole
      value.SubOperationList = this.subOpList
      this.ussAdminService.AddEditUSPRole(value).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result.Data) {
          if (result.Data > 0) {
            this.ussAdminService.SaveSubOperations({ 'subOperations': this.subOpList, 'roleId': result.Data }).pipe(takeUntil(this.destroy$)).subscribe(resultSub => {
              if (resultSub.Data) {
                this.saveSuccess.emit()
              }
            })
          }

        }
      })

    } else {
      this.selectedItem.RoleName = value.RoleName
      this.selectedItem.Description = value.Description
      this.selectedItem.IsActive = value.IsActive
      this.selectedItem.ModuleList = moduleListForRole
      this.selectedItem.OperationList = operationListForRole
      this.selectedItem.FeatureList = featureListForRole
      this.selectedItem.SubOperationList = this.subOpList
      this.ussAdminService.AddEditUSPRole(this.selectedItem).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        if (result.Data) {
          if (result.Data > 0) {
            this.ussAdminService.SaveSubOperations({ 'subOperations': this.subOpList, 'roleId': result.Data }).pipe(
              takeUntil(this.destroy$)
              ).subscribe(resultSub => {
              if (resultSub.Data) {
                this.saveSuccess.emit()
              }
            })
          }

        }
      })
    }

  }


}
