
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../../membership/services/exce-member.service'
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { USSAdminService } from '../../services/uss-admin-service'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { PlatformLocation } from '@angular/common'
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-role-management',
  templateUrl: './role-management.component.html',
  styleUrls: ['./role-management.component.scss']
})
export class RoleManagementComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId: number;
  private uspRoles: any;
  private filteredRoles: any;
  private itemResource: any = 0;
  items = [];
  itemCount = 0;;
  private inactVal: boolean = false;
  private actvVal: boolean = true;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private modalReference: any;
  private rowItem: any;
  private isAddNew: boolean = true
  private selectedIndex = 1;
  private closeResult: any;

  constructor(private exceMemberService: ExceMemberService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private ussAdminService: USSAdminService,
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService,
    private location: PlatformLocation,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;

      }
    );
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });

    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.RolesC',
      url: '/uss-admin/role-management'
    });
  }

  ngOnInit() {
    this.getRoles('', true);

  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }
  }

  getRoles(searchText?: any, isActive?: boolean) {
    this.ussAdminService.GetUSPRoles(searchText, isActive).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.uspRoles = result.Data;
        this.itemResource = new DataTableResource(this.uspRoles);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.uspRoles;
        this.itemCount = Number(result.Data.length);
      }
    })
  }

  search(searchTxt: string) {
    if (this.actvVal) {
      this.getRoles(searchTxt, true)
    } else {
      this.getRoles(searchTxt, false)
    }

  }


  filterInput(nameValue: any) {

    this.filteredRoles = this.uspRoles

    if (nameValue && nameValue !== '') {
      this.filteredRoles = this.uspRoles
      this.filteredRoles = this.filterpipe.transform('RoleName', 'NAMEFILTER', this.filteredRoles, nameValue);
    }
    this.itemResource = new DataTableResource(this.filteredRoles);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredRoles);
  }

  radioValue(event) {
    if (event.target.value === 'ACTIVE') {
      this.inactVal = false;
      this.actvVal = true
    } else {
      this.inactVal = true;
      this.actvVal = false

    }
  }



  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }



  openModel(content: any, isAddNew: any) {
    this.isAddNew = isAddNew
    this.modalReference = this.modalService.open(content);
  }


  closeForm() {
    this.modalReference.close();
  }

  rowItemLoad(event, model) {
    this.rowItem = event.row.item
    this.openModel(model, false)
  }

  viewUserDetails() {
    this.selectedIndex = 2;
  }


  saveSuccess() {
    this.getRoles('', true);
  }

}
