import { NgModule, Inject } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IAppConfig } from '../../app-config/app-config.interface';
import { APP_CONFIG } from '../../app-config/app-config.constants';
import { UserManagementComponent } from './user-settings/user-management/user-management.component';
import { RoleManagementComponent } from './user-settings/role-management/role-management.component';
import { UserDetailsComponent } from './user-settings/user-management/user-details/user-details.component';
import { UserRoleComponent } from './user-settings/role-management/user-role/user-role.component';
import { ExceToolbarService } from '../common/exce-toolbar/exce-toolbar.service';
import { TranslateService } from '@ngx-translate/core';

const routes: Routes = [
  { path: 'role-management', component: RoleManagementComponent },
  { path: 'user-management', component: UserManagementComponent },
  { path: 'user-details', component: UserDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UssAdminRoutingModule {

  constructor(
    private translate: TranslateService,
    private exceToolbarService: ExceToolbarService,
    @Inject(APP_CONFIG) private config: IAppConfig
  ) {
    const langs = [];
    this.config.LANGUAGES.map(ln => {
      langs.push(ln.ID);
    })
    this.translate.addLangs(langs);
    translate.setDefaultLang(this.config.LANGUAGE_DEFAULT.ID);

    const lang = this.exceToolbarService.getSelectedLanguage();
    this.translate.use(lang.id);
    this.exceToolbarService.langUpdated.subscribe(
      (res) => {
        this.translate.use(res.id);
      }
    );
  }
}
export const UssAdminRoutingComponents = [
  UserManagementComponent,
  RoleManagementComponent,
  UserRoleComponent,
  UserDetailsComponent
]
