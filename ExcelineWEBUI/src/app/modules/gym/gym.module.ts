import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GymHomeComponent } from './gym-home/gym-home.component';
import { GymRoutingModule } from './gym-routing.module';
import { ExceCommonModule } from '../common/exce-common.module';

@NgModule({
  imports: [
    CommonModule,
    ExceCommonModule,
    GymRoutingModule
  ],
  declarations: [GymHomeComponent]
})
export class GymModule { }
