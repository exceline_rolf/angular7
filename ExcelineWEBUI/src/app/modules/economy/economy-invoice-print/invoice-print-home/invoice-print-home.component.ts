import { Component, OnInit } from '@angular/core';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';

@Component({
  selector: 'app-invoice-print-home',
  templateUrl: './invoice-print-home.component.html',
  styleUrls: ['./invoice-print-home.component.scss']
})
export class InvoicePrintHomeComponent implements OnInit {

  constructor(
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ECONOMY.InvoicePrintC',
      url: '/economy/invoice-print-home'
    });
  }

  ngOnInit() {
  }

}
