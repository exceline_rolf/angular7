import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicePrintHomeComponent } from './invoice-print-home.component';

describe('InvoicePrintHomeComponent', () => {
  let component: InvoicePrintHomeComponent;
  let fixture: ComponentFixture<InvoicePrintHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicePrintHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicePrintHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
