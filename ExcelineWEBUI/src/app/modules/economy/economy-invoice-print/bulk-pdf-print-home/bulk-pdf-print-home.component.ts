import { Component, OnInit } from '@angular/core';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';

@Component({
  selector: 'app-bulk-pdf-print-home',
  templateUrl: './bulk-pdf-print-home.component.html',
  styleUrls: ['./bulk-pdf-print-home.component.scss']
})
export class BulkPdfPrintHomeComponent implements OnInit {

  constructor(
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ECONOMY.SystemPrintC',
      url: '/economy/bulk-pdf-print-home'
    });
  }

  ngOnInit() {
  }

}
