import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapPaymentsComponent } from './map-payments.component';

describe('MapPaymentsComponent', () => {
  let component: MapPaymentsComponent;
  let fixture: ComponentFixture<MapPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
