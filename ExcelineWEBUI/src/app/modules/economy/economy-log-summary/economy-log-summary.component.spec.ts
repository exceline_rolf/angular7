import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EconomyLogSummaryComponent } from './economy-log-summary.component';

describe('EconomyLogSummaryComponent', () => {
  let component: EconomyLogSummaryComponent;
  let fixture: ComponentFixture<EconomyLogSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EconomyLogSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EconomyLogSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
