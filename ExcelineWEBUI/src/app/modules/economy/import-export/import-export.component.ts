import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { TranslateService } from '@ngx-translate/core';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { ExceEconomyService } from './../services/exce-economy.service';
import { Extension } from 'app/shared/Utills/Extensions';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import * as moment from 'moment';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { error } from 'util';
import { UsbModalRef } from '../../../shared/components/us-modal/modal-ref';

@Component({
  selector: 'app-import-export',
  templateUrl: './import-export.component.html',
  styleUrls: ['./import-export.component.scss']
})
export class ImportExportComponent implements OnInit, OnDestroy {
  OCRImportResult: any;
  ocrMessage: string;
  showOCRMessage: boolean = false;
  OCRSummary: any;
  OCRFilePath: any;
  uploadedFile: any;
  fileUploadEvent: any;
  locale: string;
  searchForm: FormGroup;
  atgForm: FormGroup;
  ocrImportHistoryList: any;
  generateATGView: UsbModalRef;
  importOCRView: UsbModalRef;
  filelpath: any;
  fileType: any = 0;
  atgSummary: any;
  atgFileLog: any;
  statusMessage: any;
  downloadEnabled: boolean = false;
  errorMessages: any[] = [];
  ocrLog: any[] = [];
  showMessage: boolean = false;
  processStatus: any = -1; //-1: start, 0:file selected, 1:uploaded, 2:imported
  ocrFileType: string = 'OCR';

  constructor(
    private modalService: UsbModal,
    private fb: FormBuilder,
    private economyService: ExceEconomyService,
    private toolBarService: ExceToolbarService,
    private exceBreadcrumbService: ExceBreadcrumbService,
    private translateService: TranslateService,
    private http: HttpClient
  ) {
    this.exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ECONOMY.ImportExportC',
      url: '/economy/import-export'
    });
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      'startDate': [null],
      'endDate': [null]
    });

    this.atgForm = this.fb.group({
      'startDate': [null],
      'endDate': [null],
      'sendingNo': [null]
    });

    this.searchForm.patchValue({
      startDate: { date: Extension.GetFormatteredDate(this.dateAdd(new Date(), -1, 'months')) },
      endDate: { date: Extension.GetFormatteredDate(new Date()) }
    });

    this.atgForm.patchValue({
      startDate: { date: Extension.GetFormatteredDate(this.dateAdd(new Date(), 8, 'days')) }
    })

    this.getOCRImportHistory();



    const language = this.toolBarService.getSelectedLanguage();
    console.log(language)
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolBarService.langUpdated.subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
  }

  dateAdd(startDate: any, count: any, interval: string): Date {
    return moment(startDate).add(count, interval).toDate()
  }

  getOCRImportHistory() {
    let fromDate: any = this.manipulatedate(this.searchForm.value.startDate);
    let toDate: any = this.manipulatedate(this.searchForm.value.endDate);
    this.economyService.getOCRImportHistory(fromDate, toDate).subscribe(
      result => {
        if (result) {
          this.ocrImportHistoryList = result.Data;
        }
      }
    );
  }

  setFileType(type: any) {
    if (type === 'ATG') {
      this.fileType = 0;
    } else if (type === 'CAN') {
      this.fileType = 2;
    }
  }

  getATGSummary() {
    this.showMessage = false;
    this.statusMessage = '';
    if (this.atgForm.value.endDate) {
      let atgStartDueDate: any = this.manipulatedate(this.atgForm.value.startDate);
      let atgEndDueDate: any = this.manipulatedate(this.atgForm.value.endDate);
      let sendingNo: any = '';

      if (this.fileType === 2) {
        if (!this.atgForm.value.sendingNo) {
          let message: any;
          this.translateService.get('ECONOMY.AddSendingNo').subscribe(value => message = value);
          this.statusMessage = message;
          this.showMessage = true;
        } else {
          this.economyService.getATGSummary(this.fileType, atgStartDueDate, atgEndDueDate, sendingNo).subscribe(
            result => {
              if (result) {
                this.atgSummary = result.Data;
                this.errorMessages = this.atgSummary.LogRecords;
              }
            }
          );
        }
      } else {
        if (moment(atgStartDueDate).isAfter(moment(this.dateAdd(new Date(), 7, 'days')))) {
          this.economyService.getATGSummary(this.fileType, atgStartDueDate, atgEndDueDate, sendingNo).subscribe(
            result => {
              if (result) {
                this.atgSummary = result.Data;
                this.errorMessages = this.atgSummary.LogRecords;
              }
            }
          );
        } else {
          let message: any;
          this.translateService.get('ECONOMY.rangeError').subscribe(value => message = value);
          this.statusMessage = message;
          this.showMessage = true;
        }
      }
    } else {
      let message: any;
      this.translateService.get('ECONOMY.endDateError').subscribe(value => message = value);
      this.statusMessage = message;
      this.showMessage = true;
    }
  }

  generateATGFile() {
    this.showMessage = false;
    this.statusMessage = '';
    this.downloadEnabled = false;
    if (this.atgForm.value.endDate) {
      let atgStartDueDate: any = this.manipulatedate(this.atgForm.value.startDate);
      let atgEndDueDate: any = this.manipulatedate(this.atgForm.value.endDate);
      let sendingNo: any = '';

      if (this.fileType === 2) {
        if (!this.atgForm.value.sendingNo) {
          let message: any;
          this.translateService.get('ECONOMY.AddSendingNo').subscribe(value => message = value);
          this.statusMessage = message;
          this.showMessage = true;
        } else {
          this.economyService.generateATGFile(this.fileType, atgStartDueDate, atgEndDueDate, sendingNo).subscribe(
            result => {
              if (result) {
                this.atgFileLog = result.Data;
                this.filelpath = this.atgFileLog.FilePath;
                if (this.filelpath) {
                  let message: any;
                  this.translateService.get('ECONOMY.msgCancellationCreated').subscribe(value => message = value);
                  this.statusMessage = message;
                  this.showMessage = true;
                  this.downloadEnabled = true;
                }
              }
            }, error => {
              this.downloadEnabled = false;
              this.errorMessages.push(error);
            }
          );
        }
      } else {
        if (moment(atgStartDueDate).isAfter(moment(this.dateAdd(new Date(), 7, 'days')))) {
          this.economyService.generateATGFile(this.fileType, atgStartDueDate, atgEndDueDate, sendingNo).subscribe(
            result => {
              if (result) {
                this.atgFileLog = result.Data;
                this.filelpath = this.atgFileLog.FilePath;
                if (this.filelpath) {
                  let message: any;
                  this.translateService.get('ECONOMY.msgATGCreated').subscribe(value => message = value);
                  this.statusMessage = message;
                  this.showMessage = true;
                  this.downloadEnabled = true;
                }
              }
            }, error => {
              this.downloadEnabled = false;
              this.errorMessages.push(error);
            }
          );
        } else {
          let message: any;
          this.translateService.get('ECONOMY.rangeError').subscribe(value => message = value);
          this.statusMessage = message;
          this.showMessage = true;
        }
      }
    } else {
      let message: any;
      this.translateService.get('ECONOMY.endDateError').subscribe(value => message = value);
      this.statusMessage = message;
      this.showMessage = true;
    }
  }

  downloadFile() {
    // this.filelpath = 'E:\\Ami.txt'
    if (this.filelpath) {
      let fileName = this.filelpath.split("\\");
      this.economyService.getFile(this.filelpath).subscribe(
        fileData => {
          let b: any = new Blob([fileData], { type: 'text/plain' });
          var url = window.URL.createObjectURL(b);
          var a = document.createElement('a');
          document.body.appendChild(a);
          a.setAttribute('style', 'display: none');
          a.href = url;
          a.download = fileName[fileName.length - 1];
          a.click();
          window.URL.revokeObjectURL(url);
          a.remove();
        }
      );
    } else {
      let message: any;
      this.translateService.get('ECONOMY.NoFileFound').subscribe(value => message = value);
      this.statusMessage = message;
    }
  }

  manipulatedate(dateInput: any) {
    if (dateInput.date != undefined) {
      let tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }

  openGenerateATGModal(content: any) {
    this.showMessage = false;
    this.statusMessage = '';
    this.downloadEnabled = false;
    this.atgFileLog = null;
    this.generateATGView = this.modalService.open(content, { width: '800' });
  }

  openImportOCRModal(content: any) {
    this.showOCRMessage = false;
    this.ocrMessage = '';
    this.OCRFilePath = null;
    this.processStatus = -1;
    this.OCRImportResult = null;
    this.OCRSummary = null;
    this.importOCRView = this.modalService.open(content, { width: '800' });
  }

  fileUploaded(event: any) {
    this.showOCRMessage = false;
    this.ocrMessage = '';
    if (event) {
      this.fileUploadEvent = event;
      this.processStatus = 0;
    } else {
      this.processStatus = -1;
    }
  }

  uploadFile() {
    this.showOCRMessage = false;
    this.ocrMessage = '';
    this.OCRFilePath = null;
    this.processStatus = -1;
    let fileList: FileList = this.fileUploadEvent.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append('uploadFile', file, file.name);

      this.economyService.uploadFile(formData).subscribe(
        result => <any> {
          if (result) {
            if (result.Data === 'DUPLICATE') {
              let message: string = '';
              this.translateService.get('ECONOMY.MsgduplicateFileUploda').subscribe(value => message = value);
              this.ocrMessage = message;
              this.showOCRMessage = true;
            } else {
              this.OCRFilePath = result.Data;
              this.processStatus = 1;
            }
          }
        }
      );
    }
  }

  viewFileSummary() {
    if (this.OCRFilePath) {
      this.economyService.getOCRSummary(this.OCRFilePath).subscribe(
        result => {
          if (result) {
            this.OCRSummary = result.Data;
            this.ocrLog = this.OCRSummary.Messages;
          }
        }
      );
    } else {
      let message: string = '';
      this.translateService.get('ECONOMY.NoFileFound').subscribe(value => message = value);
      this.ocrMessage = message;
      this.showOCRMessage = true;
    }
  }

  setOCRFileType(type: any) {
    this.ocrFileType = type;
  }

  importOCRFile() {
    if (this.OCRFilePath) {
      this.economyService.importOCR(this.OCRFilePath, this.ocrFileType).subscribe(
        result => {
          if (result) {
            this.OCRImportResult = result.Data;
            let message: string = '';
            switch (this.OCRImportResult.Status) {
              case 1:
                this.translateService.get('ECONOMY.ImportSuccess').subscribe(value => message = value);
                this.ocrMessage = message;
                this.showOCRMessage = true;
                break;

              case 2:
                this.translateService.get('ECONOMY.FewNotImported').subscribe(value => message = value);
                this.ocrMessage = message;
                this.showOCRMessage = true;
                this.processStatus = 2;
                break;

              case -1:
                this.translateService.get('ECONOMY.ImportError').subscribe(value => message = value);
                this.ocrMessage = message;
                this.showOCRMessage = true;
                break;

              case -2:
                this.translateService.get('ECONOMY.MsgduplicateFileUploda').subscribe(value => message = value);
                this.ocrMessage = message;
                this.showOCRMessage = true;
                break;
            }
            //add the log
            this.ocrLog = this.OCRImportResult.Messages;
          }
        }
      );
    } else {
      let message: string = '';
      this.translateService.get('ECONOMY.NoFileFound').subscribe(value => message = value);
      this.ocrMessage = message;
      this.showOCRMessage = true;
    }
  }

  getErrorFiles(){
    // this.filelpath = 'E:\\Ami.txt'
    if (this.OCRFilePath) {
      let fileName = this.OCRFilePath.split("\\");
      this.economyService.getErrorFiles(this.OCRFilePath).subscribe(
        fileData => {
          // let b: any = new Blob([fileData], { type: 'application/octet-stream' });
          // var url = window.URL.createObjectURL(b);
          // var a = document.createElement('a');
          // document.body.appendChild(a);
          // a.setAttribute('style', 'display: none');
          // a.href = url;
          // a.download = "tet.zip";//   fileName[this.OCRFilePath.length - 1];
          // a.click();
          // window.URL.revokeObjectURL(url);
          // a.remove();
        }
      );
    } else {
      let message: any;
      this.translateService.get('ECONOMY.NoFileFound').subscribe(value => message = value);
      this.statusMessage = message;
    }
  }

  ngOnDestroy(): void {
    if (this.generateATGView) {
      this.generateATGView.close();
    }
    if (this.importOCRView) {
      this.importOCRView.close();
    }
  }

}
