import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { ShopHomeService } from '../../../shop/shop-home/shop-home.service';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import {ExceEconomyService} from '../../services/exce-economy.service';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { UsbModalRef } from '../../../../shared/components/us-modal/modal-ref';


@Component({
  selector: 'app-keymanagement',
  templateUrl: './KeyManagement.component.html',
  styleUrls: ['./KeyManagement.component.scss']

})
export class KeyManagementComponent implements OnInit, OnDestroy {

  branchId: number;
  salePointId: number;
  datalist: any;
  answer: string;
  isLoading: boolean;
  generateMessageView: UsbModalRef;


  constructor(
    private exceBreadcrumbService: ExceBreadcrumbService,
    private shopHomeService: ShopHomeService,
    private exceLoginService: ExceLoginService,
    private exceEconomyService: ExceEconomyService,
    private modalService: UsbModal
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ECONOMY.KeyManagementC',
      url: '/economy/kasselov/KeyManagement'
    });
  }




  ngOnInit() {
    this.branchId =  this.exceLoginService.SelectedBranch.BranchId;
    this.keysByBranch(this.branchId );
  }

  ngOnDestroy() {
    if (this.generateMessageView) {
      this.generateMessageView.close();
    }
  }

  getKeyData() {
    this.salePointId = this.shopHomeService.$shopLoginData.SalePoint.ID;
    this.exceEconomyService.getKeyForSalePoint(this.salePointId, this.branchId);
  }


  exportKey() {
    this.isLoading = false;
    this.exceEconomyService.getPublicPEMKeyForSalePoint().subscribe(
      result => { if (result && result.Data) {
        this.answer = result.Data;
        this.answer = this.answer.replace('-----BEGIN PUBLIC KEY-----', '');
        this.answer = this.answer.replace('-----END PUBLIC KEY-----', '');
        this.isLoading = true;
      }
    });

  }

  keysByBranch(branchId: number ) {
    this.exceEconomyService.GetSalePointsByBranch(branchId).subscribe(
      result => {
        if (result && result.Data) {
          this.datalist = result.Data;
        }
      });
  }



  // generatePublicKeyView(){}

  openGenerateMessageModal(content: any, input) {
    this.salePointId = input;

    this.generateMessageView = this.modalService.open(content, { width: '600' });
  }

}
