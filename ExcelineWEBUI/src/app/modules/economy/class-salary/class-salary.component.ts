import { Component, OnInit } from '@angular/core';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';

@Component({
  selector: 'app-class-salary',
  templateUrl: './class-salary.component.html',
  styleUrls: ['./class-salary.component.scss']
})
export class ClassSalaryComponent implements OnInit {

  constructor(
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ECONOMY.SalaryReportC',
      url: '/economy/class-salary'
    });
  }

  ngOnInit() {
  }

}
