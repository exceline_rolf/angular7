import { NgModule, Inject } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IAppConfig } from '../../app-config/app-config.interface';
import { APP_CONFIG } from '../../app-config/app-config.constants';

import { ExceToolbarService } from '../common/exce-toolbar/exce-toolbar.service';
import { ShopHomeComponent } from './shop-home/shop-home.component';
import { ShopItemsComponent } from './shop-home/shop-items/shop-items.component';
import { ShopArticlesComponent } from './shop-home/shop-articles/shop-articles.component';
import { ShopPaymentComponent } from './shop-home/shop-payment/shop-payment.component';
import { ShortcutKeyComponent } from './shop-home/shop-articles/shortcut-key/shortcut-key.component';
import { ArticleListComponent } from './shop-home/shop-articles/article-list/article-list.component';
import { CampaignListComponent } from './shop-home/shop-articles/campaign-list/campaign-list.component';
import { SalesHistoryComponent } from './shop-home/sales-history/sales-history.component';
import { CashWithdrawalComponent } from './shop-home/cash-withdrawal/cash-withdrawal.component';
import { DailySettlementComponent } from './shop-home/daily-settlement/daily-settlement.component';
import { TranslateService } from '@ngx-translate/core';
import { ShopHomeLayoutComponent } from 'app/modules/shop/shop-home/shop-home-layout/shop-home-layout.component';

const routes: Routes = [
  { path: '', component: ShopHomeLayoutComponent },
  { path: 'shop-payment', component: ShopPaymentComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule {
  constructor(
    private translate: TranslateService,
    private exceToolbarService: ExceToolbarService,
    @Inject(APP_CONFIG) private config: IAppConfig
  ) {
    const langs = [];
    this.config.LANGUAGES.map(ln => {
      langs.push(ln.ID);
    })
    this.translate.addLangs(langs);
    translate.setDefaultLang(this.config.LANGUAGE_DEFAULT.ID);

    const lang = this.exceToolbarService.getSelectedLanguage();
    this.translate.use(lang.id);
    this.exceToolbarService.langUpdated.subscribe(
      (res) => {
        this.translate.use(res.id);
      }
    );
  }


}
export const ShopRoutingComponents = [
  ShopHomeComponent,
  ShopItemsComponent,
  ShopArticlesComponent,
  ShopPaymentComponent,
  ShortcutKeyComponent,
  ArticleListComponent,
  CampaignListComponent,
  SalesHistoryComponent,
  CashWithdrawalComponent,
  DailySettlementComponent,
  ShopHomeLayoutComponent
]

