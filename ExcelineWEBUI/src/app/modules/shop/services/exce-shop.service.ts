import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../shared/services/common-http.service';
import { ConfigService } from '@ngx-config/core';
import { Observable, Subject } from 'rxjs';
import { ExceLoginService } from '../../../modules/login/exce-login/exce-login.service';

@Injectable()
export class ExceShopService {
  private memberApiURL: any;
  private shopApiUrl: string;
  private branchId: number;

  public refreshArticleListEvent = new Subject<any>();

  constructor(private commonHttpService: CommonHttpService, private config: ConfigService, private exceLoginService: ExceLoginService) {
    this.shopApiUrl = this.config.getSettings('EXCE_API.SHOP');
    this.memberApiURL = this.config.getSettings('EXCE_API.MEMBER');
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }

  getGiftVoucherSaleSummary(params: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopVouchers/GetGiftVoucherSaleSummary?fromDate='
      + params.fromDate + '&toDate=' + params.toDate + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getShopBillSummary(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Sales/GetShopBillSummary?fromDate='
      + params.fromDate + '&toDate=' + params.toDate + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetCategories(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetCategories?type=' + params.type + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  isCashDrawerOpen(machineName: string): Observable<any> {
    const url = this.shopApiUrl + 'ShopAccount/IsCashdrawerOpen?machinename=' + machineName;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetCategoryTypes(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetCategoryTypes?name=' + params.name + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SaveCategory(body: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/SaveCategory';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  GetGymEmployees(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetGymEmployees?branchId=' + this.branchId + '&searchText=' +
      params.searchText + '&isActive=' + params.isActive;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetInstructors(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetInstructors?branchId=' + this.branchId + '&searchText=' +
      params.searchText + '&isActive=' + params.isActive;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetTrainers(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetTrainers?branchId=' + this.branchId + '&searchText=' +
      params.searchText + '&isActive=' + params.isActive;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetMembers(body: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetMembers';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  SaveArticle(body: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/SaveArticle';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  GetArticles(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetArticles?branchId=' + this.branchId + '&articleType=' + params.articleType + '&keyWord='
      + params.keyWord + '&isActive=' + params.isActive + '&isFilterByBranch=' + params.isFilterByBranch;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetArticlesSearch(searchword: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetArticlesSearch?branchId=' + this.branchId + '&keyWord=' + searchword;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetReturnItemList(): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetReturnItemList?branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  AddReturnedItem(body: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/AddReturnedItem';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  EditReturnedItem(body: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/EditReturnedItem';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  AddEditUserHardwareProfile(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/AddEditUserHardwareProfile?branchId=' + this.branchId + '&hardwareId=' + params.hardwareId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetSelectedHardwareProfile(): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetSelectedHardwareProfile?branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  CheckEmployeeByEntNo(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/CheckEmployeeByEntNo?entityNO=' + params.entityNO;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  ValidateUserWithCard(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/ValidateUserWithCard?cardNumber=' + params.cardNumber;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  ValidateUser(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/ValidateUser?userName=' + params.userName + '&password=' + params.password;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetCountryDetails(): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetCountryDetails';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetMembersForShop(body: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetMembersForShop';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  SaveMember(body: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/SaveMember';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  GetMemberSearchCategory(): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetMemberSearchCategory?branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetMemberBasicInfo(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetMemberBasicInfo?memberId=' + params.memberId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  AddSessionValue(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/AddSessionValue?sessionKey=' + params.sessionKey + '&value=' + params.value + '&branchId=' + params.branchId + '&salepointId=' + params.salepointId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetReconciliationSession(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetReconciliationSession?sessionKey=' + params.sessionKey +
      '&dailySettlementID=' + params.dailySettlementID;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetSessionValue(sessionKey: any, user: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetSessionValue?sessionKey=' + sessionKey;
    return this.commonHttpService.GetSessionValue(url, {
      method: 'GET',
      auth: true
    }, user, true);
  }

  GetBranches(): Observable<any> {
    const url = this.shopApiUrl + 'Shop/GetBranches?branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SavePostalArea(body: any): Observable<any> {
    const url = this.shopApiUrl + 'Shop/SavePostalArea';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  GetDailySales(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Sales/GetDailySales?salesDate=' + params.salesDate + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetSalesPaymentList(): Observable<any> {
    const url = this.shopApiUrl + 'Sales/GetSalesPaymentList';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetMemberEconomyBalances(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Sales/GetMemberEconomyBalances?memberId=' + params.memberId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  // GetShopBillSummary(params: any): Observable<any> {
  //   const url = this.shopApiUrl + 'Sales/GetShopBillSummary?fromDate='
  //     + params.fromDate + '&toDate=' + params.toDate + '&branchId=' + this.branchId;
  //   return this.commonHttpService.makeHttpCall(url, {
  //     method: 'GET',
  //     auth: true
  //   })
  // }

  GetShopSaleBillDetail(params: any): Observable<any> {
    const url = this.shopApiUrl + 'Sales/GetShopSaleBillDetail?saleId=' + params.saleId + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetSalesPoint(): Observable<any> {
    const url = this.shopApiUrl + 'Sales/GetSalesPoint?branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetSalesPointByMachineName(machineName: any): Observable<any> {
    const url = this.shopApiUrl + 'Sales/GetSalesPointByMachineName?branchId=' + this.branchId + '&machineName=' + machineName;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SavePointOfSale(body: any): Observable<any> {
    const url = this.shopApiUrl + 'Sales/SavePointOfSale';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  ManageMemberShopAccount(body: any): Observable<any> {
    body.BranchId = this.branchId;
    const url = this.shopApiUrl + 'ShopAccount/ManageMemberShopAccount';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  PayCreditNote(body: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopAccount/PayCreditNote';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  AddShopAccount(body: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopAccount/AddShopAccount';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  GetMemberShopAccount(params: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopAccount/GetMemberShopAccount?memberId=' + params.memberId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetMemberShopAccountsForEntityType(params: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopAccount/GetMemberShopAccountsForEntityType?memberId=' + params.memberId
      + '&entityType=' + params.entityType;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetDailySettlements(params: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopAccount/GetDailySettlements?branchId=' + this.branchId + '&salePointId=' + params.salePointId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  AddDailySettlements(body: any): Observable<any> {
    body.BranchId = this.branchId;
    body.CreatedUser = this.exceLoginService.CurrentUser.username;
    const url = this.shopApiUrl + 'ShopAccount/AddDailySettlements';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  ViewDailySettlementReport(body: any): Observable<any> {
    body.BranchId = this.branchId;
    const url = this.shopApiUrl + 'ShopAccount/ViewDailySettlementReport';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  SaveWithdrawal(body: any): Observable<any> {
    body.BranchId = this.branchId;
    const url = this.shopApiUrl + 'ShopAccount/SaveWithdrawal';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  GetInvoicesForArticleReturn(memberID: number, branchId: number, articleNo: any, shopOnly: number): Observable<any> {
    const url = this.shopApiUrl + 'ShopAccount/GetInvoicesForArticleReturn?memberID=' + memberID + '&branchID=' + branchId +
      '&articleNo=' + articleNo + '&shopOnly=' + shopOnly;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true
    })
  }

  SaveOpenCashRegister(body: any): Observable<any> {
    body.BranchId = this.branchId;
    const url = this.shopApiUrl + 'ShopAccount/SaveOpenCashRegister';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  GetWithdrawalByMemberId(params: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopAccount/GetWithdrawalByMemberId?startDate=' + params.startDate + '&endDate=' + params.endDate
      + '&type=' + params.type + '&memberId=' + params.memberId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  AddShopSales(body: any): Observable<any> {
    body.LoggedBranchID = this.branchId;
    const url = this.shopApiUrl + 'ShopAccount/AddShopSales';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  GetDiscountList(params: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopAccount/GetDiscountList?branchId=' + this.branchId + '&discountType=' + params.discountType;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  // GetGymSettings(params: any): Observable<any> {
  //   const url = this.shopApiUrl + 'ShopSettings/GetGymSettings?branchId=' + this.branchId + '&gymSettingType' + params.gymSettingType;
  //   return this.commonHttpService.makeHttpCall(url, {
  //     method: 'GET',
  //     auth: true
  //   })
  // }

  GetSelectedGymSettings(body: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopSettings/GetSelectedGymSettings';
    body.BranchId = this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  GetConfigSettings(params: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopSettings/GetConfigSettings?type=' + params.type;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  UpdateSettingForUserRoutine(params: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopSettings/UpdateSettingForUserRoutine?key=' + params.key + '&value' + params.value;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetVouchersList(params: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopVouchers/GetVouchersList?branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  AddVoucher(body: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopVouchers/AddVoucher';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  EditVoucher(body: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopVouchers/EditVoucher';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  GetNextGiftVoucherNumber(params: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopVouchers/GetNextGiftVoucherNumber?seqId=' + params.seqId + '&subSeqId=' + params.subSeqId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetGiftVoucherSaleSummary(params: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopVouchers/GetGiftVoucherSaleSummary?fromDate' + params.fromDate + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  ValidateGiftVoucherNumber(params: any): Observable<any> {
    const url = this.shopApiUrl + 'ShopVouchers/ValidateGiftVoucherNumber?voucherNumber=' + params.voucherNumber + '&payment=' + params.payment + '&branchID=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetGiftVoucherDetail(): Observable<any> {
    const url = this.shopApiUrl + 'ShopVouchers/GetGiftVoucherDetail';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  UpdateMemberInstallment(installment: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/UpdateMemberInstallment';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: installment
    })
  }

  RegisterInstallmentPayment(request: any): Observable<any> {
    request.LoggedbranchID = this.branchId;
    const url = this.memberApiURL + '/Contract/RegisterInstallmentPayment';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: request
    })
  }

  AddInvoicePayment(request: any): Observable<any> {
    request.LoggedbranchID = this.branchId;
    const url = this.memberApiURL + '/Contract/AddInvoicePayment';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: request
    })
  }

  AddCreditNoteForBooking(request: any): Observable<any> {
    request.BranchId = this.branchId;
    const url = this.shopApiUrl + '/Shop/AddCreditNoteForBooking';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: request
    })
  }


  AddShopXMLLog(xMLData: any): Observable<any>  {
    const url = this.shopApiUrl + 'Shop/AddShopXMLLog';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: xMLData
    })
  }

  CheckIfReceiptIsValidForPrint(invoiceNo: string, branchId: number): Observable<any> {
    const url = this.shopApiUrl + 'shop/CheckIfReceiptIsValidForPrint?invoiceNo=' + invoiceNo + '&branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

}
