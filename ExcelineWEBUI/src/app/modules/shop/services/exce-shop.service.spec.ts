import { TestBed, inject } from '@angular/core/testing';

import { ExceShopService } from './exce-shop.service';

describe('ExceShopService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceShopService]
    });
  });

  it('should be created', inject([ExceShopService], (service: ExceShopService) => {
    expect(service).toBeTruthy();
  }));
});
