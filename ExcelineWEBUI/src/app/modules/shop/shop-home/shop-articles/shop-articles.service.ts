import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable()
export class ShopArticlesService {
  private articlelList: any[];
  private articlelType = 'ALL';
  private artleCategory = 'ALL';
  private activity = 'ALL';
  private articleName: any;
  private vendorName: any;
  private isCustomerPrice: Boolean;
  private searchText: string;

  private filterParams = {}
  private compaignListRoot: any[];

  private articlelListSource = new Subject<any>();
  public allArticles = this.articlelListSource.asObservable();

  private filterChange = new Subject<any>();
  public filterArticles = this.filterChange.asObservable();

  public priceTypeChange = new Subject<any>();

  public searchByKeyword = new Subject<any>();

  constructor() { }

  setArticleLists(articles) {
    this.articlelList = articles;
    this.articlelListSource.next(articles);
  }

  getAllArticles(): any {
    if (this.articlelList !== null) {
      return this.articlelList;
    } else {
      return null;
    }
  }

  public get $compaignListRoot(): any {
    return this.compaignListRoot;
  }

  public set $compaignListRoot(compaignListRoot: any) {
    this.compaignListRoot = compaignListRoot;
  }

  public get $articlelType(): any {
    return this.articlelType;
  }

  public set $articlelType(articlelType: any) {
    this.articlelType = articlelType;
    this.filterChange.next({
      articleName: this.articleName,
      vendorName: this.vendorName,
      articlelType: this.articlelType,
      artleCategory: this.artleCategory,
      activity: this.activity
    });
  }

  public get $artleCategory(): any {
    return this.artleCategory;
  }

  public set $artleCategory(artleCategory: any) {
    this.artleCategory = artleCategory;
    this.filterChange.next({
      articleName: this.articleName,
      vendorName: this.vendorName,
      articlelType: this.articlelType,
      artleCategory: this.artleCategory,
      activity: this.activity
    });
  }

  public get $activity(): any {
    return this.activity;
  }

  public set $activity(value: any) {
    this.activity = value;
    this.filterChange.next({
      articleName: this.articleName,
      vendorName: this.vendorName,
      articlelType: this.articlelType,
      artleCategory: this.artleCategory,
      activity: this.activity
    });
  }

  public get $articleName(): any {
    return this.articleName;
  }

  public set $articleName(value: any) {
    this.articleName = value;
    this.filterChange.next({
      articleName: this.articleName,
      vendorName: this.vendorName,
      articlelType: this.articlelType,
      artleCategory: this.artleCategory,
      activity: this.activity
    });
  }


  public get $vendorName(): any {
    return this.vendorName;
  }

  public set $vendorName(value: any) {
    this.vendorName = value;
    this.filterChange.next({
      articleName: this.articleName,
      vendorName: this.vendorName,
      articlelType: this.articlelType,
      artleCategory: this.artleCategory,
      activity: this.activity
    });
  }

  public get $isCustomerPrice(): Boolean {
    return this.isCustomerPrice;
  }

  public set $isCustomerPrice(value: Boolean) {
    this.isCustomerPrice = value;
    this.priceTypeChange.next(value)
  }

  public get $searchText(): string {
    return this.searchText;
  }

  public set $searchText(value: string) {
    this.searchText = value;
    this.searchByKeyword.next(this.searchText);
  }


}
