import { Extension } from 'app/shared/Utills/Extensions';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { ExceToolbarService } from '../../../common/exce-toolbar/exce-toolbar.service';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';

import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { ExceShopService } from '../../services/exce-shop.service';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { IMyDpOptions } from '../../../../shared/components/us-date-picker/interfaces';
import { Guid } from '../../../../shared/services/guid.service';
import { AmountConverterPipe } from '../../../../shared/pipes/amount-converter.pipe';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { ShopHomeService } from './../shop-home.service';
import { ExceSessionService } from 'app/shared/services/exce-session.service';
import { Observable } from 'rxjs';

const now = new Date();
@Component({
  selector: 'sales-history',
  templateUrl: './sales-history.component.html',
  styleUrls: ['./sales-history.component.scss']
})
export class SalesHistoryComponent implements OnInit, OnDestroy {
  timerSub: any;
  timer: any;
  sales = [];
  filterdSales = [];
  isShopSales = true;
  today: any;
  salesHistorySearchForm: FormGroup;
  itemCount = 0;
  filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  receiptDetails = [];
  saleItem: any;
  saleItemTotal: any;
  locale: string;
  modalRefence: any;
  branchId: any;
  jalla = true;

  public dpStartDate;
  public dpEndDate;
  fromDatePickerOptions: any;
  //fromDatePickerOptions: IMyDpOptions = {
    //disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()+1 },
  //};
  toDatePickerOptions: any;
  //toDatePickerOptions: IMyDpOptions = {
    //disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()+1 }
  //};

  salesDefs: any;

  names = {
    SaleDate: 'Salgsdato',
    SaleTime: 'Tidspunkt',
    Amount: 'Sum',
    GiftVoucherNumber: 'Gavekortnummer',
    GiftVoucherAmount: 'Verdi',
    Balance: 'Saldo',
    PayMode: 'Betalingstype',
    PaymentType: 'Betalingstype',
    MemberName: 'Kundenavn',
    CreatedUser: 'Ansatt'
  }

  colNamesNo = {
    SaleDate: 'Salgsdato',
    SaleTime: 'Tidspunkt',
    Amount: 'Sum',
    GiftVoucherNumber: 'Gavekortnummer',
    GiftVoucherAmount: 'Verdi',
    Balance: 'Saldo',
    PayMode: 'Betalingstype',
    PaymentType: 'Betalingstype',
    MemberName: 'Kundenavn',
    CreatedUser: 'Ansatt'
  }

  colNamesEn = {
    SaleDate: 'Date',
    SaleTime: 'Time',
    Amount: 'Amount',
    GiftVoucherNumber: 'Giftcard number',
    GiftVoucherAmount: 'Giftcard value',
    Balance: 'Balance',
    PayMode: 'Paymode',
    PaymentType: 'Payment type',
    MemberName: 'Member name',
    CreatedUser: 'Employee'
  }
  gridApi: any;
  gridColumnApi: any;

  @ViewChild('receiptDetailsModal') receiptDetailsModal: ElementRef;

  constructor(
    private modalService: UsbModal,
    private fb: FormBuilder,
    private shopService: ExceShopService,
    private datePipe: DatePipe,
    private amountConverterPipe: AmountConverterPipe,
    private exceLoginService: ExceLoginService,
    private shopHomeService: ShopHomeService,
    private exceMessageService: ExceMessageService,
    private toolbarService: ExceToolbarService,
    private exceSessionService: ExceSessionService
  ) {
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.salesHistorySearchForm
      = fb.group({
        'fromDate': [{ date: this.today }, Validators.required],
        'toDate': [{ date: this.today }, Validators.required],
      });
  }

  ngOnInit() {
    const disSi = new Date();
    disSi.setDate(disSi.getDate() + 1)
    this.fromDatePickerOptions = {
      disableSince: {year: disSi.getFullYear(), month: disSi.getMonth() + 1, day: disSi.getDate()}
    }
    this.toDatePickerOptions = {
      disableSince: {year: disSi.getFullYear(), month: disSi.getMonth() + 1, day: disSi.getDate()}
    }

    // **Language selecton and subscription - dateformat and amountconverter uses this **
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.toolbarService.langUpdated.subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );

    this.salesHistorySearchForm.patchValue({ fromDate: { date: this.today }, toDate: { date: this.today } });
    this.filterdSales = [];
    this.sales = [];

    this.salesDefs = [
      {headerName: this.names.SaleDate, field: 'SaleDate', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
        }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        const time = params.value.split('T');
        const date = time[0].split('-');
        return date[2] + '.' + date[1] + '.' + date[0];
      }},
      {headerName: this.names.SaleTime, field: 'SaleTime', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
      const time = params.value.split('T');
      const date = time[0].split('-');
      return date[0];
      }},
      {headerName: this.names.Amount, field: 'Amount', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.GiftVoucherNumber, field: 'GiftVoucherNumber', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.GiftVoucherAmount, field: 'GiftVoucherAmount', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.Balance, field: 'Balance', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.PayMode, field: 'PayMode', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.PaymentType, field: 'PaymentType', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.MemberName, field: 'MemberName', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.CreatedUser, field: 'CreatedUser', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: '', field: '', colId: 'Detaljer', suppressMenu: true, width: 50, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'align-content': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        return '<ng-template> <button class="btn btn-primary btn-icon-min" title="Detaljer"><span class="icon-detail"></span></button></ng-template>';
      }},
      {headerName: '', field: 'IsValidForPrint', colId: 'Print', suppressMenu: true, width: 80, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'align-content': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.data.IsValidForPrint === true || params.data.IsValidForPrint === undefined || params.data.IsValidForPrint === null ) {
        return '<ng-template> <button [disabled]="false" class="btn btn-primary btn-icon-min" title="Print"><span class="icon-print"></span></button></ng-template>';
      } else {
        return '<ng-template> <button disabled class="btn btn-primary btn-icon-min" title="Print"><span class="icon-print"></span></button></ng-template>';
      }
      }
    },
    ]
  }

  ngOnDestroy() {
    if (this.modalRefence) {
      this.modalRefence.close();
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    this.gridApi.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
    const columns = this.gridColumnApi.getAllColumns();
    columns.forEach(col => {
      switch (col.colId) {
        case 'Amount':
        if (this.isShopSales) {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        } else {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), false)
        }
        break;
        case 'GiftVoucherNumber':
        if (this.isShopSales) {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), false)
        } else {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        }
        break;
        case 'GiftVoucherAmount':
        if (this.isShopSales) {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), false)
        } else {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        }
        break;
        case 'Balance':
        if (this.isShopSales) {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), false)
        } else {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        }
        break;
        case 'PayMode':
        if (this.isShopSales) {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        } else {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), false)
        }
        break;
        case 'PaymentType':
        if (this.isShopSales) {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), false)
        } else {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        }
        break;
        case 'MemberName':
        if (this.isShopSales) {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), false)
        } else {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        }
        break;
        default:
        this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        break;
      }
    });
    this.gridApi.sizeColumnsToFit();
  }

  scroll(event) {
    // console.log(event)
  }

  searchTypeChange(isShopSales) {
    this.isShopSales = isShopSales;
    this.filterdSales = [];
    this.sales = [];
    const columns = this.gridColumnApi.getAllColumns();
    columns.forEach(col => {
      switch (col.colId) {
        case 'Amount':
        if (isShopSales) {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        } else {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), false)
        }
        break;
        case 'GiftVoucherNumber':
        if (isShopSales) {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), false)
        } else {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        }
        break;
        case 'GiftVoucherAmount':
        if (isShopSales) {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), false)
        } else {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        }
        break;
        case 'Balance':
        if (isShopSales) {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), false)
        } else {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        }
        break;
        case 'PayMode':
        if (isShopSales) {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        } else {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), false)
        }
        break;
        case 'PaymentType':
        if (isShopSales) {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), false)
        } else {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        }
        break;
        case 'MemberName':
        if (isShopSales) {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), false)
        } else {
          this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        }
        break;
        default:
        this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(col), true)
        break;
      }
    });
    this.gridApi.sizeColumnsToFit();
  }

  salesHistorySearch(value) {
    const fromDate = value.fromDate.date.year + '-' + value.fromDate.date.month + '-' + value.fromDate.date.day;
    const toDate = value.toDate.date.year + '-' + value.toDate.date.month + '-' + value.toDate.date.day;
    PreloaderService.showPreLoader();
    if (this.isShopSales) {

      this.shopService.getShopBillSummary({ fromDate: fromDate, toDate: toDate }).subscribe(
        (result) => {
        // this.sales = result.Data;
        // result.Data.forEach(element => {
        //   this.shopService.CheckIfReceiptIsValidForPrint(element.Ref, this.branchId).subscribe( res => {
        //     element.isValidForPrint = res.Data < 2 ? true : false;
        //  });
        // })
        this.filterdSales = result.Data;
        // this.gridApi.setRowData(this.filterdSales)
        // this.gridApi.redrawRows();
        PreloaderService.hidePreLoader();
      },
        error => {
          PreloaderService.hidePreLoader();
        }
        // () => { this.filterdSales = this.sales;
          // console.log('I RAN')
          // setTimeout(() => {this.gridApi.redrawRows(); }, 9000 );

        // }
        )
    } else {
      this.shopService.getGiftVoucherSaleSummary({ fromDate: fromDate, toDate: toDate }).subscribe(result => {
        this.sales = result.Data;
        this.filterdSales = this.sales;
        PreloaderService.hidePreLoader();
      },
        error => {
          PreloaderService.hidePreLoader();
        });
    }

  }

  cellClicked(event) {
    const column = event.colDef.colId;
    switch (column) {
      case 'Detaljer':
      this.viewReceiptDetails(this.receiptDetailsModal, event.data);
      break;
      case 'Print':
      if (event.data.IsValidForPrint) {
        this.printShopBill(event.data)
      }
      break;
    }
  }


  filterSalesHistory(paymentType?: any) {
    if (paymentType === 'ALL') {
      this.filterdSales = this.sales;
    } else {
      this.filterdSales = this.filterpipe.transform('PaymentCode', 'SELECT', this.sales, paymentType);
    }
  }

  onStartDateChange(date, end) {
    this.toDatePickerOptions = {
      disableUntil: date.date
    };
    end.showSelector = true;
  }

  viewReceiptDetails(content, item): void {
    this.modalRefence = this.modalService.open(content, { width: '1000' })
    this.saleItem = item;
    if (item.ShopSalesId) {
      PreloaderService.showPreLoader();
      this.shopService.GetShopSaleBillDetail({ saleId: item.ShopSalesId }).subscribe(result => {
        this.receiptDetails = result.Data.SalesItemList;
        this.receiptDetails.forEach(element => {
          element.PurchaseDate = new Date(element.PurchaseDate)
        });
        // this.itemCount = this.receiptDetails.length;
        // this.itemResource = new DataTableResource(this.receiptDetails);
        // this.itemResource.count().then(count => this.itemCount = count);
        this.saleItemTotal = result.Data.SalesItemList.reduce((a, b) => a + b.TotalAmount, 0);
        PreloaderService.hidePreLoader();


      },
        error => {
          PreloaderService.hidePreLoader();

        });
    }
  }

  printShopBill(sale): void {
    if (sale.ShopSalesId) {
      this.shopService.GetShopSaleBillDetail({ saleId: sale.ShopSalesId }).subscribe(result => {
        const saleDetail = result.Data;
        const saleList = saleDetail.SalesItemList;
        const paymentList = saleDetail.SalesPaymentList;

        const sessionKey = Extension.createGuid();
        let receiptText = '';
        receiptText = receiptText + 'MEMBER$' + saleDetail.CustId +
          '|INVNO$' + saleDetail.InvoiceNo +
          '|USER$' + this.exceLoginService.CurrentUser.username +
          '|DATE$' + this.datePipe.transform(sale.SaleDate, 'dd.MM.yyyy') + ' ' + sale.SaleTime + '|';

        if (saleList != null) {
          receiptText = receiptText + 'ITEM$';
          saleList.forEach(article => {
            receiptText = receiptText + article.ItemName + ':' +
              article.Quantity + ':' +
              this.amountConverterPipe.transform(article.DefaultAmount) + ':' +
              this.amountConverterPipe.transform(article.Discount) + ':' +
              this.amountConverterPipe.transform(article.VatRate) + ':' +
              this.amountConverterPipe.transform(article.TotalAmount) + ':' +
              this.amountConverterPipe.transform(article.GiftVoucherNumber) + ';';
          });

        }

        if (paymentList != null) {
          receiptText = receiptText + '|PAYMENT$';
          paymentList.forEach(payment => {

            let tempPaymentType: string;

            switch (payment.PaymentCode) {
              case 'CASH':
                tempPaymentType = 'Kontant';
                break;
              case 'CARD':
                tempPaymentType = 'Bankkort';
                break;
              case 'BANKTERMINAL':
                tempPaymentType = 'Bankkort';
                break;
              case 'VIPPS':
                tempPaymentType = 'Vipps';
                break;
              case 'CASHWITHDRAWA':
                tempPaymentType = 'Kontantuttak';
                break;
              case 'EMAILSMSINVOICE':
                tempPaymentType = 'Epost/SMS-Faktura';
                break;
              case 'OCR':
                tempPaymentType = 'OCR';
                break;
              case 'BANKSTATEMENT':
                tempPaymentType = 'Melding Bank';
                break;
              case 'DEBTCOLLECTION':
                tempPaymentType = 'Innfordring';
                break;
              case 'ONACCOUNT':
                tempPaymentType = 'Akonto';
                break;
              case 'NEXTORDER':
                tempPaymentType = 'Neste Ordre(' + payment.SaleDate + ')';
                break;
              case 'GIFTCARD':
                tempPaymentType = 'Gavekort';
                break;
              case 'INVOICE':
                tempPaymentType = 'Faktura';
                break;
              case 'SHOPINVOICE':
                tempPaymentType = 'shop Faktura';
                break;
              case 'PREPAIDBALANCE':
                tempPaymentType = 'Forhåndsbetalt';
                break;
            }
            receiptText = receiptText + tempPaymentType + ':' + payment.Amount.toFixed(2) + ';';
          });
        }
        receiptText = receiptText + '|';
        this.shopService.AddSessionValue({ sessionKey: sessionKey, value: receiptText, branchId: this.branchId,
          salepointId: this.shopHomeService.$shopLoginData.SalePoint.ID  }).subscribe(
          res => {
            this.exceSessionService.AddSession('REC', 'START', sessionKey, this.exceLoginService.CurrentUser.username);
          },
          error => {
            this.exceMessageService.openMessageBox('WARNING',
              {
                messageTitle: 'WARNING',
                messageBody: 'COMMON.ErrorInAddToSession'
              });
          });

      },
        error => {

        });
    }
  }


}




