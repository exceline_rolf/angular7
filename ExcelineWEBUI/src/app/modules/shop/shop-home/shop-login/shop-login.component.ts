import { error } from 'util';
import { ShopHomeService } from '../shop-home.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { ConfigService } from '@ngx-config/core';
import { ExceLoginService } from 'app/modules/login/exce-login/exce-login.service';
import { ExceShopService } from 'app/modules/shop/services/exce-shop.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'shop-login',
  templateUrl: './shop-login.component.html',
  styleUrls: ['./shop-login.component.scss']
})
export class ShopLoginComponent implements OnInit {

  @Input() IsShopLoginWithCard: boolean;
  @Input() addSalePoint: any;
  @Input() hardWareProfiles: any;
  loginFormSubmited: boolean;
  cardFormSubmited: boolean;
  salePointFormSubmitted = false;
  isLoginSuccess: boolean;
  salePoint: any = {};
  gymCode: any;
  cardValidateForm: FormGroup;
  salePointForm: FormGroup;
  salePoints: any[] = [];
  defaultSalePoint: any;
  loginForm: FormGroup;
  isForgotPassword = false;
  notSuccess = false;

  loginFormErrors = {
    'username': '',
    'password': ''
  };

  loginValidationMessages = {
    'username': {
      'required': 'LOGIN.unameRequired'
    },
    'password': {
      'required': 'LOGIN.pwordRequired',
      'loginFail': 'LOGIN.LVPwUnnotCorrect'
    }
  };
  cardFormErrors = {
    'cardNumber': ''
  };

  cardValidationMessages = {
    'cardNumber': {
      'required': 'COMMON.lgCardError'
    }
  };

  salePointFormErrors = {
    'MachineName': ''
  }

  salePointFormMessages = {
    'MachineName': {
      'taken': 'SHOP.MachineTaken'
    }
  };

  constructor(
    private shopHomeService: ShopHomeService,
    private config: ConfigService,
    private exceShopService: ExceShopService,
    private exceLoginService: ExceLoginService,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute

  ) {
    if (this.config.getSettings('ENV') === 'DEV') {
      this.gymCode = this.config.getSettings('GYMCODE')
    } else {
      this.gymCode = this.exceLoginService.CompanyCode;
    }

    this.loginForm = fb.group({
      'username': [null, [Validators.required]],
      'password': [null, [Validators.required]],
    });

    this.cardValidateForm = fb.group({
      'cardNumber': [null, [Validators.required]]
    });


    this.loginFormSubmited = false;
    this.cardFormSubmited = false;
    this.loginForm.statusChanges.subscribe(_ => {
      UsErrorService.onValueChanged(this.loginForm, this.loginFormErrors, this.loginValidationMessages)
    });
    this.cardValidateForm.statusChanges.subscribe(_ => {
      UsErrorService.onValueChanged(this.cardValidateForm, this.cardFormErrors, this.cardValidationMessages)
    });


  }

  ngOnInit() {
    this.salePointForm = this.fb.group(
      {
        'MachineName': [null],
        'SalePointName': [null]
      }
    );

    this.salePointForm.patchValue({
      MachineName: this.exceLoginService.MachineName
    });

    // set the first harwareprofile
    if (this.hardWareProfiles.length > 0){
      this.salePoint.HardwareProfileId =  this.hardWareProfiles[0].Id;
    }
  }

  shopLogin(value: any) {
    this.loginFormSubmited = true;
    if (this.loginForm.valid) {
      this.exceShopService.ValidateUser({
        userName: this.gymCode + '/' + value.username,
        password: value.password
      })
        .subscribe(result => {
          if (result.Data === '1') {
            this.isLoginSuccess = true;
            this.shopHomeService.$hasUserLoggedShop = true;
          } else {
            this.isLoginSuccess = false;
            this.loginForm.controls['password'].setErrors({
              'loginFail': true
            });
          }
        },
        error => {
          this.isLoginSuccess = false;
        });
    } else {
      UsErrorService.validateAllFormFields(this.loginForm, this.loginFormErrors, this.loginValidationMessages);
    }
  }

  validateWithCard(value: any) {
    this.cardFormSubmited = true;
    if (this.cardValidateForm.valid) {
      this.exceShopService.ValidateUserWithCard({
        cardNumber: value.cardNumber
      }).subscribe(
        res => {
          if (res.Data.Id > 0) {
            this.isLoginSuccess = true;
            this.shopHomeService.$hasUserLoggedShop = true;
          } else {
            this.cardValidateForm.controls['cardNumber'].setErrors({
              'required': true
            });
          }
        }, err => {
          console.log(err)
        });
    } else {
      UsErrorService.validateAllFormFields(this.cardValidateForm, this.cardFormErrors, this.cardValidationMessages);
    }
  }

  onHarwareProfileChanged(value: any) {
    this.salePoint.HardwareProfileId = value;
  }

  saveSalePoint() {
    this.salePointFormSubmitted = true;
    this.salePoint.Name = this.salePointForm.value.SalePointName;
    this.salePoint.MachineName = this.salePointForm.value.MachineName;
    this.salePoint.BranchID = this.exceLoginService.SelectedBranch.BranchId;
    this.exceShopService.SavePointOfSale(this.salePoint).subscribe(
      result => {
        if (result.Data > 0) {
          this.salePoint.Id = result.Data;
          this.shopHomeService.$salePoint = this.salePoint;
          this.shopHomeService.shopLoginSuccessEvent.next(true);
        } else {
          // UsErrorService.validateAllFormFields(this.salePointForm, this.salePointFormErrors, this.salePointFormMessages);
          this.salePointForm.controls['MachineName'].setErrors({'taken': true})
          this.notSuccess = true;
          UsErrorService.validateAllFormFields(this.salePointForm, this.salePointFormErrors, this.salePointFormMessages);
        }
      }
    );
    // save the sale point
  }

}
