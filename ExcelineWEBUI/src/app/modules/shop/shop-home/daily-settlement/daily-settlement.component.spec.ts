import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailySettlementComponent } from './daily-settlement.component';

describe('DailySettlementComponent', () => {
  let component: DailySettlementComponent;
  let fixture: ComponentFixture<DailySettlementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailySettlementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailySettlementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
