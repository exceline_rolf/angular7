import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import { Guid } from '../../../../shared/services/guid.service';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { ExceToolbarService } from '../../../common/exce-toolbar/exce-toolbar.service';
import { ShopHomeService } from '../shop-home.service';
import { ExceShopService } from '../../services/exce-shop.service';
import { Component, OnInit, EventEmitter, Output, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ExceSessionService } from 'app/shared/services/exce-session.service';
import { Observable } from 'rxjs';
import { ExcePdfViewerService } from 'app/shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { UsbModalRef } from '../../../../shared/components/us-modal/modal-ref';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { Subject, timer } from 'rxjs';
import { takeUntil, first } from 'rxjs/operators';
@Component({
  selector: 'daily-settlement',
  templateUrl: './daily-settlement.component.html',
  styleUrls: ['./daily-settlement.component.scss']
})
export class DailySettlementComponent implements OnInit, OnDestroy {

  settlementData?: any;
  locale: string;
  countedCash: Number;
  timer: any;
  timerSub: any;
  isConfirm = true;
  blobUrl: any;
  blob: any;
  generatePdfView: UsbModalRef;
  private destroy$ = new Subject<void>();
  safeURL: any

  @ViewChild('Pdfwindow') pdfView: ElementRef;

  @Output() closeModel = new EventEmitter();

  constructor(
    private shopService: ExceShopService,
    private shopHomeService: ShopHomeService,
    private exceMessageService: ExceMessageService,
    private toolbarService: ExceToolbarService,
    private excePdfViewerService: ExcePdfViewerService,
    private exceLoginService: ExceLoginService,
    private modalService: UsbModal,
    private sanitizer: DomSanitizer,
    private exceSessionService: ExceSessionService

  ) {

    this.exceMessageService.yesCalledHandle.subscribe((value) => {
      this.settlementData.SalePointId = this.shopHomeService.$shopLoginData.SalePoint.ID;
      this.settlementData.Deviation = 0 - Number(this.settlementData.CashIn) + Number(this.settlementData.CashOut);
      this.settlementData.CountedCash = 0;

      if (this.isConfirm) {
        this.saveDailySettlement(this.settlementData);
      } else {
        this.ViewDailySettlementReport(-1, -1);
      }
    });

    this.countedCash = 0;
    if (this.toolbarService.getSelectedLanguage().id === 'no') {
      this.locale = 'nb-NO';
    } else {
      this.locale = 'en-US';
    }
  }

  ngOnInit() {
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
    this.shopService.GetDailySettlements({ salePointId: this.shopHomeService.$shopLoginData.SalePoint.ID }).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.settlementData = res.Data;
        this.settlementData.Received = Number(this.settlementData.TotalReceived) -
          Number(this.settlementData.Exchange) - Number(this.settlementData.PrePaidPayment);
      });
  }

  generateDailySettlement() {
    this.isConfirm = true;
    if (Number.isNaN(Number(this.countedCash)) || Number(this.countedCash) <= 0) {
      this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'CONFIRM',
          messageBody: 'COMMON.MsgGenerateCountedCash'
        });
    } else {
      this.settlementData.SalePointId = this.shopHomeService.$shopLoginData.SalePoint.ID;
      this.settlementData.Deviation = Number(this.countedCash) - Number(this.settlementData.CashIn) + Number(this.settlementData.CashOut);
      this.settlementData.CountedCash = Number(this.countedCash);
      this.saveDailySettlement(this.settlementData);
    }
  }

  changeCountedCase(value) {
    if (!Number.isNaN(Number(this.countedCash))) {
      this.settlementData.Deviation = Number(this.countedCash) - Number(this.settlementData.CashIn) + Number(this.settlementData.CashOut);
    }
  }

  saveDailySettlement(req) {
    PreloaderService.showPreLoader();
    this.shopService.AddDailySettlements(req).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        if (res.Data > 0) {
          if (this.shopHomeService.$shopLoginData.GymSetting.IsBankTerminalIntegrated) {
            PreloaderService.showPreLoader();
            const sessionKey = Guid.newGuid();
            this.exceSessionService.AddSession(
              'RECONCILIATION',
              'START',
              sessionKey,
              this.exceLoginService.CurrentUser.username);
            this.timer = timer(0, 2000);
            this.timerSub = this.timer.pipe(takeUntil(this.destroy$)).subscribe(t => this.tickerFunc(t, sessionKey, res.Data));
            return;
          } else {
            this.ViewDailySettlementReport(res.Data, -1);
          }
        }
        this.closeModel.emit();
      },
      err => {
        PreloaderService.hidePreLoader();
      }, () => PreloaderService.hidePreLoader());
  }

  tickerFunc(tick: number, sessionKey: string, dailySettlementID: any) {
    PreloaderService.showPreLoader();
 this.shopService.GetReconciliationSession({ sessionKey, dailySettlementID }).pipe(
   first(),
   takeUntil(this.destroy$)
 ).subscribe(
      result => {
        if (result.Data) {
          if (result.Data > 0 || result.Data === -2 || tick > 150) {
            this.timerSub.unsubscribe();
            this.ViewDailySettlementReport(dailySettlementID, result.Data);
            this.closeModel.emit();
            PreloaderService.hidePreLoader();
          }
        }
      },
      error => {
        if (tick > 10) {
          this.timerSub.unsubscribe();
          PreloaderService.hidePreLoader();
        }
      }, () => { if (tick > 150)
        { alert('Kommunikasjon med terminalen feilet. Kontakt systemleverandør.')
        this.timerSub.unsubscribe();
        PreloaderService.hidePreLoader(); }
      }
    );
  }

  private ViewDailySettlementReport(dailySettlementID: any, reconciliationId: any) {
   /* this.shopService.ViewDailySettlementReport({
      'dailySettlementId': dailySettlementID,
      'reconciliationId': reconciliationId,
      'countedcashdraft': this.countedCash,
      'salePointId': this.shopHomeService.$shopLoginData.SalePoint.ID,
      'mode': 'PRINT',
      'startDate': this.settlementData.FromDateTime,
      'endDate': this.settlementData.ToDateTime
    }).subscribe(res => {
      // TODO: Dispaly report
      if (res.Data.FileParth) {
        this.excePdfViewerService.openPdfViewer('Report', res.Data.FileParth);
      }
      PreloaderService.hidePreLoader();
    }, err => {
      PreloaderService.hidePreLoader();
    });*/
    this.openGenerateConfirmModal( dailySettlementID, reconciliationId);
  }

  printDailySettlement() {
    this.isConfirm = false;
    if (Number.isNaN(Number(this.countedCash)) || Number(this.countedCash) <= 0) {
      this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'CONFIRM',
          messageBody: 'COMMON.MsgGenerateCountedCash'
        });
    } else {
      this.settlementData.SalePointId = this.shopHomeService.$shopLoginData.SalePoint.ID;
      this.settlementData.Deviation = Number(this.countedCash) - Number(this.settlementData.CashIn) + Number(this.settlementData.CashOut);
      this.settlementData.CountedCash = Number(this.countedCash);
      this.ViewDailySettlementReport(-1, -1);
    }
    this.closeModel.emit();
  }

  printfunc(dailySettlementID: any, reconciliationId: any) {
    const contentType = 'application/pdf'
    let b64Data;
    this.shopService.ViewDailySettlementReport({
      'dailySettlementId': dailySettlementID,
      'reconciliationId': reconciliationId,
      'countedcashdraft': this.countedCash,
      'salePointId': this.shopHomeService.$shopLoginData.SalePoint.ID,
      'mode': this.isConfirm ? 'PRINT' : 'DRAFT',
      'startDate': this.settlementData.FromDateTime,
      'endDate': this.settlementData.ToDateTime
    }).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
      if (result) {
        b64Data = result.Data; } }, err => console.log(err),
       () => {
      this.blob = this.b64toBlob(b64Data, contentType);
      this.blobUrl = URL.createObjectURL(this.blob);
      this.safeURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.blobUrl)}
      );
  }


  b64toBlob = (b64Data, contentType= '', sliceSize= 512) => {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];
  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize),
        byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

const blob = new Blob(byteArrays, {type: contentType});
return blob;
}

openGenerateConfirmModal(dailySettlementID: any, reconciliationId: any) {
  this.printfunc(dailySettlementID, reconciliationId );
  this.generatePdfView = this.modalService.open(this.pdfView, {width: '800'});
 }



PrintPDF() {
  const iframe = document.createElement('iframe');
  iframe.style.display = 'none';
  iframe.src = this.blobUrl;
  document.body.appendChild(iframe);
  iframe.contentWindow.print();
}

SavePDF() {
  saveAs(this.blob, 'DailySettlement' + '.pdf');
}

ngOnDestroy() {
  this.destroy$.next();
  this.destroy$.complete();
}

}
