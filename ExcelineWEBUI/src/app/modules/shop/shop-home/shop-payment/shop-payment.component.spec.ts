import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashCustomerComponent } from './cash-customer.component';

describe('CashCustomerComponent', () => {
  let component: CashCustomerComponent;
  let fixture: ComponentFixture<CashCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
