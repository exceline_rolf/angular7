import { Subscription } from 'rxjs';
import { ColumnDataType } from '../../../shared/enums/us-enum';
import { ExceMemberService } from '../../membership/services/exce-member.service';
import { Component, OnInit, ViewChild, OnDestroy, EventEmitter, Output, } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { UsbModal } from '../../../shared/components/us-modal/us-modal';
import { ExceShopService } from '../services/exce-shop.service';
import { Guid } from '../../../shared/services/guid.service';
import { AmountConverterPipe } from '../../../shared/pipes/amount-converter.pipe';
import { PreloaderService } from '../../../shared/services/preloader.service';
import { ExceLoginService } from '../../../modules/login/exce-login/exce-login.service';
import { ShopHomeService } from './shop-home.service';
import { AdminService } from '../../../modules/admin/services/admin.service';
import { ExceGymSetting } from '../../../shared/SystemObjects/Common/ExceGymSetting';
import { CategoryTypes } from '../../../shared/enums/category-types';
import { Observable, Subject, timer } from 'rxjs';
import { ExceSessionService } from '../../../shared/services/exce-session.service';
import { Extension } from '../../../shared/Utills/Extensions';
import { ExceMessageService } from '../../../shared/components/exce-message/exce-message.service';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'shop-home',
  templateUrl: './shop-home.component.html',
  styleUrls: ['./shop-home.component.scss'],
  providers: [DatePipe, AmountConverterPipe]
})
export class ShopHomeComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();

  isSaveShopItems: any;
  isShopItemReturn = false;
  isDefaultCustomer = true;
  isPayDisable = true;
  issaveDisable = true;
  isCashWithdrawalDisable = false;
  isSalesHistoryDisable = false;
  shopLoginData?: any = {};
  isMemberAlreadySeleced = false;



  timer: any;
  timerSub: any;

  //
  isBankTerminalActive: true;
  selectedMember?: any = {};
  //

  columns = [{ property: 'FirstName', header: 'First Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'LastName', header: 'Last Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'Age', header: 'Age', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'TelMobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'StatusName', header: 'Status', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'BranchName', header: 'Gym Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
  ];

  auotocompleteItems = [{ id: 'NAME', name: 'NAME :', isNumber: false }, { id: 'ID', name: 'Id :', isNumber: true }]
  itemCount: number;
  entitySearchType: string;
  entityItems = [];
  addMemberView: any;
  newMemberRegisterView: any;
  entitySelectionModel: any;

  selectedGymsettings = [
    'EcoNegativeOnAccount',
    'EcoSMSInvoiceShop',
    'EcoIsPrintInvoiceInShop',
    'EcoIsPrintReceiptInShop',
    'EcoIsShopOnNextOrder',
    'EcoIsOnAccount',
    'EcoIsDefaultCustomerAvailable',
    'EcoIsPayButtonsAvailable',
    'EcoIsPettyCashSameAsUserAllowed',
    'OtherIsShopAvailable',
    'OtherLoginShop',
    'HwIsShopLoginWithCard',
    'ReqMemberListRequired',
    'OtherIsLogoutFromShopAfterSale',
    'EcoIsBankTerminalIntegrated',
    'EcoIsDailySettlementForAll',
    'OtherIsValidateShopGymId'
  ];

  exceGymSetting: ExceGymSetting = new ExceGymSetting();
  cashWithdrawalModal: any;
  dailySettlementModal: any;
  isBasicInforVisible = true;
  isAddBtnVisible = true;
  isBranchVisible = true;
  isStatusVisible = true;
  isRoleVisible = true;
  isDbPagination = true;
  isCancelBtnHide = false;
  isCashCustomerBtnHide = false;
  isDailySettlementBtnHide = false;
  loadShopHome = true;
  sessionKey: string;

  paymentAdded = false;
  salepointid: any;
  machineName = 'DEFAULT';
  userName: any;
  sessionkey: string;
  timerEvent: any;
  machineSessionkey: string;
  IsCashSalesActivated = false;
  VoucherNotEditable: any;

  message: any;
  type: string;
  isShowInfo: boolean;


  constructor(
    private modalService: UsbModal,
    private router: Router,
    private shopService: ExceShopService,
    private exceMessageService: ExceMessageService,
    private amountConverterPipe: AmountConverterPipe,
    private shopHomeService: ShopHomeService,
    private adminService: AdminService,
    private fb: FormBuilder,
    private translateService: TranslateService,
    private exceLoginService: ExceLoginService,
    private memberService: ExceMemberService,
    private exceSessionService: ExceSessionService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    this.entitySearchType = CategoryTypes[CategoryTypes.MEMBER];
    shopHomeService.getShopStatusEvent.subscribe(
      response => {
        if (response.shopLoginData && response.shopLoginData.GymSetting) {
          this.isDefaultCustomer = (
            (response.items && response.items.length > 0) &&
            (response.priceType !== CategoryTypes[CategoryTypes.EMPLOYEE]))
            ? false : true;

          this.issaveDisable = (
            (response.items && response.items.length > 0) &&
            (response.shopLoginData.GymSetting.MemberRequired) ||
            this.shopHomeService.$selectedMember)
            ? false : true;

          this.isPayDisable = (
            (response.items.find(shopItem => shopItem.isReturn === true)) ||
            ((response.items && response.items.length > 0) &&
              (response.shopLoginData.GymSetting.MemberRequired) ||
              this.shopHomeService.$selectedMember))
            ? false : true;
        }
      });
    shopHomeService.shopItemReturnEvent.subscribe(isReturn => this.isShopItemReturn = isReturn);
  }

  ngOnInit() {
    this.machineName = 'DEFAULT';
    this.salepointid = null;
    this.exceSessionService.isBankTerminalActiveEvent.subscribe(
      (value) => {
        this.isBankTerminalActive = value;
        if (value === true && this.machineName === 'DEFAULT' && !this.machineSessionkey) {
          // get the machine name
          this.machineSessionkey = Extension.createGuid();
          if (this.machineSessionkey) {
            this.exceSessionService.AddSession(
              'MACHINE',
              'DEFAULT',
              this.machineSessionkey,
              this.exceLoginService.CurrentUser.username);
            this.timer = timer(0, 2000);
            this.timerEvent = this.timer.subscribe(X => this.getMachineName(X, this.machineSessionkey));
           }
        } else {
          return;
        }
      }
    )
    // temp - working on cashdraer
    // this.machineName = 'DESKTOP-TLTQ77E';
    this.userName = this.exceLoginService.CurrentUser.username.split('/')[1];
    this.shopHomeService.$isCashCustomerSelected = true;
    this.shopHomeService.paymentAddedEvent.subscribe(boolVal => {
      this.paymentAdded = boolVal;
    }, null, null
    );

    if (this.shopHomeService.$isCancelBtnHide) {
      this.isCancelBtnHide = true;
    } else {
      this.isCancelBtnHide = false;
    }

    // if (this.shopHomeService.$shopMode === 'INVOICE') {
    //   this.isCancelBtnHide = true;
    //   this.isCashCustomerBtnHide = true;
    //   this.isDailySettlementBtnHide = true;
    //   this.isDefaultCustomer = true;
    //   this.isSalesHistoryDisable = true;
    //   this.isCashWithdrawalDisable = true;
    // }

    this.shopService.GetSelectedGymSettings({ GymSetColNames: this.selectedGymsettings, IsGymSettings: true }).subscribe(
      res => {
        for (const key in res.Data) {
          if (res.Data.hasOwnProperty(key)) {
            switch (key) {
              case 'EcoNegativeOnAccount':
                this.exceGymSetting.OnAccountNegativeVale = res.Data[key];
                break;
              case 'EcoIsShopOnNextOrder':
                this.exceGymSetting.IsShopOnNextOrder = res.Data[key];
                break;
              case 'EcoIsDailySettlementForAll':
                this.exceGymSetting.IsDailySettlementForAll = res.Data[key];
                break;
              case 'EcoIsOnAccount':
                this.exceGymSetting.IsOnAccount = res.Data[key];
                break;
              case 'EcoIsDefaultCustomerAvailable':
                this.exceGymSetting.IsDefaultCustomerAvailable = res.Data[key];
                break;
              case 'EcoIsPayButtonsAvailable':
                this.exceGymSetting.IsPayButtonsAvailable = res.Data[key];
                break;
              case 'EcoIsPettyCashSameAsUserAllowed':
                this.exceGymSetting.IsPettyCashSameAsUserAllowed = res.Data[key];
                break;
              case 'EcoIsPrintInvoiceInShop':
                this.exceGymSetting.IsPrintInvoiceInShop = res.Data[key];
                break;
              case 'EcoIsPrintReceiptInShop':
                this.exceGymSetting.IsPrintReceiptInShop = res.Data[key];
                break;
              case 'OtherIsShopAvailable':
                this.exceGymSetting.IsShopAvailable = res.Data[key];
                break;
              case 'OtherLoginShop':
                this.exceGymSetting.IsShopLoginNeeded = res.Data[key];
                break;
              case 'HwIsShopLoginWithCard':
                this.exceGymSetting.IsShopLoginWithCard = res.Data[key];
                break;
              case 'ReqMemberListRequired':
                this.exceGymSetting.MemberRequired = res.Data[key];
                break;
              case 'OtherIsLogoutFromShopAfterSale':
                this.exceGymSetting.IsLogoutFromShopAfterSale = res.Data[key];
                break;
              case 'EcoSMSInvoiceShop':
                this.exceGymSetting.IsSMSInvoiceShop = res.Data[key];
                break;
              case 'EcoIsBankTerminalIntegrated':
                this.exceGymSetting.IsBankTerminalIntegrated = res.Data[key];
                break;
              case 'OtherIsValidateShopGymId':
                this.exceGymSetting.IsValidateShopGymId = res.Data[key];
                break;
            }
          }
        }

        this.shopLoginData.GymSetting = this.exceGymSetting;
        if (this.exceGymSetting != null && this.exceGymSetting.IsValidateShopGymId) {
          const sessionKey = Guid.newGuid();
          this.sessionKey = sessionKey;
          this.shopHomeService.$sessionKey = sessionKey;
          this.exceSessionService.AddSession(
            'GYMID',
            '',
            sessionKey,
            this.exceLoginService.CurrentUser.username);
        } else {
          // this.shopHomeService.isBankTerminalActive = false;
          this.exceSessionService.isBankTerminalActiveEvent.next(false);
        }


        this.shopService.GetSalesPoint().subscribe(
          salePoints => {
            salePoints.Data.forEach(saleP => {
              if (saleP.MachineName === this.machineName) {
                this.shopLoginData.SalePoint = saleP;
                this.salepointid = saleP.ID;
              }
            });
            // this.shopLoginData.SalePoint = salePoints.Data[0];
            // this.salepointid = salePoints.Data[0].ID;
            this.adminService.getGymSettings('HARDWARE', null).subscribe(
              hwSetting => {
                this.shopLoginData.HardwareProfileList = hwSetting.Data;
                this.shopLoginData.HardwareProfileList.forEach(hwProfile => {
                  if (this.shopLoginData.SalePoint != null &&
                    this.shopLoginData.SalePoint.HardwareProfileId === hwProfile.Id) {
                    this.shopLoginData.SelectedHardwareProfile = hwProfile;
                    this.shopLoginData.HardwareProfileId = hwProfile.Id;
                  }
                  this.shopHomeService.$shopLoginData = this.shopLoginData;

                });
              },
              error => {

              });
          },
          error => {

          });
      },
      error => {

      }
    )


    //  this.shopHomeService.isBankTerminalActiveEvent.subscribe(
    //   (value) => {
    //     console.log(value)
    //     this.isBankTerminalActive = value;
    //   }
    // )

    this.shopHomeService.openCashDrawerEvent.subscribe(() => {
        this.openCashDrawer();
      }, null, null
    );

    this.shopHomeService.showMessageEvent.subscribe(show => {
      this.translateService.get('SHOP.NextOrderInvoiceMessage').subscribe(deleteMessage => this.message = deleteMessage);
      this.type = 'DANGER';
      this.isShowInfo = true;
      setTimeout(() => {
        this.isShowInfo = false;
      }, 4000);
    }, null, null);

    this.adminService.getGymSettings('ECONOMY', null, this.exceLoginService.SelectedBranch.BranchId).subscribe(
      ecoSetting => {
        this.IsCashSalesActivated = ecoSetting.Data[0].IsCashSalesActivated;
        this.VoucherNotEditable = ecoSetting.Data[0].NotEditableVoucher;
        this.shopHomeService.$VoucherNotEditable = this.VoucherNotEditable;
      }, null, null
    );

  }

  getMachineName(tick: Number, sessionKey: string) {
    this.exceSessionService.getSessionValue(sessionKey).subscribe(
      result => {
        if (result) {
          if (result.Data !== 'ERROR' && result.Data !== 'DUPLICATE' && result.Data !== 'SUCCESS' && result.Data !== '') {
            this.timerEvent.unsubscribe();
            this.machineName = result.Data;
            this.shopService.GetSalesPoint().subscribe(
              salePoints => {
                salePoints.Data.forEach(saleP => {
                  if (saleP.MachineName === this.machineName) {
                    this.shopLoginData.SalePoint = saleP;
                    this.salepointid = saleP.ID;
                  }
                });
                // this.shopLoginData.SalePoint = salePoints.Data[0];
                // this.salepointid = salePoints.Data[0].ID;
                this.adminService.getGymSettings('HARDWARE', null).subscribe(
                  hwSetting => {
                    this.shopLoginData.HardwareProfileList = hwSetting.Data;
                    this.shopLoginData.HardwareProfileList.forEach(hwProfile => {
                      if (this.shopLoginData.SalePoint != null &&
                        this.shopLoginData.SalePoint.HardwareProfileId === hwProfile.Id) {
                          let cashDrawer = false;
                          hwProfile.HardwareProfileItemList.forEach(hardWare => {
                            if (hardWare.Code === 'CASHDRAWER') {
                              cashDrawer = true;
                            }
                          });
                          if (this.IsCashSalesActivated && cashDrawer) {
                            // this.IsCashSalesActivated = true;
                            this.shopHomeService.$IsCashSalesActivated = true;
                          } else {
                            // this.IsCashSalesActivated = false;
                            this.shopHomeService.$IsCashSalesActivated = false;
                          }
                          // this.IsCashSalesActivated = cashDrawer;
                        this.shopLoginData.SelectedHardwareProfile = hwProfile;
                        this.shopLoginData.HardwareProfileId = hwProfile.Id;
                      }
                      this.shopHomeService.$shopLoginData = this.shopLoginData;

                    });
                  }, null, null);
              }, null, null);
          } else {
            if (tick === 5) {
              this.timerEvent.unsubscribe();
              this.machineName = 'DEFAULT';
            }
          }
        }
      }, error => {
        this.timerEvent.unsubscribe();
        this.exceLoginService.MachineName = 'DEFAULT';
      },
      null);

  }

  savePayment() {
    // is cashdrawer open?
    // const maschinename = 'DESKTOP-TLTQ77E';
    if (this.isBankTerminalActive && this.IsCashSalesActivated) {
      this.shopService.isCashDrawerOpen(this.machineName).subscribe(
        result => {
          if (result.Data === true) {
            // errormessage
            let messageTitle = '';
            let messageBody = '';
            (this.translateService.get('SHOP.Cashdrawer').subscribe(tranlstedValue => messageTitle = tranlstedValue));
            (this.translateService.get('SHOP.CashdrawerOpenErr').subscribe(tranlstedValue => messageBody = tranlstedValue));
            this.exceMessageService.openMessageBox('ERROR',
            {
              messageTitle: messageTitle,
              messageBody: messageBody
            });
          } else {
            this.shopHomeService.savePayment();
          }
        }, null, null
      )
    } else {
      this.shopHomeService.savePayment();
   }
  }

  openPayModel(content, isSaveItems) {
    this.isSaveShopItems = isSaveItems;
    this.shopHomeService.$isSaveShopItems = isSaveItems;
    this.entityItems = [];
    if (this.shopHomeService.$priceType === CategoryTypes[CategoryTypes.MEMBER]) {
      this.isBasicInforVisible = true;
      this.isAddBtnVisible = true;
      this.isBranchVisible = true;
      this.isStatusVisible = true;
      this.isRoleVisible = true;
      this.isDbPagination = true;
      this.entitySearchType = CategoryTypes[CategoryTypes.MEMBER];
      this.columns = [
        { property: 'FirstName', header: 'First Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'LastName', header: 'Last Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'Age', header: 'Age', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'TelMobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'StatusName', header: 'Status', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'BranchName', header: 'Gym Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
      ];
    } else {
      this.isBasicInforVisible = false;
      this.isAddBtnVisible = false;
      this.isBranchVisible = false;
      this.isStatusVisible = false;
      this.isRoleVisible = false;
      this.isDbPagination = true;
      this.entitySearchType = CategoryTypes[CategoryTypes.EMPLOYEE];
      this.columns = [
        { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      ];
    }

    if (this.shopHomeService.$selectedMember && this.shopHomeService.$selectedMember.Id > 0) {
      this.selecteMember(this.shopHomeService.$selectedMember);
    } else {
      this.entitySelectionModel = this.modalService.open(content);
    }
  }

  searchDeatail(val: any) {
    switch (val.entitySelectionType) {
      case CategoryTypes[CategoryTypes.MEMBER]:
        PreloaderService.showPreLoader();
        this.memberService.getMembers(val.branchSelected.BranchId, val.searchVal, val.statusSelected.id,
          'SHOP', val.roleSelected.id, val.hit, false,
          false, ''
        ).subscribe
          (result => {
            PreloaderService.hidePreLoader();
            if (result) {
              this.entityItems = result.Data;
              this.itemCount = result.Data.length;
            } else {
            }
          }, err => {
            PreloaderService.hidePreLoader();
          }, null);
        break;
      case CategoryTypes[CategoryTypes.EMPLOYEE]:
        PreloaderService.showPreLoader();
        this.shopService.GetGymEmployees({ searchText: val.searchVal, isActive: true }).subscribe(result => {
            this.entityItems = result.Data;
            this.itemCount = result.Data.length;
            PreloaderService.hidePreLoader();
          }, err => {
            PreloaderService.hidePreLoader();
          }, null);
        break;
    }

  }

  selecteMember(item) {
    this.shopHomeService.$selectedMember = item;
    this.shopHomeService.$isCashCustomerSelected = false;
    if (this.isSaveShopItems) {
      // TODO implement generating invoice methos
      if (this.entitySelectionModel) {
        this.entitySelectionModel.close();
      }
    } else {
      if (this.shopHomeService.$priceType === CategoryTypes[CategoryTypes.EMPLOYEE]) {
        this.loadShopAccount(item.Id, 'EMP');
      } else if (this.shopHomeService.$priceType === CategoryTypes[CategoryTypes.MEMBER]) {
        this.loadShopAccount(item.Id, 'MEM');
      }
    }
    this.shopHomeService.memberSelectedEvent.next(item);
  }

  openCashCustomer() {
    const isCashWithdrawalOpen = false;
    const isSaveMode = false
    if (this.shopHomeService.$priceType === CategoryTypes[CategoryTypes.EMPLOYEE]) {
    } else if (this.shopHomeService.$priceType === CategoryTypes[CategoryTypes.MEMBER]) {
    }
    this.shopHomeService.$isCashCustomerSelected = true;
    this.shopHomeService.$shopAccount = null;
    // this.router.navigate(['shop/shop-payment']);
    // this.loadShopHome = false;
  }

  loadShopAccount(memberId: Number, entityType: string) {
    // this.checkEmployeeByEntNo
    this.shopService.GetMemberShopAccountsForEntityType({ memberId: memberId, entityType: entityType }).subscribe(
      res => {
        this.shopHomeService.$shopAccount = res.Data;
        // this.router.navigate(['shop/shop-payment']);
        // this.loadShopHome = false;
        if (this.entitySelectionModel) {
          this.entitySelectionModel.close();
        }
      }, null, null);
  }

  checkEmployeeByEntNo(entNo: number) {
    this.shopService.CheckEmployeeByEntNo({ entityNO: entNo }).subscribe(
      null, null, null);
  }

  addEntity(item) {
    this.addMemberView.close();
    this.selecteMember(item);
  }

  addMemberModal(content: any) {
    this.addMemberView = this.modalService.open(content, { width: '800' });
    this.entitySelectionModel.close();
  }

  closeMemberSelection() {
    this.entitySelectionModel.close();
  }

  openCashWithdrawal(content) {
    this.cashWithdrawalModal = this.modalService.open(content, { width: '400' });
  }

  openDailySettlement(content) {
    this.dailySettlementModal = this.modalService.open(content, { width: '800' });
  }

  openSalesHistory(content) {
    this.modalService.open(content);
  }

  openAddNewMemberModel(content) {
    this.modalService.open(content, { width: '1000' });
  }

  saveShopItems() {
  }

  cancelShop() {
    if (this.shopHomeService.$shopOpenedFromOrders === true) {
      this.shopHomeService.closeShopEvent.next(false);
      // this.exceBreadcrumbService.reomveShopInstance.next();
      this.ngOnDestroy();
    } else {
      this.shopHomeService.clearShopItems();
      this.router.navigateByUrl('/');
      // this.exceBreadcrumbService.reomveShopInstance.next();
      this.ngOnDestroy();
    }
  }


  reloadItems(params) {

  }

  rowClick(rowEvent) {
  }

  rowDoubleClick(rowEvent) {

  }

  rowTooltip(item) { return item.jobTitle; }

  clickButton(item) {
  }

  reloadReceiptDetails(params): void {
  }

  closeWithdrowalModel() {
    this.cashWithdrawalModal.close();
  }

  closeDailySettlement() {
    this.dailySettlementModal.close();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();

    this.shopHomeService.$selectedMember = {};
    this.shopHomeService.$isCreditNote = false;
    this.shopHomeService.$isOnAccountPayment = false;
    this.shopHomeService.$isBookingPayment = false;
    this.shopHomeService.$giftVoucher = false;

    if (this.entitySelectionModel) {
      this.entitySelectionModel.close();
    }
    if (this.addMemberView) {
      this.addMemberView.close();
    }
    if (this.cashWithdrawalModal) {
      this.cashWithdrawalModal.close();
    }
    if (this.dailySettlementModal) {
      this.dailySettlementModal.close();
    }

  }

  closePaymentView(event) {
    this.loadShopHome = true;
    this.shopHomeService.closeShopEvent.next(event);
    // this.closeShop.emit();
  }


  private tickerFunc(tick: number, sessionKey: string) {
    this.exceSessionService.getSessionValue(sessionKey).subscribe(
      result => {
        if (result.Data) {
          if (result.Data === 'OPENCASHDRAWER') {
            PreloaderService.hidePreLoader();
            this.timerSub.unsubscribe();
            this.logOpenCashdrawer();
          }
        }
      },
        err => {
          PreloaderService.hidePreLoader();
          this.timerSub.unsubscribe();
          this.exceMessageService.openMessageBox('ERROR',
          {
            messageTitle: 'Exceline',
            messageBody: 'COMMON.PaymentError'
          });
        }
      );
    }

  logOpenCashdrawer() {
    const bodyOCD = {
      SalesPointId: this.shopHomeService.$shopLoginData.SalePoint.ID,
      EntitiyId: this.selectedMember ? this.selectedMember.Id : -2,
      EntityRoleType: (this.shopHomeService.$priceType === 'MEMBER' || this.shopHomeService.$priceType ===  'MEM') ? 'MEM' : 'EMP',
      UserName: this.exceLoginService.CurrentUser.username,
      BranchId: this.selectedMember ? ((this.selectedMember.BranchID > 0) ? this.selectedMember.BranchID : 1) : 1,
      MachineName: this.machineName
    };

    this.shopService.SaveOpenCashRegister(bodyOCD).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data > 0) {
        }
      }, err => {
        this.exceMessageService.openMessageBox('ERROR',
          {
            messageTitle: 'Exceline',
            messageBody: 'Problem med lagring av open cash drawer.'
          });
      });
  }

  openCashDrawer() {
    const sessionKey = Extension.createGuid();
    PreloaderService.showPreLoader();
    this.exceSessionService.AddSession(
      'CASHDRAWER|BranchId:' + (this.selectedMember ? ((this.selectedMember.BranchID > 0) ? this.selectedMember.BranchID : 1) : 1) + '|SalePointId:' + this.shopHomeService.$shopLoginData.SalePoint.ID,
      '',
      sessionKey,
      this.exceLoginService.CurrentUser.username
    );

    this.timer = timer(0, 5000);
    this.timerSub = this.timer.subscribe(t => this.tickerFunc(t, sessionKey));

    return;
  }


}
