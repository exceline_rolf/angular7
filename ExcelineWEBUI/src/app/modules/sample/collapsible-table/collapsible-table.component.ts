import {
  Component, Input, Output, EventEmitter, ContentChildren, QueryList,
  TemplateRef, ContentChild, ViewChildren, OnInit, AfterContentInit
} from '@angular/core';
@Component({
  selector: 'collapsible-table',
  templateUrl: './collapsible-table.component.html',
  styleUrls: ['./collapsible-table.component.scss']
})
export class CollapsibleTableComponent implements OnInit, AfterContentInit {

  private jsonObj = [{ 'Id': 15, 'Name': 'Isb', 'Age': 25 },
  { 'Id': 15, 'Name': 'Isb', 'Age': 25 },
  { 'Id': 15, 'Name': 'Isb', 'Age': 25 },
  { 'Id': 25, 'Name': 'Isb', 'Age': 25 },
  { 'Id': 45, 'Name': 'Isb', 'Age': 25 }]

  private headers = []
   sortedObj = []
  private sortBy = 'Id'

//  @ContentChildren(CollapsibleTableColumn) columns: QueryList<CollapsibleTableColumn>;
  constructor() {
    this.headers = Object.keys(this.jsonObj[0])
    this.sortedObj = [[{ 'Id': 15, 'Name': 'Isb', 'Age': 25 },
    { 'Id': 15, 'Name': 'Isb', 'Age': 25 },
    { 'Id': 15, 'Name': 'Isb', 'Age': 25 }],
    [{ 'Id': 25, 'Name': 'Isb', 'Age': 25 }, { 'Id': 25, 'Name': 'Isb', 'Age': 25 }],
    [{ 'Id': 45, 'Name': 'Isb', 'Age': 25 }]
    ]
  }

  ngOnInit() {
  }

  ngAfterContentInit() {
    // this.columns.forEach(tabInstance => console.log(tabInstance))
  }

}
