import { Component, OnInit, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { UsPopover } from '../../../shared/components/us-popover/us-popover.component';
import { ExceMessageService } from '../../../shared/components/exce-message/exce-message.service';
import { IMyDpOptions } from '../../../shared/components/us-date-picker/interfaces';
import { PreloaderService } from '../../../shared/services/preloader.service';
import { UsbModal } from '../../../shared/components/us-modal/us-modal';
import { ExceCategoryService } from '../../../shared/components/exce-category/exce-category.service';

const now = new Date();

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss']
  // providers: [ExceCategoryService]
})
export class ColorPickerComponent implements OnInit {



  private newCategoryRegisterView: any;
  private selectedTime = '';
  private intelNumber = '+94715511954';
  private today;
  private modal;
  private myValue;
  private fromDatePickerOptions: IMyDpOptions = {
  };

  constructor(
    private elRef: ElementRef,
    private exceMessageService: ExceMessageService,
    private usbModal: UsbModal
  ) {

    this.exceMessageService.yesCalledHandle.subscribe((value) => {
      this.yesHandler();
    });
    this.exceMessageService.noCalledHandle.subscribe((value) => {
      this.noHandler();
    });
    this.exceMessageService.okCalledHandle.subscribe((value) => {
      this.okHandler();
    });

    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
  }

  displayTime() {
  }

  fullValueChange(event) {
    this.intelNumber = event.value;
  }

  displayNumber() {
  }

  valueChanged(value) {
  }

  ngOnInit() {
  }

  showPreloader() {
    PreloaderService.showPreLoader();
  }

  hidePreloader() {
    PreloaderService.hidePreLoader();
  }

  openNav() {
    document.getElementById('mySidenav').style.width = '250px';
    this.elRef.nativeElement.querySelector('.color-picker').style.marginLeft = '250px';
  }

  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
    this.elRef.nativeElement.querySelector('.color-picker').style.marginLeft = '0';
  }

  openExceMessage() {
    this.modal = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Confirm',
        messageBody: 'Do you confirm the Changes...'
      }
    );
  }

  yesHandler() {
  }

  noHandler() {
  }

  okHandler() {
  }

  newCategoryModal(content: any) {
    this.newCategoryRegisterView = this.usbModal.open(content, { width: '800' });
  }

  closeView() {
    this.newCategoryRegisterView.close();
  }

  newModal(content: any) {
    this.usbModal.open(content, { width: '600' });
  }

  openModal(content: any) {
    this.usbModal.open(content, { width: '300' });
  }



}
