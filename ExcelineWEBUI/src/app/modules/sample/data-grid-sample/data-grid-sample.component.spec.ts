import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataGridSampleComponent } from './data-grid-sample.component';

describe('DataGridSampleComponent', () => {
  let component: DataGridSampleComponent;
  let fixture: ComponentFixture<DataGridSampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataGridSampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataGridSampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
