import { NgModule, Inject } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IAppConfig } from '../../app-config/app-config.interface';
import { APP_CONFIG } from '../../app-config/app-config.constants';

import { ExceToolbarService } from '../common/exce-toolbar/exce-toolbar.service';

import { FormValidationComponent } from './form-validation/form-validation.component';
import { CalandarTestComponent } from './calandar-test/calandar-test.component';
import { DataGridSampleComponent } from './data-grid-sample/data-grid-sample.component';
import { AuthGuard } from '../../guards/auth.guard';
import { ColorPickerComponent } from './color-picker/color-picker.component';
import { DataTableSampleComponent } from './data-table-sample/data-table-sample.component';
import { NewFormComponent } from './new-form/new-form.component';
import { TreeTableComponent } from './tree-table/tree-table.component';
import { TranslateService } from '@ngx-translate/core';
import { CollapsibleTableComponent } from './collapsible-table/collapsible-table.component';
import { ColTableSampleComponent } from './col-table-sample/col-table-sample.component';
import { CropImageComponent } from './crop-image/crop-image.component';
const routes: Routes = [
  { path: '', component: FormValidationComponent },
  { path: 'form', component: FormValidationComponent },
  { path: 'calandar', component: CalandarTestComponent },
  { path: 'datagrid', component: DataGridSampleComponent },
  { path: 'colorPicker', component: ColorPickerComponent },
  { path: 'datatable', component: DataTableSampleComponent },
  { path: 'new-form', component: NewFormComponent },
  { path: 'tree-table', component: TreeTableComponent },
  { path: 'col-table', component: CollapsibleTableComponent },
  { path: 'col-sample', component: ColTableSampleComponent },
  { path: 'crop-image', component: CropImageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SampleRoutingModule {

  constructor(
    private translate: TranslateService,
    private exceToolbarService: ExceToolbarService,
    @Inject(APP_CONFIG) private config: IAppConfig
  ) {

    const langs = [];
    this.config.LANGUAGES.map(ln => {
      langs.push(ln.ID);
    })
    this.translate.addLangs(langs);
    translate.setDefaultLang(this.config.LANGUAGE_DEFAULT.ID);

    const lang = this.exceToolbarService.getSelectedLanguage();
    this.translate.use(lang.id);
    this.exceToolbarService.langUpdated.subscribe(
      (lang1) => {
        this.translate.use(lang.id);
      }
    );
  }
}
export const sampleRoutingComponents = [
  FormValidationComponent,
  CalandarTestComponent,
  DataGridSampleComponent,
  DataGridSampleComponent,
  ColorPickerComponent,
  DataTableSampleComponent,
  NewFormComponent,
  CollapsibleTableComponent,
  ColTableSampleComponent
]


