import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

import { OperationsRoutingModule } from './operations-routing.module';
import { OperationsRoutingComponents } from './operations-routing.module';
import { GenerateDailyMembersComponent } from './generate-daily-members/generate-daily-members.component';
import { OperationService } from '../operations/services/operation-service'
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { ExceCommonModule } from '../common/exce-common.module';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/modules/operations/', '.json');
}

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ExceCommonModule,
    OperationsRoutingModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    OperationsRoutingComponents,
    GenerateDailyMembersComponent,
  ],
  entryComponents: [
    GenerateDailyMembersComponent,
  ],
  providers: [OperationService,
  ]
})
export class OperationsModule { }
