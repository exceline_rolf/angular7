import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceLoginService } from '../../../modules/login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../shared/components/exce-message/exce-message.service';
import * as FileSaver from 'file-saver';
import { OperationService } from '../services/operation-service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
const now = new Date();

@Component({
  selector: 'app-generate-daily-members',
  templateUrl: './generate-daily-members.component.html',
  styleUrls: ['./generate-daily-members.component.scss']
})

export class GenerateDailyMembersComponent implements OnInit, OnDestroy {

  private branchId;
  private model: any;
  private selectedItem: any;
  private systemId: number;
  private destroy$ = new Subject<void>();
  constructor(
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService,
    private operationService: OperationService
  ) {

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;

    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;

      }
    );

    this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      if (value.id === 'CHANGE') {
        this.changedMembers();
      } else if (value.id === 'ARX') {
        this.aRXMembers();
      }
    });
  }

  ngOnInit() {
    this.operationService.getSystemId().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.systemId = result.Data;
      }
    })
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changedMembers() {
    // let uriContent = "data:application/octet-stream," + encodeURIComponent("content");
    // let newWindow = window.open(uriContent, 'neuesDokument');
    let changedMemberList = ''
    const xmlString = '<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>'
    let xmlList = ''
    const fileName = now.getFullYear() + '-' + now.getMonth() + '-' + now.getDate() + '.xml'
    this.operationService.getChangedList(this.systemId).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        changedMemberList = result;
        xmlList = xmlString + changedMemberList
        const blob = new Blob([xmlList], {
          type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-16le'
        });
        FileSaver.saveAs(blob, fileName);
      }
    })

  }

  aRXMembers() {
    // let uriContent = "data:application/octet-stream," + encodeURIComponent("content");
    // let newWindow = window.open(uriContent, 'neuesDokument');
    let changedMemberList = ''
    const xmlString = '<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>'
    let xmlList = ''
    const fileName = now.getFullYear() + '-' + now.getMonth() + '-' + now.getDate() + '.xml'
    this.operationService.getARXList(this.systemId).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        changedMemberList = result;
        xmlList = xmlString + changedMemberList
        const blob = new Blob([xmlList], {
          type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-16le'
        });
        FileSaver.saveAs(blob, fileName);
      }
    })

  }

  downloadChanged() {
    const fileName = now.getFullYear() + '-' + now.getMonth() + '-' + now.getDate()
    this.downloadChangedMessage('File Download- Security Warning', 'Do you want to save ' + fileName + '?')
  }
  downloadChangedMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('CONFIRM', {
      messageTitle: title,
      messageBody: body,
      msgBoxId: 'CHANGE'
      }
    );

  }

  downloadARX() {
    const fileName = now.getFullYear() + '-' + now.getMonth() + '-' + now.getDate()
    this.downloadARXMessage('File Download- Security Warning', 'Do you want to save ' + fileName + '?')
  }

  downloadARXMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: title,
        messageBody: body,
        msgBoxId: 'ARX'

      }
    );
  }

}
