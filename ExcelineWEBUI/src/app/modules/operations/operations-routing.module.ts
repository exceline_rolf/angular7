import { NgModule, Inject } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IAppConfig } from '../../app-config/app-config.interface';
import { APP_CONFIG } from '../../app-config/app-config.constants';

import { ExceToolbarService } from '../common/exce-toolbar/exce-toolbar.service';
import { GenerateDailyMembersComponent } from './generate-daily-members/generate-daily-members.component';
import { TranslateService } from '@ngx-translate/core';
const routes: Routes = [
  { path: '', component: GenerateDailyMembersComponent },
  { path: 'member-list-download', component: GenerateDailyMembersComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OperationsRoutingModule {
  constructor(
    private translate: TranslateService,
    private exceToolbarService: ExceToolbarService,
     @Inject(APP_CONFIG) private config: IAppConfig
  ) {
    const langs = [];
    this.config.LANGUAGES.map(ln => {
      langs.push(ln.ID);
    })
    this.translate.addLangs(langs);
    translate.setDefaultLang(this.config.LANGUAGE_DEFAULT.ID);

    const lang = this.exceToolbarService.getSelectedLanguage();
    this.translate.use(lang.id);
    this.exceToolbarService.langUpdated.subscribe(
      (lang1) => {
        this.translate.use(lang1.id);
      }
    );
  }


}
export const OperationsRoutingComponents = [
  GenerateDailyMembersComponent
]



