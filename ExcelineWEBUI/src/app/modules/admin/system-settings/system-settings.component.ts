import { Component, OnInit, ComponentFactoryResolver, ViewContainerRef, ViewChild, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { UsTabService } from '../../../shared/components/us-tab/us-tab.service'
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-system-settings',
  templateUrl: './system-settings.component.html',
  styleUrls: ['./system-settings.component.scss']
})
export class SystemSettingsComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  selectedTab;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private usTabService: UsTabService,
    private cdr: ChangeDetectorRef,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.SystemC',
      url: '/adminstrator/system-settings'
    });

    usTabService.verticleTabSelectionChanged.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      tabIndex => {
        this.selectedTab = tabIndex
      }
    )
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

}

