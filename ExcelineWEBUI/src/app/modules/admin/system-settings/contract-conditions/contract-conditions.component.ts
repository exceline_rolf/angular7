import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminService } from 'app/modules/admin/services/admin.service';
import { TranslateService } from '@ngx-translate/core';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { UsErrorService } from '../../../../shared/directives/us-error/us-error.service'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { element } from 'protractor';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
@Component({
  selector: 'contract-conditions',
  templateUrl: './contract-conditions.component.html',
  styleUrls: ['./contract-conditions.component.scss']
})
export class ContractConditionsComponent implements OnInit, OnDestroy {

  private contractConditions: any;
  items = [];
  private emailmodalReference: any;
  private editorValue: any;
  private isEdit = false;
  private templateForm: any;
  private formSubmited = false;
  private editItem: any;
  private branchId: number;
  private username: String;
  private destroy$ = new Subject<void>();

  formErrors = {
    'Name': '',
    'IsDefault': '',
  };

  validationMessages = {
    'Name': {
      'required': 'Name is required.',
    },

    'IsDefaultText': {
      'required': 'IsDefault is required.',
    },

  };

  constructor(private adminService: AdminService,
    private translateService: TranslateService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private exceLoginService: ExceLoginService,
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.username = this.exceLoginService.CurrentUser.username
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;

      }, null, null
    );
  }

  ngOnInit() {
    this.getConstractList()
    this.templateForm = this.fb.group({
      'Name': [null, [Validators.required]],
      'IsDefault': [null]
    });
    this.templateForm.statusChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(_ => {
      UsErrorService.onValueChanged(this.templateForm, this.formErrors, this.validationMessages)
    }, null, null);
  }

  ngOnDestroy() {
    if (this.emailmodalReference) {
      this.emailmodalReference.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  getConstractList() {
    this.items = []
    this.adminService.GetGymCompanySettings('CONTRACT').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      this.contractConditions = result.Data;
      let textYes = '';
      let textNo = '';
      (this.translateService.get('ADMIN.YES').subscribe(tranlstedValue => textYes = tranlstedValue));
      (this.translateService.get('ADMIN.NO').subscribe(tranlstedValue => textNo = tranlstedValue));
      // tslint:disable-next-line:no-shadowed-variable
      this.contractConditions.forEach(element => {
        if (element.IsDefault) {
          element.IsDefaultText = textYes;
        } else {
          element.IsDefaultText = textNo;
        }
        this.items.push(element);
      });
    }, null, null)
  }

  openConditionDeleteMessage() {
  }

  deleteContractCondition(rowEvent) {

  }

  openEmail(content, item?) {
    if (item) {
      this.isEdit = true
      this.editorValue = item.Condition;
      this.editItem = item;
      this.templateForm.patchValue(item)
    } else {
      this.editorValue = '';
      this.isEdit = false
    }
    this.emailmodalReference = this.modalService.open(content, { width: '990' });
  }

  AddUpdateGymCompanySettings(value) {

    if (this.isEdit) {
      // tslint:disable-next-line:prefer-const
      let saveObj = this.editItem
      saveObj.Name = value.Name
      saveObj.IsDefault = value.IsDefault
      saveObj.User = this.username
      saveObj.BranchId = this.branchId
      saveObj.Condition = this.editorValue
      this.adminService.SaveGymCompanyContractSettings(saveObj).pipe(
        takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result.Data) {
          this.emailmodalReference.close()
          this.getConstractList()

        }
      }, null)

    } else {
      const saveObj = {
        'Name': value.Name,
        'IsDefault': value.IsDefault,
        'User': this.username,
        'BranchId': this.branchId,
        'Condition': this.editorValue
      }
      this.adminService.SaveGymCompanyContractSettings(saveObj).pipe(
        takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result.Data) {
          this.emailmodalReference.close()
          this.getConstractList()
        }
      }, null, null)
    }


  }

  submitForm(value) {
    this.formSubmited = true;
    if (this.templateForm.valid) {
      this.AddUpdateGymCompanySettings(value)
      this.formSubmited = false;
    } else {
      UsErrorService.validateAllFormFields(this.templateForm, this.formErrors, this.validationMessages);
    }
  }


  deleteItem(item) {
    item.isDelete = true
    this.adminService.SaveGymCompanyContractSettings(item).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result.Data) {
        this.getConstractList()
      }
    }, null, null)
  }

}
