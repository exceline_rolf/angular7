import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminService } from 'app/modules/admin/services/admin.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'other-settings',
  templateUrl: './other-settings.component.html',
  styleUrls: ['./other-settings.component.scss']
})
export class OtherSettingsComponent implements OnInit, OnDestroy {

  popOver: any;
  private otherSetting: any;
  private contractCategoriesList = [];
  private conCategoryIdList = [];
  selectedCategotyCount: any;
  public otherSettingsForm: FormGroup;
  isBrisIntegrated: any;
  private destroy$ = new Subject<void>();

  constructor(private adminService: AdminService,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.adminService.GetGymCompanySettings('other').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      this.otherSetting = result.Data;
      this.getContractCategories();
      if (this.otherSetting.ExcludingCategories === '') {
        this.selectedCategotyCount = 0;
      }

      this.isBrisIntegrated = this.otherSetting.IsBrisIntegrated;

      this.otherSettingsForm.patchValue({
        Id: this.otherSetting.Id,
        ClassGeneratingDays: this.otherSetting.ClassGeneratingDays,
        NoOfCopiesForInvoice: this.otherSetting.NoOfCopiesForInvoice,
        NetsSendingNo: this.otherSetting.NetsSendingNo,
        CustomerNo: this.otherSetting.CustomerNo,
        ContractNo: this.otherSetting.ContractNo,
        ExcludingCategories: (this.otherSetting.ExcludingCategories === '') ? 0 : this.otherSetting.ExcludingCategories
      });
    }, null, null)

    this.otherSettingsForm = this.fb.group({
      'Id': [],
      'ClassGeneratingDays': [],
      'NoOfCopiesForInvoice': [],
      'NetsSendingNo': [],
      'CustomerNo': [],
      'ContractNo': [],
      'ExcludingCategories': [],
    })
  }

  saveOtherSettings() {
    let basicInfoId: any;
    let updatedExcludingCategories = '';

    this.contractCategoriesList.forEach(element => {
      if (element.IsCheck) {
        updatedExcludingCategories += element.Id + ',';
      }
    });

    this.otherSettingsForm.patchValue({
      ExcludingCategories: updatedExcludingCategories.substring(0, updatedExcludingCategories.length - 1),
    });

    this.adminService.saveGymCompanyOtherSettings(this.otherSettingsForm.value).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      basicInfoId = result.Data;
    }, null, null);
  }

  getContractCategories() {
    this.adminService.getCategories('PACKAGE').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      const contractCategories = result.Data;
      let conCategory: any;
      const excludingCategories = this.otherSetting.ExcludingCategories;
      if (this.otherSetting.ExcludingCategories === '') {
        this.selectedCategotyCount = 0;
      } else {
        this.conCategoryIdList = excludingCategories.split(',');
        this.selectedCategotyCount = this.conCategoryIdList.length;
      }

      contractCategories.forEach(item => {
        if (this.conCategoryIdList.indexOf(String(item.Id)) !== -1) {
          conCategory = item;
          conCategory.IsCheck = true;
          this.contractCategoriesList.push(conCategory);
        } else {
          conCategory = item;
          conCategory.IsCheck = false;
          this.contractCategoriesList.push(conCategory);
        }
      });
    }, null, null);
  }

  changeContractCategory() {
    this.selectedCategotyCount = (this.contractCategoriesList.filter(data => data.IsCheck === true)).length;
  }

  viewLoad(content) {
    if (this.popOver) {
      this.popOver.close();
    }
    this.popOver = content;
    this.popOver.open();
  }

  closePopOver() {
    this.popOver.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
