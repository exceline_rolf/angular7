import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityUnavailabletimeComponent } from './activity-unavailabletime.component';

describe('ActivityUnavailabletimeComponent', () => {
  let component: ActivityUnavailabletimeComponent;
  let fixture: ComponentFixture<ActivityUnavailabletimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityUnavailabletimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityUnavailabletimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
