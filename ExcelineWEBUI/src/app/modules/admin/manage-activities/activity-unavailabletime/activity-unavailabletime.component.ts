import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminService } from '../../services/admin.service';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { IMyDpOptions } from '../../../../shared/components/us-date-picker/interfaces';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import * as moment from 'moment';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil, flatMap } from 'rxjs/operators';
import { Subject } from 'rxjs';

const now = new Date();

@Component({
  selector: 'app-activity-unavailabletime',
  templateUrl: './activity-unavailabletime.component.html',
  styleUrls: ['./activity-unavailabletime.component.scss']
})
export class ActivityUnavailabletimeComponent implements OnInit, OnDestroy {
  public unavailableTimeForm: FormGroup;
  private isUnavailableTimes = true;
  items = [];
  itemCount = 0;
  manageActivities?: any[] = [];
  itemResource: DataTableResource<any>;
  activityTypes: any;
  private model: any;
  private deletedItem: any;
  private tempId = 0;
  private branchId: number;
  myFormSubmited = false;
  private destroy$ = new Subject<void>();

  startDatePickerOptions: IMyDpOptions = {
    disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 },
  };
  endDatePickerOptions: IMyDpOptions = {
  };
  formErrors = {
    'Startdate': '',
    'EndDate': '',
    'ActivityId': ''
  };
  validationMessages = {
    'Startdate': {
      'required': 'ADMIN.MsgStartDateNeeded'
    },
    'EndDate': {
      'required': 'ADMIN.MsgEndDateNeeded'
    },
    'ActivityId': {
      'required': 'ADMIN.MsgActivityNeeded'
    }
  };

  constructor(
    private adminService: AdminService,
    private ngbModal: UsbModal,
    private formbuilder: FormBuilder,
    private exceMessageService: ExceMessageService,
    private translateService: TranslateService,
    private exceLoginService: ExceLoginService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.ManageTimeC',
      url: '/adminstrator/activity-unavailabletime'
    });

    this.unavailableTimeForm = formbuilder.group({
      'Id': [null],
      'ActivityId': [null, [Validators.required]],
      'Startdate': [null, [Validators.required]],
      'EndDate': [null, [Validators.required]],
      'StartTime': [null],
      'EndTime': [null],
      'Reason': [null]
    });

    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.yesHandler();
    }, error => {

    }, () => {

    });

    this.exceMessageService.noCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.noHandler();
    }, error => {

    }, () => {

    });

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
    ).subscribe((branch) => {
        this.branchId = branch.BranchId;
    }, error => {

    }, () => {

    });
  }

  ngOnInit() {
    this.unavailableTimeForm.statusChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(() => {
      UsErrorService.onValueChanged(this.unavailableTimeForm, this.formErrors, this.validationMessages)
    }, error => {

    }, () => {

    });

    // get unavailable-time data for the data table
    this.adminService.getActivityTimes(this.isUnavailableTimes).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.items = result.Data;
        this.manageActivities = this.items;
        this.itemCount = this.manageActivities.length;
        this.itemResource = new DataTableResource(this.manageActivities);
        this.itemResource.count().then(count => this.itemCount = count);
      }
    }, error => {
        console.log(error);
    }, () => {

    });

    // get the activity names for the dropbox in unavailable-time
    this.adminService.getActivities().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.activityTypes = result.Data;
      }
    }, error => {
        console.log(error);
    }, () => {

    });
  }

  // set the valid dates to pick (date picker validations)
  onStartDateChange(date, end) {
    this.endDatePickerOptions = {
      disableUntil: date.date
    };
    end.showSelector = true;
  }

  // only push added data to the data table (not save in the db)
  addActivityTimes(value: any) {
    this.myFormSubmited = true;
    const unavailableItem = this.items.find(x => x.Id === value.Id);
    const selectedActivity = this.activityTypes.find(x => x.Id == value.ActivityId);

    if (value.StartTime !== null) {
      value.StartTime = '1900-01-01T' + value.StartTime + ':00';
    }
    if (value.EndTime !== null) {
      value.EndTime = '1900-01-01T' + value.EndTime + ':00';
    }

    if (this.unavailableTimeForm.valid) {
      if (!unavailableItem) {
        // add new activity
        this.items.push(
          {
            Id: this.tempId--, // keep a variable to identify edited data this will change dynamically
            ActivityId: selectedActivity.Id,
            ActivityName: selectedActivity.Name,
            Startdate: this.dateConverter(JSON.parse(JSON.stringify(value.Startdate))),
            EndDate: this.dateConverter(JSON.parse(JSON.stringify(value.EndDate))),
            StartTime: value.StartTime,
            EndTime: value.EndTime,
            BranchId: this.branchId,
            Reason: value.Reason
          });

      } else {
        // edit existing activity
        unavailableItem.ActivityId = value.ActivityId;
        unavailableItem.ActivityName = selectedActivity.Name;
        if (unavailableItem.Startdate.formatted) {
          unavailableItem.Startdate = this.dateConverter(JSON.parse(JSON.stringify(value.Startdate)));
        } else {
          unavailableItem.Startdate = this.getDateConverter(value.Startdate.date);
        }
        if (unavailableItem.EndDate.formatted) {
          unavailableItem.EndDate = this.dateConverter(JSON.parse(JSON.stringify(value.EndDate)));
        } else {
          unavailableItem.EndDate = this.getDateConverter(value.EndDate.date);
        }
        unavailableItem.StartTime = value.StartTime;
        unavailableItem.EndTime = value.EndTime;
        unavailableItem.BranchId = this.branchId;
        unavailableItem.Reason = value.Reason;
      }
      this.myFormSubmited = false;
      this.unavailableTimeForm.reset();
    } else {
      UsErrorService.validateAllFormFields(this.unavailableTimeForm, this.formErrors, this.validationMessages);
    }
  }

  // convert date js object in to string
  getDateConverter(date: any) {
    return date.year + '-' + date.month + '-' + date.day;
  }

  // convert date object in to string
  dateConverter(dateObj: any) {
    let dateS = '';
    if (dateObj) {
      if (dateObj.formatted) {
        const splited = dateObj.formatted.split('/')
        if (splited.length > 1) {
          dateS = splited[2] + '-' + splited[1] + '-' + splited[0] + 'T00:00:00'
        }
      }
    }
    return dateS;
  }

  // Load table row data to the form
  rowItemLoad(rowItem) {
    // set the date with our format to the text field
    const sDate = new Date(rowItem.Startdate);
    const eDate = new Date(rowItem.EndDate);

    this.unavailableTimeForm.patchValue({
      Id: rowItem.Id,
      ActivityId: rowItem.ActivityId,
      Startdate: {
        date: {
          year: sDate.getFullYear(),
          month: sDate.getMonth() + 1,
          day: sDate.getDate()
        }
      },
      EndDate: {
        date: {
          year: eDate.getFullYear(),
          month: eDate.getMonth() + 1,
          day: eDate.getDate()
        }
      },
      StartTime: [moment(new Date(rowItem.StartTime)).format('HH:mm')],
      EndTime: [moment(new Date(rowItem.EndTime)).format('HH:mm')],
      Reason: rowItem.Reason
    });
  }

  // save the unavailable times form data to the database
  saveUnavailableTimes(): any {
    const activityTimes = {
      ActivityTimeList: this.items
    }
    this.adminService.AddActivityTimes(activityTimes).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
    }, error => {
        console.log(error);
    }, () => {

    });
  }

  // reset the form fields after add and edit functions
  resetForm() {
    this.unavailableTimeForm.reset();
  }

  // delete unavailable times from the db
  deleteUnavailableTimes(item) {
    this.deletedItem = item;
    let messageTitle = '';
    let messageBody = '';
    (this.translateService.get('Confirm').pipe(
      takeUntil(this.destroy$)
    ).subscribe(tranlstedValue => messageTitle = tranlstedValue));
    (this.translateService.get('ADMIN.ConfirmMessage').pipe(
      takeUntil(this.destroy$)
    ).subscribe(tranlstedValue => messageBody = tranlstedValue));
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: messageTitle,
        messageBody: messageBody
      });
  }

  yesHandler() {
    const index: number = this.items.indexOf(this.deletedItem);
    this.deletedItem = null;

    if (index !== -1) {
      this.items.splice(index, 1);
    }
  }

  noHandler() {
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
