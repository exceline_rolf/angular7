import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceMemberService } from '../../membership/services/exce-member.service';
import { FormBuilder } from '@angular/forms';
import { Extension } from 'app/shared/Utills/Extensions';
import * as moment from 'moment';
import { AdminService } from 'app/modules/admin/services/admin.service';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { Guid } from '../../../shared/services/guid.service';
import { ExceLoginService } from './../../login/exce-login/exce-login.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-manage-sponsorships',
  templateUrl: './manage-sponsorships.component.html',
  styleUrls: ['./manage-sponsorships.component.scss']
})
export class ManageSponsorshipsComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  _dateFormat: string;
  locale: string;
  sponsors: any;
  sponsorDetailsForm: FormGroup;
  validationEnabled = false;
  rowTooltip: any;
  selectedGymID: any = -2;
  formErrors = {
    'startDueDate': '',
    'endDueDate': '',
    'visitStartDate': '',
    'visitEndDate': '',
    'includeOrderlines': '',
    'sponsoringDueDate': ''
  }

  validationMessages = {
    'startDueDate': {
      'required': 'ADMIN.PleaseSpecify'
    },
    'endDueDate': {
      'required': 'ADMIN.PleaseSpecify'
    },
    'visitStartDate': {
      'required': 'ADMIN.PleaseSpecify'
    },
    'visitEndDate': {
      'required': 'ADMIN.PleaseSpecify'
    },
    'includeOrderlines': {
      'required': 'ADMIN.PleaseSpecify'
    },
    'sponsoringDueDate': {
      'required': 'ADMIN.PleaseSpecify'
    }
  }

  branches: any[] = [];
  constructor(
    private memberService: ExceMemberService,
    private fb: FormBuilder,
    private adminService: AdminService,
    private toolbarService: ExceToolbarService,
    private exceBreadcrumbService: ExceBreadcrumbService,
    private exceLoginService: ExceLoginService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.SponsingC',
      url: '/adminstrator/manage-sponsorships'
    });
  }

  ngOnInit() {
    this.sponsorDetailsForm = this.fb.group(
      {
        'startDueDate': [null, Validators.required],
        'endDueDate': [null, Validators.required],
        'visitStartDate': [null],
        'visitEndDate': [null],
        'includeOrderlines': [null],
        'selectedbranch': [-2],
        'sponsoringDueDate': [{ date: Extension.GetFormatteredDate(new Date()) }, Validators.required]
      }
    );
    this.memberService.getBranches().pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.branches = result.Data;
        }
      }
    );
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getSponsors() {
    if (this.sponsorDetailsForm.valid) {
      //get sponsores
      const startDueDate: any = this.manipulatedate(this.sponsorDetailsForm.value.startDueDate);
      const endDueDate: any = this.manipulatedate(this.sponsorDetailsForm.value.endDueDate);
      this.adminService.getSponsors(this.selectedGymID, startDueDate, endDueDate).pipe(
        takeUntil(this.destroy$)
        ).subscribe(
        result => {
          if (result) {
            this.sponsors = result.Data;
            this.sponsors.forEach(element => {
              element.IsSelected = false;
            });
          }
        }
      );
    }else {
      this.validationEnabled = true;
      UsErrorService.validateAllFormFields(this.sponsorDetailsForm, this.formErrors, this.validationMessages);
    }
  }

  onBranchChanged(branchId: any) {
    this.selectedGymID = branchId;
  }

  manipulatedate(dateInput: any) {
    if (dateInput.date != undefined) {
      let tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }

  rowsSelected(event: any) {
    if (event.selected) {
      event.item.IsSelected = true;
    } else {
      event.item.IsSelected = false;
    }
  }


  generateSponsoring() {
    const branchId = this.exceLoginService.SelectedBranch.BranchId;
    let sponsorData;
    const sponsorIdList = [];
    const sessionKey = Guid.newGuid();
    this.sponsors.forEach(element => {
      if (element.IsSelected) { sponsorIdList.push(element.Id) }
    });

    sponsorData = {
      StartDueDate: this.sponsorDetailsForm.getRawValue().startDueDate.formatted,
      EndDueDate: this.sponsorDetailsForm.getRawValue().endDueDate.formatted,
      SponsoringDueDate: this.sponsorDetailsForm.getRawValue().sponsoringDueDate.date.day + '.'  + this.sponsorDetailsForm.getRawValue().sponsoringDueDate.date.month
       + '.' + this.sponsorDetailsForm.getRawValue().sponsoringDueDate.date.year,
      VisitStartDate: this.sponsorDetailsForm.getRawValue().visitStartDate.formatted,
      VisitEndDate: this.sponsorDetailsForm.getRawValue().visitEndDate.formatted,
      ShowOrderLines: this.sponsorDetailsForm.getRawValue().includeOrderlines,
      UserBranchId: this.sponsorDetailsForm.getRawValue().selectedbranch,
      SposoringBranchID: branchId,
      SposoringGuiId: sessionKey,
      PaymentDueDate: null,
      SelectedSponsors: sponsorIdList
    }
    console.log('SponsorData: ', sponsorData)
    let success = false;
    this.adminService.ExcelineManualSponsoringGeneration(sponsorData).subscribe(
      response => {
        if (response.Data) {
          if (response.Data === true) {
            success = true;
          } else {
            success = false
          }
        }
      } , err => success = false, () => {
        success ? console.log('Sponsoring Succeeded') : console.log('Sponsoring failed');
      });
    }
}
