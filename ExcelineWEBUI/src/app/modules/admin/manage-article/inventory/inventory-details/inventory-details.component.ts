import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { UsBreadcrumbService } from '../../../../../shared/components/us-breadcrumb/us-breadcrumb.service';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from '../../../services/admin.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { ExceLoginService } from '../../../../login/exce-login/exce-login.service';
import { PreloaderService } from '../../../../../shared/services/preloader.service';
import { ExcePdfViewerService } from '../../../../../shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { ExceToolbarService } from '../../../../common/exce-toolbar/exce-toolbar.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-inventory-details',
  templateUrl: './inventory-details.component.html',
  styleUrls: ['./inventory-details.component.scss']
})

export class InventoryDetailsComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  locale: string;
  private inventoryId: number;
  articleList: any[];
  private articleItems = [];
  itemCount = 0;
  private itemResource?: DataTableResource<any>;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  textValue: string;
  private branchId: number;
  auotocompleteItems = [
    { id: 'ArticleNo', name: 'Article No :', isNumber: true },
    { id: 'Name', name: 'Name :', isNumber: false },
    { id: 'Barcode', name: 'Barcode :', isNumber: false }
  ];

  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };
  @ViewChild('autobox') public autocomplete;
  constructor(
    private toolbarService: ExceToolbarService,
    private route: ActivatedRoute,
    private breadcrumbService: UsBreadcrumbService,
    private adminService: AdminService,
    private loginservice: ExceLoginService,
    private excePdfViewerService: ExcePdfViewerService,
    private exceMessageService: ExceMessageService
  ) {
    breadcrumbService.hideRoute('/adminstrator/inventory-home/inventory-detail');

  }

  ngOnInit() {
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';
    this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
    this.autocomplete.setSearchValue(this.auotocompleteItems);
    this.branchId = this.loginservice.SelectedBranch.BranchId;
    this.inventoryId = this.route.snapshot.params['Id'];
    this.adminService.GetInventoryDetail(this.inventoryId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      res => {
        this.articleList = res.Data;
        this.articleItems = this.articleList;
        this.itemResource = new DataTableResource(this.articleList);
        this.itemResource.count().then(count => this.itemCount = count);
        this.reloadArticleList({ offset: 0, limit: 50 })
      }, err => {
        console.log(err);
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  reloadArticleList(params) {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.articleList = items);
    }
  }

  searchArticles() {
    const sTextArray = this.autocomplete.value.split(':');
    let serachText;
    if (sTextArray.length > 1) {
      const findItem = this.auotocompleteItems.find(x => x.name.split(':')[0].trim() === sTextArray[0].trim())
      serachText = findItem.id + ' :' + sTextArray[1];
    } else {
      serachText = this.autocomplete.value;
    }
    this.searchInventoryDetail(serachText);
    const selectedSearchItem = this.autocomplete.getTextValue();
  }

  onSelectSearchItem(item: any) {
    this.searchInventoryDetail(item.searchText);
  }

  searchInventoryDetail(searchText: string) {
    const sTextArray = searchText.split(':');
    let seachFiled;
    let seachValue;
    let filteredArticlebySeachFiled = this.articleItems;
    if (sTextArray.length > 1) {
      seachFiled = sTextArray[0].trim();
      seachValue = sTextArray[1].trim();
      if (seachFiled === 'ArticleNo') {
        filteredArticlebySeachFiled = this.filterpipe.transform('ArticleNo', 'NAMEFILTER', filteredArticlebySeachFiled, seachValue);
      } else if (seachFiled === 'Name') {
        filteredArticlebySeachFiled = this.filterpipe.transform('Description', 'NAMEFILTER', filteredArticlebySeachFiled, seachValue);
      } else if (seachFiled === 'Barcode') {
        filteredArticlebySeachFiled = this.filterpipe.transform('Barcode', 'NAMEFILTER', filteredArticlebySeachFiled, seachValue);
      }
    } else {
      seachValue = searchText.trim();
      filteredArticlebySeachFiled = filteredArticlebySeachFiled.filter(
        item =>
          (item.ArticleNo && item.ArticleNo !== '' && item.ArticleNo.toString().toLowerCase().indexOf(seachValue.toString().toLowerCase()) !== -1)
          ||
          (item.Description && item.Description !== '' && item.Description.toString().toLowerCase().indexOf(seachValue.toString().toLowerCase()) !== -1)
          || (item.Barcode && item.Barcode !== '' && item.Barcode.toString().toLowerCase().indexOf(seachValue.toString().toLowerCase()) !== -1)
      )
    }
    this.itemResource = new DataTableResource(filteredArticlebySeachFiled);
    this.reloadArticleList(filteredArticlebySeachFiled);
  }

  filterArticle(numberValue?: any, nameValue?: any, barcodeValue?: any) {
    let filteredArticlebyNumber = this.articleItems;
    if (numberValue) {
      filteredArticlebyNumber = this.filterpipe.transform('ArticleNo', 'NAMEFILTER', filteredArticlebyNumber, numberValue);
    }
    let filteredArticlebyName = filteredArticlebyNumber;
    if (nameValue) {
      filteredArticlebyName = this.filterpipe.transform('Description', 'NAMEFILTER', filteredArticlebyName, nameValue);
    }
    let filteredArticlebyBarcoder = filteredArticlebyName;
    if (barcodeValue) {
      filteredArticlebyBarcoder = this.filterpipe.transform('BarCode', 'NAMEFILTER', filteredArticlebyBarcoder, nameValue);
    }
    this.itemResource = new DataTableResource(filteredArticlebyBarcoder);
    this.reloadArticleList(filteredArticlebyBarcoder);
  }

  countEnterEvent(countVal, selectedItem) {
    this.adminService.GetNetValueForArticle(this.inventoryId, selectedItem.Id, countVal, this.branchId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      res => {
        let countedVal;
        let netVal;
        for (const key in res.Data) {
          if (res.Data.hasOwnProperty(key)) {
            netVal = key;
            countedVal = res.Data[key];
            selectedItem.NetValue = netVal;
            selectedItem.CountedValue = countedVal;
            selectedItem.Deviation = countVal - selectedItem.StockLevel;
          }
        }
      }, err => {
        console.log(err);
      });
  }

  saveInventoryDetails() {
    const saveInventoryDetails = {
      InventoryId: this.inventoryId,
      ArticleList: this.articleList
    }
    PreloaderService.showPreLoader();
    this.adminService.SaveInventoryDetail(saveInventoryDetails).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data > 0) {
          this.adminService.GetInventoryDetail(this.inventoryId).pipe(
            takeUntil(this.destroy$)
            ).subscribe(
            result => {
              this.articleList = result.Data;
              this.articleItems = this.articleList;
              this.itemResource = new DataTableResource(this.articleList);
              this.itemResource.count().then(count => this.itemCount = count);
              this.reloadArticleList({ offset: 0, limit: 50 })
            }, err => {
              console.log(err);
            });
        }
      },
      err => {
        PreloaderService.hidePreLoader();
      });
  }

  printInventory() {
    PreloaderService.showPreLoader();
    this.adminService.PrintInventoryList(this.branchId, this.inventoryId).pipe(takeUntil(this.destroy$)).subscribe(result => {
      PreloaderService.hidePreLoader();
      if (result.Data.FileParth.trim() !== '') {
        // TO DO ee.IsPriview
        // this.excePdfViewerService.openPdfViewer('Exceline', 'http://www.pdf995.com/samples/pdf.pdf');
        this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
      }
    },
      err => {
        PreloaderService.hidePreLoader();
        // this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.MsgATGDocFailed' });
      });
  }

}
