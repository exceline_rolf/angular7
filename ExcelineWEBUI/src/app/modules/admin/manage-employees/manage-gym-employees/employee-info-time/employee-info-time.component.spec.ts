import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeInfoTimeComponent } from './employee-info-time.component';

describe('EmployeeInfoTimeComponent', () => {
  let component: EmployeeInfoTimeComponent;
  let fixture: ComponentFixture<EmployeeInfoTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeInfoTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeInfoTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
