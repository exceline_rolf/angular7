import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeInfoWorkComponent } from './employee-info-work.component';

describe('EmployeeInfoWorkComponent', () => {
  let component: EmployeeInfoWorkComponent;
  let fixture: ComponentFixture<EmployeeInfoWorkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeInfoWorkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeInfoWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
