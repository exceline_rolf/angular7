
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { AdminService } from '../../../services/admin.service'
import { EmployeeBasicInfoService } from '../employee-basic-info/employee-basic-info.service';
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { error } from 'util';
import { PreloaderService } from '../../../../../shared/services/preloader.service'
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { EntitySelectionType, ColumnDataType } from '../../../../../shared/enums/us-enum';
import * as moment from 'moment';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
const now = new Date();

@Component({
  selector: 'app-employee-info-work',
  templateUrl: './employee-info-work.component.html',
  styleUrls: ['./employee-info-work.component.scss']
})

export class EmployeeInfoWorkComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId = 1;
  private itemResource: any = 0;
  private scheduleItemResource: any = 0;
  private scheduleItems = [];
  private scheduleItemCount = 0;
  private workItems = [];
  public branches = [];
  private Categories: any;
  private filteredWork = [];
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private lastSelectedDate: any;
  private lastToDate: any;
  private currentEmployee: any;
  private selectedDate: any;
  private contractList = []
  private textComment = '';
  private newModal: any;
  private ContractItemList = [];
  private selectedCategoryString = '';
  private today;
  private entitySearchType = 'RES';
  private workDay = '';
  private selectedScheduleItem: any;

  public ResourceInfo: any;
  public rowTooltip: any;
  public items = [];
  public itemCount = 0;
  workView: any;
  resourceForm: any;
  selectedCategory: string;
  selectedBranch: number
  public formSubmited = false;
  public isThumbsUp = false
  entitySelectionView: any;
  entityItems: any;
  entityItemCount: any;
  selectedResource: any;
  selectedResouceDate: any;
  schedules: any;
  selectedDay: any;
  branch: any = null;

  formErrors = {
    'resource': '',
    'StartDate': ''
  };

  validationMessages = {
    'resource': {
      'required': 'Work date is required',
    },
    'StartDate': {
      'required': 'Branch is Required',
      'inValidDate': 'Please select a' + this.workDay + 'within this schedule period'
    },
  }

  columns = [
    { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  ];

  auotocompleteItems = [{ id: 'Name', name: 'Name :', isNumber: false },
  { id: 'Id', name: 'Id :', isNumber: true }]
  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };

  constructor(
    private employeeBasicInfoService: EmployeeBasicInfoService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private modalService: UsbModal,
    private fb: FormBuilder,
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (branch) => {
        this.branch = branch
        this.branchId = branch.BranchId;
      });
    this.adminService.getBranches(-1).pipe(
      takeUntil(this.destroy$)
    ).subscribe(branch => {
      if (branch.Data) {
        this.branches = branch.Data;
      }
    })
    this.adminService.getCategoriesByType('TIME').pipe(
      takeUntil(this.destroy$)
      ).subscribe(category => {
      if (category.Data) {
        this.Categories = category.Data;
      }
    })
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.selectedDate = { date: this.today }
  }

  ngOnInit() {
    // load the data to the table
    this.employeeBasicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
      ).subscribe(employee => {
        if (employee) {
          this.currentEmployee = employee
          this.getWorkItems()
        }
      });
    this.resourceForm = this.fb.group({
      'resource': [null],
      'StartDate': [null]
    });
    this.resourceForm.statusChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(_ => UsErrorService.onValueChanged(this.resourceForm, this.formErrors, this.validationMessages));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.workView) {
      this.workView.close();
    }
    if (this.entitySelectionView) {
      this.entitySelectionView.close();
    }
  }

  getWorkItems() {
    PreloaderService.showPreLoader();
    this.adminService.GetGymEmployeeWorkPlan(this.currentEmployee.Id).pipe(
      takeUntil(this.destroy$)
      ).subscribe(event => {
      if (event.Data) {
        this.workItems = event.Data
        this.itemResource = new DataTableResource(this.workItems);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.workItems;
        this.itemCount = Number(this.workItems.length);
        PreloaderService.hidePreLoader();
      } else {
        PreloaderService.hidePreLoader();
      }
    }, Error => {
      PreloaderService.hidePreLoader();
    });
  }

  filterInput(resourceVal: string, dayValue: string, branchValue: any, isApproved: any, DateVal?: any, ) {
    if (DateVal) {
      DateVal = DateVal.date.year + '-' + DateVal.date.month + '-' + DateVal.date.day
      this.lastSelectedDate = DateVal
    }
    this.filteredWork = this.workItems
    if (branchValue && branchValue !== 'ALL') {
      this.filteredWork = this.filterpipe.transform('BranchId', 'NAMEFILTER', this.filteredWork, branchValue);
    }
    if (this.lastSelectedDate && this.lastSelectedDate !== '' && this.lastSelectedDate != '0-0-0') {
      this.filteredWork = this.filterpipe.transform('StartDate', 'DATE_FILTER', this.filteredWork, this.lastSelectedDate);
    }
    if (dayValue && dayValue !== '') {
      this.filteredWork = this.filterpipe.transform('Day', 'NAMEFILTER', this.filteredWork, dayValue);
    }
    if (resourceVal && resourceVal !== '') {
      this.filteredWork = this.filterpipe.transform('Resource', 'NAMEFILTER', this.filteredWork, resourceVal);
    }

    this.filteredWork = this.filterpipe.transform('IsApproved', 'SELECT', this.filteredWork, isApproved);
    this.itemResource = new DataTableResource(this.filteredWork);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredWork);
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  openAddWorkView(content: any) {
    this.workView = this.modalService.open(content, { width: '800' });
  }

  changeDate(value) {
    this.selectedDate = value;
  }

  submitForm(value) {
    if (!this.selectedResource.Name) {

      this.resourceForm.controls['resource'].setErrors({
        'required': false
      });
    }
    if (value.StartDate && value.StartDate.jsdate) {
      let dayString = this.getDay(value.StartDate.jsdate.toString().split(' ')[0])
      this.scheduleItems.forEach(element => {
        if (element.isSelected) {
          this.workDay = element.Day
          this.selectedScheduleItem = element;
        }
      });

      if (dayString != this.workDay) {

        this.validationMessages.StartDate.inValidDate = 'Please select a ' + this.workDay + ' within this schedule period';
        this.resourceForm.controls['StartDate'].setErrors({
          'inValidDate': false
        });
      }
    } else {
      this.resourceForm.controls['StartDate'].setErrors({
        'required': false
      });
    }

    this.formSubmited = true;
    if (this.resourceForm.valid) {
      this.saveWork(value)
      this.formSubmited = false;
    } else {
      UsErrorService.validateAllFormFields(this.resourceForm, this.formErrors, this.validationMessages);
    }
  }

  getDay(val) {
    switch (val) {
      case 'Mon':
        return 'Monday'
      case 'Tue':
        return 'Tuesday'
      case 'Wed':
        return 'Wednesday'
      case 'Thu':
        return 'Thursday'
      case 'Fri':
        return 'Friday'
      case 'Sat':
        return 'Saturday'
      case 'Sun':
        return 'Sunday'
    }
  }

  saveWork(value) {
    let workObj = {
      EmployeeId: this.currentEmployee.Id,
      ScheduleItemId: this.selectedScheduleItem.Id,
      Date: this.dateConverter(value.StartDate),
      StartTime: this.selectedScheduleItem.StartTime,
      EndTime: this.selectedScheduleItem.EndTime,
      Resource: this.selectedResource.Name,
      Day: this.selectedScheduleItem.Day,
      IsEnabledApprove: true,
      BranchId: this.branchId,
    }

    this.adminService.SaveWorkItem({ 'work': workObj, 'branchId': this.branchId }).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result.Data) {
        this.closeModal()
        this.getWorkItems()
      }
    })
  }

  dateConverter(dateObj: any) {
    var dateS = '';
    if (dateObj) {
      if (dateObj.formatted) {
        var splited = dateObj.formatted.split('/')
        if (splited.length > 1) {
          dateS = splited[2] + '-' + splited[1] + '-' + splited[0] + 'T00:00:00'
        }
      }
    }
    return dateS
  }

  closeModal() {
    this.selectedScheduleItem = null
    this.selectedDay = ''
    this.selectedDate = null
    this.selectedResouceDate = null
    this.selectedResource = null
    this.scheduleItems = []
    this.resourceForm.reset();
    this.workView.close()
  }

  openEntitySelectionView(content: any) {
    this.entitySelectionView = this.modalService.open(content, { width: '990' });
  }

  searchDetail(val: any) {
    switch (val.entitySelectionType) {
      case EntitySelectionType[EntitySelectionType.RES]:
        this.adminService.GetResources(this.branchId, val.searchVal, -1, -1, 2, true
        ).pipe(takeUntil(this.destroy$)).subscribe
          (result => {
            this.entityItems = result.Data;
            this.entityItemCount = result.Data.length;
          }, null, null)
        break;
    }
  }

  selectResource(resource) {
    this.selectedResource = resource
    this.getResourceScheduleItems()
    this.entitySelectionView.close()
  }

  resDateChanged(event) {
    this.selectedResouceDate = event
  }

  getResourceScheduleItems() {
    this.adminService.GetResourceScheduleItems(this.selectedResource.Id, '').pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result.Data) {
        this.schedules = result.Data
        this.scheduleItemResource = new DataTableResource(this.schedules.SheduleItemList);
        this.scheduleItemResource.count().then(count => this.scheduleItemCount = count);
        this.scheduleItems = this.schedules.SheduleItemList;
        this.scheduleItemCount = Number(this.schedules.SheduleItemList.length);
        this.scheduleItems.forEach(item => {
          item.isSelected = false;
        })
        if (this.scheduleItems.length > 0) {
          this.scheduleItems[0].isSelected = true
        }
      }
    })
  }

  daySelect(event) {
    this.selectedDay = event
  }

  AdminApproveEmployeeWork(item) {
    const itemObj = {
      'EmployeeId': this.currentEmployee.Id,
      'ScheduleItemId': item.Id,
      'Date': item.StartDate,
      'StartTime': moment(item.StartTime).format('HH:mm'),
      'EndTime': moment(item.EndTime).format('HH:mm'),
      'IsApproved': item.IsApproved
    }

    this.adminService.AdminApproveEmployeeWork(itemObj).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
      }
    });
  }

}

