import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class GymEmployeeHomeService {


  public LoadFollowupsData = new Subject<any>();

  private selectedGymEmployee: any;
  /**
   * Getter $selectedGymEmployee
   * @return {any}
   */
  public get $selectedGymEmployee(): any {
    return this.selectedGymEmployee;
  }

  /**
   * Setter $selectedGymEmployee
   * @param {any} value
   */
  public set $selectedGymEmployee(value: any) {
    this.selectedGymEmployee = value;
  }

  private activeTimes: any;

  /**
   * Getter $activeTimes
   * @return {any}
   */
  public get $activeTimes(): any {
    return this.activeTimes;
  }

  /**
   * Setter $activeTimes
   * @param {any} value
   */
  public set $activeTimes(value: any) {
    this.activeTimes = value;
  }

  private jobTaskList: any;

  /**
   * Getter $jobTaskList
   * @return {any}
   */
  public get $jobTaskList(): any {
    return this.jobTaskList;
  }

  /**
   * Setter $jobTaskList
   * @param {any} value
   */
  public set $jobTaskList(value: any) {
    this.jobTaskList = value;
  }


  private followUpTaskList: any;

  /**
   * Getter $followUpTaskList
   * @return {any}
   */
  public get $followUpTaskList(): any {
    return this.followUpTaskList;
  }

  /**
   * Setter $followUpTaskList
   * @param {any} value
   */
  public set $followUpTaskList(value: any) {
    this.followUpTaskList = value;
  }



  constructor() { }

}
