import { TestBed, inject } from '@angular/core/testing';

import { GymEmployeeHomeService } from './gym-employee-home.service';

describe('GymEmployeeHomeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GymEmployeeHomeService]
    });
  });

  it('should be created', inject([GymEmployeeHomeService], (service: GymEmployeeHomeService) => {
    expect(service).toBeTruthy();
  }));
});
