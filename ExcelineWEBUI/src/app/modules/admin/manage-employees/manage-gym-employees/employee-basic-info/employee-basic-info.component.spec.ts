import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeBasicInfoComponent } from './employee-basic-info.component';

describe('EmployeeBasicInfoComponent', () => {
  let component: EmployeeBasicInfoComponent;
  let fixture: ComponentFixture<EmployeeBasicInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeBasicInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeBasicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
