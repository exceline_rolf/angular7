import { Injectable, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ExcelineMember } from '../../../../membership/models/ExcelineMember';


@Injectable()
export class EmployeeBasicInfoService {
  private _brisStatus = false;
  private _isEditMode = false;
  private _isImageEdit = false;
  private memberSource = new BehaviorSubject<ExcelineMember>(null);

  countries: any;
  notifyMethods: any;
  memberStatus: any;
  memberCategories: any;
  gymSettings: any;
  public SelectedContractId: number = -1;

  public currentMember = this.memberSource.asObservable();

  @Output() changeEvent: EventEmitter<any> = new EventEmitter();
  @Output() countriesEvent: EventEmitter<any> = new EventEmitter();
  @Output() notifyMethodEvent: EventEmitter<any> = new EventEmitter();
  @Output() memberStatusEvent: EventEmitter<any> = new EventEmitter();
  @Output() changeImageEvent: EventEmitter<any> = new EventEmitter();
  @Output() memberCategoryEvent: EventEmitter<any> = new EventEmitter();
  @Output() gymSettingEvent: EventEmitter<any> = new EventEmitter();
  @Output() addNewContractEvent: EventEmitter<any> = new EventEmitter();
  @Output() selectContractEvent: EventEmitter<any> = new EventEmitter();
  constructor() { }

  setSelectedMember(selectedMember: ExcelineMember) {
    if (selectedMember.BirthDay === (new Date('1990-01-01')).toDateString() || selectedMember.BirthDay ==='1900-01-01T00:00:00'){
      selectedMember.BirthDay = null;
      selectedMember.Age = 0;
    }
    this.memberSource.next(selectedMember);
  }

  /**
   * @deprecated Subscribe to EmployeeBasicInfoService.currentMember instead
   */
  getSelectedMember(): any {
/*     if (this._member.BirthDay === (new Date("1990-01-01")).toDateString() || this._member.BirthDay ==="1900-01-01T00:00:00"){
      this._member.BirthDay = null;
      this._member.Age = 0;
    }
    if (this._member !== null) {
      return this._member;
    } else {
      return null;
    } */
  }

  updateEditMode(mode: boolean, editMode: string) {
    switch (editMode) {
      case 'CARD': {
        this._isEditMode = mode;
        this.changeEvent.emit(this._isEditMode);
        break;
      }

    }
  }

  getEditMode() {
    return this.changeEvent;
  }


  getGymSettings() {
    return this.gymSettings;
  }

  setGymSettings(gymSettings: any) {
    this.gymSettings = gymSettings;
    this.gymSettingEvent.emit(this.gymSettings);
  }
}
