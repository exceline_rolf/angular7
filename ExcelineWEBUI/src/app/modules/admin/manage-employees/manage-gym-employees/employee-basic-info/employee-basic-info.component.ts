
import { Component, OnInit, OnDestroy } from '@angular/core';
import { EmployeeBasicInfoService } from './employee-basic-info.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ExceMemberService } from '../../../../membership/services/exce-member.service';
import { DataGridSampleComponent } from '../../../../sample/data-grid-sample/data-grid-sample.component';
import { PostalArea } from '../../../../../shared/SystemObjects/Common/PostalArea';
import { UsbModal } from '../../../../../shared/components/us-modal/us-modal';
import { configFactory } from '../../../../../app.module';
import { IMyDpOptions } from '../../../../../shared/components/us-date-picker/interfaces';
import { ExcelineMember } from '../../../../membership/models/ExcelineMember';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { TranslateService } from '@ngx-translate/core';
import { AdminService } from '../../../services/admin.service'
import { MemberCardService } from 'app/modules/membership/member-card/member-card.service';
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { Subject } from 'rxjs';
import { takeUntil, concatMap } from 'rxjs/operators';
const now = new Date();

@Component({
  selector: 'employee-basic-info',
  templateUrl: './employee-basic-info.component.html',
  styleUrls: ['./employee-basic-info.component.scss']
})
export class EmployeeBasicInfoComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private selectedStatus: any;
  private memberBirthdate: any;
  private loggedBranchId: number;
  private _myFormSubmited = false;
  private smsText = '';
  private imageUploaderModal: any;
  private selectedIndex = 0;

  dataUpdateMember: any;
  selectedEmployee: any;
  postalData?: PostalArea = new PostalArea();
  public employeeForm: FormGroup;
  public postalDataForm: FormGroup;
  countries: any;
  isEditMode: boolean;
  notifiMethods: any;
  memberStatus: any;
  postalDataView: any;
  smsTemplateView: any;
  _editImage = false;
  memberCategoies: any;
  smsTemplates: any;
  deleteButtonEnabled = true;
  gymSettings: any;
  branchId: number
  branches: any;
  exceEmpRoles: any;
  employeeRoles: any;
  showGyms = false;
  today: { year: number; month: number; day: number; };
  selectedRoles = 0;
  rolesPop
  homeGymPop
  selectedEmployeeClone: any
  birthDayStr: string
  startDateStr: string
  endDateStr: string;
  model: any;

  formErrors = {
    'LastName': '',
    'FirstName': '',
    'Address1': '',
    'Address2': '',
    'PostCode': '',
    'PostPlace': '',
    'Gender': '',
    'MemberCardNo': '',
    'MobilePrefix': '',
    'Mobile': '',
    'PrivateTeleNoPrefix': '',
    'PrivateTeleNo': '',
    'WorkTeleNoPrefix': '',
    'WorkTeleNo': '',
    'Email': '',
    'BirthDay': ''
  };

  validationMessages = {
    'LastName': {
      'required': 'ADMIN.AddEmLastNameReq'
    },
    'FirstName': {
      'required': 'ADMIN.AddEmFirstNameReq'
    },
    'CompanyName': {

    },
    'Email': {
      'pattern': 'email is not valid.'
    },

    'Address1': {

    },
    'PostCode': {

    },
    'Mobile': {

    }
  };

  constructor(
    private employeeBasicInfoService: EmployeeBasicInfoService,
    private memberService: ExceMemberService,
    private fb: FormBuilder,
    private modalService: UsbModal,
    private translateService: TranslateService,
    private memberCardService: MemberCardService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService,
  ) {

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

    this.loggedBranchId = JSON.parse(Cookie.get('selectedBranch')).BranchId;
    this.employeeBasicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
      ).subscribe((member: ExcelineMember) => {
      this.selectedEmployee = member
      this.birthDayStr = member.BirthDay;
      this.startDateStr = member.StartDate;
      this.endDateStr = member.EndDate;
      this.fetchData();
    });
  }

  ngOnInit() {
    this.employeeForm = this.fb.group({
      'LastName': [this.selectedEmployee.LastName, Validators.required],
      'FirstName': [this.selectedEmployee.FirstName, Validators.required],
      'Address1': [this.selectedEmployee.Address1],
      'Address2': [this.selectedEmployee.Address2],
      'PostCode': [this.selectedEmployee.PostCode],
      'PostPlace': [this.selectedEmployee.PostPlace],
      'Gender': [null],
      'MobilePrefix': [this.selectedEmployee.MobilePrefix],
      'Mobile': [this.selectedEmployee.Mobile],
      'PrivateTeleNoPrefix': [this.selectedEmployee.PrivateTeleNoPrefix],
      'Private': [this.selectedEmployee.PrivateTeleNo],
      'WorkTeleNoPrefix': [this.selectedEmployee.WorkTeleNoPrefix],
      'Work': [this.selectedEmployee.WorkTeleNo],
      'Email': [this.selectedEmployee.Email],
      'PayrollNumber': [this.selectedEmployee.PayrollNumber],
      'Description': [this.selectedEmployee.Description],
      'HomeGymName': [this.selectedEmployee.HomeGymName],
      'EmployeeCardNo': [this.selectedEmployee.EmployeeCardNo],
      'BirthDay': [null],
      'StartDate': [null],
      'EndDate': [null],
      'Age': [this.selectedEmployee.Age],
    });

    this.employeeForm.statusChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(_ => UsErrorService.onValueChanged(this.employeeForm, this.formErrors, this.validationMessages));

    this.employeeBasicInfoService.getEditMode().pipe(
      takeUntil(this.destroy$)
      ).subscribe(mode => {
      this.isEditMode = mode
      this.selectedEmployee.ExceEmpRoleList.forEach(element => {
        for (let empRole of this.exceEmpRoles) {
          if (empRole.Id == element.RoleId) {
            empRole.isSelected = true
            if (empRole.branches) {
              for (let branch of empRole.branches) {
                if (element.BranchList.indexOf(branch.Id) > -1) {
                  branch.IsCheck = true;
                }
              }
            }
          }
        }
      });
      if (this.selectedEmployee.BirthDay) {
        const tempDate = new Date(this.selectedEmployee.BirthDay);
        this.selectedEmployee.BirthDay = {
          date: {
            year: tempDate.getFullYear(),
            month: tempDate.getMonth() + 1,
            day: tempDate.getDate()
          }
        };
      }

      if (this.selectedEmployee.StartDate) {

        const tempDate = new Date(this.selectedEmployee.StartDate);
        this.selectedEmployee.StartDate = {
          date: {
            year: tempDate.getFullYear(),
            month: tempDate.getMonth() + 1,
            day: tempDate.getDate()
          }
        };
      } else {
        const tempDate = new Date();
        this.selectedEmployee.StartDate = {
          date: {
            year: tempDate.getFullYear(),
            month: tempDate.getMonth() + 1,
            day: tempDate.getDate()
          }
        };
      }
      if (this.selectedEmployee.EndDate) {

        const tempDate = new Date(this.selectedEmployee.EndDate);
        this.selectedEmployee.EndDate = {
          date: {
            year: tempDate.getFullYear(),
            month: tempDate.getMonth() + 1,
            day: tempDate.getDate()
          }
        };
      }

      if (this.isEditMode) {
        setTimeout(() => {
          this.employeeForm.patchValue(this.selectedEmployee)
        }, 100);
      } else {
        this.selectedEmployee.BirthDay = this.birthDayStr
        this.selectedEmployee.StartDate = this.startDateStr
        this.selectedEmployee.EndDate = this.endDateStr
        this.fetchData();
      }
    });
  }

  fetchData() {
    if (this.selectedEmployee) {
      if (this.selectedEmployee.ExceEmpRoleList) {
        this.selectedRoles = this.selectedEmployee.ExceEmpRoleList.length;
      } else {
        this.selectedRoles = 0;
      }
      // this.selectedEmployee.Age = (this.selectedEmployee.Age == 'NaN') ? '' : this.selectedEmployee.Age;
      this.selectedEmployee.Age = this.getAge(this.selectedEmployee.BirthDay, true);
      this.getRoles();
    }
  }

  getRoles() {
    this.adminService.getBranches(-1).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result.Data) {
        this.branches = result.Data;
        this.adminService.GetExcelineEmpRoles().pipe(
          takeUntil(this.destroy$)
          ).subscribe(resultEmp => {
          if (result.Data) {
            this.exceEmpRoles = resultEmp.Data.slice(1, resultEmp.Data.length);
            this.exceEmpRoles.forEach(empRole => {
              empRole.isSelected = false;
              const clonedBranches = this.branches.map(x => Object.assign({}, x));
              empRole.branches = clonedBranches;
              empRole.RoleId = empRole.Id
            });
          }
        })
      }
    })

    this.memberService.getEmployeeRoles().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        this.employeeRoles = result.Data;
      }
    })
  }

  changeHomeGym(event, branch) {
    if (event.target.value) {
      this.selectedEmployee.HomeBranchId = branch.Id
      this.selectedEmployee.HomeGymName = branch.BranchName
      this.employeeForm.controls['HomeGymName'].setValue(branch.BranchName);
    }
  }

  saveEmployee(value) {
    let isValid = true;
    let exceEmpRolesClone = this.exceEmpRoles.filter(x => x.isSelected == true)
    let EmpRoleList = []
    exceEmpRolesClone.forEach(element => {

      element.IsActive = true
      delete element.isSelected;
      EmpRoleList.push(element.RoleId)
      let branchCount = 0
      element.branches.forEach(branch => {
        if (branch.IsCheck) {
          element.BranchList.push(branch.Id)
          branchCount++;
        }
      });
      if (branchCount < 1) {
        isValid = false;
      }
      delete element.branches;
    });
    if (isValid) {
      value.EmpRoleList = EmpRoleList
      value.ExceEmpRoleList = exceEmpRolesClone
      value.BranchId = this.branchId;
      if (value.BirthDay && value.BirthDay.date) {
        value.BirthDay = value.BirthDay.date.year + '-' + value.BirthDay.date.month + '-' + value.BirthDay.date.day
      }
      if (value.StartDate && value.StartDate.date) {
        value.StartDate = value.StartDate.date.year + '-' + value.StartDate.date.month + '-' + value.StartDate.date.day
      }
      if (value.EndDate && value.EndDate.date) {
        value.EndDate = value.EndDate.date.year + '-' + value.EndDate.date.month + '-' + value.EndDate.date.day
      }
      for (let key in value) {
        if (value[key] || value[key] != 0) {
          this.selectedEmployee[key] = value[key]
        }
      }
      this.adminService.SaveGymEmployee(this.selectedEmployee).pipe(
        concatMap(() => this.adminService.GetGymEmployeeByEmployeeId(this.branchId, this.selectedEmployee.Id)),
        takeUntil(this.destroy$)
      ).subscribe( (resultSuccess) => {
        if (resultSuccess.Data) {
          this.employeeBasicInfoService.setSelectedMember(resultSuccess.Data)
          this.employeeBasicInfoService.updateEditMode(false, 'CARD');
        }
      })
/*       this.adminService.SaveGymEmployee(this.selectedEmployee).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result.Data) {
          this.adminService.GetGymEmployeeByEmployeeId(this.branchId, this.selectedEmployee.Id).pipe(takeUntil(this.destroy$)).subscribe
            (
            resultSuccess => {
              if (resultSuccess.Data) {
                this.employeeBasicInfoService.setSelectedMember(resultSuccess.Data)
                this.employeeBasicInfoService.updateEditMode(false, 'CARD');
              }
            });
        }
      }, null, null) */
    } else {
      this.selectBranchMessage('Confirm', 'There is a role without selecting branch,please select the branch')
    }
  }

  updateNameFormat(name: string, nameType: string) {
    let formatedName = '';
    if (typeof name !== 'undefined' && name) {
      for (let i = 0; i < name.length; i++) {
        if (i > 0) {
          formatedName = name[i - 1] === '-' || name[i - 1] === ' ' || name[i - 1] === '&' || name[i - 1] === '/' ?
            formatedName + name[i].toUpperCase() : formatedName + name[i].toLowerCase();
        } else {
          formatedName += name[i].toUpperCase();
        }
        if (nameType === 'FNAME') {
          this.employeeForm.patchValue({ FirstName: formatedName });
        } else if (nameType === 'LNAME') {
          this.employeeForm.patchValue({ LastName: formatedName });
        }
      }
    }
  }

  openRolesPop(content) {
    this.rolesPop = content
    if (this.homeGymPop) {
      this.homeGymPop.close();
    }
    this.rolesPop.open();
  }

  closeRolesPop() {
    this.rolesPop.close()
  }

  openHomeGymPop(content) {
    this.homeGymPop = content
    if (this.rolesPop) {
      this.rolesPop.close();
    }
    this.homeGymPop.open();
  }

  closeHomeGymPop() {
    this.homeGymPop.close()
  }

  getAge(value, isJsDate?) {
    if (value !== null) {
      let BirthDay;
      if (isJsDate) {
        BirthDay = value
      } else {
        BirthDay = this.dateConverter(value)
      }
      const tempDate = new Date(BirthDay);
      let diff = Date.now() - Date.parse(BirthDay);
      const age = Math.floor((diff / (1000 * 3600 * 24)) / 365);
      if (isJsDate) {
        return age;
      } else {
        this.employeeForm.controls['Age'].setValue(age)
      }
    }
  }

  submitForm(empInfoFormVal) {
    UsErrorService.validateAllFormFields(this.employeeForm, this.formErrors, this.validationMessages)
    if (this.employeeForm.valid) {
      this.saveEmployee(empInfoFormVal)
    }
  }

  dateConverter(dateObj: any) {
    var dateS = '';
    if (dateObj) {
      if (dateObj.formatted) {
        var splited = dateObj.formatted.split('/')
        if (splited.length > 1) {
          dateS = splited[2] + '-' + splited[1] + '-' + splited[0] + 'T00:00:00'
        }
      }
    }
    return dateS
  }

  cancelUpdate() {
    this.employeeBasicInfoService.updateEditMode(false, 'CARD');
  }


  selectBranchMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('ERROR',
      {
        messageTitle: 'ERROR',
        messageBody: body,
        msgBoxId: 'SELECT_BRANCH'
      }
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
