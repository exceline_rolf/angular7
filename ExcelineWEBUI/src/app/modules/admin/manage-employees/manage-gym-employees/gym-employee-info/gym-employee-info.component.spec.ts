import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymEmployeeInfoComponent } from './gym-employee-info.component';

describe('GymEmployeeInfoComponent', () => {
  let component: GymEmployeeInfoComponent;
  let fixture: ComponentFixture<GymEmployeeInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymEmployeeInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymEmployeeInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
