import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeInfoEventsComponent } from './employee-info-events.component';

describe('EmployeeInfoEventsComponent', () => {
  let component: EmployeeInfoEventsComponent;
  let fixture: ComponentFixture<EmployeeInfoEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeInfoEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeInfoEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
