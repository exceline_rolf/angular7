
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { AdminService } from '../../../services/admin.service'
import { EmployeeBasicInfoService } from '../employee-basic-info/employee-basic-info.service';
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { error } from 'util';
import { PreloaderService } from '../../../../../shared/services/preloader.service';
import { takeUntil, concatMap, map } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-employee-info-events',
  templateUrl: './employee-info-events.component.html',
  styleUrls: ['./employee-info-events.component.scss']
})

export class EmployeeInfoEventsComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private itemResource: any = 0;
  private eventItems = []
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();

  public branches = []
  private filteredEvents = []
  public branchId = 1;
  public items = [];
  public itemCount = 0;
  public rowTooltip: any;

  // @ViewChild('followUpProfile') public followUpProfile;
  // @ViewChild('updateMember') public updateMember;

  constructor(
    private employeeBasicInfoService: EmployeeBasicInfoService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      });
    this.adminService.getBranches(-1).pipe(
      takeUntil(this.destroy$)
      ).subscribe(branch => {
      if (branch.Data) {
        this.branches = branch.Data;
      }
    })
  }

  ngOnInit() {
    // load the data to the table
    this.employeeBasicInfoService.currentMember.pipe(
      concatMap(currentEmployee => this.adminService.GetEmployeeEvents(currentEmployee.Id)),
      map(result => result.Data),
      takeUntil(this.destroy$)
    ).subscribe(employeeEvents => {
      this.eventItems = employeeEvents;
      this.itemResource = new DataTableResource(this.eventItems);
      this.itemResource.count().then(count => this.itemCount = count);
      this.items = this.eventItems;
      this.itemCount = Number(this.eventItems.length);
      PreloaderService.showPreLoader();
    }, null, null);

/*     this.employeeBasicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      employee => {
        if (employee) {
          this.adminService.GetEmployeeEvents(employee.Id).pipe(takeUntil(this.destroy$)).subscribe(event => {
            if (event.Data) {
              this.eventItems = event.Data
              this.itemResource = new DataTableResource(this.eventItems);
              this.itemResource.count().then(count => this.itemCount = count);
              this.items = this.eventItems;
              this.itemCount = Number(this.eventItems.length);
              PreloaderService.showPreLoader();
            //  PreloaderService.hidePreLoader();
            }else{
        //    PreloaderService.hidePreLoader();
            }
          }, Error => {
        //    PreloaderService.hidePreLoader();
          }
          );

        }
      }); */
  }

  filterInput(statusValue: string, codeValue: string, branchValue: any) {
    this.filteredEvents = this.eventItems
    if (branchValue && branchValue !== 'ALL') {
      this.filteredEvents = this.filterpipe.transform('BranchName', 'NAMEFILTER', this.filteredEvents, branchValue.split('_')[1]);

    }

    if (statusValue && statusValue !== '') {

      this.filteredEvents = this.filterpipe.transform('Status', 'NAMEFILTER', this.filteredEvents, statusValue);

    }

    if (codeValue && codeValue !== '') {

      this.filteredEvents = this.filterpipe.transform('Code', 'NAMEFILTER', this.filteredEvents, codeValue);

    }

    this.itemResource = new DataTableResource(this.filteredEvents);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredEvents);
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}




