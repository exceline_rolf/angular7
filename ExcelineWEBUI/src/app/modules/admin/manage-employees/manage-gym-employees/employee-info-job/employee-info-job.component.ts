
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { AdminService } from '../../../services/admin.service'
import { EmployeeBasicInfoService } from '../employee-basic-info/employee-basic-info.service';
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { error } from 'util';
import { PreloaderService } from '../../../../../shared/services/preloader.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ExcelineMember } from 'app/modules/membership/models/ExcelineMember';
const now = new Date();

@Component({
  selector: 'app-employee-info-job',
  templateUrl: './employee-info-job.component.html',
  styleUrls: ['./employee-info-job.component.scss']
})

export class EmployeeInfoJobComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId = 1;
  private jobItems = []
  private itemResource: any = 0;
  private filteredEvents = []
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();

  public items = [];
  public itemCount = 0;
  public branches = []
  public jobCategories: any
  public rowTooltip: any;

  // @ViewChild('followUpProfile') public followUpProfile;
  // @ViewChild('updateMember') public updateMember;

  constructor(
    private employeeBasicInfoService: EmployeeBasicInfoService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;

      });
    this.adminService.getBranches(-1).pipe(
      takeUntil(this.destroy$)
      ).subscribe(branch => {
      if (branch.Data) {
        this.branches = branch.Data;
      }
    })

    this.adminService.GetJobCategories(this.branchId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(category => {
      if (category.Data) {
        this.jobCategories = category.Data;
      }
    })
  }

  ngOnInit() {
    // load the data to the table
    let currentMember: ExcelineMember;
    this.employeeBasicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
      ).subscribe( (employee: ExcelineMember) => {
        currentMember = employee;
      });

      PreloaderService.showPreLoader();
      this.adminService.GetEmployeeJobs(currentMember.Id).pipe(
        takeUntil(this.destroy$)
        ).subscribe(event => {
        if (event.Data) {
          this.jobItems = event.Data
          // const today = now.getFullYear() + '-' + now.getMonth() + 1 + '-' + now.getDate()
        this.jobItems.forEach(jobItem => {
          if (Date.now() > Date.parse(jobItem.JobEndDate)) {
            jobItem.Status = 'OLD';
          }else if (Date.now() < Date.parse(jobItem.JobStartDate)) {
            jobItem.Status = 'PENDING';
          } else {
            jobItem.Status = 'ACTIVE';
          }
          });
        this.itemResource = new DataTableResource(this.jobItems);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.jobItems;
        this.itemCount = Number(this.jobItems.length);
        PreloaderService.hidePreLoader();
      } else {
          PreloaderService.hidePreLoader();
        }
      }, Error => {
    PreloaderService.hidePreLoader();
  }, null);
};



filterInput(catValue: string, statusValue: string, branchValue: any) {
  this.filteredEvents = this.jobItems
  if (branchValue && branchValue !== 'ALL') {
    this.filteredEvents = this.filterpipe.transform('BranchName', 'NAMEFILTER', this.filteredEvents, branchValue.split('_')[1]);

  }

  if (statusValue && statusValue !== '') {

    this.filteredEvents = this.filterpipe.transform('Status', 'NAMEFILTER', this.filteredEvents, statusValue);

  }

  if (catValue && catValue !== 'ALL') {

    this.filteredEvents = this.filterpipe.transform('JobCategoryId', 'SELECT', this.filteredEvents, catValue);

  }

  this.itemResource = new DataTableResource(this.filteredEvents);
  this.itemResource.count().then(count => this.itemCount = count);
  this.reloadItems(this.filteredEvents);
}

reloadItems(params): void {
  if (this.itemResource) {
    this.itemResource.query(params).then(items => this.items = items);
  }
}

ngOnDestroy(): void {
  this.destroy$.next();
  this.destroy$.complete();
}

}





