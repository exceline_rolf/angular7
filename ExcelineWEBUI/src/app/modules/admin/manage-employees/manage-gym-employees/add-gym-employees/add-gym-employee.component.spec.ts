import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGymEmployeeComponent } from './add-gym-employee.component';

describe('AddGymEmployeeComponent', () => {
  let component: AddGymEmployeeComponent;
  let fixture: ComponentFixture<AddGymEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddGymEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGymEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
