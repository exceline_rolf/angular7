import { Component, OnInit, Input, Output, ViewChild, ElementRef, EventEmitter, OnDestroy } from '@angular/core';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule, AbstractControl } from '@angular/forms';
import { AdminService } from '../../../services/admin.service'
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { element } from 'protractor';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { forEach } from '@angular/router/src/utils/collection';
import { UsErrorService } from '../../../../../shared/directives/us-error/us-error.service'
import { EntitySelectionType, ColumnDataType } from '../../../../../shared/enums/us-enum';
import { ExceMemberService } from '../../../../membership/services/exce-member.service'
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { Observable } from 'rxjs';
import { CropperSettings } from '../../../../../shared/components/us-crop-image/cropperSettings';
import { UsCropImageComponent } from '../../../../../shared/components/us-crop-image/us-crop-image.component';
import { Bounds } from '../../../../../shared/components/us-crop-image/model/bounds';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil, debounceTime, take, map, first } from 'rxjs/operators';
import { Subject, of } from 'rxjs';
import { debugOutputAstAsTypeScript } from '@angular/compiler';
const now = new Date();

@Component({
  selector: 'add-gym-employee',
  templateUrl: './add-gym-employee.component.html',
  styleUrls: ['./add-gym-employee.component.scss']
})


export class AddGymEmployeeComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId: number;
  private entityItems: any;
  private itemCount: any;
  private exceEmpRoles: any;
  private branches: any;
  private exceEmpRoleList;
  private showGyms: false;
  private gymSettings = []
  private currentUser: any;
  private employeeRoles: any;
  private homeGym: any;
  private today: NgbDateStruct;
  private CategoryList: ElementRef;
  private openMemberSelection: any;
  private entitySearchType: any;
  private isAddressRequired = false
  private isMobileNumberValid = true;
  private isWorkNumberValid = true;
  private isPrivateNumberValid = true;
  private phoneNumberDetail: string;
  private model: any;
  private formValid: boolean;
  public empInfoForm: any;
  public selectedRole: any;
  @ViewChild('CategoryList')
  selectedGender = 'MALE'
  formSubmited = false;
  selectedRoles = 0
  ageValue = 0;
  isMobileRequired = true;
  isEmailRequired = true;
  isZipRequired = false
  isFirstNameRequired = true;
  isLastNameRequired = true;

  @Output() savedSuccess = new EventEmitter();
  @Output() canceled = new EventEmitter();

  formErrors = {
    'FirstName': '',
    'LastName': '',
    'Address1': '',
    'Mobile': '',
    'PostCode': '',
    'Work': '',
    'Private': '',
    'Email': '',
    'Gender': ''
  };

  validationMessages = {
    'LastName': { 'required': 'value required.', },
    'FirstName': { 'required': 'value required.', },
    'Address1': { 'required': 'value required.', },
    'Mobile': { 'required': 'value required.', 'phoneNumber': 'invalid phone number' },
    'PostCode': { 'required': 'value required.', },
    'Work': { 'phoneNumberW': 'invalid phone number' },
    'Private': { 'phoneNumberP': 'invalid phone number' },
    'Email': { 'email': 'invalid email address' },
  };

  columns = [
    { property: 'FirstName', header: 'First Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'LastName', header: 'Last Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'Number', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'Age', header: 'Age', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'Mobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'Status', header: 'Status', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'GymName', header: 'Gym Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
  ];

  auotocompleteItems = [{ id: 'Name', name: 'Name :', isNumber: false },
  { id: 'Id', name: 'Id :', isNumber: true }]
  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };

  private _selectedGymSettings: string[] = [
    'ReqAddressRequired',
    'ReqZipCodeRequired',
    'ReqMobileRequired'
  ]


  /* **********************************************************************
   * variables for cropping image
   * Tue Apr 03 2018 11:16:51 GMT+0530 (Sri Lanka Standard Time)
  **********************************************************************/
 atDecition = true;
 useWebcam = false;
 useFileUpload = false;
 reader = new FileReader();
 name: string;
 data1: any;
 cropperSettings1: CropperSettings;
 croppedWidth: number;
 croppedHeight: number;
 @ViewChild('cropper', undefined) cropper: UsCropImageComponent;
  countries: any;
  listOfCountryId: any;
  intlOptions: { initialCountry: string; formatOnDisplay: boolean; separateDialCode: boolean; onlyCountries: any; autoPlaceholder: string; };
 /////////////////////////////////////

  constructor(
    private modalService: UsbModal,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private memberService: ExceMemberService,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private fb: FormBuilder) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
    this.currentUser = this.exceLoginService.CurrentUser
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    /***********************************************************************
    * initialize values for cropping image
    * Tue Apr 03 2018 11:16:51 GMT+0530 (Sri Lanka Standard Time)
    **********************************************************************/
   this.cropperSettings1 = new CropperSettings();
   this.cropperSettings1.width = 100;
   this.cropperSettings1.height = 100;

   this.cropperSettings1.croppedWidth = 200;
   this.cropperSettings1.croppedHeight = 200;

   this.cropperSettings1.canvasWidth = 200;
   this.cropperSettings1.canvasHeight = 200;

   this.cropperSettings1.minWidth = 10;
   this.cropperSettings1.minHeight = 10;

   this.cropperSettings1.rounded = false;
   this.cropperSettings1.keepAspect = false;

   this.cropperSettings1.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
   this.cropperSettings1.cropperDrawSettings.strokeWidth = 2;

   this.data1 = {};
    //////////////////////////////////////////////////////////////////////

  }

  ngOnInit() {
    this.entitySearchType = 'MEM';
    this.empInfoForm = this.fb.group({
      'Gender': [null],
      'LastName': [null, Validators.required],
      'FirstName': [null, Validators.required],
      'Description': [null],
      'BirthDay': [null],
      'Age': [null],
      'Address1': [null],
      'Address2': [null],
      'PostCode': [null],
      'PostPlace': [null],
      'MobilePrefix': [null],
      'Mobile': [null, [Validators.required], [this.mobileNumberValidator.bind(this)]],
      'PrivateTeleNoPrefix': [null],
      'Private': [null, [Validators.nullValidator], [this.privateNumberValidator.bind(this)]],
      'WorkTeleNoPrefix': [null],
      'Work': [null, [Validators.nullValidator], [this.workNumberValidator.bind(this)]],
      'Email': [null, [Validators.email]],
      'StartDate': [{ date: this.today }],
      'EndDate': [null],
      'EmployeeCardNo': [null],
      'CustId': [null],
      'PayrollNumber': [null],
      'HomeGymName': [null]
    });

    this.getRoles();
    this.getSelectedGymSetting();
    this.memberService.getCountries().pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.countries = result.Data;
          this.countries.forEach(element => {
            let tempName;
            this.listOfCountryId.push(element);
            const translateExpression = 'COUNTRIES.' + element.Id;
            this.translateService.get(translateExpression).pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => tempName = tranlstedValue);
            element.DisplayName = tempName;
          });
        }
      });

      this.intlOptions = {
        initialCountry: 'no',
        formatOnDisplay: true,
        separateDialCode: true,
        onlyCountries: this.listOfCountryId,
        autoPlaceholder: 'off'
      };

  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.openMemberSelection) {
      this.openMemberSelection.close();
    }
  }

  /***********************************************************************
   * functions for cropping image
   * Tue Apr 03 2018 11:16:51 GMT+0530 (Sri Lanka Standard Time)
   **********************************************************************/
  wantToUploadFile() {
    this.atDecition = false;
    this.useFileUpload = true;
    this.useWebcam = false;
  }

  wantToUseWebCam() {
    this.atDecition = false;
    this.useFileUpload = false;
    this.useWebcam = true;
  }

  cropped(bounds: Bounds) {
    this.croppedHeight = bounds.bottom - bounds.top;
    this.croppedWidth = bounds.right - bounds.left;
  }

  fileChangeListener($event) {
    const image = new Image();
    const file = $event.target.files[0];
    const myReader = new FileReader();
    const that = this;
    myReader.onloadend = function (loadEvent: any) {
        image.src = loadEvent.target.result;
        that.cropper.setImage(image);
    };
    myReader.readAsDataURL(file);
  }

  cancelAction() {
    this.atDecition = true;
    this.useFileUpload = false;
    this.useWebcam = false;
    this.data1 = {};
  }

  ////////////////////////////////////////////////////////////////////////
  getSelectedGymSetting() {
    // let settingsList: string[] = ['ReqAddressRequired'];
    // // settingsList.push('ReqAddressRequired')
    // settingsList.push('ReqZipCodeRequired')
    // settingsList.push('ReqMobileRequired')
    this.adminService.GetSelectedGymSettingsPost({ GymSetColNames: this._selectedGymSettings, IsGymSettings: true, BranchId: this.branchId }).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        const settings = result.Data
        this.isAddressRequired = settings.ReqAddressRequired3
        this.isMobileRequired = settings.ReqMobileRequired
        this.isZipRequired = settings.ReqZipCodeRequired

      }
    })
  }

  openMemberSelectionModel(content: any) {
    this.openMemberSelection = this.modalService.open(content);
  }

  // openEmployeeInfoModal(content: any) {
  //   this.openEmpInfoView = this.modalService.open(content, { width: '1400' });
  // }

  searchDeatail(val: any) {
    switch (val.entitySelectionType) {
      case EntitySelectionType[EntitySelectionType.MEM]:

        this.memberService.getMembers(val.branchSelected.BranchId, val.searchVal, val.statusSelected.id,
          'LINKEMPLOYEE', val.roleSelected.id, val.hit, false,
          false, ''
        ).pipe(takeUntil(this.destroy$)).subscribe
          (result => {
            this.entityItems = result.Data;
            this.itemCount = result.Data.length;

          }, null, null)
        break;
    }
  }


  saveEmployee(value) {
    let isValid = true;
    const exceEmpRolesClone = this.exceEmpRoles.filter(x => x.isSelected == true)
    const EmpRoleList = []
    exceEmpRolesClone.forEach(element => {
      element.IsActive = true
      delete element.isSelected;
      EmpRoleList.push(element.RoleId)
      let branchCount = 0
      element.branches.forEach(branch => {
        if (branch.IsCheck) {
          element.BranchList.push(branch.Id)
          branchCount++;
        }
      });
      if (branchCount < 1) {
        isValid = false;
      }
      delete element.branches;
    });
    if (isValid) {
      value.Gender = this.selectedGender
      value.BirthDay = this.dateConverter(value.BirthDay)
      value.StartDate = this.dateConverter(value.StartDate)
      value.EndDate = this.dateConverter(value.EndDate)
      if (this.homeGym) {
        value.HomeBranchId = this.homeGym.Id
        value.HomeGymName = this.homeGym.BranchName
      }
      value.RoleId = 'EMP'
      value.CreatedUser = this.currentUser.username;
      value.ExceEmpRoleList = exceEmpRolesClone
      value.BranchId = this.branchId;
      value.EmpRoleList =  EmpRoleList;
      value.image = this.data1.image;
      this.adminService.SaveGymEmployee(value).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result.Data) {
          this.savedSuccess.emit();
        }
      }, error => {
        this.canceled.emit();
      }, null)
    } else {
      this.selectBranchMessage('Confirm', 'There is a role without selecting branch,please select the branch')
    }
  }
  selectBranchMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('ERROR',
      {
        messageTitle: 'ERROR',
        messageBody: body,
        msgBoxId: 'SELECT_BRANCH'
      }
    );
  }

  onCancel() {
    this.canceled.emit();
  }

  submitUserForm(empInfoFormVal) {
    this.formValid = true
    if (this.isFirstNameRequired) {
      if (empInfoFormVal.FirstName === '' || empInfoFormVal.FirstName === null) {
        this.empInfoForm.controls['FirstName'].setErrors({
          'required': true
        });
        this.formValid = false;
      }
    }
    if (this.isLastNameRequired) {
      if (empInfoFormVal.LastName === '' || empInfoFormVal.LastName === null) {
        this.empInfoForm.controls['LastName'].setErrors({
          'required': true
        });
        this.formValid = false;
      }
    }
    if (this.isAddressRequired) {
      if (empInfoFormVal.Address1 === '' || empInfoFormVal.Address1 === null) {
        this.empInfoForm.controls['Address1'].setErrors({
          'required': true
        });
        this.formValid = false;
      }
    }
    if (this.isMobileRequired) {
      if (empInfoFormVal.Mobile === '' || empInfoFormVal.Mobile === null) {
        this.empInfoForm.controls['Mobile'].setErrors({
          'required': true
        });
        this.formValid = false;
      }
    }
    if (this.isEmailRequired) {
      if (empInfoFormVal.Email === '' || empInfoFormVal.Email === null) {
        this.empInfoForm.controls['Email'].setErrors({
          'required': true
        });
        this.formValid = false;
      }
    }
    if (this.isZipRequired) {
      if (empInfoFormVal.PostCode === '' || empInfoFormVal.PostCode === null) {
        this.empInfoForm.controls['PostCode'].setErrors({
          'required': true
        });
        this.formValid = false;
      }
    }
    this.formSubmited = true;

     if (this.formValid) {
       console.log('valid')
       this.saveEmployee(empInfoFormVal);
     } else {
       UsErrorService.validateAllFormFields(this.empInfoForm, this.formErrors, this.validationMessages)
    }
  }

  getRoles() {
    this.adminService.getBranches(-1).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        this.branches = result.Data;
        this.adminService.GetExcelineEmpRoles().pipe(takeUntil(this.destroy$)).subscribe(resultEmp => {
          if (result.Data) {
            this.exceEmpRoles = resultEmp.Data;
            this.exceEmpRoles.forEach(empRole => {
              empRole.isSelected = false;
              const clonedBranches = this.branches.map(x => Object.assign({}, x));
              empRole.branches = clonedBranches;
              empRole.RoleId = empRole.Id
            });
          }
        })
      }
    })
    this.memberService.getEmployeeRoles().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        this.employeeRoles = result.Data;
      }
    })
  }
  selectMember(member) {
    if (member.Gender === 1 || member.Gender === 'MALE') {
      this.selectedGender = 'MALE'
    } else {
      this.selectedGender = 'FEMALE'
    }
    this.empInfoForm.controls['Gender'].setValue(this.selectedGender);
    this.empInfoForm.patchValue(member)
    this.openMemberSelection.close();
  }

  changerRoles(event) {
    if (event.target.checked) {
      this.exceEmpRoles.filter(x => x.Id === this.selectedRole.Id)[0].BranchList.push(+event.target.value)
    } else {
      this.exceEmpRoles.filter(x => x.Id === this.selectedRole.Id)[0].BranchList = this.exceEmpRoles.filter(x => x.Id === this.selectedRole.Id)[0].BranchList.filter(x => x !== +event.target.value)
    }
  }

  dateConverter(dateObj: any) {
    let dateS = '';
    if (dateObj) {
      if (dateObj.formatted) {
        const splited = dateObj.formatted.split('/')
        if (splited.length > 1) {
          dateS = splited[2] + '-' + splited[1] + '-' + splited[0] + 'T00:00:00'
        }
      }
    }
    return dateS
  }

  changeHomeGym(event, branch) {
    if (event.target.value) {
      this.homeGym = branch
      this.empInfoForm.controls['HomeGymName'].setValue(this.homeGym.BranchName);
    }
  }

  getAge(value) {
    const BirthDay = this.dateConverter(value)
    const tempDate = new Date(BirthDay);
    const diff = Date.now() - Date.parse(BirthDay);
    const age = Math.floor((diff / (1000 * 3600 * 24)) / 365);
    this.ageValue = age;
  }

  mobileChange(event) {
    this.phoneNumberDetail = event;
    this.empInfoForm.patchValue({
      Mobile: event.value,
      MobilePrefix: event.extension
    });
    this.isMobileNumberValid = event.valid;
  }

  privateChange(event) {
    this.phoneNumberDetail = event;
    this.empInfoForm.patchValue({
      Private: event.value,
      PrivateTeleNoPrefix: event.extension
    });
    this.isPrivateNumberValid = event.valid;
  }
  workChange(event) {
    this.phoneNumberDetail = event;
    this.empInfoForm.patchValue({
      Work: event.value,
      WorkTeleNoPrefix: event.extension
    });
    this.isWorkNumberValid = event.valid;
  }

  mobileNumberValidator(control: AbstractControl) {
    if (!control.valueChanges) {
      return of(null);
    } else {
      return control.valueChanges.pipe(
        debounceTime(500),
        take(1),
        map(value => {
          return this.isMobileNumberValid === false ? {'phoneNumber': true } : null;
        }),
        first()
      );
    }
  }

  /**
   * 
   * @depricated as of Rxjs 6
   */
  // mobileNumberValidator(control: AbstractControl) {
  //   if (!control.valueChanges) {
  //     return of(null);
  //   } else {
  //     return control.valueChanges
  //     .debounceTime(500)
  //     .take(1)
  //       .map(value => {
  //         return this.isMobileNumberValid === false ? { 'phoneNumber': true } : null;
  //       }
  //       ).first();
  //   }
  // }

  workNumberValidator(control: AbstractControl) {
    if (!control.valueChanges) {
      return of(null);
    } else {
      return control.valueChanges.pipe(
        debounceTime(500),
        take(1),
        map(value => {
          return this.isWorkNumberValid === false ? { 'phoneNumberW': true } : null;
        }),
        first()
      );
    }
  }

  /**
   * 
   * @depricated as of Rxjs 6
   */
  // workNumberValidator(control: AbstractControl) {
  //   if (!control.valueChanges) {
  //     return Observable.of(null);
  //   } else {
  //     return control.valueChanges
  //     .debounceTime(500)
  //     .take(1)
  //       .map(value => {
  //         return this.isWorkNumberValid === false ? { 'phoneNumberW': true } : null;
  //       }
  //       ).first();
  //   }
  // }

  privateNumberValidator(control: AbstractControl) {
    if (!control.valueChanges) {
      return of(null);
    } else {
      return control.valueChanges.pipe(
        debounceTime(500),
        take(1),
        map(value => {
          return this.isPrivateNumberValid === false ? { 'phoneNumberP': true } : null;
        }),
        first()
      );
    }
  }

  /**
   * 
   * @depricated as of Rxjs 6
   */
  // privateNumberValidator(control: AbstractControl) {
  //   if (!control.valueChanges) {
  //     return Observable.of(null);
  //   } else {
  //     return control.valueChanges
  //     .debounceTime(500)
  //     .take(1)
  //       .map(value => {
  //         return this.isPrivateNumberValid === false ? { 'phoneNumberP': true } : null;
  //       }
  //       ).first();
  //   }
  // }

}
