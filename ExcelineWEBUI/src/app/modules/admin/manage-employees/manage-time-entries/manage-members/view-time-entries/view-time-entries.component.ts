import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { AdminService } from '../../../../services/admin.service';
import { ExceLoginService } from '../../../../../../modules/login/exce-login/exce-login.service';
import { DataTableResource } from '../../../../../../shared/components/us-data-table/tools/data-table-resource';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableFilterPipe } from '../../../../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { TreeNode } from 'app/shared/components/us-tree-table/tree-node';
import { asEnumerable } from 'linq-es2015';
import { IMyDpOptions } from 'app/shared/components/us-date-picker/interfaces';
import { TranslateService } from '@ngx-translate/core';
import { ExceBreadcrumbService } from '../../../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

const now = new Date();

@Component({
  selector: 'app-view-time-entries',
  templateUrl: './view-time-entries.component.html',
  styleUrls: ['./view-time-entries.component.scss']
})
export class ViewTimeEntriesComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId: number;
  private items = [];
  private itemCount = 0;
  private memberVisits?: any[] = [];
  private memberVisitsBase?: any[] = [];
  private itemResource: DataTableResource<any>;
  private today: NgbDateStruct;
  private monthbefore: NgbDateStruct;
  private startDate: string;
  private endDate: string;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private language: any;
  private dateformat: any;
  private memberVisitsList?: any[] = [];
  private changedFromDate: any;
  private changedToDate: any;
  groupType: any;
  files: TreeNode[] = [];
  memberVisitsForm: FormGroup;
  fromDatePickerOptions: any;
  // fromDatePickerOptions: IMyDpOptions = {
  //   disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 },
  // };

  // toDatePickerOptions: IMyDpOptions = {
  //   disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 }
  // };
  toDatePickerOptions: any;
  filesBase: TreeNode[];
  @ViewChild('GroupByName') GroupByName: ElementRef;
  //@ViewChild('AccessPointFilter') AccessPointFilter: ElementRef;
  accessPoints: any;

  constructor(
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private fb: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private exceToolbarService: ExceToolbarService,
    private translate: TranslateService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.MemberVisitC',
      url: '/adminstrator/view-time-entries'
    });

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;

    this.monthbefore = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

    this.memberVisitsForm = this.fb.group({
      'fromDate': [{ date: this.monthbefore }],
      'toDate': [{ date: this.today }]
    });

    this.memberVisitsForm.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.monthbefore = { year: data.fromDate.date.year, month: data.fromDate.date.month, day: data.fromDate.date.day };
      this.today = { year: data.toDate.date.year, month: data.toDate.date.month + 1, day: data.toDate.date.day };
    });
  }

  ngOnInit() {
    const disSi = new Date();
    disSi.setDate(disSi.getDate() + 1)
    this.fromDatePickerOptions = {
      disableSince: {year: disSi.getFullYear(), month: disSi.getMonth() + 1, day: disSi.getDate()}
    }
    this.toDatePickerOptions = {
      disableSince: {year: disSi.getFullYear(), month: disSi.getMonth() + 1, day: disSi.getDate()}
    }
    // this.adminService.GetAccessPoints().pipe(takeUntil(this.destroy$)).subscribe(
    //   result => {
    //     this.accessPoints = result.Data
    //   }
    // )
    this.startDate = (this.today.month + '/' + this.today.day + '/' + this.today.year);
    this.endDate = (this.today.month + '/' + this.today.day + '/' + this.today.year);
    this.searchVisits();
  }

  // get from date change
  fromDateChange(event, toDate) {
    this.toDatePickerOptions = {
      disableUntil: event.date
    };
    toDate.showSelector = true;
  }

  // get to date change
  toDateChange(event) {
    this.searchVisits();
  }

  rowDblClick(event) {
    const node = event.node
    if (node.type === '1') {
      const url = '/membership/card/' + node.data.EntityId + '/' + node.data.BranchId + '/MEM';
      this.router.navigate([url]);
    }
  }

  // search the member visits data
  memberVisitsSearch(value) {
    this.GroupByName.nativeElement.value = 'all';
    //this.AccessPointFilter.nativeElement.value = 'all';
    this.changedFromDate = value.fromDate.date;
    this.changedToDate = value.toDate.date;
    this.startDate = (this.changedFromDate.month + '/' + this.changedFromDate.day + '/' + this.changedFromDate.year);
    this.endDate = (this.changedToDate.month + '/' + this.changedToDate.day + '/' + this.changedToDate.year);
    this.searchVisits();
  }

  // filter data according to the EntityName and VisitDate
  searchVisits() {
    this.adminService.GetMemberVisits({
       startDate: this.startDate,
       endDate: this.endDate
      }).pipe(
        takeUntil(this.destroy$)
        ).subscribe(
      result => {
        if (result) {
          this.files = [];
          this.items = result.Data;
          this.memberVisits = this.items;
          this.memberVisits.forEach(v => {
            this.files.push({
                'data': {
                  'EntityName': v.EntityName,
                  'VisitDate': v.VisitDate,
                  'InTime': v.InTime,
                  'OutTime': v.OutTime,
                  'EntityId': v.EntityId,
                  //'Role': v.Role,
                  'BranchId': v.BranchId,
                  'CutomerNo': v.CutomerNo,
                  'AccessControlType': v.AccessControlType
                },
                'type' : '1'
              });
          });
          this.filesBase = this.files;
          this.memberVisitsBase = this.memberVisits;
        }
      }, null, null);
  }

  // filter by name (search field)
  filterInputByName(param?: any) {
    const dataList = this.filterpipe.transform('EntityName', 'NAMEFILTER', this.memberVisitsBase, param);
    this.files = [];
    dataList.forEach(data => {
      this.files.push({
          'data': {
            'EntityName': data.EntityName,
            'VisitDate': data.VisitDate,
            'InTime': data.InTime,
            'OutTime': data.OutTime,
            'EntityId': data.EntityId,
            //'Role': data.Role,
            'BranchId': data.BranchId,
            'CutomerNo': data.CutomerNo,
            'AccessControlType': data.AccessControlType
          },
          'type' : '1'
        });
    });
  }

  filterAccess(value) {
    if (value !== 'all') {
      const dataList = this.filterpipe.transform('AccessControlType', 'TEXT', this.memberVisitsBase, value);
    this.files = [];
    dataList.forEach(data => {
      this.files.push({
          'data': {
            'EntityName': data.EntityName,
            'VisitDate': data.VisitDate,
            'InTime': data.InTime,
            'OutTime': data.OutTime,
            'EntityId': data.EntityId,
            // 'Role': data.Role,
            'BranchId': data.BranchId,
            'CutomerNo': data.CutomerNo,
            'AccessControlType': data.AccessControlType
          },
          'type' : '1'
        });
      });
    } else {
      this.files = this.filesBase;
    }
  }


  // filter by date (search field)
  filterInputByDate(param?: any) {
    this.language = this.exceToolbarService.getSelectedLanguage();
    this.translate.get('COMMON.DateFormat').pipe(takeUntil(this.destroy$)).subscribe((res: string) => {
      this.dateformat = res;
    });
    const dataList = this.filterpipe.transform('VisitDate', 'DATE', this.items,
      param, this.language.id, this.dateformat);
    this.files = [];
    dataList.forEach(data => {
      this.files.push({
        'data': {
          'EntityName': data.EntityName,
          'VisitDate': data.VisitDate,
          'InTime': data.InTime,
          'OutTime': data.OutTime,
          'EntityId': data.EntityId,
          //'Role': data.Role,
          'BranchId': data.BranchId,
          'CutomerNo': data.CutomerNo,
          'AccessControlType': data.AccessControlType
        },
        'type': '1'
      });
    });
  }

  GroupBy(value) {
    this.groupType = value;
    this.groupDataset();
  }

  // group data by name and visit date
  groupDataset() {
    this.files = [];
    if (this.groupType === 'name') {
      let sortedArr = (this.memberVisits.reduce(function (res, current) {
        res[current.EntityName] = res[current.EntityName] || [];
        res[current.EntityName].push(current);
        return res;
      }, {}));
      // tslint:disable-next-line:prefer-const
      let files = []
      this.files = []
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line:forin
      for (let key in sortedArr) {
        let children = []
        const constArr = sortedArr[key]
        constArr.forEach(element => {
          children.push({
            'data': element,
            'type': '1'
          })
        });
        this.files.push({
          'data': { 'EntityName': key },
          'children': children,
          'type': '0'
        });
      }
    } else if (this.groupType === 'visitDate') {
      const sortedArr = (this.memberVisits.reduce(function (res, current) {
        res[current.VisitDate] = res[current.VisitDate] || [];
        res[current.VisitDate].push(current);
        return res;
      }, {}));
      // tslint:disable-next-line:prefer-const
      let files = []
      this.files = []
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line:forin
      for (const key in sortedArr) {
        const children = []
        const constArr = sortedArr[key]
        constArr.forEach(element => {
          children.push({
            'data': element,
            'type': '1'
          })
        });
        const sDate = new Date(key);
        let time = sDate.toLocaleTimeString();
        const vDate = {
          date: {
            year: sDate.getFullYear(),
            month: sDate.getMonth() + 1,
            day: sDate.getDate()
          }
        }
        this.files.push({
          'data': { 'EntityName': this.getDateConverter(vDate) + ' ' + time },
          'children': children,
          'type': '0'
        });
      }

    } else {
      this.files = this.filesBase;
    }
  }

  // convert date js object in to string
  getDateConverter(datee: any) {
    const dd = (datee.date.day < 10 ? '0' : '') + datee.date.day;
    const MM = ((datee.date.month + 1) < 10 ? '0' : '') + datee.date.month;
    const yyyy = datee.date.year;
    return (dd + '/' + MM + '/' + yyyy);
  }

  // unsubscribe the subscribed
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}





