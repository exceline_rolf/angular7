


import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { AdminService } from '../../services/admin.service'
// import { EmployeeBasicInfoService } from './employee-basic-info/employee-basic-info.service';
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { error } from 'util';
import { PreloaderService } from '../../../../shared/services/preloader.service'
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { EntitySelectionType, ColumnDataType } from '../../../../shared/enums/us-enum';
import { Console } from '@angular/core/src/console';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil, window } from 'rxjs/operators';
import { Subject } from 'rxjs';
const now = new Date();

@Component({
  selector: 'app-gym-employee-times',
  templateUrl: './gym-employee-times.component.html',
  styleUrls: ['./gym-employee-times.component.scss']
})

export class GymEmployeeTimesComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId = 1;
  private itemResource: any = 0;
  private timeEntries = []
  private branches = []
  private Categories: any
  private filteredTime = []
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private lastFromDate: any;
  private lastToDate: any;
  private selectedDate: any;
  private today;
  private formSubmited = false;
  private entitySearchType = 'EMP';
  private selectedList = []
  public items = [];
  public itemCount = 0;
  gymEmpView: any;
  NewTimeForm: any;
  selectedCategory: string;
  selectedBranch: number
  public currentEmployee: any;
  empCount
  employees
  isAllApproved = false
  TodayString = ''
  monthBeforeString = ''
  monthBefore


  columns = [
    { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'CustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  ];

  auotocompleteItems = [{ id: 'Name', name: 'Name :', isNumber: false },
  { id: 'Id', name: 'Id :', isNumber: true }]
  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };

  constructor(
    //  private employeeBasicInfoService: EmployeeBasicInfoService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.HeldTimeListsC',
      url: '/adminstrator/gym-employee-times'
    });

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;

      });

    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    const monthBeforeDate = now
    monthBeforeDate.setMonth(now.getMonth() - 1);
    this.monthBefore = { year: monthBeforeDate.getFullYear(), month: monthBeforeDate.getMonth() + 1, day: now.getDate() };

    this.TodayString = this.getDateConverterForDatePicker(this.today)
    this.monthBeforeString = this.getDateConverterForDatePicker(this.monthBefore)
    this.lastFromDate = { date: this.monthBefore }
    this.lastToDate = { date: this.today }
  }


  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.gymEmpView) {
      this.gymEmpView.close();
    }
  }

  openSearchEmployee(content: any) {
    this.gymEmpView = this.modalService.open(content, { width: '600' });
  }

  getDateConverter(datee: any) {
    const dd = (datee.day < 10 ? '0' : '') + datee.day;
    const MM = ((datee.month + 1) < 10 ? '0' : '') + datee.month;
    const yyyy = datee.year;
    return (yyyy + '-' + MM + '-' + dd + 'T00:00:00');
  }
  getDateConverterForDatePicker(datee: any) {
    const dd = (datee.day < 10 ? '0' : '') + datee.day;
    const MM = ((datee.month + 1) < 10 ? '0' : '') + datee.month;
    const yyyy = datee.year;
    return (dd + '/' + MM + '/' + yyyy);
  }
  searchDetail(val: any) {
    switch (val.entitySelectionType) {
      case EntitySelectionType[EntitySelectionType.EMP]:
        this.adminService.GetGymEmployees(this.branchId, val.searchVal, true, -1).pipe(takeUntil(this.destroy$)).subscribe
          (result => {
            this.employees = result.Data;
            this.empCount = result.Data.length;

          }, null, null)
        break;
    }
  }

  selectResource(event) {
    this.currentEmployee = event
    this.getTimeEntries()
    this.gymEmpView.close()
  }

  getTimeEntries() {
    PreloaderService.showPreLoader();
    this.adminService.GetEmployeeTimeEntries(this.currentEmployee.Id).pipe(takeUntil(this.destroy$)).subscribe(event => {
      if (event.Data) {
        this.timeEntries = event.Data
        // const today = now.getFullYear() + '-' + now.getMonth() + 1 + '-' + now.getDate()
        this.itemResource = new DataTableResource(this.timeEntries);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.timeEntries;
        this.itemCount = Number(this.timeEntries.length);
        this.filterInput()
        PreloaderService.hidePreLoader();
      } else {
        PreloaderService.hidePreLoader();
      }
    }, Error => {
      PreloaderService.hidePreLoader();
    }
    );
  }

  selectRow(event) {
    if (event.selected) {
      if (!(event.item in this.selectedList)) {
        this.selectedList.push(event.item)
      }
    } else {
      this.selectedList = this.selectedList.filter(item => item != event.item)
    }
  }

  approveWork() {
    let timeIdList = ''
    this.selectedList.forEach(timeEntry => {
      timeIdList = timeIdList + timeEntry.Id + ','
    })
    timeIdList = timeIdList.substring(0, timeIdList.length - 1);
    this.adminService.ApproveEmployeeTimes({ employeeId: this.currentEmployee.Id, isAllApproved: this.isAllApproved, timeIdString: timeIdList }).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        this.getTimeEntries()
      }
    });
  }

  filterInput(FromDateVal?: any, ToDateVal?: any) {
    if (FromDateVal) {
      this.lastFromDate = JSON.parse(JSON.stringify(FromDateVal));
      FromDateVal = FromDateVal.date.year + '-' + FromDateVal.date.month + '-' + FromDateVal.date.day
      if (this.lastToDate && this.lastToDate.formatted) {
        ToDateVal = this.lastToDate.date.year + '-' + this.lastToDate.date.month + '-' + this.lastToDate.date.day
      } else {
        ToDateVal = ""
      }
    }
    if (ToDateVal) {
      this.lastToDate = JSON.parse(JSON.stringify(ToDateVal));
      ToDateVal = ToDateVal.date.year + '-' + ToDateVal.date.month + '-' + ToDateVal.date.day
      if (this.lastFromDate && this.lastFromDate.formatted) {
        FromDateVal = this.lastFromDate.date.year + '-' + this.lastFromDate.date.month + '-' + this.lastFromDate.date.day
      } else {
        FromDateVal = ""
      }
    }
    if (!ToDateVal && !FromDateVal) {
      if (this.lastFromDate && this.lastFromDate.date) {
        // FromDateVal = this.lastFromDate.date.year + '-' + this.lastFromDate.date.month + '-' + this.lastFromDate.date.day
        FromDateVal = this.getDateConverter(this.lastFromDate.date)
      }
      if (this.lastToDate && this.lastToDate.date) {
        // ToDateVal = this.lastToDate.date.year + '-' + this.lastToDate.date.month + '-' + this.lastToDate.date.day
        ToDateVal = this.getDateConverter(this.lastToDate.date)
      }
    }
    this.filteredTime = this.timeEntries
    if (FromDateVal && FromDateVal !== '' && FromDateVal != '0-0-0') {
      this.filteredTime = this.filterpipe.transform('WorkDate', 'FROM_DATE', this.filteredTime, FromDateVal);
    }
    if (ToDateVal && ToDateVal !== '' && ToDateVal != '0-0-0') {
      this.filteredTime = this.filterpipe.transform('WorkDate', 'TO_DATE', this.filteredTime, ToDateVal);
    }

    this.itemResource = new DataTableResource(this.filteredTime);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredTime);
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }













  closeModal() {

    this.gymEmpView.close()
  }

}

