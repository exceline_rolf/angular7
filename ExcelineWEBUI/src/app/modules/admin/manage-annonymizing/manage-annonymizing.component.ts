import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminService } from '../services/admin.service'
import { ExceLoginService } from '../../../modules/login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../shared/components/exce-message/exce-message.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-manage-annonymizing',
  templateUrl: './manage-annonymizing.component.html',
  styleUrls: ['./manage-annonymizing.component.scss']
})

export class ManageAnnonymizingComponent implements OnInit, OnDestroy {
  annonymizingTypes: any;
  private branchId;
  private model: any;
  selectedItem: any
  private destroy$ = new Subject<void>();

  constructor(private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService, ) {

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;

    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }

  ngOnInit() {
    this.adminService.GetAnonymizingData().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.annonymizingTypes = result.Data;
        this.selectedItem = this.annonymizingTypes[0];
      }
    });
  }

  annonimize() {
    this.adminService.DeleteAnonymizingData({ 'Anonymizing': this.selectedItem, 'BranchId': this.branchId }).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
        }
      },
      error => {
        this.deleteErrorMessage('Error', 'Error Saving data');
      });
  }

  deleteErrorMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('WARNING',
      {
        messageTitle: title,
        messageBody: body,
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
