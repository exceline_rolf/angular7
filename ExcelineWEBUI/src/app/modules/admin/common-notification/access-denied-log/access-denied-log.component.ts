import { Component, OnInit, OnDestroy } from '@angular/core';
import { IMyDpOptions } from 'app/shared/components/us-date-picker/interfaces';
import { NotificationService } from 'app/modules/admin/services/notification.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { DataTableResource } from 'app/shared/components/us-data-table/tools/data-table-resource';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

const now = new Date();

@Component({
  selector: 'app-access-denied-log',
  templateUrl: './access-denied-log.component.html',
  styleUrls: ['./access-denied-log.component.scss']
})
export class AccessDeniedLogComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private today: NgbDateStruct;
  private weekbefore: NgbDateStruct;
  accessDeniedLogForm: FormGroup;
  private startDate: string;
  private endDate: string;
  private changedFromDate: any;
  private changedToDate: any;
  itemCount = 0;
  private itemResource?: DataTableResource<any>;
  private accessDeniedLogList = [];
  filterdAccessDeniedLogList = [];

  fromDatePickerOptions: IMyDpOptions = {
    disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 },
  };

  toDatePickerOptions: IMyDpOptions = {
    disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 }
  };

  constructor(
    private notificationService: NotificationService,
    private fb: FormBuilder,
  ) {

    const weekBeforeDate = new Date();
    weekBeforeDate.setDate(now.getDate() - 7);

    this.weekbefore = { year: weekBeforeDate.getFullYear(), month: weekBeforeDate.getMonth() + 1, day: weekBeforeDate.getDate() };
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

    this.accessDeniedLogForm = this.fb.group({
      'fromDate': [{ date: this.weekbefore }],
      'toDate': [{ date: this.today }]
    });
  }

  ngOnInit() {
    this.startDate = (this.weekbefore.month + '/' + this.weekbefore.day + '/' + this.weekbefore.year);
    this.endDate = (this.today.month + '/' + this.today.day + '/' + this.today.year);

    this.searchAccessDeniedLog();
  }

  // get from date change
  fromDateChange(event, toDate) {
    this.toDatePickerOptions = {
      disableUntil: event.date
    };
    toDate.showSelector = true;
  }

  // get to date change
  toDateChange(event) {
  }

  getDateChanges(value) {
    this.changedFromDate = value.fromDate.date;
    this.changedToDate = value.toDate.date;
    this.startDate = (this.changedFromDate.month + '/' + this.changedFromDate.day + '/' + this.changedFromDate.year);
    this.endDate = (this.changedToDate.month + '/' + this.changedToDate.day + '/' + this.changedToDate.year);
    this.searchAccessDeniedLog();
  }

  // fill the table
  searchAccessDeniedLog() {
    this.notificationService.GetAccessDeniedList({
      fromDate: this.startDate,
      toDate: this.endDate
    }).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result) {
          this.accessDeniedLogList = result.Data;
          this.filterdAccessDeniedLogList = this.accessDeniedLogList;
          this.itemCount = this.accessDeniedLogList.length;
          this.itemResource = new DataTableResource(this.filterdAccessDeniedLogList);
          this.itemResource.count().then(count => this.itemCount = count);
        }
      }, null, null);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
