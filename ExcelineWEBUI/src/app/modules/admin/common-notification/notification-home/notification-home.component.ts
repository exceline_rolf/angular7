import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../../membership/services/exce-member.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NotificationService } from '../../services/notification.service';
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil, mergeMap } from 'rxjs/operators';
import { Subject } from 'rxjs';
const now = new Date();

@Component({
  selector: 'app-notification-home',
  templateUrl: './notification-home.component.html',
  styleUrls: ['./notification-home.component.scss']
})
export class NotificationHomeComponent implements OnInit, OnDestroy {
  private branchId: number;
  private filteredLoadedObj: any;
  private itemResource: any = 0;
  items = [];
  itemCount = 0;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private modalReference: any;
  advSearchForm: any;
  private cat = [];
  private rowItem: any;
  private username;
  newNotifications = [];
  attendedNotifications = [];
  ignoredNotifications = [];
  doneNotifications = [];
  private loadedObject = [];
  private isButtonsVisible = true
  private isNotificationDetail: any;
  private currentCat = 1;
  private selectedItems = []
  isDoneIgnoreVisible = false;
  isAdvSearch = false;
  private destroy$ = new Subject<void>();

  constructor(private exceMemberService: ExceMemberService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.SystemLogC',
      url: '/adminstrator/notification-home'
    });

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.username = this.exceLoginService.CurrentUser.username;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
    }, null, null);
  }

  ngOnInit() {
    this.getAllNotefications();
    this.advSearchForm = this.fb.group({
      'Title': [null],
      'AssignTo': [null,],
      'FromRecieveDate': [null],
      'ToRecieveDate': [null],
      'IsMessage': [null],
      'IsToDo': [null],
      'IsWarning': [null],
      'IsError': [null],
      'IsMinor': [null],
      'IsModerate': [null],
      'IsCritical': [null],
      'FromDueDate': [null],
      'ToDueDate': [null]
    });
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  private getAllNotefications() {
    /* Get all notefications asynchronously as concurrent subscriptions are not dependent on its predecessor */
    this.notificationService.GetNotificationSummaryList(this.branchId, 1).pipe(
      mergeMap(newNotefication => {
        this.newNotifications = newNotefication.Data;
        return this.notificationService.GetNotificationSummaryList(this.branchId, 2).pipe(
          takeUntil(this.destroy$)
        );
      }),
      mergeMap(attendedNotifications => {
        this.attendedNotifications = attendedNotifications.Data;
        return this.notificationService.GetNotificationSummaryList(this.branchId, 3).pipe(
          takeUntil(this.destroy$)
        );
      }),
      mergeMap(ignoredNoteficatins => {
        this.ignoredNotifications = ignoredNoteficatins;
        return this.notificationService.GetNotificationSummaryList(this.branchId, 4).pipe(
          takeUntil(this.destroy$)
        );
      }),
      takeUntil(this.destroy$)
      ).subscribe(finishedNotefication => {
        /* Clean up */
        if (finishedNotefication) {
          this.doneNotifications = finishedNotefication.Data;
          if (this.currentCat) {
            this.loadCategory(this.currentCat);
          } else {
            this.loadCategory(1);
          }
        }
      }, null, null);
  }

  /**
   * @deprecated use getAllNotifications
   */
  getNewNotifications() {
    this.notificationService.GetNotificationSummaryList(this.branchId, 1).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.newNotifications = result.Data;
        this.getAttendedNotifications();
      }
    });
  }

  /**
   * @deprecated use getAllNotifications
   */
  getAttendedNotifications() {
    this.notificationService.GetNotificationSummaryList(this.branchId, 2).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.attendedNotifications = result.Data;
        this.getIgnoredNotifications();
      }
    });
  }

  /**
   * @deprecated use getAllNotifications
   */
  getIgnoredNotifications() {
    this.notificationService.GetNotificationSummaryList(this.branchId, 3).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.ignoredNotifications = result.Data;
        this.getDoneNotifications();
      }
    });
  }

  /**
   * @deprecated use getAllNotifications
   */
  getDoneNotifications() {
    this.notificationService.GetNotificationSummaryList(this.branchId, 4).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.doneNotifications = result.Data;
        if (this.currentCat) {
          this.loadCategory(this.currentCat)
        } else {
          this.loadCategory(1);
        }
      }
    });
  }

  loadCategory(cat) {
    if (cat === 1) {
      this.currentCat = 1;
      this.loadedObject = this.newNotifications;
      this.loadTable();
    } else if (cat === 2) {
      this.currentCat = 2;
      this.loadedObject = this.attendedNotifications;
      this.loadTable();
    } else if (cat === 3) {
      this.currentCat = 3;
      this.loadedObject = this.ignoredNotifications;
      this.loadTable();
    } else {
      this.currentCat = 4;
      this.loadedObject = this.doneNotifications;
      this.loadTable();
    }
  }

  loadTable() {
    this.itemResource = new DataTableResource(this.loadedObject);
    this.itemResource.count().then(count => this.itemCount = count);
    this.items = this.loadedObject;
    this.itemCount = Number(this.loadedObject.length);
  }

  viewAdvSearch() {
    this.isAdvSearch = true;
  }

  hideAdvSearch() {
    this.isAdvSearch = false;
  }

  selectRow(event) {
    if (event._selected) {
      this.selectedItems.push(event.item.Id);
      this.isDoneIgnoreVisible = true;
    } else {
      this.selectedItems = this.selectedItems.filter(item => item !== event.item.Id);
      if (this.selectedItems.length === 0) {
        this.isDoneIgnoreVisible = false;
      }
    }
  }

  advSearch(value) {
    const severityIds = [];
    const typeIds = [];
    value.FromRecieveDate = this.dateConverter(value.FromRecieveDate);
    value.ToRecieveDate = this.dateConverter(value.ToRecieveDate);
    value.FromDueDate = this.dateConverter(value.FromDueDate);
    value.ToDueDate = this.dateConverter(value.ToDueDate);

    if (value.IsCritical) {
      severityIds.push(1);
    }
    if (value.IsModerate) {
      severityIds.push(2);
    }
    if (value.IsMinor) {
      severityIds.push(3);
    }
    if (value.IsError) {
      typeIds.push(1);
    }
    if (value.IsWarning) {
      typeIds.push(2);
    }
    if (value.IsToDo) {
      typeIds.push(3);
    }
    if (value.IsMessage) {
      typeIds.push(4);
    }

    const searchobj = {
      'branchId': this.branchId,
      'title': value.Title ? value.Title : '',
      'assignTo': value.AssignTo ? value.AssignTo : '',
      'receiveDateFrom': value.FromRecieveDate ? value.FromRecieveDate : '',
      'receiveDateTo': value.ToRecieveDate ? value.ToRecieveDate : '',
      'dueDateFrom': value.FromDueDate ? value.FromDueDate : '',
      'dueDateTo': value.ToDueDate ? value.ToDueDate : '',
      'severityIdList': severityIds,
      'typeIdList': typeIds,
      'statusId': this.currentCat
    }

    if (!searchobj.title) {
      searchobj.title = '';
    }

    this.notificationService.GetNotificationSearchResult(searchobj).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result.Data) {
        this.loadedObject = result.Data;
        this.loadTable();
      }
    }, null, null);
  }

  clearForm() {
    this.advSearchForm.reset();
  }

  dateConverter(date: any) {
    let dateStr = '';

    if (date ? date.date : false) {
      dateStr = date.date.year + '-' + date.date.month + '-' + date.date.day;
    }
    return dateStr;
  }

  filterInput(nameValue: any) {
    this.filteredLoadedObj = this.loadedObject;

    if (nameValue && nameValue !== '') {
      this.filteredLoadedObj = this.filterpipe.transform('Title', 'NAMEFILTER', this.filteredLoadedObj, nameValue);
    }

    this.itemResource = new DataTableResource(this.filteredLoadedObj);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredLoadedObj);
  }


  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }


  rowItemLoad(event, form) {
    this.rowItem = event.row.item;
    if (this.rowItem.StatusId === 3 || this.rowItem.StatusId === 4) {
      this.isButtonsVisible = false;
    }

    this.openEventModel(form, true);
  }

  openEventModel(content: any, isNotificationDetail: boolean) {
    this.modalReference = this.modalService.open(content);
    this.isNotificationDetail = isNotificationDetail;
  }

  onDoneOrIgnore() {
    this.getAllNotefications();
  }

  updateNotificationStatus(button) {
    switch (button) {
      case 'DONE':
        this.exceMemberService.updateNotificationStatus({ selectedIds: this.selectedItems, status: 4 }).pipe(
          takeUntil(this.destroy$)
        ).subscribe(
          result => {
            if (result.Data) {
              this.isDoneIgnoreVisible = false;
              this.getAllNotefications();
            }
          }, error => {

          }, () => {

          });
        break;
      case 'IGNORE':
        this.exceMemberService.updateNotificationStatus({ selectedIds: this.selectedItems, status: 3 }).pipe(
          takeUntil(this.destroy$)
        ).subscribe(
          result => {
            if (result.Data) {
              this.isDoneIgnoreVisible = false
              this.getAllNotefications()
            }
          }, error => {

          }, () => {

          });
        break;
      default:
        break;
    }
  }
}
