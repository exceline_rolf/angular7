import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotificationService } from 'app/modules/admin/services/notification.service';
import { DataTableFilterPipe } from 'app/shared/components/us-data-table/tools/data-table-filter-pipe';
import { DataTableResource } from 'app/shared/components/us-data-table/tools/data-table-resource';
import { FormGroup, FormBuilder } from '@angular/forms';
import { IMyDpOptions } from 'app/shared/components/us-date-picker/interfaces';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

const now = new Date();

@Component({
  selector: 'app-change-log',
  templateUrl: './change-log.component.html',
  styleUrls: ['./change-log.component.scss']
})
export class ChangeLogComponent implements OnInit, OnDestroy {

  private fromDate;
  private toDate;
  private notifyMethod;
  private notificationList = [];
  filterdNotificationList = [];
  itemCount = 0;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private itemResource?: DataTableResource<any>;
  logNotificationForm: FormGroup;
  private today: NgbDateStruct;
  private weekbefore: NgbDateStruct;
  private changedFromDate: any;
  private changedToDate: any;
  private startDate: string;
  private endDate: string;
  private modalReference?: any;
  private rowItem: any;
  private isButtonsVisible = true;
  private title: any;
  private destroy$ = new Subject<void>();

  fromDatePickerOptions: IMyDpOptions = {
    disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 },
  };

  toDatePickerOptions: IMyDpOptions = {
    disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 }
  };

  constructor(
    private notificationService: NotificationService,
    private fb: FormBuilder,
    private modalService: UsbModal,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.ChangeLogC',
      url: '/adminstrator/change-log'
    });

    const weekBeforeDate = new Date();
    weekBeforeDate.setDate(now.getDate() - 7);

    this.weekbefore = { year: weekBeforeDate.getFullYear(), month: weekBeforeDate.getMonth() + 1, day: weekBeforeDate.getDate() };
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

    this.logNotificationForm = this.fb.group({
      'fromDate': [{ date: this.weekbefore }],
      'toDate': [{ date: this.today }]
    });
  }

  ngOnInit() {
    this.notifyMethod = 'EVENTLOG';
    this.startDate = (this.weekbefore.month + '/' + this.weekbefore.day + '/' + this.weekbefore.year);
    this.endDate = (this.today.month + '/' + this.today.day + '/' + this.today.year);

    this.searchLogNotification();
  }

  // convert date js object in to string
  getDateConverter(datee: any) {
    const dd = (datee.day < 10 ? '0' : '') + datee.day;
    const MM = ((datee.month + 1) < 10 ? '0' : '') + datee.month;
    const yyyy = datee.year;
    return (yyyy + '-' + MM + '-' + dd + 'T00:00:00');
  }

  // get from date change
  fromDateChange(event, toDate) {
    this.toDatePickerOptions = {
      disableUntil: event.date
    };
    toDate.showSelector = true;
  }

  // get to date change
  toDateChange(event) {
  }

  // get the date changes
  getDateChanges(value) {
    this.notifyMethod = 'EVENTLOG';
    this.changedFromDate = value.fromDate.date;
    this.changedToDate = value.toDate.date;

    this.startDate = (this.changedFromDate.month + '/' + this.changedFromDate.day + '/' + this.changedFromDate.year);
    this.endDate = (this.changedToDate.month + '/' + this.changedToDate.day + '/' + this.changedToDate.year);

    this.searchLogNotification();
  }

  // fill the table
  searchLogNotification() {
    this.notificationService.GetChangeLogNotificationList({
      fromDate: this.startDate,
      toDate: this.endDate,
      notifyMethod: this.notifyMethod
    }).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.notificationList = result.Data;
          this.filterdNotificationList = this.notificationList;
          this.itemCount = this.notificationList.length;
          this.itemResource = new DataTableResource(this.filterdNotificationList);
          this.itemResource.count().then(count => this.itemCount = count);
        }
      }, null, null);
  }

  // filter by Category (search field)
  filterByCategory(param?: any) {
    this.filterdNotificationList = this.filterpipe.transform('Category', 'NAMEFILTER', this.notificationList, param);
  }

  // filter by Employee (search field)
  filterByEmployee(param?: any) {
    this.filterdNotificationList = this.filterpipe.transform('AssignTo', 'NAMEFILTER', this.notificationList, param);
  }

  openEventModel(content: any) {
    this.modalReference = this.modalService.open(content);
  }

  rowItemLoad(rowItem, form) {
    const selectedItem = rowItem;
    this.notificationService.GetNotificationByIdAction(selectedItem.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
        result => {
          if (result) {
            this.title = result.Data.Title;
            this.rowItem = rowItem
            this.rowItem.Title = this.title;

            if (rowItem.StatusId === 3 || rowItem.StatusId === 4) {
              this.isButtonsVisible = false
            }
            this.openEventModel(form);
          }
        }, null, null);
  }

  onDoneOrIgnore() {
    this.notificationList = this.notificationList.filter(item => item !== this.rowItem);
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }

    this.destroy$.next();
    this.destroy$.complete();
  }

}
