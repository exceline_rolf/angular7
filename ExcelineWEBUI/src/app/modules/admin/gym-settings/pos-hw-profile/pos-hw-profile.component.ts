
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ExceService } from '../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { GymSettingsService } from '../gym-settings.service'
import { AdminService } from '../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil, take } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'pos-hw-profile',
  templateUrl: './pos-hw-profile.component.html',
  styleUrls: ['./pos-hw-profile.component.scss']
})
export class PosHwProfileComponent implements OnInit, OnDestroy {
  private branch: any;
  public hwSettingsForm: any;
  public hwSettingsFormUpdate: any;
  public updateForm: any;
  public hwItems = [];
  private editCatArray = []
  public hwCount = 0;
  private hwResource: any;
  public hwSettingsItems = [];
  hwSettingsCount = 0;
  private hwSettingsesource: any;
  public checkBoxValidator = false;
  isAddNew = true
  private rowItem: any
  hwTableForm: any
  private addedItems = []
  private model: any;
  private deletingItem: any;
  private modalReference: any;
  hwSelected: any;
  hwItemsUpdate: any;
  Name: any;
  ChangeMoney: any;
  private destroy$ = new Subject<void>();

  message: string;
  type: string;
  isShowInfo: boolean;

  constructor(private adminService: AdminService, private modalService: UsbModal,
    private exceLoginService: ExceLoginService,
    private translateService: TranslateService,
    private gymListService: GymSettingsService,
    private fb: FormBuilder,
    private exceService: ExceService,
    private exceMessageService: ExceMessageService) {
    if (this.gymListService.SelectedGym) {
      this.fetchData(this.gymListService.SelectedGym);
    }
    this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.fetchData(value.some)
    }, null, null);

    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.deleteHandler();
    }, null, null);

  }

  ngOnInit() {
    this.hwSettingsForm = this.fb.group({
      'Name': [null, [Validators.required]],
      'ChangeMoney': [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
      'HardwareProfileItemList': [null],
    });
    this.hwSettingsFormUpdate = this.fb.group({
      'Name': [null, [Validators.required]],
      'ChangeMoney': [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
      'HardwareProfileItemList': [null],
    });
    this.hwTableForm = this.fb.group({
      'HardwareProfileItemList': [null],
    });
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }

    this.destroy$.next();
    this.destroy$.complete();
  }

  fetchData(branch) {
    this.branch = branch;

    this.adminService.getCategoriesByType('HARDWARE').pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.hwItems = result.Data;
          this.hwItemsUpdate = result.Data;
          // this.hwItemsUpdate = result.Data;
          this.hwResource = new DataTableResource(this.hwItems);
          this.hwResource.count().then(count => this.hwCount = count);
          this.hwCount = Number(this.hwItems.length);

        } else {

        }
      }, null, null)

    this.adminService.getGymSettings('HARDWARE', null, this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.hwSettingsItems = result.Data;
          this.hwSettingsesource = new DataTableResource(this.hwSettingsItems);
          this.hwSettingsesource.count().then(count => this.hwCount = count);
          this.hwSettingsCount = Number(this.hwSettingsItems);
        }
      }, null, null)
  }

  onCheckChange(event, rowItem) {
    if (event.target.checked) {
      this.checkBoxValidator = true;
      rowItem.CategoryName = rowItem.Name;
      rowItem.CategoryId = rowItem.Id
      this.editCatArray.push(rowItem)
    } else {
      this.editCatArray = this.editCatArray.filter(item => item.Code !== rowItem.Code);
      if (this.editCatArray.length === 0) {
        this.checkBoxValidator = false
      }
    }
  }


  addSetting(value) {
    const hwSettingObj = {
      'Id': '',
      'BranchId': this.gymListService.SelectedGym.Id,
      'BranchName': this.gymListService.SelectedGym.BranchName,
      'CanDelete': true,
      'CategoryListString': '',
      'ChangeMoney': Number(value.ChangeMoney).toFixed(2),
      'HardwareProfileItemList': [],
      'IsDelete': false,
      'IsExpress': this.gymListService.SelectedGym.IsExpressGym,
      'Name': value.Name,
      'Region': this.gymListService.SelectedGym.Region,
    }
    // if (!this.isAddNew) {
    //   hwSettingObj.Id = this.rowItem.Id
    //   hwSettingObj.CanDelete = this.rowItem.CanDelete
    //   hwSettingObj.IsDelete = this.rowItem.IsDelete
    // }
    let catListString = ''
    for (const editItem of this.editCatArray) {
      if (editItem) {
        const hwItemObj = {
          'CategoryId': editItem.Id,
          'CategoryName': editItem.Name,
          'Code': editItem.Code,
          'HardwareProfileId': '',
          'Id': '',
        }
        hwSettingObj.HardwareProfileItemList.push(hwItemObj)
        catListString = catListString + hwItemObj.CategoryName + ','
      }
    }
    hwSettingObj.CategoryListString = catListString;
    this.hwSettingsForm.reset();
    this.hwTableForm.reset();
    // if (!this.isAddNew) {
    //   this.hwSettingsItems = this.hwSettingsItems.filter(item => item !== this.rowItem);
    //   if (this.rowItem.Id !== '') {
    //     this.rowItem.IsDelete = true
    //     this.addedItems.push(this.rowItem)
    //   }
    // }
    this.hwSettingsItems.push(hwSettingObj)
    this.addedItems.push(hwSettingObj)
    this.isAddNew = true;
    const hardWare = { 'BranchId': this.gymListService.SelectedGym.Id, 'HardwareProfileList': this.addedItems };
    this.saveSettings(hardWare);
  }

  deleteSetting(deletingItem) {
    this.deletingItem = deletingItem;
    let messageTitle = '';
    let messageBody = '';
    this.translateService.get('COMMON.Confirm').pipe(
      takeUntil(this.destroy$)
    ).subscribe(tranlstedValue => messageTitle = tranlstedValue);
    this.translateService.get('ADMIN.DeleteHardWare').pipe(
      takeUntil(this.destroy$)
    ).subscribe(tranlstedValue => messageBody = tranlstedValue);
    this.model = this.exceMessageService.openMessageBox('CONFIRM',{
      messageTitle: messageTitle,
      messageBody: messageBody
    });
  }

  deleteHandler() {
    this.hwSettingsItems = this.hwSettingsItems.filter(item => item !== this.deletingItem);
    if (this.deletingItem.Id !== '') {
      this.deletingItem.IsDelete = true
      this.addedItems.push(this.deletingItem)
      this.clearForm()
    } else {
      this.addedItems = this.addedItems.filter(item => item !== this.deletingItem);
    }
  }

  rowItemLoad(rowItem) {
    rowItem.ChangeMoney = Number(rowItem.ChangeMoney).toFixed(2);
    this.editCatArray = [];
    this.hwSettingsFormUpdate.patchValue(rowItem)
    this.hwItemsUpdate.forEach(element => {
      element.IsCheck = false
    });
    this.isAddNew = false;
    this.rowItem = rowItem
    rowItem.HardwareProfileItemList.forEach(element => {
      this.editCatArray.push(element)
      this.hwItemsUpdate.forEach(hwElement => {
        if (hwElement.Code === element.Code) {
          hwElement.IsCheck = true
        }
      });
    });
  }

  onBlurMethod(val: any) {
    if (this.isAddNew) {
      this.validateName(val);
    }
  }

  validateName(val: any) {
    try {
      if (val != null) {
        const inputName: string = val;
        this.hwSettingsItems.forEach(element => {
          if (element.Name === inputName) {
            this.hwSettingsForm.controls['Name'].setErrors({
              'duplicateName': false
            });
            this.exceService.callComponent(this.hwSettingsForm.controls['Name']);
          }
        });
      }
    } catch (e) {
      this.hwSettingsForm.controls['Name'].setErrors({
        'duplicateName': false
      });
      this.exceService.callComponent(this.hwSettingsForm.controls['Name']);
    }
  }

  clearForm() {
    this.isAddNew = true
    this.hwSettingsForm.reset();
    this.hwTableForm.reset();
  }

  saveSettings(hardWare) {
    this.adminService.SaveHardwareProfile(hardWare).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
        if (result) {
          this.adminService.getGymSettings('HARDWARE', null, this.branch.Id).pipe(
            takeUntil(this.destroy$)
          ).subscribe(
            res => {
              if (res) {
                this.hwSettingsItems = res.Data;
                this.hwSettingsesource = new DataTableResource(this.hwSettingsItems);
                this.hwSettingsesource.count().then(count => this.hwCount = count);
                this.hwSettingsCount = Number(this.hwSettingsItems);
              }
            });
          if (result.Status === 'OK') {
            this.translateService.get('ADMIN.SaveSuccess').pipe(
              takeUntil(this.destroy$)
            ).subscribe(translatedValue => this.message = translatedValue);
            this.type = 'SUCCESS';
            this.isShowInfo = true;
            setTimeout(() => {
              this.isShowInfo = false;
            }, 4000);
          }
          this.addedItems = []
          this.clearForm();
        }
      }, error => {
        this.translateService.get('ADMIN.SaveNotSuccess').pipe(
          takeUntil(this.destroy$)
        ).subscribe(translatedValue => this.message = translatedValue);
        this.type = 'DANGER';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      }, null);
  }

  showHardWareDetails(content, item) {
    this.modalReference = this.modalService.open(content, { width: '400' });
    this.rowItemLoad(item)
    this.hwSelected = item;
  }

  closeModal() {
    this.modalReference.close();
    this.editCatArray = []
    this.clearForm();
  }

  updateHW(deleteVal) {
    const val = Number(this.hwSettingsFormUpdate.get('ChangeMoney').value);
    const hwSettingObj = {
      'Id': this.hwSelected.Id,
      'BranchId': this.gymListService.SelectedGym.Id,
      'BranchName': this.gymListService.SelectedGym.BranchName,
      'CanDelete': true,
      'CategoryListString': '',
      'ChangeMoney': val.toFixed(2),
      'HardwareProfileItemList': [],
      'IsDelete': deleteVal,
      'IsExpress': this.gymListService.SelectedGym.IsExpressGym,
      'Name': this.hwSettingsFormUpdate.get('Name').value,
      'Region': this.gymListService.SelectedGym.Region,
    }

    let catListString = ''
    for (const editItem of this.editCatArray) {
      if (editItem) {
        const hwItemObj = {
          'CategoryId': editItem.CategoryId,
          'CategoryName': editItem.CategoryName,
          'Code': editItem.Code,
          'HardwareProfileId': this.hwSelected.Id,
          'Id': editItem.Id,
        }
        hwSettingObj.HardwareProfileItemList.push(hwItemObj)
        catListString = catListString + hwItemObj.CategoryName + ','
      }
    }

    hwSettingObj.CategoryListString = catListString;

    this.addedItems.push(hwSettingObj)
    const hardWare = { 'BranchId': this.gymListService.SelectedGym.Id, 'HardwareProfileList': this.addedItems };

    this.saveSettings(hardWare)

    this.closeModal();
  }

  nameChange(event) {
    this.hwSettingsFormUpdate.patchValue({'Name': event.target.value})
  }

  moneyChange(event) {
    const val = Number(event.target.value).toFixed(2)
    this.hwSettingsFormUpdate.patchValue({'ChangeMoney': val})
  }
}
