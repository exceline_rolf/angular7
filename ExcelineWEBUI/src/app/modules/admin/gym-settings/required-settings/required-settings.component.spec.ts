import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequiredSettingsComponent } from './required-settings.component';

describe('RequiredSettingsComponent', () => {
  let component: RequiredSettingsComponent;
  let fixture: ComponentFixture<RequiredSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequiredSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequiredSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
