
import { Component, OnInit, Injectable, OnDestroy } from '@angular/core';
import { AdminService } from '../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ExceError } from '../../../../shared/directives/exce-error/exce-error';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { BankAccountValidator } from '../../../../shared/directives/exce-error/util/validators/bank-account-validator';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { IMyDpOptions } from '../../../../shared/components/us-date-picker/interfaces';
import { Directive, Input, HostListener } from '@angular/core';
import { ExceService } from '../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { GymSettingsService } from '../gym-settings.service'
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil, mergeMap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'gym-info',
  templateUrl: './gym-info.component.html',
  styleUrls: ['./gym-info.component.scss']
})

export class GymInfoComponent implements OnInit, OnDestroy {
  private branchId: any;
  private branches: any
  private branch: any
  branchesForm: any;
  countryList: any;
  private currentCountryId: any
  regions: any;
  private modalReference: any;
  private itemResource: any;
  private items = [];
  private itemCount = 0;
  private filteredBranches: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private mngRegionForm: any;
  private mngCountryForm: any;
  private branchName: String = '';
  private branchNumber: any = '';
  regDate: String = '';
  formSubmited = false;
  private destroy$ = new Subject<void>();

  message: string;
  type: string;
  isShowInfo: boolean;

  formErrors = {
    'BranchName': '',
    'CountryId': '',
    'CollectionAccountNo': '',
    'BankAccountNo': '',
    'TelWork': '',
    'Fax': '',
    'Email': '',
    'RegisteredNo': '',
    'CompanyIdSalary': '',
    'Area': '',
    'OtherAdminEmail': '',
    'OtherAdminMobile': '',
    'CountryMobilePrefix': '',
    'Web': '',
    'BureauAccountNO': '',
    'KidSwapAccountNo': '',
    'OfficialName': '',
    'ReorderSmsReceiver': ''
  };

  validationMessages = {
    'BranchName': {
      'required': 'ADMIN.Nameisrequired',
    },
    'CountryId': {
      'required': 'ADMIN.MngBranchesValCountry'
    },
    'CollectionAccountNo': {
      'BankAccount': 'ADMIN.MngBranchesValBankAccount'
    },
    'BankAccountNo': {
      'BankAccount': 'ADMIN.MngBranchesValBankAccount',
      'required': 'ADMIN.MngBranchesValBankAccount'
    },
    'Email': {
      'email': 'ADMIN.InvalidEmail'
    },
    'OtherAdminEmail': {
      'email': 'ADMIN.InvalidEmail'
    },
    'BureauAccountNO': {
      'BankAccount': 'ADMIN.MngBranchesValBankAccount'
    },
    'KidSwapAccountNo': {
      'BankAccount': 'ADMIN.MngBranchesValBankAccount'
    },
  }

  constructor(private adminService: AdminService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private exceService: ExceService,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private exceLoginService: ExceLoginService,
    private gymListService: GymSettingsService) {

    this.branchesForm = this.fb.group({
      'BranchName': [null, [Validators.required]],
      'IsExpressGym': [null],
      'Addr1': [''],
      'Addr2': [''],
      'Addr3': [''],
      'ZipCode': [''],
      'ZipName': [null],
      'CountryId': [null, [Validators.required]],
      'CollectionAccountNo': ['', [BankAccountValidator.BankAccount]],
      'BankAccountNo': ['', [BankAccountValidator.BankAccount]],
      'TelWork': [null],
      'Fax': [''],
      'Email': [null, [Validators.email]],
      'RegisteredNo': [null],
      'Region': [null],
      'CompanyIdSalary': [null],
      'Area': [null],
      'OtherAdminEmail': [null],
      'OtherAdminMobile': [null],
      'CountryMobilePrefix': [null],
      'Web': [null],
      'BureauAccountNO': ['', [BankAccountValidator.BankAccount]],
      'KidSwapAccountNo': ['', [BankAccountValidator.BankAccount]],
      'OfficialName': [null],
      'ReorderSmsReceiver': ['']
    });

    this.mngRegionForm = this.fb.group({
      'Code': [null, [Validators.required]],
      'Name': [null, [Validators.required]],
      'CountryId': [null, [Validators.required]],
      'Description': [null],
    });

    this.mngCountryForm = this.fb.group({
      'CountryCode': [null, [Validators.required]],
      'Name': [null, [Validators.required]],
    });

    this.branchesForm.statusChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(_ => {
      UsErrorService.onValueChanged(this.branchesForm, this.formErrors, this.validationMessages)
    }, error => {

    }, () => {

    });

    if (this.gymListService.SelectedGym) {
      this.fetchData(this.gymListService.SelectedGym);
    }

    this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.fetchData(value.some)
    }, error => {

    }, () => {

    });
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }

    this.destroy$.next();
    this.destroy$.complete();
  }

  submitForm(value) {
    this.formSubmited = true;
    if (this.branchesForm.valid) {
      this.saveBranch(value)
      this.formSubmited = false;
    } else {
      UsErrorService.validateAllFormFields(this.branchesForm, this.formErrors, this.validationMessages);
    }
  }

  loadCountries() {
    this.adminService.getCountryDetails().pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.countryList = result.Data;
          this.currentCountryId = this.countryList[0].Id
        }
      }, error => {

      }, () => {

      });
  }

  filterInput(nameValue: any, regionValue: any, typeValue: any) {
    this.filteredBranches = this.branches

    if (nameValue) {
      this.filteredBranches = this.filterpipe.transform('BranchName', 'TEXT', this.filteredBranches, nameValue);
    }

    if (regionValue) {
      this.filteredBranches = this.filterpipe.transform('Region', 'TEXT', this.filteredBranches, regionValue);
    }

    if (typeValue !== 'ALL') {
      if (typeValue === 'true') {
        this.filteredBranches = this.filterpipe.transform('IsExpressGym', 'SELECT', this.filteredBranches, true);
      } else {
        this.filteredBranches = this.filterpipe.transform('IsExpressGym', 'SELECT', this.filteredBranches, false);
      }
    }

    this.itemResource = new DataTableResource(this.filteredBranches);
    this.itemResource.count().then(count => this.itemCount = count);
    this.itemResource.query(this.filteredBranches).then(items => this.items = items);
  }

  loadRegions(countryId: string) {
    this.regions = null;
    this.branchesForm.controls['Region'].setValue('');
    this.adminService.getRegions(countryId).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.regions = result.Data;
        }
      }, error => {

      }, () => {

      });
  }

  getBranches() {
    this.adminService.getBranches(-2).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.branches = result.Data;
          this.itemResource = new DataTableResource(result.Data);
          this.itemResource.count().then(count => this.itemCount = count);
          this.items = this.branches;
          this.itemCount = Number(result.Data.length);
        }
      }, error => {

      }, () => {

      });
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  openModel(content: any) {
    this.modalReference = this.modalService.open(content, { width: '320' });
  }

  rowDoubleClick(rowEvent: any) {
    this.branch = rowEvent.row.item;
    this.branchName = this.branch.BranchName
    this.branchNumber = this.branch.Id
    this.loadRegions(this.branch.CountryId)
    this.branchesForm.patchValue(this.branch);
    this.closeForm()
  }

  closeForm() {
    this.modalReference.close();
  }

  saveCountry(country) {
    this.adminService.addUpdateCountryDetails(country).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result.Data) {
          console.error('Saving country')
          this.closeForm();
        } else {
          alert('Error Saving')
        }
      }, error => {

      }, () => {

      });
  }

  saveRegion(region) {
    this.adminService.addUpdateCountryDetails(region).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result.Data) {
          this.closeForm();
        } else {
          alert('Error Saving')
        }
      }, error => {

      }, () => {

      });
  }

  saveBranch(GymInfo: any) {
    GymInfo.Id = this.branch.Id

    for (let key in GymInfo) {
      if (GymInfo[key] || GymInfo[key] !== 0) {
        this.branch[key] = GymInfo[key]
      }
    }

    // this.adminService.updateBranchDetails(this.branch).pipe(
    //   takeUntil(this.destroy$)
    // ).subscribe(result => {
    //   if (result.Data) {
    //     if (result.Status === 'OK') {
    //       this.translateService.get('ADMIN.SaveSuccess').pipe(
    //         takeUntil(this.destroy$)
    //       ).subscribe(translatedValue => this.message = translatedValue);
    //       this.type = 'SUCCESS';
    //       this.isShowInfo = true;
    //       setTimeout(() => {
    //         this.isShowInfo = false;
    //       }, 4000);
    //     }
    //   }
    // },
    //   error => {
    //     console.log(error);
    //   });

    this.adminService.updateBranchDetails(this.branch).pipe(
      mergeMap(result => {
        if (result.Data) {
          if (result.Status === 'OK') {
            this.type = 'SUCCESS';
            this.isShowInfo = true;
            return this.translateService.get('ADMIN.SaveSuccess').pipe(
              takeUntil(this.destroy$)
            );
          } else {
            this.type = 'DANGER';
            return this.translateService.get('ADMIN.SaveNotSuccess').pipe(
              takeUntil(this.destroy$)
            );
          }
        }
      }),
      takeUntil(this.destroy$)
    ).subscribe(translatedValue => {
      this.message = translatedValue;
      setTimeout(() => {
        this.isShowInfo = false;
      }, 4000);
    }, null, null);
  }

  onRowDoubleClick(branch) {
    this.branch = branch;
    this.branchName = this.branch.BranchName
    this.branchNumber = this.branch.Id
    this.regDate = this.branch.RegisteredDate
    this.loadCountries();
    this.loadRegions(this.branch.CountryId)
    this.branchesForm.patchValue(this.branch);
  }

  fetchData(branch) {
    this.branch = branch;
    this.loadRegions(this.branch.CountryId)
    this.loadCountries();
    this.branchName = this.branch.BranchName
    this.branchNumber = this.branch.Id
    this.regDate = this.branch.RegisteredDate
    this.branchesForm.patchValue(this.branch)
  }
}
