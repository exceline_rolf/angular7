
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceService } from '../../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { GymSettingsService } from '../../gym-settings.service'
import { AdminService } from '../../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { AccessControlTypeService } from '../access-control-type-service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'info-gym',
  templateUrl: './info-gym.component.html',
  styleUrls: ['./info-gym.component.scss']
})
export class InfoGymComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branch: any;
  private infoObj: any
  private typeString: string;
  InfoForm: any;

  constructor(private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private gymListService: GymSettingsService, private fb: FormBuilder,
    private accessControlTypeService: AccessControlTypeService
  ) {
    if (this.gymListService.SelectedGym) {
      this.fetchData(this.gymListService.SelectedGym);
    }
    this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
      ).subscribe((value) => {
      this.fetchData(value.some)
    });
    if (this.accessControlTypeService.SelectedType) {
      this.typeString = this.accessControlTypeService.SelectedType;
    }
    this.accessControlTypeService.accessProfileStringLoaded.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      result => {
        this.typeString = result
      });
  }

  ngOnInit() {
    this.InfoForm = this.fb.group({
      'ApiKey': [null],
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  fetchData(branch) {
    this.branch = branch;
    this.adminService.getGymSettings('OTHERINTEGRATION', 'Infogym', this.branch.Id).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result) {
          this.infoObj = result.Data
          this.InfoForm.patchValue(result.Data)
        }
      }, null, null);
  }

  saveSettings(value) {
    this.infoObj.ApiKey = value.ApiKey
    this.infoObj.SystemName = 'Infogym'
    this.infoObj.AccessContolTypes = this.typeString
    this.adminService.AddUpdateOtherIntegrationSettings({
      'BranchId': this.gymListService.SelectedGym.Id,
      'OtherIntegrationSettings': this.infoObj
    }).pipe(
      takeUntil(this.destroy$)
      ).subscribe(null, null, null);
  }

  clearForm() {
    this.InfoForm.reset()
  }
}
