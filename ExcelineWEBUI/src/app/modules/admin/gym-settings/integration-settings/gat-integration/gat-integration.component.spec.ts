import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GatIntegrationComponent } from './gat-integration.component';

describe('GatIntegrationComponent', () => {
  let component: GatIntegrationComponent;
  let fixture: ComponentFixture<GatIntegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GatIntegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GatIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
