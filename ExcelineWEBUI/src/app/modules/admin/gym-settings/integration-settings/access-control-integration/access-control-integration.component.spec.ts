import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessControlIntegrationComponent } from './access-control-integration.component';

describe('AccessControlIntegrationComponent', () => {
  let component: AccessControlIntegrationComponent;
  let fixture: ComponentFixture<AccessControlIntegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessControlIntegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessControlIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
