
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceService } from '../../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { GymSettingsService } from '../../gym-settings.service'
import { AdminService } from '../../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { AccessControlTypeService } from '../access-control-type-service'
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'access-control-integration',
  templateUrl: './access-control-integration.component.html',
  styleUrls: ['./access-control-integration.component.scss']
})
export class AccessControlIntegrationComponent implements OnInit, OnDestroy {
  private branch: any;
  ExItems = [];
  ACIForm: any;
  ExCount = 0;
  private ExResource: any;
  activities = []
  private accessProfiles = []
  accessProfileItems = [];
  private accessProfileForm: any;
  accessProfileCount = 0;
  private accessProfileResource: any;
  private articleItems = [];
  private articleCount = 0;
  private articleResource: any;
  private deletingItem: any
  private model: any;
  private typeString: string;
  private popOver: any;
  private destroy$ = new Subject<void>();

  private rowItem: any;
  constructor(private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private gymListService: GymSettingsService, private fb: FormBuilder,
    private accessControlTypeService: AccessControlTypeService
  ) {
    if (this.gymListService.SelectedGym) {
      this.fetchData(this.gymListService.SelectedGym);
    }
    this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.fetchData(value.some)
    }, null, null);
    if (this.accessControlTypeService.SelectedType) {
      this.typeString = this.accessControlTypeService.SelectedType;
    }
    this.accessControlTypeService.accessProfileStringLoaded.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        this.typeString = result
      }, null, null);
  }

  ngOnInit() {
    this.ACIForm = this.fb.group({
      'Name': [null, [Validators.required]],
      'ActivityId': [null, [Validators.required]],
    });
  }


  fetchData(branch) {
    this.branch = branch;

    this.fetchGATTable()

    this.adminService.getActivities(this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.activities = result.Data
        }
      }, null, null)

    this.adminService.GetAccessProfiles().pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.accessProfileItems = result.Data
          this.accessProfileResource = new DataTableResource(this.accessProfileItems);
          this.accessProfileResource.count().then(count => this.accessProfileCount = count);
          this.accessProfileCount = Number(this.accessProfileItems.length);
        }
      }, null, null)
  }

  saveSettings(value) {
    const AccessProfileList = []
    this.accessProfileItems.forEach(element => {
      if (element.isChecked) {
        AccessProfileList.push(element)
      }
    });
    if (value.ActivityId === 0) {
      value.ActivityName = ''
    } else {
      this.activities.forEach(element => {
        if (element.Id === value.ActivityId) {
          value.ActivityName = element.Name
        }
      })
    }
    value.AccessProfileList = AccessProfileList;
    for (const key in value) {
      if (this.rowItem[key]) {
        this.rowItem[key] = value[key]
      }
    }
    this.rowItem.AccessContolTypes = this.typeString;
    this.adminService.SaveGymIntegrationExcSettings({ 'BranchId': this.gymListService.SelectedGym.Id, 'GymIntegrationSettings': this.rowItem }).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        this.fetchGATTable()
        this.clearForm()
      }, null, null)
  }

  fetchGATTable() {
    this.adminService.getGymSettings('EXCINTEGRATION', null, this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.ExItems = result.Data
          this.ExResource = new DataTableResource(this.ExItems);
          this.ExResource.count().then(count => this.ExCount = count);
          this.ExCount = Number(this.ExItems.length);
        }
      }, null, null)
  }

  gatRowItemLoad(rowItem) {
    this.rowItem = rowItem
    this.accessProfileItems.forEach(element => {
      element.isChecked = false;
      element.IsDrawPuncheSelect = false
    })
    this.accessProfileItems.forEach(element => {
      this.rowItem.AccessProfileList.forEach(item => {
        if (item.Id === element.Id) {
          element.isChecked = true

          if (item.IsDrawPuncheSelect) {
            element.IsDrawPuncheSelect = true
          }
        }
      })
    });
    this.ACIForm.patchValue(this.rowItem)

  }

  viewLoad(rowItem, popOver) {
    if (this.popOver) {
      this.popOver.close();
    }
    this.popOver = popOver;
    this.popOver.open();
    this.accessProfiles = []
    rowItem.AccessProfileList.forEach(element => {
      this.accessProfiles.push(element)
    });
  }

  clearForm() {
    this.rowItem = null
    this.ACIForm.reset()
    this.accessProfileItems.forEach(element => {
      element.isChecked = false
      element.IsDrawPuncheSelect = false
    })
  }

  closePopOver() {
    this.popOver.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
