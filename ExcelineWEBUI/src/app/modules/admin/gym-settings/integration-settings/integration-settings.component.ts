import { Component, OnInit, OnDestroy, ComponentFactoryResolver, ViewContainerRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { GymInfoComponent } from '../gym-info/gym-info.component';
import { UsTabService } from '../../../../shared/components/us-tab/us-tab.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'integration-settings',
  templateUrl: './integration-settings.component.html',
  styleUrls: ['./integration-settings.component.scss']
})
export class IntegrationSettingsComponent implements OnInit, OnDestroy {


  public selectedTab = 0;
  private destroy$ = new Subject<void>();

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private usTabService: UsTabService,
    private cdr: ChangeDetectorRef
  ) {
    usTabService.tabSelectionChanged.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      tabIndex => {
        this.selectedTab = tabIndex
      }, null, null);
  }

  ngOnInit() {

  }
  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
