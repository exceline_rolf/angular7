

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceService } from '../../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { GymSettingsService } from '../../gym-settings.service'
import { AdminService } from '../../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { AccessControlTypeService } from '../access-control-type-service'
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';



@Component({
  selector: 'wellness',
  templateUrl: './wellness.component.html',
  styleUrls: ['./wellness.component.scss']
})
export class WellnessComponent implements OnInit, OnDestroy {
  private branch: any;
  private wellnessObj: any
  WellnessForm: any;
  private typeString: string;
  private destroy$ = new Subject<void>();


  constructor(private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private gymListService: GymSettingsService, private fb: FormBuilder,
    private accessControlTypeService: AccessControlTypeService
  ) {
    if (this.gymListService.SelectedGym) {
      this.fetchData(this.gymListService.SelectedGym);
    }
    this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.fetchData(value.some)
    }, null, null);
    if (this.accessControlTypeService.SelectedType) {
      this.typeString = this.accessControlTypeService.SelectedType;
    }
    this.accessControlTypeService.accessProfileStringLoaded.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        this.typeString = result
      }, null, null);
  }

  ngOnInit() {
    this.WellnessForm = this.fb.group({
      'ServiceBaseUrl': [null],
      'FacilityUrl': [null],
      'UserName': [null],
      'ApiKey': [null],
      'Password': [null],
      'Guid': [null],
    });
  }


  fetchData(branch) {
    this.branch = branch;
    this.adminService.getGymSettings('OTHERINTEGRATION', 'Wellness', this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.wellnessObj = result.Data
          this.WellnessForm.patchValue(result.Data)
        }
      }, null, null);
  }

  saveSettings(value) {
    for (let key in value) {
      if (this.wellnessObj[key]) {
        this.wellnessObj[key] = value[key]
      }
    }
    this.wellnessObj.AccessContolTypes = this.typeString
    this.wellnessObj.SystemName = 'Wellness'
    this.adminService.AddUpdateOtherIntegrationSettings({
      'BranchId': this.gymListService.SelectedGym.Id,
      'OtherIntegrationSettings': this.wellnessObj
    }).pipe(
      takeUntil(this.destroy$)
    ).subscribe(null, null, null);
  }

  clearForm() {
    this.WellnessForm.reset()
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
