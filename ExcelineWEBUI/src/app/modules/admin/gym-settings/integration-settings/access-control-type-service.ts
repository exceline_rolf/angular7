import { Injectable, EventEmitter, ElementRef } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable()
export class AccessControlTypeService {
  private selectedType: any;
  private selectedString:string;

  get SelectedType() {
   // this.accessProfileTypeLoaded.next(this.selectedType);
    return this.selectedType;
  }

  set SelectedType(SelectedType) {
    this.selectedType = SelectedType;
    this.accessProfileTypeLoaded.next(this.selectedType);
  }
  get SelectedString() {
    // this.accessProfileTypeLoaded.next(this.selectedType);
     return this.selectedType;
   }
 
   set SelectedString(SelectedString) {
     this.selectedString = SelectedString;
     this.accessProfileStringLoaded.next(this.selectedString);
   }

  public selectType: Subject<any> = new Subject();
  public accessProfileTypeLoaded: Subject<any> = new Subject();
  public accessProfileStringLoaded: Subject<any> = new Subject();
  

  public EventEmitter
  constructor() {

  }

  callComponent(value) {
    this.selectType.next({ some: value })
  }


}