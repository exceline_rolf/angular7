
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceService } from '../../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { GymSettingsService } from '../../gym-settings.service'
import { AdminService } from '../../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { AccessControlTypeService } from '../access-control-type-service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'arx',
  templateUrl: './arx.component.html',
  styleUrls: ['./arx.component.scss']
})
export class ArxComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branch: any;
  private arxObj: any
  private ArxSettings: any;
  private selectedValue = 0;
  private tempCodeDeleteDate: number
  private typeString: string;

  minValue;
  maxValue;
  ArxForm: any;
  arxFormats = []


  constructor(private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private gymListService: GymSettingsService, private fb: FormBuilder,
    private exceService: ExceService,
    private accessControlTypeService: AccessControlTypeService
  ) {
    if (this.gymListService.SelectedGym) {
      this.fetchData(this.gymListService.SelectedGym);
    }
    this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
      ).subscribe((value) => {
      this.fetchData(value.some)
    });
    if (this.accessControlTypeService.SelectedType) {
      this.typeString = this.accessControlTypeService.SelectedType;

    }
    this.accessControlTypeService.accessProfileStringLoaded.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      result => {
        this.typeString = result
      });
  }

  ngOnInit() {
    this.ArxForm = this.fb.group({
      'FormatTypeId': [null],
      'MinLength': [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$'),
      Validators.min(0), Validators.max(this.maxValue)])],
      'MaxLength': [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$'),
      Validators.min(this.minValue)])],
      'TempCodeDeleteDate': [null],
    });
  }

ngOnDestroy(): void {
  this.destroy$.next();
  this.destroy$.complete();
}

  fetchData(branch) {
    this.branch = branch;
    this.adminService.GetArxFormatType().pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      result => {
        if (result) {
          this.arxFormats = result.Data
          this.adminService.GetArxSettingDetail(this.branch.Id).pipe(
            takeUntil(this.destroy$)
            ).subscribe(settingsResult => {
              if (settingsResult) {
                const tempDel = ['TempCodeDeleteDate'];
                this.adminService.GetSelectedGymSettingsPost({ 'GymSetColNames': tempDel, 'IsGymSettings': true, 'BranchId': this.branch.Id }).pipe(
                  takeUntil(this.destroy$)
                  ).subscribe(
                  tempCodeResultResult => {
                    this.ArxSettings = settingsResult.Data
                    this.tempCodeDeleteDate = tempCodeResultResult.Data.TempCodeDeleteDate
                    this.ArxSettings[0].TempCodeDeleteDate = this.tempCodeDeleteDate
                    this.ArxForm.patchValue(this.ArxSettings[0])
                  })
              }
            })
        }
      })
  }

  onValueChange() {

  }

  saveSettings(value) {
    let arxObj = this.ArxSettings[this.selectedValue]
    for (let key in value) {
      if (arxObj[key] || arxObj[key] === 0) {
        arxObj[key] = value[key]
      }
    }
    this.adminService.SaveArxSettingDetail({
      'BranchId': this.gymListService.SelectedGym.Id,
      'arxFormatDetail': arxObj,
      'accessContolTypes': this.typeString
    }).pipe(
      takeUntil(this.destroy$)
      ).subscribe(null, null, null)
  }

  clearForm() {
    this.ArxForm.reset()

  }

  TypeChange(event) {
    let i = 0;
    for (const element of this.ArxSettings) {
      // tslint:disable-next-line:triple-equals  // comparing only values here
      if (element.FormatTypeId == event.target.value) {
        break
      }
      i++;
    }
    this.selectedValue = i
    this.ArxSettings[i].TempCodeDeleteDate = this.tempCodeDeleteDate
    this.ArxForm.patchValue(this.ArxSettings[i])

  }

  minValidator(val) {
    try {
      let min: number = val
      if (min > this.maxValue) {
        this.ArxForm.controls['MinLength'].setErrors({
          'MinValueLimit': false
        });
        this.exceService.callComponent(this.ArxForm.controls['MinLength']);
      }
    } catch (e) {
      this.ArxForm.controls['MinLength'].setErrors({
        'MinValueLimit': false
      });
      this.exceService.callComponent(this.ArxForm.controls['MinLength']);
    }
  }

  maxValidator(val) {
    try {
      let max: number = val
      if (max < this.minValue) {
        this.ArxForm.controls['MaxLength'].setErrors({
          'MaxValueLimit': false
        });
        this.exceService.callComponent(this.ArxForm.controls['MaxLength']);

      }

    } catch (e) {
      this.ArxForm.controls['MaxLength'].setErrors({
        'MaxValueLimit': false
      });
      this.exceService.callComponent(this.ArxForm.controls['MaxLength']);
    }
  }


  onBlurMethod(val: any, isMinVal) {
    if (isMinVal) {
      this.minValidator(val);

    } else {
      this.maxValidator(val);
    }
  }
}
