import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArxComponent } from './arx.component';

describe('ArxComponent', () => {
  let component: ArxComponent;
  let fixture: ComponentFixture<ArxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
