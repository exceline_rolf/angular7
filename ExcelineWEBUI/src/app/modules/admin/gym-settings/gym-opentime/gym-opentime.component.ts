import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceService } from '../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { GymSettingsService } from '../gym-settings.service'
import { AdminService } from '../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { AtLeastOneFieldValidator } from './atleastOneValidator'
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'gym-opentime',
  templateUrl: './gym-opentime.component.html',
  styleUrls: ['./gym-opentime.component.scss']
})

export class GymOpentimeComponent implements OnInit, OnDestroy {
  private branch: any;
  public openTinmeS: any;
  public gymOpenTimeForm: any;
  public dateForm: any;
  public openTimeItems = [];
  openTimeCount = 0;
  private openTimeResource: any;
  private dateSelected: false;
  private dateSettings: any;
  private duplicateEntry;
  private model;
  private deletedObjects = [];
  private addedObjects = [];
  public isAddNew = true;
  private rowItem;
  private openTimeItemsClone: any;
  private destroy$ = new Subject<void>();

  type: string;
  message: string;
  isShowInfo: boolean;

  constructor(private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private translateService: TranslateService,
    private gymListService: GymSettingsService,
    private fb: FormBuilder,
    private exceMessageService: ExceMessageService) {


    this.exceMessageService.okCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.okHandler();
    }, null, null);
    if (this.gymListService.SelectedGym) {
      this.fetchData(this.gymListService.SelectedGym);
    }
    this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      if (value.some) {
        this.fetchData(value.some)
      }
    }, null, null);
    this.gymOpenTimeForm = this.fb.group({
      'StartTime': [null, [Validators.required]],
      'EndTime': [null, [Validators.required]],
    });
    this.dateForm = new FormGroup({
      IsMonday: new FormControl(''),
      IsTuesday: new FormControl(''),
      IsWednesday: new FormControl(''),
      IsThursday: new FormControl(''),
      IsFriday: new FormControl(''),
      IsSaturday: new FormControl(''),
      IsSunday: new FormControl('')
    }, AtLeastOneFieldValidator);
  }

  ngOnInit() {
  }


  fetchData(branch) {
    this.branch = branch;
    this.adminService.getGymSettings('GYMOPENTIME', null, this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.openTimeItems = result.Data;
          this.openTimeResource = new DataTableResource(this.openTimeItems);
          this.openTimeResource.count().then(count => this.openTimeCount = count);
          this.openTimeCount = Number(this.openTimeItems.length);
        }
      }, null, null);
  }

  saveSettings() {
    const settingsObj = {
      'BranchId': this.branch.Id,
      'GymOpenTimeList': this.openTimeItems
    }
    this.adminService.SaveGymOpenTimes(settingsObj).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          if (result.Status === 'OK') {
            this.translateService.get('ADMIN.SaveSuccess').pipe(
              takeUntil(this.destroy$)
            ).subscribe(translatedValue => this.message = translatedValue);
            this.type = 'SUCCESS';
            this.isShowInfo = true;
            setTimeout(() => {
              this.isShowInfo = false;
            }, 4000);
          }
        }
      }, error => {
        this.translateService.get('ADMIN.SaveNotSuccess').pipe(
          takeUntil(this.destroy$)
        ).subscribe(translatedValue => this.message = translatedValue);
        this.type = 'DANGER';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      }, null);
  }


  onCheckChange(event) {
    if (event.target.checked) {
      // this.dateSelected = true
    }

  }

  addSetting(timeSettings, dateSettings) {
    this.openTimeItemsClone = this.openTimeItems
    if (!this.isAddNew) {
      this.openTimeItemsClone = this.openTimeItemsClone.filter(item => item !== this.rowItem);
    }
    this.duplicateEntry = false
    const openTimeObj = {
      'BranchId': this.branch.Id,
      'BranchName': this.branch.BranchName,
      'IsExpress': this.branch.IsExpressGym,
      'EndTime': '2017-01-01T' + timeSettings.EndTime + ':00',
      'StartTime': '2017-01-01T' + timeSettings.StartTime + ':00',
      'IsFriday': false,
      'IsMonday': false,
      'IsSaturday': false,
      'IsSunday': false,
      'IsThursday': false,
      'IsTuesday': false,
      'IsWednesday': false,
      'Region': this.branch.Region,
      'Days': '',
      'Id': ''
    }
    let daysString = ''
    if (dateSettings.IsMonday) {
      daysString = daysString + 'Mon' + ', '
      openTimeObj.IsMonday = true
      this.timeValidator('IsMonday', openTimeObj.StartTime, openTimeObj.EndTime)
    }
    if (dateSettings.IsTuesday) {
      daysString = daysString + 'Tue' + ', '
      openTimeObj.IsTuesday = true
      this.timeValidator('IsTuesday', openTimeObj.StartTime, openTimeObj.EndTime)
    }
    if (dateSettings.IsWednesday) {
      daysString = daysString + 'Wed' + ', '
      openTimeObj.IsWednesday = true
      this.timeValidator('IsWednesday', openTimeObj.StartTime, openTimeObj.EndTime)
    }
    if (dateSettings.IsThursday) {
      daysString = daysString + 'Thu' + ', '
      openTimeObj.IsThursday = true
      this.timeValidator('IsThursday', openTimeObj.StartTime, openTimeObj.EndTime)
    }
    if (dateSettings.IsFriday) {
      daysString = daysString + 'Fri' + ', '
      openTimeObj.IsFriday = true
      this.timeValidator('IsFriday', openTimeObj.StartTime, openTimeObj.EndTime)
    }
    if (dateSettings.IsSaturday) {
      daysString = daysString + 'Sat' + ', '
      openTimeObj.IsSaturday = true
      this.timeValidator('IsSaturday', openTimeObj.StartTime, openTimeObj.EndTime)
    }
    if (dateSettings.IsSunday) {
      daysString = daysString + 'Sun' + ', '
      openTimeObj.IsSunday = true
      this.timeValidator('IsSunday', openTimeObj.StartTime, openTimeObj.EndTime)
    }
    openTimeObj.Days = daysString;
    if (!this.duplicateEntry) {
      if (!this.isAddNew) {
        openTimeObj.Id = this.rowItem.Id
        this.openTimeItems = this.openTimeItems.filter(item => item !== this.rowItem);
      }
      this.openTimeItems.push(openTimeObj)
      this.openTimeResource = new DataTableResource(this.openTimeItems);
      this.openTimeResource.count().then(count => this.openTimeCount = count);
      this.openTimeCount = Number(this.openTimeItems);
      this.addedObjects.push(openTimeObj)
      this.clearForm()
    } else {
      this.openExceMessage('Exceline', 'Duplicate Entry')
    }
  }


  timeValidator(day, fromTime, toTime) {
    this.openTimeItemsClone.forEach(element => {
      if (element[day]) {
        const existingFromTime = this.getTime(element.StartTime)
        const existingToTime = this.getTime(element.EndTime)
        const newFromTime = this.getTime(fromTime)
        const newToTime = this.getTime(toTime)

        if (!((Date.parse('01/01/2017 ' + newFromTime) > Date.parse('01/01/2017 ' + existingToTime))
          || (Date.parse('01/01/2017 ' + existingFromTime) > Date.parse('01/01/2017 ' + newToTime)))) {
          this.duplicateEntry = true
        }
      }

    });
  }

  getTime(dateTimeObje: string) {
    return dateTimeObje.split('T')[1]
  }

  openExceMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('ERROR',
      {
        messageTitle: title,
        messageBody: body
      }
    );
  }
  okHandler() {
    this.clearForm()
  }


  deleteSetting(setting) {
    this.openTimeItems = this.openTimeItems.filter(item => item !== setting);
    if (setting in this.addedObjects) {
      this.addedObjects = this.addedObjects.filter(item => item !== setting);
    } else {
      this.deletedObjects.push(setting)
    }
  }

  rowItemLoad(rowItem) {
    this.rowItem = rowItem
    const sTime: string = rowItem.StartTime + ''
    const eTime: string = rowItem.EndTime + ''

    const StartTime = sTime.substring(11, 16)
    const EndTime = eTime.substring(11, 16)

    this.isAddNew = false;
    this.gymOpenTimeForm.patchValue({ 'StartTime': StartTime, 'EndTime': EndTime })
    this.dateForm.patchValue(rowItem)
  }

  clearForm() {
    this.gymOpenTimeForm.reset();
    this.dateForm.reset();
    this.isAddNew = true
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
