
import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { ExceService } from '../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { GymSettingsService } from '../gym-settings.service'
import { AdminService } from '../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'ecconomy-settings',
  templateUrl: './ecconomy-settings.component.html',
  styleUrls: ['./ecconomy-settings.component.scss']
})
export class EcconomySettingsComponent implements OnInit, OnDestroy {
  private branch: any;
  private ecconomySettings: any;
  public ecconomySettingsFrom: any;
  public gymList: any;
  public days = [];
  private destroy$ = new Subject<void>();

  message: string;
  type: string;
  isShowInfo: boolean;

  constructor(private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private gymListService: GymSettingsService, private fb: FormBuilder) {
    if (this.gymListService.SelectedGym) {
      this.fetchData(this.gymListService.SelectedGym);
    }
    this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.fetchData(value.some)
    }, null, null);
  }

  ngOnInit() {
    this.ecconomySettingsFrom = this.fb.group({
      'InvoiceAccountNo': [null],
      'PaymentAccountNo': [null],
      'DebtCollectionPeriod': [null],
      'RemainderPeriod': [null],
      'SmsReminderPeriod': [null],
      'RemainderFee': [null],
      'SmsRemainderFee': [null],
      'InvoiceCharge': [null],
      'SmsInvoiceCharge': [null],
      'InvoiceCancellationPeriod': [null],
      'MemberFeeBranchID': [null],
      'MemberFeeGenerateDay': [null],
      'SuspendedDays': [null],
      'NoOfOrdersWhenResign': [null],
      'OnNegativeAccount': [null],
      'NetsAssignmentNo': [null],
      'NetsCancellationPeriod': [null],
      'CreditSaleLimit': [null],
      'CreditPaidDeviationSettableAmount': [null],
      'PaidAccessMode': [null],
      'MoveToPrepaidBalanace': [null],
      'MoveToShopAccount': [null],
      'RestPlusMonth': [null],
      'PostPay': [null],
      'EnrollmentFeeFirstOrderIfSponsor': [null],
      'CreditUnpaidInvoiceFee': [null],
      'CreditUnpaidRemainderFee': [null],
      'ShopOnNextOrder': [null],
      'OnAccount': [null],
      'IsDefaultCustomerAvailable': [null],
      'IsPayButtonAvailable': [null],
      'IsPettyCashAllowedOnUser': [null],
      'IsBankTerminalIntegrated': [null],
      'IsCashSalesActivated': [null],
      'IsPrintInvoiceInShop': [null],
      'IsPrintReceiptInShop': [null],
      'IsSmsInvoiceShop': [null],
      'IsDailySettlementForAll': [null],
      'EditRemainingPunches': [null],
      'IsMemberFee': [null],
      'IsMemberFeeMonth': [null],
      'IsMemberFeeAutoGenerate': [null],
      'IsMemberFeeATG': [null],
      'BasisX': [null],
      'CCXEnabled': [null],
      'PrintIpccx': [null],
      'PrintPpccx': [null],
      'PrintSpccx': [null],
      'NotEditableVoucher': [null],
      'ShopReturnOnSelectedBranchOnly': [null]
    });
  }


  fetchData(branch) {
    this.branch = branch;
    this.adminService.getGymSettings('ECONOMY', null, this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.ecconomySettings = result.Data;
          this.getGymList();
          this.ecconomySettingsFrom.patchValue(this.ecconomySettings[0])
          // if (!this.ecconomySettings[0].BasisX) {

          //   this.ecconomySettingsFrom.controls.CCXEnabled.disable();
          //   this.ecconomySettingsFrom.controls.PrintIpccx.disable();
          //   this.ecconomySettingsFrom.controls.PrintPpccx.disable();
          //   this.ecconomySettingsFrom.controls.PrintSpccx.disable();
          // }

          if (!this.ecconomySettings[0].IsMemberFee) {
            this.ecconomySettingsFrom.controls.IsMemberFeeMonth.disable();
            this.ecconomySettingsFrom.controls.IsMemberFeeAutoGenerate.disable();
            this.ecconomySettingsFrom.controls.IsMemberFeeATG.disable();
          }
        }
      }, null, null
    )
  }


  getGymList() {
    this.adminService.getBranches(-1).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.gymList = result.Data;
        for (let i = 1; i < 32; i++) {
          this.days.push(i);
        }
      }
    }, null, null);
  }

  saveSettings(value) {
    // tslint:disable-next-line:forin
    for (const key in value) {
      this.ecconomySettings[0][key] = value[key]
    }

    this.adminService.SaveGymEconomySettings(this.ecconomySettings[0]).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.translateService.get('ADMIN.SaveSuccess').pipe(
            takeUntil(this.destroy$)
          ).subscribe(translatedValue => this.message = translatedValue);
          this.type = 'SUCCESS';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
        }
      }, null, null);
  }

  onBasisChangwe(isChecked) {
    if (isChecked) {
      this.ecconomySettingsFrom.controls.CCXEnabled.enable();
      this.ecconomySettingsFrom.controls.PrintIpccx.enable();
      this.ecconomySettingsFrom.controls.PrintPpccx.enable();
      this.ecconomySettingsFrom.controls.PrintSpccx.enable();
    } else {
      this.ecconomySettingsFrom.controls.CCXEnabled.disable();
      this.ecconomySettingsFrom.controls.PrintIpccx.disable();
      this.ecconomySettingsFrom.controls.PrintPpccx.disable();
      this.ecconomySettingsFrom.controls.PrintSpccx.disable();
    }
  }

  onMemmberFeeChange(isChecked){
    if (isChecked) {
      this.ecconomySettingsFrom.controls.IsMemberFeeMonth.enable();
      this.ecconomySettingsFrom.controls.IsMemberFeeAutoGenerate.enable();
      this.ecconomySettingsFrom.controls.IsMemberFeeATG.enable();
    } else {
      this.ecconomySettingsFrom.controls.IsMemberFeeMonth.disable();
      this.ecconomySettingsFrom.controls.IsMemberFeeAutoGenerate.disable();
      this.ecconomySettingsFrom.controls.IsMemberFeeATG.disable();
    }
  }

  ngOnDestroy () {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
