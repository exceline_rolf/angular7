import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymSettingsComponent } from './gym-settings.component';

describe('GymSettingsComponent', () => {
  let component: GymSettingsComponent;
  let fixture: ComponentFixture<GymSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
