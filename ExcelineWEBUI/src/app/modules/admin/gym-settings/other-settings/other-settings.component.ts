import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceService } from '../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { GymSettingsService } from '../gym-settings.service'
import { AdminService } from '../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'other-gym-settings',
  templateUrl: './other-settings.component.html',
  styleUrls: ['./other-settings.component.scss']
})
export class OtherSettingsComponent implements OnInit, OnDestroy {
  private branch: any;
  private otherSettings: any;
  otherSettingsFrom: any;
  private destroy$ = new Subject<void>();

  // Variables in this block are intentionally left ambiguous, as they are used for various backend confirmations.
  type: string;
  isShowInfo: boolean;
  message: string;
  // -----

  constructor(private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private gymListService: GymSettingsService, private fb: FormBuilder) {
    if (this.gymListService.SelectedGym) {
      this.fetchData(this.gymListService.SelectedGym);
    }
    this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.fetchData(value.some)
    }, null, null);
  }

  ngOnInit() {
    this.otherSettingsFrom = this.fb.group({
      'TimeOutAfter': [null],
      'AnonymizingTimePeriod': [null],
      'PhoneCode': [null],
      'Gmt': [null],
      'IsShopAvailable': [null],
      'IsLogoutFromShopAfterSale': [null],
      'IsAntiDopingRequired': [null],
      'CategoryFollowUpNoVisits': [null],
      'FollowUpNewPerson': [null],
      'IsPrintATGAuthorization': [null],
      'IsFreezeConfirmationPopUp': [null],
      'IsLoginShop': [null],
      'IsPrieviewWhenPrint': [null],
      'IsBookingBackDate': [null],
      'IsCalendarPriority': [null],
      'IsIBookingIntegrated': [null],
      'IsValidateShopGymId': [null],
      'IsUsingIntroducedBy': [null],
      'IsUsingGantner': [null],
      'SortBranchesById': [null]
    });
  }


  fetchData(branch) {
    this.branch = branch;
    this.adminService.getGymSettings('OTHER', null, this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.otherSettings = result.Data;
          this.otherSettingsFrom.patchValue(this.otherSettings[0])
        }
      }, error => {
        this.translateService.get('ADMIN.UnableToFetchData').pipe(
          takeUntil(this.destroy$)
        ).subscribe(trans => this.message = trans);
        this.type = 'INFO';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
    }, null)
  }

  saveSettings(value) {
    value.Id = this.otherSettings[0].Id
    // tslint:disable-next-line:forin
    for (const key in value) {
      this.otherSettings[0][key] = value[key]
    }
    this.otherSettings[0].AnonymizingTimePeriod = value.AnonymizingTimePeriod + ''
    this.otherSettings[0].TimeOutAfter = value.TimeOutAfter + ''
    this.adminService.SaveGymOtherSettings(this.otherSettings[0]).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          if (result.Status === 'OK') {
            this.translateService.get('ADMIN.SaveSuccess').pipe(
              takeUntil(this.destroy$)
            ).subscribe(trans => this.message = trans);
            this.type = 'SUCCESS';
            this.isShowInfo = true;
            setTimeout(() => {
              this.isShowInfo = false;
            }, 4000);
          }
        }
      }, error => {
        this.translateService.get('ADMIN.SaveNotSuccess').pipe(
          takeUntil(this.destroy$)
        ).subscribe(trans => this.message = trans);
        this.type = 'DANGER';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      }, null);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
