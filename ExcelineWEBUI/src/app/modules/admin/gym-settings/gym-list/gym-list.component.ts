
import { Component, OnInit, Output, EventEmitter, Injectable, ElementRef, OnDestroy } from '@angular/core';
import { AdminService } from '../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ExceError } from '../../../../shared/directives/exce-error/exce-error';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { BankAccountValidator } from '../../../../shared/directives/exce-error/util/validators/bank-account-validator';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { IMyDpOptions } from '../../../../shared/components/us-date-picker/interfaces';
import { Directive, Input, HostListener } from '@angular/core';
import { ExceService } from '../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { GymSettingsService } from '../gym-settings.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'gym-list',
  templateUrl: './gym-list.component.html',
  styleUrls: ['./gym-list.component.scss']
})
export class GymListComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId: any;
  private branches: any
  private branch: any
  private branchesForm: any;
  private modalReference: any;
  private itemResource: any;
  private items = [];
  private itemCount = 0;
  private filteredBranches: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  branchName: String = '';
  branchNumber: any = '';
  private regDate: String = '';


  @Output() onRowDoubleClick: EventEmitter<number> = new EventEmitter<number>();

  constructor(private adminService: AdminService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private exceService: ExceService,
    private exceLoginService: ExceLoginService,
    private gymListService: GymSettingsService) { }

  ngOnInit() {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
    this.adminService.getBranches().pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result) {
          this.branches = result.Data;
          this.branch = this.branches[0];
          //  this.gymListService.setSelectedBranch(this.branch);
          this.gymListService.callComponent(this.branch);
          this.gymListService.SelectedGym = this.branch;
          // this.onRowDoubleClick.emit(this.branch);
          this.branchName = this.branch.BranchName
          this.branchNumber = this.branch.Id
          this.regDate = this.branch.RegisteredDate
        }
      }, null, null)
      this.getBranches();
  }
  filterInput(nameValue: any, regionValue: any, typeValue: any) {
    this.filteredBranches = this.branches
    if (nameValue) {
      this.filteredBranches = this.filterpipe.transform('BranchName', 'TEXT', this.filteredBranches, nameValue);
    }
    if (regionValue) {
      this.filteredBranches = this.filterpipe.transform('Region', 'TEXT', this.filteredBranches, regionValue);
    }
    if (typeValue !== 'ALL') {
      if (typeValue === 'true') {
        this.filteredBranches = this.filterpipe.transform('IsExpressGym', 'SELECT', this.filteredBranches, true);
      } else {
        this.filteredBranches = this.filterpipe.transform('IsExpressGym', 'SELECT', this.filteredBranches, false);
      }
    }
    this.itemResource = new DataTableResource(this.filteredBranches);
    this.itemResource.count().then(count => this.itemCount = count);
    this.itemResource.query(this.filteredBranches).then(items => this.items = items);
  }

  getBranches() {
    this.adminService.getBranches(-1).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.branches = result.Data;
          this.itemResource = new DataTableResource(result.Data);
          this.itemResource.count().then(count => this.itemCount = count);
          this.items = this.branches;
          this.itemCount = Number(result.Data.length);
        }
      }, null, null)
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  openModel(content: any) {
    this.modalReference = this.modalService.open(content);

  }

  rowDoubleClick(rowEvent: any) {
    this.branch = rowEvent.row.item;
    this.gymListService.SelectedGym = this.branch;
    this.branchName = this.branch.BranchName
    this.branchNumber = this.branch.Id
    this.gymListService.callComponent(this.branch);
    // this.onRowDoubleClick.emit(this.branch);
    this.closeForm()
  }

  closeForm() {
    this.modalReference.close();
  }


}
