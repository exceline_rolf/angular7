import { Component, OnInit, OnDestroy, ComponentFactoryResolver, ViewContainerRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { GymInfoComponent } from './gym-info/gym-info.component';
import { UsTabService } from '../../../shared/components/us-tab/us-tab.service';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-gym-settings',
  templateUrl: './gym-settings.component.html',
  styleUrls: ['./gym-settings.component.scss']
})
export class GymSettingsComponent implements OnInit, OnDestroy {
  selectedTab;
  private destroy$ = new Subject<void>();

  constructor(
    private exceBreadcrumbService: ExceBreadcrumbService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private usTabService: UsTabService,
    private cdr: ChangeDetectorRef
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.DepartmentC',
      url: '/adminstrator/gym-settings'
    });

    usTabService.verticleTabSelectionChanged.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      tabIndex => {
        this.selectedTab = tabIndex
      }, error => {

      }, () => {

      });
  }

  ngOnInit() {

  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}







