import { TestBed, inject } from '@angular/core/testing';

import { GymSettingsService } from './gym-settings.service';

describe('GymSettingsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GymSettingsService]
    });
  });

  it('should be created', inject([GymSettingsService], (service: GymSettingsService) => {
    expect(service).toBeTruthy();
  }));
});
