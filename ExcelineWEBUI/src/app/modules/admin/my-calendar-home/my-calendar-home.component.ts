import { Component, OnInit, Input } from '@angular/core';
import { ExceLoginService } from '../../login/exce-login/exce-login.service';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';


@Component({
  selector: 'app-my-calendar-home',
  templateUrl: './my-calendar-home.component.html',
  styleUrls: ['./my-calendar-home.component.scss']
})
export class MyCalendarHomeComponent implements OnInit {

  employeeId: number;
  constructor(
    private exceLoginService: ExceLoginService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {

    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.MYCALENDAR',
      url: '/adminstrator/my-calandar-home'
    });
  }

  ngOnInit() {
    this.employeeId = this.exceLoginService.LoggedUserDetails.EmployeeId;
  }

}
