import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalMemberFollowupComponent } from './cal-member-followup.component';

describe('CalMemberFollowupComponent', () => {
  let component: CalMemberFollowupComponent;
  let fixture: ComponentFixture<CalMemberFollowupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalMemberFollowupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalMemberFollowupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
