import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyCalendarHomeComponent } from './my-calendar-home.component';

describe('MyCalendarHomeComponent', () => {
  let component: MyCalendarHomeComponent;
  let fixture: ComponentFixture<MyCalendarHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyCalendarHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyCalendarHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
