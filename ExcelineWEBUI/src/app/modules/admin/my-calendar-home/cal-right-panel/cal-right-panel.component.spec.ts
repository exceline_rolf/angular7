import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalRightPanelComponent } from './cal-right-panel.component';

describe('CalRightPanelComponent', () => {
  let component: CalRightPanelComponent;
  let fixture: ComponentFixture<CalRightPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalRightPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalRightPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
