import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsTileMenuService } from '../../common/us-tile-menu/us-tile-menu.service';
import { ExceLoginService } from '../../login/exce-login/exce-login.service';
import { UsBreadcrumbService } from '../../../shared/components/us-breadcrumb/us-breadcrumb.service';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss']
})
export class AdminHomeComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  selectedModule: any;
  modules: any;
  constructor(
    private exceLoginService: ExceLoginService,
    private usTileMenuService: UsTileMenuService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'COMMON.Administrator',
      url: '/adminstrator/home'
    });
  }

  ngOnInit() {
    if (this.usTileMenuService.SelectedModule) {
      this.selectedModule = this.usTileMenuService.SelectedModule;
    } else {
      this.usTileMenuService.getUserInfo(this.exceLoginService.CurrentUser.username).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
          this.modules = result.Data.ModuleList;
          this.selectedModule = this.modules.find(x => x.ID === 'ExceAdmin');
        }, null, null);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
