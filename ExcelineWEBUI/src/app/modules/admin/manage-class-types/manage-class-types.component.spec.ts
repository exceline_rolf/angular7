import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageClassTypesComponent } from './manage-class-types.component';

describe('ManageClassTypesComponent', () => {
  let component: ManageClassTypesComponent;
  let fixture: ComponentFixture<ManageClassTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageClassTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageClassTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
