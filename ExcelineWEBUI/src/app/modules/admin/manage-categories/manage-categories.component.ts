import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { AdminService } from '../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ExceError } from '../../../shared/directives/exce-error/exce-error';
import { DataTableResource } from '../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../shared/components/us-modal/us-modal';
import { ExceLoginService } from '../../../modules/login/exce-login/exce-login.service';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-manage-categories',
  templateUrl: './manage-categories.component.html',
  styleUrls: ['./manage-categories.component.scss'],
})

export class ManageCategoriesComponent implements OnInit, OnDestroy {
  public categoryForm: FormGroup;
  private closeResult: string;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private branchId: number
  private isAddNew = false;
  private dataArray: any;
  private tableHeaderMetaData: any;
  private categories?: any[];
  private filteredCategories?: any[];
  categoryTypes: any;
  private itemResource: any = 0;
  items = [];
  itemCount = 0;
  private addNew: any;
  private generatedCode: String;
  private savedId: String;
  private nameValue = '';
  private typeValue: any;
  private modalReference?: any;
  private editCat: any;
  private editCatClone; any;
  private currentColor: any;
  private offset = 0;
  limit = 50;
  private destroy$ = new Subject<void>();

  constructor(
    private exceBreadcrumbService: ExceBreadcrumbService,
    private adminService: AdminService,
    private modalService: UsbModal,
    private fb: FormBuilder, private exceLoginService: ExceLoginService) {

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;

    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }, null, null);

    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.CATEGORIES',
      url: '/adminstrator/manage-categories'
    });
  }

  ngOnInit() {
    this.adminService.getCategories('').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.categories = result.Data;
        this.itemResource = new DataTableResource(result.Data);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.categories;
        this.itemCount = Number(result.Data.length);
      }
    }, null, null);

    this.adminService.getCategoryTypes(this.branchId, '').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.categoryTypes = result.Data;
      }
    }, null, null);

    this.filteredCategories = this.categories;
    this.categoryForm = this.fb.group({
      'TypeId': [null, [Validators.pattern('.*\\S.*'), Validators.required]],
      'Name': [null, Validators.compose([Validators.pattern('.*\\S.*'), Validators.required])],
      'Color': [''],
      'Description': [null]
    });
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  setOffset(): number {
    return this.offset;
  }

  openCategoryModel(content: any, isAddNew: boolean) {
    this.isAddNew = isAddNew;
    this.modalReference = this.modalService.open(content, { width: '500' });
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  rowDoubleClick(rowEvent) {
    this.editCat = rowEvent.row.item;
    this.categoryForm.controls['TypeId'].setValue(this.editCat.TypeId);
    this.categoryForm.controls['Name'].setValue(this.editCat.Name);
    this.currentColor = this.editCat.Color;
    this.categoryForm.controls['Description'].setValue(this.editCat.Description);
  }

  clickButton(item) {
  }

  filterInput(nameValue?: any, typeValue?: any) {
    if (typeValue === 'ALL') {
      this.filteredCategories = this.filterpipe.transform('Name', 'NAMEFILTER', this.categories, nameValue);
    } else {
      this.filteredCategories = this.filterpipe.transform('Name', 'NAMEFILTER', this.categories, nameValue);
      this.filteredCategories = this.filterpipe.transform('TypeId', 'SELECT', this.filteredCategories, typeValue);
    }
    this.itemResource = new DataTableResource(this.filteredCategories);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredCategories);
    this.nameValue = nameValue;
    this.typeValue = typeValue;
  }

  saveCategory(value) {
    value.BranchId = this.branchId;
    value.ActiveStatus = true;
    value.Color = this.currentColor;
    value.Code = value.Name + Math.floor(Math.random() * 10000000000);
    this.generatedCode = value.Code;
    this.addNew = value;
    this.adminService.saveCategory(value).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      this.savedId = result.Data;
    }, null, null);

    this.closeForm();
    this.categories.push(
      {
        'Id': this.savedId,
        'TypeId': this.addNew.TypeId,
        'TypeName': '',
        'BranchId': this.branchId,
        'Code': this.generatedCode,
        'CategoryTypeCode': '',
        'Name': this.addNew.Name,
        'Description': this.addNew.Description,
        'CategoryImage': null,
        'CreatedDate': '1900-01-01T00:00:00',
        'LastModifiedDate': '0001-01-01T00:00:00',
        'CreatedUser': '',
        'LastModifiedUser': '',
        'ActiveStatus': true,
        'IsCheck': false,
        'Color': this.currentColor
      });
    this.filterResults();
  }

  editCategory(value) {
    this.editCatClone = this.editCat;
    this.editCat.Name = value.Name;
    this.editCat.TypeId = value.TypeId;
    this.editCat.Description = value.Description;
    this.editCat.Color = this.currentColor;
    this.adminService.updateCategory(this.editCat).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      this.categories = this.categories.filter(item => item !== this.editCatClone);
      this.filteredCategories = this.categories.filter(item => item !== this.editCat);
      this.categories.push(this.editCat);
      this.filteredCategories = this.categories;
      this.closeForm();
      this.filterResults();
    }, null, null);
  }

  deleteCategory(value) {
    this.adminService.deleteCategory(this.editCat).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      this.categories = this.categories.filter(item => item !== this.editCat);
      this.filterResults();
      this.closeForm();
    }, null, null);
  }

  filterResults() {
    this.categories = this.sortByKey(this.categories, 'Name');
    if (this.typeValue === 'ALL' || this.typeValue == null) {
      if (this.nameValue !== '') {
        this.filteredCategories = this.filterpipe.transform('Name', 'NAMEFILTER', this.categories, this.nameValue);
      }
    } else {
      if (this.nameValue !== '') {
        this.filteredCategories = this.filterpipe.transform('Name', 'NAMEFILTER', this.categories, this.nameValue);
        this.filteredCategories = this.filterpipe.transform('TypeId', 'SELECT', this.filteredCategories, this.typeValue);
      } else {
        this.filteredCategories = this.filterpipe.transform('TypeId', 'SELECT', this.categories, this.typeValue);
      }
    }
    if ((this.typeValue === 'ALL' || this.typeValue == null) && (this.nameValue === '')) {
      this.itemResource = new DataTableResource(this.categories);
      this.itemResource.count().then(count => this.itemCount = count);
      this.reloadItems(this.categories);
    } else {
      this.filteredCategories = this.sortByKey(this.filteredCategories, 'Name');
      this.itemResource = new DataTableResource(this.filteredCategories);
      this.itemResource.count().then(count => this.itemCount = count);
      this.reloadItems(this.filteredCategories);
    }
  }

  closeForm() {
    this.categoryForm.reset();
    this.currentColor = '#fff';
    this.modalReference.close();
  }

  colorChange(color) {
    this.currentColor = color;
  }

  sortByKey(array: any, key: any) {
    return array.sort(function (a, b) {
      const x = a[key]; const y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }
}
