import { TestBed, inject } from '@angular/core/testing';

import { ManageSeasonsService } from './manage-seasons.service';

describe('ManageSeasonsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ManageSeasonsService]
    });
  });

  it('should be created', inject([ManageSeasonsService], (service: ManageSeasonsService) => {
    expect(service).toBeTruthy();
  }));
});
