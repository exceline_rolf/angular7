import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class ManageSeasonsService {

  public selectSeason = new Subject<any>();

  private classList: any[];

  set ClassList(classList) {
    this.classList = classList;
  }

  get ClassList() {
    return this.classList;
  }

  private selectedSeason: any;

  set SelectedSeason(selectedSeason) {
    this.selectSeason.next(selectedSeason);
    this.selectedSeason = selectedSeason;
  }

  get SelectedSeason() {
    return this.selectedSeason;
  }

  constructor() { }

}
