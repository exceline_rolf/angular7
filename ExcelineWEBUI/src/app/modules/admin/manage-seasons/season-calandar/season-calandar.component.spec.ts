import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonCalandarComponent } from './season-calandar.component';

describe('SeasonCalandarComponent', () => {
  let component: SeasonCalandarComponent;
  let fixture: ComponentFixture<SeasonCalandarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeasonCalandarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonCalandarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
