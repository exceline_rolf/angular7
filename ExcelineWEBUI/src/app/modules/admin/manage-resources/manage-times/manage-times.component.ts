import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';

@Component({
  selector: 'app-manage-times',
  templateUrl: './manage-times.component.html',
  styleUrls: ['./manage-times.component.scss']
})
export class ManageTimesComponent implements OnInit, OnDestroy {

  newAvailableTimeView: any;
  newResourceUpdateView: any;
  items = [];
  itemCount: any;
  rowTooltip: any;

  constructor(
    private modalService: UsbModal,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.AdministerTimesC',
      url: '/adminstrator/manage-times'
    });
  }


  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.newAvailableTimeView) {
      this.newAvailableTimeView.close();
    }
  }

  openAvailableTime(content: any) {
    this.newAvailableTimeView = this.modalService.open(content, { width: '800' });
  }

  rowDoubleClick(content: any) {
    this.newAvailableTimeView = this.modalService.open(content, { width: '520' });
  }

}
