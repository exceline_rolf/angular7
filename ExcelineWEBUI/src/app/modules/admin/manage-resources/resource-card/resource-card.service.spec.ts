import { TestBed, inject } from '@angular/core/testing';

import { ResourceCardService } from './resource-card.service';

describe('ResourceCardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResourceCardService]
    });
  });

  it('should be created', inject([ResourceCardService], (service: ResourceCardService) => {
    expect(service).toBeTruthy();
  }));
});
