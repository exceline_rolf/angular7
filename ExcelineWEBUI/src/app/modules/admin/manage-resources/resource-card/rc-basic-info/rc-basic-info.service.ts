
import { Subject } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable()
export class RcBasicInfoService {

  private _resource: any;
  private _isEditMode = false;

  public updateTitleEvent = new Subject<any>();
  public editTitle: any;

  @Output() changeEvent: EventEmitter<any> = new EventEmitter();

  // public resourceSource = new BehaviorSubject<any>(this._resource);
  // currentResource = this.resourceSource.asObservable();
  public resourceSource = new Subject<any>();
  public currentResource: any

  constructor() { }

  setSelectedResource(selectedResource: any) {
    this._resource = selectedResource;
    this.resourceSource.next(this._resource);
    this.resourceSource.next(selectedResource);
  }

  getSelectedResource(): any {
    if (this._resource !== null) {
      return this._resource;
    } else {
      return null;
    }
  }

  updateEditMode(mode: boolean, editMode: string) {
    switch (editMode) {
      case 'CARD': {
        this._isEditMode = mode;
        this.changeEvent.emit(this._isEditMode);
        break;
      }
    }
  }

  getEditMode() {
    return this.changeEvent;
  }

  setEditTitle(eTitle) {
    this.updateTitleEvent.next(eTitle);
    this.editTitle = eTitle;
  }

  getEditTitle() {
    return this.editTitle;
  }

  setTitle(eTitle) {
    if (this._resource) {
      this._resource.Name = eTitle
      this.resourceSource.next(this._resource);
    }
  }

  getTitle() {
    return this.editTitle;
  }

}
