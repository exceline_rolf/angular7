import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RcBasicInfoComponent } from './rc-basic-info.component';

describe('RcBasicInfoComponent', () => {
  let component: RcBasicInfoComponent;
  let fixture: ComponentFixture<RcBasicInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RcBasicInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RcBasicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
