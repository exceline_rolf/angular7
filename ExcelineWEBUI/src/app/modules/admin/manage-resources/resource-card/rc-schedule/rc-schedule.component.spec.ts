import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RcScheduleComponent } from './rc-schedule.component';

describe('RcScheduleComponent', () => {
  let component: RcScheduleComponent;
  let fixture: ComponentFixture<RcScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RcScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RcScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
