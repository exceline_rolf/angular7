import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { UsBreadcrumbService } from '../../../../shared/components/us-breadcrumb/us-breadcrumb.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AdminService } from 'app/modules/admin/services/admin.service';
import { RcBasicInfoService } from './rc-basic-info/rc-basic-info.service';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-resource-card',
  templateUrl: './resource-card.component.html',
  styleUrls: ['./resource-card.component.scss']
})
export class ResourceCardComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private _isMenuOpen = false;
  isEdit: any;
  resourceId: number;
  title: string;
  selectedMember?: any;
  _editMode = false;

  isleftPanelCollapsed: boolean;

  constructor(
    private breadcrumbService: UsBreadcrumbService,
    private route: ActivatedRoute,
    private elRef: ElementRef,
    private router: Router,
    private adminService: AdminService,
    private translateService: TranslateService,
    private resourceBasicInfoService: RcBasicInfoService
  ) {
    this.resourceId = this.route.snapshot.params['Id'];
    breadcrumbService.hideRoute('/adminstrator/resource-card/' + this.route.snapshot.params['Id']);
  }

  ngOnInit() {
    this.resourceBasicInfoService.updateTitleEvent.pipe(takeUntil(this.destroy$)).subscribe(result => {
      this.isEdit = result;
    });
    this.adminService.GetResourceDetailsById(this.resourceId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      result => {
        if (result) {
          this.resourceBasicInfoService.setSelectedResource(result.Data);
          this.breadcrumbService.addFriendlyNameForRoute('/adminstrator/resource-card/' + this.route.snapshot.params['Id'], this.selectedMember.Name);
        }
      });
    this.resourceBasicInfoService.resourceSource.pipe(takeUntil(this.destroy$)).subscribe(
      member => this.selectedMember = member
    );
    this.resourceBasicInfoService.getEditMode().pipe(takeUntil(this.destroy$)).subscribe(editMode => this._editMode = editMode);
    this.translateService.get('ADMIN.Schedule').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
    this.setTitle();
    this.setCardState('rc-schedule');
  }

  setTitle() {
    const currentComponent = this.router.url.split('/')[this.router.url.split('/').length - 1]
    switch (currentComponent) {
      case 'rc-schedule': {
        this.translateService.get('ADMIN.Schedule').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      default: {
      }
        this.closeNav();
    }
  }

  openNav() {
    if (!this._isMenuOpen) {
      this._isMenuOpen = true;
      document.getElementById('mc-menu-slide').style.width = '200px';
      this.elRef.nativeElement.querySelector('.off-canvas-panel-ani').style.marginRight = '200px';
    } else {
      this.closeNav();
    }
  }

  closeNav() {
    // this.resourceBasicInfoService.setEditTitle('');
    this._isMenuOpen = false;
    if (document.getElementById('mc-menu-slide')) {
      document.getElementById('mc-menu-slide').style.width = '0';
    }
    if (this.elRef.nativeElement.querySelector('.off-canvas-panel-ani')) {
      this.elRef.nativeElement.querySelector('.off-canvas-panel-ani').style.marginRight = '0';
    }
  }

  setCardState(status: string) {
    const parentRoute = '/adminstrator/resource-card/' + this.route.snapshot.params['Id']
    this._editMode = false;
    switch (status) {
      case 'rc-schedule': {
        this.router.navigate([parentRoute + '/rc-schedule']);
        this.translateService.get('ADMIN.Schedule').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      default: {
      }
    }
  }

  updateResource() {
    this.resourceBasicInfoService.updateEditMode(true, 'CARD');
    this._editMode = true;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
