import { UsbModalRef } from '../../../../shared/components/us-modal/modal-ref';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { Component, ViewChild, TemplateRef, Output, EventEmitter, OnInit, Input, ElementRef } from '@angular/core';
import * as moment from 'moment';
import * as _ from 'lodash';
import * as $ from 'jquery';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AdminService } from '../../services/admin.service';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import { DayOfWeek, EntitySelectionType, ColumnDataType } from '../../../../shared/enums/us-enum';
import { forEach } from '@angular/router/src/utils/collection';
import { Dictionary } from 'lodash';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { ExceShopService } from '../../../shop/services/exce-shop.service';
import { ExceGymSetting } from '../../../../shared/SystemObjects/Common/ExceGymSetting';
import { ShopHomeService } from '../../../shop/shop-home/shop-home.service';
import { Router} from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-resource-booking',
  templateUrl: './resource-booking.component.html',
  styleUrls: ['./resource-booking.component.scss']
})
export class ResourceBookingComponent implements OnInit, OnDestroy {
  private self;
  private bookingType: string; // Used to set appropriate title to modal header
  private selectedResource: any; // Used to show the resources name in the modal header
  private destroy$ = new Subject<void>();
  isDisableNext = false;
  pageNumber = 1;
  defaultDate: string;
  selectedStartTime: any;
  deleteEventId: any;
  translatedMsgs: any;
  updateClassActiveTimeHelper: any = {};
  newEndTime: Date;
  resourceIdList: any[] = [];
  instructorIdList: any[] = [];
  selectedInstructors: any;
  newStartTime: Date;
  schedule: any;
  yeshandlerSubs: Subscription;
  nohandlerSubs: Subscription;
  model: any;
  classBookings: any;
  selectedActiveTime: any;
  isNewBooking: any;
  resourceLabelText: string;
  resourceAreaWidth: string;
  selectedResourceId: any;

  entitySelectionViewTitle: string;
  isBranchVisible = true;
  isStatusVisible = true;
  isRoleVisible = true;
  isAddBtnVisible = true;
  isBasicInforVisible = true;
  defaultRole = 'ALL'
  multipleRowSelect = false;
  memberRegisterView: any;
  items = [];
  itemCount: number;
  isDbPagination = true;
  entitySelectionType: string;
  entitySearchType: string;
  columns: any;
  entitySelectionModal: any;
  auotocompleteItems = [{ id: 'NAME', name: 'NAMEE :', isNumber: false }, { id: 'ID', name: 'Idd :', isNumber: true }]

  addBookingModel: UsbModalRef;
  addUnavailanleModalModel: UsbModalRef;
  removeBookingModal: UsbModalRef;
  categoryModel: any;
  bookingCancelModel: any;
  memberSelectedModel: any;
  smsFreeTextModel: any;
  switchResourceModel: any;
  addScheduleModel: any;
  dayOfWeek: number;
  fromDate: { date: { year: number; month: number; day: number; }; };
  toDate: { date: { year: number; month: number; day: number; }; };
  startTime: string;
  tooltipY: number;
  tooltipX: number;
  isShowTooltip: boolean;
  eventContextmenuY: any;
  eventontextmenuX: any;
  contextmenuY: any;
  contextmenuX: any;
  scheduler: any;
  calandarDateRange: any;
  isShowTimeDuration: boolean;
  calandarDate: any;
  classActiveTimes: any;
  isShowDayContextMenu: boolean;
  isShowEventContextMenu: boolean;
  isShowBookingContextMenu: boolean;
  isShowUnAvailableContextMenu: boolean;

  public dateFilterForm: FormGroup;
  today: { year: number; month: number; day: number; };
  tommorow: any;

  buttonText: { prev: string; next: string; };
  buttonIcons: { prev: string; next: string; };
  theame: boolean;
  events: any[] = [];
  resources: any[] = [];
  notArrivedMembers: any[] = [];
  selectedMembers: any = [];
  isLoaded = false;
  selectedMember: any;

  @ViewChild('p') public popover: NgbPopover;
  public unAvailableForm: FormGroup;
  public switchResourceForm: FormGroup;
  public scheduleForm: FormGroup;

  header: any;
  event: any;
  dialogVisible = false;
  idGen = 100;
  viewType: string;
  Currentevent: string;
  views: any;
  scrollTime: string;

  branchId: number;
  isBookingBackDate: boolean;
  categoryList: any;
  bookingCategoryList: any;
  currentViewMode: string;
  resourceList: any = [];
  filterResourceList: any = [];
  filterResIdList: any = [];
  resScheduleItemList: any = [];
  viewStartDate: any = new Date();
  viewEndDate: any = new Date();
  bookingActiveTimeLst: any = [];
  activityUnAvelabelList: any = [];
  activeTimeDetail: any = {};
  activeTimes: any = [];
  selectedResScheduleItem: any = {};
  selectedUnAvelableTime: any = {};
  memberDictionary: Dictionary<string>;
  isManySelected: boolean;
  isVisibleDayOnlyView = true;
  selectedUnAvelabelList: any = [];
  selectedActivitityUnAvelableList: any = [];
  selectedBookingList: any = [];

  smsFreeText: string;
  cancelBookingComment: string;
  shopLoginData?: any = {};
  exceGymSetting: ExceGymSetting = new ExceGymSetting();
  shopModalReference: UsbModalRef;

  private selectedGymsettings = [
    'EcoNegativeOnAccount',
    'EcoSMSInvoiceShop',
    'EcoIsPrintInvoiceInShop',
    'EcoIsPrintReceiptInShop',
    'EcoIsShopOnNextOrder',
    'EcoIsOnAccount',
    'EcoIsDefaultCustomerAvailable',
    'EcoIsPayButtonsAvailable',
    'EcoIsPettyCashSameAsUserAllowed',
    'OtherIsShopAvailable',
    'OtherLoginShop',
    'HwIsShopLoginWithCard',
    'ReqMemberListRequired',
    'OtherIsLogoutFromShopAfterSale',
    'EcoIsBankTerminalIntegrated',
    'EcoIsDailySettlementForAll',
    'OtherIsValidateShopGymId'
  ];
  list: any;
  showDeletedMessage = false;
  isDisablePrev: boolean;
  changeResourceModal: any;
  allResources: any;
  resgridApi: any;
  resourceDefs: any;
  resourceSelected = false;
  resoureComment = false;

  @ViewChild('BookingCancelModel') public refTobookingCancelModel;
  private targetBooking: any;

  constructor(
    private modalService: UsbModal,
    private exceMessageService: ExceMessageService,
    private fb: FormBuilder,
    private translate: TranslateService,
    private adminService: AdminService,
    private loginservice: ExceLoginService,
    private exceBreadcrumbService: ExceBreadcrumbService,
    private shopService: ExceShopService,
    private shopHomeService: ShopHomeService,
    private router: Router
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.ResourceBooking',
      url: '/adminstrator/resource-booking'
    });
  }

  ngOnInit() {
    this.unAvailableForm = this.fb.group({
      StartTime: [],
      EndTime: []
    });
    this.switchResourceForm = this.fb.group({
      StartDate: [],
      EndDate: [],
      ResourceName: [],
      ResourceId: [],
      Comment: []
    });
    this.scheduleForm = this.fb.group({
      StartDate: [],
      EndDate: [],
      ResourceId: [],
      ResourcesName: [],
      EmpId: [],
      EmpName: [],
      StartTime: [],
      EndTime: [],
      Comment: []
    });
    this.branchId = this.loginservice.SelectedBranch.BranchId;
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.isShowTooltip = false;
    this.isShowTimeDuration = false;
    this.isShowUnAvailableContextMenu = false;
    this.showDeletedMessage = false;
    this.calandarDate = new Date();
    const now = new Date();
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.dateFilterForm = this.fb.group({
      fromDate: [{ date: this.today }, [Validators.required]],
      toDate: [{ date: this.today }, [Validators.required]]
    });

    this.theame = false;
    this.viewType = 'agendaDay';

    this.header = {
      right: '',
      center: '',
      left: ''
    };

    this.resourceAreaWidth = '20%';
    this.resourceLabelText = 'Classes';

    this.views = {
      agendaDayWithoutResources: {
        type: 'agenda',
        duration: { days: 1 },
        groupByResource: true,
        groupByDateAndResource: true
      }
    };

    const selectedGymsettings = ['BookingBackDate'];

    this.adminService.getCategories('RESOURCE').pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.categoryList = res.Data;
      }, null, null);

    this.adminService.GetResourceCalender(this.branchId, 1).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        if (res.Data) {
          this.resourceList = res.Data;
          this.filterResourceList = this.resourceList;
          this.resourceList.forEach(resource => {
            this.filterResIdList.push(resource.Id);
          });
          this.initializeCalendar();
        }
      }, null, null);

    this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      switch (value.id) {
        case 'SEND_SMSFREETEXT': {
          this.sendSmsFreeTextConfirm(value.optionalData);
          break;
        }
        case 'SEND_SMSREMINDER': {
          this.sendSmsReminderConfirm(value.optionalData);
          break;
        }
        case 'PAID_BOOKING_CANCEL': {
          this.paidBookingCancelConfirm(value.optionalData);
          break;
        }
        case 'PUNCHCARD_BOOKING_CANCEL': {
          this.punchCardBookingCancelConfirm(value.optionalData);
          break;
        }
        default: {
          // Used in dev
          alert('Unrecognized message sender')
        }
      }



    });

    this.resourceDefs = [
      {headerName: 'Navn', field: 'Name', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
    ]

  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.entitySelectionModal) {
      this.entitySelectionModal.close();
    }
    if (this.switchResourceModel) {
      this.switchResourceModel.close();
    }
    if (this.smsFreeTextModel) {
      this.smsFreeTextModel.close();
    }
    if (this.memberSelectedModel) {
      this.memberSelectedModel.close();
    }
    if (this.categoryModel) {
      this.categoryModel.close();
    }
    if (this.bookingCancelModel) {
      this.bookingCancelModel.close();
    }
    if (this.addScheduleModel) {
      this.addScheduleModel.close();
    }
    if (this.shopModalReference) {
      this.shopModalReference.close();
    }
    if (this.addUnavailanleModalModel) {
      this.addUnavailanleModalModel.close();
    }
    if (this.addBookingModel) {
      this.addBookingModel.close();
    }
    if (this.changeResourceModal) {
      this.changeResourceModal.close();
    }
  }

  initializeCalendar() {
    const scheduleDetails = {
      'ResIdList': this.filterResIdList,
      'RoleTpye': 'RES'
    }
    const entityActiveTime = {
      'Branchid': this.branchId,
      'StartDate': this.viewStartDate,
      'EndDate': this.viewEndDate,
      'EntityList': this.filterResIdList,
      'EntityRoleType': 'RES'
    }
    this.adminService.GetResourceCalenderScheduleItems(scheduleDetails).pipe(takeUntil(this.destroy$)).subscribe(res => {
        if (res.Data) {
          this.resScheduleItemList = res.Data;
          this.resScheduleItemList.forEach(schedule => {
            this.filterResourceList.forEach(resource => {
              if (resource.Id === schedule.EntityId) {
                resource.Schedule = schedule;
              }
            });
          });
          if (this.isLoaded) {
            this.handelActiveTime(this.filterResourceList, this.currentViewMode, this.viewStartDate, this.viewEndDate);
          }
          this.isLoaded = true;

          let i = 0;
          this.filterResourceList.forEach(resource => {
            this.adminService.GetBookingsOnDeletedScheduleItem(resource.Id).pipe(takeUntil(this.destroy$)).subscribe(
              res2 => {
                if (res2) {
                  let schedulesRetrieved = [];
                  schedulesRetrieved = res2.Data;
                  if (schedulesRetrieved.length > 0) {
                    schedulesRetrieved.forEach(element => {
                      if (element.ParentId === -2) {
                        resource.Schedule.SheduleItemList.push(element)
                      }
                      if (this.filterResourceList.length === (i + 1) && schedulesRetrieved.length === schedulesRetrieved.indexOf(element) + 1) {
                        this.adminService.GetEntityActiveTimes(entityActiveTime).pipe(takeUntil(this.destroy$)).subscribe(
                          result => {
                            if (result) {
                              this.bookingActiveTimeLst = result.Data;
                              this.handelActiveTime(this.filterResourceList, this.currentViewMode, this.viewStartDate, this.viewEndDate);
                              this.isLoaded = true;
                            }
                          }, err => {
                            console.log(err);
                          });
                      }
                    });
                  } else {
                    if (this.filterResourceList.length === (i + 1)) {
                      this.adminService.GetEntityActiveTimes(entityActiveTime).pipe(takeUntil(this.destroy$)).subscribe(
                        result => {
                          if (result) {
                            this.bookingActiveTimeLst = result.Data;
                            this.handelActiveTime(this.filterResourceList, this.currentViewMode, this.viewStartDate, this.viewEndDate);
                            this.isLoaded = true;
                          }
                        }, err => {
                          console.log(err);
                        });
                    }
                  }
                }
                i++
              });
          });
            }
          }, err => {
            console.log(err);
        });
  }

  onCategoryChange(value) {
    this.filterResIdList = [];
    this.filterResourceList = [];
    this.isVisibleDayOnlyView = false;
    if (value === 'ALL') {
      this.filterResourceList = this.resourceList;
    } else {
      this.filterResourceList = this.resourceList.filter(x => x.ResourceCategory.Code == value);
    }
    this.filterResourceList.forEach(resource => {
      this.filterResIdList.push(resource.Id);
    });
    this.refreshCalendar();
  }

  onResourceChange(value) {
    this.filterResourceList = [];
    this.filterResIdList = [];
    if (value == -1) {
      this.filterResourceList = this.resourceList;
      this.isVisibleDayOnlyView = true;
    } else {
      this.filterResourceList.push(this.resourceList.find(x => x.Id == value));
      this.isVisibleDayOnlyView = false;
    }
    this.filterResourceList.forEach(resource => {
      this.filterResIdList.push(resource.Id);
    });
    this.refreshCalendar();
  }

  onDateChanged(event, scheduler) {
    scheduler.changeView('agendaDayWithoutResources');
    scheduler.gotoDate(event.jsdate);
    this.generateTitle(scheduler);
  }

  saveBooking(val) {
    this.selectedActiveTime = {};
    this.activeTimeDetail = {};
    this.refreshCalendar();
    this.addBookingModel.close();
  }

  refreshCalendar() {
    this.viewStartDate = moment(this.viewStartDate).format('YYYY-MM-DD HH:mm:ss');
    this.viewEndDate = moment(this.viewEndDate).format('YYYY-MM-DD HH:mm:ss');
    const entityActiveTime = {
      'Branchid': this.branchId,
      'StartDate': this.viewStartDate,
      'EndDate': this.viewEndDate,
      'EntityList': this.filterResIdList,
      'EntityRoleType': 'RES'
    }
    this.adminService.GetEntityActiveTimes(entityActiveTime).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        if (res.Data) {
          this.bookingActiveTimeLst = res.Data;
          this.handelActiveTime(this.filterResourceList, this.currentViewMode, this.viewStartDate, this.viewEndDate);
        }
      }, err => {
        console.log(err);
    });
  }

  // Create resource events for calandar
  createEvents(itemList: any, pageNumber: number) {
    const resList: any[] = [];
    const eventList: any[] = [];
    this.filterResourceList.forEach((resource, index) => {
      // addevents make sure scheduleitems that are deleted and with no bookings dont show
      let addEvents = true;
      if ((index < 10 * pageNumber) && (index >= (10 * pageNumber - 10))) {
        const resScheduleList = itemList.filter(x => x.EntityId === resource.Id);
        const entityActiveTimes = this.bookingActiveTimeLst.filter(x => x.EntityId === resource.Id && !x.IsUnAvailableActiveTime);
        const unAvailableActiveTime = this.bookingActiveTimeLst.filter(x => x.EntityId === resource.Id && x.IsUnAvailableActiveTime);
        const activityunAvailableList = this.activityUnAvelabelList.filter(x => x.EntityId === resource.Id);
        resList.push({
          id: resource.Id.toString(),
          title: resource.Name,
          eventColor: '#c2c2c2'
        });
        resScheduleList.forEach(activeTime => {
          if (activeTime.ActiveStatus === false) {
            if (entityActiveTimes.length > 0) {
            } else if (entityActiveTimes.length === 0) {
              addEvents = false;
            }
            if (addEvents) {
              eventList.push({
                resourceId: resource.Id.toString(),
                id: activeTime.Id.toString(),
                start: activeTime.StartDateTime,
                end: activeTime.EndDateTime,
                rendering: 'background',
                backgroundColor: '#a9a9a9', //activeTime.IsActivitySchedule ? '#cfcfcf' : activeTime.Color,
                resSchedule: activeTime,
                title: activeTime.IsActivitySchedule ? activeTime.Name : resource.Name,
                ActiveStatus: activeTime.ActiveStatus
              });
            }
          } else {
            eventList.push({
              resourceId: resource.Id.toString(),
              // id: activeTime.Id.toString(),
              start: activeTime.StartDateTime,
              end: activeTime.EndDateTime,
              rendering: 'background',
              backgroundColor: '#a9a9a9',//activeTime.IsActivitySchedule ? '#FFD700' : activeTime.Color,
              resSchedule: activeTime,
              title: activeTime.IsActivitySchedule ? activeTime.Name : resource.Name,
              ActiveStatus: activeTime.ActiveStatus,
              id: activeTime.Id.toString()
            });
          }
        });
        entityActiveTimes.forEach(eActiveTime => {
          // const resSchedule not used
          /* const resSchedule = resScheduleList.find(x => moment(x.StartDateTime) <= moment(eActiveTime.StartDateTime) && moment(eActiveTime.StartDateTime) <= moment(x.EndDateTime)
            && moment(x.StartDateTime) <= moment(eActiveTime.EndDateTime) && moment(eActiveTime.EndDateTime) <= moment(x.EndDateTime)); */
            if (eActiveTime.BookingMemberList.length > 0) {
              eventList.push({
                resourceId: resource.Id.toString(),
                title: eActiveTime.BookingMemberList[0].Name,
                bookerBranch: eActiveTime.BookingMemberList[0].BranchId,
                bookerRole: eActiveTime.BookingMemberList[0].MemberRole,
                bookerId: eActiveTime.BookingMemberList[0].Id,
                start: eActiveTime.StartDateTime,
                end: eActiveTime.EndDateTime,
                constraint: eActiveTime.Id.toString(),
                color: eActiveTime.BookingCategoryColor,
                type: 'BOOKING',
                bookingId: eActiveTime.Id,
                activeTimeDetail: eActiveTime,
                ActiveStatus: true
              });
            }
        });

          activityunAvailableList.forEach(unAvelable => {
            const st = unAvelable.StartDateTime;
            const en = unAvelable.EndDateTime;
            eventList.push({
              resourceId: resource.Id.toString(),
              title: unAvelable.Name,
              start: st,
              end: en,
              rendering: 'background',
              className: 'fc-bgevent-disabled',
              ActiveStatus: true
              // , constraint:  element.ScheduleId.toString(), color:  '#FFD700', type: 'UNAVAILABLE',  editable: false
            });
          });

          unAvailableActiveTime.forEach(unAvelable => {
            const st = unAvelable.StartDateTime;
            const en = unAvelable.EndDateTime;
            let title = '';
            this.translate.get('ADMIN.UnavailableTime').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => title = translatedValue);
            eventList.push({
              resourceId: resource.Id.toString(),
              title:  title,
              start: st,
              end: en,
              rendering: 'background',
              className: 'unavailable fc-bgevent-disabled',
              ActiveStatus: true
              // , constraint:  element.ScheduleId.toString(), color:  '#FFD700', type: 'UNAVAILABLE',  editable: false
            });
          });
      }
    });
    this.resources = resList;
    this.events = eventList;
  }

  availableEvent() {
    const startDateTime = moment(this.selectedUnAvelableTime.StartTime).format('YYYY-MM-DD HH:mm:ss');
    const endDateTime = moment(this.selectedUnAvelableTime.EndTime).format('YYYY-MM-DD HH:mm:ss');
    const available = {
      ScheduleItemId: this.selectedUnAvelableTime.SheduleItemId,
      ActiveTimeId: this.selectedUnAvelableTime.Id,
      StartDate: startDateTime,
      EndDate: endDateTime
    }
    this.adminService.UnavailableResourceBooking(available).pipe(takeUntil(this.destroy$)).subscribe(obj => {
      if (obj) {
        this.isShowUnAvailableContextMenu = false;
        this.initializeCalendar();
      }
    });
  }

  handelActiveTime(resitems, viewMode, startDate, endDate) {
    if (resitems) {
      // let activeTimes: any = [];
      this.activeTimes = [];
      let tempId = 0;
      this.activityUnAvelabelList = [];
      resitems.forEach(resourceItem => {
              let maximumDayPerBooking = endDate;
              if (resourceItem.ActivityId > 0 && resourceItem.DaysForBookingPerActivity > 0) {
                // let currentDate: any = new Date();
                maximumDayPerBooking = moment(new Date(), 'DD-MM-YYYY').add(1, 'days'); // currentDate.AddDays(resourceItem.DaysForBookingPerActivity);
              }
              let itemLst = resourceItem.Schedule.SheduleItemList;
              if (itemLst) {
                itemLst.forEach(sheduleItemDc => {
                  let tempStartDate = startDate;
                  let tempEndDate = endDate;
                  if (tempStartDate.Date <= sheduleItemDc.StartDate.Date) {
                    tempStartDate = sheduleItemDc.StartDate.Date;
                  }
                  if (endDate > sheduleItemDc.EndDate.Date) {
                    tempEndDate = sheduleItemDc.EndDate.Date;
                  }
                  let switchPeriods: any = [];
                  for (const key in sheduleItemDc.SwitchDataRange) {
                    if (sheduleItemDc.SwitchDataRange.hasOwnProperty(key)) {
                      switchPeriods = sheduleItemDc.SwitchDataRange[key];
                    }
                  }
                  const activeTimeLst = this.getWeekActiveTimes(sheduleItemDc, tempStartDate, tempEndDate);
                  activeTimeLst.forEach(entityActiveTime => {
                    const activeTime: any = {};
                    tempId--;
                    activeTime.Id = tempId;
                    activeTime.ActiveStatus = entityActiveTime.ActiveStatus;
                    activeTime.EntityId = resourceItem.Id;
                    activeTime.EntityName = resourceItem.Name;
                    activeTime.SheduleItemId = sheduleItemDc.Id;
                    activeTime.EntityRoleType = 'RES';
                    activeTime.AdditionalResId = sheduleItemDc.ResourceId;
                    activeTime.ResName = sheduleItemDc.ResourcesName;
                    activeTime.IsScheduleActiveTime = true;
                    activeTime.StartDateTime = moment(entityActiveTime.StartDateTime).format('YYYY-MM-DD HH:mm:ss'); // entityActiveTime.StartDateTime;
                    activeTime.EndDateTime = moment(entityActiveTime.EndDateTime).format('YYYY-MM-DD HH:mm:ss'); // entityActiveTime.EndDateTime;
                    activeTime.ActivityName = resourceItem.ActivityName;
                    activeTime.ActivityId = resourceItem.ActivityId;
                    activeTime.IsActivitySchedule = false;
                    if (resourceItem.ResourceCategory != null) {
                      activeTime.CategoryName = resourceItem.ResourceCategory.Name;
                      activeTime.Color = resourceItem.ResourceCategory.Color;
                    }
                    if (resourceItem.ActivityTimeLst) {
                      this.handelUnActiveTime(activeTime, resourceItem.ActivityTimeLst, resourceItem.Id);
                    }

                    if (switchPeriods.filter(x => x[0] <= moment(activeTime.StartDateTime).format('YYYY-MM-DD') &&
                      moment(activeTime.StartDateTime).format('YYYY-MM-DD') <= x[1])) {
                      activeTime.IsActivitySchedule = true;
                      activeTime.IsSwitchSchedule = true;
                      activeTime.Name = 'Switch Resource';
                    }

                    if (moment(activeTime.StartDateTime).format('YYYY-MM-DD') > moment(maximumDayPerBooking).format('YYYY-MM-DD')) {
                      activeTime.IsActivitySchedule = true;
                      activeTime.Name = 'Booking Not allowed';
                    }
                    this.activeTimes.push(activeTime);
                  });
                });
              }
              if (resitems.indexOf(resourceItem) === resitems.length - 1) {
                this.createEvents(this.activeTimes, 1);
              }
            }
      )
      // check for (parent)scheuldeItems with activeStatus 0 and check if there any future bookings on them
      // if true => add scheduleItems to rescource.Schedule.ScheduleItemList
    }
  }

  handelUnActiveTime(scheduleActive, unActivTimeList, resId) {
    unActivTimeList.forEach(activityTimeDc => {
      if (activityTimeDc.EndDate == null) {
        activityTimeDc.EndDate = moment('9999-01-01');
      }
      if (activityTimeDc.EndTime == null) {
        activityTimeDc.EndTime = moment(moment(new Date(), 'DD-MM-YYYY') + ' ' + '23:30');
      }
    });
    // let activityUnAvelabelList = [];
    let itemLst = unActivTimeList.filter(x =>
      (x.Startdate < scheduleActive.StartDateTime.Date && x.EndDate < scheduleActive.StartDateTime.Date) ||
      (x.Startdate > scheduleActive.EndDateTime.Date && x.EndDate > scheduleActive.EndDateTime.Date));

    unActivTimeList.forEach(item => {
      let activetyUnavailable: any = {};
      if (moment(item.StartTime).format('HH:mm') <= moment(scheduleActive.StartDateTime).format('HH:mm')
        && moment(scheduleActive.EndDateTime).format('HH:mm') <= moment(item.EndTime).format('HH:mm')) {
        activetyUnavailable.ScheduleId = scheduleActive.Id;
        activetyUnavailable.StartDateTime = scheduleActive.StartDateTime;
        activetyUnavailable.EndDateTime = scheduleActive.EndDateTime;
        activetyUnavailable.EntityId = resId;
        activetyUnavailable.IsActivitySchedule = true;
        activetyUnavailable.EntityRoleType = 'RES';
        activetyUnavailable.Name = item.Reason; // "deviating hours of the activity";
        if (activetyUnavailable.EndDateTime > activetyUnavailable.StartDateTime) {
          this.activityUnAvelabelList.push(activetyUnavailable);
        }
      } else if (moment(item.StartTime).format('HH:mm') <= moment(scheduleActive.StartDateTime).format('HH:mm')
        && moment(scheduleActive.EndDateTime).format('HH:mm') > moment(item.EndTime).format('HH:mm')) {
        activetyUnavailable.ScheduleId = scheduleActive.Id;
        activetyUnavailable.StartDateTime = scheduleActive.StartDateTime;
        activetyUnavailable.EndDateTime = moment(moment(scheduleActive.EndDateTime).format('YYYY-MM-DD') + ' ' + moment(item.EndTime).format('HH:mm')).format('YYYY-MM-DD HH:mm:ss');
        // Convert.ToDateTime(scheduleActive.EndDateTime.ToShortDateString() + " " + Convert.ToDateTime(item.EndTime).TimeOfDay.ToString());
        activetyUnavailable.EntityId = resId;
        activetyUnavailable.IsActivitySchedule = true;
        activetyUnavailable.EntityRoleType = 'RES';
        activetyUnavailable.Name = item.Reason; // "deviating hours of the activity";
        if (activetyUnavailable.EndDateTime > activetyUnavailable.StartDateTime) {
          this.activityUnAvelabelList.push(activetyUnavailable);
        }
      } else if (moment(item.StartTime).format('HH:mm') > moment(scheduleActive.StartDateTime).format('HH:mm')
        && moment(scheduleActive.EndDateTime).format('HH:mm') <= moment(item.EndTime).format('HH:mm')) {
        activetyUnavailable.ScheduleId = scheduleActive.Id;
        activetyUnavailable.StartDateTime = moment(moment(scheduleActive.StartDateTime).format('YYYY-MM-DD') + ' ' + moment(item.StartTime).format('HH:mm')).format('YYYY-MM-DD HH:mm:ss');
        // Convert.ToDateTime(scheduleActive.StartDateTime.ToShortDateString() + " " + Convert.ToDateTime(item.StartTime).TimeOfDay.ToString());
        activetyUnavailable.EndDateTime = scheduleActive.EndDateTime;
        activetyUnavailable.EntityId = resId;
        activetyUnavailable.IsActivitySchedule = true;
        activetyUnavailable.EntityRoleType = 'RES';
        activetyUnavailable.Name = item.Reason; // "deviating hours of the activity";
        if (activetyUnavailable.EndDateTime > activetyUnavailable.StartDateTime) {
          this.activityUnAvelabelList.push(activetyUnavailable);
        }
      } else if ((moment(item.StartTime).format('HH:mm') > moment(scheduleActive.StartDateTime).format('HH:mm'))
        && (moment(scheduleActive.EndDateTime).format('HH:mm') > moment(item.EndTime).format('HH:mm'))) {
        // let stDate = moment(scheduleActive.StartDateTime).format('DD-MM-YYYY');
        // let enTime = moment(item.StartTime).format('HH:mm');
        // let datetimeA = moment(stDate + ' ' + enTime);
        activetyUnavailable.ScheduleId = scheduleActive.Id;
        activetyUnavailable.StartDateTime = moment(moment(scheduleActive.StartDateTime).format('YYYY-MM-DD') + ' ' + moment(item.StartTime).format('HH:mm')).format('YYYY-MM-DD HH:mm:ss');
        activetyUnavailable.EndDateTime = moment(moment(scheduleActive.EndDateTime).format('YYYY-MM-DD') + ' ' + moment(item.EndTime).format('HH:mm')).format('YYYY-MM-DD HH:mm:ss');
        activetyUnavailable.EntityId = resId;
        activetyUnavailable.IsActivitySchedule = true;
        activetyUnavailable.EntityRoleType = 'RES';
        activetyUnavailable.Name = item.Reason; // "deviating hours of the activity";
        if (activetyUnavailable.EndDateTime > activetyUnavailable.StartDateTime) {
          this.activityUnAvelabelList.push(activetyUnavailable);
        }
      }
    });
    // return activityUnAvelabelList;
  }

  getWeekActiveTimes(scheduleItem, startDate, endDate) {
    switch (scheduleItem.Day) {
      case 'Sunday':
        return this.generateActiveTimes(scheduleItem, DayOfWeek.Sunday, startDate, endDate);

      case 'Monday':
        return this.generateActiveTimes(scheduleItem, DayOfWeek.Monday, startDate, endDate);

      case 'Tuesday':
        return this.generateActiveTimes(scheduleItem, DayOfWeek.Tuesday, startDate, endDate);

      case 'Wednesday':
        return this.generateActiveTimes(scheduleItem, DayOfWeek.Wednesday, startDate, endDate);

      case 'Thursday':
        return this.generateActiveTimes(scheduleItem, DayOfWeek.Thursday, startDate, endDate);

      case 'Friday':
        return this.generateActiveTimes(scheduleItem, DayOfWeek.Friday, startDate, endDate);

      case 'Saturday':
        return this.generateActiveTimes(scheduleItem, DayOfWeek.Saturday, startDate, endDate);

      case 'søndag':
        return this.generateActiveTimes(scheduleItem, DayOfWeek.Sunday, startDate, endDate);

      case 'mandag':
        return this.generateActiveTimes(scheduleItem, DayOfWeek.Monday, startDate, endDate);

      case 'tirsdag':
        return this.generateActiveTimes(scheduleItem, DayOfWeek.Tuesday, startDate, endDate);

      case 'onsdag':
        return this.generateActiveTimes(scheduleItem, DayOfWeek.Wednesday, startDate, endDate);

      case 'torsdag':
        return this.generateActiveTimes(scheduleItem, DayOfWeek.Thursday, startDate, endDate);

      case 'fredag':
        return this.generateActiveTimes(scheduleItem, DayOfWeek.Friday, startDate, endDate);

      case 'lørdag':
        return this.generateActiveTimes(scheduleItem, DayOfWeek.Saturday, startDate, endDate);
    }
  }

  generateActiveTimes(scheduleItem, dayofWeek, StartDate, EndDate) {
    const activeTimes = [];
    const stDate = new Date(StartDate);
    const endDate = new Date(EndDate);
    const timeDiff = Math.abs(endDate.getTime() - stDate.getTime());
    const days = Math.ceil(timeDiff / (1000 * 3600 * 24));

    let activestartDate = StartDate;
    if (scheduleItem.WeekType === 2) {
      let i = 0;
      for (i = 0; i < days + 1; i++) {
        const week = moment(activestartDate).weekday();
        if (week === dayofWeek) {
          const activeTime: any = {};
          activeTime.SheduleItemId = scheduleItem.Id;
          activeTime.ActiveStatus = scheduleItem.ActiveStatus;
          activeTime.StartDateTime = moment(activestartDate).format('YYYY-MM-DD') + ' ' + moment(scheduleItem.StartTime).format('HH:mm');
          activeTime.EndDateTime = moment(activestartDate).format('YYYY-MM-DD') + ' ' + moment(scheduleItem.EndTime).format('HH:mm');
          activeTimes.push(activeTime);
        }
        activestartDate = moment(activestartDate, 'DD-MM-YYYY').add(1, 'days'); // activestartDate.AddDays(1);
      }
    } else {
      // DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
      // Calendar cal = dfi.Calendar;
      let i = 0;
      for (i = 0; i < days + 1; i++) {
        const weekNumber = moment(activestartDate, 'MMDDYYYY').isoWeek(); // cal.GetWeekOfYear(activestartDate, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
        const oddEvenStatus = weekNumber % 2;
        if (oddEvenStatus === scheduleItem.WeekType) {
          const week = moment(activestartDate).weekday();
          if (week === dayofWeek) {
            const activeTime: any = {};
            activeTime.SheduleItemId = scheduleItem.Id;
            activeTime.ActiveStatus = scheduleItem.ActiveStatus;
            activeTime.StartDateTime = moment(activestartDate).format('YYYY-MM-DD') + ' ' + moment(scheduleItem.StartTime).format('HH:mm');
            activeTime.EndDateTime = moment(activestartDate).format('YYYY-MM-DD') + ' ' + moment(scheduleItem.EndTime).format('HH:mm');
            activeTimes.push(activeTime);
          }
        }
        activestartDate = moment(activestartDate, 'DD-MM-YYYY').add(1, 'days'); // activestartDate.AddDays(1);
      }
    }
    return activeTimes;
  }

  loadResourceView(scheduler) {
    scheduler.changeView('timelineDay');
  }

  selectDateRange(val, scheduler) {
    this.scheduler = scheduler;
    switch (val) {
      case 'TODAY':
        this.viewStartDate = this.today;
        this.viewEndDate = this.today;
        this.dateFilterForm.patchValue({
          fromDate: { date: this.today },
          toDate: { date: this.today }
        });
        scheduler.changeView('agendaDay');
        scheduler.today();
        this.generateTitle(scheduler);
        this.refreshCalendar();
        break;

      case 'DAYVIEW':
        this.viewStartDate = this.today;
        this.viewEndDate = this.today;
        this.dateFilterForm.patchValue({
          fromDate: { date: this.today },
          toDate: { date: this.today }
        });
        scheduler.changeView('agendaDay');
        scheduler.today();
        this.generateTitle(scheduler);
        this.refreshCalendar();
        break;
      case 'WEEKVIEW':

        const startOfWeek = moment().startOf('week').toDate();
        const endOfWeek = moment().endOf('week').toDate();
        this.viewStartDate = startOfWeek;
        this.viewEndDate = endOfWeek;

        this.dateFilterForm.patchValue({
          fromDate: { date: { year: startOfWeek.getFullYear(), month: startOfWeek.getMonth() + 1, day: startOfWeek.getDate() } },
          toDate: { date: { year: endOfWeek.getFullYear(), month: endOfWeek.getMonth() + 1, day: endOfWeek.getDate() } }
        });
        scheduler.changeView('agendaWeek', startOfWeek, endOfWeek);
        this.generateTitle(scheduler);
        this.refreshCalendar();
        break;
      case 'MONTHVIEW':
        const startOfMonth = moment().startOf('month').toDate();
        const endOfMonth = moment().endOf('month').toDate();
        this.viewStartDate = startOfMonth;
        this.viewEndDate = endOfMonth;

        this.dateFilterForm.patchValue({
          fromDate: { date: { year: startOfMonth.getFullYear(), month: startOfMonth.getMonth() + 1, day: startOfMonth.getDate() } },
          toDate: { date: { year: endOfMonth.getFullYear(), month: endOfMonth.getMonth() + 1, day: endOfMonth.getDate() } }
        });
        scheduler.changeView('month');
        this.generateTitle(scheduler);
        this.refreshCalendar();

        break;

      default:
        break;
    }
  }

  onRightClick(event) {
    let selectedSched;
    let isselected = false;
    event.element.events.forEach(ev => {
      if (event.resource === ev.resourceId && !isselected) {
        selectedSched = ev;
        isselected = true;
      }
    });
    try {
      if (selectedSched.ActiveStatus === false) {
        this.showDeletedMessage = true;
        this.selectedStartTime = event.date.format();
        this.contextmenuX = event.jsEvent.pageX;
        this.contextmenuY = event.jsEvent.pageY;
        this.isShowEventContextMenu = false;
        this.isShowDayContextMenu = false;
        this.isShowBookingContextMenu = false;
        this.isShowUnAvailableContextMenu = false;
      } else {
        // fc-widget-content
        this.selectedResource = event.element.resources.find(x => Number(x.id) === Number(event.resource)); // Get the name of the resource to put in the modal-title
        this.selectedStartTime = event.date.format();
        this.selectedResourceId = Number(event.resource);
        if (event.jsEvent.target.classList.contains('fc-bgevent')) {
          if (event.jsEvent.target.classList.contains('fc-bgevent-disabled')) {
            if (event.jsEvent.target.classList.contains('unavailable')) {
              this.contextmenuX = event.jsEvent.pageX;
              this.contextmenuY = event.jsEvent.pageY;
              this.isShowUnAvailableContextMenu = true;
              this.selectedUnAvelableTime = this.bookingActiveTimeLst.find(x => x.EntityId === Number(event.resource) && x.IsUnAvailableActiveTime
                && moment(this.selectedStartTime).isBetween(x.StartDateTime, x.EndDateTime));
            } else {
              /* Background event is available */
              this.isShowUnAvailableContextMenu = false;
            }
          } else {
            /* Background event is abled. */
            this.targetBooking = event;
            this.selectedStartTime = event.date.format();
            this.selectedResScheduleItem = this.activeTimes.find(x => x.EntityId === Number(event.resource) && moment(this.selectedStartTime).isBetween(x.StartDateTime, x.EndDateTime, null, '[)'));
            this.contextmenuX = event.jsEvent.pageX;
            this.contextmenuY = event.jsEvent.pageY;
            this.isShowBookingContextMenu = true;
            this.isShowEventContextMenu = false;
            this.isShowDayContextMenu = false;
            this.isShowUnAvailableContextMenu = false;
            this.showDeletedMessage = false;
          }
        } else {
          /* Not a background event, i.e. you clicked the white background */
          this.contextmenuX = event.jsEvent.pageX;
          this.contextmenuY = event.jsEvent.pageY;
          this.isShowDayContextMenu = true;
          this.isShowEventContextMenu = false;
          this.isShowBookingContextMenu = false;
          this.isShowUnAvailableContextMenu = false;
          this.showDeletedMessage = false;
        }
      }
    } catch (err) {
      console.log(err)
      if (err.name === 'TypeError')  {
        this.selectedStartTime = event.date.format();
        this.selectedResourceId = Number(event.resource);
        if (event.jsEvent.target.classList.contains('fc-bgevent')) {
          if (event.jsEvent.target.classList.contains('fc-bgevent-disabled')) {
            if (event.jsEvent.target.classList.contains('unavailable')) {
              this.contextmenuX = event.jsEvent.pageX;
              this.contextmenuY = event.jsEvent.pageY;
              this.isShowUnAvailableContextMenu = true;
              this.selectedUnAvelableTime = this.bookingActiveTimeLst.find(x => x.EntityId == event.resource && x.IsUnAvailableActiveTime
                && moment(this.selectedStartTime).isBetween(x.StartDateTime, x.EndDateTime));
            } else {
              this.isShowUnAvailableContextMenu = false;
            }
          } else {
            this.selectedStartTime = event.date.format();
            this.selectedResScheduleItem = this.activeTimes.find(x => x.EntityId == event.resource && moment(this.selectedStartTime).isBetween(x.StartDateTime, x.EndDateTime))
            this.contextmenuX = event.jsEvent.pageX;
            this.contextmenuY = event.jsEvent.pageY;
            this.isShowBookingContextMenu = true;
            this.isShowDayContextMenu = false;
            this.isShowEventContextMenu = false;
            this.isShowUnAvailableContextMenu = false;
            this.showDeletedMessage = false;
          }
        } else {
          this.selectedStartTime = event.date.format();
          this.contextmenuX = event.jsEvent.pageX;
          this.contextmenuY = event.jsEvent.pageY;
          this.isShowDayContextMenu = true;
          this.isShowEventContextMenu = false;
          this.isShowBookingContextMenu = false;
          this.isShowUnAvailableContextMenu = false;
          this.showDeletedMessage = false;
        }
        // Do nothing! --> origin: if (selectedSched.ActiveStatus === false)  if schedule dont belong to resource
      }
    }
  }

  openAddNewScheduleModel(content) {
    /* Opens a model that allows for a new time plan. Set time interval to 'available' */
    // const date = moment(this.selectedStartTime).format('YYYY-MM-DD');
    const sTime = moment(this.selectedStartTime).format('HH:mm');
    const eTime = moment(this.selectedStartTime).add(1, 'hours').format('HH:mm');
    const date = new Date(this.selectedStartTime);
    const sDate = {
      date: {
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate()
      }
    }
    this.scheduleForm.patchValue({
      StartDate: sDate,
      EndDate: sDate,
      StartTime: sTime,
      EndTime: eTime
    });
    // this.scheduleForm.patchValue({StartDate: date, EndDate: date, startTime: time});
    this.isShowBookingContextMenu = false;
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.showDeletedMessage = false;
    this.addScheduleModel = this.modalService.open(content, { width: '600' });
  }

  openAddNewBookingModel(content) {
    this.bookingType = 'NewBooking';
    this.isShowBookingContextMenu = false;
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.showDeletedMessage = false;
    const today = new Date();
    this.startTime = moment(this.selectedStartTime).format('HH:mm');
    this.toDate = { date: this.today };
    this.fromDate = { date: this.today };
    this.dayOfWeek = today.getDay();
    if (this.selectedResScheduleItem) {
      this.activeTimeDetail = {};
      this.activeTimeDetail.EntityId = this.selectedResScheduleItem.EntityId;
      this.activeTimeDetail.StartDateTime = this.selectedStartTime;
      this.activeTimeDetail.EndDateTime = this.selectedResScheduleItem.EndDateTime;
      this.activeTimeDetail.ActivityId = this.selectedResScheduleItem.ActivityId;
      this.activeTimeDetail.ActivityName = this.selectedResScheduleItem.ActivityName;
      this.activeTimeDetail.CategoryName = this.selectedResScheduleItem.CategoryName;
      this.activeTimeDetail.SheduleItemId = this.selectedResScheduleItem.SheduleItemId;
      // this.activeTimeDetail.
    }
    this.selectedActivitityUnAvelableList = this.activityUnAvelabelList.filter(x => x.EntityId === this.selectedResScheduleItem.EntityId
      && moment(x.StartDateTime).format('YYYY-MM-DD') === moment(this.selectedStartTime).format('YYYY-MM-DD'));
    this.selectedUnAvelabelList = this.bookingActiveTimeLst.filter(x => x.EntityId === this.selectedResScheduleItem.EntityId && x.IsUnAvailableActiveTime);
    this.selectedBookingList = this.bookingActiveTimeLst.filter(x => x.EntityId === this.selectedResScheduleItem.EntityId
      && moment(x.StartDateTime).format('YYYY-MM-DD') === moment(this.selectedStartTime).format('YYYY-MM-DD'));
    this.addBookingModel = this.modalService.open(content, { width: '800' });
  }

  openUnAvailableModel(content) {
    this.selectedActivitityUnAvelableList = this.activityUnAvelabelList.filter(x => x.EntityId === this.selectedResScheduleItem.EntityId
      && moment(x.StartDateTime).format('YYYY-MM-DD') === moment(this.selectedStartTime).format('YYYY-MM-DD'));
    this.selectedUnAvelabelList = this.bookingActiveTimeLst.filter(x => x.EntityId === this.selectedResScheduleItem.EntityId && x.IsUnAvailableActiveTime);

    const sTime = moment(this.selectedStartTime).format('HH:mm');
    const eTime = moment(this.selectedStartTime).add(1, 'hours').format('HH:mm');

    this.unAvailableForm.patchValue({
      StartTime: sTime,
      EndTime: eTime
    });
    this.isShowBookingContextMenu = false;
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.selectedActivitityUnAvelableList = this.activityUnAvelabelList.filter(x => x.EntityId === this.selectedResScheduleItem.EntityId
      && moment(x.StartDateTime).format('YYYY-MM-DD') === moment(this.selectedStartTime).format('YYYY-MM-DD'));
    this.selectedUnAvelabelList = this.bookingActiveTimeLst.filter(x => x.EntityId === this.selectedResScheduleItem.EntityId && x.IsUnAvailableActiveTime);

    this.addUnavailanleModalModel = this.modalService.open(content, { width: '400' });
  }

  onEventRightclick(event) {
    this.targetBooking = event; // Save target for use in nested menu
    // this.selectedStartTime = event.date.format();
    this.eventontextmenuX = event.jsEvent.pageX;
    this.eventContextmenuY = event.jsEvent.pageY;
    if (event.date.type === 'UNAVAILABLE') {
      this.isShowEventContextMenu = false;
      this.isShowDayContextMenu = false;
      this.isShowBookingContextMenu = false;
      this.showDeletedMessage = false;
    } else {
      /* Opens menu for editing, deleting, routing to member, etc */
      this.selectedResource = event.element.resources.find(x => Number(x.id) === Number(event.date.resourceId)); // Get the name of the resource to put in the modal-title
      this.selectedActiveTime = event.date.activeTimeDetail;
      this.selectedResScheduleItem = this.bookingActiveTimeLst.find(activeBooking => activeBooking.Id === Number(event.date.constraint));
      this.isShowEventContextMenu = true;
      this.isShowDayContextMenu = false;
      this.isShowBookingContextMenu = false;
      this.showDeletedMessage = false;
    }
  }

  handleOnDrop(event) {
    const scheduleId = event.event.constraint;
    const newStartTime = new Date(event.event.start.format());
    const newEndTime = new Date(event.event.end.format());

    const selectedItem = this.activityUnAvelabelList.find(x => x.ScheduleId.toString() === scheduleId);
    const preBookingItem = Object.assign({}, this.bookingActiveTimeLst.find(x => x.Id === event.event.bookingId));

    if (!moment(newStartTime).isBetween(selectedItem.StartDateTime, selectedItem.EndDateTime) &&
      !moment(newEndTime).isBetween(selectedItem.StartDateTime, selectedItem.EndDateTime)) {
      const bookingItem = event.event.activeTimeDetail;
      bookingItem.StartDateTime = moment(newStartTime).format('YYYY-MM-DD HH:mm:ss');
      bookingItem.EndDateTime = moment(newEndTime).format('YYYY-MM-DD HH:mm:ss');
      bookingItem.BranchId = this.branchId;
      bookingItem.RoleType = 'BOOKING';
      PreloaderService.showPreLoader();
      this.adminService.SaveResourceActiveTime(bookingItem).pipe(takeUntil(this.destroy$)).subscribe(
        res => {
          PreloaderService.hidePreLoader();
          if (res.Data > 0) {
            if (bookingItem.Id < 0) {
              bookingItem.Id = res.Data;
            }
          } else {
            bookingItem.StartDateTime = moment(preBookingItem.StartDateTime).format('YYYY-MM-DD HH:mm:ss');
            bookingItem.EndDateTime = moment(preBookingItem.EndDateTime).format('YYYY-MM-DD HH:mm:ss');
          }
          this.createEvents(this.activeTimes, 1);
        },
        err => {
          PreloaderService.hidePreLoader();
          bookingItem.StartDateTime = moment(preBookingItem.StartDateTime).format('YYYY-MM-DD HH:mm:ss');
          bookingItem.EndDateTime = moment(preBookingItem.EndDateTime).format('YYYY-MM-DD HH:mm:ss');
          this.createEvents(this.activeTimes, 1);
          this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ErrorSavingArticle' });
        });
    } else {
      this.createEvents(this.activeTimes, 1);
    }
  }

  bookingEditEvent(content) {
    this.bookingType = 'EditBooking';
    this.isShowBookingContextMenu = false;
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.showDeletedMessage = false;
    const today = new Date();
    this.startTime = moment(this.selectedStartTime).format('HH:mm');
    this.toDate = { date: this.today };
    this.fromDate = { date: this.today };
    this.dayOfWeek = today.getDay();
    this.activeTimeDetail = this.selectedActiveTime;
    this.addBookingModel = this.modalService.open(content, { width: '1000' });
  }

  removeEventShortcut(event) {
    this.selectedActiveTime = event.calEvent.activeTimeDetail;
    this.bookingCancelEvent(this.refTobookingCancelModel)
  }

  bookingCancelEvent(content) {
    this.isShowEventContextMenu = false;
    const bookingItem = this.selectedActiveTime;
    if (bookingItem && bookingItem.BookingMemberList) {
      const memberList = bookingItem.BookingMemberList;
      const filterMemList = memberList.filter(x => x.ItemTypeList.length > 0 && x.ItemTypeList.filter(y => y === 'NO' || y === 'EML' || y === 'IN'));
      if (filterMemList && filterMemList.length > 0) {
        this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'ADMIN.BookingCancelValidation' });
      } else {
        this.bookingCancelModel = this.modalService.open(content, { width: '500' });
      }
    } else {
      this.bookingCancelModel = this.modalService.open(content, { width: '500' });
    }
  }

  closeModal() {
    this.bookingCancelModel.close();
  }

  cancelBooking() {
    const cancelBookingItem = this.selectedActiveTime;
    if (this.cancelBookingComment && cancelBookingItem) {
      if (cancelBookingItem.ArticleId > 0) {
        if (cancelBookingItem.IsPunchCard || cancelBookingItem.IsContractBooking) {
          this.punchCardContractBookingCancel(cancelBookingItem);
        } else if (cancelBookingItem.BookingMemberList && cancelBookingItem.BookingMemberList.filter(x => x.IsPaid) &&
          cancelBookingItem.BookingMemberList.filter(x => x.IsPaid).length > 0) {
          this.paidBookingArticleCancel(cancelBookingItem.BookingMemberList.filter(x => x.IsPaid));
        } else {
          this.saveCancelActiveTime(cancelBookingItem);
        }
      } else {
        cancelBookingItem.IsDeleted = true;
        this.saveCancelActiveTime(cancelBookingItem);
      }
    }
  }

  punchCardContractBookingCancel(cancelBookingItem) {
    let msg: string;
    let msgType: string;
    this.translate.get('COMMON.Confirm').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msgType = translatedValue);
    this.translate.get('ADMIN.ConfirmPunchReturn').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msg = translatedValue);
    this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: msgType,
        messageBody: msg,
        msgBoxId: 'PUNCHCARD_BOOKING_CANCEL',
        optionalData: cancelBookingItem
      });
  }

  punchCardBookingCancelConfirm(cancelBookingItem) {
    if (cancelBookingItem && cancelBookingItem.BookingMemberList) {
      cancelBookingItem.BookingMemberList.forEach(mem => {
        mem.IsDeleted = true;
        mem.IsBookingCreditback = true;
      });
    }
    if (cancelBookingItem && cancelBookingItem.BookingResourceList) {
      cancelBookingItem.BookingResourceList.forEach(res => {
        res.IsDeleted = true;
      });
    }
    this.saveCancelActiveTime(cancelBookingItem);
  }

  paidBookingArticleCancel(cancelBookingItem) {
    let msg: string;
    let msgType: string;
    this.translate.get('COMMON.Confirm').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msgType = translatedValue);
    this.translate.get('ADMIN.Doyouwanttocreaditback').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msg = translatedValue);
    this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: msgType,
        messageBody: msg,
        msgBoxId: 'PAID_BOOKING_CANCEL',
        optionalData: cancelBookingItem
      });
  }

  paidBookingCancelConfirm(cancelBookingItem) {
    /* Sletting av klippekort bookinger skal gå videre herifra. Ref case HT-281. */
    const bookingEntityList = cancelBookingItem.BookingMemberList.filter(x => x.IsPaid);
    if (cancelBookingItem && bookingEntityList) {
      if (bookingEntityList.length > 1) {
        this.isManySelected = true;
      } else {
        this.isManySelected = false;
      }
    }
  }

  saveCancelActiveTime(cancelBookingItem) {
    cancelBookingItem.BranchId = this.branchId;
    cancelBookingItem.IsDeleted = true;
    this.adminService.SaveResourceActiveTime(cancelBookingItem).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data > 0) {
          this.refreshCalendar();
          this.bookingCancelModel.close();
        } else {
          cancelBookingItem.IsDeleted = false;
          this.createEvents(this.activeTimes, 1);
        }
      },
      err => {
        PreloaderService.hidePreLoader();
        cancelBookingItem.IsDeleted = false;
        this.createEvents(this.activeTimes, 1);
        this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ErrorSavingArticle' });
      });
  }

  bookingCategorySelectEvent(content) {
    this.isShowBookingContextMenu = false;
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.showDeletedMessage = false;
    this.adminService.getCategories('BOOKINGCATEGORY').pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.bookingCategoryList = res.Data;
        this.categoryModel = this.modalService.open(content, { width: '500' });
      }, null, null);
  }

  bookingCategoryRowDoubleClick(bookingCategory) {
    const bookingItem = this.selectedActiveTime; // Object.assign({}, this.selectedActiveTime);
    bookingItem.BookingCategoryId = bookingCategory.Id;
    bookingItem.BookingCategoryName = bookingCategory.Name;
    bookingItem.BookingCategoryColor = bookingCategory.Color;
    bookingItem.BranchId = this.branchId;
    bookingItem.RoleType = 'BOOKING';
    PreloaderService.showPreLoader();
    this.adminService.SaveResourceActiveTime(bookingItem).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data > 0) {
        } else {
          // bookingItem.StartDateTime = moment(preBookingItem.StartDateTime).format('YYYY-MM-DD HH:mm:ss');
          // bookingItem.EndDateTime = moment(preBookingItem.EndDateTime).format('YYYY-MM-DD HH:mm:ss');
        }
        this.createEvents(this.activeTimes, 1);
      },
      err => {
        PreloaderService.hidePreLoader();
        // bookingItem.StartDateTime = moment(preBookingItem.StartDateTime).format('YYYY-MM-DD HH:mm:ss');
        // bookingItem.EndDateTime = moment(preBookingItem.EndDateTime).format('YYYY-MM-DD HH:mm:ss');
        this.createEvents(this.activeTimes, 1);
        this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ErrorSavingArticle' });
      });
    // this.bookingForm.patchValue({
    //   BookingCategoryId: item.Id,
    //   BookingCategoryName: item.Name
    //  });
    this.categoryModel.close();
  }

  bookingArrivedEvent(content) {
    this.isShowBookingContextMenu = false;
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.showDeletedMessage = false;
    const bookingItem = this.selectedActiveTime;
    if (bookingItem && bookingItem.BookingMemberList) {
      const notArrivedMemberlist = bookingItem.BookingMemberList.filter(x => !x.IsArrived);
      if (notArrivedMemberlist.length > 1) {
        this.notArrivedMembers = notArrivedMemberlist;
        this.memberSelectedModel = this.modalService.open(content, { width: '500' });
      } else if (notArrivedMemberlist.length === 1) {
        const memberIdList: any = [];
        memberIdList.push(notArrivedMemberlist[0].Id);
        const arrivedObj = {
          ActiveTimeId: bookingItem.Id,
          ArticleName: bookingItem.ArticleName,
          StartDate: bookingItem.StartDateTime,
          RoleType: bookingItem.RoleTpye,
          IsArrived: true,
          IsPaid: false,
          EntityList: memberIdList
        }
        PreloaderService.showPreLoader();
        this.adminService.DeleteResourceActiveTime(arrivedObj).pipe(takeUntil(this.destroy$)).subscribe(
          res => {
            PreloaderService.hidePreLoader();
            if (res.Data > 0) {
            }
          });
      }
    }
  }

  arrivedMemberSelect() {
    const memberIdList: any = [];
    const bookingItem = this.selectedActiveTime;
    this.notArrivedMembers.forEach(mem => {
      memberIdList.push(mem.Id);
    });
    const arrivedObj = {
      ActiveTimeId: bookingItem.Id,
      ArticleName: bookingItem.ArticleName,
      StartDate: bookingItem.StartDateTime,
      RoleType: bookingItem.RoleTpye,
      IsArrived: true,
      IsPaid: false,
      EntityList: memberIdList
    }
    PreloaderService.showPreLoader();
    this.adminService.DeleteResourceActiveTime(arrivedObj).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data > 0) {
        }
      });
  }

  bookingCheckOutEvent(content, shopContent) {
    this.isShowBookingContextMenu = false;
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.showDeletedMessage = false;
    const bookingItem = this.selectedActiveTime;
    if (bookingItem && bookingItem.BookingMemberList) {
      const notPaidMemberlist = bookingItem.BookingMemberList.filter(x => !x.IsPaid);
      if (notPaidMemberlist.length > 1) {
        this.selectedMembers = notPaidMemberlist;
        this.memberSelectedModel = this.modalService.open(content, { width: '500' });
      } else if (notPaidMemberlist.length === 1) {
        this.openShop(shopContent, notPaidMemberlist[0], bookingItem);
      } else {
        // no any member fot the payment
      }
    }
  }

  openShop(content, item, bookingItem) {
    this.selectedMember = item;
    this.shopService.GetSelectedGymSettings({ GymSetColNames: this.selectedGymsettings, IsGymSettings: true }).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        for (const key in res.Data) {
          if (res.Data.hasOwnProperty(key)) {
            switch (key) {
              case 'EcoNegativeOnAccount':
                this.exceGymSetting.OnAccountNegativeVale = res.Data[key];
                break;
              case 'EcoIsShopOnNextOrder':
                this.exceGymSetting.IsShopOnNextOrder = res.Data[key];
                break;
              case 'EcoIsDailySettlementForAll':
                this.exceGymSetting.IsDailySettlementForAll = res.Data[key];
                break;
              case 'EcoIsOnAccount':
                this.exceGymSetting.IsOnAccount = res.Data[key];
                break;
              case 'EcoIsDefaultCustomerAvailable':
                this.exceGymSetting.IsDefaultCustomerAvailable = res.Data[key];
                break;
              case 'EcoIsPayButtonsAvailable':
                this.exceGymSetting.IsPayButtonsAvailable = res.Data[key];
                break;
              case 'EcoIsPettyCashSameAsUserAllowed':
                this.exceGymSetting.IsPettyCashSameAsUserAllowed = res.Data[key];
                break;
              case 'EcoIsPrintInvoiceInShop':
                this.exceGymSetting.IsPrintInvoiceInShop = res.Data[key];
                break;
              case 'EcoIsPrintReceiptInShop':
                this.exceGymSetting.IsPrintReceiptInShop = res.Data[key];
                break;
              case 'OtherIsShopAvailable':
                this.exceGymSetting.IsShopAvailable = res.Data[key];
                break;
              case 'OtherLoginShop':
                this.exceGymSetting.IsShopLoginNeeded = res.Data[key];
                break;
              case 'HwIsShopLoginWithCard':
                this.exceGymSetting.IsShopLoginWithCard = res.Data[key];
                break;
              case 'ReqMemberListRequired':
                this.exceGymSetting.MemberRequired = res.Data[key];
                break;
              case 'OtherIsLogoutFromShopAfterSale':
                this.exceGymSetting.IsLogoutFromShopAfterSale = res.Data[key];
                break;
              case 'EcoSMSInvoiceShop':
                this.exceGymSetting.IsSMSInvoiceShop = res.Data[key];
                break;
              case 'EcoIsBankTerminalIntegrated':
                this.exceGymSetting.IsBankTerminalIntegrated = res.Data[key];
                break;
              case 'OtherIsValidateShopGymId':
                this.exceGymSetting.IsValidateShopGymId = res.Data[key];
                break;
            }

          }
        }
        this.shopLoginData.GymSetting = this.exceGymSetting;
        this.shopService.GetSalesPoint().pipe(
          takeUntil(this.destroy$)
          ).subscribe(
          salePoints => {
            this.shopLoginData.SalePoint = salePoints.Data[0];
            this.adminService.getGymSettings('HARDWARE', null).pipe(
              takeUntil(this.destroy$)
              ).subscribe(
              hwSetting => {
                this.shopLoginData.HardwareProfileList = hwSetting.Data;
                this.shopLoginData.HardwareProfileList.forEach(hwProfile => {
                  if (this.shopLoginData.SalePoint != null &&
                    this.shopLoginData.SalePoint.HardwareProfileId === hwProfile.Id) {
                    this.shopLoginData.SelectedHardwareProfile = hwProfile;
                    this.shopLoginData.HardwareProfileId = hwProfile.Id;
                  } else {
                    this.shopLoginData.SelectedHardwareProfile = null;
                    this.shopLoginData.HardwareProfileId = null;
                  }
                  this.shopHomeService.$shopLoginData = this.shopLoginData;
                });
                this.setCheoutEvent(content, item, bookingItem);
              }, null, null);
          }, null, null);
      }, null, null)
  }

  setCheoutEvent(content, member, bookingItem) {
    let orderArticles = [];
    member.RoleId = 1;
    this.shopHomeService.$selectedMember = member;
    if (bookingItem.ActivityId > 0) {
      const articleSearch = {
        ActivityId: bookingItem.ActivityId,
        BranchId: this.branchId
      }
      this.adminService.GetArticleForResouceBooking(articleSearch).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
          if (result) {
            if (bookingItem.ArticleId > 0) {
              const articleItem = result.Data.find(x => x.Id === bookingItem.ArticleId && x.CategoryCode === 'BOOKING' &&
                !x.IsPunchCard && !x.IsContractBooking);
              if (articleItem) {
                if (articleItem.DefaultPrice <= 0) {
                  this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ArticlePriceIsZero' });
                } else {
                  articleItem.Price = articleItem.DefaultPrice;
                  orderArticles.push({
                    Id: articleItem.Id,
                    ArticleNo: articleItem.ArticleId,
                    Description: articleItem.Description,
                    SelectedPrice: articleItem.Price,
                    UnitPrice: articleItem.Price,
                    DefaultPrice: articleItem.Price,
                    Price: articleItem.Price,
                    OrderQuantity: 1,
                    Discount: articleItem.Discount,
                    ExpiryDate: articleItem.ExpiryDate,
                    VoucherNumber: articleItem.VoucherNo,
                    IsEditable: false
                  })
                  this.shopHomeService.$priceType = 'MEMBER';
                  this.shopHomeService.$shopMode = 'BOOKING';
                  this.shopHomeService.ShopItems = orderArticles;
                  this.shopHomeService.$isUpdateInstallments = false;
                  this.shopModalReference = this.modalService.open(content);
                }
              } else {
                this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ArticlesOfPunchcard' });
              }
            } else {
              this.shopHomeService.$priceType = 'MEMBER';
              this.shopHomeService.$shopMode = 'BOOKING';
              this.shopHomeService.$activityId = bookingItem.ActivityId;
              this.shopHomeService.$isUpdateInstallments = false;
              this.shopModalReference = this.modalService.open(content);
            }
          }
        }
        )
    }
  }

  closeShop() {
    this.shopModalReference.close();
  }

  closeShopAfterSave(response) {
    let paymentType = '';
    const bookingItem = this.selectedActiveTime;
    response.payModes.forEach( (payMode, index) => {
      if (index === 0) {
        paymentType = this.getTransType(payMode.PaymentTypeCode);
      } else {
        paymentType = ',' + this.getTransType(payMode.PaymentTypeCode);
      }
    });
    let bookingPayment = {
      ActivetimeId: bookingItem.Id,
      ArticleId: response.articlesPaid[0].ArticleId,
      MemberId: this.selectedMember.Id,
      Paid: true,
      AritemNo: response.arItemNo,
      Amount: response.totalPrice,
      PaymentType: paymentType
    }
    this.adminService.SavePaymentBookingMember(bookingPayment).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      res => {
        if (res.Data > 0) {
          const paidMember = bookingItem.BookingMemberList.find(x => x.Id === this.selectedMember.Id);
          if (paidMember) {
            paidMember.IsPaid = true;
            paidMember.Amount = response.totalPrice;
            paidMember.ArItemNo = response.arItemNo;
            if (paymentType) {
              const paymentTypeArray = paymentType.split(',');
              paidMember.ItemTypeList = paymentTypeArray;
            }
          }
        }
      });
    this.shopModalReference.close();
  }

  getTransType(paymentType) {
    switch (paymentType) {
        case 'CASH':
            return 'CSH';

        case 'BANKTERMINAL':
            return 'TMN';

        case 'OCR':
            return 'LOP';

        case 'BANKSTATEMENT':
            return 'BS';

        case 'DEBTCOLLECTION':
            return 'DC';

        case 'ONACCOUNT':
            return 'OA';

        case 'GIFTCARD':
            return 'GC';

        case 'BANK':
            return 'BNK';

        case 'PREPAIDBALANCE':
            return 'PB';

        case 'EMAILSMSINVOICE':
            return 'EML';

        case 'NEXTORDER':
            return 'NO';

        case 'INVOICE':
            return 'IN';

    }
    return '';
  }

  bookingSmsReminderEvent() {
    this.isShowBookingContextMenu = false;
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.showDeletedMessage = false;
    const bookingItem = this.selectedActiveTime;
    this.memberDictionary = {};

    if (bookingItem && bookingItem.BookingMemberList) {
      for (const mem of bookingItem.BookingMemberList) {
        if (mem.Mobile) {
          this.memberDictionary[mem.Id] = mem.Name;
        }
      }
    }

    if (this.memberDictionary && _.size(this.memberDictionary) > 0) {
      let withoutMobileMemName: string;
      bookingItem.BookingMemberList.forEach(member => {
        if (!member.Mobile) {
          if (withoutMobileMemName) {
            withoutMobileMemName = withoutMobileMemName + '\n' + member.Name;
          } else {
            withoutMobileMemName = member.Name;
          }
        }
      });
      if (withoutMobileMemName) {
        let msg: string;
        let msgType: string;
        this.translate.get('COMMON.Confirm').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msgType = translatedValue);
        this.translate.get('ADMIN.ConfirmSendSms').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msg = translatedValue);
        msg = msg + '\n' + withoutMobileMemName;
        this.exceMessageService.openMessageBox('CONFIRM',
          {
            messageTitle: msgType,
            messageBody: msg,
            msgBoxId: 'SEND_SMSREMINDER',
            optionalData: bookingItem
          });
      } else {
        this.sendSmsReminder(bookingItem);
      }
    } else {
      this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'ADMIN.WarningSendSms' });
    }
  }

  sendSmsReminderConfirm(bookingItem) {
    this.sendSmsReminder(bookingItem);
  }

  sendSmsReminder(bookingItem) {
    const smsObj = {
      MemberIdList: this.memberDictionary,
      BranchId: this.branchId,
      MemberName: '', // not need to send it's getting using member id list
      ResName: bookingItem.EntityName,
      StartTime: bookingItem.StartDateTime,
      EndTime: bookingItem.EndDateTime,
      BookingDate: moment(bookingItem.StartDateTime).format('YYYY-MM-DD')
    }
    PreloaderService.showPreLoader();
    this.adminService.AddFreeDefineNotification(smsObj).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data === true) {
          this.smsFreeTextModel.close();
        } else {
        }
      });
  }

  bookingSmsFreeTextEvent(content) {
    this.isShowBookingContextMenu = false;
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.showDeletedMessage = false;
    const bookingItem = this.selectedActiveTime;
    this.memberDictionary = {};
    if (bookingItem && bookingItem.BookingMemberList) {
      for (const mem of bookingItem.BookingMemberList) {
        if (mem.Mobile) {
          this.memberDictionary[mem.Id] = mem.Name;
        }
      }

      if (this.memberDictionary && _.size(this.memberDictionary) > 0) {
        let withoutMobileMemName: string;
        bookingItem.BookingMemberList.forEach(member => {
          if (!member.Mobile) {
            if (withoutMobileMemName) {
              withoutMobileMemName = withoutMobileMemName + '\n' + member.Name;
            } else {
              withoutMobileMemName = member.Name;
            }
          }
        });
        if (withoutMobileMemName) {
          let msg: string;
          let msgType: string;
          this.translate.get('COMMON.Confirm').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msgType = translatedValue);
          this.translate.get('ADMIN.ConfirmSendSms').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msg = translatedValue);
          msg = msg + '\n' + withoutMobileMemName;
          this.exceMessageService.openMessageBox('CONFIRM',
            {
              messageTitle: msgType,
              messageBody: msg,
              msgBoxId: 'SEND_SMSFREETEXT',
              optionalData: content
            });
        } else {
          this.smsFreeTextModel = this.modalService.open(content, { width: '500' });
          //    let smsObj = {
          //     MemberIdList: this.memberDictionary,
          //     BranchId: this.branchId,
          //     Message: this.smsFreeText
          //    }
          //   PreloaderService.showPreLoader();
          //  this.adminService.AddFreeDefineNotification(smsObj).pipe(takeUntil(this.destroy$)).subscribe(
          //   res => {
          //     PreloaderService.hidePreLoader();
          //     if (res.Data > 0) {
          //     }
          //   });
        }
      } else {
        this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'ADMIN.WarningSendSms' });
      }
    }
  }

  sendSmsFreeTextConfirm(content) {
    this.smsFreeTextModel = this.modalService.open(content, { width: '500' });
  }

  sendSmsFreetext() {
    const smsObj = {
      MemberIdList: this.memberDictionary,
      BranchId: this.branchId,
      Message: this.smsFreeText
    }
    PreloaderService.showPreLoader();
    this.adminService.SendSmsForBooking(smsObj).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data === true) {
          this.smsFreeTextModel.close();
        } else {
        }
      });
  }

  bookingSwitchResourceEvent(content) {
    // const bookingItem = this.selectedActiveTime;
    // const stDate = bookingItem.StartDateTime;
    // const edDate = bookingItem.EndDateTime;
    // this.switchResourceForm.patchValue({
    //   StartDate: stDate,
    //   EndDate: edDate
    // })
    this.isShowEventContextMenu = false;
    this.switchResourceModel = this.modalService.open(content, { width: '500' });
  }

  selectSwitchResourceModel(content) {
    // this.allResources
    this.adminService.GetResources(this.branchId, '', -1, -1, 2, true).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.allResources = result.Data;
      }
    }, null,
    () => {
      this.allResources = this.allResources.filter(extraResource => extraResource.Id !== Number(this.selectedResource.id));
    });
    this.changeResourceModal = this.modalService.open(content, { width: '400' });
  }

  onChooseResourceGridReady(params) {
    this.resgridApi = params.api;
    // this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    this.resgridApi.sizeColumnsToFit();
    // hiding loading message on load
    this.resgridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  saveResourceChange() {
    if (this.resourceSelected && this.resoureComment) {
      // change resource on booking
      this.adminService.ChangeResourceOnBooking(this.selectedActiveTime.SheduleItemId,
        this.switchResourceForm.get('ResourceId').value, this.switchResourceForm.get('Comment').value).pipe(takeUntil(this.destroy$)).subscribe(resourceRes => {
          if (resourceRes.Data === 1) {
            this.refreshCalendar();
            this.resourceSelected = false;
            this.resoureComment = false;
            this.switchResourceModel.close();
          }
        })
    } else {
      if (!this.resourceSelected) {
        this.exceMessageService.openMessageBox('ERROR', {
          messageTitle: 'Exceline', messageBody: 'ADMIN.TimePeriodNotValid'
        });
      } else if (!this.resoureComment) {
        this.exceMessageService.openMessageBox('ERROR', {
          messageTitle: 'Exceline', messageBody: 'ADMIN.TimePeriodNotValid'
        });
      }
    }
  }

  closesaveResourceChange() {
    this.switchResourceModel.close();
    this.resourceSelected = false;
    this.resoureComment = false;
  }

  resourceChangeTextare(event) {
    if (event.length > 0) {
      this.resoureComment = true;
    } else {
      this.resoureComment = false;
    }
  }

  selectResourceDblClick(event) {
    this.resourceSelected = true;
    this.switchResourceForm.patchValue({
      ResourceName: event.Name,
      ResourceId: event.Id})
    this.changeResourceModal.close();
  }

  bookingOpenCustomerCardEvent() {
    const url = '/membership/card/' + this.targetBooking.date.bookerId + '/' + this.targetBooking.date.bookerBranch + '/' + this.targetBooking.date.bookerRole;
    this.router.navigate([url]);
  }

  closeBookingEvent() {
    this.addBookingModel.close();
  }

  onBackgroundRightClick(event) {
    this.selectedStartTime = event.date.format();
    this.contextmenuX = event.jsEvent.pageX;
    this.contextmenuY = event.jsEvent.pageY;
    this.isShowDayContextMenu = true;
    this.isShowEventContextMenu = false;
    this.showDeletedMessage = false;
  }

  handleEventClick(e) {
    const self = this;
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.showDeletedMessage = false;
    this.event = {};
    this.event.title = e.calEvent.title;

    const start = e.calEvent.start;
    const end = e.calEvent.end;
    if (e.view.name === 'month' || e.view.name === 'basicWeek' || e.view.name === 'basicDay') {
      start.stripTime();
    }

    if (end) {
      end.stripTime();
      this.event.end = end.format();
    }

    this.event.id = e.calEvent.id;
    this.event.start = start.format();
    this.dialogVisible = true;
  }

  // Display active button
  onButtonGroupClick($event) {
    const clickedElement = $event.target || $event.srcElement;
    if (clickedElement.nodeName === 'BUTTON') {

      const isCertainButtonAlreadyActive = clickedElement.parentElement.querySelector('.active');
      if (isCertainButtonAlreadyActive) {
        isCertainButtonAlreadyActive.classList.remove('active');
      }
      clickedElement.className += ' active';
    }
  }

  // Hide context menu if appering
  hideContextMenu() {
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.showDeletedMessage = false;
  }

  handleDayClick(event) {
    this.isShowDayContextMenu = false;
    this.isShowBookingContextMenu = false;
    this.isShowEventContextMenu = false;
    this.showDeletedMessage = false;
  }

  // Load privious day of schedular
  loadPriviousDay(scheduler) {
    scheduler.prev();
    this.generateTitle(scheduler);
  }

  // Load next day of schedular
  loadNextDay(scheduler) {
    scheduler.next();
    this.generateTitle(scheduler);
  }


  private generateTitle(scheduler: any) {
    if (scheduler.getView() === 'agendaWeek' || scheduler.getView() === 'month') {
      this.isShowTimeDuration = true;
      this.calandarDateRange = scheduler.getCalendarDateRange();
      const start = { year: this.calandarDateRange.start.getFullYear(), month: this.calandarDateRange.start.getMonth() + 1, day: this.calandarDateRange.start.getDate() };
      const end = { year: this.calandarDateRange.end.getFullYear(), month: this.calandarDateRange.end.getMonth() + 1, day: this.calandarDateRange.end.getDate() };
      this.dateFilterForm.patchValue({
        fromDate: { date: start },
        toDate: { date: end }
      });
      this.viewStartDate = start;
      this.viewEndDate = end;
      this.refreshCalendar();

    } else {
      this.calandarDate = scheduler.getDate();
      this.isShowTimeDuration = false;
      const date = new Date(scheduler.getDate());
      const newDate = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() };
      this.dateFilterForm.patchValue({
        fromDate: { date: newDate },
        toDate: { date: newDate }
      });
      this.viewStartDate = date;
      this.viewEndDate = date;
      this.refreshCalendar();
    }
  }

  eventRender(this, event, element) {

    if (event.rendering === 'background' &&
      event.className[0] === 'unavailable' &&
      event.className[1] === 'fc-bgevent-disabled'
    ) {
      element.append('<div class="unavailable-title">' + event.title + '</div>');
    }

    const self = this;
    if (event.activeTimeDetail) {
      ;
      let memberNames = '';
      let arrival = ' Arrival : NO';
      const memList = event.activeTimeDetail.BookingMemberList;
      if (memList && memList.filter(x => x.IsArrived).length === memList.length) {
        arrival = ' Arrival : YES';
      }
      // const arrival = event.activeTimeDetail.Arrival ? ' Arrival : YES' : ' Arrival : NO';
      event.activeTimeDetail.BookingMemberList.forEach(item => {
        if (memberNames) {
          memberNames = memberNames + '\n' + item.Name;
        } else {
          memberNames = item.Name;
        }
      });

      const arrivedIcon = event.activeTimeDetail.BookingMemberList.filter(x => !x.IsArrived).length > 0 ? false : true;
      const memberIcon = event.activeTimeDetail.BookingMemberList.length > 1 ? true : false;

      if (arrivedIcon && memberIcon) {
        element.find('.fc-content').append(`<span class="icon-done text-dark exc-cal-changed-icon ml-0"></span>
        <span class="text-dark">` + arrival + `</span>
         <span class="icon-calendar-member-group text-dark exc-cal-changed-icon"></span>
         <i  class=\'icon-close float-right fc-close\' id=\'` + event.activeTimeDetail.Id + `\'></i>`);
      } else if (arrivedIcon && !memberIcon) {
        element.find('.fc-content').append(`<span class="icon-done text-dark exc-cal-changed-icon ml-0"></span>
        <span class="text-dark">` + arrival + `</span>
         <i  class=\'icon-close float-right fc-close\' id=\'` + event.activeTimeDetail.Id + `\'></i>`);
      } else if (!arrivedIcon && memberIcon) {
        element.find('.fc-content').append(` <span class="text-dark">` + arrival + `</span>
         <span class="icon-calendar-member-group text-dark exc-cal-changed-icon"></span>
         <i  class=\'icon-close float-right fc-close\' id=\'` + event.activeTimeDetail.Id + `\'></i>`);
      } else {
        element.find('.fc-content').append(` <span class="text-dark">` + arrival + `</span>
        <i  class=\'icon-close float-right fc-close\' id=\'` + event.activeTimeDetail.Id + `\'></i>`);
      }

      // if (event.activeTimeDetail.BookingMemberList.filter(x => !x.IsArrived).length > 0) {
      //   element.find('.fc-content').append(`
      //   <span class="text-dark">` + arrival + `</span>
      //    <span class="icon-calendar-member-group text-dark exc-cal-changed-icon"></span>
      //    <i  class=\'icon-close float-right fc-close\' id=\'` + event.activeTimeDetail.Id + `\'></i>`);
      // } else {
      //   element.find('.fc-content').append(`<span class="icon-done text-dark exc-cal-changed-icon ml-0"></span>
      //   <span class="text-dark">` + arrival + `</span>
      //    <span class="icon-calendar-member-group text-dark exc-cal-changed-icon"></span>
      //    <i  class=\'icon-close float-right fc-close\' id=\'` + event.activeTimeDetail.Id + `\'></i>`);
      // }
    }
  }

  dayRender(date, cell) {

  }

  mouseOver(event) {
    const self = this;
    // this.scheduler = scheduler;
    let memberListHtml = '';
    let resourceLstHtml = '';
    let arrived = 'No';
    const memList = event.calEvent.activeTimeDetail.BookingMemberList;
    if (memList && memList.filter(x => x.IsArrived).length === memList.length) {
      arrived = 'YES';
    }

    const arrivedMemberList = event.calEvent.activeTimeDetail.BookingMemberList.filter(x => x.IsArrived);
    const notArrivedMemberList = event.calEvent.activeTimeDetail.BookingMemberList.filter(x => !x.IsArrived);
    if (arrivedMemberList && arrivedMemberList.length > 0) {
      memberListHtml = '<ul class="m-0 p-0 fc-exc-list fc-exc-list-arrived">';
      arrivedMemberList.forEach(mem => {
        memberListHtml = memberListHtml + '<li>' + mem.CustId + '-' + mem.Name + '</li>';
      });
      memberListHtml = memberListHtml + '</ul>';
    }
     if (notArrivedMemberList && notArrivedMemberList.length > 0) {
      memberListHtml = memberListHtml + '<ul class="m-0 p-0 fc-exc-list fc-exc-list-not-arrived">';
      notArrivedMemberList.forEach(mem => {
        memberListHtml = memberListHtml + '<li>' + mem.CustId + '-' + mem.Name + '</li>';
      });
      memberListHtml = memberListHtml + '</ul>';
    }
     if (event.calEvent.activeTimeDetail.ResourceLst && event.calEvent.activeTimeDetail.ResourceLst.length > 0) {
      resourceLstHtml = '<ul class="m-0 p-0 fc-exc-list fc-exc-list-location">';
      event.calEvent.activeTimeDetail.ResourceLst.forEach(ins => {
        resourceLstHtml = resourceLstHtml + '<li>' + ins.DisplayName + '</li>';
      });
      resourceLstHtml = resourceLstHtml + '</ul>';
    }

    const tooltip =
      `<div class="event-tooltip">
      <div><span class="text-muted">Date :</span>
        <span>` + moment(event.calEvent.start._d).format('MM/DD/YYYY') + `</span>
      </span>
      </div>
      <div><span class="text-muted">Day :</span>
        <span>` + moment(event.calEvent.start._d).format('dddd') + `</span>
        <span> | </span>
        <span>` + moment(event.calEvent.start).format('h:mm a') + `
        <span> - </span>` + moment(event.calEvent.end).format('h:mm a') + `
      </span>
      </div>
      <div><span class="text-muted">Category :</span>
        <span>
          ` + event.calEvent.activeTimeDetail.CategoryName + `
        </span>
      </div>
      <div><span class="text-muted">Booking Category :</span>
        <span>
          ` + event.calEvent.activeTimeDetail.BookingCategoryName + `
        </span>
      </div>
      <div><span class="text-muted">Arrival :</span>
        <span>
          ` + arrived + `
        </span>
      </div>
      <div><span class="text-muted">Members No And Name :</span></div>
      <div>` + memberListHtml + `
      </div>
      <div><span class="text-muted">Article :</span>
        <span>
          ` + event.calEvent.activeTimeDetail.ArticleName + `
        </span>
      </div>
    </div>`;

    const $tooltip = $(tooltip).appendTo('body');
    let clientHeight = 0;
    let clientWidth = 0;
    $(event.jsEvent.currentTarget).mouseover(function (e) {
      $(event.jsEvent.currentTarget).css('z-index', 10000);
      $tooltip.fadeIn('500');
      $tooltip.fadeTo('10', 1.9);
      clientHeight = document.body.clientHeight;
      clientWidth = document.body.clientWidth;

    }).mousemove(function (e) {

      this.isShowTooltip = true;


      if ((e.pageX + $('.event-tooltip').width() + 70) > clientWidth) {
        this.tooltipX = e.pageX - $('.event-tooltip').width();
        $tooltip.css('left', e.pageX - ($('.event-tooltip').width() + 40));
      } else {
        this.tooltipX = e.pageX + 20;
        $tooltip.css('left', e.pageX + 20);
      }

      if ((e.pageY + $('.event-tooltip').height() + 30) > clientHeight) {
        this.tooltipY = e.pageY - ($('.event-tooltip').height() + 30);
        $tooltip.css('top', e.pageY - ($('.event-tooltip').height() + 30));

      } else {
        $tooltip.css('top', e.pageY + 10);
        this.tooltipY = e.pageY + 10;

      }



    });
  }

  mouseOut(event) {
    $(event.jsEvent.currentTarget).css('z-index', 8);
    $('.event-tooltip').remove();
  }


  entitySelection(content: any, type: any) {

    if (type === 'EMP') {
      this.translate.get('MEMBERSHIP.SelectEmployee').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.entitySelectionViewTitle = tranlstedValue);
      this.isBranchVisible = false;
      this.multipleRowSelect = false;
      this.isRoleVisible = false;
      this.isStatusVisible = false;
      this.isAddBtnVisible = false;
      this.isDbPagination = false;
      this.isBasicInforVisible = false;
      this.entitySelectionType = EntitySelectionType[EntitySelectionType.EMP];
      this.columns = [{ property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'CustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
      ]
      PreloaderService.showPreLoader();
      this.adminService.GetGymEmployees(this.branchId, '', true, -1).pipe(takeUntil(this.destroy$)).subscribe
        (result => {
          if (result) {
            PreloaderService.hidePreLoader();
            this.items = result.Data;
            this.itemCount = result.Data.length;
            this.entitySelectionModal = this.modalService.open(content, { width: '800' });
          } else {
            PreloaderService.hidePreLoader();
          }
        }
        )
    } else if (type === 'RES') {
      this.translate.get('MEMBERSHIP.SelectEmployee').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.entitySelectionViewTitle = tranlstedValue);
      this.isBranchVisible = false;
      this.multipleRowSelect = false;
      this.isRoleVisible = false;
      this.isStatusVisible = false;
      this.isAddBtnVisible = false;
      this.isDbPagination = false;
      this.isBasicInforVisible = false;
      this.entitySelectionType = EntitySelectionType[EntitySelectionType.RES];
      this.columns = [{ property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
      ]
      PreloaderService.showPreLoader();
      this.adminService.GetResources(this.branchId, '', -1, -1, 2, true).pipe(takeUntil(this.destroy$)).subscribe
        (result => {
          if (result) {
            this.allResources = result.Data;
            PreloaderService.hidePreLoader();
            this.items = result.Data;
            this.itemCount = result.Data.length;
            this.entitySelectionModal = this.modalService.open(content, { width: '800' });
          } else {
            PreloaderService.hidePreLoader();
          }
        }
        )
    }
  }

  searchDetail(val: any) {
    switch (val.entitySelectionType) {
      case EntitySelectionType[EntitySelectionType.EMP]:
        this.adminService.GetGymEmployees(this.branchId, val.searchVal, true, -1).pipe(takeUntil(this.destroy$)).subscribe
          (result => {
            this.items = result.Data;
            this.itemCount = result.Data.length;
          }, err => {
          });
        break;
      case EntitySelectionType[EntitySelectionType.RES]:
        this.adminService.GetResources(this.branchId, val.searchVal, -1, -1, 2, true
        ).pipe(takeUntil(this.destroy$)).subscribe
          (result => {
            this.items = result.Data;
            this.itemCount = result.Data.length;
          }, err => {
          });
        break;
    }
  }

  selectedRowItem(item) {
    switch (this.entitySelectionType) {
      case EntitySelectionType[EntitySelectionType.EMP]:
        this.scheduleForm.patchValue({ EmpId: item.Id })
        this.scheduleForm.patchValue({ EmpName: item.Name })
        break;

      case EntitySelectionType[EntitySelectionType.RES]:
        this.scheduleForm.patchValue({ ResourceId: item.Id })
        this.scheduleForm.patchValue({ ResourcesName: item.Name })
        break;
    }
    this.entitySelectionModal.close();
  }
  selectedRowItems(item) {
    this.entitySelectionModal.close();
  }

  saveSchedule(value) {
    /* Save new available schedule time slot */
    const sDate: Date = new Date(value.StartDate.date.year, value.StartDate.date.month - 1, value.StartDate.date.day);
    const eDate: Date = new Date(value.EndDate.date.year, value.EndDate.date.month - 1, value.EndDate.date.day);
    const dayName = moment(sDate).format('dddd');
    const dayList: string[] = [];
    dayList.push(dayName);
    const addedscheduleItem = {
      Id: -1,
      ActiveStatus: true,
      StartDate: moment(sDate).format('YYYY-MM-DD'),
      StartTime: '1900-01-01T' + value.StartTime + ':00',
      EndDate: moment(eDate).format('YYYY-MM-DD'),
      EndTime: '1900-01-01T' + value.EndTime + ':00',
      WeekType: 2,
      EmpId: value.EmpId,
      ResourceId: value.ResourceId, /* Should be extraResource, as it is the id of an appended resource. If resourceId is not null, the booking will also show on the added resource's calendar */
      DayList: dayList,
      Occurrence: 'WEEKLY',
      UpdateStatus: value.UpdateStatus
    }
    const scheduleItemListObj = {
      scheduleItem: addedscheduleItem,
      resId: this.selectedResourceId,
      branchId: this.branchId
    }
    this.adminService.SaveScheduleItem(scheduleItemListObj).pipe(takeUntil(this.destroy$)).subscribe(obj => {
      if (obj) {
        addedscheduleItem.Id = obj.Data;
        const selectedRes = this.filterResourceList.find(x => x.Id === this.selectedResourceId);
        const scheduleItemList = selectedRes.Schedule.SheduleItemList;
        scheduleItemList.push(addedscheduleItem);
        this.initializeCalendar();
        this.addScheduleModel.close();
      }
    });
  }

  saveUnavailableTime(value) {
    const startDateTime = moment(moment(this.selectedStartTime).format('YYYY-MM-DD') + ' ' + value.StartTime).format('YYYY-MM-DD HH:mm:ss');
    const endDateTime = moment(moment(this.selectedStartTime).format('YYYY-MM-DD') + ' ' + value.EndTime).format('YYYY-MM-DD HH:mm:ss');
    const filterUnAvelableItem = this.activityUnAvelabelList.filter(x => x.EntityId === this.selectedResourceId && moment(startDateTime).isBetween(x.StartDateTime, x.EndDateTime)
        || moment(endDateTime).isBetween(x.StartDateTime, x.EndDateTime)
        || moment(x.StartDateTime).isBetween(startDateTime, endDateTime));

    const filterBookingItem = this.bookingActiveTimeLst.filter(x => x.EntityId === this.selectedResourceId && moment(startDateTime).isBetween(x.StartDateTime, x.EndDateTime)
      || moment(endDateTime).isBetween(x.StartDateTime, x.EndDateTime)
      || moment(x.StartDateTime).isBetween(startDateTime, endDateTime));
    if (filterUnAvelableItem.length === 0 && filterBookingItem.length === 0) {
      const unavailable = {
        ScheduleItemId: this.selectedResScheduleItem.SheduleItemId,
        ActiveTimeId: -1,
        StartDate: startDateTime,
        EndDate: endDateTime
      }
      this.adminService.UnavailableResourceBooking(unavailable).pipe(takeUntil(this.destroy$)).subscribe(obj => {
        if (obj) {
          this.addUnavailanleModalModel.close();
          this.initializeCalendar();
        }
      });
    } else {
      this.exceMessageService.openMessageBox('ERROR', {
        messageTitle: 'Exceline', messageBody: 'ADMIN.TimePeriodNotValid'
      });
    }
  }

  loadPrevResources() {
    this.pageNumber--;
    if (this.pageNumber > 1) {
      this.isDisableNext = false;
      this.isDisablePrev = false;
    } else {
      this.isDisableNext = false;
      this.isDisablePrev = true;
    }
    this.createEvents(this.activeTimes, this.pageNumber);
  }

  loadNextResources() {
    this.pageNumber++;
    if (this.filterResourceList.length > (10 * this.pageNumber)) {
      this.isDisableNext = false;
      this.isDisablePrev = false;
    } else {
      this.isDisableNext = true;
      this.isDisablePrev = false;
    }
    this.createEvents(this.activeTimes, this.pageNumber);
  }

}
