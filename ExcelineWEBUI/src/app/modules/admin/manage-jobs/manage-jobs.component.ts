import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { AdminService } from '../services/admin.service';
import { ExceLoginService } from '../../login/exce-login/exce-login.service';
import { TranslateService } from '@ngx-translate/core';
import { ExceMessageService } from '../../../shared/components/exce-message/exce-message.service';
import { DataTableFilterPipe } from '../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CommonUITypes } from '../../../shared/enums/us-enum';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { element } from 'protractor';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

const now = new Date();

@Component({
  selector: 'app-manage-jobs',
  templateUrl: './manage-jobs.component.html',
  styleUrls: ['./manage-jobs.component.scss']
})
export class ManageJobsComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private _user: string;
  private uiTypes = CommonUITypes;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private branchId: number;
  extFieldList?: any[] = [];
  selectedJobItem: any;
  isEdit: boolean;
  FieldTypeCheck: number;
  FieldTypeCal: number;
  FieldTypeDes: number;
  FieldTypeNT: number;
  Title: any;
  jobCategoryForm: FormGroup;
  extendedFields?: any[] = [];
  extendedFieldForm: FormGroup;
  filteredJobItems = [];
  deletedItemId: any;
  deletedItem: any;
  delMsgBxModal: any;
  jobItems = [];
  jobItemsCount = 0;
  addJobCategoryView: any;
  editJobCategoryView: any;
  myFormSubmited = false;
  jobFormSubmited = false;
  rowTooltip: any;

  formErrors = {
    'Title': '',
    'CategoryName': ''
  };
  validationMessages = {
    'Title': {
      'required': 'ADMIN.FieldNameRequired'
    },
    'CategoryName': {
      'required': 'ADMIN.TaskCategoryNameRequired'
    }
  };

  constructor(
    private loginService: ExceLoginService,
    private fb: FormBuilder,
    private modalService: UsbModal,
    private exceBreadcrumbService: ExceBreadcrumbService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.JobCategoriesC',
      url: '/adminstrator/manage-jobs'
    });

    this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      this.yesHandler();
    });

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this._user = this.loginService.CurrentUser.username;

    this.jobCategoryForm = fb.group({
      'CategoryName': [null],
      'IsRole': [null],
      'IsEmployee': [true]
    });

    this.extendedFieldForm = fb.group({
      'Title': [null],
      'FieldType': [null]
    });
  }

  keys(): Array<string> {
    const key = Object.keys(this.uiTypes);
    return key.slice(key.length / 2);
  }

  ngOnInit() {
    this.extendedFieldForm.statusChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(_ => {
      UsErrorService.onValueChanged(this.extendedFieldForm, this.formErrors, this.validationMessages)
    });

    this.jobCategoryForm.statusChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(_ => {
      UsErrorService.onValueChanged(this.jobCategoryForm, this.formErrors, this.validationMessages)
    });
    this.getExtFieldsByTaskCategory();
  }

  // open new job category form
  openaddJobCategoryView(content: any) {
    this.Title = 'ADMIN.AddJobCategoryTitle';
    this.extendedFields = [];
    this.addJobCategoryView = this.modalService.open(content, { width: '990' });
  }

  // when edit and row double click
  openEditJobCategory(content: any, item: any) {
    this.selectedJobItem = item;
    this.Title = 'ADMIN.EditJobCategoryTitle';
    this.extendedFields = [];
    this.isEdit = true;
    this.jobCategoryForm.patchValue({
      'CategoryName': item.Name,
      'IsRole': item.IsRole,
      'IsEmployee': item.IsEmployee
    });

    // get extended fields for the data table
    this.adminService.getExtFieldsByCategory(item.Id, 'JOB').pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result) {
          result.Data.forEach(element => {
            if (element.FieldType == 0) {
              this.FieldTypeNT = 0;
            }
            if (element.FieldType == 1) {
              this.FieldTypeDes = 1;
            }
            if (element.FieldType == 2) {
              this.FieldTypeCal = 2;
            }
            if (element.FieldType == 3) {
              this.FieldTypeCheck = 3;
            }
          });
          this.extendedFields = result.Data;
        }
      });
    this.editJobCategoryView = this.modalService.open(content, { width: '990' });
  }

  getExtFieldsByTaskCategory() {
    this.adminService.GetJobCategories(this.branchId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.jobItems = result.Data;
        this.filteredJobItems = this.jobItems;
        this.jobItemsCount = this.jobItems.length;
      }
    });
  }

  deleteJob(item) {
    this.deletedItem = item;
    this.deletedItemId = item.Id;
    if (item) {
      let messageTitle = '';
      let messageBody = '';
      this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue);
      this.translateService.get('ADMIN.ConfirmMessage').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this.delMsgBxModal = this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: messageTitle,
          messageBody: messageBody
        });
    }
  }

  // delete yes handler
  yesHandler() {
    const index: number = this.jobItems.indexOf(this.deletedItem);
    this.adminService.DeleteJobCategory(this.deletedItemId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (index !== -1) {
        this.jobItems.splice(index, 1);
      }
    }, null, null);
  }

  // search job categories by name
  filterInputByName(param) {
    this.filteredJobItems = this.filterpipe.transform('Name', 'NAMEFILTER', this.jobItems, param);
  }

  // to add extended fields
  addExtendedFields(value) {
    if (value.FieldType == 'NORMALTEXT') {
      value.FieldType = 0;
    } else if (value.FieldType == 'DESCRIPTION') {
      value.FieldType = 1;
    } else if (value.FieldType == 'CALENDAR') {
      value.FieldType = 2;
    } else if (value.FieldType == 'CHECKBOX') {
      value.FieldType = 3;
    }

    if (value.Title !== null) {
      value.Title = value.Title.trim();
    }
    if (value.Title == null || value.Title == '') {
      this.extendedFieldForm.controls['Title'].setErrors({ 'required': true });
    } else {
      this.extendedFieldForm.controls['Title'].setErrors(null);
    }

    // when click add button fill the tabel not to the database;
    this.myFormSubmited = true;
    if (this.extendedFieldForm.valid) {
      const newField = {
        'Title': value.Title,
        'FieldType': value.FieldType
      }
      this.extendedFields.push(newField);
      this.extendedFieldForm.reset();
    } else {
      UsErrorService.validateAllFormFields(this.extendedFieldForm, this.formErrors, this.validationMessages);
    }
  }

  // delete fields from the data table
  deleteField(field) {
    this.extendedFields.splice(this.extendedFields.indexOf(field), 1);
  }

  saveJobCategory(value) {
    if (value.CategoryName !== null) {
      value.CategoryName = value.CategoryName.trim();
    }
    if (value.CategoryName == null || value.CategoryName == '') {
      this.jobCategoryForm.controls['CategoryName'].setErrors({ 'required': true });
    } else {
      this.jobCategoryForm.controls['CategoryName'].setErrors(null);
    }

    this.extendedFields.forEach(itm => {
      if (itm.FieldType == 0) {
        itm.FieldType = 'NORMALTEXT';
      } else if (itm.FieldType == 1) {
        itm.FieldType = 'DESCRIPTION';
      } else if (itm.FieldType == 2) {
        itm.FieldType = 'CALENDAR';
      } else if (itm.FieldType == 2) {
        itm.FieldType = 'CHECKBOX';
      }
      const extFldList = {
        Title: itm.Title,
        FieldType: itm.FieldType,
        CreatedUser: this._user,
        LastModifiedUser: this._user
      }
      this.extFieldList.push(extFldList);
    });
    const jobCat = {
      Name: value.CategoryName,
      Id: (this.selectedJobItem == undefined) ? -1 : this.selectedJobItem.Id,
      IsRole: (value.IsRole == null) ? false : value.IsRole,
      IsEmployee: value.IsEmployee,
      ExtendedFieldsList: this.extFieldList
    }
    this.jobFormSubmited = true;
    if (this.jobCategoryForm.valid) {
      const jobCateObj = {
        jobCategory: jobCat,
        isEdit: (this.isEdit !== undefined) ? this.isEdit : false
      }
      this.adminService.SaveJobCategory(jobCateObj).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        this.getExtFieldsByTaskCategory();
        this.closeAddJobCategoryView();
        if (this.editJobCategoryView) {
          this.editJobCategoryView.close();
        }
        this.isEdit = false;
        this.extFieldList = [];
      });

    } else {
      UsErrorService.validateAllFormFields(this.jobCategoryForm, this.formErrors, this.validationMessages);
    }
  }

  closeAddJobCategoryView() {
    this.extendedFieldForm.reset();
    this.jobCategoryForm.reset();

    if (this.addJobCategoryView) {
      this.addJobCategoryView.close();
    }
    if (this.editJobCategoryView) {
      this.editJobCategoryView.close();
    }
  }

  // editJobCategory(event) {
  // }

  getFieldType(event) {
    // this.FieldType = event.target.value;
    // if (event.target.value == 'NORMALTEXT') {
    //   this.FieldType = 0;
    // } else if (event.target.value == 'DESCRIPTION') {
    //   this.FieldType = 1;
    // } else if (event.target.value == 'CALENDAR') {
    //   this.FieldType = 2;
    // } else if (event.target.value == 'CHECKBOX') {
    //   this.FieldType = 3;
    // }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.delMsgBxModal) {
      this.delMsgBxModal.unsubscribe();
    }
    if (this.editJobCategoryView) {
      this.editJobCategoryView.close();
    }
    if (this.addJobCategoryView) {
      this.addJobCategoryView.close();
    }
  }
}
