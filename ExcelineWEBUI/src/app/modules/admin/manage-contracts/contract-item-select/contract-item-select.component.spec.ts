import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractItemSelectComponent } from './contract-item-select.component';

describe('ContractItemSelectComponent', () => {
  let component: ContractItemSelectComponent;
  let fixture: ComponentFixture<ContractItemSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractItemSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractItemSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
