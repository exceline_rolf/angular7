
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../../membership/services/exce-member.service'
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from '../../services/admin.service'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { Dictionary } from 'lodash';
import { ISlimScrollOptions } from '../../../../shared/directives/us-scroll/classes/slimscroll-options.class';
import { SlimScrollEvent } from '../../../../shared/directives/us-scroll/classes/slimscroll-event.class';
import { UsScrollService } from '../../../../shared/directives/us-scroll/us-scroll.service';
import { UsTabService } from '../../../../shared/components/us-tab/us-tab.service';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { ExceToolbarComponent } from 'app/modules/common/exce-toolbar/exce-toolbar.component';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

const now = new Date();


@Component({
  selector: 'app-contract-template-home',
  templateUrl: './contract-template-home.component.html',
  styleUrls: ['./contract-template-home.component.scss']
})
export class ContractTemplateHomeComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId: number;
  private itemResource: any = 0;
  private branchResource: any = 0;
  private branchCount = 0;
  private filteredContracts: any;
  private obsVal  = false;
  private actvVal  = false;
  private allVal  = true;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private modalReference: any;
  private classTypeForm: any;
  private formSubmited = false;
  private currentColor: string;
  private classGroupCat = []
  private classLevelCat = []
  private isLocal = false
  private subModelReference: any;
  private selectedBranches = []
  private branchesList = []
  private kwResource: any = 0;
  private kw = [];
  private kwCount = 0;
  private kwModelReference: any;
  private selectedKw = []
  private kwList = []
  private selectedCat = []
  private CatList = []
  private CatModelReference: any;
  private cat = [];
  private catCount = 0;
  private catResource: any = 0;
  private catModellReference: any;
  private rowItem: any;
  private isAddNew = true;
  private CatModalReference: any;
  private model: any;
  private registerTemplateView: any;
  private contracts: any;
  private getContractsObj: any;
  private selectedItem: any;
  private isEdit: boolean;
  private searchText: string;
  private searchVal: string;
  private newPackageId: number;
  items = [];
  itemCount = 0;
  branches = [];
  activityCats: any;
  contractCats: any;
  activeVal = 0;
  searchTypeForm: any;
  branchFilterValue: any;
  contractFilterValue: any;
  activityFilterValue: any;

  names = {
    IntTemplateNumber: 'Nummer',
    PackageName: 'Navn',
    ContractTypeValue: 'Kategori',
    InStock: 'På lager',
    NumOfVisits: 'Klipp',
    NumOfInstallments: 'Order',
    NoOfMonths: 'Måneder',
    StartUpItemPrice: 'Varepris',
    ServicePrice: 'Månedspris',
    PackagePrice: 'Totalpris',
    EndDate: 'Salg til'
  }

  colNamesNo = {
    IntTemplateNumber: 'Nummer',
    PackageName: 'Navn',
    ContractTypeValue: 'Kategori',
    InStock: 'På lager',
    NumOfVisits: 'Klipp',
    NumOfInstallments: 'Order',
    NoOfMonths: 'Måneder',
    StartUpItemPrice: 'Varepris',
    ServicePrice: 'Månedspris',
    PackagePrice: 'Totalpris',
    EndDate: 'Salg til'
  }

  colNamesEn = {
    IntTemplateNumber: 'Number',
    PackageName: 'Name',
    ContractTypeValue: 'Category',
    InStock: 'Stock status',
    NumOfVisits: 'Punches',
    NumOfInstallments: 'Orders',
    NoOfMonths: 'Months',
    StartUpItemPrice: 'Item price',
    ServicePrice: 'Monthly price',
    PackagePrice: 'Total price',
    EndDate: 'Sale end'
  }
  @ViewChild('newTemplateModalView') newTemplateModalView: ElementRef;
  auotocompleteItems = [{ id: 'TEMPLATE NO', name: 'Template no :', isNumber: true }, { id: 'NAME', name: 'Name :', isNumber: false }]
  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };
  formErrors = {
    'Code': '',
    'Name': '',
    'ClassGroupId': '',
    'ClassLevelId': '',
    'TimeCategoryId': '',
    'Colour': '',
    'ObsoleteLevel': '',
    'TimeDuration': '',
    'MaxNoOfBookings': '',
    'HoursToBePaid': '',
    'Comment': ''
  };
  validationMessages = {
    'Code': {
      'required': 'Date is required.',
    },
    'Name': { 'required': 'Date is required.', },
    'ClassGroupId': { 'required': 'Date is required.', },
    'ClassLevelId': { 'required': 'Date is required.', },
    'TimeCategoryId': { 'required': 'Date is required.', },
    'Colour': { 'required': 'Date is required.', },
    'ObsoleteLevel': { 'required': 'Date is required.', },
    'TimeDuration': { 'required': 'Date is required.', },
    'MaxNoOfBookings': { 'required': 'Date is required.', },
    'Comment': { 'required': 'Date is required.', }
  };
  gridApi: any;
  templateDefs: any;
  lang: any;

  constructor(private exceMemberService: ExceMemberService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService,
    private exceBreadcrumbService: ExceBreadcrumbService,
    private exceToolbarService: ExceToolbarService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.ContractTemplatesC',
      url: '/adminstrator/contract-template-home'
    });

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.registerTemplateView) {
      this.registerTemplateView.close();
    }
  }

  ngOnInit() {
    this.lang = this.exceToolbarService.getSelectedLanguage()
    switch (this.lang.id) {
      case 'no':
        this.names = this.colNamesNo;
        break;
      case 'en':
        this.names = this.colNamesEn;
        break;
    }

    this.contractFilterValue =
    this.getContracts();
    this.getBranches();
    this.getActivities();
    this.getContCats();

    this.searchTypeForm = this.fb.group({
      'searchVal': [this.searchVal]
    });

    this.classTypeForm = this.fb.group({
      'Code': [null],
      'Name': [null, [Validators.required]],
      'ClassGroupId': [null],
      'ClassLevelId': [null],
      'TimeCategoryId': [null],
      'Colour': [null],
      'ObsoleteLevel': [null],
      'TimeDuration': [0],
      'HoursToBePaid': [0.00],
      'MaxNoOfBookings': [0],
      'Comment': [null]
    });


    this.templateDefs = [
      {headerName: this.names.IntTemplateNumber, field: 'IntTemplateNumber', width: 120, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, suppressSorting: true, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, rowDrag: true, pinned: 'left'},
      {headerName: this.names.PackageName, field: 'PackageName',  width: 300, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, suppressSorting: true, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, pinned: 'left'},
      {headerName: this.names.ContractTypeValue, field: 'ContractTypeValue', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        return params.data.ContractTypeValue.Name;
      }, suppressFilter: true,  suppressSorting: true},
      {headerName: this.names.InStock, field: 'InStock', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'},  width: 100, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, suppressFilter: true},
      {headerName: this.names.NumOfVisits, field: 'NumOfVisits', width: 120, suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, suppressFilter: true},
      {headerName: this.names.NumOfInstallments, field: 'NumOfInstallments', width: 120, suppressSorting: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, suppressFilter: true},
      {headerName: this.names.NoOfMonths, field: 'NoOfMonths', suppressSorting: true, width: 120, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, suppressFilter: true},
      {headerName: this.names.StartUpItemPrice, field: 'StartUpItemPrice', suppressSorting: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, suppressFilter: true, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return;
        }}
      },
      {headerName: this.names.ServicePrice, field: 'ServicePrice', suppressSorting: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, suppressFilter: true, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return;
        }}
      },
      {headerName: this.names.PackagePrice, field: 'PackagePrice', suppressSorting: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, suppressFilter: true, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return;
        }}
      },
      {headerName: this.names.EndDate, field: 'EndDate', width: 150, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, suppressSorting: true, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const d = params.value.split('T');
          const dDate = d[0].split('-');
          return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
        } else {
          return
        }
      }, suppressFilter: true},
    ];
  }

  rowDragEnded(event) {
    this.updatePriority()
  }


  onSelect(item: any, value: any) {
    this.searchText = item.searchText; // this.getSearchText(item.name);
    this.searchVal = item.searchVal;
    this.getContracts(item.searchVal, 0, item.id);
  }

  getContracts(searchText?: string, searchType?: number, searchCriteria?: string) {
    this.getContractsObj = {
      'searchCriteria': '',
      'branchId': this.branchId,
      'searchText': '',
      'searchType': 0
    };
    if (searchText) {
      this.getContractsObj.searchText = searchText
    }
    if (searchType) {
      this.getContractsObj.searchType = searchType
    }
    if (searchCriteria) {
      this.getContractsObj.searchCriteria = searchCriteria
    }
    this.adminService.GetContracts(this.getContractsObj).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.contracts = result.Data;
        this.items = this.contracts.then( this.filterInput(this.branchFilterValue, this.contractFilterValue, this.activityFilterValue))
        this.contracts.sort(this.GetSortOrder('SortingNo'));
        this.itemResource = new DataTableResource(this.contracts);
        this.itemResource.count().then(count => this.itemCount = count);
        this.itemCount = this.items.length;
      }
    })
  }

  getBranches() {
    this.adminService.getBranches(-1).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.branches = result.Data;
      }
    })
  }
  getActivities() {
    this.adminService.getCategoriesByType('ACTIVITY').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.activityCats = result.Data;
      }
    })
  }
  getContCats() {
    this.adminService.getCategoriesByType('CONTRACT').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.contractCats = result.Data;
      }
    })
  }

  GetSortOrder(prop) {
    return function (a, b) {
      if (a[prop] > b[prop]) {
        return 1;
      } else if (a[prop] < b[prop]) {
        return -1;
      }
      return 0;
    }
  }

  changeActiveVal(event) {
    if (event.target.checked) {
      this.activeVal = event.target.value;
      this.getContracts('', this.activeVal, '');
    }
  }

  newTemplateModal(content: any, isEdit: boolean) {
    this.isEdit = isEdit;
    this.registerTemplateView = this.modalService.open(content, {});
  }

  filterInput(branchValue: any, contractValue: any, activityValue: any) {
    this.branchFilterValue = branchValue;
    this.contractFilterValue = contractValue;
    this.activityFilterValue = activityValue;
    this.filteredContracts = this.contracts
    if (branchValue && branchValue !== '') {
      this.filteredContracts = this.filterpipe.transform('BranchId', 'SELECT', this.filteredContracts, branchValue);
    }
    if (contractValue && contractValue !== null) {
      this.filteredContracts = this.filterpipe.transform('ContractTypeId', 'SELECT', this.filteredContracts, contractValue);
    }
    if (activityValue && activityValue != null) {
      this.filteredContracts = this.filterpipe.transform('ActivityId', 'SELECT', this.filteredContracts, activityValue);
    }
    this.itemResource = new DataTableResource(this.filteredContracts);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredContracts);
  }

  rowDoubleClick(event, content) {
    this.selectedItem = event.data;
    this.newTemplateModal(this.newTemplateModalView, true)
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  updatePriority() {
    const priorityList = this.SetSortingValues()
    this.adminService.updatePriority(priorityList).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result.Data) {
        this.getContracts()
      }
    })
  }

  onGridReady(params) {
    this.gridApi = params.api;
    // this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    this.gridApi.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  SetSortingValues() {
    const sortingNo = 1;
    const priorityValues: Dictionary<number> = {};
    // for (const pack of this.contracts) {
    //   priorityValues[pack.PackageId + ''] = pack.SortingNo;
    // }
    this.gridApi.forEachNode( function(rowNode, index) {
      priorityValues[rowNode.data.PackageId + ''] = index
    })
    return priorityValues;
  }
}
