import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractTemplateHomeComponent } from './contract-template-home.component';

describe('ContractTemplateHomeComponent', () => {
  let component: ContractTemplateHomeComponent;
  let fixture: ComponentFixture<ContractTemplateHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractTemplateHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractTemplateHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
