export class ContractTemplateModel {
    StartUpItemList: any = [];
    EveryMonthItemList: any = [];
    NextTemplateId: number;
    NextContractTemplateName: string;
    ContractConditionId: number;
    ContractCondition: string;
    ContractTypeValue: any = [];
    MemberFeeArticle: any = [];
    BranchIdList: any = [];
    CountryIdList: any = [];
    RegionIdList: any = [];
    ActivityId: number;
    ActivityName: string;
    ArticleId: number;
    ContractActivity: any;
    BranchId: number;
    PackageCategory: any;
}
