import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewContractTemplateComponent } from './add-new-contract-template.component';

describe('AddNewContractTemplateComponent', () => {
  let component: AddNewContractTemplateComponent;
  let fixture: ComponentFixture<AddNewContractTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewContractTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewContractTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
