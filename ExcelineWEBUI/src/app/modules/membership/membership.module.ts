import { ShopModule } from '../shop/shop.module';
import { NgModule, Directive } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { MembershipRoutingModule } from './membership-routing.module';
import { MembershipRoutingComponents } from './membership-routing.module';
import { ExceMemberService } from './services/exce-member.service';
import { McBasicInfoService } from './member-card/mc-basic-info/mc-basic-info.service';
import { McContractsComponent } from './member-card/mc-contract/mc-contracts/mc-contracts.component';
import { McContractDetailsComponent } from './member-card/mc-contract/mc-contract-details/mc-contract-details.component';
import { AmountConverterPipe } from '../../shared/pipes/amount-converter.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { McPaymentsComponent } from './member-card/mc-payments/mc-payments.component';
import { McInfoWithNotesComponent } from './member-card/mc-info-with-notes/mc-info-with-notes.component';
import { McClassesComponent } from './member-card/mc-classes/mc-classes.component';
import { McXtraLogComponent } from './member-card/mc-xtra-log/mc-xtra-log.component';
import { McBookingComponent } from './member-card/mc-booking/mc-booking.component';
import { McEventLogComponent } from './member-card/mc-event-log/mc-event-log.component';
import { McCommunicationLogComponent } from './member-card/mc-communication-log/mc-communication-log.component';
import { MemberRegisterComponent } from 'app/modules/membership/member-register/member-register.component';
import { McInterestsComponent } from './member-card/mc-interests/mc-interests.component';
import { ExceCategoryService } from 'app/shared/components/exce-category/exce-category.service';
import { McContractRegComponent } from './member-card/mc-contract/mc-contract-reg/mc-contract-reg.component';
import { McSponsoringComponent } from './member-card/mc-sponsoring/mc-sponsoring.component';
import { ExceAddmemberService } from 'app/shared/components/exce-addmember/exce-addmember.service';
import { ExcePostalcodeService } from 'app/shared/components/exce-postalcode/exce-postalcode.service';
import { McVisitComponent } from './member-card/mc-visit/mc-visit.component';
import { McIntroducedMembersComponent } from './member-card/mc-introduced-members/mc-introduced-members.component';
import { McOrdersDetailsComponent } from './member-card/mc-orders/mc-orders-details/mc-orders-details.component';
import { McOrdersHomeComponent } from './member-card/mc-orders/mc-orders-home/mc-orders-home.component';
import { McDocumentsComponent } from './member-card/mc-documents/mc-documents.component';
import { McFollowUpComponent } from './member-card/mc-follow-up/mc-follow-up.component';
import { McResignComponent } from './member-card/mc-resign/mc-resign.component';
import { MemberCardService } from './member-card/member-card.service';
import { ResignContractComponent } from './member-card/mc-resign/resign-contract/resign-contract.component';
import { OrderDetailsViewComponent } from './member-card/mc-resign/resign-contract/order-details-view/order-details-view.component';
import { ExceAddFollowUpService } from 'app/shared/components/exce-followup/exce-add-follow-up.service';
import { ExceFollowupCommonService } from 'app/shared/components/exce-followup/exce-followup-common.service';
import { APP_BASE_HREF, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { McTrainingProgramComponent } from './member-card/mc-training-program/mc-training-program.component';
import { McFreezeHomeComponent } from './member-card/mc-freeze/mc-freeze-home/mc-freeze-home.component';
import { ExceContractTmpSelectComponent } from '../../shared/components/exce-contract-tmp-select/exce-contract-tmp-select.component';
import { FreezeComponent } from './member-card/mc-freeze/mc-freeze-home/freeze/freeze.component';
import { NewFreezeComponent } from './member-card/mc-freeze/mc-freeze-home/freeze/new-freeze/new-freeze.component';
import { McEconomyComponent } from './member-card/mc-economy/mc-economy.component';
import { McInvoicesComponent } from './member-card/mc-invoices/mc-invoices.component';
import { McShopComponent } from './member-card/mc-shop/mc-shop.component';
import { McShopAccountComponent } from './member-card/mc-shop/mc-shop-account/mc-shop-account.component';
import { FreezeDetailsComponent } from './member-card/mc-freeze/mc-freeze-home/freeze/freeze-details/freeze-details.component';
import { MemberShopService } from './services/member-shop.service';
import { SelectOrderlinesComponent } from './member-card/mc-resign/resign-contract/order-details-view/select-orderlines/select-orderlines.component';
import { IntlTelInputDirective } from 'app/shared/directives/intl-tel-input/intl-tel-input.directive';
import { IntlTelInputComponent } from 'app/shared/components/intl-tel-input/intl-tel-input/intl-tel-input.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { McDocumentComponent } from 'app/modules/membership/member-card/mc-document/mc-document.component';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { McContractTemplatesComponent } from './member-card/mc-contract/mc-contract-templates/mc-contract-templates.component';
import { McInvoiceDetailsComponent } from './member-card/mc-invoices/mc-invoice-details/mc-invoice-details.component';
import { McCreditNoteComponent } from './member-card/mc-credit-note/mc-credit-note.component';
import { ExceTitleService } from './services/exce-title.service';
import { McContractRenewComponent } from './member-card/mc-contract/mc-contract-renew/mc-contract-renew.component';
import { McContractMoveOrdersComponent } from './member-card/mc-contract/mc-contract-move-orders/mc-contract-move-orders.component';
import { McContractService } from './member-card/mc-contract/mc-contract.service';
import { CKEditorModule } from 'ngx-ckeditor';
import { AgGridModule } from 'ag-grid-angular';


export function translateLoader(http: HttpClient) {

  return new MultiTranslateHttpLoader(http, [
    { prefix: './assets/i18n/', suffix: '.json' },
    { prefix: './assets/i18n/modules/membership/', suffix: '.json' },
    { prefix: './assets/i18n/modules/shop/', suffix: '.json' }
  ]);
}

export class MultiTranslateHttpLoader implements TranslateLoader {

  constructor(private http: HttpClient,
    public resources: { prefix: string, suffix: string }[] = [
      {
        prefix: '/assets/i18n/',
        suffix: '.json'
      }
    ]) { }

  /**
   * Gets the translations from the server
   * @param lang
   * @returns {any}
   */
  public getTranslation(lang: string): any {

    return forkJoin(this.resources.map(config => {
      return this.http.get(`${config.prefix}${lang}${config.suffix}`);
    })).pipe(
      map(response => {
        return response.reduce((a, b) => {
          return Object.assign(a, b);
        });
      })
    )
  }
}


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MembershipRoutingModule,
    AgGridModule.withComponents(null),
    FormsModule,
    CKEditorModule,
    ReactiveFormsModule,
    ShopModule,
    NgbModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (translateLoader),
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    MembershipRoutingComponents,
    McContractsComponent,
    McContractDetailsComponent,
    McPaymentsComponent,
    McInfoWithNotesComponent,
    McClassesComponent,
    McXtraLogComponent,
    McEventLogComponent,
    McCommunicationLogComponent,
    McInterestsComponent,
    MemberRegisterComponent,
    McContractRegComponent,
    McSponsoringComponent,
    McVisitComponent,
    McOrdersDetailsComponent,
    McOrdersHomeComponent,
    McDocumentsComponent,
    McFollowUpComponent,
    McResignComponent,
    McIntroducedMembersComponent,
    McBookingComponent,
    ResignContractComponent,
    OrderDetailsViewComponent,
    McTrainingProgramComponent,
    McFreezeHomeComponent,
    FreezeComponent,
    NewFreezeComponent,
    McEconomyComponent,
    McInvoicesComponent,
    McShopComponent,
    McShopAccountComponent,
    FreezeDetailsComponent,
    SelectOrderlinesComponent,
    McDocumentComponent,
    McContractTemplatesComponent,
    McInvoiceDetailsComponent,
    McCreditNoteComponent,
    McContractRenewComponent,
    McContractMoveOrdersComponent
    // MemberNotificationDetailComponent
  ],
  providers: [
    McBasicInfoService,
    ExceCategoryService,
    ExceAddmemberService,
    MemberShopService,
    ExcePostalcodeService,
    MemberCardService,
    ExceAddFollowUpService,
    ExceFollowupCommonService,
    ExceTitleService,
    McContractService,
    DatePipe,
    AmountConverterPipe

  ],
  exports: [
    McFollowUpComponent
  ],
  entryComponents: [
    McContractsComponent,
    McContractDetailsComponent,
    McPaymentsComponent,
    McInfoWithNotesComponent,
    McClassesComponent,
    McXtraLogComponent,
    McEventLogComponent,
    McCommunicationLogComponent,
    McInterestsComponent,
    MemberRegisterComponent,
    McContractRegComponent,
    McSponsoringComponent,
    McVisitComponent,
    McOrdersDetailsComponent,
    McOrdersHomeComponent,
    McDocumentsComponent,
    McFollowUpComponent,
    McResignComponent,
    McIntroducedMembersComponent,
    McBookingComponent,
    ResignContractComponent,
    OrderDetailsViewComponent,
    McTrainingProgramComponent,
    ExceContractTmpSelectComponent,
    FreezeComponent,
    NewFreezeComponent,
    McFreezeHomeComponent,
    McEconomyComponent,
    McInvoicesComponent,
    McShopComponent,
    FreezeDetailsComponent
  ]

})
export class MembershipModule { }
