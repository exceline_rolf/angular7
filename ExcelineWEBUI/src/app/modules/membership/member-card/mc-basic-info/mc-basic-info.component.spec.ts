import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McBasicInfoComponent } from './mc-basic-info.component';

describe('McBasicInfoComponent', () => {
  let component: McBasicInfoComponent;
  let fixture: ComponentFixture<McBasicInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McBasicInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McBasicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
