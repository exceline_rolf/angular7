import { TestBed, inject } from '@angular/core/testing';

import { McBasicInfoService } from './mc-basic-info.service';

describe('McBasicInfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [McBasicInfoService]
    });
  });

  it('should be created', inject([McBasicInfoService], (service: McBasicInfoService) => {
    expect(service).toBeTruthy();
  }));
});
