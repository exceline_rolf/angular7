
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../services/exce-member.service'
import { McBasicInfoService } from './../mc-basic-info/mc-basic-info.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment/moment'
import { Subject } from 'rxjs';
import { takeUntil, mergeMap } from 'rxjs/operators';

const now = new Date();

@Component({
  selector: 'app-mc-event-log',
  templateUrl: './mc-event-log.component.html',
  styleUrls: ['./mc-event-log.component.scss']
})
export class McEventLogComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchID: number = JSON.parse(Cookie.get('selectedBranch')).BranchId
  private eventLogs: any;
  private filteredEventLogs: any;
  private itemResource: any = 0;
  private resources: any;
  private activityCategories: any;
  private resourceCategories: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private resourceValue: string;
  private employeeValue: string;
  private categoryValue: string;
  private modalReference?: any;
  private memberBookingForm: any;
  private otherMembers: any;
  private rowItem: any;
  private printBookingForm: any;
  private printResult: any;
  private rowColors: any;
  private selectedMember: any;
  private today: NgbDateStruct;
  private monthBefore: any;
  private fromdate: any;
  private toDate: any;
  private isButtonsVisible = true;
  private isNotificationDetail = true;

  public items = [];
  public dateForm: any;
  public itemCount = 0;
  evenId: any;
  eventTitle: any;
  eventDescription: any;
  eventCreatedDate: any;
  eventCategory: any;
  eventCreatedUser: any;

  constructor(
    private exceMemberService: ExceMemberService,
    private basicInfoService: McBasicInfoService,
    private modalService: UsbModal,
    private fb: FormBuilder,

  ) {
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.monthBefore = {
      day: 1,
      month: Number(moment().subtract(30, 'days').calendar().slice(0, 2)),
      year: Number(moment().subtract(30, 'days').calendar().slice(6, 10))
    }

    this.dateForm = this.fb.group({
      'fromDate': [{ date: this.monthBefore }],
      'toDate': [{ date: this.today }],
    });


  }

  ngOnInit() {
    this.basicInfoService.currentMember.pipe(
      mergeMap(currentMember => {
        this.selectedMember = currentMember;
        return this.exceMemberService.getNotificationByNotifiyMethod(
          this.selectedMember.Id,
          this.selectedMember.BranchId,
          (this.monthBefore.month + '/' + this.monthBefore.day + '/' + this.monthBefore.year),
          (this.today.month + '/' + this.today.day + '/' + this.today.year),
          'EVENTLOG'
        );
      }),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.eventLogs = result.Data;
        this.itemResource = new DataTableResource(this.eventLogs);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.eventLogs;
        this.itemCount = Number(result.Data.length);
      }
    });

    // this.basicInfoService.currentMember.mergeMap( currentMember => {
    //   this.selectedMember = currentMember;
    //   return this.exceMemberService.getNotificationByNotifiyMethod(this.selectedMember.Id,
    //     this.selectedMember.BranchId,
    //     (this.monthBefore.month + '/' + this.monthBefore.day + '/' + this.monthBefore.year),
    //     (this.today.month + '/' + this.today.day + '/' + this.today.year), 'EVENTLOG');
    // }).pipe(
    //   takeUntil(this.destroy$)
    // ).subscribe(
    //   result => {
    //     if (result) {
    //       this.eventLogs = result.Data;
    //       this.itemResource = new DataTableResource(this.eventLogs);
    //       this.itemResource.count().then(count => this.itemCount = count);
    //       this.items = this.eventLogs;
    //       this.itemCount = Number(result.Data.length);
    //     }
    //   });
/*
    this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
          this.exceMemberService.getNotificationByNotifiyMethod(this.selectedMember.Id,
            this.selectedMember.BranchId,
            (this.monthBefore.month + '/' + this.monthBefore.day + '/' + this.monthBefore.year),
            (this.today.month + '/' + this.today.day + '/' + this.today.year), 'EVENTLOG').pipe(takeUntil(this.destroy$)).subscribe(
            result => {
              if (result) {
                this.eventLogs = result.Data;
                this.itemResource = new DataTableResource(this.eventLogs);
                this.itemResource.count().then(count => this.itemCount = count);
                this.items = this.eventLogs;
                this.itemCount = Number(result.Data.length);

              }
            });
        }
      }); */
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  filterInput(categoryValue?: any, employeeValue?: any) {

    this.filteredEventLogs = this.eventLogs

    if (categoryValue) {
      this.filteredEventLogs = this.filterpipe.transform('Category', 'NAMEFILTER', this.filteredEventLogs, categoryValue);
    }
    if (employeeValue) {
      this.filteredEventLogs = this.filterpipe.transform('CreatedUser', 'NAMEFILTER', this.filteredEventLogs, employeeValue);
    }

    this.itemResource = new DataTableResource(this.filteredEventLogs);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredEventLogs);
    this.categoryValue = categoryValue;
    this.employeeValue = employeeValue;
  }


  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }


  openEventModel(event, content: any) {
    const e = event.row.item;
    this.evenId = e.Id;
    this.eventTitle = e.Title;
    this.eventDescription = e.Description;
    this.eventCreatedDate = e.CreatedDate;
    this.eventCategory = e.Category;
    this.eventCreatedUser = e.CreatedUser;
    this.modalReference = this.modalService.open(content, { width: '600' });
  }

  dateFilter(dateRange) {
    const fromDate = dateRange.fromDate.date.month + '/' + dateRange.fromDate.date.day + '/' + dateRange.fromDate.date.year
    const toDate = dateRange.toDate.date.month + '/' + dateRange.toDate.date.day + '/' + dateRange.toDate.date.year
    this.exceMemberService.getNotificationByNotifiyMethod(this.selectedMember.Id, this.selectedMember.BranchId,
      fromDate,
      toDate, 'EVENTLOG').pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.eventLogs = result.Data;
          this.itemResource = new DataTableResource(this.eventLogs);
          this.itemResource.count().then(count => this.itemCount = count);
          this.items = this.eventLogs;
          this.itemCount = Number(result.Data.length);
        }
      });
  }

  onDoneOrIgnore() {
    this.eventLogs = this.eventLogs.filter(item => item !== this.rowItem);
    this.filterResults();
  }

  filterResults() {
    this.filteredEventLogs = this.eventLogs
    if (this.categoryValue) {
      this.filteredEventLogs = this.filterpipe.transform('Category', 'NAMEFILTER', this.filteredEventLogs, this.categoryValue);
    }
    if (this.employeeValue) {
      this.filteredEventLogs = this.filterpipe.transform('CreatedUser', 'NAMEFILTER', this.filteredEventLogs, this.employeeValue);
    }
    this.itemResource = new DataTableResource(this.filteredEventLogs);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredEventLogs);
  }

}
