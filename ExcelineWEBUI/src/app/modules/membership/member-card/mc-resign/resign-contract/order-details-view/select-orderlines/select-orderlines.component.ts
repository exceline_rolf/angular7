import { PreloaderService } from '../../../../../../../shared/services/preloader.service';
import { ExceToolbarService } from '../../../../../../common/exce-toolbar/exce-toolbar.service';
import { AdminService } from '../../../../../../admin/services/admin.service';
import { ExceMemberService } from '../../../../../services/exce-member.service';
import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'select-orderlines',
  templateUrl: './select-orderlines.component.html',
  styleUrls: ['./select-orderlines.component.scss']
})
export class SelectOrderlinesComponent implements OnInit, OnDestroy {
  locale: string;
  articleCategories: any[];
  articleList: any[];

  orderDefs: any;
  defaultColDef = { editable: true };

  selectedCategory: any;
  selecteedCategoryType = 'ALL';
  keyWord: any;
  auotocompleteItems = [
    { id: 'ArticleNo', name: 'Article No :', isNumber: false },
    { id: 'Name', name: 'Name :', isNumber: false },
    { id: 'Barcode', name: 'Barcode :', isNumber: false },
    { id: 'Vendor', name: 'Vendor :', isNumber: false }
  ];
  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };
  selectionTypes: any = [{
    value: 'ITEM',
    Display: 'MEMBERSHIP.Items'
  }, {
    value: 'SERVICE',
    Display: 'MEMBERSHIP.Service'
  }];

  @Output() selectItemByDoubleClick = new EventEmitter<any>();
  @Output() selectMultipleArticles = new EventEmitter<any>();
  @Output() closeModel = new EventEmitter<string>();
  @Input() selectionType: string;
  gridApi: any;
  gridColumnApi: any;
  singleSelection = false;
  private destroy$ = new Subject<void>();
  constructor(
    private adminService: AdminService,
    private toolbarService: ExceToolbarService,
    private memberService: ExceMemberService
  ) {
    this.articleCategories = [];
    this.articleList = [];
    this.selectedCategory = 'ALL';
    this.selecteedCategoryType = 'ALL';
    this.keyWord = '';
  }

  ngOnInit() {
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
    this.loadArticles(this.selecteedCategoryType, (this.selectedCategory === 'ALL') ? null : this.selectedCategory, this.keyWord);
    if (this.selectionType) { // change this if ALL is added
      if (this.selectionType === 'ITEM') {
         this.selecteedCategoryType = 'ITEM';
         this.getArticleCategories('ITEM');
          this.loadArticles('ITEM', null, this.keyWord);
          this.selectionTypes.splice(1, 1);
      } else if (this.selectionType === 'SERVICE') {
         this.selecteedCategoryType = 'SERVICE';
         this.getArticleCategories('SERVICE');
          this.loadArticles('SERVICE', null, this.keyWord);
          this.selectionTypes.splice(1, 1);
      }
      this.singleSelection = true;
    } else {
      this.selectionType = 'ALL';
      this.loadArticles('ALL', null, this.keyWord);
    }

    this.orderDefs = [
      {headerName: '', field: '', width: 40, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, suppressFilter: true, singleClickEdit: false, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true},
      {headerName: 'Artikkelnummer', field: 'ArticleNo', width: 100, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, singleClickEdit: false, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: 'Artikkelnavn', field: 'Description', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, autoHeight: true},
    {headerName: 'Varepris', field: 'Defaultprice', suppressFilter: true, width: 100, suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
      if (params.value) {
        const val = Number(params.value)
        return val.toFixed(2);
      } else {
        return;
      }}},
    {headerName: 'Lagernivå', field: 'StockLevel', suppressFilter: true, width: 100, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, autoHeight: true},
    {headerName: 'Leverandør', field: 'VendorName', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, autoHeight: true},
    {headerName: 'Antall', field: 'Quantity', width: 100, suppressFilter: true, suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, autoHeight: true},
    ]
  }

  // cellRenderer: function(params) {
  //  return '<input type="number" value="' + params.value + '" class="form-control form-control-sm">';
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
  onGridReady(params, type) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    params.api.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  onSelectSearchItem(item: any) {
    this.keyWord = item.value;
    this.searchArticles()
  }

  searchArticles() {
    this.loadArticles(this.selecteedCategoryType, (this.selectedCategory === 'ALL') ? null : this.selectedCategory, this.keyWord);
  }

  selectRowDblClick(item) {
    this.selectItemByDoubleClick.emit(item);
  }

  orderlineModelClose() {
    this.closeModel.emit();
  }

  selectArticles() {
    const selectedArticles = this.gridApi.getSelectedRows();
    this.selectMultipleArticles.emit(selectedArticles);
  }

  changeCategory(category) {
    this.selectedCategory = category;
    let searchCat;
    this.articleCategories.forEach(cat => {
      if (cat.Code === category) {
        searchCat = cat;
      }
    });
    this.loadArticles(this.selecteedCategoryType, searchCat, this.keyWord)
  }

  getArticleCategories(type) {
    this.selecteedCategoryType = type;
    this.articleCategories = [];
    this.memberService.getCategories(type).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        this.articleCategories = result.Data;
      }
    );
    this.loadArticles(this.selecteedCategoryType, null, this.keyWord);
  }

  loadArticles(type, category, keyword) {
   // PreloaderService.showPreLoader();
    this.articleList = [];
    this.adminService.getArticles(type, keyword, category, -1, true, true).pipe(takeUntil(this.destroy$)).subscribe
      (
      res => {
        res.Data.forEach(element => {
          element.Quantity = 1;
        });
        this.articleList = res.Data;
       // PreloaderService.hidePreLoader();
      },
      err => {
        // PreloaderService.hidePreLoader();

      });
  }

}
