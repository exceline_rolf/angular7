import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResignContractComponent } from './resign-contract.component';

describe('ResignContractComponent', () => {
  let component: ResignContractComponent;
  let fixture: ComponentFixture<ResignContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResignContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResignContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
