import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { ExcePdfViewerService } from '../../../../../shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { UsErrorService } from '../../../../../shared/directives/us-error/us-error.service';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { PreloaderService } from '../../../../../shared/services/preloader.service';
import { ConfigService } from '@ngx-config/core';
import { ExceToolbarService } from '../../../../common/exce-toolbar/exce-toolbar.service';
import { ExceLoginService } from '../../../../login/exce-login/exce-login.service';
import { ExceMemberService } from '../../../services/exce-member.service';
import { McBasicInfoService } from '../../mc-basic-info/mc-basic-info.service';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { UsbModal } from '../../../../../shared/components/us-modal/us-modal';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forEach } from '@angular/router/src/utils/collection';
import { TranslateService } from '@ngx-translate/core';
import { MemberRole } from 'app/shared/enums/us-enum';
import { AdminService } from '../../../../../modules/admin/services/admin.service';
import { Subject } from 'rxjs';
import { takeUntil, mergeMap, retry } from 'rxjs/operators';

@Component({
  selector: 'resign-contract',
  templateUrl: './resign-contract.component.html',
  styleUrls: ['./resign-contract.component.scss']
})

export class ResignContractComponent implements OnInit, OnDestroy {
  translated: any = {};
  proceedText: string;
  intoOneOrderText: string;
  allSelectedText: string;
  isUpdated: any;
  resignatioinSaved: boolean;
  selectedMeargeInstallment: any;
  updatedInstallment: any = [];
  mergingOrder: any[];
  limit: any;
  params: any;
  installmentCount: number;
  private installmentsResource: DataTableResource<any>;
  deletedOrdersIds: any = [];
  deletedInstallemts: any = [];
  selectedInstallment: any;
  enableOrderOperations: boolean;
  enableSave: boolean;
  resignFormSubmited: boolean;
  cmModel: any;
  resignDetails: {};
  mobile: any;
  email: any;
  selectedResignCategory: any;
  tempCommunicationMethods: any[];
  communicationChannel: any;
  communicationMethods: any[] = [];
  resignContract: FormGroup;
  resignPeriod: any;
  selectedOrder: any;
  contractId: any;
  sub: any;
  installments: any;
  selectedMember: any;
  memSubs: any;
  locale: string;
  branchId: any;
  private modalReference?: any;
  public resignCategories: any[];
  private memberContract: any;
  private ordersDeleted = false;
  private destroy$ = new Subject<void>();
  isMemberFee: false;

  formErrors = {
    'ResignCategory': ''
  };

  validationMessages = {
    'ResignCategory': {
      'required': 'MEMBERSHIP.selectResignCategory'
    },
  };
  deleteDisabled = true
  hasInstallments = false;
  IsBrisIntegrated: any;
  gridApi: any;
  orderDefs: any;

  @ViewChild('orderDetails') orderDetails: ElementRef;

  names = {
    MemberContractNo: 'Kontrakt',
    No: 'Ordrenummer',
    Periode: 'Periode',
    Duedate: 'Forfall',
    ActivityPrice: 'Aktivitetspris',
    AdonPrice: 'Tilleggspris',
    Amount: 'Ordrebeløp',
    Content: 'Innhold',
    Type: 'Type',
    Status: 'Status'
  }

  colNamesNo = {
    MemberContractNo: 'Kontrakt',
    No: 'Ordrenummer',
    Periode: 'Periode',
    Duedate: 'Forfall',
    ActivityPrice: 'Aktivitetspris',
    AdonPrice: 'Tilleggspris',
    Content: 'Innhold',
    Amount: 'Ordrebeløp',
    Type: 'Type',
    Status: 'Status'
  }

  colNamesEn = {
    MemberContractNo: 'Contract',
    No: 'Order no',
    Periode: 'Periode',
    Duedate: 'Due date',
    ActivityPrice: 'Activity price',
    AdonPrice: 'Addon price',
    Amount: 'Order amount',
    Type: 'Type',
    Status: 'Status'
  }
  endDate: any;
  message: string;
  type: string;
  isShowInfo: boolean;
  rowClassRules: any;

  constructor(
    private basicInfoService: McBasicInfoService,
    private memberService: ExceMemberService,
    private router: Router,
    private route: ActivatedRoute,
    private exceLoginService: ExceLoginService,
    private toolbarService: ExceToolbarService,
    private config: ConfigService,
    private modalService: UsbModal,
    private exceMessageService: ExceMessageService,
    private excePdfViewerService: ExcePdfViewerService,
    private adminService: AdminService,
    private translate: TranslateService,
    private fb: FormBuilder
  ) {
    this.resignContract = this.fb.group({
      ResignDate: [null],
      ResignCategory: [null, [Validators.required]],
      ContractEndDate: [null],
      LockInPeriodUntilDate: [{ value: null, disabled: true }],
      MemberFeeArticleName: [null]
    });

    this.resignContract.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(__ => {
      UsErrorService.onValueChanged(this.resignContract, this.formErrors, this.validationMessages)
    });

    this.resignFormSubmited = false;
    this.enableOrderOperations = true;
    this.enableSave = false;

    this.installmentsResource = new DataTableResource(this.installments);
    this.installments = [];
    this.resignatioinSaved = false;
    this.isUpdated = false;

    translate.get([
      'MEMBERSHIP.AllSelected',
      'MEMBERSHIP.IntoOneOrder',
      'MEMBERSHIP.Proceed',
      'MEMBERSHIP.willDeletedAreUSure',
      'MEMBERSHIP.Allordersfromorderno'
    ]).pipe(takeUntil(this.destroy$)).subscribe((res: string) => {
      this.translated = res;
    });

  }

  rowColors(item) {
    if (item.IsLocked) {
      return '#F44336';
    } else {
      return '#8BC34A';
    }
  }

  ngOnInit() {
    this.adminService.GetGymCompanySettings('other').pipe(takeUntil(this.destroy$)).subscribe(result => {
      this.IsBrisIntegrated = result.Data.IsBrisIntegrated;
    });
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );

    this.fetchSettingsData(this.branchId);

    // this.resignPeriod = Number(this.basicInfoService.getGymSettings().EcoNoOfOrdersResigning);
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';
    this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
    this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      if (value.id === 'CONFIRM_ResignConformMsg') {
        this.resignFromContract(this.communicationChannel);
      } else if (value.id === 'CONFIRM_MsgMemberFeeNotRemoved') {
        this.openCommChannels(this.communicationChannel);
      } else if (value.id === 'CONFIRM_DELETE_INS') {
        this.deleteInstallment();
      } else if (value.id === 'MEARGE_INSTALLMENT') {
        this.meargeInstallments();
      } else if (value.id === 'CANCEL_RESIGN') {
        this.cancelResignYes();
      }
    });

    this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
          // member.IntCustId = member.CustId; Does not seem to do anything.
        }
      }
    );

    this.memberService.getCategories('CONTRACTRESIGN').pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.resignCategories = result.Data;
        }
      }
    );

    this.memberService.getCategories('NOTIFYMETHOD').pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.communicationMethods.push({
            Code: 'PRINT',
            Name: 'COMMON.Print'
          });
          this.communicationMethods = result.Data;
          this.communicationMethods = this.communicationMethods.filter(item => item.Code !== 'POPUP' || item.Code !== 'NONE')
        }
      }
    );

    this.route.params.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.contractId = params.ContractId;
      this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
        member => {
          if (member) {
            this.selectedMember = member;
            if (member.GuardianId > 0) {
              this.mobile = member.GuardianMobile;
              this.email = member.GuardianEmail;
            } else {
              this.mobile = member.Mobile;
              this.email = member.Email;
            }
            this.memberService.getContractDetails(this.contractId, this.selectedMember.BranchId).pipe(takeUntil(this.destroy$)).subscribe(
              result => {
                this.memberContract = result.Data;
                this.memberContract.ResignDate = new Date();
                if (this.memberContract.ContractEndDate) {
                  this.memberContract.ContractEndDate = new Date(this.memberContract.ContractEndDate);
                }
                if (this.memberContract.LockInPeriodUntilDate) {
                  this.memberContract.LockInPeriodUntilDate = new Date(this.memberContract.LockInPeriodUntilDate);
                  this.resignContract.patchValue({

                    LockInPeriodUntilDate: {
                      date: {
                        year: this.memberContract.LockInPeriodUntilDate.getFullYear(),
                        month: this.memberContract.LockInPeriodUntilDate.getMonth() + 1,
                        day: this.memberContract.LockInPeriodUntilDate.getDate()
                      }
                    },
                  });

                }

                this.resignContract.patchValue({
                  ResignDate: {
                    date: {
                      year: this.memberContract.ResignDate.getFullYear(),
                      month: this.memberContract.ResignDate.getMonth() + 1,
                      day: this.memberContract.ResignDate.getDate()
                    }
                  },
                  ContractEndDate: {
                    date: {
                      year: this.memberContract.ResignDate.getFullYear(), // this.memberContract.ContractEndDate.getFullYear(),
                      month: this.memberContract.ResignDate.getMonth() + 1 + this.resignPeriod, // this.memberContract.ContractEndDate.getMonth() + 1,
                      day: this.memberContract.ResignDate.getDate() // this.memberContract.ContractEndDate.getDate()
                    }
                  },
                  // ResignCategory: this.memberContract.ResignCategory,
                  MemberFeeArticleName: this.memberContract.MemberFeeArticleName
                });
                this.loadInstallment();

              });

          }
        });
    });

    this.fetchSettingsData(this.branchId);
    this.orderDefs = [
      {headerName: '', field: 'Check', width: 50, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true,
      checkboxSelection: function(params) {
        if (params.data.IsLocked) {
          return false;
        } else {
          return true;
        }
      },
      },
      {headerName: this.names.No, field: 'InstallmentNo', sortable: false, width: 50, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.MemberContractNo, field: 'MemberContractNo', width: 90, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, autoHeight: true, sortable: false},
      // {headerName: this.names.No, field: 'InstallmentNo', width: 90, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      //   filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      //     sortAscending: '<i class="icon-sort-ascending"/>',
      //     sortDescending: '<i class="icon-sort-descending"/>'
      // }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.Periode, field: 'Text', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, sortable: false},
      {headerName: this.names.Duedate, field: 'DueDate', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const d = params.value.split('T');
          const dDate = d[0].split('-');
          return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
        } else {
          return
        }}, sortable: false},
      {headerName: this.names.ActivityPrice, field: 'ActivityPrice', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return;
        }}, sortable: false},
      {headerName: this.names.AdonPrice, field: 'AdonPrice', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return '0.00';
        }}, sortable: false},
        {headerName: this.names.Amount, field: 'Amount', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
        }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
          if (params.value) {
            const val = Number(params.value)
            return val.toFixed(2);
          } else {
            return;
          }}, sortable: false},
      // {headerName: '', field: 'Betaling', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      // filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
      //   sortAscending: '<i class="icon-sort-ascending"/>',
      //   sortDescending: '<i class="icon-sort-descending"/>'
      // }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
      //   if (params.data.IsOrderOptionsEnabled) {
      //       return '<button class="btn btn-primary btn-icon-min" title="Betaling"><span class="icon-checkout"></span></button>';
      //     } else {
      //       return '<button disabled class="btn btn-primary btn-icon-min" title="Betaling"><span class="icon-checkout"></span></button>';
      //     }
      //   }
      // },
      // {headerName: '', field: 'SMS/EPOST', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      // filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
      //   sortAscending: '<i class="icon-sort-ascending"/>',
      //   sortDescending: '<i class="icon-sort-descending"/>'
      // }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
      //   if (params.data.IsOrderOptionsEnabled) {
      //       return '<button class="btn btn-primary btn-icon-min" title="SMS/Epost faktura"><span class="icon-sms"></span></button>';
      //     } else {
      //       return '<button disabled class="btn btn-primary btn-icon-min" title="SMS/Epost faktura"><span class="icon-sms"></span></button>';
      //     }
      //   }
      // },
      // {headerName: '', field: 'Faktura', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      // filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
      //   sortAscending: '<i class="icon-sort-ascending"/>',
      //   sortDescending: '<i class="icon-sort-descending"/>'
      // }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
      //   if (params.data.IsOrderOptionsEnabled) {
      //       return '<button class="btn btn-primary btn-icon-min" title="Opprett faktura"><span class="icon-invoice"></span></button>';
      //     } else {
      //       return '<button disabled class="btn btn-primary btn-icon-min" title="Opprett faktura"><span class="icon-invoice"></span></button>';
      //     }
      //   }
      // },
      {headerName: '', field: 'Detaljer', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 80, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        return '<button class="btn btn-primary btn-icon-min" title="Ordredetaljer"><span class="icon-detail"></span></button>';
        }, sortable: false
      }
    ];


    this.rowClassRules = {
      'node-isLocked' : (params) => {
        return params.data.IsLocked;
      },
      // 'node-selected' : (params) => {
      //   if (!params.data.IsLocked && params.node.selected) {
      //     return true;
      //   }
      // },
      // 'node-notLocked' : (params) => {
      //   if (!params.data.IsLocked && !params.node.selected) {
      //     return true;
      //   }
      // }
  };

  }

  onGridReady(params) {
    this.gridApi = params.api;
    // auto fits columns to fit screen
    this.gridApi.sizeColumnsToFit();
    // hiding loading message on load

    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();

    //  params.api.gridCore.gridOptions.getRowStyle = function(p) {
    //   if (p.data.IsLocked) {
    //     return { background: 'rgb(211, 10, 34)' }
    //   } else if (p.data.IsLocked === false && p.node.selected === true) {
    //   } else {
    //     return { background: 'rgb(94, 165, 101)' }
    //   }
    // }
  }

  categorySelected(event) {
    this.enableSave = true;
  }

  fetchSettingsData(branch) {
    this.adminService.getGymSettings('ECONOMY', null, branch).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.isMemberFee = result.Data[0].IsMemberFee;
          this.resignPeriod = result.Data[0].NoOfOrdersWhenResign;
        }
      }
    )
  }

  private loadInstallment(): void {
    PreloaderService.showPreLoader();
    this.memberService.getInstallments(this.selectedMember.BranchId, this.contractId).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        if (res) {
          const installments = res.Data;
          if (installments) {
            this.hasInstallments = true;
          }
          installments.forEach(ins => {
            if (ins.InstallmentType !== 'MF') {
              if (ins.TrainingPeriodStart < this.memberContract.ContractEndDate) {
                ins.IsLocked = true;
              } else {
                ins.IsLocked = false;
              }
            }
          });
          if (this.IsBrisIntegrated) {
            this.setBRISContractEndDate(this.memberContract, installments);
          } else {
            this.setContractEndDate(this.memberContract, installments);
          }
        }
        PreloaderService.hidePreLoader();
      },
      err => {
        PreloaderService.hidePreLoader();
      });
  }


  setContractEndDate(memberContract: any, installments): any {
    installments.forEach(element => {
    });
    if (installments.length > 0) {
      // List < InstallmentDC > installments =  memberContract.InstalllmentList.OrderBy(x => x.InstallmentNo).ToList();
      installments = _.sortBy(installments, function (dateObj) {
        return new Date(dateObj.InstallmentNo);
      });

      // InstallmentDC memberFeeOrder = installments.FirstOrDefault(X => X.InstallmentType == "MF");
      // if (memberFeeOrder != null) {
      //   installments.Remove(memberFeeOrder);
      //   installments.Insert(0, memberFeeOrder);
      // }

      // const memberFeeOrder = _.findIndex(installments, { InstallmentType: 'MF' });
      // if (memberFeeOrder) {
      //   installments.splice(0, 0, installments.splice(memberFeeOrder, 1)[0]);
      // }

      // int resignPeriod = viewModel.GymSetting.ResignPeriod;
      // int remaininOrderCount = installments.Count;
      // if (resignPeriod > remaininOrderCount)
      //     resignPeriod = remaininOrderCount;

      const remaininOrderCount = installments.length;
      if (this.resignPeriod > remaininOrderCount) {
        this.resignPeriod = remaininOrderCount;
      }
      if (this.resignPeriod > 0) {
        const testInstallments = installments.filter( x => new Date(x.TrainingPeriodEnd) >= new Date());
        const lastOrderInResignPeriod = testInstallments[(this.resignPeriod === 0) ? 0 : (this.resignPeriod - 1)];
        if (new Date(memberContract.LockInPeriodUntilDate) > new Date(lastOrderInResignPeriod.TrainingPeriodEnd)) {
          let newEndDate = new Date(memberContract.LockInPeriodUntilDate);
          memberContract.ContractEndDate = new Date(memberContract.LockInPeriodUntilDate);
          testInstallments.forEach(ins => {
            if (ins.InstallmentType !== 'MF') {
              if (new Date(ins.TrainingPeriodStart) < new Date(memberContract.ContractEndDate)) {
                ins.IsLocked = true;
              } else {
                ins.IsLocked = false;
              }
              if (
                (
                  (new Date(ins.TrainingPeriodStart) < new Date(memberContract.ContractEndDate))
                  &&  (new Date(ins.TrainingPeriodEnd) > new Date(memberContract.ContractEndDate))
                )) {
                newEndDate = new Date(ins.TrainingPeriodEnd)
              }
            }
          });
          memberContract.ContractEndDate = newEndDate;
          this.resignContract.patchValue({ ContractEndDate: {
            date: {
              year: this.memberContract.ContractEndDate.getFullYear(),
              month: this.memberContract.ContractEndDate.getMonth() + 1,
              day: this.memberContract.ContractEndDate.getDate()
              }
            }
          });
          this.memberContract.InstalllmentList = this.installments;
        } else {
          memberContract.ContractEndDate = new Date(testInstallments[(this.resignPeriod === 0 ? 0 : (this.resignPeriod - 1)) ].TrainingPeriodEnd);
          this.resignContract.patchValue({ ContractEndDate: {
            date: {
              year: this.memberContract.ContractEndDate.getFullYear(),
              month: this.memberContract.ContractEndDate.getMonth() + 1,
              day: this.memberContract.ContractEndDate.getDate()
              }
            }
          });
          testInstallments.forEach(ins => {
            if (ins.InstallmentType !== 'MF') {
              if (new Date(ins.TrainingPeriodStart) < new Date(memberContract.ContractEndDate)) {
                ins.IsLocked = true;
              } else {
                ins.IsLocked = false;
              }
            }
          });
        }
        installments.forEach(element => {
          testInstallments.forEach(element2 => {
            if (element.Id === element2.Id && element2.IsLocked === true) {
              element.IsLocked = true;
            }
          });
          if (new Date(element.TrainingPeriodEnd) < new Date()) {
            element.IsLocked = true;
          }
        });
        this.installments = installments;
        // this.installmentsResource = new DataTableResource(this.installments);
        // this.installmentsResource.count().then(count => this.installmentCount = count);
        // this.limit = this.installments.length;
        // this.reloadInstallments(this.params);

      } else {
        memberContract.ContractEndDate = new Date();
        this.installments = installments;
        // this.installmentsResource = new DataTableResource(this.installments);
        // this.installmentsResource.count().then(count => this.installmentCount = count);
        // this.limit = this.installments.length;
        // this.reloadInstallments(this.params);
        // this.rowColors = this.rowColors.bind(this);
      }
      // memberContract.InstalllmentList =
      //   new System.Collections.ObjectModel.ObservableCollection<InstallmentDC>(installments);
      // viewModel.SelectedContractCopy = viewModel.SelectedContract.Clone();
    } else {
      // viewModel.SelectedContractCopy = viewModel.SelectedContract.Clone();
    }
  }

  setBRISContractEndDate(memberContract: any, installments): any {
    if (installments.length > 0) {
      installments = _.sortBy(installments, function (dateObj) {
        return new Date(dateObj.TrainingPeriodStart);
      });

      const memberFeeOrder = _.findIndex(installments, { InstallmentType: 'MF' });
      if (memberFeeOrder) {
        installments.splice(0, 0, installments.splice(memberFeeOrder, 1)[0]);
      }

      const resignPeriod = Number(this.basicInfoService.getGymSettings().EcoNoOfOrdersResigning);
      if (memberContract.LockInPeriodUntilDate) {
        const today = new Date();
        if (new Date(memberContract.LockInPeriodUntilDate) > today) {
          const LockInPeriodUntilDate = new Date(memberContract.LockInPeriodUntilDate);
          // get the last day of next month
          memberContract.ContractEndDate = new Date(LockInPeriodUntilDate.getFullYear(), LockInPeriodUntilDate.getMonth() + 1, 0);

          installments.forEach(ins => {
            if (ins.InstallmentType !== 'MF') {
              if (new Date(ins.TrainingPeriodStart) < new Date(memberContract.ContractEndDate)) {
                ins.IsLocked = true;
              } else {
                ins.IsLocked = false;
              }
            }
          });
        } else {
          today.setMonth(today.getMonth() + resignPeriod);
          // get the last day of next month
          const lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);
          memberContract.ContractEndDate = lastDay;
          installments.forEach(ins => {
            if (ins.InstallmentType !== 'MF') {
              if (new Date(ins.TrainingPeriodStart) < new Date(memberContract.ContractEndDate)) {
                ins.IsLocked = true;
              } else {
                ins.IsLocked = false;
              }
            }
          });
        }
      } else {
        // get the last day of next month
        const today = new Date();
        today.setMonth(today.getMonth() + resignPeriod);
        const lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);
        memberContract.ContractEndDate = lastDay;
        installments.forEach(ins => {
          if (ins.InstallmentType !== 'MF') {
            if (new Date(ins.TrainingPeriodStart) < new Date(memberContract.ContractEndDate)) {
              ins.IsLocked = true;
            } else {
              ins.IsLocked = false;
            }
          }
        });

      }
      this.installments = installments;
      this.installmentsResource = new DataTableResource(this.installments);
      this.installmentsResource.count().then(count => this.installmentCount = count);
      this.limit = this.installments.length;
      // this.reloadInstallments(this.params);
      this.rowColors = this.rowColors.bind(this);
    }
  }

  onResignCatChange(value) {
  }

  openOrderDetails(content, selectedOrder) {
    const dueDate = new Date(selectedOrder.DueDate);
    this.selectedOrder = selectedOrder;
    this.selectedOrder.DueDateFormated = { year: dueDate.getFullYear(), month: dueDate.getMonth() + 1, day: dueDate.getDate() };

    const traningStart = new Date(selectedOrder.TrainingPeriodStart);
    this.selectedOrder.TrainingPeriodStart = { year: traningStart.getFullYear(), month: traningStart.getMonth() + 1, day: traningStart.getDate() };
    const traningEnd = new Date(selectedOrder.TrainingPeriodEnd);
    this.selectedOrder.TrainingPeriodEnd = { year: traningEnd.getFullYear(), month: traningEnd.getMonth() + 1, day: traningEnd.getDate() };
    const installmentDate = new Date(selectedOrder.InstallmentDate);
    this.selectedOrder.InstallmentDate = { year: installmentDate.getFullYear(), month: installmentDate.getMonth() + 1, day: installmentDate.getDate() };
    const originalDueDate = new Date(selectedOrder.OriginalDueDate);
    this.selectedOrder.OriginalDueDate = { year: originalDueDate.getFullYear(), month: originalDueDate.getMonth() + 1, day: originalDueDate.getDate() };
    const estimatedInvoiceDate = new Date(selectedOrder.EstimatedInvoiceDate);
    this.selectedOrder.EstimatedInvoiceDate = { year: estimatedInvoiceDate.getFullYear(), month: estimatedInvoiceDate.getMonth() + 1, day: estimatedInvoiceDate.getDate() };

    this.modalReference = this.modalService.open(content);
  }

  // reloadInstallments(params) {
  //   if (this.installments.length > 0) {
  //     this.installmentsResource.query(params).then(installments => {
  //       this.installments = installments
  //     }
  //     );
  //   }
  // }


  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();

    if (this.cmModel) {
      this.cmModel.close();
    }
    if (this.modalReference) {
      this.modalReference.close();
    }
  }

  closeModelRef() {
    this.modalReference.close();
  }

  orderUpdateSuccess() {
    this.loadInstallment();
    this.modalReference.close();
  }

  saveResination(communicationChannel) {
    this.communicationChannel = communicationChannel;
    if (!this.ordersDeleted && this.memberContract.InstalllmentList.length > 1) {

      this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.ResignConformMsg',
          msgBoxId: 'CONFIRM_ResignConformMsg'
        });
    } else {
      this.resignFromContract(communicationChannel);
    }
  }

  private resignFromContract(communicationChannel): void {
    this.tempCommunicationMethods =
      this.communicationMethods.filter(x => (x.Code === 'PRINT' || x.Code === 'SMS' || x.Code === 'EMAIL' || x.Code === 'EVENTLOG') && x.CategoryTypeCode === 'NOTIFYMETHOD');
    this.selectedResignCategory = this.tempCommunicationMethods[0];
    if (this.memberContract.MemberFeeArticleID > 0) {
      this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.MsgMemberFeeNotRemoved',
          msgBoxId: 'CONFIRM_MsgMemberFeeNotRemoved'
        });


    } else {
      this.openCommChannels(communicationChannel);
    }
  }

  openCommChannels(communicationChannel) {
    this.resignFormSubmited = true;
    if (this.resignContract.valid) {
      this.cmModel = this.modalService.open(communicationChannel, { width: '250' });
    } else {
      UsErrorService.validateAllFormFields(this.resignContract, this.formErrors, this.validationMessages);
    }

  }

  cmModelClose() {
    this.cmModel.close();
  }

  onCMChange(item) {
    this.selectedResignCategory = item
  }

  private saveResignContract(): void {
    let senderDescription = this.mobile;
    if (this.selectedResignCategory.Code === 'SMS') {
      senderDescription = this.mobile;
    } else if (this.selectedResignCategory.Code === 'EMAIL') {
      senderDescription = this.email;
    }
    if (this.selectedResignCategory != null) {
      const date = this.resignContract.value.ContractEndDate.date;
      const enddate = new Date(date.year, date.month - 1, date.day, 12)
      this.memberContract.ContractEndDate = enddate;

      const rdate = this.resignContract.value.ResignDate.date;
      const renddate = new Date(rdate.year, rdate.month - 1, rdate.day, 12)
      this.memberContract.ResignDate = renddate;

      if (this.memberContract.LockInPeriod > 0 && this.memberContract.LockInPeriodUntilDate) {
        const lockDate = new Date(this.memberContract.LockInPeriodUntilDate.getFullYear(),
        this.memberContract.LockInPeriodUntilDate.getMonth(), this.memberContract.LockInPeriodUntilDate.getDate(), 12, 0)
        this.memberContract.LockInPeriodUntilDate = lockDate;
      }
      PreloaderService.showPreLoader();
      this.resignDetails = {
        ResignDetail: {
          ResignCategoryId: this.resignContract.value.ResignCategory,
          MemberId: this.memberContract.MemberId,
          MemberContractNo: this.memberContract.MemberContractNo,
          ContractEndDate: this.memberContract.ContractEndDate,
          LockInPeriod: this.memberContract.LockInPeriod,
          LockUntillDate: this.memberContract.LockInPeriodUntilDate,
          MemberContractId: this.memberContract.Id,
          ResignDate: this.memberContract.ResignDate,
          RemainingInstallments: this.installments,
          MemberFeeArticleID: this.memberContract.MemberFeeArticleID,
          DeletedOrders: this.deletedOrdersIds,
        },
        SenderDescription: senderDescription,
        NotificationMethod: this.selectedResignCategory.Code
      };

      this.memberService.ResignContract(this.resignDetails).pipe(takeUntil(this.destroy$)).subscribe(
        res => {
          PreloaderService.hidePreLoader();
          this.cmModel.close();
          if (res.Data) {
            // if (res.Data === 'PRINTERROR') {
            //   this.exceMessageService.openMessageBox('ERROR',
            //     {
            //       messageTitle: 'Exceline',
            //       messageBody: 'MEMBERSHIP.DocumentFailed'
            //     });
            // } else {
            //   if (this.selectedResignCategory.Code === 'PRINT') {
            //     this.excePdfViewerService.openPdfViewer('PDF Viewer', 'http://www.pdf995.com/samples/pdf.pdf');
            //   }
            // }
            this.resignatioinSaved = true;
            // this.isOrderOptionsEnabled = true;
            this.enableSave = false;
            this.enableOrderOperations = false;
            this.deleteDisabled = true
            this.hasInstallments = false;
            this.router.navigate(['/membership/card/' + this.selectedMember.Id + '/' + this.selectedMember.BranchId + '/' + MemberRole[this.selectedMember.Role]
             + '/contracts/' + this.memberContract.Id])
              this.translate.get('MEMBERSHIP.ResignSuccess').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.message = translatedValue);
              this.type = 'SUCCESS';
              this.isShowInfo = true;
              setTimeout(() => {
                this.isShowInfo = false;
              }, 4000);
          }
        },
        err => {
          this.cmModel.close();
            this.translate.get('MEMBERSHIP.errorInSaveResignation').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.message = translatedValue);
            this.type = 'DANGER';
            this.isShowInfo = true;
            setTimeout(() => {
              this.isShowInfo = false;
            }, 4000);
          PreloaderService.hidePreLoader();
        });

    } else {

    }
  }


  ordersSelected(event) {
    if (event.data.IsSelected === false) {
    this.installments.forEach(element => {
      if (element.InstallmentNo >= event.data.InstallmentNo) {
        element.IsSelected =  true;
      }
    });
    this.gridApi.forEachNode( function(node) {
      if ( node.data.IsSelected === true) {
        node.selected = true;
      } else { node.selected = false; }
    })
    this.gridApi.redrawRows();
  }else {
    this.installments.forEach(element => {
      if (element.InstallmentNo >= event.data.InstallmentNo) {
        element.IsSelected = false;
      }
    });
    this.gridApi.forEachNode( function(node) {
      if ( node.data.IsSelected === true) {
        node.selected = true;
      } else { node.selected = false; }
    })
    this.gridApi.redrawRows();
  }
    console.log(event)
    // this.gridApi.redrawRows({
    //   rowNodes: [this.orderDefs]
    // }

    // this.gridApi.gridCore.gridOptions.getRowStyle = function(p) {
    //   if (p.node.selected) {
    //     console.log("selected")
    //     return { background: 'rgb(94, 129, 186)' }
    //   } else {
    //     console.log("not")
    //     if (p.data.IsLocked) {
    //       return { background: 'rgb(211, 10, 34)' }
    //     } else {
    //       return { background: 'rgb(94, 165, 101)' }
    //     }
    //   }
    // }


    // // if (event.node.data.IsLocked) {
    // //   return;
    // // }
    // // this.gridApi.deselectAll();
    // // let disable = true;
    // // const row = event;

    // // this.gridApi.forEachNode( function(node) {
    // //   // node.selected = true;
    // //   if (node.rowIndex >= row.node.rowIndex && !node.data.IsLocked) {
    // //     node.setSelected(true);
    // //   }
    // //   // node.setSelected(true);
    // //   if (node.selected) {
    // //     node.setDataValue('IsSelected', true);
    // //     disable = false;
    // //   } else {
    // //     node.setDataValue('IsSelected', false)
    // //   }
    // // })
    // // this.deleteDisabled = disable;

    // this.selectedInstallment = item;
  }

  // deleteOrders() {
  //   if (this.selectedInstallment != null) {
  //     if (!this.selectedInstallment.IsLocked) {
  //       this.exceMessageService.openMessageBox('CONFIRM',
  //         {
  //           messageTitle: 'Exceline',
  //           messageBody: this.translated['MEMBERSHIP.Allordersfromorderno'] + ' ' +
  //             this.selectedInstallment.InstallmentNo + ' ' + this.translated['MEMBERSHIP.willDeletedAreUSure'],
  //           msgBoxId: 'CONFIRM_DELETE_INS'
  //         });
  //     } else {
  //       this.exceMessageService.openMessageBox('WARNING',
  //         {
  //           messageTitle: 'Exceline',
  //           messageBody: 'MEMBERSHIP.orderscannotbeDeleted'
  //         });
  //     }
  //   } else {
  //     this.exceMessageService.openMessageBox('WARNING',
  //         {
  //           messageTitle: 'Exceline',
  //           messageBody: 'MEMBERSHIP.ChooseOrder'
  //         });
  //   }
  // }

  cellClickedOrder(event) {
    const order = event.data;
    const column = event.column.colDef.field;
    switch (column) {
      // case 'Betaling':
      //   // this.openShop(this.shopHomeModel, order)
      // break;
      // case 'SMS/EPOST':
      // // this.SMSInvoiceEvent(order, this.editTemplateModal1)
      // break;
      // case 'Faktura':
      // // this.generateInvoice(order)
      // break;
      case 'Detaljer':
      this.openOrderDetails(this.orderDetails, order)
      break;
      default:
      break;
    }
  }

  deleteInstallment() {
    const deleteIdlist = [];
    const deleteInstList = [];
    const newList = [];
    this.gridApi.forEachNode( function(node) {
      const installment = node.data;
      if (node.selected) {
        const id = node.data.Id;
        deleteIdlist.push(id);
        deleteInstList.push(installment);
      } else {
        newList.push(installment)
      }
    })
    if (deleteIdlist.length === 0 || deleteInstList.length === 0) {
      this.translate.get('MEMBERSHIP.NoOrdersSelected').pipe(takeUntil(this.destroy$)).subscribe(trans => this.message = trans);
      this.type = 'DANGER';
      this.isShowInfo = true;
      setTimeout(() => {
        this.isShowInfo = false;
      }, 4000);
      return;
    }
    this.deletedOrdersIds = deleteIdlist;
    this.deletedInstallemts = deleteInstList;
    this.installments = newList;

    this.gridApi.setRowData(newList)

    this.enableSave = true;
    // this.installments.forEach(installment => {
    //   if (this.selectedInstallment && this.selectedInstallment.InstallmentNo <= installment.InstallmentNo) {
    //     this.deletedOrdersIds.push(installment.Id);
    //     this.deletedInstallemts.push(installment);
    //   }
    // });
    // this.installments = this.installments.filter(x => x.Id < this.selectedInstallment.Id );
    this.ordersDeleted = true;
    this.isUpdated = true;
  }

  meargeOrders(): void {
    this.enableSave = true;
    this.mergingOrder = [];
    if (this.memberContract) {
      if (this.selectedInstallment) {
        // mergingOrder = SelectedContract.InstalllmentList.Where(x => x.InstallmentNo <= startOrder.InstallmentNo).ToList();
        this.mergingOrder = this.installments.filter(x => x.InstallmentNo <= this.selectedInstallment.InstallmentNo);
      } else {
        this.mergingOrder = this.installments.filter(x => x.IsLocked === true);
      }
      // List < InstallmentDC > deletedInstallemts = new List<InstallmentDC>();

      if (this.mergingOrder.length > 1) {
        this.selectedMeargeInstallment = this.mergingOrder[0];
        this.exceMessageService.openMessageBox('CONFIRM',
          {
            messageTitle: 'Exceline',
            messageBody: this.translated['MEMBERSHIP.AllSelected'] + this.mergingOrder.length +
              this.translated['MEMBERSHIP.IntoOneOrder'] + ' ' + this.selectedMeargeInstallment.InstallmentNo + '. ' +
              this.translated['MEMBERSHIP.Proceed'],
            msgBoxId: 'MEARGE_INSTALLMENT'
          });

      }
    }
  }

  private meargeInstallments() {
    this.isUpdated = true;
    this.mergingOrder.forEach((order, index) => {
      if (index > 0) {
        this.deletedOrdersIds.push(order.Id);
        this.selectedMeargeInstallment.Amount += order.Amount;
        this.selectedMeargeInstallment.Balance += order.Balance;
        this.selectedMeargeInstallment.ServiceAmount += order.ServiceAmount;
        this.selectedMeargeInstallment.ItemAmount += order.ItemAmount;
        this.selectedMeargeInstallment.ActivityPrice += order.ActivityPrice;
        this.selectedMeargeInstallment.AdonPrice += order.AdonPrice;
        this.selectedMeargeInstallment.EstimatedOrderAmount += order.EstimatedOrderAmount;
        if (index === this.mergingOrder.length - 1) {
          this.selectedMeargeInstallment.TrainingPeriodEnd = order.TrainingPeriodEnd;
          this.selectedMeargeInstallment.Text = (new Date(this.selectedMeargeInstallment.TrainingPeriodStart)).toLocaleDateString() + ' - '
            + (new Date(order.TrainingPeriodEnd)).toLocaleDateString();
        }

        order.AddOnList.forEach(adon => {
          const samilarAdon = this.selectedMeargeInstallment.AddOnList.filter(x => x.ArticleId === adon.ArticleId)[0];
          if (samilarAdon) {
            samilarAdon.Price += adon.Price;
          } else {
            this.selectedMeargeInstallment.AddOnList.push(adon);
          }
        });

        this.deletedInstallemts.push(order);
      }
    });

    this.updatedInstallment.push(this.selectedMeargeInstallment);
    this.deletedInstallemts.forEach((deletedOrder, index) => {
      this.installments = this.installments.filter(x => x.Id !== deletedOrder.Id)
    });


    this.installmentsResource = new DataTableResource(this.installments);
    this.installmentsResource.count().then(count => this.installmentCount = count);
    this.limit = this.installments.length;

  }

  cancelResign() {
    if (!this.resignatioinSaved) {
      if (this.isUpdated) {
        this.exceMessageService.openMessageBox('CONFIRM',
          {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.MsgDoYouWantToExit',
            msgBoxId: 'CANCEL_RESIGN'
          });
      } else {
        this.exit();
      }
    } else {
      this.exit();
    }
  }

  private cancelResignYes() {
    this.exit();
  }

  private exit() {
    this.router.navigate(['membership/card/' + this.selectedMember.Id + '/' + this.branchId + '/' + MemberRole[this.selectedMember.Role] + '/contracts/' + this.contractId]);

  }


}
