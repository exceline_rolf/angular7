import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McVisitComponent } from './mc-visit.component';

describe('McVisitComponent', () => {
  let component: McVisitComponent;
  let fixture: ComponentFixture<McVisitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McVisitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McVisitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
