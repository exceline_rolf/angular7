import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McOrdersDetailsComponent } from './mc-orders-details.component';

describe('McOrdersDetailsComponent', () => {
  let component: McOrdersDetailsComponent;
  let fixture: ComponentFixture<McOrdersDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McOrdersDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McOrdersDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
