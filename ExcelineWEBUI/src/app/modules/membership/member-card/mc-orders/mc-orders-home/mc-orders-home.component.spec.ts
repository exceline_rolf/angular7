import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McOrdersHomeComponent } from './mc-orders-home.component';

describe('McOrdersHomeComponent', () => {
  let component: McOrdersHomeComponent;
  let fixture: ComponentFixture<McOrdersHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McOrdersHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McOrdersHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
