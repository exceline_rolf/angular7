import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McFollowUpComponent } from './mc-follow-up.component';

describe('McFollowUpComponent', () => {
  let component: McFollowUpComponent;
  let fixture: ComponentFixture<McFollowUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McFollowUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McFollowUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
