import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McSponsoringComponent } from './mc-sponsoring.component';

describe('McSponsoringComponent', () => {
  let component: McSponsoringComponent;
  let fixture: ComponentFixture<McSponsoringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McSponsoringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McSponsoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
