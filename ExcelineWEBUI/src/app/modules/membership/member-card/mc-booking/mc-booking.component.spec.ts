import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McBookingComponent } from './mc-booking.component';

describe('McBookingComponent', () => {
  let component: McBookingComponent;
  let fixture: ComponentFixture<McBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
