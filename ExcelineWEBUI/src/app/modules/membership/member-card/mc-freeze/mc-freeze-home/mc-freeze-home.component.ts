import {ExceToolbarService} from '../../../../common/exce-toolbar/exce-toolbar.service';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { ExceMemberService } from '../../../services/exce-member.service'
import { McBasicInfoService } from '../../mc-basic-info/mc-basic-info.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil, mergeMap } from 'rxjs/operators';


const now = new Date();

@Component({
  selector: 'mc-freeze-home',
  templateUrl: './mc-freeze-home.component.html',
  styleUrls: ['./mc-freeze-home.component.scss']
})
export class McFreezeHomeComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private freeze: any;
  private itemResource: any = 0;
  private items = [];
  private itemCount = 0;
  private resources: any;
  private modalReference?: any;
  private rowItem: any;
  private selectedMember: any;
  private today: NgbDateStruct;
  private monthbefore: NgbDateStruct;
  private fromdate: any;
  private toDate: any;
  private selectedContract: number;
  private rowColors: any;

  selectedIndex = 1;
  locale: string;

  constructor(private exceMemberService: ExceMemberService,
    private basicInfoService: McBasicInfoService,
    private translateService: TranslateService,
    private toolbarService: ExceToolbarService

  ) {
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.monthbefore = { year: now.getFullYear(), month: now.getMonth(), day: now.getDate() };

  }

  ngOnInit() {

    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );

    this.basicInfoService.currentMember.pipe(
      mergeMap(currentMember => {
        this.selectedMember = currentMember;
        return this.exceMemberService.getMemberContractSummeries(this.selectedMember.Id, this.selectedMember.BranchId, true, true);
      }),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.freeze = result.Data;
        this.freeze.forEach(element => {
          if (element.EndDate) {
            element.endDateString = element.EndDate.split('T')[0].replace(new RegExp('-', 'g'), '/')
          }
          const atg = element.IsATG ? 'Yes' : 'No';
          let translatedVal = '';
          this.translateService.get('MEMBERSHIP.' + atg).pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => translatedVal = tranlstedValue);
          element.atgDisplay = translatedVal;
        });
        this.rowColors = this.getRowColors.bind(this);
        this.itemResource = new DataTableResource(this.freeze);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.freeze;
        this.itemCount = Number(result.Data.length);
      }
    })

    // this.basicInfoService.currentMember.mergeMap(currentMember => {
    //   this.selectedMember = currentMember;
    //   return this.exceMemberService.getMemberContractSummeries(this.selectedMember.Id, this.selectedMember.BranchId, true, true).pipe(
    //     takeUntil(this.destroy$)
    //   )
    // }).pipe(
    //   takeUntil(this.destroy$)
    //   ).subscribe(
    //   result => {
    //     if (result) {
    //       this.freeze = result.Data;
    //       this.freeze.forEach(element => {
    //         if (element.EndDate) {
    //           element.endDateString = element.EndDate.split('T')[0].replace(new RegExp('-', 'g'), '/')
    //         }
    //         const atg = element.IsATG ? 'Yes' : 'No';
    //         let translatedVal = '';
    //         this.translateService.get('MEMBERSHIP.' + atg).pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => translatedVal = tranlstedValue);
    //         element.atgDisplay = translatedVal;
    //       });
    //       this.rowColors = this.getRowColors.bind(this);
    //       this.itemResource = new DataTableResource(this.freeze);
    //       this.itemResource.count().then(count => this.itemCount = count);
    //       this.items = this.freeze;
    //       this.itemCount = Number(result.Data.length);

    //     }
    //   })

/*     this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
          this.exceMemberService.getMemberContractSummeries(this.selectedMember.Id, this.selectedMember.BranchId,
            true, true).pipe(takeUntil(this.destroy$)).subscribe(
            result => {
              if (result) {
                this.freeze = result.Data;
                this.freeze.forEach(element => {
                  if (element.EndDate) {
                    element.endDateString = element.EndDate.split('T')[0].replace(new RegExp('-', 'g'), '/')
                  }
                  const atg = element.IsATG ? 'Yes' : 'No';
                  let translatedVal = '';
                  this.translateService.get('MEMBERSHIP.' + atg).pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => translatedVal = tranlstedValue);
                  element.atgDisplay = translatedVal;
                });
                this.rowColors = this.getRowColors.bind(this);
                this.itemResource = new DataTableResource(this.freeze);
                this.itemResource.count().then(count => this.itemCount = count);
                this.items = this.freeze;
                this.itemCount = Number(result.Data.length);

              }
            })
        }
      }); */
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
  click() {
    this.selectedIndex = 2;
  }

  getRowColors(freezeList) {
    if (Date.parse(freezeList.endDateString) <= Date.parse(this.today.year + '/' + this.today.month + '/' + this.today.day)) {
      return 'rgb(255, 255, 197)';
    }

  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }


  rowDoubleClick(rowEvent) {
    this.rowItem = rowEvent.row.item;
    if (this.rowItem.ContractTypeCode === 'CONTRACT') {
      this.selectedContract = this.rowItem.Id
      if (this.selectedContract) {
        this.selectedIndex = 2;
      }
    }
  }

  rowClick(item) {
    this.rowItem = item;
    if (this.rowItem.ContractTypeCode === 'CONTRACT') {
      this.selectedContract = this.rowItem.Id
      if (this.selectedContract) {
        this.selectedIndex = 2;
      }
    }
  }

  cancel() {
    this.selectedIndex = 1;
  }

}
