import { ExceToolbarService } from './../../../../../common/exce-toolbar/exce-toolbar.service';

import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DataTableResource } from '../../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { UsbModal } from '../../../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../../../services/exce-member.service'
import { McBasicInfoService } from '../../../mc-basic-info/mc-basic-info.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ExceLoginService } from '../../../../../../modules/login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../../../../shared/components/exce-message/exce-message.service';
import * as moment from 'moment';
import { Extension } from 'app/shared/Utills/Extensions';
import { start } from 'repl';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil, mergeMap, concatMap } from 'rxjs/operators';
const now = new Date();

@Component({
  selector: 'app-freeze',
  templateUrl: './freeze.component.html',
  styleUrls: ['./freeze.component.scss']
})
export class FreezeComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private freezeItems: any;
  private itemResource: any = 0;
  private items = [];
  private itemCount = 0;
  private resources: any;
  private modalReference?: any;
  private rowItem: any;
  private selectedMember: any;
  private today: NgbDateStruct;
  private monthbefore: NgbDateStruct;
  private fromdate: any;
  private toDate: any;
  private selectedItem: any;
  private contract: any;
  private isEdit = false;
  private insItems: any;
  private insItemResource: any;
  private insItemsToFreeze: any;
  private insCount: any;
  private editType = '';
  private branch: any;
  private deletingItem: any;
  private model: any;

  public selectedIndex = 1;
  locale: string;
  type: string;
  isShowInfo: boolean;
   message: string;
  @Input() selectedContract: number;
  @Output() canceled = new EventEmitter();

  constructor(private exceMemberService: ExceMemberService,
    private basicInfoService: McBasicInfoService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService,
    private translateService: TranslateService,
    private toolbarService: ExceToolbarService

  ) {
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.monthbefore = { year: now.getFullYear(), month: now.getMonth(), day: now.getDate() };
    this.branch = this.exceLoginService.SelectedBranch;

    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branch = branch;
      }
    );
  }

  ngOnInit() {
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );

    this.basicInfoService.currentMember.pipe(
      concatMap(currentMember => {
        this.selectedMember = currentMember;
        return this.exceMemberService.getContractDetails(this.selectedContract, this.selectedMember.BranchId);
      }),
      mergeMap(contractDetail => {
        this.contract = contractDetail.Data;
        return this.exceMemberService.GetContractFreezeItems(this.selectedContract);
      }),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.freezeItems = result.Data;
        this.itemResource = new DataTableResource(this.freezeItems);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.freezeItems;
        this.items.forEach(element => {
          element.FromDate = Extension.setNoonTransform(element.FromDate);
          element.ToDate = Extension.setNoonTransform(element.ToDate);
          element.ContractEndDate = Extension.setNoonTransform(element.ContractEndDate);
          let translatedVal = '';
          if (element.IsExtended) {
            this.translateService.get('MEMBERSHIP.Yes').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => translatedVal = tranlstedValue);
          } else {
            this.translateService.get('MEMBERSHIP.No').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => translatedVal = tranlstedValue);
          }
          element.isExtended = translatedVal;
        });
        this.itemCount = Number(result.Data.length);
      }
    })

    // this.basicInfoService.currentMember.concatMap(currentMember => {
    //   this.selectedMember = currentMember;
    //   return this.exceMemberService.getContractDetails(this.selectedContract, this.selectedMember.BranchId).pipe(
    //     takeUntil(this.destroy$)
    //   );
    // }).mergeMap(contractDetail => {
    //   this.contract = contractDetail.Data;
    //   return this.exceMemberService.GetContractFreezeItems(this.selectedContract).pipe(
    //     takeUntil(this.destroy$)
    //   )
    // }).pipe(
    //   takeUntil(this.destroy$)
    // ).subscribe(result => {
    //   if (result) {
    //     this.freezeItems = result.Data;
    //     this.itemResource = new DataTableResource(this.freezeItems);
    //     this.itemResource.count().then(count => this.itemCount = count);
    //     this.items = this.freezeItems;
    //     this.items.forEach(element => {
    //       element.FromDate = Extension.setNoonTransform(element.FromDate);
    //       element.ToDate = Extension.setNoonTransform(element.ToDate);
    //       element.ContractEndDate = Extension.setNoonTransform(element.ContractEndDate);
    //       let translatedVal = '';
    //       if (element.IsExtended) {
    //         this.translateService.get('MEMBERSHIP.Yes').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => translatedVal = tranlstedValue);
    //       } else {
    //         this.translateService.get('MEMBERSHIP.No').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => translatedVal = tranlstedValue);
    //       }
    //       element.isExtended = translatedVal;
    //     });
    //     this.itemCount = Number(result.Data.length);
    //   }
    // });

/*     this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
          this.exceMemberService.getContractDetails(this.selectedContract, this.selectedMember.BranchId).pipe(takeUntil(this.destroy$)).subscribe(
            result => {
              if (result) {
                this.contract = result.Data;
              }
            })
          this.getFreezeContractItems()
        }
      }); */

      this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
        if (value.id === 'DELETE') {
          this.deleteContract();
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.modalReference) {
      this.modalReference.close();
    }
  }


  getFreezeContractItems() {
    this.exceMemberService.GetContractFreezeItems(this.selectedContract).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          this.freezeItems = result.Data;
          this.itemResource = new DataTableResource(this.freezeItems);
          this.itemResource.count().then(count => this.itemCount = count);
          this.items = this.freezeItems;
          this.items.forEach(element => {
            element.FromDate = Extension.setNoonTransform(element.FromDate);
            element.ToDate = Extension.setNoonTransform(element.ToDate);
            element.ContractEndDate = Extension.setNoonTransform(element.ContractEndDate);
            let translatedVal = '';
            if (element.IsExtended) {
              this.translateService.get('MEMBERSHIP.Yes').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => translatedVal = tranlstedValue);
            } else {
              this.translateService.get('MEMBERSHIP.No').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => translatedVal = tranlstedValue);
            }
            element.isExtended = translatedVal;
          });
          this.itemCount = Number(result.Data.length);
        } else {
        }
      }
      )
  }
  openNewContract() {
    this.editType = 'NEW'
    this.selectedIndex = 2;
  }



  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }


  editFreeze(rowEvent, view) {
    this.rowItem = rowEvent.row.item;
    this.selectedItem = this.rowItem
    this.getInstallmentstoFreeze()
    this.modalReference = this.modalService.open(view);
  }

  freezeDetail(item, view) {
    this.rowItem = item;
    this.selectedItem = this.rowItem
    this.getInstallmentstoFreeze()
    this.modalReference = this.modalService.open(view);
  }

  getInstallmentstoFreeze() {
      this.exceMemberService.GetContractFreezeInstallments(this.selectedItem.Id).pipe(takeUntil(this.destroy$)).subscribe( result => {
        if (result) {
          this.insItemsToFreeze = [];
          this.insItems = result.Data;
          this.insItemResource = new DataTableResource(this.insItems);
          this.insItemResource.count().then(count => this.itemCount = count);
          this.insItemsToFreeze = this.insItems;
          this.insCount = Number(result.Data.length);
        } else {

        }
      }
    )
  }

  unFreeze() {
    if (this.selectedItem) {
      this.editType = 'UNFREEZE'
      this.selectedIndex = 2;
    }
  }

  extendContract() {
    if (this.selectedItem) {
      this.editType = 'EXTEND'
      this.selectedIndex = 2;
    }
  }

  cancel() {
    this.canceled.emit();
  }

  openSelectChannelModal(modal, value) {

  }
  callSaveSuccess() {

    this.selectedIndex = 1;
    this.getFreezeContractItems();
  }

  callCancel() {
    this.selectedIndex = 1;
  }

  daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }

  dateAdd(startDate: any, count: any, interval: string): Date {
    return moment(startDate).add(count, interval).toDate()
  }

  getNumberOfDaysInMonth(year: number, month: number): number {
    // adding 1 because date is 0
    return moment(year + '-' + (month + 1), 'YYYY-MM').daysInMonth();
  }

  setDateBack(date) {
    const tempDate = moment(date).toDate();
    const daysInMonth = this.getNumberOfDaysInMonth(tempDate.getFullYear(), tempDate.getMonth());
    const isLastOfMonth = (tempDate.getDate() === daysInMonth);
    const helpDate = new Date(tempDate.getFullYear(), tempDate.getMonth() - this.deletingItem.ShiftCount, 1);
    const daysInMonthNewDate = this.getNumberOfDaysInMonth(helpDate.getFullYear(), helpDate.getMonth())
    let newDate;
    if (tempDate.getDate() > daysInMonthNewDate) {
      newDate = new Date(helpDate.getFullYear(), helpDate.getMonth(), daysInMonthNewDate)
    } else {
      if (isLastOfMonth) {
        newDate = new Date(helpDate.getFullYear(), helpDate.getMonth(), daysInMonthNewDate);
      } else {
        newDate = new Date(helpDate.getFullYear(), helpDate.getMonth(), tempDate.getDate()); // Removing +1
      }
    }
    return newDate;
  }


  private deleteContract() {
    let freezeedInstallments = [];
    const installmentsToChange = [];
    const freezeInstallments = [];
    let installmentsToReset = [];
    this.exceMemberService.GetContractFreezeInstallments(this.deletingItem.Id).pipe(
      mergeMap(freezeInstallmentsFromDb => {
        freezeedInstallments = freezeInstallmentsFromDb.Data;
        return this.exceMemberService.GetInstallmentsToFreeze(this.selectedContract);
      }),
      mergeMap(contractToFreezeFrom => {
        installmentsToReset = contractToFreezeFrom.Data;
        installmentsToReset.forEach(installment => {
          freezeedInstallments.forEach(freezedInstallment => {
            if (installment.TableId === freezedInstallment.InstallmentId) {
              installmentsToChange.push(installment);
            }
          });
        })
        installmentsToChange.forEach(element => {
          const NewTrainingPeriodStart = this.setDateBack(element.TrainingPeriodStart);
          const NewTrainingPeriodEnd = this.setDateBack(element.TrainingPeriodEnd);
          const NewInstallmentDueDate = this.dateAdd(element.DueDate, this.deletingItem.ShiftCount * -1, 'months');

          let estimatedInvDate = element.MemberCreditPeriod > 0 ? this.dateAdd(element.NewInstallmentDueDate,
            (element.MemberCreditPeriod * -1), 'days') : this.dateAdd(element.NewInstallmentDueDate, (element.CreditPeriod * -1), 'days');
          this.dateAdd(element.DueDate, this.deletingItem.ShiftCount * -1, 'months');
          estimatedInvDate = this.setDateBack(estimatedInvDate)
          const freezeIns = {
            InstallmentId: element.TableId,
            InstallmentDueDate: Extension.setNoonTransform(element.DueDate),
            NewInstallmentDueDate: Extension.setNoonTransform(NewInstallmentDueDate),
            TrainingPeriodStart: Extension.setNoonTransform(NewTrainingPeriodStart),
            TrainingPeriodEnd: Extension.setNoonTransform(NewTrainingPeriodEnd),
            InstallmentAmount: element.Amount,
            ExtendTrainingPeriodDays: this.deletingItem.FreezeDays,
            EstimatedInvoiceDate: element.MemberCreditPeriod > 0 ? this.dateAdd(element.NewInstallmentDueDate, element.MemberCreditPeriod * -1, 'days')
            :
              this.dateAdd(element.NewInstallmentDueDate, (element.CreditPeriod * -1), 'days')
          }
          freezeInstallments.push(freezeIns);
        });
        this.deletingItem.ActiveStatus = false;
        const FromDate = this.deletingItem.FromDate.split('T');
        const ToDate = this.deletingItem.ToDate.split('T');
        const contratEndDate = Extension.setNoonTransform(this.setDateBack(this.deletingItem.ContractEndDate));
        const freezeItem = {
          Id: this.deletingItem.Id,
          NotifyMethod: '',
          MemberId: this.selectedMember.Id,
          MemberContractId: this.contract.Id,
          CategoryId: this.deletingItem.CategoryId,
          FromDate: FromDate[0],
          ToDate: ToDate[0],
          Note: '',
          FreezeMonths: this.deletingItem.FreezeMonths,
          FreezeDays: this.deletingItem.FreezeDays,
          ShiftCount: this.deletingItem.ShiftCount,
          ShiftStartInstallmentId: 0,
          ContractEndDate:  contratEndDate,
          IsExtended: true,
          ActiveStatus: this.deletingItem.ActiveStatus,
          MemberContractNo: this.contract.MemberContractNo
        };
        const freezeDeletion = {
          'freezeItem': freezeItem,
          'freezeInstallments': freezeInstallments,
          'branchId': this.selectedMember.BranchId,
          'notifyMethod' : '',
          'senderDescription' : '',
          'notificationTitle': '',
          'gymName': this.branch.BranchName
        };
        return this.exceMemberService.ExtendContractFreezeItem(freezeDeletion);
      }),
      mergeMap(deleteSuccess => {
        if (deleteSuccess) {
          this.translateService.get('MEMBERSHIP.DeletionSuccessful').pipe(takeUntil(this.destroy$)).subscribe(value => this.message = value);
          this.type = 'SUCCESS';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
          this.basicInfoService.setUpdateOrderList('update-from-freeze-success');
          return this.exceMemberService.GetContractFreezeItems(this.selectedContract);
        } else {
            this.translateService.get('MEMBERSHIP.DeletionNotSuccessful').pipe(takeUntil(this.destroy$)).subscribe(value => this.message = value);
            this.type = 'DANGER';
            this.isShowInfo = true;
            setTimeout(() => {
              this.isShowInfo = false;
            }, 4000);
        }
      }),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.freezeItems = result.Data;
        this.itemResource = new DataTableResource(this.freezeItems);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.freezeItems;
        this.items.forEach(element => {
          element.FromDate = Extension.setNoonTransform(element.FromDate);
          element.ToDate = Extension.setNoonTransform(element.ToDate);
          element.ContractEndDate = Extension.setNoonTransform(element.ContractEndDate);
          let translatedVal = '';
          if (element.IsExtended) {
            this.translateService.get('MEMBERSHIP.Yes').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => translatedVal = tranlstedValue);
          } else {
            this.translateService.get('MEMBERSHIP.No').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => translatedVal = tranlstedValue);
          }
          element.isExtended = translatedVal;
        });
        this.itemCount = Number(result.Data.length);
      }
    });
  }


  // deleteContract() {
  //   console.log('old way')
  //   this.exceMemberService.GetContractFreezeInstallments(this.deletingItem.Id).pipe(takeUntil(this.destroy$)).subscribe(res => {
  //     if (res) {
  //       const freezedInstallments = res.Data;
  //       this.exceMemberService.GetInstallmentsToFreeze(this.selectedContract).pipe(takeUntil(this.destroy$)).subscribe(result => {
  //       if (result.Data) {
  //         const installmentsToChange = [];
  //         const freezeInstallments = [];
  //         let installmentsToReset = [];
  //         installmentsToReset = result.Data;
  //         installmentsToReset.forEach(installment => {
  //           freezedInstallments.forEach(freezedInstallment => {
  //             if (installment.TableId === freezedInstallment.InstallmentId) {
  //               installmentsToChange.push(installment);
  //             }
  //           });
  //         })
  //         installmentsToChange.forEach(element => {
  //           const NewTrainingPeriodStart = this.setDateBack(element.TrainingPeriodStart);
  //           const NewTrainingPeriodEnd = this.setDateBack(element.TrainingPeriodEnd);
  //           const NewInstallmentDueDate = this.dateAdd(element.DueDate, this.deletingItem.ShiftCount * -1, 'months');

  //           let estimatedInvDate = element.MemberCreditPeriod > 0 ? this.dateAdd(element.NewInstallmentDueDate,
  //             (element.MemberCreditPeriod * -1), 'days') : this.dateAdd(element.NewInstallmentDueDate, (element.CreditPeriod * -1), 'days');
  //           this.dateAdd(element.DueDate, this.deletingItem.ShiftCount * -1, 'months');
  //           estimatedInvDate = this.setDateBack(estimatedInvDate)
  //           const freezeIns = {
  //             InstallmentId: element.TableId,
  //             InstallmentDueDate: Extension.setNoonTransform(element.DueDate),
  //             NewInstallmentDueDate: Extension.setNoonTransform(NewInstallmentDueDate),
  //             TrainingPeriodStart: Extension.setNoonTransform(NewTrainingPeriodStart),
  //             TrainingPeriodEnd: Extension.setNoonTransform(NewTrainingPeriodEnd),
  //             InstallmentAmount: element.Amount,
  //             ExtendTrainingPeriodDays: this.deletingItem.FreezeDays,
  //             EstimatedInvoiceDate: element.MemberCreditPeriod > 0 ? this.dateAdd(element.NewInstallmentDueDate, element.MemberCreditPeriod * -1, 'days')
  //             :
  //               this.dateAdd(element.NewInstallmentDueDate, (element.CreditPeriod * -1), 'days')
  //           }
  //           freezeInstallments.push(freezeIns);
  //         });
  //         this.deletingItem.ActiveStatus = false;
  //         const FromDate = this.deletingItem.FromDate.split('T');
  //         const ToDate = this.deletingItem.ToDate.split('T');
  //         const contratEndDate = Extension.setNoonTransform(this.setDateBack(this.deletingItem.ContractEndDate));
  //         const freezeItem = {
  //           Id: this.deletingItem.Id,
  //           NotifyMethod: '',
  //           MemberId: this.selectedMember.Id,
  //           MemberContractId: this.contract.Id,
  //           CategoryId: this.deletingItem.CategoryId,
  //           FromDate: FromDate[0],
  //           ToDate: ToDate[0],
  //           Note: '',
  //           FreezeMonths: this.deletingItem.FreezeMonths,
  //           FreezeDays: this.deletingItem.FreezeDays,
  //           ShiftCount: this.deletingItem.ShiftCount,
  //           ShiftStartInstallmentId: 0,
  //           ContractEndDate:  contratEndDate,
  //           IsExtended: true,
  //           ActiveStatus: this.deletingItem.ActiveStatus,
  //           MemberContractNo: this.contract.MemberContractNo
  //         };
  //         const freezeDeletion = {
  //           'freezeItem': freezeItem,
  //           'freezeInstallments': freezeInstallments,
  //           'branchId': this.selectedMember.BranchId,
  //           'notifyMethod' : '',
  //           'senderDescription' : '',
  //           'notificationTitle': '',
  //           'gymName': this.branch.BranchName
  //         };

  //         this.exceMemberService.ExtendContractFreezeItem(freezeDeletion).pipe(takeUntil(this.destroy$)).subscribe(delteSuccess => {
  //               if (delteSuccess) {
  //                 this.getFreezeContractItems(); // reload freeze installments
  //                 this.translateService.get('MEMBERSHIP.DeletionSuccessful').pipe(takeUntil(this.destroy$)).subscribe(value => this.message = value);
  //                 this.type = 'SUCCESS';
  //                 this.isShowInfo = true;
  //                 setTimeout(() => {
  //                   this.isShowInfo = false;
  //                 }, 4000);
  //                 this.basicInfoService.setUpdateOrderList('update-from-freeze-success');
  //               }
  //           }, error => {
  //             this.translateService.get('MEMBERSHIP.DeletionNotSuccessful').pipe(takeUntil(this.destroy$)).subscribe(value => this.message = value);
  //             this.type = 'DANGER';
  //             this.isShowInfo = true;
  //             setTimeout(() => {
  //               this.isShowInfo = false;
  //             }, 4000);
  //           })
  //       }
  //     }, error => {

  //       });
  //     }
  //   }, error => {

  //     }
  //   );
  // }

  manipulateDateToDateTime(dateInput: any) {
    if (dateInput.date !== undefined) {
      const tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }

  deleteMessage(item) {
    this.deletingItem = item;
    this.openDeleteMessage('MEMBERSHIP.Confirm', 'MEMBERSHIP.DeleteItemConfirm')
  }



  openDeleteMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: title,
        messageBody: body,
        msgBoxId: 'DELETE'
      }
    );
  }

}
