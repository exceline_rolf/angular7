import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McFreezeHomeComponent } from './mc-freeze-home.component';

describe('McFreezeHomeComponent', () => {
  let component: McFreezeHomeComponent;
  let fixture: ComponentFixture<McFreezeHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McFreezeHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McFreezeHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
