import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McClassesComponent } from './mc-classes.component';

describe('McClassesComponent', () => {
  let component: McClassesComponent;
  let fixture: ComponentFixture<McClassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McClassesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
