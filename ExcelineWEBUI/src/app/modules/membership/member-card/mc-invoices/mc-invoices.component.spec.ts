import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McInvoicesComponent } from './mc-invoices.component';

describe('McInvoicesComponent', () => {
  let component: McInvoicesComponent;
  let fixture: ComponentFixture<McInvoicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McInvoicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
