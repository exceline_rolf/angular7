import { Component, OnInit, OnDestroy } from '@angular/core';
import { McBasicInfoService } from './../mc-basic-info/mc-basic-info.service';
import { ExceMemberService } from '../../services/exce-member.service'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil, mergeMap } from 'rxjs/operators';

const now = new Date();

@Component({
  selector: 'app-mc-introduced-members',
  templateUrl: './mc-introduced-members.component.html',
  styleUrls: ['./mc-introduced-members.component.scss']
})
export class McIntroducedMembersComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private selectedMember: any;
  private SelectedBranchId: number = JSON.parse(Cookie.get('selectedBranch')).BranchId;
  private csDateList = [];
  public itemCount = 0;
  private itemResource: DataTableResource<any>;
  private introducedMemberDetailForm: FormGroup;
  private editIntroducedMembers: any;
  private modalReference?: any;
  private model;
  private deletedItem: any;
  private memberId: number;
  private creditedDate: any;
  private creditedText: string;
  private isDelete = true;
  private editMemberId: number;
  private deletedItemId: any;
  private deletedItemCreditedDate: any;
  private deletedItemCreditedText: any;

  public introducedMembers = [];
  constructor(
    private basicInfoService: McBasicInfoService,
    private exceMemberService: ExceMemberService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private exceMessageService: ExceMessageService,
    private translateService: TranslateService
  ) {

    this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      this.yesHandler();
    });
    this.exceMessageService.noCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      this.noHandler();
    });

    this.introducedMemberDetailForm = fb.group({
      'memberId': [null],
      'CustId': [null], // CustId = Customer No
      'Name': [null],
      'IntroduceDate': [null],
      'ContractSignDate': [null],
      'CreditedDate': [null],
      'CreditedText': [null]
    });
  }

  ngOnInit() {
    // get the selected member
    this.basicInfoService.currentMember.pipe(
      mergeMap(currentMember => {
        this.selectedMember = currentMember;
        return this.exceMemberService.getIntroducedMembers(this.selectedMember.Id, this.SelectedBranchId);
      }),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.introducedMembers = result.Data;

        // default ContractSignDate value set ''
        for (const date of this.introducedMembers) {
          if (date.ContractSignDate === '1900-01-01T00:00:00') {
            date.ContractSignDate = '';
          }
        }
        // default daCreditedDatete value set ''
        for (const date of this.introducedMembers) {
          if (date.CreditedDate === '1900-01-01T00:00:00') {
            date.CreditedDate = '';
          }
        }
        this.itemResource = new DataTableResource(this.introducedMembers);
        this.itemResource.count().then(count => this.itemCount = count);
        this.itemCount = Number(result.Data.length);
      }
    })

    // this.basicInfoService.currentMember.mergeMap(currentMember => {
    //   this.selectedMember = currentMember;
    //   return this.exceMemberService.getIntroducedMembers(this.selectedMember.Id, this.SelectedBranchId).pipe(
    //     takeUntil(this.destroy$)
    //   )
    // }).pipe(takeUntil(this.destroy$)).subscribe(result => {
    //   if (result) {
    //     this.introducedMembers = result.Data;

    //     // default ContractSignDate value set ''
    //     for (const date of this.introducedMembers) {
    //       if (date.ContractSignDate === '1900-01-01T00:00:00') {
    //         date.ContractSignDate = '';
    //       }
    //     }
    //     // default daCreditedDatete value set ''
    //     for (const date of this.introducedMembers) {
    //       if (date.CreditedDate === '1900-01-01T00:00:00') {
    //         date.CreditedDate = '';
    //       }
    //     }
    //     this.itemResource = new DataTableResource(this.introducedMembers);
    //     this.itemResource.count().then(count => this.itemCount = count);
    //     this.itemCount = Number(result.Data.length);
    //   }
    // })


/*     this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
          // get the introduced members details for the data table
          this.exceMemberService.getIntroducedMembers(this.selectedMember.Id, this.SelectedBranchId).pipe(takeUntil(this.destroy$)).subscribe(result => {
              if (result) {
                this.introducedMembers = result.Data;

                // default ContractSignDate value set ''
                for (const date of this.introducedMembers) {
                  if (date.ContractSignDate === '1900-01-01T00:00:00') {
                    date.ContractSignDate = '';
                  }
                }
                // default daCreditedDatete value set ''
                for (const date of this.introducedMembers) {
                  if (date.CreditedDate === '1900-01-01T00:00:00') {
                    date.CreditedDate = '';
                  }
                }
                this.itemResource = new DataTableResource(this.introducedMembers);
                this.itemResource.count().then(count => this.itemCount = count);
                this.itemCount = Number(result.Data.length);
              }
            });
        }
      }); */
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  rowItem(rowItem) {
    this.editIntroducedMembers = rowItem;
    this.introducedMemberDetailForm.patchValue(this.editIntroducedMembers);
    const conDate = new Date(this.editIntroducedMembers.CreditedDate);
    this.introducedMemberDetailForm.patchValue({
      CreditedDate: {
        date: {
          year: conDate.getFullYear(),
          month: conDate.getMonth() + 1,
          day: conDate.getDate()
        }
      }
    });
  }

  // pop up the Introduced Member Details View
  openIntroducedMemberDetailsView(content: any) {
    this.modalReference = this.modalService.open(content, { width: '500' });
  }

  // row double click event
  rowDoubleClick(rowEvent) {
    this.editIntroducedMembers = rowEvent.row.item;
    this.introducedMemberDetailForm.patchValue(this.editIntroducedMembers);

    const conDate = new Date(this.editIntroducedMembers.CreditedDate);
    this.introducedMemberDetailForm.patchValue({
      CreditedDate: {
        date: {
          year: conDate.getFullYear(),
          month: conDate.getMonth() + 1,
          day: conDate.getDate()
        }
      }
    });
  }

  // how to dismiss the Introduced Member Details View popup
  private getDismissReason(reason: any) {
    // if (reason === ModalDismissReasons.ESC) {
    //   return 'by pressing ESC';
    // } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    //   return 'by clicking on a backdrop';
    // } else {
    //   return `with: ${reason}`;
    // }
  }

  // save the Introduced Member Details View
  saveIntroducedMembers(value): any {
    this.introducedMemberDetailForm.value.CreditedDate =
      this.introducedMemberDetailForm.value.CreditedDate.date.year + '.' +
      this.introducedMemberDetailForm.value.CreditedDate.date.month + '.' +
      this.introducedMemberDetailForm.value.CreditedDate.date.day;

    this.creditedDate = value.creditedDate;

    // this.memberId = this.selectedMember.Id;
    value.memberId = this.editIntroducedMembers.Id;

    this.exceMemberService.updateIntroducedMembers(value).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        const updatedItem = this.introducedMembers.find(x => x.Id === this.editIntroducedMembers.Id)
        updatedItem.CreditedDate = value.CreditedDate;
        updatedItem.CreditedText = value.CreditedText;
      }
    },
      error => {
        console.log(error);
      });
    this.modalReference.close();
  }

  // delete unavailable times from the db
  deleteIntroducedMembers(item) {
    this.deletedItem = item;
    this.deletedItemId = item.Id;
    this.deletedItemCreditedDate = item.CreditedDate;
    this.deletedItemCreditedText = item.CreditedText;

    let messageTitle = '';
    let messageBody = '';

    (this.translateService.get('COMMON.Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
    (this.translateService.get('MEMBERSHIP.MsgDeleteIntroducedMemberConfirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));

    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: messageTitle,
        messageBody: messageBody
      }
    );
  }

  yesHandler() {
    const index: number = this.introducedMembers.indexOf(this.deletedItem);
    this.deletedItem = null;

    if (index !== -1) {
      this.introducedMembers.splice(index, 1);
    } else {
    }

    this.exceMemberService.updateIntroducedMembers({
      memberId: this.deletedItemId,
      creditedDate: this.deletedItemCreditedDate,
      creditedText: this.deletedItemCreditedText,
      isDelete: true
    }).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          this.modalReference.close();
        } else {
        }
      }
      )
  }

  noHandler() {
  }

  // close the Introduced Member Details View popup when click the cancel button
  closeIntroducedMemberDetailForm() {
    this.modalReference.close();
  }

}
