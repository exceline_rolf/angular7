import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McIntroducedMembersComponent } from './mc-introduced-members.component';

describe('McIntroducedMembersComponent', () => {
  let component: McIntroducedMembersComponent;
  let fixture: ComponentFixture<McIntroducedMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McIntroducedMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McIntroducedMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
