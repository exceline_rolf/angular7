import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McTrainingProgramComponent } from './mc-training-program.component';

describe('McTrainingProgramComponent', () => {
  let component: McTrainingProgramComponent;
  let fixture: ComponentFixture<McTrainingProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McTrainingProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McTrainingProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
