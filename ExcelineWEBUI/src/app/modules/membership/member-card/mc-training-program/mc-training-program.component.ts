import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { ExceMemberService } from '../../services/exce-member.service'
import { McBasicInfoService } from './../mc-basic-info/mc-basic-info.service';
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { Subject } from 'rxjs';
import { takeUntil, mergeMap } from 'rxjs/operators';

const now = new Date();

@Component({
  selector: 'app-mc-training-program',
  templateUrl: './mc-training-program.component.html',
  styleUrls: ['./mc-training-program.component.scss']
})
export class McTrainingProgramComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private selectedMember: any;
  private integrationSettings = []
  private WellnessIntegrationSetting: any
  private branchId: number

  public infoGymId = '';
  public wellnessId = '';

  constructor(private exceMemberService: ExceMemberService,
    private basicInfoService: McBasicInfoService,
    private exceLoginService: ExceLoginService,

  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }

  ngOnInit() {
    this.basicInfoService.currentMember.pipe(
      mergeMap( currentMember => {
        this.selectedMember = currentMember;
        this.infoGymId = this.selectedMember.InfoGymId;
        return this.exceMemberService.GetMemberIntegrationSettings(this.selectedMember.BranchId, this.selectedMember.Id);
      }),
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.integrationSettings = result.Data
          this.integrationSettings.forEach(element => {
            if (element) {
              switch (element.SystemName) {
                case 'Wellness':
                  this.WellnessIntegrationSetting = element
                  this.wellnessId = this.WellnessIntegrationSetting.UserId
                  break;
              }
            }
          })
        }
      });
  }

  ngOnDestroy () {
    this.destroy$.next();
    this.destroy$.complete();
  }


  getIntegrationSetings() {
    this.exceMemberService.GetMemberIntegrationSettings(this.selectedMember.BranchId, this.selectedMember.Id).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.integrationSettings = result.Data
          this.integrationSettings.forEach(element => {

            if (element) {
              switch (element.SystemName) {
                case 'Wellness':
                  this.WellnessIntegrationSetting = element
                  this.wellnessId = this.WellnessIntegrationSetting.UserId
                  break;
              }
            }
          })
        }
      });
  }

  loadExcorlive() {
/*     var win = window.open('about:blank', '_blank');
    this.exceMemberService.GetExorLiveClientAppUrl().pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          if (result.Data != null) {
            let url: string = result.Data
            let custId: string = ''
            this.exceMemberService.GetEncryptKey(this.selectedMember.CustId).pipe(takeUntil(this.destroy$)).subscribe(result => {
              if (result) {
                let gymCode = result.Data.gymCode;
                custId = result.Data.custId;
                let link = url + "?custId=" + custId + "&gymCode=" + gymCode;
                win.location.href = link;
              }
            })
          }
        }
      }) */
      let win = window.open('about:blank', '_blank');
      let url: string;
      this.exceMemberService.GetExorLiveClientAppUrl().pipe(
        mergeMap( ExorLiveurl => {
          url = ExorLiveurl.Data;
          return this.exceMemberService.GetEncryptKey(this.selectedMember.CustId)
        }),
        takeUntil(this.destroy$)
      ).subscribe( result => {
        const gymCode = result.Data.gymCode;
        const custId = result.Data.custId;
        const link = url + "?custId=" + custId + "&gymCode=" + gymCode;
        win.location.href = link;
      })
  }


  validateMember(member) {
    if ((!this.selectedMember.FirstName) && (this.selectedMember.Role === 0)) {
      alert('First Name Required ')
      return false;
    }
    if ((!this.selectedMember.LastName)) {
      alert('Last Name required')
      return false;
    }
    if ((!this.selectedMember.Gender) && (this.selectedMember.Role === 0)) {
      alert('Gender Required')
      return false;
    }
    return true;
  }


  registerWellness() {
    if (this.validateMember(this.selectedMember)) {
      this.exceMemberService.WellnessIntegrationOperation({
        'Member': this.selectedMember,
        'WellnessIntegrationSetting': '',
        'WellnessOperationType': 'REGISTER',
        'BranchId': this.selectedMember.BranchId
      }).pipe(
         mergeMap( result => this.exceMemberService.GetMemberIntegrationSettings(this.selectedMember.BranchId, this.selectedMember.Id)),
         takeUntil(this.destroy$)
      ).subscribe(
        result => {
          if (result) {
            this.integrationSettings = result.Data
            this.integrationSettings.forEach(element => {
              if (element) {
                switch (element.SystemName) {
                  case 'Wellness':
                    this.WellnessIntegrationSetting = element
                    this.wellnessId = this.WellnessIntegrationSetting.UserId
                    break;
                }
              }
            })
          }
        });
    }
  }

  updateWellness() {
    if (this.validateMember(this.selectedMember)) {
      this.exceMemberService.WellnessIntegrationOperation({
        'Member': this.selectedMember,
        'WellnessIntegrationSetting': this.WellnessIntegrationSetting,
        'WellnessOperationType': 'UPDATE',
        'BranchId': this.selectedMember.BranchId
      }).pipe(
        mergeMap(result => this.exceMemberService.GetMemberIntegrationSettings(this.selectedMember.BranchId, this.selectedMember.Id)),
        takeUntil(this.destroy$)
      ).subscribe(
        result => {
         if (result) {
           this.integrationSettings = result.Data
           this.integrationSettings.forEach(element => {
             if (element) {
               switch (element.SystemName) {
                 case 'Wellness':
                   this.WellnessIntegrationSetting = element
                   this.wellnessId = this.WellnessIntegrationSetting.UserId
                   break;
               }
             }
           })
         }
       });
    }
  }

  infoGymGenerate() {
    var infoGymMember = {
        'keycard': this.selectedMember.MemberCardNo,
        'name': this.selectedMember.Name,
        'email': this.selectedMember.Email,
        'phone': this.selectedMember.Mobile
      };
    if (!this.infoGymId) {
      this.exceMemberService.InfoGymRegisterMember({
        'Member': infoGymMember,
        'MemberID': this.selectedMember.Id
      }).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result.Data) {
          this.selectedMember.InfoGymId = result.Data;
          this.infoGymId = result.Data;
        }
      })
    } else {
      this.exceMemberService.InfoGymUpdateMember({
        'Member': infoGymMember,
        'InfoGymId': this.infoGymId
      }).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result.Data) {
          this.selectedMember.InfoGymId = result.Data;
          this.infoGymId = result.Data;
        }
      })
    }
  }



}
