import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../services/exce-member.service'
import { McBasicInfoService } from './../mc-basic-info/mc-basic-info.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { takeUntil, mergeMap } from 'rxjs/operators';

const now = new Date();

@Component({
  selector: 'app-mc-communication-log',
  templateUrl: './mc-communication-log.component.html',
  styleUrls: ['./mc-communication-log.component.scss']
})
export class McCommunicationLogComponent implements OnInit, OnDestroy {
  private commLogs: any;
  private filteredCommLogs: any;
  private itemResource: any = 0;
  public items = [];
  public itemCount = 0;
  private resources: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();;
  private modalReference?: any;
  private memberBookingForm: any;
  searchFrom: any;
  private commLogFrom: any
  private rowItem: any;
  private selectedMember: any;
  private today: NgbDateStruct;
  private monthbefore: NgbDateStruct;
  private fromdate: any;
  private toDate: any;
  private destroy$ = new Subject<void>();

  constructor(
    private exceMemberService: ExceMemberService,
    private basicInfoService: McBasicInfoService,
    private modalService: UsbModal,
    private fb: FormBuilder,
  ) {
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.monthbefore = { year: now.getFullYear(), month: now.getMonth(), day: now.getDate() };
    this.searchFrom = this.fb.group({
      'toDate': [{ date: this.today }],
      'fromDate': [{ date: this.monthbefore }],
      'type': [null]
    });

    this.commLogFrom = this.fb.group({
      // 'Description': [null],
      // 'MobileNo': [null],
      // 'Type': [null],
      // 'CreatedUser': [null],
      // 'GymName': [null],
      // 'Status': [null],
      // 'Comment': [null]
    });
  }

  ngOnInit() {
/*     this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
          this.exceMemberService.getNotificationByNotifiyMethod(this.selectedMember.Id, this.selectedMember.BranchId,
            (this.monthbefore.month + '/' + this.monthbefore.day + '/' + this.monthbefore.year),
            (this.today.month + '/' + this.today.day + '/' + this.today.year), 'SMSEMAIL').pipe(takeUntil(this.destroy$)).subscribe(result => {
              if (result) {
                this.commLogs = result.Data;
                this.itemResource = new DataTableResource(this.commLogs);
                this.itemResource.count().then(count => this.itemCount = count);
                this.items = this.commLogs;
                this.itemCount = Number(result.Data.length);

              } else {
              }
            })
        }
    }); */
    this.basicInfoService.currentMember.pipe(
      mergeMap(
        member => {
          this.selectedMember = member;
          return this.exceMemberService.getNotificationByNotifiyMethod(member.Id, member.BranchId,
            (this.monthbefore.month + '/' + this.monthbefore.day + '/' + this.monthbefore.year),
            (this.today.month + '/' + this.today.day + '/' + this.today.year), 'SMSEMAIL').pipe(
              takeUntil(this.destroy$)
            )
        })
    ).pipe(
      takeUntil(this.destroy$)
    ).subscribe( result => {
      this.commLogs = result.Data;
      this.itemResource = new DataTableResource(this.commLogs);
      this.itemResource.count().then(count => this.itemCount = count);
      this.items = this.commLogs;
      this.itemCount = Number(result.Data.length);
    });
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  filterInput(typeValue: any) {
    this.filteredCommLogs = this.commLogs
    if (typeValue !== 'SMSEMAIL') {
      this.filteredCommLogs = this.filterpipe.transform('Type', 'SELECT', this.filteredCommLogs, typeValue);
    }
    this.itemResource = new DataTableResource(this.filteredCommLogs);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredCommLogs);
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = this.removehtmltext(items))
    }
  }
  openCommModel(content: any) {
    this.commLogFrom.patchValue(this.rowItem);
    this.modalReference = this.modalService.open(content, { width: '850' });
  }

  rowDoubleClick(rowEvent) {
    this.rowItem = rowEvent.row.item;
  }

  rowItemLoad(rowItem) {
    this.rowItem = rowItem
  }

  removehtmltext(content: any) {
    content.forEach(element => {
        element.Description = element.Description ? String(element.Description).replace(/<[^>]+>/gm, '') : ''
        element.Description = element.Description ? String(element.Description).replace('&nbsp', '') : ''
    });

  return content
  }

  dateFilter(dateRange, type) {
    const fromDate = dateRange.fromDate.date.month + '/' + dateRange.fromDate.date.day + '/' + dateRange.fromDate.date.year
    const toDate = dateRange.toDate.date.month + '/' + dateRange.toDate.date.day + '/' + dateRange.toDate.date.year
    this.exceMemberService.getNotificationByNotifiyMethod(this.selectedMember.Id, this.selectedMember.BranchId,
      fromDate,
      toDate, type).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          this.commLogs = result.Data;
          this.itemResource = new DataTableResource(this.commLogs);
          this.itemResource.count().then(count => this.itemCount = count);
          this.items = this.removehtmltext(this.commLogs);
          this.itemCount = Number(result.Data.length);
        } else {
        }
      })
  }

}
