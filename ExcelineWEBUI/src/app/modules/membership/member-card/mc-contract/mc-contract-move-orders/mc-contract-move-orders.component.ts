import { MemberRole } from './../../../../../shared/enums/us-enum';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { McContractService } from '../mc-contract.service';
import { McBasicInfoService } from 'app/modules/membership/member-card/mc-basic-info/mc-basic-info.service';
import { ExceLoginService } from 'app/modules/login/exce-login/exce-login.service';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import * as moment from 'moment';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
import { Subscription } from 'rxjs';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../../services/exce-member.service';
import { error } from 'util';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-mc-contract-move-orders',
  templateUrl: './mc-contract-move-orders.component.html',
  styleUrls: ['./mc-contract-move-orders.component.scss']
})
export class McContractMoveOrdersComponent implements OnInit, OnDestroy {
  private _user: any;
  private _branchID: any;
  private _deletingOrder: any;
  private _yesHandlerSubcription: Subscription;
  private _movingOrder: any;
  private _movingDirection: string;
  private _dateFormat: string;
  private destroy$ = new Subject<void>();

  public modalReference: any;
  public selectedMember: any;
  public selectedOrder: any;
  public selectedContract: any;
  public locale: string;

  constructor(
    private contractService: McContractService,
    private basicInfoService: McBasicInfoService,
    private loginservice: ExceLoginService,
    private toolBarService: ExceToolbarService,
    private messageService: ExceMessageService,
    private modalService: UsbModal,
    private memberService: ExceMemberService,
    private loginService: ExceLoginService,
    private router: Router
  ) {}

  ngOnInit() {
    const language = this.toolBarService.getSelectedLanguage();
    this.locale = language.culture;
    this._dateFormat = 'DD.MM.YYYY';
    this.toolBarService.langUpdated.pipe(
      takeUntil(this.destroy$))
    .subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );

    this._branchID = this.loginService.SelectedBranch.BranchId;
    this.basicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
    ).subscribe(currentMember => this.selectedMember = currentMember);

    this._user = this.loginService.CurrentUser.username;
    this.selectedContract = this.contractService.getSelectedContract();
    // subscribe for the messages
    this.messageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      switch (value.id) {
        case 'ORDER':
          this.deleteOrder();
          break;
        case 'MOVEORDER':
          this.moveOrders();
          break;
      }
    });
  }

  updateOrders() {
    // save orders
    const contractOrderUpdateDetails = {
      installmentList: this.selectedContract.InstalllmentList,
      branchId: this._branchID
    }
    this.memberService.updateContractOrders(contractOrderUpdateDetails).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      result => {
        if (result) {
          const base = '/membership/card/' + this.selectedMember.Id + '/' + this.selectedMember.BranchId + '/' + MemberRole[this.selectedMember.Role] + '/contracts/';
          this.router.navigate([base + '/' + this.selectedContract.Id]);
        }
      }, error => {
        if (error) {
          this.messageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'MEMBERSHIP.OrderUpdateFailed'
            }
          )
        }
      }
    );
  }

  closeoveOrders() {
    const base = '/membership/card/' + this.selectedMember.Id + '/' + this.selectedMember.BranchId + '/' + MemberRole[this.selectedMember.Role] + '/contracts/';
    this.router.navigate([base + '/' + this.selectedContract.Id]);
  }

  moveOrdersEvent(item: any, direction: string) {
    this._movingOrder = item;
    this._movingDirection = direction;

    if (direction === 'UP') {
      this.messageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.ConfirmDateUp', // this is changed to be more meaningfull
          msgBoxId: 'MOVEORDER'
        });
    } else if (direction === 'DOWN') {
      this.messageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.ConfirmDatedwn', // this is changed to be more meaningfull
          msgBoxId: 'MOVEORDER'
        });
    }
  }

  moveOrders() {
    if (this._movingDirection === 'DOWN') {
      this.selectedContract.InstalllmentList.forEach(order => {
        if (order.InstallmentNo >= this._movingOrder.InstallmentNo) {
          order.DueDate = moment(this.dateAdd(order.DueDate, -1, 'months')).format('YYYY-MM-DD').toString();
          if (this.getNumberOfDaysInMonth(moment(order.TrainingPeriodEnd).year(), moment(order.TrainingPeriodEnd).month() + 1) === moment(order.TrainingPeriodEnd).date()) {
            order.TrainingPeriodStart = moment(this.dateAdd(order.TrainingPeriodStart, -1, 'months')).format('YYYY-MM-DD').toString();
            order.TrainingPeriodEnd = this.dateAdd(order.TrainingPeriodEnd, -1, 'months');
            const numberOfDaysinNewMonth = this.getNumberOfDaysInMonth(moment(order.TrainingPeriodEnd).year(), moment(order.TrainingPeriodEnd).month() + 1)
            const newEndDate: Date = new Date(moment(order.TrainingPeriodEnd).toDate().getFullYear(), moment(order.TrainingPeriodEnd).toDate().getMonth(), numberOfDaysinNewMonth);
            order.TrainingPeriodEnd = newEndDate;
            order.TrainingPeriodEnd = moment(order.TrainingPeriodEnd).format('YYYY-MM-DD').toString();
          } else {
            order.TrainingPeriodStart = moment(this.dateAdd(order.TrainingPeriodStart, -1, 'months')).format('YYYY-MM-DD').toString();
            order.TrainingPeriodEnd = moment(this.dateAdd(order.TrainingPeriodEnd, -1, 'months')).format('YYYY-MM-DD').toString();
          }
          order.Text = moment(order.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(order.TrainingPeriodEnd).format(this._dateFormat);
        }
      });
    } else if (this._movingDirection === 'UP') {
      this.selectedContract.InstalllmentList.forEach(order => {
        if (order.InstallmentNo >= this._movingOrder.InstallmentNo) {
          order.DueDate = moment(this.dateAdd(order.DueDate, 1, 'months')).format('YYYY-MM-DD').toString();
          if (this.getNumberOfDaysInMonth(moment(order.TrainingPeriodEnd).year(), moment(order.TrainingPeriodEnd).month() + 1) === moment(order.TrainingPeriodEnd).date()) {
            order.TrainingPeriodStart = moment(this.dateAdd(order.TrainingPeriodStart, 1, 'months')).format('YYYY-MM-DD').toString();
            order.TrainingPeriodEnd = this.dateAdd(order.TrainingPeriodEnd, 1, 'months');
            const numberOfDaysinNewMonth = this.getNumberOfDaysInMonth(moment(order.TrainingPeriodEnd).year(), moment(order.TrainingPeriodEnd).month() + 1)
            const newEndDate: Date = new Date(moment(order.TrainingPeriodEnd).toDate().getFullYear(), moment(order.TrainingPeriodEnd).toDate().getMonth(), numberOfDaysinNewMonth);
            order.TrainingPeriodEnd = newEndDate;
            order.TrainingPeriodEnd = moment(order.TrainingPeriodEnd).format('YYYY-MM-DD').toString();
          } else {
            order.TrainingPeriodStart = moment(this.dateAdd(order.TrainingPeriodStart, 1, 'months')).format('YYYY-MM-DD').toString();
            order.TrainingPeriodEnd = moment(this.dateAdd(order.TrainingPeriodEnd, 1, 'months')).format('YYYY-MM-DD').toString();
          }
          order.Text = moment(order.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(order.TrainingPeriodEnd).format(this._dateFormat);
        }
      });
    }
  }

  openDetailView(content, selectedOrder) {
    this.selectedOrder = selectedOrder;
    const dueDate = new Date(selectedOrder.DueDate);
    this.selectedOrder = selectedOrder;
    this.selectedOrder.DueDateFormated = { year: dueDate.getFullYear(), month: dueDate.getMonth() + 1, day: dueDate.getDate() };


    if (!selectedOrder.isOpened || selectedOrder.isFormatedDates) {
      const traningStart = new Date(selectedOrder.TrainingPeriodStart);
      this.selectedOrder.TrainingPeriodStart = { year: traningStart.getFullYear(), month: traningStart.getMonth() + 1, day: traningStart.getDate() };
      const traningEnd = new Date(selectedOrder.TrainingPeriodEnd);
      this.selectedOrder.TrainingPeriodEnd = { year: traningEnd.getFullYear(), month: traningEnd.getMonth() + 1, day: traningEnd.getDate() };
      const installmentDate = new Date(selectedOrder.InstallmentDate);
      this.selectedOrder.InstallmentDate = { year: installmentDate.getFullYear(), month: installmentDate.getMonth() + 1, day: installmentDate.getDate() };
      const originalDueDate = new Date(selectedOrder.OriginalDueDate);
      this.selectedOrder.OriginalDueDate = { year: originalDueDate.getFullYear(), month: originalDueDate.getMonth() + 1, day: originalDueDate.getDate() };
      const estimatedInvoiceDate = new Date(selectedOrder.EstimatedInvoiceDate);
      this.selectedOrder.EstimatedInvoiceDate = { year: estimatedInvoiceDate.getFullYear(), month: estimatedInvoiceDate.getMonth() + 1, day: estimatedInvoiceDate.getDate() };
    }

    selectedOrder.isOpened = true;
    this.modalReference = this.modalService.open(content);
  }

  closeOderDetailModal() {
    this.modalReference.dismiss();
  }

  orderUpdateSuccess() {
    this.modalReference.dismiss();
  }

  removeOrder(item) {
    this._deletingOrder = item;
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDelete',
        msgBoxId: 'ORDER'
      });
  }

  deleteOrder() {
    const deletingIndex = this.selectedContract.InstalllmentList.indexOf(this._deletingOrder);
    this.selectedContract.InstalllmentList.splice(deletingIndex, 1);
  }

  dateAdd(startDate: any, count: any, interval: string): Date {
    return moment(startDate).add(count, interval).toDate()
  }

  getNumberOfDaysInMonth(year: number, month: number): number {
    return moment(year + '-' + month, 'YYYY-MM').daysInMonth();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.modalReference) {
      this.modalReference.dismiss();
    }
  }

}
