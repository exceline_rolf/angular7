import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McContractMoveOrdersComponent } from './mc-contract-move-orders.component';

describe('McContractMoveOrdersComponent', () => {
  let component: McContractMoveOrdersComponent;
  let fixture: ComponentFixture<McContractMoveOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McContractMoveOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McContractMoveOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
