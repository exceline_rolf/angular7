import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McContractTemplatesComponent } from './mc-contract-templates.component';

describe('McContractTemplatesComponent', () => {
  let component: McContractTemplatesComponent;
  let fixture: ComponentFixture<McContractTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McContractTemplatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McContractTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
