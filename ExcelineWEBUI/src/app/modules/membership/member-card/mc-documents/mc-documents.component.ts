
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../services/exce-member.service'
import { McBasicInfoService } from './../mc-basic-info/mc-basic-info.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { DomSanitizer } from '@angular/platform-browser';
import { UsErrorService } from '../../../../shared/directives/us-error/us-error.service'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { FileUploader } from 'ng2-file-upload';
import { ConfigService } from '@ngx-config/core';
import { ExceSessionService } from 'app/shared/services/exce-session.service';
import { ExcePdfViewerService } from 'app/shared/components/exce-pdf-viewer/exce-pdf-viewer.service'
import { CommonHttpService } from 'app/shared/services/common-http.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Subject, Observable, Subscription, of, throwError, timer } from 'rxjs';
import { takeUntil, mergeMap, catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-mc-documents',
  templateUrl: './mc-documents.component.html',
  styleUrls: ['./mc-documents.component.scss']
})
export class McDocumentsComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private filteredDocuments = []
  private documentResources: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private modalReference?: any;
  private selectedMember: any;
  private documentList: any;
  private deletingItem: any;
  private model: any;
  private selectedItem: any;
  private currentFileEvent: any;
  private contracts: any;
  private isFileSelected = false;
  private isAddDoc: boolean;
  private memberApiURL: any;
  private sub: any;
  private timer: any;
  private router: any;
  private location: Location;

  public documents = [];
  public documentCount = 0;
  public Categories: any;
  public uploadForm: any;
  public formSubmited = false;

  formErrors = {
    'File': '',
    'Type': '',
    'ActivityId': ''
  };
  validationMessages = {
    'File': {
      'required': 'MEMBERSHIP.FileCannotbeEmpty',
    },
    'Type': {
      'required': 'MEMBERSHIP.SelectDoc',
    },
    'Contract': {
      'required': 'Contract is required.',
    }
  };

  modalReference2: any;

  constructor(private exceMemberService: ExceMemberService,
    private basicInfoService: McBasicInfoService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private exceMessageService: ExceMessageService,
    public sanitizer: DomSanitizer,
    private http: HttpClient,
    private config: ConfigService,
    private excePdfViewerService: ExcePdfViewerService,
    private exceSessionService: ExceSessionService
  ) {
    this.memberApiURL = this.config.getSettings('EXCE_API.MEMBER');
/*     this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
      ).subscribe((value) => {
      if (value.id === 'DELETE_DOC') {
        this.deleteDocument();
      }
    }); */

    this.exceMessageService.yesCalledHandle.pipe(
      mergeMap(value => {
        if (value.Id === 'DELETE_DOC') {
          return this.exceMemberService.DeleteDocumentData({ 'DocId': this.deletingItem.FileId, 'BranchId': this.selectedMember.BranchId });
        }
      })).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        if (result) {
          this.documents = this.documents.filter(item => item !== this.deletingItem);
          this.documentList = this.documentList.filter(item => item !== this.deletingItem);
        }
      });

    this.uploadForm = this.fb.group({
      'File': [null, [Validators.required]],
      'Type': [null, [Validators.required]],
      'Contract': [null]
    });

  }

  ngOnInit() {
    this.uploadForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => {
      UsErrorService.onValueChanged(this.uploadForm, this.formErrors, this.validationMessages)
    });

    this.basicInfoService.currentMember.pipe(
      mergeMap(member => {
        this.selectedMember = member;
        return this.exceMemberService.GetDocumentData(true, this.selectedMember.CustId, '', -1, this.selectedMember.BranchId);
      })
    ).pipe(
      mergeMap(result => {
        if (result) {
          this.documentList = result.Data;
          this.documentResources = new DataTableResource(this.documents);
          this.documentResources.count().then(count => this.documentCount = count);
          this.documents = this.documentList
          this.documentCount = Number(result.Data.length);
        }
        return this.exceMemberService.getCategories('DOCUMENTCATEGORY')
      })
    ).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.Categories = result.Data;
      }
    });

/*     this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
          this.getDocuments();
          this.exceMemberService.getCategories('DOCUMENTCATEGORY').pipe(takeUntil(this.destroy$)).subscribe(result => {
              if (result) {
                this.Categories = result.Data;
              }
            });
        }
      }); */
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.modalReference) {
      this.modalReference.close();
    }
    if (this.modalReference2) {
      this.modalReference2.close();
    }
  }

  getDocuments(searchText?) {
    if (!searchText) {
      searchText = ''
    }
    this.exceMemberService.GetDocumentData(true, this.selectedMember.CustId, searchText, -1, this.selectedMember.BranchId).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          this.documentList = result.Data;
          this.documentResources = new DataTableResource(this.documents);
          this.documentResources.count().then(count => this.documentCount = count);
          this.documents = this.documentList
          this.documentCount = Number(result.Data.length);
        } else {
        }
      })
  }

  filterInput(nameValue?: String, categoryValue?: any) {
    this.filteredDocuments = this.documentList
    if (categoryValue !== 'ALL') {
      this.filteredDocuments = this.filterpipe.transform('DocumentCategory', 'SELECT', this.filteredDocuments, categoryValue);
    }
    if (nameValue !== '') {
      this.filteredDocuments = this.filterpipe.transform('FileName', 'NAMEFILTER', this.filteredDocuments, nameValue);
    }
    this.documentResources = new DataTableResource(this.filteredDocuments);
    this.documentResources.count().then(count => this.documentCount = count);
    this.reloadItems(this.filteredDocuments);
  }

  openAddDocumentModel(content, isAddDoc) {
    this.formSubmited = false;
    this.isAddDoc = isAddDoc
    this.exceMemberService.getMemberContractSummeries(this.selectedMember.Id, this.selectedMember.BranchId, false, false).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.contracts = result.Data;
        this.modalReference = this.modalService.open(content, { width: '400' });
      }
    })
  }

  openAddScanDoc(content) {
    this.modalReference2 = this.modalService.open(content, { width: '400' });
  }

  reloadItems(params): void {
    if (this.documentResources) {
      this.documentResources.query(params).then(items => this.documents = items);
    }
  }

  searchDocuments(param: string) {
    this.getDocuments(param);
  }

  deleteDoc(deletingItem) {
    this.deletingItem = deletingItem;
    this.openExceDeleteMessage('Confirm', 'MEMBERSHIP.DeleteDocumentErrorMsg')
  }

  openExceDeleteMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: title,
        messageBody: body,
        msgBoxId: 'DELETE_DOC'
      });
  }

  deleteDocument() {
     this.exceMemberService.DeleteDocumentData({ 'DocId': this.deletingItem.FileId, 'BranchId': this.selectedMember.BranchId }).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.documents = this.documents.filter(item => item !== this.deletingItem);
          this.documentList = this.documentList.filter(item => item !== this.deletingItem);
        } else {
        }
      });
  }

  fileChangeEvent(event) {
    this.currentFileEvent = event
  }

  fileUpload(value) {
    if (this.currentFileEvent === undefined || this.currentFileEvent == null) {
      this.uploadForm.controls['File'].setErrors({ 'required': true });
    }
    this.formSubmited = true;
    if (this.uploadForm.valid) {
      if (this.currentFileEvent) {
        // debugger;
        const fileList: FileList = this.currentFileEvent.target.files;
        if (fileList.length > 0) {
          const file: File = fileList[0];
          const formData: FormData = new FormData();
          formData.append('uploadFile', file, file.name);
          const token = JSON.parse(Cookie.get('currentUser')).token
          const headers = new HttpHeaders();
          headers.append('Access-Control-Allow-Origin', '*');
          headers.append('Authorization', 'bearer ' + token);
          headers.append('UserName', JSON.parse(Cookie.get('currentUser')).username);
          headers.append('BranchId', this.selectedMember.BranchId);
          headers.append('CustId', this.selectedMember.CustId);
          const options = { headers: headers };
          const apiUrl1 = this.memberApiURL + '/UploadFileApi';

          this.http.post(apiUrl1, formData, options).pipe(
            map(res => res),
            catchError(error => throwError(error)),
            takeUntil(this.destroy$)
          ).subscribe(data => <any> {
            if (data) {
              this.saveDocumentInfo(data.Data, value, file);
              this.uploadForm.reset();
              this.formSubmited = false;
              this.currentFileEvent = null;
            }
          })

          // this.http.post(apiUrl1, formData, options)
          //   .map(res => res)
          //   .catch(error => Observable.throw(error))
          //   .pipe(takeUntil(this.destroy$)).subscribe(
          //     data => {
          //       if (data) {
          //         this.saveDocumentInfo(data.Data, value, file);
          //         this.uploadForm.reset();
          //         this.formSubmited = false;
          //         this.currentFileEvent = null;
          //       }
          //     })
        }
      }
    } else {
      UsErrorService.validateAllFormFields(this.uploadForm, this.formErrors, this.validationMessages);
    }
  }

  closeUploadDocModal() {
    this.uploadForm.reset();
  }

  saveDocumentInfo(path: string, formData, file) {
    const docDataObj = {
      'FileId': 0,
      'FileName': file.name,
      'BranchId': this.selectedMember.BranchId,
      'FilePath': path,
      'UploadedUser': JSON.parse(Cookie.get('currentUser')).username,
      'DocumentType': formData.Type,
      'IsActive': this.selectedMember.ActiveStatus,
      'CusId': this.selectedMember.CustId,
      'ContractNo': formData.Contract
    }
    this.SaveDoctData(docDataObj, path)
  }

  SaveDoctData(docDataObj: any, path: any) {
    if (path !== '') {
      this.exceMemberService.SaveDocumentData({ 'BranchId': this.selectedMember.BranchId, 'Document': docDataObj }).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          this.getDocuments();
          this.uploadForm.reset();
          this.closeForm()
        }
      })
    }
  }

  closeForm() {
    this.modalReference.close();
  }

  createGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      // tslint:disable-next-line:no-bitwise
      const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  uploadScanDoc(value) {

    const sessionKey = this.createGuid();
    if (sessionKey) {
      const docDetail =
        'CUSTID$' + this.selectedMember.CustId +
        '|BRANCHID$' + this.selectedMember.BranchId +
        '|GYMCODE$' + JSON.parse(Cookie.get('currentUser')).username.toString().split('/')[0] +
        '|DOCTYPE$' + value.Type;
      this.exceMemberService.AddSessionValue({ 'SessionKey': 'SCAN' + sessionKey, 'Value': docDetail }).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          this.exceSessionService.AddSession('SCNDOC', 'SCAN', sessionKey, JSON.parse(Cookie.get('currentUser')).username);
        }
      })
      this.timer = timer(0, 2000);
      this.sub = this.timer.pipe(takeUntil(this.destroy$)).subscribe(t => this.tickerFunc(t, sessionKey, value));
    }
  }

  tickerFunc(tick: number, sessionKey: string, value: any) {
    this.exceMemberService.getSessionValue(sessionKey).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result.Data) {
          this.sub.unsubscribe();
          if (result.Data === 'ERROR') {
            // Error while uploading - add message
          } else {
            const docDataObj = {
              'FileId': 0,
              'FileName': result.Data.replace(/^.*[\\\/]/, ''),
              'BranchId': this.selectedMember.BranchId,
              'FilePath': result.Data,
              'UploadedUser': JSON.parse(Cookie.get('currentUser')).username,
              'DocumentType': value.Type,
              'IsActive': this.selectedMember.ActiveStatus,
              'CusId': this.selectedMember.CustId,
              'ContractNo': value.Contract
            }
            this.SaveDoctData(docDataObj, result.Data);
            this.sub.unsubscribe();
          }
        } else if (tick === 9) {
          this.sub.unsubscribe();
        }
      });
  }

  submitForm(value) {
    this.formSubmited = true;
    if (this.uploadForm.valid) {
      this.formSubmited = false;
    } else {
      UsErrorService.validateAllFormFields(this.uploadForm, this.formErrors, this.validationMessages);
    }
  }

  openPdfViewer(item) {
    this.selectedItem = item
    this.model = this.excePdfViewerService.openPdfViewer('Exceline', this.config.getSettings('EXCE_API.PDFVIEWERURL') + item.FilePath);
  }

}
