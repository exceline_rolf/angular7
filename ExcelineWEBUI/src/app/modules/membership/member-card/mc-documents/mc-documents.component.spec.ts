import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McDocumentsComponent } from './mc-documents.component';

describe('McDocumentsComponent', () => {
  let component: McDocumentsComponent;
  let fixture: ComponentFixture<McDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
