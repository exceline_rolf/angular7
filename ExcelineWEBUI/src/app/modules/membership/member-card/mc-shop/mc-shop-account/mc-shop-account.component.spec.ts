import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McShopAccountComponent } from './mc-shop-account.component';

describe('McShopAccountComponent', () => {
  let component: McShopAccountComponent;
  let fixture: ComponentFixture<McShopAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McShopAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McShopAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
