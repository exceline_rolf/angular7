import { ExceMessageService } from './../../../../shared/components/exce-message/exce-message.service';
import { ExceSessionService } from './../../../../shared/services/exce-session.service';
import { AmountConverterPipe } from './../../../../shared/pipes/amount-converter.pipe';
import { ExcePdfViewerService } from './../../../../shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { Extension } from './../../../../shared/Utills/Extensions';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import { UsbModalRef } from '../../../../shared/components/us-modal/modal-ref';
import { ShopHomeService } from '../../../shop/shop-home/shop-home.service';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { ExceToolbarService } from '../../../common/exce-toolbar/exce-toolbar.service';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { McBasicInfoService } from '../mc-basic-info/mc-basic-info.service';
import { IMyDpOptions } from '../../../../shared/components/us-date-picker/interfaces/my-options.interface';
import { MemberShopService } from '../../services/member-shop.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ExceShopService } from 'app/modules/shop/services/exce-shop.service';
import { PlatformLocation, DatePipe } from '@angular/common';
import { MemberRole } from 'app/shared/enums/us-enum';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { takeUntil, mergeMap } from 'rxjs/operators';

const now = new Date();

@Component({
  selector: 'app-mc-shop',
  templateUrl: './mc-shop.component.html',
  styleUrls: ['./mc-shop.component.scss']
})

export class McShopComponent implements OnInit, OnDestroy {
  branchId: any;
  durationSelectModel: UsbModalRef;
  shopView: any;
  filterArticleName: any;
  filterArticleNo: any;
  filterPaymentType: any;
  purchaseHistory: any[];
  withdrawalHistory: any[];
  selectedMember: any;
  itemCount = 0;
  itemResource?: DataTableResource<any>;
  saleItem: any;
  receiptDetails: any = [];
  saleItemTotal: any;
  locale: string;
  filterdSales: any = [];
  filterpipe: DataTableFilterPipe = new DataTableFilterPipe();

  purchaseSearchForm: FormGroup;
  addGiftVoucherForm: FormGroup;
  withdrawalsSearchForm: FormGroup;
  diurationSelectForm: FormGroup;
  printForm: FormGroup;
  startDate: any;
  endDate: any;

  private destroy$ = new Subject<void>();
  fromDatePickerOptions: IMyDpOptions = {
    disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() },
  };

  toDatePickerOptions: IMyDpOptions = {
    disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }
  };

  fromwDatePickerOptions: IMyDpOptions = {
    disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() },
  };

  towDatePickerOptions: IMyDpOptions = {
    disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }
  };

  constructor(
    private memberShopService: MemberShopService,
    private shopHomeService: ShopHomeService,
    private shopService: ExceShopService,
    private modalService: UsbModal,
    private toolbarService: ExceToolbarService,
    private fb: FormBuilder,
    private location: PlatformLocation,
    private exceLoginService: ExceLoginService,
    private basicInfoService: McBasicInfoService,
    private route: ActivatedRoute,
    private router: Router,
    private excePdfViewerService: ExcePdfViewerService,
    private datePipe: DatePipe,
    private amountConverterPipe: AmountConverterPipe,
    private exceSessionService: ExceSessionService,
     private exceMessageService: ExceMessageService
  ) {

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );

    location.onPopState((event) => {
      if (this.durationSelectModel) {
        this.durationSelectModel.close();
      }
      if (this.shopView) {
        this.shopView.close();
      }
    });

    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );

    const today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    const days10Before = new Date();
    days10Before.setDate(days10Before.getDate() - 10);
    const days1mBefore = new Date();
    days1mBefore.setMonth(days1mBefore.getMonth() - 1);
    const days2mBefore = new Date();
    days2mBefore.setMonth(days2mBefore.getMonth() - 2);

    this.purchaseSearchForm = fb.group({
      'toDate': [{ date: today }],
      'fromDate': [{ date: { year: days10Before.getFullYear(), month: days10Before.getMonth() + 1, day: days10Before.getDate() } }]
    });

    this.withdrawalsSearchForm = fb.group({
      'toDate': [{ date: today }],
      'fromDate': [{ date: { year: days2mBefore.getFullYear(), month: days2mBefore.getMonth() + 1, day: days2mBefore.getDate() } }]
    });

    this.diurationSelectForm = fb.group({
      'toDate': [{ date: today }],
      'fromDate': [{ date: { year: days1mBefore.getFullYear(), month: days1mBefore.getMonth() + 1, day: days1mBefore.getDate() } }]
    });

    this.filterPaymentType = 'ALL';

    this.addGiftVoucherForm = fb.group({
      'VoucherNumber': [{ value: '', disabled: true }],
      'Price': [{ value: '', disabled: true }],
      'PurchasedPrice': [{ value: '', disabled: true }],
      'ExpiryDate': [{ value: '', disabled: true }],
    });
  }

  ngOnInit() {
    this.basicInfoService.currentMember.pipe(
      mergeMap(currentMemeber => {
        this.selectedMember = currentMemeber;
        const body = {
          memberId: this.selectedMember.Id,
          fromDate: this.purchaseSearchForm.value.fromDate.date.year + '-' + this.purchaseSearchForm.value.fromDate.date.month + '-' + this.purchaseSearchForm.value.fromDate.date.day,
          toDate: this.purchaseSearchForm.value.toDate.date.year + '-' + this.purchaseSearchForm.value.toDate.date.month + '-' + this.purchaseSearchForm.value.toDate.date.day
        };

        return this.memberShopService.GetMemberPurchaseHistory(body);
      }),
      mergeMap(memberPurchaseHistory => {
        const body = {
          memberId: this.selectedMember.Id,
          startDate: this.withdrawalsSearchForm.value.fromDate.date.year + '-' + this.withdrawalsSearchForm.value.fromDate.date.month + '-' + this.withdrawalsSearchForm.value.fromDate.date.day,
          endDate: this.withdrawalsSearchForm.value.toDate.date.year + '-' + this.withdrawalsSearchForm.value.toDate.date.month + '-' + this.withdrawalsSearchForm.value.toDate.date.day,
          type: 'CASHWITHDRAWAL'
        };
        return this.shopService.GetWithdrawalByMemberId(body);
      }),
      takeUntil(this.destroy$)
    ).subscribe(res => {
      this.withdrawalHistory = res.Data;
      this.withdrawalHistory.forEach(item => {
        switch (item.WithdrawalType) {
          case 1:
            item.WithdrawalType = 'COMMON.CashWithdrawal'
            break;
          case 2:
            item.WithdrawalType = 'COMMON.PettyCash'
            break;
        }
      });
    }, err => {

    });

    // this.basicInfoService.currentMember.mergeMap(currentMember => {
    //   this.selectedMember = currentMember;
    //    const body = {
    //     memberId: this.selectedMember.Id,
    //     fromDate: this.purchaseSearchForm.value.fromDate.date.year + '-' + this.purchaseSearchForm.value.fromDate.date.month + '-' + this.purchaseSearchForm.value.fromDate.date.day,
    //     toDate: this.purchaseSearchForm.value.toDate.date.year + '-' + this.purchaseSearchForm.value.toDate.date.month + '-' + this.purchaseSearchForm.value.toDate.date.day
    //   };
    //   return this.memberShopService.GetMemberPurchaseHistory(body).pipe(
    //     takeUntil(this.destroy$)
    //   )
    // }).mergeMap(memberPurchaseHistory => {
    //   const body = {
    //     memberId: this.selectedMember.Id,
    //     startDate: this.withdrawalsSearchForm.value.fromDate.date.year + '-' + this.withdrawalsSearchForm.value.fromDate.date.month + '-' + this.withdrawalsSearchForm.value.fromDate.date.day,
    //     endDate: this.withdrawalsSearchForm.value.toDate.date.year + '-' + this.withdrawalsSearchForm.value.toDate.date.month + '-' + this.withdrawalsSearchForm.value.toDate.date.day,
    //     type: 'CASHWITHDRAWAL'
    //   };
    //   return this.shopService.GetWithdrawalByMemberId(body).pipe(
    //     takeUntil(this.destroy$)
    //   )
    // }).pipe(
    //   takeUntil(this.destroy$)
    //   ).subscribe(
    //   res => {
    //     this.withdrawalHistory = res.Data;
    //     this.withdrawalHistory.forEach(item => {
    //       switch (item.WithdrawalType) {
    //         case 1:
    //           item.WithdrawalType = 'COMMON.CashWithdrawal'
    //           break;
    //         case 2:
    //           item.WithdrawalType = 'COMMON.PettyCash'
    //           break;
    //       }
    //     });
    //   }, err => {

    //   });

/*     this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
          // member.IntCustId = member.CustId; // Does not seem to do anything.
          this.searchPurchaceHistory();
          this.searchWithdrawalsHistory();
        }
      }
    ); */


    this.startDate = this.dateAdd(new Date(), -1, 'months');
    this.endDate = new Date();

    this.printForm = this.fb.group({
      'startDate': [{ date: Extension.GetFormatteredDate(this.startDate) }],
      'endDate': [{ date: Extension.GetFormatteredDate(this.endDate) }]
    });

  }

  searchPurchaceHistory() {
    const body = {
      memberId: this.selectedMember.Id,
      fromDate: this.purchaseSearchForm.value.fromDate.date.year + '-' + this.purchaseSearchForm.value.fromDate.date.month + '-' + this.purchaseSearchForm.value.fromDate.date.day,
      toDate: this.purchaseSearchForm.value.toDate.date.year + '-' + this.purchaseSearchForm.value.toDate.date.month + '-' + this.purchaseSearchForm.value.toDate.date.day
    };
    this.memberShopService.GetMemberPurchaseHistory(body).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.purchaseHistory = res.Data.ShopSalesItemList;
        this.purchaseHistory.forEach(item => {
          switch (item.ItemType.trim()) {
            case 'OP':
              item.ItemType = 'MEMBERSHIP.OCR';
              break;

            case 'DC':
              item.ItemType = 'MEMBERSHIP.PtDC';
              break;

            case 'TMN':
              item.ItemType = 'MEMBERSHIP.PtTMN';
              break;

            case 'CSH':
              item.ItemType = 'MEMBERSHIP.PtCSH';
              break;

            case 'BS':
              item.ItemType = 'MEMBERSHIP.PtBS';
              break;

            case 'OA':
              item.ItemType = 'MEMBERSHIP.PtOA';
              break;

            case 'GC':
              item.ItemType = 'MEMBERSHIP.PtGC';
              break;

            case 'LOP':
              item.ItemType = 'MEMBERSHIP.OCR';
              break;

            case 'NOR':
              item.ItemType = 'MEMBERSHIP.NOR';
              break;

            case 'RD':
              item.ItemType = 'MEMBERSHIP.RD';
              break;

            case 'BNK':
              item.ItemType = 'MEMBERSHIP.PtBank';
              break;

            case 'PB':
              item.ItemType = 'MEMBERSHIP.PrepaidBalance';
              break;
          }
        });
        this.filterdSales = this.purchaseHistory;
      }, err => {

      });
  }

  searchWithdrawalsHistory() {
    const body = {
      memberId: this.selectedMember.Id,
      startDate: this.withdrawalsSearchForm.value.fromDate.date.year + '-' + this.withdrawalsSearchForm.value.fromDate.date.month + '-' + this.withdrawalsSearchForm.value.fromDate.date.day,
      endDate: this.withdrawalsSearchForm.value.toDate.date.year + '-' + this.withdrawalsSearchForm.value.toDate.date.month + '-' + this.withdrawalsSearchForm.value.toDate.date.day,
      type: 'CASHWITHDRAWAL'
    };
    this.shopService.GetWithdrawalByMemberId(body).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.withdrawalHistory = res.Data;
        this.withdrawalHistory.forEach(item => {
          switch (item.WithdrawalType) {
            case 1:
              item.WithdrawalType = 'COMMON.CashWithdrawal'
              break;
            case 2:
              item.WithdrawalType = 'COMMON.PettyCash'
              break;
          }
        });
      }, err => {

      });
  }

  reloadHistory(params) {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.purchaseHistory = items);
    }
  }

  openShopAccount() {
    this.router.navigate(['membership/card/' + this.selectedMember.Id + '/' + this.branchId + '/' + MemberRole[this.selectedMember.Role] + '/shop-account']);
  }

  viewReceiptDetails(content, item): void {
    this.modalService.open(content)
    this.saleItem = item;
    if (item.ShopSalesId) {
      PreloaderService.showPreLoader();
      this.shopService.GetShopSaleBillDetail({ saleId: item.ShopSalesId })
      .pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        this.receiptDetails = result.Data.SalesItemList;
        this.receiptDetails.forEach(element => {
          element.PurchaseDate = new Date(element.PurchaseDate)
        });
        this.itemCount = this.receiptDetails.length;
        this.itemResource = new DataTableResource(this.receiptDetails);
        this.itemResource.count().then(count => this.itemCount = count);
        this.saleItemTotal = result.Data.SalesItemList.reduce((a, b) => a + b.TotalAmount, 0);
        PreloaderService.hidePreLoader();


      },
        error => {
          PreloaderService.hidePreLoader();

        });
    }
  }

  openGiftCardModel(content, item): void {
    this.modalService.open(content, { width: '300' })
    const expDate = new Date(item.ExpiryDate)
    this.addGiftVoucherForm.patchValue(
      {
        'VoucherNumber': item.VoucherNo,
        'Price': item.UnitPrice,
        'PurchasedPrice': item.TotalAmount,
        'ExpiryDate': {
          date: {
            year: expDate.getFullYear(),
            month: expDate.getMonth() + 1,
            day: expDate.getDate()
          }
        }
      });
  }

  printShopBill(sale): void {
    if (sale.ShopSalesId) {
      this.shopService.GetShopSaleBillDetail({ saleId: sale.ShopSalesId }).pipe(takeUntil(this.destroy$)).subscribe(result => {
        const saleDetail = result.Data;
        const saleList = saleDetail.SalesItemList;
        const paymentList = saleDetail.SalesPaymentList;

        const sessionKey = Extension.createGuid();
        let receiptText = '';
        receiptText = receiptText + 'MEMBER$' + saleDetail.CustId +
          '|INVNO$' + saleDetail.InvoiceNo +
          '|USER$' + this.exceLoginService.CurrentUser.username +
          '|DATE$' + this.datePipe.transform(sale.SaleDate, 'MM/dd/yyyy') + ' ' +  this.datePipe.transform(sale.SaleDate, 'HH:mm') + '|';

        if (saleList != null) {
          receiptText = receiptText + 'ITEM$';
          saleList.forEach(article => {
            receiptText = receiptText + article.ItemName + ':' +
              article.Quantity + ':' +
              this.amountConverterPipe.transform(article.DefaultAmount) + ':' +
              this.amountConverterPipe.transform(article.Discount) + ':' +
              this.amountConverterPipe.transform(article.VatRate) + ':' +
              this.amountConverterPipe.transform(article.TotalAmount) + ':';
              this.amountConverterPipe.transform(article.GiftVoucherNumber) + ';';
          });

        }

        if (paymentList != null) {
          receiptText = receiptText + '|PAYMENT$';
          paymentList.forEach(payment => {

            let tempPaymentType: string;

            switch (payment.PaymentCode) {
              case 'CASH':
                tempPaymentType = 'Kontant';
                break;
              case 'CARD':
                tempPaymentType = 'Bankkort';
                break;
              case 'BANKTERMINAL':
                tempPaymentType = 'Bankkort';
                break;
              case 'VIPPS':
                tempPaymentType = 'Vipps';
                break;
              case 'CASHWITHDRAWA':
                tempPaymentType = 'Kontantuttak';
                break;
              case 'EMAILSMSINVOICE':
                tempPaymentType = 'Epost/SMS-Faktura';
                break;
              case 'OCR':
                tempPaymentType = 'OCR';
                break;
              case 'BANKSTATEMENT':
                tempPaymentType = 'Melding Bank';
                break;
              case 'DEBTCOLLECTION':
                tempPaymentType = 'Innfordring';
                break;
              case 'ONACCOUNT':
                tempPaymentType = 'Akonto';
                break;
              case 'NEXTORDER':
                tempPaymentType = 'Neste Ordre(' + payment.SaleDate + ')';
                break;
              case 'GIFTCARD':
                tempPaymentType = 'Gavekort';
                break;
              case 'INVOICE':
                tempPaymentType = 'Faktura';
                break;
              case 'SHOPINVOICE':
                tempPaymentType = 'shop Faktura';
                break;
              case 'PREPAIDBALANCE':
                tempPaymentType = 'Forhåndsbetalt';
                break;
            }
            receiptText = receiptText + tempPaymentType + ':' + payment.Amount.toFixed(2) + ';';
          });
        }
        receiptText = receiptText + '|';
        this.shopService.AddSessionValue({ sessionKey: sessionKey, value: receiptText }).pipe(takeUntil(this.destroy$)).subscribe(
          res => {
            this.exceSessionService.AddSession('REC', 'START', sessionKey, this.exceLoginService.CurrentUser.username);
          },
          error => {
            this.exceMessageService.openMessageBox('WARNING',
              {
                messageTitle: 'WARNING',
                messageBody: 'COMMON.ErrorInAddToSession'
              });
          });

      },
        error => {

        });
    }
  }

  onFromDateChange(date, end) {
    this.toDatePickerOptions = {
      disableUntil: date.date
    };
    end.showSelector = true;
  }

  onFromwDateChange(date, end) {
    this.toDatePickerOptions = {
      disableUntil: date.date
    };
    end.showSelector = true;
  }

  changePaymentType(paymentType?: any) {
    this.filterPaymentType = paymentType;
    this.filterSalesHistory();
  }

  filterByArticleNo(value) {
    this.filterArticleNo = value;
    this.filterSalesHistory();
  }

  filterByArticleName(value) {
    this.filterArticleName = value;
    this.filterSalesHistory();
  }

  filterSalesHistory() {
    this.filterdSales = this.purchaseHistory;
    if (this.filterPaymentType === 'ALL' &&
      !(this.filterArticleNo && this.filterArticleNo.trim().length > 0) &&
      !(this.filterArticleName && this.filterArticleName.trim().length > 0)) {
      this.filterdSales = this.purchaseHistory;
    }
    if (this.filterPaymentType !== 'ALL') {
      this.filterdSales = this.filterpipe.transform('ItemType', 'SELECT', this.filterdSales, this.filterPaymentType);
    }
    if (this.filterArticleNo) {
      this.filterdSales = this.filterpipe.transform('ArticleId', 'NAMEFILTER', this.filterdSales, this.filterArticleNo);
    }
    if (this.filterArticleName) {
      this.filterdSales = this.filterpipe.transform('ItemName', 'NAMEFILTER', this.filterdSales, this.filterArticleName);
    }
  }

  openShopMOdal(content) {
    this.shopHomeService.$isCancelBtnHide = true;
    this.selectedMember.BranchID = this.selectedMember.BranchId;
    this.shopHomeService.$selectedMember = this.selectedMember;
    this.shopView = this.modalService.open(content);
    this.shopHomeService.$isCancelBtnHide = true;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.durationSelectModel) {
      this.durationSelectModel.close();
    }
    if (this.shopView) {
      this.shopView.close();
    }
  }

  closeShop() {
    this.shopView.close();
    this.searchPurchaceHistory();
  }

  printPurchasePopup(content) {
    this.durationSelectModel = this.modalService.open(content, { width: '300' });
  }

  closeDurationSelectPopup() {
    this.durationSelectModel.close();
  }

  printShopHistory() {
    let startDate = this.manipulatedate(this.printForm.value.startDate);
    let endDate = this.manipulatedate(this.printForm.value.endDate);
    this.memberShopService.printPurchaseHistory(this.selectedMember.Id, startDate, endDate).pipe(takeUntil(this.destroy$)).subscribe(result => {
      PreloaderService.hidePreLoader();
      if (result.Data.FileParth.trim() !== '') {
        this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
      }
    });
    this.durationSelectModel.close();
  }

  dateAdd(startDate: any, count: any, interval: string): Date {
    return moment(startDate).add(count, interval).toDate()
  }

  manipulatedate(dateInput: any) {
    if (dateInput.date != undefined) {
      let tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }


}
