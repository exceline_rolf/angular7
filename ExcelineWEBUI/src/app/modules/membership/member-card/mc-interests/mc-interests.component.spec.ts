import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McInterestsComponent } from './mc-interests.component';

describe('McInterestsComponent', () => {
  let component: McInterestsComponent;
  let fixture: ComponentFixture<McInterestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McInterestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McInterestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
