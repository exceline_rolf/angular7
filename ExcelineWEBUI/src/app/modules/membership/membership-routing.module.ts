import { NgModule, Inject } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IAppConfig } from '../../app-config/app-config.interface';
import { APP_CONFIG } from '../../app-config/app-config.constants';
import { ExceToolbarService } from '../common/exce-toolbar/exce-toolbar.service';
import { MemberHomeComponent } from './member-home/member-home.component';
import { MemberCardComponent } from './member-card/member-card.component';
import { McEconomyComponent } from './member-card/mc-economy/mc-economy.component';
import { McInfoWithNotesComponent } from './member-card/mc-info-with-notes/mc-info-with-notes.component';
import { McBasicInfoComponent } from './member-card/mc-basic-info/mc-basic-info.component';
import { McContractsComponent } from './member-card/mc-contract/mc-contracts/mc-contracts.component';
import { McPaymentsComponent } from './member-card/mc-payments/mc-payments.component'
import { McBookingComponent } from './member-card/mc-booking/mc-booking.component'
import { McClassesComponent } from './member-card/mc-classes/mc-classes.component'
import { McXtraLogComponent } from './member-card/mc-xtra-log/mc-xtra-log.component';
import { McEventLogComponent } from './member-card/mc-event-log/mc-event-log.component';
import { McIntroducedMembersComponent } from './member-card/mc-introduced-members/mc-introduced-members.component';
import { McContractDetailsComponent } from './member-card/mc-contract/mc-contract-details/mc-contract-details.component';
import { McCommunicationLogComponent } from './member-card/mc-communication-log/mc-communication-log.component';
import { McInterestsComponent } from './member-card/mc-interests/mc-interests.component';
import { McVisitComponent } from './member-card/mc-visit/mc-visit.component';
import { McOrdersDetailsComponent } from './member-card/mc-orders/mc-orders-details/mc-orders-details.component';
import { McOrdersHomeComponent } from './member-card/mc-orders/mc-orders-home/mc-orders-home.component';
import { McDocumentsComponent } from './member-card/mc-documents/mc-documents.component';
import { McFollowUpComponent } from './member-card/mc-follow-up/mc-follow-up.component';
import { McResignComponent } from './member-card/mc-resign/mc-resign.component';
import { ResignContractComponent } from './member-card/mc-resign/resign-contract/resign-contract.component';
import { OrderDetailsViewComponent } from './member-card/mc-resign/resign-contract/order-details-view/order-details-view.component';
import { McTrainingProgramComponent } from './member-card/mc-training-program/mc-training-program.component';
import { McFreezeHomeComponent } from './member-card/mc-freeze/mc-freeze-home/mc-freeze-home.component';
import { FreezeComponent } from './member-card/mc-freeze/mc-freeze-home/freeze/freeze.component';
import { NewFreezeComponent } from './member-card/mc-freeze/mc-freeze-home/freeze/new-freeze/new-freeze.component';
import { McSponsoringComponent } from './member-card/mc-sponsoring/mc-sponsoring.component';
import { McInvoicesComponent } from './member-card/mc-invoices/mc-invoices.component';
import { McShopComponent } from './member-card/mc-shop/mc-shop.component';
import { McShopAccountComponent } from './member-card/mc-shop/mc-shop-account/mc-shop-account.component';
import { FreezeDetailsComponent } from './member-card/mc-freeze/mc-freeze-home/freeze/freeze-details/freeze-details.component';
import { TranslateService } from '@ngx-translate/core';
import { McContractRegComponent } from './member-card/mc-contract/mc-contract-reg/mc-contract-reg.component';
import { McInvoiceDetailsComponent } from './member-card/mc-invoices/mc-invoice-details/mc-invoice-details.component';
import { McContractMoveOrdersComponent } from './member-card/mc-contract/mc-contract-move-orders/mc-contract-move-orders.component';
import { McContractRenewComponent } from './member-card/mc-contract/mc-contract-renew/mc-contract-renew.component';

const routes: Routes = [
  { path: 'invoice-details', component: McInvoiceDetailsComponent },
  { path: '', component: MemberHomeComponent },
  { path: 'freeze-details', component: FreezeDetailsComponent },
  {
    path: 'card/:Id/:BranchId/:Role', component: MemberCardComponent, /* data: {shouldReuse: false}, */
    children: [
      { path: '', redirectTo: 'infoWithNotes', pathMatch: 'full' },
      { path: 'infoWithNotes', component: McInfoWithNotesComponent },
      { path: 'orders', component: McOrdersHomeComponent },
      { path: 'contracts', component: McContractsComponent },
      { path: 'contracts/register-contract', component: McContractRegComponent },
      { path: 'contracts/:ContractId', component: McContractDetailsComponent },
      { path: 'contracts/:ContractId/contract-renew', component: McContractRenewComponent },
      { path: 'contracts/:ContractId/contract-move-orders', component: McContractMoveOrdersComponent },
      { path: 'contracts/:ContractId/resign', component: ResignContractComponent },
      { path: 'payments', component: McPaymentsComponent },
      { path: 'freeze', component: McFreezeHomeComponent },
      { path: 'resign', component: McResignComponent },
      { path: 'resign/:ContractId', component: ResignContractComponent },
      { path: 'followUp', component: McFollowUpComponent },
      { path: 'interests', component: McInterestsComponent },
      { path: 'visits', component: McVisitComponent },
      { path: 'booking', component: McBookingComponent },
      { path: 'classes', component: McClassesComponent },
      { path: 'interests', component: McInterestsComponent },
      { path: 'introduce-members', component: McIntroducedMembersComponent },
      { path: 'xtra-log', component: McXtraLogComponent },
      { path: 'communication-log', component: McCommunicationLogComponent },
      { path: 'event-log', component: McEventLogComponent },
      { path: 'documents', component: McDocumentsComponent },
      { path: 'training-program', component: McTrainingProgramComponent },
      { path: 'ecconomy', component: McEconomyComponent },
      { path: 'sponsoring', component: McSponsoringComponent },
      { path: 'invoices', component: McInvoicesComponent },
      { path: 'invoice/invoiceDetails', component: McInvoicesComponent },
      { path: 'purchase-history', component: McShopComponent },
      { path: 'shop-account', component: McShopAccountComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MembershipRoutingModule {
  constructor(
    private translate: TranslateService,
    private exceToolbarService: ExceToolbarService,
    @Inject(APP_CONFIG) private config: IAppConfig
  ) {
    const langs = [];
    this.config.LANGUAGES.map(ln => {
      langs.push(ln.ID);
    })
    this.translate.addLangs(langs);
    translate.setDefaultLang(this.config.LANGUAGE_DEFAULT.ID);

    const lang = this.exceToolbarService.getSelectedLanguage();
    this.translate.use(lang.id);
    this.exceToolbarService.langUpdated.subscribe(
      (res) => {
        this.translate.use(res.id);
      }
    );
  }
}
export const MembershipRoutingComponents = [
  MemberHomeComponent,
  MemberCardComponent,
  McEconomyComponent,
  McInfoWithNotesComponent,
  McBasicInfoComponent,
  McPaymentsComponent,
  McBookingComponent,
  McClassesComponent,
  McXtraLogComponent,
  McIntroducedMembersComponent,
  McEventLogComponent,
  // MemberNotificationDetailComponent,
  McContractDetailsComponent,
  McCommunicationLogComponent,
  McInterestsComponent,
  McVisitComponent,
  McOrdersDetailsComponent,
  McOrdersHomeComponent,
  McDocumentsComponent,
  McFollowUpComponent,
  McResignComponent,
  ResignContractComponent,
  OrderDetailsViewComponent,
  McTrainingProgramComponent,
  NewFreezeComponent,
  FreezeComponent,
  McFreezeHomeComponent,
  McSponsoringComponent,
  McInvoicesComponent,
  McShopComponent,
  FreezeDetailsComponent,
  McInvoiceDetailsComponent,
  McContractRenewComponent,
  McContractMoveOrdersComponent,
  McContractRegComponent
]
