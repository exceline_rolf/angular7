import { TestBed, inject } from '@angular/core/testing';

import { ExceTitleService } from './exce-title.service';

describe('ExceTitleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceTitleService]
    });
  });

  it('should be created', inject([ExceTitleService], (service: ExceTitleService) => {
    expect(service).toBeTruthy();
  }));
});
