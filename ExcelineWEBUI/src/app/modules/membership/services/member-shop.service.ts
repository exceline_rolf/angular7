import { CommonHttpService } from '../../../shared/services/common-http.service';
import { Injectable } from '@angular/core';
import { ConfigService } from '@ngx-config/core';
import { ExceLoginService } from 'app/modules/login/exce-login/exce-login.service';
import { Observable } from 'rxjs';

@Injectable()
export class MemberShopService {
  private memberApiUrl: string;
  private branchId: number;

  constructor(private commonHttpService: CommonHttpService, private config: ConfigService, private exceLoginService: ExceLoginService) {
    this.memberApiUrl = this.config.getSettings('EXCE_API.MEMBER');
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }

  GetMemberPurchaseHistory(params: any): Observable<any> {
    const url = this.memberApiUrl + '/Shop/GetMemberPurchaseHistory?fromDate='
      + params.fromDate + '&toDate=' + params.toDate + '&branchId=' + this.branchId + '&memberId=' + params.memberId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

   printPurchaseHistory(memberId: any, fromDate: any, toDate: any): Observable<any> {
    const url = this.memberApiUrl + '/PrintPurchaseHistory?branchId=' + this.branchId + '&memberId=' + memberId + '&fromDate=' + fromDate + '&toDate=' + toDate +'&branchID='+this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }



}
