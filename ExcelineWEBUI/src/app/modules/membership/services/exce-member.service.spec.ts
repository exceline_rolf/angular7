import { TestBed, inject } from '@angular/core/testing';

import { ExceMemberService } from './exce-member.service';

describe('ExceMemberService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceMemberService]
    });
  });

  it('should be created', inject([ExceMemberService], (service: ExceMemberService) => {
    expect(service).toBeTruthy();
  }));
});
