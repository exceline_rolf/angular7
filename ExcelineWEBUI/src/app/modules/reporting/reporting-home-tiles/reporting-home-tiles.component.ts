import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceLoginService } from '../../login/exce-login/exce-login.service';
import { UsTileMenuService } from '../../common/us-tile-menu/us-tile-menu.service';
import { UsBreadcrumbService } from '../../../shared/components/us-breadcrumb/us-breadcrumb.service';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-reporting-home-tiles',
  templateUrl: './reporting-home-tiles.component.html',
  styleUrls: ['./reporting-home-tiles.component.scss']
})
export class ReportingHomeTilesComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  selectedModule: any;
  modules: any;
  constructor(
    private exceLoginService: ExceLoginService,
    private usTileMenuService: UsTileMenuService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'COMMON.Reporting',
      url: '/reporting/home'
    });
  }

  ngOnInit() {
    if (this.usTileMenuService.SelectedModule) {
      this.selectedModule = this.usTileMenuService.SelectedModule;
    } else {
      this.usTileMenuService.getUserInfo(this.exceLoginService.CurrentUser.username).pipe(
        takeUntil(this.destroy$)
      ).subscribe(result => {
          this.modules = result.Data.ModuleList;
          this.selectedModule = this.modules.find(x => x.ID === 'ReportsModule');
        }, null, null);
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
