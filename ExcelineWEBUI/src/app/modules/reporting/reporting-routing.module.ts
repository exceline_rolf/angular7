import { NgModule, Inject } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ExceToolbarService } from '../common/exce-toolbar/exce-toolbar.service';
import { IAppConfig } from '../../app-config/app-config.interface';
import { APP_CONFIG } from '../../app-config/app-config.constants';
import { GymUsageReportSettingComponent } from './gym-usage-report-setting/gym-usage-report-setting.component';
import { ReportingHomeComponent } from './reporting-home/reporting-home.component';
import { ReportSettingComponent } from './report-admin/report-setting/report-setting.component';
import { EntityPropertiesSettingComponent } from './report-admin/entity-properties-setting/entity-properties-setting.component';
import { AddNewReportComponent } from './reporting-home/add-new-report/add-new-report.component'
import { ReportingEditQueryComponent } from './reporting-home/reporting-edit-query/reporting-edit-query.component';
import { ReportOperationsComponent } from './reporting-home/report-operations/report-operations.component';
import { ReportingHomeTilesComponent } from './reporting-home-tiles/reporting-home-tiles.component';
const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: ReportingHomeTilesComponent },
  { path: 'gym-usage-report-setting', component: GymUsageReportSettingComponent },
  { path: 'reporting-home', component: ReportingHomeComponent },
  { path: 'report-setting', component: ReportSettingComponent },
  { path: 'entity-properties-setting', component: EntityPropertiesSettingComponent },
  { path: 'add-new-report', component: AddNewReportComponent },
  { path: 'reporting-home/edit-report-query/:Id', component: ReportingEditQueryComponent },
  { path: 'reporting-home/report-operations/:Id', component: ReportOperationsComponent },
  { path: 'reporting-home/edit-report/:Id', component: AddNewReportComponent },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportingRoutingModule {
  constructor(
    private translate: TranslateService,
    private exceToolbarService: ExceToolbarService,
    @Inject(APP_CONFIG) private config: IAppConfig
  ) {
    const langs = [];
    this.config.LANGUAGES.map(ln => {
      langs.push(ln.ID);
    })
    this.translate.addLangs(langs);
    translate.setDefaultLang(this.config.LANGUAGE_DEFAULT.ID);

    const lang = this.exceToolbarService.getSelectedLanguage();
    this.translate.use(lang.id);
    this.exceToolbarService.langUpdated.subscribe(
      (res) => {
        console.log(res)
        this.translate.use(res.id);
      }
    );
  }
}

export const ReportRoutingComponents = [
  GymUsageReportSettingComponent,
  ReportingHomeComponent,
  ReportSettingComponent,
  EntityPropertiesSettingComponent,
  AddNewReportComponent,
  ReportingEditQueryComponent,
  ReportOperationsComponent
]
