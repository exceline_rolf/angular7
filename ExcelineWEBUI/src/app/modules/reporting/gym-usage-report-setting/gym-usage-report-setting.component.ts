import { Component, OnInit } from '@angular/core';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';

@Component({
  selector: 'app-gym-usage-report-setting',
  templateUrl: './gym-usage-report-setting.component.html',
  styleUrls: ['./gym-usage-report-setting.component.scss']
})
export class GymUsageReportSettingComponent implements OnInit {

  constructor(
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'COMMON.ReportSettingC',
      url: '/reporting/gym-usage-report-setting'
    });
  }

  ngOnInit() {
  }

}
