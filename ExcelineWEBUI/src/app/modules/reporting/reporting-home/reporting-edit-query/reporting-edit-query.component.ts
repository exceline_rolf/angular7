import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ReportingService } from '../../reporting-service'
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'reporting-edit-query',
  templateUrl: './reporting-edit-query.component.html',
  styleUrls: ['./reporting-edit-query.component.scss']
})
export class ReportingEditQueryComponent implements OnInit, OnDestroy {
  reportId: number;
  uneditablePart = '';
  editablePart = '';
  _queryEditablePart;
  ParameterList = [];
  headers = [];
  columns = [];
  tableData;
  tableDataJSON;
  private itemResource: any = 0;
  items = [];
  private itemCount = 0;
  private destroy$ = new Subject<void>();

  get QueryEditablePart() {
    return this._queryEditablePart
  }

  set QueryEditablePart(value) {
    this._queryEditablePart = value
    let matches = this._queryEditablePart.match('@(?<after>\s*\S*)')
    if (matches.length > 0) {
      for (let i = 0; i < matches.length; i++) {
        // ParameterData pd=new ParameterData();
        // pd.ParameterName=matches[i].Groups["after"].ToString().Trim();
        // pd.ParameterValue = ReportResource.ParameterValueText;
        const parameter = {
          ParameterName: matches[i].toString(),
          ParameterValueText: matches[i]
        }
        this.ParameterList.push(parameter);
      }
    }
  }

  currentReport: any;
  constructor(private reportingService: ReportingService,
    private route: ActivatedRoute,
    private modalService: UsbModal) {
  }

  ngOnInit() {
    this.reportId = this.route.snapshot.params['Id'];
    this.reportingService.GetReportById(this.reportId).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        this.currentReport = result.Data
        this.uneditablePart = this.currentReport.QueryForReport.split('FROM')[0] + ' FROM'
        this.editablePart = this.currentReport.QueryForReport.split('FROM')[1]
      }
    }, error => {

    })
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  openTestModal(content: any) {
    //  this.selectedItem=event.row.item
    this.viewOperations()
    this.modalService.open(content, { width: '800' });
  }

  viewOperations() {
    let _hasUserParameters = false;
    // foreach (var whereCondition in _currentProcessingReport.WhereConditionList)
    // {
    //     if (whereCondition.Value.StartsWith("@"))
    //     {
    //         _hasUserParameters = true;
    //         break;
    //     }
    // }

    for (const whereCondition of this.currentReport.WhereConditionList) {
      if (whereCondition.Value.startsWith('@')) {
        _hasUserParameters = true;
        break;
      }
    }

    if (_hasUserParameters) {
      // ViewOperation qev = new ViewOperation(_currentProcessingReport);
      // qev.ViewModel.CurruntProccessingReport = _currentProcessingReport;
      // qev.ViewModel.ReportSuccessfullySavedEvent1 += new ReportSuccessfullySaved1(viewModel_ReportSuccessfullySavedEvent1);
      // qev.ViewModel.CloseReportSaveDialogEvent1 += new CloseEditedQueryReportSaveDialog(viewModel_CloseReportSaveDialogEvent1);
      // //_viewBox = new ReportingCommonLightBox();
      // //_viewBox.Title = "View Operation Result";
      // //_viewBox.Width = 1000;
      // //_viewBox.Height = 500;
      // //_viewBox.Content = qev;

      // if (_homeControl != null) {
      //   _homeControl.SubOperationOpened((IUSSOperation)qev);
      // }
    } else {
      // USContentPanel.ViewProgressBar(ReportResource.DataRetrievingProgress);
      // _progressTesting = new USGMSPrograssBar();
      // _progressTesting.Show();
      let QueryFixedPart = this.currentReport.QueryForReport.split('FROM')[0] + ' FROM';
      //     let QueryEditablePart = this.currentReport.QueryForReport.split('FROM')[1];
      for (let i = this.currentReport.EntityList.length - 1; i >= 0; i--) {
        if (QueryFixedPart.includes(this.currentReport.EntityList[i].PrimaryColumn)) {
          QueryFixedPart = this.GetPrimaryColumnToBegining(this.currentReport.EntityList[i].PrimaryColumn, QueryFixedPart);
        } else {
          QueryFixedPart = this.AddPrimaryColumnToBegining(this.currentReport.EntityList[i].PrimaryColumn, QueryFixedPart);
        }
      }
      this.reportingService.TestQuery({ query: QueryFixedPart + this.editablePart }).pipe(takeUntil(this.destroy$)).subscribe(testResults => {
        if (testResults.Data) {
          this.tableData = testResults.Data
          // this.tableData=this.tableData.filter(item)
          const headerTitles = this.tableData[0].split(';')
          headerTitles.forEach(element => {
            this.headers.push({ headerVal: element, text: '' })
          });

          headerTitles.forEach(head => {
            this.columns.push({ header: head })
          })
          this.tableData.splice(0, 1)
          this.tableDataJSON = []
          let jsonObj_1
          // this.tableData.forEach(element => {
          for (let j = 0; j < 100; j++) {
            if (this.tableData[j]) {
              const rowData = this.tableData[j].split(';')
              jsonObj_1 = {}
              for (let i = 0; i < headerTitles.length; i++) {
                jsonObj_1[headerTitles[i]] = rowData[i]
              }
              this.tableDataJSON.push(jsonObj_1)
            } else {
              break;
            }
          }
          this.itemResource = new DataTableResource(this.tableDataJSON);
          this.itemResource.count().then(count => this.itemCount = count);
          this.items = this.tableDataJSON;
          this.itemCount = Number(this.tableDataJSON.length);
        }
      }, error => {
        alert('Test unsuccesful')
      });
    }
  }

  AddPrimaryColumnToBegining(p, fixedPart): string {
    let finalString = '';
    const list1 = fixedPart.split('.');
    const list2 = list1[0].split(' ');
    // list2.insert(list2.length - 1, " ");
    // list2.insert(list2.length - 1, p);
    // list2.insert(list2.length - 1, " AS ");
    // list2.insert(list2.length - 1, p.Split('.')[1] + ",");
    // list2.insert(list2.length - 1, " ");
    list2.splice(list2.length - 1, 0, ' ');
    list2.splice(list2.length - 1, 0, p);
    list2.splice(list2.length - 1, 0, ' AS ');
    list2.splice(list2.length - 1, 0, p.split('.')[1] + ',');
    list2.splice(list2.length - 1, 0, ' ');

    list2.forEach(item => {
      finalString = finalString + ' ' + item;
    });
    for (let i = 1; i < list1.length; i++) {
      finalString = finalString + '.' + list1[i];
    }
    return finalString;
  }

  // Array.prototype.insert = function (index, item) {
  //   this.splice(index, 0, item);
  // };

  GetPrimaryColumnToBegining(p, fixedPart): string {
    // tslint:disable-next-line:max-line-length
    // string aa = "SELECT DISTINCT TOP 1000 ci.Name AS CreditorName,ci.AddrNo AS CreditorAddrNo,ci.CreditorInkassoID AS CreditorInkassoID,ci.Addr1 AS CreditorAddr1,di.Name AS DebtorName,di.Addr1 AS DebtorAddr1 FROM";
    // string bb = "ci.CreditorInkassoID";
    let finalString = '';
    let addFromWord = false;
    let dd = '';
    let indexOfItem = -1;
    const list0 = fixedPart.split(',');

    for (const item of list0) {
      if (item.includes(p)) {
        indexOfItem = list0.indexOf(item);
        break;
      }
    }
    if (indexOfItem === (list0.length - 1)) {
      addFromWord = true;
    }
    if (indexOfItem === 0) {
      return fixedPart;
    } else {
      // list0.RemoveAt(indexOfItem);
      list0.splice(indexOfItem, 1);
      list0.forEach(item => {
        if (dd !== '') {
          dd = dd + ',' + item;
        } else {
          dd = item;
        }
      });
      // foreach (var item in list0)
      // {
      //     dd = dd + "," + item;
      // }
      // dd = dd.trim(',');
      dd.trim()
      const list1 = dd.split('.');
      const list2 = list1[0].split(' ');
      // list2.insert(list2.length - 1, " ");
      // list2.insert(list2.length - 1, p);
      // list2.insert(list2.length - 1, " AS ");
      // list2.insert(list2.length - 1, p.Split('.')[1] + ",");
      // list2.insert(list2.length - 1, " ");
      list2.splice(list2.length - 1, 0, ' ');
      list2.splice(list2.length - 1, 0, p);
      list2.splice(list2.length - 1, 0, ' AS ');
      list2.splice(list2.length - 1, 0, p.split('.')[1] + ',');
      list2.splice(list2.length - 1, 0, ' ');

      list2.forEach(item => {
        finalString = finalString + ' ' + item;
      });
      // foreach (var item in list2)
      // {
      //     finalString = finalString + " " + item;
      // }
      for (let i = 1; i < list1.length; i++) {
        finalString = finalString + '.' + list1[i];
      }
      if (!addFromWord) {
        return finalString;
      } else {
        return finalString + 'FROM';
      }
    }
  }

  SaveQuery() {
    this.reportingService.TestQuery({ query: this.uneditablePart + this.editablePart }).pipe(takeUntil(this.destroy$)).subscribe(testResults => {
      if (testResults.Data) {
        this.currentReport.QueryForReport = this.uneditablePart + this.editablePart
        this.reportingService.SaveEditedReportConfiguration({ report: this.currentReport, fileName: '' }).pipe(takeUntil(this.destroy$)).subscribe(result => {
          alert('Success')
        })
      }
    }, error => {
      alert('Query Error')
    });
  }
}
