
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
// import { AdminService } from '.../services/admin.service';
import { ReportingService } from '../../reporting-service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { TreeNode } from 'app/shared/components/us-tree-table/tree-node';
import * as _ from 'lodash';
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from '@ngx-config/core';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { CANCELLED } from 'dns';
import { Subject } from 'rxjs';
import { takeUntil, catchError, map } from 'rxjs/operators';
@Component({
  selector: 'save-report-view',
  templateUrl: './save-report-view.component.html',
  styleUrls: ['./save-report-view.component.scss']
})
export class SaveReportViewComponent implements OnInit, OnDestroy {
  currentReport;
  currentUser;
  reportModal;
  reportUrl;
  reportServer;
  FixRport = true;
  selection = 'HOME';
  parentRoute = '/reporting/reporting-home/';
  currentFileEvent;
  ReportCategoryList;
  reportServiceUrl;
  reportCategories;
  uploadForm;
  userRoles;
  modelReference;
  formSubmited = false;
  modalReference;
  existingEditVal: string;
  private destroy$ = new Subject<void>();
  @Input() isReUpload = false;
  @Input() isUpload = false;
  @Input() isEdit = false;
  @Input() selectedItem: any;

  @Output() saveSuccess: EventEmitter<any> = new EventEmitter();
  @Output() canceled: EventEmitter<any> = new EventEmitter();
  formErrors = {
    'Name': '',
    'Category': ''
    // 'Description': '',
  };
  validationMessages = {
    'Name': { 'required': 'Name is required.', },
    'Category': { 'required': 'Category is required.', }
    // 'Description': { 'required': 'Description is required.', },
  };

  constructor(
    private modalService: UsbModal,
    private reportingService: ReportingService,
    private fb: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private exceLoginService: ExceLoginService,
    private config: ConfigService,
    private http: HttpClient,
  ) {
    this.reportServiceUrl = this.config.getSettings('EXCE_API.REPORTING');
    this.currentUser = this.exceLoginService.CurrentUser
  }

  ngOnInit() {
    const tempWhereConditionList = []
    this.selectedItem.WhereConditionList.forEach(element => {
      const newEl = {
        EntityPropertyForLeftOperand: element.EntityPropertyForLeftOperand,
        Operator: element.Operator,
        EntityPropertyForRightOperand: element.EntityPropertyForRightOperand,
        Conjunction: element.Conjunction,
        IsChecked: element.IsChecked
      }
      tempWhereConditionList.push(newEl);
    });
    this.selectedItem.WhereConditionList = tempWhereConditionList;
    this.uploadForm = this.fb.group({
      'Name': [null, [Validators.required]],
      'Category': [null, [Validators.required]],
      'Description': [null]
    });
    // tslint:disable-next-line:no-shadowed-variable
    this.uploadForm.statusChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(_ => UsErrorService.onValueChanged(this.uploadForm, this.formErrors, this.validationMessages));
    this.getData();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.modelReference) {
      this.modelReference.close();
    }
  }

  getData() {
    this.reportingService.GetAllReportCategories().pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result.Data) {
        this.reportCategories = result.Data;
        if ((this.isReUpload && this.selectedItem) || (this.isEdit && this.selectedItem)) {
          this.currentReport = this.selectedItem;
          this.userRoles = this.selectedItem ? this.selectedItem.RoleList : [];
          this.uploadForm.patchValue(this.selectedItem)
        } else {
          this.reportingService.GetAllRoles().pipe(
            takeUntil(this.destroy$)
            ).subscribe(roles => {
            if (roles.Data) {
              this.userRoles = roles.Data;
            }
          })
        }
      }
    })
  }

  fileChangeEvent(event) {
    this.currentFileEvent = event
  }

  submitForm(value, isSaveAs) {
    this.formSubmited = true;
    if (this.uploadForm.valid) {
      if (this.isUpload) {
        this.fileUpload(value, isSaveAs);
        this.formSubmited = false;
      } else {
        this.saveReport(value, isSaveAs);
      }
    } else {
      UsErrorService.validateAllFormFields(this.uploadForm, this.formErrors, this.validationMessages);
    }
  }

  saveReport(value, isSaveAs) {
    this.selectedItem.Description = value.Description
    this.selectedItem.Name = value.Name
    this.selectedItem.RoleList = this.userRoles
    this.selectedItem.Category = value.Category

    if (this.isEdit) {
      this.reportingService.SaveEditedReportConfiguration({ report: this.selectedItem, fileName: '' }).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result.Data) {
          this.saveSuccess.emit()
        }
      })

    } else {
      if (isSaveAs) {
        this.reportingService.SaveAsReportConfiguration({ report: this.selectedItem, fileName: '' }).pipe(takeUntil(this.destroy$)).subscribe(result => {
          if (result.Data) {
            this.saveSuccess.emit()
          }
        })
      } else {
        this.reportingService.SaveReportConfiguration({ report: this.selectedItem, fileName: '' }).pipe(takeUntil(this.destroy$)).subscribe(result => {
          if (result.Data) {
            this.saveSuccess.emit()
          }
        })
      }
    }
  }

  fileUpload(value, isSaveAs) {
    let _reportToBeSaved
    if (this.isReUpload) {
      _reportToBeSaved = this.currentReport
      _reportToBeSaved.Category = value.Category
      _reportToBeSaved.RoleList = this.userRoles
      _reportToBeSaved.Name = value.Name
      _reportToBeSaved.Description = value.Description

    } else {
      _reportToBeSaved = {
        Category: value.Category,
        RoleList: this.userRoles,
        Name: value.Name,
        Description: value.Description,
        FixReport: true
      }
    }

    if (this.currentFileEvent) {
      let fileList: FileList = this.currentFileEvent.target.files;
      if (fileList.length > 0) {
        let file: File = fileList[0];
        let rdlFileName = file.name
        let formData: FormData = new FormData();
        formData.append('uploadFile', file, file.name);
        let token = JSON.parse(Cookie.get('currentUser')).token
        let headers = new HttpHeaders();
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Authorization', 'bearer ' + token);
        headers.append('UserName', JSON.parse(Cookie.get('currentUser')).username);
        headers.append('Category', value.Category);
        // headers.append('CustId', this.selectedMember.CustId);
        let options = { headers: headers };
        let apiUrl1 = this.reportServiceUrl + 'UploadReportApi';

        this.http.post(apiUrl1, formData, options).pipe(
          map(res => res),
          catchError(error => throwError(error)),
          takeUntil(this.destroy$)
        ).subscribe(data => {
          if (data) {
            //  this.saveDocumentInfo(data.Data, value, file)
            if (isSaveAs) {
              this.reportingService.SaveAsReportConfiguration({
                report: _reportToBeSaved,
                fileName: rdlFileName
              }).pipe(takeUntil(this.destroy$)).subscribe(result => {
                if (result.Data) {
                  this.saveSuccess.emit()
                  // this.closeUploadModal()
                }
              })
            } else {
              this.reportingService.SaveReportConfiguration({
                report: _reportToBeSaved,
                fileName: rdlFileName
              }).pipe(takeUntil(this.destroy$)).subscribe(result => {
                if (result.Data) {
                  this.saveSuccess.emit()
                  // this.closeUploadModal()
                }
              })
            }
          }
        })

        // this.http.post(apiUrl1, formData, options)
        //   .map(res => res)
        //   .catch(error => Observable.throw(error))
        //   .pipe(takeUntil(this.destroy$)).subscribe(
        //     data => {
        //       if (data) {
        //         //  this.saveDocumentInfo(data.Data, value, file)
        //         if (isSaveAs) {
        //           this.reportingService.SaveAsReportConfiguration({
        //             report: _reportToBeSaved,
        //             fileName: rdlFileName
        //           }).pipe(takeUntil(this.destroy$)).subscribe(result => {
        //             if (result.Data) {
        //               this.saveSuccess.emit()
        //               // this.closeUploadModal()
        //             }
        //           })
        //         } else {
        //           this.reportingService.SaveReportConfiguration({
        //             report: _reportToBeSaved,
        //             fileName: rdlFileName
        //           }).pipe(takeUntil(this.destroy$)).subscribe(result => {
        //             if (result.Data) {
        //               this.saveSuccess.emit()
        //               // this.closeUploadModal()
        //             }
        //           })
        //         }
        //       }
        //     })
      }
    }
  }

  // openUplloadModel(content, isReupload?, item?) {
  //   this.reportingService.GetAllReportCategories().pipe(takeUntil(this.destroy$)).subscribe(result => {
  //     if (result.Data) {
  //       this.reportCategories = result.Data;
  //       if (isReupload) {
  //         this.isReUpload = true
  //         this.currentReport = item;
  //         this.userRoles = item ? item.RoleList : []
  //         this.uploadForm.patchValue(item)
  //         this.modelReference = this.modalService.open(content, { width: '1000' });
  //       } else {
  //         this.isReUpload = false
  //         this.reportingService.GetAllRoles().pipe(takeUntil(this.destroy$)).subscribe(roles => {
  //           if (roles.Data) {
  //             this.userRoles = roles.Data;
  //             this.modelReference = this.modalService.open(content, { width: '1000' });
  //           }
  //         })
  //       }
  //     }
  //   })

  // }

  closeUploadModal() {
    this.canceled.emit()
  }

  openAddEditCategory(content, editVal?) {
    if (editVal) {
      this.existingEditVal = editVal
    }
    this.modelReference = this.modalService.open(content, { width: '440' });
  }

  addCategory(value) {
    this.reportingService.SaveCategory(value).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        this.reportCategories.push(value)
        this.closeCategoryModal()
      }
    })
  }

  editCategory(value) {
    this.reportingService.UpdateCategory({ oldName: this.existingEditVal, newName: value }).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        this.reportCategories = this.reportCategories.filter(x => x !== this.existingEditVal)
        this.reportCategories.push(value)
        this.closeCategoryModal()
      }
    })
  }

  closeCategoryModal() {
    this.modelReference.close()
  }

















}
