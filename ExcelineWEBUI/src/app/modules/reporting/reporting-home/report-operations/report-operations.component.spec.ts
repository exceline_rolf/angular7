import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportOperationsComponent } from './report-operations.component';

describe('ReportOperationsComponent', () => {
  let component: ReportOperationsComponent;
  let fixture: ComponentFixture<ReportOperationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportOperationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportOperationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
