
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ReportingService } from '../../reporting-service'
import { headersToString } from 'selenium-webdriver/http';
import { element } from 'protractor';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { IMyDpOptions } from '../../../../shared/components/us-date-picker/interfaces';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const now = new Date();

@Component({
  selector: 'app-add-new-report',
  templateUrl: './add-new-report.component.html',
  styleUrls: ['./add-new-report.component.scss']
})
export class AddNewReportComponent implements OnInit, OnDestroy {

  selectedEntity: any;
  statusValue: any;
  statusOperator: any;
  leftOperand: any;
  delModalRef: any;
  deletedItem: any;
  reportScheduleList: any;
  selectedCatIntVal: number;
  hideFields: boolean;
  reportScheduleForm: FormGroup;
  selectedTemplate: any;
  addTemplateModalRef: any;
  templateList?: any[] = [];
  selectedCategory: any;
  DateList = [];
  reportId: number
  headers = []
  columns = []
  tableData
  tableDataJSON
  private itemResource: any = 0;
  private items = [];
  private itemCount = 0;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  filteredData
  currentReport: any;
  EntityList = []
  selectedEntityList = []
  AllProperties = []
  modelReference
  conditionList = []
  _operatorList = []
  userRoles
  isEdit = false;
  myFormSubmited = false;
  private destroy$ = new Subject<void>();
  fromDatePickerOptions: IMyDpOptions = {
    disableUntil: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() - 1 }
  };

  toDatePickerOptions: IMyDpOptions = {
  };

  formErrors = {
    'ScheduleRecurrence': '',
    'ScheduleStartDate': '',
    'ScheduleEnddate': '',
    'ScheduleItemCategory': '',
    'ReportPrimaryEntity': '',
    'ScheduleItemTemplate': ''
  };

  validationMessages = {
    'ScheduleRecurrence': {
      'required': 'REPORT.ScheduleRecurrenceNotSelected'
    },
    'ScheduleStartDate': {
      'required': 'REPORT.ScheduleStartDateEmpty'
    },
    'ScheduleEnddate': {
      'required': 'REPORT.ScheduleEndDateEmpty'
    },
    'ScheduleItemCategory': {
      'required': 'REPORT.ScheduleCategoryNotSelected'
    },
    'ReportPrimaryEntity': {
      'required': 'REPORT.ScheduleEntityNotSelected'
    },
    'ScheduleItemTemplate': {
      'required': 'REPORT.ScheduleTemplateEmpty'
    }
  };
  index = 0;

  constructor(
    private translateService: TranslateService,
    private fb: FormBuilder,
    private reportingService: ReportingService,
    private route: ActivatedRoute,
    private modalService: UsbModal,
    private router: Router,
    private exceMessageService: ExceMessageService
  ) {
    this._operatorList.push('=');
    this._operatorList.push('!=');
    this._operatorList.push('>');
    this._operatorList.push('<');
    this._operatorList.push('>=');
    this._operatorList.push('<=');
    this._operatorList.push('LIKE');
    this._operatorList.push('Start With');
    this._operatorList.push('End With');
    this._operatorList.push('IN');
    this._operatorList.push('IS NULL OR EMPTY');
    this._operatorList.push('(Length) =');
    this._operatorList.push('(YEAR) =');
    this._operatorList.push('(MONTH) =');
    this._operatorList.push('(DAY) =');
    this._operatorList.push('NOT IN');

    this.reportScheduleForm = this.fb.group({
      'ScheduleRecurrence': [null, [Validators.required]],
      'ScheduleMonth': [null],
      'ScheduleDay': [null],
      'ScheduleDate': [null],
      'ScheduleTime': [null],
      'ScheduleStartDate': [null, [Validators.required]],
      'ScheduleStartTime': [null],
      'ScheduleEnddate': [null, [Validators.required]],
      'ScheduleEndTime': [null],
      'IsUntillFurtherNotice': [null],
      'ScheduleItemCategory': [null, [Validators.required]],
      'ReportPrimaryEntity': [null, [Validators.required]],
      'ScheduleItemTemplate': [null, [Validators.required]],
      'ScheduleItemRecipient': [null],
      'ScheduleItemRecipientRole': [null],
      'FollowupStartTime': [null],
      'FollowupEndTime': [null],
      'Text': [null],
      'Description': [null]
    });

    this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      this.reportScheduleYesHandler();
    });

  }

  ngOnInit() {
    // this.reportId = this.route.snapshot.params['Id'];
    this.reportScheduleForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => {
      UsErrorService.onValueChanged(this.reportScheduleForm, this.formErrors, this.validationMessages)
    });
    this.dateList();
    this.reportingService.GetAllEntitiesWithProperties().pipe(takeUntil(this.destroy$)).subscribe(entities => {
      if (entities.Data) {
        this.EntityList = entities.Data
        this.EntityList.forEach(ent => {
          ent.isSelect = false;
        });
        this.reportingService.GetAllRoles().pipe(takeUntil(this.destroy$)).subscribe(roles => {
          if (roles.Data) {
            this.userRoles = roles.Data;
            // tslint:disable-next-line:no-shadowed-variable
            this.userRoles.forEach(element => {
              element.IsEditable = true
              element.IsView = true
            });
            this.reportId = this.route.snapshot.params['Id'];
            if (this.reportId && this.reportId > 0) {
              this.reportingService.GetReportById(this.reportId).pipe(takeUntil(this.destroy$)).subscribe(result => {
                if (result.Data) {
                  this.currentReport = result.Data
                  this.isEdit = true;
                  if (this.currentReport) {
                    this.selectedEntityList = this.currentReport.EntityList
                    this.selectedEntityList.forEach(entity => {
                      this.AllProperties = this.AllProperties.concat(entity.Properties);
                    })
                    this.conditionList = this.currentReport.WhereConditionList;
                    // if (this.conditionList.length > 0) {
                    //   this.conditionList.forEach(ele => {
                    //     ele.EntityPropertyForLeftOperand = ele.EntityPropertyForLeftOperand.DataColumnReturnName
                    //   });
                    // }

                    this.currentReport.EntityList.forEach(element => {
                      // if(this.EntityList.filter(x => x.Id == element.Id)[0]){
                      //   this.EntityList.filter(x => x.Id == element.Id)[0].isSelect=true
                      // }
                      this.EntityList.forEach(entity => {
                        if (element.Id === entity.Id) {
                          entity.isSelect = true
                        }
                      })
                    });
                    // this.uneditablePart = this.currentReport.QueryForReport.split('FROM')[0] + ' FROM'
                    // this.editablePart = this.currentReport.QueryForReport.split('FROM')[1]
                  }
                }
              }, error => {
                alert('Error')
              }, null)
            }
          }
        })
      }
    })
  }

  ngOnDestroy () {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.addTemplateModalRef) {
      this.addTemplateModalRef.close();
    }
    if (this.modelReference) {
      this.modelReference.close();
    }
  }

  getTemplateList() {
    const dataObj = {
      category: { Id: this.selectedCatIntVal, DisplayName: this.selectedCategory },
      entity: this.selectedEntity
    }
    this.reportingService.GetScheduleTemplateList(dataObj).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.templateList = result.Data;
      }
    });
  }

  getSelectedEntity(event) {
    this.selectedEntity = event.target.value;
  }

  openAddTemplateView(content) {
    if (this.selectedCategory != null && this.reportScheduleForm.value.ReportPrimaryEntity != null) {
      this.getTemplateList();
      this.addTemplateModalRef = this.modalService.open(content, { width: '750' });
    } else if (this.selectedCategory == null && this.reportScheduleForm.value.ReportPrimaryEntity == null) {
      let messageBody = '';
      this.translateService.get('REPORT.ScheduleCategoryEntityNotSelected').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceliene', messageBody: messageBody, });
    } else if (this.selectedCategory == null) {
      let messageBody = '';
      this.translateService.get('REPORT.ScheduleCategoryNotSelected').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: messageBody });
    } else if (this.reportScheduleForm.value.ReportPrimaryEntity == null) {
      let messageBody = '';
      this.translateService.get('REPORT.ScheduleEntityNotSelected').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: messageBody });
    }
  }

  rowDoubleClickItemSelect(event) {
    this.reportScheduleForm.controls['ScheduleItemTemplate'].setValue(event.row.item.TemplateName);
    this.addTemplateModalRef.close();
  }

  rowClickItemSelect(event) {
    this.selectedTemplate = event.row.item
  }

  rowClickPatchItem() {
    this.reportScheduleForm.controls['ScheduleItemTemplate'].setValue(this.selectedTemplate.TemplateName);
    this.addTemplateModalRef.close();
  }

  enititySelect(event, entity) {
    if (event.target.checked) {
      this.selectedEntityList.push(entity)
    } else {
      this.selectedEntityList = this.selectedEntityList.filter(x => x !== entity)
    }

    this.selectedEntityList.forEach(entity => {
      this.AllProperties = this.AllProperties.concat(entity.Properties);
    })
  }

  addNewClasue() {
    // if (this.conditionList.length == 1) {
    // }
    if (this.conditionList.length > 0) {
      this.conditionList.push({
        // EntityPropertyForLeftOperand: {
        //   DataColumnName: '',
        //   DataColumnReturnName: (this.leftOperand == undefined) ? this.AllProperties[0] : this.leftOperand,
        //   InConditionFixValue: true
        // },
        Index: this.index,
        EntityPropertyForLeftOperand: (this.leftOperand == undefined) ? this.AllProperties[0] : this.leftOperand,
        Operator: '',
        EntityPropertyForRightOperand: { UserParameterPromptNane: '', ConditionFixValue: '', InConditionFixValue: true },
        Conjunction: 'AND',
        IsChecked: false
      })
    } else {
      this.conditionList.push({
        Index: this.index,
        EntityPropertyForLeftOperand: null,
        Operator: '',
        EntityPropertyForRightOperand: { UserParameterPromptNane: '', ConditionFixValue: '', InConditionFixValue: true },
        Conjunction: '',
        IsChecked: false
      });
    }
    this.index = this.index + 1;
  }

  getLeftOperand(event, condition) {
    if (event.target.value === undefined) {
      this.leftOperand = this.AllProperties.filter(x => x.DataColumnReturnName == 'INV____MemberID');
    } else {
      // this.leftOperand = event.target.value;
      this.AllProperties.forEach(el => {
        // console.log(el.DataColumnReturnName.toString())
        if (el.DataColumnReturnName.toString() === event.target.value.toString()) {
          this.leftOperand = el;
        }
      });
    }
    this.conditionList.forEach(cond => {
      if (cond.Index === condition.Index) {
        cond.EntityPropertyForLeftOperand = this.leftOperand;
      }
    });
  }

  deleteClause(item) {
    this.conditionList = this.conditionList.filter(x => x !== item);
  }

  filterInput() {
    this.filteredData = this.tableDataJSON
    // if (branchValue && branchValue !== 'ALL') {
    //   this.filteredData = this.filterpipe.transform('BranchId', 'SELECT', this.filteredData, branchValue);
    // }
    this.headers.forEach(element => {
      if (element.text && element.text !== '') {
        this.filteredData = this.filterpipe.transform(element.headerVal, 'NAMEFILTER', this.filteredData, element.text);
      }
    })
    // if (val && val !== '') {
    //   this.filteredData = this.filterpipe.transform(prop, 'NAMEFILTER', this.filteredData, val);
    // }
    // if (catValue && catValue !== 'ALL') {
    //   this.filteredData = this.filterpipe.transform('TaskCategoryId', 'SELECT', this.filteredData, catValue);
    // }

    this.itemResource = new DataTableResource(this.filteredData);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredData);
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  openSaveReport(content) {
    // if (this.conditionList.length > 1) {
    //   // tslint:disable-next-line:no-shadowed-variable
    //   this.statusOperator = this.conditionList[0].slice(1).filter(x => x.Operator == '');
    //   this.statusValue = this.conditionList.slice(1).filter(x => x.EntityPropertyForRightOperand.ConditionFixValue == ''); // EntityPropertyForRightOperand

    //   if (this.statusOperator.length > 0) {
    //     let messageTitle = '';
    //     let messageBody = '';
    //     (this.translateService.get('REPORT.TitleError').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
    //     (this.translateService.get('REPORT.ConjunctionLeftOperandOperatorEmpty').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
    //     this.delModalRef = this.exceMessageService.openMessageBox('ERROR', { messageTitle: messageTitle, messageBody: messageBody });
    //   } else {
    //     if (this.statusValue.length == 0) {
    //       this.modelReference = this.modalService.open(content, { width: '1000' });
    //     }
    //   }
    // }

    if (this.isEdit) {
      this.currentReport.FixReport = false;
      this.currentReport.EntityList = this.selectedEntityList;
      this.currentReport.WhereConditionList = this.conditionList;
    } else {
      this.currentReport = {
        FixReport: false,
        EntityList: this.selectedEntityList,
        WhereConditionList: this.conditionList
      }
    }
    this.modelReference = this.modalService.open(content, { width: '1000' });

  }

  backToReports() {
    const routeUrl = '/reporting/reporting-home'
    this.router.navigate([routeUrl]);
  }


  openSchedule(content) {
    this.reportScheduleForm.reset();
    this.formPatchValues();

    this.hideFields = true;
    this.modelReference = this.modalService.open(content);
  }
  saveSuccess() {
    this.modelReference.close()
    const routeUrl = '/reporting/reporting-home'
    this.router.navigate([routeUrl]);
  }

  formPatchValues() {
    this.reportScheduleForm.patchValue({
      'ScheduleTime': '00:00',
      'ScheduleStartTime': '00:00',
      'ScheduleEndTime': '00:00',
      'FollowupStartTime': '08:30',
      'FollowupEndTime': '10.30'
    });
  }

  closeUploadModal() {
    this.modelReference.close();
    this.selectedCategory = null;
    this.selectedTemplate = null;
    // this.reportScheduleList = [];
    this.reportScheduleForm.reset();
    this.myFormSubmited = false;
  }

  getRecurrence(event) {
    if (event.target.value == 'Yearly') {
      this.reportScheduleForm.controls['ScheduleDay'].disable();
      this.reportScheduleForm.controls['ScheduleMonth'].enable();
      this.reportScheduleForm.controls['ScheduleDate'].enable();
    } else if (event.target.value == 'Monthly') {
      this.reportScheduleForm.controls['ScheduleMonth'].disable();
      this.reportScheduleForm.controls['ScheduleDay'].disable();
      this.reportScheduleForm.controls['ScheduleDate'].enable();
    } else if (event.target.value == 'Weekly') {
      this.reportScheduleForm.controls['ScheduleMonth'].disable();
      this.reportScheduleForm.controls['ScheduleDate'].disable();
      this.reportScheduleForm.controls['ScheduleDay'].enable();
    } else if (event.target.value == 'Daily') {
      this.reportScheduleForm.controls['ScheduleMonth'].disable();
      this.reportScheduleForm.controls['ScheduleDay'].disable();
      this.reportScheduleForm.controls['ScheduleDate'].disable();
    }
  }

  getMonth(event) {
  }

  getDay(event) {
  }

  dateList() {
    for (let i = 1; i <= 31; i++) {
      this.DateList.push(i);
    }
  }

  getDate(date) {
  }

  // get from date change
  fromDateChange(event, toDate) {
    // event.date = new Date();
    this.toDatePickerOptions = {
      // disableUntil: event.date
      disableUntil: { year: event.date.year, month: event.date.month, day: event.date.day - 1 }
    };
    toDate.showSelector = true;
  }

  getCategoryChange(event) {
    this.selectedCatIntVal = event.target.value;
    if (event.target.value == 1) {
      this.selectedCategory = 'Follow-Up';
    }
    if (event.target.value == 2) {
      this.selectedCategory = 'SMS';
    }
    if (event.target.value == 3) {
      this.selectedCategory = 'Email';
    }
    if (event.target.value == 4) {
      this.selectedCategory = 'Print';
    }
    if (this.selectedCategory === 'Follow-Up') {
      this.hideFields = false;
    } else {
      this.hideFields = true;
    }
  }

  getSelectedRole(item) {
  }

  untilNoticedChange(event) {
  }

  GetScheduleFormValues(value) {
    this.myFormSubmited = true;

    if (this.reportScheduleForm.valid) {
      value.ScheduleStartDate = this.getDateConverter(value.ScheduleStartDate.date) + ' ' + value.ScheduleStartTime;
      value.ScheduleEnddate = this.getDateConverter(value.ScheduleEnddate.date) + ' ' + value.ScheduleEndTime;
      value.ScheduleItemCategory = this.selectedCategory;

      if (value.ScheduleStartDate == undefined) {
        value.ScheduleStartDate = this.getDateConverter(this.reportScheduleForm.controls['ScheduleStartDate'].value.date + ' ' + value.ScheduleStartTime);
      }
      if (value.ScheduleEnddate == undefined) {
        value.ScheduleEnddate = this.getDateConverter(this.reportScheduleForm.controls['ScheduleEnddate'].value.date + ' ' + value.ScheduleEndTime);
      }

      this.reportScheduleList = [value];
      this.reportScheduleForm.reset();
      this.formPatchValues();
      this.myFormSubmited = false;
    } else {
      UsErrorService.validateAllFormFields(this.reportScheduleForm, this.formErrors, this.validationMessages);
    }
  }

  // convert date js object in to string
  getDateConverter(datee: any) {
    const dd = (datee.day < 10 ? '0' : '') + datee.day;
    const MM = ((datee.month + 1) < 10 ? '0' : '') + datee.month;
    const yyyy = datee.year;
    return (dd + '/' + MM + '/' + yyyy);
  }

  deleteSelectedSchedule(item) {
    this.deletedItem = item;
    let messageTitle = '';
    let messageBody = '';
    this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue);
    this.translateService.get('REPORT.RecordDeleteMsg').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
    this.delModalRef = this.exceMessageService.openMessageBox('CONFIRM', { messageTitle: messageTitle, messageBody: messageBody });
  }

  reportScheduleYesHandler() {
    const index: number = this.reportScheduleList.indexOf(this.deletedItem);
    if (index !== -1) {
      this.reportScheduleList.splice(index, 1);
    }
  }

  editSelectedSchedule(item) {
    item.ScheduleStartDate = item.ScheduleStartDate.split(' ')[0];
    item.ScheduleEnddate = item.ScheduleEnddate.split(' ')[0];
    const StartDate = this.dateConverter(item.ScheduleStartDate);
    const Enddate = this.dateConverter(item.ScheduleEnddate);
    if (item.ScheduleItemCategory == 'Follow-Up') {
      item.ScheduleItemCategory = 1;
    }
    if (item.ScheduleItemCategory == 'SMS') {
      item.ScheduleItemCategory = 2;
    }
    if (item.ScheduleItemCategory == 'Email') {
      item.ScheduleItemCategory = 3;
    }
    if (item.ScheduleItemCategory == 'Print') {
      item.ScheduleItemCategory = 4;
    }
    item.ScheduleStartDate = {
      date: {
        year: StartDate.getFullYear(),
        month: StartDate.getMonth() + 1,
        day: StartDate.getDate()
      }
    }
    item.ScheduleEnddate = {
      date: {
        year: Enddate.getFullYear(),
        month: Enddate.getMonth() + 1,
        day: Enddate.getDate()
      }
    }
    const index: number = this.reportScheduleList.indexOf(item);
    if (index !== -1) {
      this.reportScheduleList.splice(index, 1);
    }
    this.reportScheduleForm.patchValue(item);
  }

  // convert date object in to string
  dateConverter(dateObj: any) {
    let dateS = '';
    if (dateObj) {
      const splited = dateObj.split('/')
      if (splited.length > 1) {
        dateS = splited[2] + '-' + splited[1] + '-' + splited[0] + 'T00:00:00'
      }
    }
    return new Date(dateS);
  }
}
