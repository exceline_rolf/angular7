import { Injectable, EventEmitter } from '@angular/core';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators'
import { ConfigService } from '@ngx-config/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import { CommonHttpService } from '../../../shared/services/common-http.service';
import { IBranch } from 'app/modules/login/interfaces/IBranch';


@Injectable()
export class ExceLoginService {
  memberApiURL: any;
  loginApi: any;
  sessionServiceURL: string;
  private token: string;
  private userInfo: any;
  private loggedUserDetails: any;
  private currentUser: any;
  private machineName: string = 'DEFAULT';

  branchSelected: Subject<IBranch> = new Subject<IBranch>();
  userLogingOut: Subject<boolean> = new Subject<boolean>();

  private isUserLoggedIn: boolean = false;
  private isBranchSelected: boolean = false;
  private loginApiUrl: string;
  private selectedBranch: IBranch;
  private companyCode: string;

  constructor(
    private commonHttpService: CommonHttpService,
    private config: ConfigService) {
    this.loginApiUrl = this.config.getSettings('EXCE_API.LOGIN');
    this.loginApi = this.config.getSettings('EXCE_API.SESSION');
    this.memberApiURL = this.config.getSettings('EXCE_API.MEMBER');
  }

  login(username: string, password: string): Observable<boolean> {
    const data = new HttpParams()
      .append('username', username)
      .append('password', password)
      .append('grant_type', 'password')
    const headers = new HttpHeaders()
      .append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');

    return this.commonHttpService.makeHttpCall(this.loginApiUrl + 'token', {
      headers: headers,
      method: 'POST',
      body: data.toString(),
    });
  }

  setToken(val) {
    this.token = val;
  }

  setHttpServiceUserToNull() {
    this.commonHttpService.CurrentUser = null;
  }
  // Send unauthorized call to database with potential username as payload.
  requestNewPassword(gymCode: string, PotentialUserName: string): Observable<any> {
    const url = this.loginApiUrl + 'api/Login/AnonymousUserPasswordReset?gymCode=' + gymCode + '&PotentialUserName=' + gymCode + '/' + PotentialUserName;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: false
    });
  }

  sendNewPWForConfirmation(resetInfo: any): Observable<any> {
    const url = this.loginApiUrl + 'api/Login/SetNewUserPassword';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: resetInfo
    })
  }

  GetLoggedUserDetails(): Observable<any> {
    const url = this.memberApiURL + 'api/login/GetLoggedUserDetails';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }
  getNewPass(userName: string): Observable<any> {
    const url = this.memberApiURL + '/getNewPass?userName=' + userName;
    return this.commonHttpService.GetSessionValue(url, {
      method: 'GET'}, userName, false);
  }

  loginSuccess(response: any, username: string): boolean {
    // login successful if there's a jwt token in the response
    const token = response && response.access_token;
    if (token) {
      // set token property
      this.token = token;
      this.isUserLoggedIn = true;

      // store username and jwt token in local storage to keep user logged in between page refreshes
      Cookie.set('currentUser', JSON.stringify({ username: username, token: token }), this.config.getSettings('COOKIE_EXPIRATION.DAYS'));
      Cookie.set('isUserLoggedIn', this.isUserLoggedIn.toString(), this.config.getSettings('COOKIE_EXPIRATION.DAYS'));

      // return true to indicate successful login
      this.CurrentUser = { username: username, token: token };
      this.commonHttpService.CurrentUser = { username: username, token: token };
      return this.isUserLoggedIn;
    } else {
      // return false to indicate failed login
      this.isUserLoggedIn = false;
      return this.isUserLoggedIn;
    }
  }

  logout(): void {
    let newToken;
    this.isUserLoggedIn = false;
    this.token = newToken;
    newToken = 'used';
    this.CurrentUser = null;
    this.commonHttpService.CurrentUser = null;
    this.userLogingOut.next(true);
    // this.token = null;
    Cookie.delete('currentUser');
    Cookie.set('isUserLoggedIn', 'false')
    // Cookie.deleteAll();
  }

  setIsBranchSelected(branch: IBranch, isSelected: boolean): void {
    this.SelectedBranch = branch;
    this.isBranchSelected = isSelected;
  }

  getIsBranchSelected(): boolean {
    return this.isBranchSelected;
  }

  set SelectedBranch(branch: IBranch) {
    console.log(branch)
    this.branchSelected.next(branch);
    Cookie.set('selectedBranch', JSON.stringify(branch), this.config.getSettings('COOKIE_EXPIRATION.DAYS'));
    this.selectedBranch = branch;
  }

  get SelectedBranch(): IBranch {
    if (this.config.getSettings('ENV') === 'DEV') {
      const selectedbra = Cookie.get('selectedBranch');
      return JSON.parse(Cookie.get('selectedBranch'));
    } else {
      return this.selectedBranch;
    }
  }

  set UserInfo(userInfo) {
    this.userInfo = userInfo;
  }

  get UserInfo(): any {
    return this.userInfo;
  }

  set LoggedUserDetails(loggedUserDetails) {
    Cookie.set('loggedUserDetails', JSON.stringify(loggedUserDetails), this.config.getSettings('COOKIE_EXPIRATION.DAYS'));
    this.loggedUserDetails = loggedUserDetails;
  }

  get LoggedUserDetails(): any {
    if (this.config.getSettings('ENV') === 'DEV') {
      return JSON.parse(Cookie.get('loggedUserDetails'));
    } else {
      return this.loggedUserDetails;
    }
  }

  set IsUserLoggedIn(val: boolean) {
    this.isUserLoggedIn = val;
  }

  get IsUserLoggedIn(): boolean {
    if (this.config.getSettings('ENV') === 'DEV') {
      return Cookie.get('isUserLoggedIn') ? Boolean(Cookie.get('isUserLoggedIn')) : false;
    } else {
      return this.isUserLoggedIn;
    }
  }

  set CurrentUser(user) {
    this.currentUser = user;
  }

  get CurrentUser() {
    if (this.config.getSettings('ENV') === 'DEV') {
      return JSON.parse(Cookie.get('currentUser'));
    } else {
      return this.currentUser;
    }
  }

  set MachineName(machineName: string) {
    this.machineName = machineName;
  }

  get MachineName(): string {
    return this.machineName;
  }

  get CompanyCode() {
    return this.companyCode;
  }

  set CompanyCode(companyCode) {
    this.companyCode = companyCode;
  }

  // getSessionValue(sessionKey: string, userName: string, count: any) {
  //   const url = this.loginApi + '/GetSession?sessionKey=' + sessionKey + '&userName=' + userName + '&count=' + count;
  //   return this.commonHttpService.GetSessionValue(url, {
  //     method: 'GET',
  //     auth: true
  //   });
  // }

  getSessionValue(sessionKey: string, userName: string, count: any): Observable<any> {
    const url = this.memberApiURL + '/GetSessionValue?sessionKey=' + sessionKey
    return this.commonHttpService.GetSessionValue(url, {
      method: 'GET',
      auth: true
    }, userName, false);
  }
}
