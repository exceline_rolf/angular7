export interface IBranch {
    AccountNo: string;
    BranchId: number,
    BranchName: string,
    Id: number,
    IsDeleted: boolean,
    IsExpressGym: boolean,
    UserId: number
}