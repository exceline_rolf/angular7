import { TestBed, inject } from '@angular/core/testing';

import { ExceCommonService } from './exce-common.service';

describe('ExceCommonService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceCommonService]
    });
  });

  it('should be created', inject([ExceCommonService], (service: ExceCommonService) => {
    expect(service).toBeTruthy();
  }));
});
