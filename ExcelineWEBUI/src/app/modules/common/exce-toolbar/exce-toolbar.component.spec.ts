import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceToolbarComponent } from './exce-toolbar.component';

describe('ExceToolbarComponent', () => {
  let component: ExceToolbarComponent;
  let fixture: ComponentFixture<ExceToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
