import { Inject, Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { ExceToolbarService } from '../exce-toolbar/exce-toolbar.service';
import { FormGroup, FormControl, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { UsbModal } from '../../../shared/components/us-modal/us-modal';
import { IAppConfig } from '../../../app-config/app-config.interface';
import { APP_CONFIG } from '../../../app-config/app-config.constants';
import { UsTileMenuService } from '../us-tile-menu/us-tile-menu.service';
import { ExceLoginService } from '../../login/exce-login/exce-login.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { PreloaderService } from '../../../shared/services/preloader.service';
import { TranslateService } from '@ngx-translate/core';
import { ExceCommonService } from '../services/exce-common.service';
import { ExceMessageService } from '../../../shared/components/exce-message/exce-message.service';
import { Subscription } from 'rxjs';
import { CommonEncryptionService } from '../../../shared/services/common-Encryptor-Decryptor';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AdminService } from 'app/modules/admin/services/admin.service';

@Component({
  selector: 'exce-toolbar',
  templateUrl: './exce-toolbar.component.html',
  styleUrls: ['./exce-toolbar.component.scss']
})

export class ExceToolbarComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  model: void;
  closeResult: string;

  lang: any;
  languages: any;
  isShowSitemap: Boolean;
  userProfileForm: FormGroup;
  isResetPassword = false;
  modules: any[];
  userGyms: any[];
  selectedBranch: any;
  userInfo: any;
  openUserInfoModel: any;

  isFormSubmited = false;

  versionNo: string;
  // userModuleLoaded = new Subject<any>()

  formErrors = {
    'displayName': '',
    'password': '',
    'newPassword': '',
    'reTypePassword': ''
  };

  validationMessages = {
    'displayName': {
      'required': 'COMMON.PleaseEnterDisplayName'
    },
    'password': {
      'required': 'COMMON.PleaseEnterPassword',
      'misMatchOldPassword': 'COMMON.EnterOldPassword'
    },
    'newPassword': {
      'required': 'COMMON.PleaseEnterNewPassword'
    },
    'reTypePassword': {
      'required': 'COMMON.PleaseEnterConfirmPassword',
      'compare': 'COMMON.FailComparePassword'
    }
  };
  sortById: any;


  constructor(
    private router: Router,
    private modalService: UsbModal,
    private exceToolbarService: ExceToolbarService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private exceCommonService: ExceCommonService,
    private usTileMenuService: UsTileMenuService,
    private fb: FormBuilder,
    @Inject(APP_CONFIG) private config: IAppConfig,
    private _translate: TranslateService,
    private exceMessageService: ExceMessageService,
    private commonEncryptionService: CommonEncryptionService
  ) {
    this.isShowSitemap = false;
    this.userGyms = exceToolbarService.getUserBranches();
    this.selectedBranch = exceLoginService.SelectedBranch;
    this.isFormSubmited = false;

    exceCommonService.GetVersionNoList().pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      res => {
        this.versionNo = res.Data[0];
      }, null, null);
    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      if (value.id === 'LOGOUT_CONFIRM') {
        this.exceLoginService.logout();
      }
    });
  }


  ngOnInit() {
    this.userGyms = this.exceToolbarService.getUserBranches();
    this.adminService.getGymSettings('OTHER', null, this.selectedBranch.BranchId).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.sortById = result.Data[0].SortBranchesById;
        if (!this.sortById) {
          this.userGyms = this.sortBranches()
        }
      }
    }, null, null);
    this.isResetPassword = false;
    this.languages = this.config.LANGUAGES;

    this.lang = { id: this.config.LANGUAGE_DEFAULT.ID, title: this.config.LANGUAGE_DEFAULT.NAME, culture: this.config.LANGUAGE_DEFAULT.CULTURE }
    this.exceToolbarService.setSelectedLanguage(this.lang);

    this.userProfileForm = this.fb.group({
      userName: [{ value: null, disabled: true }],
      displayName: [null, [Validators.required]],
      email: [{ value: null, disabled: true }],
      isResetPassword: this.isResetPassword,
      password: [{ value: null, disabled: true }, [Validators.required]],
      newPassword: [{ value: null, disabled: true }, [Validators.required]],
      reTypePassword: [{ value: null, disabled: true }, [Validators.required], [this.comparePassword.bind(this)]]

    });

    this.usTileMenuService.getUserInfo(this.exceLoginService.CurrentUser.username).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
        this.exceLoginService.UserInfo = result.Data;
        this.modules = result.Data.ModuleList;
        this.usTileMenuService.setUserModules(this.modules);
        this.usTileMenuService.userModulesLoaded.next(this.modules);
        this.userInfo = result.Data;
        this.userProfileForm.patchValue({
          'displayName': this.userInfo.DisplayName,
          'email': this.userInfo.Email,
          'userName': this.userInfo.UserName
        });

      }, null, null);

    this.userProfileForm.statusChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(_ => UsErrorService.onValueChanged(this.userProfileForm, this.formErrors, this.validationMessages));
  }

  sortBranches() {
    const returnList = [];
    this.userGyms.forEach(gym => {
      const compareGym = gym.BranchName.split('_')[1];
      if (returnList.length === 0) {
        returnList.push(gym)
      } else {
        for (let i = 0; i < returnList.length; i++) {
          const tempGym = returnList[i].BranchName.split('_')[1];
          const val = compareGym.localeCompare(tempGym)
          if (val === -1) {
            returnList.splice(i, 0, gym);
            break
          } else if (i === returnList.length - 1) {
            returnList.push(gym);
            break
          }
        }
      }
    });
    return returnList;
  }

  comparePassword(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (
          this.userProfileForm.value.newPassword && this.userProfileForm.value.reTypePassword &&
          (this.userProfileForm.value.newPassword !== this.userProfileForm.value.reTypePassword)) {
          resolve({
            'compare': true
          })
        } else {
          resolve(null);
        }
      }, 10);
    })
  }

  open(content) {
    this.openUserInfoModel = this.modalService.open(content, { width: '500' });
  }

  isPasswordChangeHandle(value: boolean) {
    this.isResetPassword = value;
    if (value) {
      this.userProfileForm.get('password').enable();
      this.userProfileForm.get('newPassword').enable();
      this.userProfileForm.get('reTypePassword').enable();
    } else {
      this.userProfileForm.get('password').disable();
      this.userProfileForm.get('newPassword').disable();
      this.userProfileForm.get('reTypePassword').disable();
    }
  }

  closeModel() {
    this.isFormSubmited = false;
    this.openUserInfoModel.close();
  }

  submitForm() {
    this.isFormSubmited = true;
    if (this.userProfileForm.valid) {
      const body = {
        UserID: this.exceLoginService.UserInfo.UserID,
        DisplayName: this.userProfileForm.value.displayName,
        Email: this.exceLoginService.UserInfo.Email,
        isPasswdChange: this.isResetPassword,
        Password: this.isResetPassword ? this.commonEncryptionService.encryptValue(this.userProfileForm.value.newPassword) : null,
        IsLoginFirstTime: this.isResetPassword,
        loggedUser: this.exceLoginService.UserInfo.UserName
      }
      PreloaderService.showPreLoader();
      this.usTileMenuService.editUserInfo(body).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        res => {
          PreloaderService.hidePreLoader();
          this.openUserInfoModel.close();
        },
        error => {
          PreloaderService.hidePreLoader();
        });
    } else {
      UsErrorService.validateAllFormFields(this.userProfileForm, this.formErrors, this.validationMessages);
    }
  }

  changeLanguage(): void {
    // This method must change when another language came
    this.lang = (this.lang.id === this.config.LANGUAGE_DEFAULT.ID) ?
      { id: this.languages[1].ID, title: this.languages[1].NAME, culture: this.languages[1].CULTURE } :
      { id: this.languages[0].ID, title: this.languages[0].NAME, culture: this.languages[0].CULTURE };

    this.exceToolbarService.setSelectedLanguage(this.lang);
  }

  showSiteMap(): void {
    this.isShowSitemap = !this.isShowSitemap;
  }

  gotoHome(): void {
    this.router.navigateByUrl('/');
  }

  navigateToShop(): void {
    this.router.navigateByUrl('/shop');
  }

  navigateToCalandar(): void {
    this.router.navigateByUrl('/adminstrator/my-calandar-home');
  }

  navigate(data): void {
    this.router.navigateByUrl(data.OperationHomeURL);
  }

  navigateModule(module): void {
    this.router.navigateByUrl(module.ModuleHomeURL);
  }

  selectGym(gym): void {
    this.selectedBranch = gym;
    this.exceLoginService.SelectedBranch = gym;
    this.exceCommonService.UpdateSettingForUserRoutine({ key: 'InitialGym', value: gym.BranchId }).pipe(
      takeUntil(this.destroy$)
    ).subscribe(null, null, null);
    this.router.navigateByUrl('/');
  }

  logout() {
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'COMMON.Confirm',
        messageBody: 'COMMON.MsgLogOutConfirm',
        msgBoxId: 'LOGOUT_CONFIRM'
      }
    );

  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.openUserInfoModel) {
      this.openUserInfoModel.close();
    }
  }

}
