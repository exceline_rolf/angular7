import { TestBed, inject } from '@angular/core/testing';

import { ExceToolbarService } from './exce-toolbar.service';

describe('ExceToolbarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceToolbarService]
    });
  });

  it('should be created', inject([ExceToolbarService], (service: ExceToolbarService) => {
    expect(service).toBeTruthy();
  }));
});
