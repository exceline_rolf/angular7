import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsTileMenuComponent } from './us-tile-menu.component';

describe('UsTileMenuComponent', () => {
  let component: UsTileMenuComponent;
  let fixture: ComponentFixture<UsTileMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsTileMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsTileMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
