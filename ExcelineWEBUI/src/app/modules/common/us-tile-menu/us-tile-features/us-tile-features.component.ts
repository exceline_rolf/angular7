import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UsTileMenuService } from '../us-tile-menu.service';
import { UsBreadcrumbService } from '../../../../shared/components/us-breadcrumb/us-breadcrumb.service';
import { ExceBreadcrumbService } from 'app/shared/components/exce-breadcrumb/exce-breadcrumb.service';

@Component({
  selector: 'us-tile-features',
  templateUrl: './us-tile-features.component.html',
  styleUrls: ['./us-tile-features.component.scss'],
})
export class UsTileFeaturesComponent implements OnInit {
  @Input() selectedModule?: any;
  selectedOperation: any;
  selectedFeatureName: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private usTileMenuService: UsTileMenuService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {

    // breadcrumbService.hideRoute('/features');

  }

  ngOnInit() {
    // this.selectedModule = this.usTileMenuService.getModuleById(this.route.snapshot.params['id']);
    // if (this.selectedModule) {
    //   this.breadcrumbService.addFriendlyNameForRoute('/features/' + this.route.snapshot.params['id'], this.selectedModule.DisplayName);
    // } else {
    //   this.router.navigate(['/']);
    // }

  }

  navigateToHome(): void {
    this.router.navigateByUrl('/');
  }

  featureSelected(feature) {
    if (feature.FeatureId === 42)
    {
      window.open(
        [feature.FeatureHomeURL].toString(),'_blank')
    }
    else if (feature.Operations.length > 0 && feature.FeatureId !== 42) {
      this.selectedFeatureName = feature.DisplayName;
      this.selectedOperation = feature.Operations;
    } else {
      this.router.navigate([feature.FeatureHomeURL])
    }

  }

  operationSelected(operation) {
    this.router.navigate([operation.OperationHomeURL]);
  }

}
