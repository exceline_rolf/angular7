### Biggest changes

`TypeScript` removing vendor specific types. See Notes further down.

### Upgrade from 6 to 7

Make sure you have a working copy of Exceline with Angular 6 installed. See [this guide](https://bitbucket.org/exceline/usexceline/wiki/Upgrading%20from%20Angular%205%20to%206).
There's no need to delete the `node_modules` or `package-lock.json` this time.

Enter these commands and it should be a breeze to update from Angular 6 to 7:

~~~~bash
ng update @angular/cli@7.0.1
npm uninstall
  angular-split-ng6
npm install 
  @angular/animations@^7.0.1
  @angular/common@^7.0.1
  @angular/compiler@^7.0.1
  @angular/compiler-cli@^7.0.1
  @angular/core@^7.0.1
  @angular/forms@^7.0.1
  @angular/platform-browser@^7.0.1
  @angular/platform-browser-dynamic@^7.0.1
  @angular/platform-server@^7.0.1
  @angular/router@^7.0.1
  @ng-bootstrap/ng-bootstrap@^4.0.0
  @ngx-config/core@7.0.0
  @ngx-congig/http-loader@7.0.0
  angular-split@2.0.1
  typescript@3.2
~~~~

Change imports of `angular-split-ng6` to `angular-split` in

- `app.module`
- `admin.module.ts`
- `reporting.module.ts`
- `shop.module.ts`

As of `angular-split` v.1.0.0: prefix `as-` added to component/directive to follow best practises: `<as-split>` & `<as-split-area>`.[^1] Just `find-replace-all`:

~~~~html
<split> -> <as-split>
</split> -> </as-split>
<split-area> -> <as-split-area>
</split-area- -> </as-split-area>
~~~~

##### Compile/serve errors

**Error**

`Property 'msMatchesSelector' does not exist on type 'Element'` 

**Fix** [^2] [^3]

Since `TypeScript 3.1` removes some vendor specific types from their `lib.d.ts`, `msMatchesSelector` is no longer a property of `Element`. This error occurs in `app/shared/components/us-tree-table/dom-handler.ts` - `line 236`.

To fix this error, create a file named `dom.ie.d.ts` inside `app/shared/components/us-tree-table` with the following content:

~~~~ts
interface Element {
    msMatchesSelector(selectors: string): boolean;
}
~~~~

##### Runtime errors

**Error**

`Endpoint unreachable`

**Fix**

Check if you have the `src/assets/config.json` file. Without it no APIs are defined.

[^1]: https://bertrandg.github.io/angular-split/#/changelog
[^2]: https://stackoverflow.com/questions/52696154/latest-typescript-breaking-changes-in-lib-dom-ts-file#52697231
[^3]: https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#typescript-31