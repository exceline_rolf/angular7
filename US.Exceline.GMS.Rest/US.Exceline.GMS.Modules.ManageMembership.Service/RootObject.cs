﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IO.Swagger.Model;
using Newtonsoft.Json;

namespace US.Exceline.GMS.Modules.ManageMembership.Service
{
   public class RootObject
    {
        [JsonProperty("brukerDtos")]
        public List<BrukerDto> Results { get; set; }
    }
}
