﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Activation;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Common.Web.UI.Core.SystemObjects;
using US.Common.Web.UI.Core.SystemObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.Service
{
   
    [ServiceContract]
    public interface IManageMembershipService
    {
        [OperationContract]
        string SaveMember(OrdinaryMemberDC member, string user, int branchId, string notificationTitle);

          [OperationContract]
        bool UpdateExternalCustomerId(int memberId, int userId, string user);

        [OperationContract]
        SystemSettingEconomySettingDC DummyGetEconomySystemSetting();

        [OperationContract]
        List<USNotificationSummaryInfo> GetNotificationSummaryList(int memberId, NotificationStatusEnum status, string user);

        [OperationContract]
        string GetGymCompanySettings(string user, GymCompanySettingType gymCompanySettingType);

        [OperationContract]
        NotificationTemplateText GetNotificationTextByType(NotifyMethodType method, TextTemplateEnum type, string user, int branchId);

        [OperationContract]
        List<WithdrawalDC> GetWithdrawalByMemberId(DateTime startDate, DateTime endDate, WithdrawalTypes type, int memberId, string user);

        [OperationContract]
        List<USNotificationSummaryInfo> GetNotificationSearchResult(int memberId, string user, string title, string assignTo, DateTime? receiveDateFrom, DateTime? receiveDateTo, DateTime? dueDateFrom, DateTime? dueDateTo, List<int> severityIdList, List<int> typeIdList, int statusId);

        [OperationContract]
        List<OrdinaryMemberDC> GetGroupMemebersByGroup(int branchId, string searchText, bool IsActive, int groupId, string user);

        [OperationContract]
        USPUser ValidateUserWithCard(string cardNumber, string user);

        [OperationContract]
        bool ValidateUser(string userName, string password);

        [OperationContract]
        List<ExcelineBranchDC> GetBranches(string user, int branchId);

        [OperationContract]
        ShopBillDetailDC GetShopSaleBillDetail(int saleId, int branchId, string user);

        [OperationContract]
        List<ExcelineMemberDC> GetMembers(int branchId, string user, string searchText, int statuse, MemberSearchType searchType, MemberRole memberRole, int hit, bool isHeaderClick, bool isAscending, string sortName);

        [OperationContract]
        List<PackageDC> GetContracts(int branchId, int contractType, string userName);

        [OperationContract]
        ContractSaveResultDC SaveMemberContract(MemberContractDC memberContract, string user, int branchId, string notificationTitle);

        [OperationContract]
        int UpdateMemberContract(MemberContractDC memberContract,  string user, int branchId, string notificationTitle);

        [OperationContract]
        List<USNotificationSummaryInfo> GetNotificationByNotifiyMethod(int memberId, string user, int branchId, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod);

        [OperationContract]
        int AddCreditNote(CreditNoteDC creditNoteList, string user, int branchId);

        [OperationContract]
        CreditNoteDC GetCreditNoteDetails(int arItemNo, string user);

        [OperationContract]
        USNotificationSummaryInfo GetNotificationById(int notificationId, string user);

        [OperationContract]
        MemberContractDC GetMemberContractDetails(int contractId, string user, int branchId);

        [OperationContract]
        List<InstallmentDC> UpdateInstallmentsWithMemberContract(string user, decimal installmentAmount, int updatingInstallmentNo, MemberContractDC memberContract);

        [OperationContract]
        int FreezeMemberContract(int memberContractId, string user);

        [OperationContract]
        bool UpdateNotificationStatus(List<int> selectedIds, int status, string user);

        [OperationContract]
        string ResignContract(ContractResignDetailsDC resignDetails, string user, int branchId, string notificationMethod, string senderDescription);

        [OperationContract]
        string UpdateMember(OrdinaryMemberDC member, string user);

        [OperationContract]
        bool DisableMember(int memberId, string user, bool activeState, string comment);

        [OperationContract]
        List<ContractItemDC> GetContractItems(int contractId, int branchId, string user);

        [OperationContract]
        List<ActivityDC> GetActivities(int branchId, string user);

        [OperationContract]
        SalePointDC GetSalesPointByMachineName(int branchID, string machineName, string user);

        [OperationContract]
        NotificationStatusEnum GetNotificationStatus();

        [OperationContract]
        List<NotificationTemplateText> GetNotificationTemplateText(string user, int branchId);

        [OperationContract]
        List<InstallmentDC> GetInstallments(int branchId, string user, int memberContractId);

        [OperationContract]
        MemberContractDC RenewMemberContract(MemberContractDC renwedContract, List<InstallmentDC> installmenList, string user, int branchId);

        [OperationContract]
        bool UpdateMemberInstallment(bool isEditedFromUI, InstallmentDC installment, string user, int branchId, string notificationTitle);

        [OperationContract]
        EntityVisitDetailsDC GetMemberVisits(int memberId, DateTime selectedDate, int branchId, string user);

        [OperationContract]
        bool UpdateMemberAddonInstallments(List<InstallmentDC> installments, string user);

        [OperationContract]
        bool DeleteInstallment(List<int> installmentIdList, int memberContractId, string user);

        [OperationContract]
        List<InstallmentDC> GetMemberOrders(int memberId, string type, string user);

        [OperationContract]
        int SwitchPayer(int memberId, int payerId, DateTime activatedDate, bool isSave, string user);

        [OperationContract]
        int SaveMemberVisit(EntityVisitDC memberVisit, string user);

        [OperationContract]
        NotificationSeverityEnum GetSeverity();

        [OperationContract]
        bool DeleteMemberVisit(int memberVisitId, int memberContractId, string user);

        [OperationContract]
        bool DeleteCreditNote(int creditNoteId, string user);

        [OperationContract]
        bool GetSponsoringProcessStatus(string guiID,  string user);

        [OperationContract]
        bool IntroducePayer(int memberId, int introduceId, string user);

        [OperationContract]
        bool CancelMemberContract(int branchId, string user, int memberContractId, string canceledBy, string comment, List<int> installmentIds, int minNumber, int tergetInstallmentId);

        [OperationContract]
        List<MemberContractDC> GetMemberContractsForActivity(int memberID, int activityId, int branchId, string user);

        [OperationContract]
        List<ClassDetailDC> GetClassByMemberId(int branchId, string user, int memberId, DateTime? classDate);

        [OperationContract]
        bool AddFamilyMember(int memberId, int familyMemberId, int memberContractId, string user);

        [OperationContract]
        bool SaveSponsor(int memberId, int sponsorId, int branchId, string user);

        [OperationContract]
        SaleResultDC AddShopSales(InstallmentDC installment, ShopSalesDC shopSaleDetails, PaymentDetailDC paymentDetails, int memberBranchID, int loggedBranchID, string user, bool printInvoice);

        [OperationContract]
        string SaveMemberParent(OrdinaryMemberDC memberParent, int branchId, string user);

        [OperationContract]
        OrdinaryMemberDC GetMemberDetailsByMemberId(int branchId, string user, int memberId, string memberRole);

        [OperationContract]
        List<ExcelineMemberDC> GetGroupMembers(int groupId, string user);

        [OperationContract]
        List<ExcelineMemberDC> GetFamilyMembers(int memberId, string user);

        [OperationContract]
        List<FollowUpDC> GetFollowUps(int memberId, string user);

        [OperationContract]
        List<FollowUpTemplateTaskDC> GetFollowUpTask(string user);

        [OperationContract]
        List<USPRole> GetEmpRole(string user);

        [OperationContract]
        int SaveFollowUp(List<FollowUpDC> followUpList, string user);

        [OperationContract]
        List<ContractBookingDC> GetContractBookings(int branchId, string user, int membercontractId);

        [OperationContract]
        PDFPrintResultDC ViewInstallmentInvoice(int branchId, string user, string installmentId);

        [OperationContract]
        TemplateField GetTemplateFieldValue();

        [OperationContract]
        List<USNotificationSummaryInfo> GetNotificationsByMemberId(int branchId, string user, int memberId);

        [OperationContract]
        NotificationTypeEnum GetNotificationType();

        [OperationContract]
        List<CategoryDC> GetInterestCategoryByMember(string user, int memberId, int branchId);

        [OperationContract]
        int AddMemberNotification(USNotificationTree notification, string text);

        [OperationContract]
        bool SaveInterestCategoryByMember(string user, int memberId, int branchId, List<CategoryDC> interestCategoryList);

        [OperationContract]
        List<OrdinaryMemberDC> GetSwitchPayers(string user, int branchId, int memberId);

        [OperationContract]
        List<string> GetInstallmentInvoice(int branchId, string user, string installmentId);

        [OperationContract]
        OrdinaryMemberDC GetEconomyDetails(int memberId, string user);

        [OperationContract]
        List<ExcelineMemberDC> GetListOfGuardian(int guardianId, string user);

        [OperationContract]
        bool SaveEconomyDetails(OrdinaryMemberDC member, string user);

        [OperationContract]
        OrdinaryMemberDC GetMemberInfoWithNotes(int branchId, int memberId, string user);

        [OperationContract]
        string SaveMemberInfoWithNotes(OrdinaryMemberDC member, string gymCode);

        [OperationContract]
        List<GymEmployeeDC> GetGymEmployees(int branchId, string searchText, string user, int roleId);

        [OperationContract]
        GymEmployeeDC GetGymEmployeeById(int branchId, int employeeId, string user);

        [OperationContract]
        List<CategoryDC> GetCategories(string type, string user, int branchId);

        [OperationContract]
        List<CategoryTypeDC> GetCategoryTypes(string name, string user, int branchId);

        [OperationContract]
        int SaveCategory(CategoryDC category, string user, int branchId);

        [OperationContract]
        List<DiscountDC> GetGroupDiscountByType(int branchId, string user, int discountTypeId, int sponsorId);

        [OperationContract]
        List<ExcelineMemberDC> GetMembersBySponsorId(int sponsorId, int branchId, string user);

        [OperationContract]
        List<int> GetSponsoredMemberListByTimePeriod(int sponsorId, Dictionary<int, List<DateTime>> sponsorMembers, string user);

        [OperationContract]
        SponsorSettingDC GetSponsorSetting(int branchId, string user, int sponsorId);

        [OperationContract]
        bool SaveSposorSetting(SponsorSettingDC sponsorSetting, int branchId, string user);

        [OperationContract]
        bool RemoveSponsorshipOfMember(int sponsoredRecordId, string user);

        [OperationContract]
        bool AddSponserShipForMember(int memContractId, int sponserContractId, int employeeCategotyID, string user, int actionType, string employeeRef);

        [OperationContract]
        bool DeleteDiscountCategory(int categoryID, string user);

        [OperationContract]
        bool DeleteEmployeeCategory(int categoryID, string user);

        [OperationContract]
        List<OrdinaryMemberDC> GetMembersByRoleType(int branchId, string user, int status, MemberRole roleType, string searchText);

        [OperationContract]
        List<EmployeeCategoryDC> GetSponsorEmployeeCategoryList(int branchId, string user, int sponsorId);

        [OperationContract]
        List<ArticleDC> GetArticles(int branchId, string user, ArticleTypes categpryType, string keyword, CategoryDC category, bool isActive, bool filterByGym);

        [OperationContract]
        List<ActivityDC> GetActivitiesForEntity(int entityId, string entityType, string user, int branchId);

        [OperationContract]
        List<EntityActiveTimeDC> GetEntityActiveTimes(int branchid, DateTime startDate, DateTime endDate, List<int> entityList, string entityRoleType, string user);

        [OperationContract]
        bool UpdateActiveTime(EntityActiveTimeDC activeTime, string user,int branchId, bool isDelete);

        [OperationContract]
        List<ScheduleItemDC> GetScheduleItemsByScheduleId(int scheduleId, string user);

        [OperationContract]
        bool UpdateSheduleItems(ScheduleDC schedule, string user);

        [OperationContract]
        bool SaveTaskTemplate(TaskTemplateDC taskTemplate, bool isEdit, string user);

        [OperationContract]
        bool UpdateTaskTemplate(TaskTemplateDC taskTemplate, string user);

        [OperationContract]
        bool DeleteTaskTemplate(int taskTemplateId, string user);

        [OperationContract]
        bool DeActivateTaskTemplate(int taskTemplateId, string user);

        [OperationContract]
        List<TaskTemplateDC> GetTaskTemplates(TaskTemplateType templateType, int branchId, int templateId, string user);

        [OperationContract]
        List<FollowUpTemplateDC> GetFollowUpTemplates(string user, int branchId);

        [OperationContract]
        List<ExtendedTaskTemplateDC> GetTaskTemplateExtFields(int extFieldId, string user);

        [OperationContract]
        bool SaveExcelineTask(ExcelineTaskDC excelineTask, string user);

        [OperationContract]
        List<ExcelineTaskDC> GetExcelineTasks(int branchId, bool isFxied, int templateId, int assignTo, string roleType, int followupMemberId, string user);

        [OperationContract]
        List<InstructorDC> GetInstructors(int branchId, string searchText, string user, bool isActive);

        [OperationContract]
        List<TrainerDC> GetTrainers(int branchId, string searchText, string user, bool isActive);

        [OperationContract]
        bool UpdateExcelineTask(ExcelineTaskDC excelineTask, string user);

        [OperationContract]
        List<ExcelineTaskDC> SearchExcelineTask(string searchText, TaskTemplateType type, int followUpMemId, int branchId, bool isFxied, string user);

        [OperationContract]
        bool SaveTaskWithOutSchedule(ExcelineTaskDC excelineTask, string user);

        [OperationContract]
        bool AddMemberToGroup(List<int> memberIdList, int groupId, string user);

        [OperationContract]
        bool RemoveMemberFromGroup(List<int> memberList, int groupId, string user);

        [OperationContract]
        List<DiscountDC> GetDiscountList(int branchId, DiscountType discountType, string user);

        [OperationContract]
        List<DiscountDC> GetDiscountsForActivity(int branchId, int activityId, string user);

        [OperationContract]
        ShopSalesDC GetMemberPurchaseHistory(int memberId, DateTime fromDate, DateTime toDate, int branchId, string user);

        [OperationContract]
        string SaveContractFreezeItem(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> freezeInstallments, string user, int branchId, string notificationTitle, string notifyMethod, string senderDescription, string gymname);

        [OperationContract]
        List<ContractFreezeItemDC> GetContractFreezeItems(int memberContractId, string user);

        [OperationContract]
        List<ContractFreezeInstallmentDC> GetContractFreezeInstallments(int freezeItemId, string user);

        [OperationContract]
        List<InstallmentDC> GetInstallmentsToFreeze(int memberContractId, string user);

        [OperationContract]
        int ExtendContractFreezeItem(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> freezeInstallments, string user, int branchId, string notificationTitle);

        [OperationContract]
        int UnfreezeContract(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> unfreezeInstallments, string user, int branchId, string notificationTitle);

        [OperationContract]
        int IsFreezeAllowed(int memberContractId, int memberId, int branchId, string user);

        [OperationContract]
        string RegisterPayment(int branchId, int memberId, int articleNo, decimal paymentAmount, decimal discount, string createdUser, PaymentTypes paymentType);

        [OperationContract]
        SaleResultDC RegisterInstallmentPayment(int memberBranchID, int loggedBranchID, string user, InstallmentDC installment, PaymentDetailDC paymentDetail, int salePointId, ShopSalesDC salesDetails);

        [OperationContract]
        SaleResultDC AddInvoicePayment(PaymentDetailDC paymentDetails, string user, int branchID, ShopSalesDC salesDetails);

        [OperationContract]
        string GetCityForPostalCode(string postalCode, string user);

        [OperationContract]
        List<CalendarHoliday> GetHolidays(DateTime calendarDate, string user, int branchId);

        [OperationContract]
        string GetGymSettings(int branchId, string user, GymSettingType gymSettingType);

        [OperationContract]
        bool ManageMemberShopAccount(MemberShopAccountDC memberShopAccount, string mode, string user, int branchId, int salePointId);

        [OperationContract]
        MemberShopAccountDC GetMemberShopAccounts(int memberId, int hit, bool itemsNeeded, string user);

        [OperationContract]
        List<MemberShopAccountItemDC> GetMemberShopAccountItems(int shopAccountId, string user, int branchId);

        [OperationContract]
        ExcePaymentInfoDC getTest();

        [OperationContract]
        List<ExcePaymentInfoDC> GetMemberPaymentsHistory(int branchId, string user, int hit, int memberId, string paymentType);

        [OperationContract]
        int SaveNote(int branchId, string use, int memberId, string note);

        [OperationContract]
        OrdinaryMemberDC GetNote(int branchId, string user, int memberId);

        #region GetSelectedGymSettings
        [OperationContract]
        Dictionary<string, object> GetSelectedGymSettings(List<string> gymSetColNames, bool isGymSettings, int branchId, string user);
        #endregion

        #region DummyMethodForPassDomainObject
        [OperationContract]
        void DummyMethodForPassDomainObject(ExceGymSettingDC gymSetting);
        #endregion

        [OperationContract]
        string SaveGymEmployee(GymEmployeeDC employee, string user);

        [OperationContract]
        string UpdateEmployees(EmployeeDC employeeDc, string user);

        [OperationContract]
        List<ExceUserBranchDC> GetUserSelectedBranches(string userName, string user);

        [OperationContract]
        List<ResourceDC> GetResources(int branchId, string searchText, int categoryId, int activityId, bool isActive, string user);

        [OperationContract]
        List<CountryDC> GetCountryDetails(string user);

        [OperationContract]
        int SavePostalArea(string user, string postalCode, string postalArea, long population, long houseHolds);

        [OperationContract]
        ExcelineMemberDC GetMem();

        [OperationContract]
        bool CancelInvoice(int arItemNo, string user);

        [OperationContract]
        string GetMemberInvoices(int memberId, int hit, string user);

        [OperationContract]
        MemberInvoiceDC GetInvoices();

        [OperationContract]
        ExcelineInvoiceDetailDC GetInvoiceDetails(int invoiceID, int branchId, string user);

        [OperationContract]
        SaleResultDC GenerateInvoice(int invoiceBranchId, string user, InstallmentDC installment, PaymentDetailDC paymentDetail, string invoiceType, string notificationTitle, int memberBranchID);

        [OperationContract]
        bool UpdateInvoice(int arItemNo, string user, DateTime DueDate, string comment);

        [OperationContract]
        ArticleDC GetInvoiceFeeArticle(int branchId, string user);

        [OperationContract]
        PDFPrintResultDC ViewMemberContractDetail(int branchId, string user, int memberId);

        [OperationContract]
        PDFPrintResultDC ViewMemberATGContract(int branchId, string user, int memberId);

        [OperationContract]
        PDFPrintResultDC ViewSponsorInvoiceDetails(int branchId, string user, int arItemNo);

        [OperationContract]
        PDFPrintResultDC ViewInvoiceDetails(int branchId, string user, int arItemNo);

        [OperationContract]
        PDFPrintResultDC ViewCreditNoteDetails(int branchId, string user, int CreditNoteID);

        [OperationContract]
        PDFPrintResultDC PrintInvoicePaidReceipt(int branchId, string user, int arItemNo);

        [OperationContract]
        PDFPrintResultDC ViewSponsorOrderDetails(int branchId, string user, int orderNo);

        [OperationContract]
        int SendSMSInvoice(int branchId, string user, int aritemNo);

        [OperationContract]
        String EmailContractDetail(int branchId, string user, int memberId);

        [OperationContract]
        OrdinaryMemberDC GetEmployeeCategoryBySponsorId(int branchId, string user, int sponsorId);

        [OperationContract]
        string GetContractMembers(int memberContractId, string user);

        [OperationContract]
        List<ExcelineMemberDC> GetIntroducedMembers(int memberId, int branchId, string user);

        [OperationContract]
        bool UpdateIntroducedMembers(int memberId, DateTime? creditedDate, string creditedText, bool isDelete, string username);

        [OperationContract]
        List<MemberStatus> GetMemberStatus(string user);

        [OperationContract]
        bool UpdateContractOrder(List<InstallmentDC> installmentList, int branchId, string user);

        [OperationContract]
        int SaveContractGroupMembers(int groupId, List<ExcelineMemberDC> groupMemberList, int contractId, string user);

        [OperationContract]
        int UpdateContractPriority(Dictionary<int, int> packages, string user);

        [OperationContract]
        bool CheckBookingAvailability(int resourceID, string day, DateTime startTime, DateTime endTime, string user);

        [OperationContract]
        int AddInstallment(InstallmentDC installment, string user);

        [OperationContract]
        string GetMemberSearchCategory(int branchId, string user);

        [OperationContract]
        InstallmentDC GetMemberContractLastInstallment(int memberContractId, string user);

        [OperationContract]
        
        List<ImportStatusDC> GetClaimExportStatusForMember(string user, int memberID, int hit, ImportStatusDC status, DateTime? transferDate, string type);

        [OperationContract]
        bool AttachFile(string fileName, string custId, string companyCode, string username, int branchId);

        [OperationContract]
        bool SaveDocumentData(DocumentDC document, string username, int branchId);

        [OperationContract]
        List<DocumentDC> GetDocumentData(bool isActive, string custId, string searchText, int documentType, string username, int branchId);

        [OperationContract]
        bool DeleteDocumentData(int docId, string username, int branchId);

        [OperationContract]
        List<ArticleDC> GetVisitArticles(string userName, int branchID);

        [OperationContract]
        PDFPrintResultDC PrintVisitsListByMember(int branchId, string user, int memberId, DateTime fromDate, DateTime toDate);

        [OperationContract]
        PDFPrintResultDC PrintClassListByMember(int branchId, string user, int memberId, DateTime fromDate, DateTime toDate);

        [OperationContract]
        PDFPrintResultDC PrintPaymentHistoryByMember(int branchId, string user, int memberId, DateTime fromDate, DateTime toDate);

        [OperationContract]
        PDFPrintResultDC PrintGroupContractMemberList(int branchId, string user, string contractID, string memberType);

        [OperationContract]
        PDFPrintResultDC PrintGroupMemberListByMember(int branchId, string user, string memberID);

        [OperationContract]
        string GetConfigSettings(string type);

        [OperationContract]
        CustomerBasicInfoDC GetMemberBasicInfo(int memberId, string user);

        [OperationContract]
        List<ContractSummaryDC> GetContractSummaries(int memberId, int branchId, string user, bool isFreezDetailNeeded, bool isFreezdView);

        [OperationContract]
        int GetVisitCount(int memberId, DateTime startDate, DateTime endDate, string user);
        
        [OperationContract]
        List<SalePointDC> GetSalesPointList(int branchID, string user);

        [OperationContract]
        int SavePointOfSale(int branchId, SalePointDC pointOfSale, string user);

        [OperationContract]
        int GetSelectedHardwareProfile(int branchId, string user);

        [OperationContract]
        List<InstallmentDC> MergeOrders(List<InstallmentDC> updatedOrders, List<int> removedOrder, string removedOrderNo, int branchID, int memberContractId, string user);

        [OperationContract]
        List<FollowUpTemplateTaskDC> GetFollowUpTaskByTemplateId(int followUpTemplateId, string user);

        [OperationContract]
        List<ExceAccessProfileDC> GetAccessProfiles(string user, Gender gender);

        [OperationContract]
        List<MemberBookingDC> GetMemberBooking(int memberId, string user);

        [OperationContract]
        List<ExcelineMemberDC> GetOtherMembersForBooking(int scheduleId, string user);

        [OperationContract]
        string GetSponsorsforSponsoring(int branchId, DateTime startDate, DateTime endDate, string user);

        [OperationContract]
        bool GenerateSponsorOrders(SponsorShipGenerationDetailsDC sponsorDetails, string user);

        [OperationContract]
        bool GenerateAndSendInvoiceSMSANDEmail(int invoicebranchID, string user, InstallmentDC selectedOrder, PaymentDetailDC paymentDetails, Dictionary<string, bool> isSelected, Dictionary<string, string> text, Dictionary<string, string> senderDetail, int memberBranchID);

        [OperationContract]
        bool SendInvoiceSMSANDEmail(int branchId, string user, int arItemNo, int memberID, int guardianId, Dictionary<string, bool> isSelected, Dictionary<string, string> text, Dictionary<string, string> senderDetail);

        [OperationContract]
        List<ExceNotificationTepmlateDC> GetSMSNotificationTemplateByEntity(string user);

        [OperationContract]
        bool SendSMSFromMemberCard(int memberID, int branchId, string user, string message, string mobileNo);

        [OperationContract]
        bool SendEmailFromMemberCard(int memberID, int branchId, string user, string message, string emial, string subject);

        [OperationContract]
        List<ExceNotificationTepmlateDC> GetNotificationTemplateListByMethod(NotificationMethodType method, TextTemplateEnum temp, string user, int branchId);

        [OperationContract]
        string GetSessionValue(string sessionKey, string user);

        [OperationContract]
        List<USPRole> GetEmpRoles(string user);

        [OperationContract]
        List<InstallmentDC> GetSponsorOrders(List<int> sponsors, string user);

        [OperationContract]
        PackageDC GetRenewTemplate(int templateID, string user, string type);

        [OperationContract]
        bool SetContractSequenceID(List<PackageDC> contractList, int branchID, string user);

        [OperationContract]
        List<MemberIntegrationSettingDC> GetMemberIntegrationSettings(int branchId, int memberId, string user);

        [OperationContract]
        bool WellnessIntegrationOperation(OrdinaryMemberDC member, MemberIntegrationSettingDC wellnessIntegrationSetting, WellnessOperationType wellnessOperationType, string user, int branchId);

        [OperationContract]
        PDFPrintResultDC PrintOrder(int orderId, int branchId, string user);

        // [OperationContract]
        //string PrintOrder(int orderId, int branchId, string user);

        [OperationContract]
        int PayCreditNote(int aritemNo, List<PayModeDC> paymentModes, int memberId, int shopAccountid, string user, int salePointId, int loggedbranchID);

        [OperationContract]
        DailySettlementDC GetDailySettlements(int branchId, int salePointId, string user);

        [OperationContract]
        Dictionary<string, decimal> GetMemberEconomyBalances(string user, int memberId);

        [OperationContract]
        int SaveWithdrawal(int branchId, WithdrawalDC withDrawal, string user);

        [OperationContract]
        int AddSessionValue(string sessionKey, string value, string user);

        [OperationContract]
        bool AddCountryDetails(CountryDC country, string user);

        [OperationContract]
        int CheckContractGymChange(int memberID, int memberContractID, int oldBranchID, int newBranchID, string user);

        [OperationContract]
        int ValidateChangeGymInMember(int currentBranchID, int newBranchID, int branchID, int memberID, string user);

        [OperationContract]
        List<ExceUserBranchDC> GetGymsforAccountNumber(string accountNumber, string user);

        [OperationContract]
        MemberFeeGenerationResultDC GenerateMemberFee(List<int> gyms, int month, string user);
        
        [OperationContract]
        bool ValidateGenerateMemberFeeWithMemberFeeMonth(List<int> gyms, string user);
        
        [OperationContract]
        List<ExcelineRoleDc> GetUserRole(string user);

        [OperationContract]
        int ValidateMemberWithStatus(int memberId, int statusId, string user);    
       

        [OperationContract]
        string GetExorLiveClientAppUrl();

        [OperationContract]
        Dictionary<string, string> GetEncryptKey(string gymCode, string custId);

        [OperationContract]
        List<string> ConfirmationDetailForGenerateMemberFee(List<int> gyms, int month, string user);

        [OperationContract]
        int CancelPayment(int paymentID, bool keepthePayment, string user, string comment);

        [OperationContract]
        int GetNextGiftVoucherNumber(string seqId, string subSeqId, string user);

        [OperationContract]
        int ValidateGiftVoucherNumber(string voucherNumber, decimal payment, string user);

        [OperationContract]
        Dictionary<string, string> GetGiftVoucherDetail(string user);

        [OperationContract]
        int GetPriceChangeContractCount(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string user, bool changeToNextTemplate);

        [OperationContract]
        bool UpdatePrice(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string user, bool changeToNextTemplate);

        [OperationContract]
        string GetPriceUpdateContracts(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string user, bool changeToNextTemplate , int hit);

        [OperationContract]
        ExcelineContractDC getList();

        [OperationContract]
        List<PackageDC> GetTemplatesForPriceUpdate(string user);

        [OperationContract]
        string InfoGymGetAccessToken(string scope, string user);

        [OperationContract]
        string InfoGymRegisterMember(InfoGymMember member, int memberID, string user);

        [OperationContract]
        string InfoGymUpdateMember(InfoGymMember member, string inforGymId, string user);

        [OperationContract]
        PDFPrintResultDC PrintMemberBooking(int memberId, int branchId, string user, DateTime fromDate, DateTime toDate);

         [OperationContract]
        DateTime? ValidateSponsoringGeneration(string user);

        [OperationContract]
         bool UpdateSettingForUserRoutine(string key, string value, string user);

         [OperationContract]
         PDFPrintResultDC PrintMemberSales(int memberID, DateTime? startdate, DateTime? endDate, string user, int branchID);

         [OperationContract]
         string GetMemberInvoicesForContract(int memberId, int contractId, int hit, string user);

         [OperationContract]
         ContractCondition TempContractCondition();

          [OperationContract]
        bool IsBrisIntegration();

         [OperationContract]
          List<OrdinaryMemberDC> SearchBrisMember(string userId, string firstName, string lastName, string email, string mobile, string birthDay, string cardNumber, string ssn, string user);

          [OperationContract]
         OrdinaryMemberDC SaveBrisMember(OrdinaryMemberDC member);

         [OperationContract]
        List<LanguageDC> GetLangugeDetails(string user);
    }
}
