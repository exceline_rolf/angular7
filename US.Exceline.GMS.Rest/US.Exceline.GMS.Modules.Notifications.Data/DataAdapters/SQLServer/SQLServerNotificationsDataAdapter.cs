﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : US.Exceline.GMS
// Project Name      : US.Exceline.GMS.Modules.Notifications.Data
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 19/6/2012 4:27:11 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Notifications.Data.SystemObjects;
using US.Exceline.GMS.Modules.Notifications.Data.DataAdapters.SQLServer.Commands;
using US.GMS.Core.DomainObjects.Notification;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.Exceline.GMS.Modules.Notifications.Data.DataAdapters.SQLServer
{
    public class SQLServerNotificationsDataAdapter : INotificationsDataAdapter
    {
        #region  Notification

        public int SaveNotification(NotificationDC notification, string createdUser, int branchId, string gymCode)
        {
            try
            {
                SaveNotificationAction action = new SaveNotificationAction(notification, createdUser, branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int SaveNotificationTemplate(NotificationTemplateDC notificationTemplate, string createdUser, int branchId, string gymCode)
        {
            try
            {
                SaveNotificationTemplateAction action = new SaveNotificationTemplateAction(notificationTemplate, createdUser, branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveNotificationMethod(NotificationMethodDC notificationMethod, string createdUser, int branchId, string gymCode)
        {
            try
            {
                SaveNotificationMethodAction action = new SaveNotificationMethodAction(notificationMethod, createdUser, branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<NotificationMethodDC> GetNotifications(int branchId, string gymCode)
        {
            try
            {
                GetNotificationsAction action = new GetNotificationsAction(branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<NotificationTemplateDC> GetNotificationTemplates(int branchId,string gymCode)
        {
            try
            {
                GetNotificationTemplatesAction action = new GetNotificationTemplatesAction(branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<NotificationMethodDC> GetNotificationsByMember(int branchId, int memeberId,string gymCode)
        {
            try
            {
                GetNotificationsActionByMemberID action = new GetNotificationsActionByMemberID(branchId, memeberId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateIsNotified(int notificationMethodId, string createdUser, int branchId,string gymCode)
        {
            try
            {
                SaveNotificationHistoryAction action = new SaveNotificationHistoryAction(notificationMethodId, createdUser, branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool DeleteNotificationMethod(int notificationMethodId, string createdUser, int branchId, string gymCode)
        {
            try
            {
                DeleteNotificationMethodAction action = new DeleteNotificationMethodAction(notificationMethodId, createdUser, branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
