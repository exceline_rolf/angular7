﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.Notifications.Data.DataAdapters.SQLServer.Commands
{
    public class SaveNotificationHistoryAction : USDBActionBase<int>
    {
        private int _notificationMethodId = -1;
        
        

        public SaveNotificationHistoryAction(int notificationMethodId, string createdUser, int branchId)
        {
            _notificationMethodId = notificationMethodId;
          

        }

        protected override int Body(DbConnection connection)
        {
            int result = -1;
            string StoredProcedureName = "USExceGMSNotificationsSaveNotificationHistory";

            try
            {

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@notificationMethodId", DbType.Int32, _notificationMethodId));

                DbParameter outputPara = new SqlParameter();
                outputPara.DbType = DbType.Int32;
                outputPara.ParameterName = "@OutID";
                outputPara.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputPara);
                cmd.ExecuteNonQuery();

                result = Convert.ToInt32(outputPara.Value);

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }
    }
}
