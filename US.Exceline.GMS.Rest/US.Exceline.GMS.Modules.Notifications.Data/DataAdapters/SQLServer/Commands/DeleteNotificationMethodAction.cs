﻿
// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Notifications.Data
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 28/6/2012 10:53:39
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Notifications.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteNotificationMethodAction : USDBActionBase<bool>
    {
        private int _notificationMethodId = -1;
          
        public DeleteNotificationMethodAction(int notificationMethodId, string createdUser, int branchId)
        {
            _notificationMethodId = notificationMethodId;
           
        }

        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSNotificationsDeleteNotificationMethod";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _notificationMethodId));
                cmd.ExecuteNonQuery();
                result = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
