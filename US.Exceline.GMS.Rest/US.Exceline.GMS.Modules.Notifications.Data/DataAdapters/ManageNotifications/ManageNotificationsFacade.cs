﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : US.Exceline.GMS
// Project Name      : US.Exceline.GMS.Modules.Notifications.Data
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 19/6/2012 4:27:11 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Notifications.Data.SystemObjects;
using US.Exceline.GMS.Modules.Notifications.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.Notification;

namespace US.Exceline.GMS.Modules.Notifications.Data.DataAdapters.ManageNotifications
{
    public class ManageNotificationsFacade
    {
        private static INotificationsDataAdapter GetDataAdapter()
        {
            return new SQLServerNotificationsDataAdapter();
        }

        #region  Notification
        public static int SaveNotification(NotificationDC notification, string createdUser, int branchId, string gymCode)
        {
            return GetDataAdapter().SaveNotification(notification, createdUser, branchId, gymCode);
        }


        public static int SaveNotificationTemplate(NotificationTemplateDC notificationTemplate, string createdUser, int branchId,string gymCode)
        {
            return GetDataAdapter().SaveNotificationTemplate(notificationTemplate, createdUser, branchId, gymCode);
        }
        public static int SaveNotificationMethod(NotificationMethodDC notificationMethod, string createdUser, int branchId,string gymCode)
        {
            return GetDataAdapter().SaveNotificationMethod(notificationMethod, createdUser, branchId, gymCode);
        }

        public static List<NotificationTemplateDC> GetNotificationTemplates(int branchId,string gymCode)
        {
            return GetDataAdapter().GetNotificationTemplates(branchId, gymCode);
        }

        public static List<NotificationMethodDC> GetNotifications(int branchId,string gymCode)
        {
            return GetDataAdapter().GetNotifications(branchId, gymCode);
        }


        public static List<NotificationMethodDC> GetNotificationsByMember(int branchId, int memberId,string gymCode)
        {
            return GetDataAdapter().GetNotificationsByMember(branchId, memberId, gymCode);
        }

        public static int UpdateIsNotified(int notificationMethodId, string createdUser, int branchId,string gymCode)
        {
            return GetDataAdapter().UpdateIsNotified(notificationMethodId, createdUser, branchId, gymCode);
        }

        public static bool DeleteNotificationMethod(int notificationMethodId, string createdUser, int branchId,string gymCode)
        {
            return GetDataAdapter().DeleteNotificationMethod(notificationMethodId, createdUser, branchId, gymCode);
        }

        #endregion
    }
}
