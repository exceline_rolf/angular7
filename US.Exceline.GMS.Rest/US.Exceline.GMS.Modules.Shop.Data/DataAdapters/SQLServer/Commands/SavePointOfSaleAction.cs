﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class SavePointOfSaleAction : USDBActionBase<int>
    {
        private int _branchId;
        private SalePointDC _pointOfSale = new SalePointDC();
        private string _user = string.Empty;

        public SavePointOfSaleAction(int branchId, SalePointDC pointOfSale, string user)
        {
            _branchId = branchId;
            _pointOfSale = pointOfSale;
            _user = user;
            OverwriteUser(user);
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int id = -1;

            try
            {
                string storedProcedure = "USExceGMSSavePointOfSale";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _pointOfSale.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@machineName", DbType.String, _pointOfSale.MachineName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@hardwareProfileId", DbType.Int32, _pointOfSale.HardwareProfileId));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 50;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                id = Convert.ToInt32(output.Value);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return id;
        }
    }
}
