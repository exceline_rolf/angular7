﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;
using US_DataAccess;


namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class ExcelineGetDailySettlementDataForPrint : USDBActionBase<DailySettlementPrintData>
    {

        String _dailySettlementId = String.Empty;
        String _reconiliationId = "-1";
        String _salePointId = String.Empty;
        String _branchId = String.Empty;
        String _gymCode = String.Empty;
        String _mode = String.Empty;
        String _startDate = "0001-01-01 00:00:00";
        String _endDate = "0001-01-01 00:00:00";
        String _countedcashdraft = String.Empty;

        public ExcelineGetDailySettlementDataForPrint(String dailySettlementId, String reconiliationId, String salePointId, String branchId, String mode, String startDate, String endDate, String gymCode, String countedCashDraft)
        {
            /* Inputs */
            _dailySettlementId = dailySettlementId;
            _reconiliationId = reconiliationId;
            _salePointId = salePointId;
            _branchId = branchId;
            _gymCode = gymCode;
            _mode = mode;
            _startDate = startDate;
            _endDate = endDate;
            _countedcashdraft = countedCashDraft;
        }
        protected override DailySettlementPrintData Body(System.Data.Common.DbConnection dBconnection)
        {
            var printData = new DailySettlementPrintData();
            const string storedProcedureName = "USC_ADP_LDSP_GetDailySettlement";

            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@dailySettlementId", System.Data.DbType.String, _dailySettlementId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@reconciliationId", System.Data.DbType.String, _reconiliationId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@salePointId", System.Data.DbType.String, _salePointId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.String, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@gymCode", System.Data.DbType.String, _gymCode));
                command.Parameters.Add(DataAcessUtils.CreateParam("@mode", System.Data.DbType.String, _mode));
                command.Parameters.Add(DataAcessUtils.CreateParam("@startDate", System.Data.DbType.String, _startDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@endDate", System.Data.DbType.String, _endDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@countedCashDraft", System.Data.DbType.String, _countedcashdraft));


                var reader = command.ExecuteReader();

                while (reader.Read())
                {

                    printData.GymName = Convert.ToString(reader["GymName"]);
                    printData.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    printData.DailySettlementID = Convert.ToString(reader["DailySettlementtID"]);
                    printData.FromDateTime = Convert.ToDateTime(reader["FromDateTime"]).ToString();
                    printData.ToDateTime = Convert.ToDateTime(reader["ToDateTime"]).ToString();
                    printData.ActivityTurnOver = Convert.ToString(reader["ActiviyTurnOver"]);
                    printData.ShopTurnOver = Convert.ToString(reader["ShopTurnOver"]);
                    printData.GiftCardTurnOver = Convert.ToString(reader["GiftCardTurnOver"]);
                    printData.OnAccountTurnOver = Convert.ToString(reader["OnAccountTurnOver"]);
                    printData.TotalTurnOver = Convert.ToString(reader["TotalTurnOver"]);
                    printData.Cash = Convert.ToString(reader["Cash"]);
                    printData.BankTerminal = Convert.ToString(reader["BankTerminal"]);
                    printData.GiftCard = Convert.ToString(reader["GiftCard"]);
                    printData.OnAccount = Convert.ToString(reader["OnAccount"]);
                    printData.NextOrder = Convert.ToString(reader["NextOrder"]);
                    printData.Invoice = Convert.ToString(reader["Invoice"]);
                    printData.SMSEmailInvoice = Convert.ToString(reader["SMSEmailInvoice"]);
                    printData.PrePaid = Convert.ToString(reader["PrePaid"]);
                    printData.TotalReceived = Convert.ToString(reader["TotalReceived"]);
                    printData.CountedCash = Convert.ToString(reader["CountedCash"]);
                    printData.Exchange = Convert.ToString(reader["Exchange"]);
                    printData.CashInOut = Convert.ToString(reader["CashInOut"]);
                    printData.PaidOut = Convert.ToString(reader["PaidOut"]);
                    printData.Deviation = Convert.ToString(reader["Deviation"]);
                    printData.ToBank = Convert.ToString(reader["ToBank"]);
                    printData.ShopReturn = Convert.ToString(reader["ShopReturn"]);
                    printData.CashWithdrawal = Convert.ToString(reader["CashWithdrawal"]);
                    printData.PettyCash = Convert.ToString(reader["PettyCash"]);
                    printData.TotalPayments = Convert.ToString(reader["TotalPayments"]);
                    printData.SalePointId = Convert.ToString(reader["salePointId"]);
                    printData.CashIn = Convert.ToString(reader["CashIn"]);
                    printData.CashOut = Convert.ToString(reader["CashOut"]);
                    printData.TotalInvoice = Convert.ToString(reader["TotalInvoice"]);
                    printData.CashReturn = Convert.ToString(reader["CashReturn"]);
                    printData.ReconciliationText = Convert.ToString(reader["ReconciliationText"]);
                    printData.IsReconciled = Convert.ToString(reader["IsReconciled"]);
                    printData.ID = Convert.ToString(reader["ID"]);
                    printData.DocType = Convert.ToString(reader["DocType"]);
                    printData.GymCode = Convert.ToString(reader["GymCode"]);
                    printData.BranchId = Convert.ToString(reader["BranchID"]);
                    printData.OrgNo = Convert.ToString(reader["OrgNo"]);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return printData;
        }
    }
}



