﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageShop;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    class EditVoucherAction : USDBActionBase<int>
    {
        private int _branchId = -1;
        private VoucherDC _voucher;

        public EditVoucherAction(int branchId, VoucherDC voucher)
        {
            this._branchId = branchId;
            this._voucher = voucher;
        }

        protected override int Body(DbConnection connection)
        {
            int voucherId = 0;
            string storedProcedureName = "USExceGMSShopEditVouchers";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _voucher.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _voucher.Amount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@payMoodCash", DbType.Decimal, _voucher.PayMoodCash));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@payMoodBankTerminal", DbType.Decimal, _voucher.PayMoodBankTerminal));

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@outId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);

                cmd.ExecuteNonQuery();

                voucherId = Convert.ToInt32(param.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return voucherId;
        }
    }
}
