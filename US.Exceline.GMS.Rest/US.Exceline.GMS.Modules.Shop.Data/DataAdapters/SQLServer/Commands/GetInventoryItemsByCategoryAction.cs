﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetInventoryItemsByCategoryAction : USDBActionBase<List<InventoryItemDC>>
    {
        private int _branchId = -1;
        private int _categoryId = -1;
        private bool _isActive;

        public GetInventoryItemsByCategoryAction(int categoryId, bool isActive, int branchId)
        {
            _categoryId = categoryId;
            _branchId = branchId;
            _isActive = isActive;
        }

        protected override List<InventoryItemDC> Body(System.Data.Common.DbConnection connection)
        {
            List<InventoryItemDC> inventoryItemList = new List<InventoryItemDC>();
            string storedProcedure = "USExceGMSShopGetInventoryItemsByCategory";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryId", DbType.Int32, _categoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isActive", DbType.Boolean, _isActive));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    InventoryItemDC inventoreItem = new InventoryItemDC();

                    inventoreItem.Id = Convert.ToInt32(reader["Id"].ToString());
                    inventoreItem.ItemCode = reader["ItemCode"].ToString();
                    inventoreItem.Name = reader["Name"].ToString();
                    inventoreItem.IsActive = Convert.ToBoolean(reader["IsActive"]);
                    inventoreItem.CategoryId = Convert.ToInt32(reader["CategoryId"].ToString());
                    inventoreItem.CategoryName = reader["CategoryName"].ToString();
                    inventoreItem.PurchasedPrice = Convert.ToDecimal(reader["PurchasedPrice"].ToString());
                    inventoreItem.UnitPriceForCustomers = Convert.ToDecimal(reader["CustomerPrice"].ToString());
                    inventoreItem.UnitPriceForEmployee = Convert.ToDecimal(reader["Employeeprice"].ToString());
                    inventoreItem.Quantity = Convert.ToInt32(reader["Qty"].ToString());
                    inventoreItem.ReOrderLevel = Convert.ToInt32(reader["ReOrderLevel"].ToString());
                    inventoreItem.ReceivedDate = Convert.ToDateTime(reader["ReceivedDate"].ToString());
                    inventoreItem.ExpireDate = Convert.ToDateTime(reader["ExpireDate"].ToString());

                    inventoreItem.Discount = Convert.ToDouble(reader["Discount"]);
                    inventoreItem.DiscountStartDate = Convert.ToDateTime(reader["DiscountStartDate"].ToString());
                    inventoreItem.DiscountStartDate = Convert.ToDateTime(reader["DiscountEndDate"].ToString());
                    inventoryItemList.Add(inventoreItem);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return inventoryItemList;
        }
    }
}
