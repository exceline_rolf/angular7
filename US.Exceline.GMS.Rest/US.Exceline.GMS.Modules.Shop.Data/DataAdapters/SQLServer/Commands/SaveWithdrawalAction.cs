﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer.Commands;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class SaveWithdrawalAction : USDBActionBase<int>
    {

        private WithdrawalDC _withDrawal = new WithdrawalDC();
        private string _user = string.Empty;
        private string _gymCode = string.Empty;
        private int _branchId = -1;

        public SaveWithdrawalAction(int branchId, WithdrawalDC withDrawal, string user, string gymCode)
        {
            _withDrawal = withDrawal;
            _branchId = branchId;
            _gymCode = gymCode;
            _user = user;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int id = -1;
            try
            {
                string storedProcedure = "USExceGMSShopSaveWithdrawal";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _withDrawal.PaymentAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, _withDrawal.Comment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, _withDrawal.WithdrawalType.ToString()));
                if (_withDrawal.WithdrawalType == WithdrawalTypes.CASHWITHDRAWAL)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _withDrawal.MemberId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@withdrwalFrom", DbType.String, _withDrawal.WithdrawalFrom));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@paymentMode", DbType.String, _withDrawal.PaymentType.Code));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRole", DbType.String, "MEM"));
                }
                else if (_withDrawal.WithdrawalType == WithdrawalTypes.PETTYCASH)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _withDrawal.EmployeeId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRole", DbType.String, "EMP"));
                }
                if (_withDrawal.SalePointId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@salePointId", DbType.Int32, _withDrawal.SalePointId));
                }
                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 50;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();

                if (output.Value != null)
                {
                    id = Convert.ToInt32(output.Value);
                }
                else
                {
                    id = -1;
                }

                //------------------save shop transations data -----------------------------------------------------------------
                if (id > 0)
                {

                    ShopTransactionDC _shopTrans = new ShopTransactionDC();
                    _shopTrans.BranchId = _branchId;
                    _shopTrans.CreatedUser = _user;
                    _shopTrans.CreatedDate = DateTime.Now;
                    _shopTrans.Amount = _withDrawal.PaymentAmount;
                    _shopTrans.SalePointId = _withDrawal.SalePointId;

                    MemberShopAccountDC memberAccount = new MemberShopAccountDC();
                    memberAccount.MemberId = _withDrawal.MemberId;
                    memberAccount.ActiveStatus = true;


                    MemberShopAccountItemDC accountItem = new MemberShopAccountItemDC();
                    accountItem.PaymentAmount = _withDrawal.PaymentAmount;
                    accountItem.PayMode = _withDrawal.PaymentType.Code;
                    List<MemberShopAccountItemDC> accountItemList = new List<MemberShopAccountItemDC>();
                    accountItemList.Add(accountItem);
                    memberAccount.CreditaccountItemList = accountItemList;

                    if (_withDrawal.WithdrawalType == WithdrawalTypes.CASHWITHDRAWAL)
                    {
                        _shopTrans.Mode = TransactionTypes.CASHWITHDRAWAL;
                        _shopTrans.EntityId = _withDrawal.MemberId;
                        _shopTrans.EntityRoleType = "MEM";
                        if (_withDrawal.PaymentType != null && _withDrawal.PaymentType.Code == "CASH")
                        {
                            SaveShopTransactionAction shopTransactionAction = new SaveShopTransactionAction(_shopTrans);
                            shopTransactionAction.Execute(EnumDatabase.Exceline, _gymCode);
                        }

                        if (_withDrawal.WithdrawalFrom == "ONACCOUNT")
                        {
                            ManageMemberShopAccountAction manageShopAccountAction = new ManageMemberShopAccountAction(memberAccount, "SUB", _user, _branchId, _withDrawal.SalePointId, _gymCode);
                            manageShopAccountAction.Execute(EnumDatabase.Exceline, _gymCode);
                        }

                        if (_withDrawal.WithdrawalFrom == "PREPAID")
                        {
                            ManageMemberPrePaidAccountAction manageShopAccountAction = new ManageMemberPrePaidAccountAction(memberAccount, "SUB", _user, _branchId, _withDrawal.SalePointId, _gymCode);
                            manageShopAccountAction.Execute(EnumDatabase.Exceline, _gymCode);
                        }
                    }
                    else if (_withDrawal.WithdrawalType == WithdrawalTypes.PETTYCASH)
                    {
                        _shopTrans.Mode = TransactionTypes.PETTYCASH;
                        _shopTrans.EntityId = _withDrawal.EmployeeId;
                        _shopTrans.EntityRoleType = "EMP";
                        SaveShopTransactionAction shopTransactionAction = new SaveShopTransactionAction(_shopTrans);
                        shopTransactionAction.Execute(EnumDatabase.Exceline, _gymCode);
                    }
                }
                //----------------------------------------------------------------------------------------------------------

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return id;
        }
    }
}
