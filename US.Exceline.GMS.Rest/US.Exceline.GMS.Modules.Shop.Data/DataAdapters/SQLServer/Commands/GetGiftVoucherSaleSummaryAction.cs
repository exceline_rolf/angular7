﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetGiftVoucherSaleSummaryAction : USDBActionBase<List<ShopSalesPaymentDC>>
    {
        private readonly DateTime _fromDate;
        private readonly DateTime _toDate;
        private readonly int _branchId;

        public GetGiftVoucherSaleSummaryAction(DateTime fromDate, DateTime toDate, int branchId)
        {
            _fromDate = fromDate;
            _toDate = toDate;
            _branchId = branchId;
        }
        protected override List<ShopSalesPaymentDC> Body(System.Data.Common.DbConnection dBconnection)
        {
            var shopSaleHistoryList = new List<ShopSalesPaymentDC>();
            const string storedProcedureName = "USExceGMSGetGiftVoucherSalesHistory";
 
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", System.Data.DbType.Date, _fromDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@endDate", System.Data.DbType.Date, _toDate));

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var gvSale = new ShopSalesPaymentDC();
                    gvSale.ShopSalesId = Convert.ToInt32(reader["id"]);
                    gvSale.SaleDate = Convert.ToDateTime(reader["SaleDate"]);
                    gvSale.SaleTime = (reader["SaleTime"].ToString());
                    gvSale.GiftVoucherNumber = reader["VoucherNo"].ToString();
                    gvSale.GiftVoucherAmount = Convert.ToDecimal(reader["DefaultPrice"]);
                    gvSale.PaymentType = reader["PaymentTypes"].ToString();
                    gvSale.PaymentCode = reader["PaymentTypeCodes"].ToString();
                    gvSale.PaymentAmount = Convert.ToDecimal(reader["PurchasedPrice"]);
                    gvSale.MemberName = reader["MemberName"].ToString();
                    gvSale.CreatedUser = reader["CreatedUser"].ToString();
                    gvSale.Balance = Convert.ToDecimal(reader["Balance"]);
                    shopSaleHistoryList.Add(gvSale);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return shopSaleHistoryList;
        }
    }
}
