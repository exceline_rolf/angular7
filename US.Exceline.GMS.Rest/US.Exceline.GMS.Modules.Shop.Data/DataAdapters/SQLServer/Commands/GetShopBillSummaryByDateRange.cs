﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 14/08/2014
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetShopBillSummaryByDateRange : USDBActionBase<List<ShopSalesPaymentDC>>
    {

        private DateTime _fromDate = DateTime.Now;
        private DateTime _toDate = DateTime.Now;
        private int _branchId = -1;

        public GetShopBillSummaryByDateRange(DateTime fromDate, DateTime toDate, int branchId)
        {
            _fromDate = fromDate;
            _toDate = toDate;
            _branchId = branchId;
        }

        protected override List<ShopSalesPaymentDC> Body(System.Data.Common.DbConnection connection)
        {
            DbDataReader reader = null;
            List<ShopSalesPaymentDC> billList = new List<ShopSalesPaymentDC>();
            string spName = "USExceGMSShopGetShopSaleBillSummary";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchID", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", System.Data.DbType.Date, _fromDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@endDate", System.Data.DbType.Date, _toDate));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ShopSalesPaymentDC shopBill = new ShopSalesPaymentDC();

                    if (reader["id"] != DBNull.Value)
                        shopBill.ShopSalesId = Convert.ToInt32(reader["id"]);
                    if (reader["SaleDate"] != DBNull.Value)
                        shopBill.SaleDate = Convert.ToDateTime(reader["SaleDate"]);
                    if (reader["SaleTime"] != DBNull.Value)
                        shopBill.SaleTime = Convert.ToString(reader["SaleTime"]);
                    if (reader["Total"] != DBNull.Value)
                        shopBill.Amount = Convert.ToDecimal(reader["Total"]);

                    shopBill.CreatedUser = Convert.ToString(reader["EmployeeName"]);
                    shopBill.PayMode = Convert.ToString(reader["PaymentTypes"]);
                    shopBill.PaymentCode = Convert.ToString(reader["PaymentCodes"]);
                    shopBill.Ref = Convert.ToString(reader["Ref"]);
                    shopBill.MemberName = Convert.ToString(reader["MemberName"]);
                    shopBill.MemberCustId = Convert.ToString(reader["MemberCustId"]);
                    billList.Add(shopBill);
                }
            }
            catch
            {
                if (reader != null)
                    reader.Close();
                throw;
            }
            return billList;
        }
    }
}
