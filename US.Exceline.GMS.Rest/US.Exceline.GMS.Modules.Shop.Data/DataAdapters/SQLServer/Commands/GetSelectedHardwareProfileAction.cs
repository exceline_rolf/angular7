﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 05/09/2013 3:28:56 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetSelectedHardwareProfileAction : USDBActionBase<int>
    {
        private int _branchId;
        private string _user = string.Empty;

        public GetSelectedHardwareProfileAction(int branchId, string user)
        {
            _branchId = branchId;
            _user = user;
            //OverwriteUser(user);
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int hardwareProfileId = -1;
            try
            {
                string storedProcedure = "USExceGMSGetSelectedHardwareProfile";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@userName", DbType.String, _user));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    hardwareProfileId = Convert.ToInt32(reader["hardwareProfileId"]);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return hardwareProfileId;
        }
    }
}
