﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageShop;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class AddVoucherAction : USDBActionBase<bool>
    {
        private int _brnachId;
        private VoucherDC _voucher;

        public AddVoucherAction(int branchId, VoucherDC voucher)
        {
            this._brnachId = branchId;
            this._voucher = voucher;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result =false;
            string storedProcedureName = "USExceGMSShopAddVoucher";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@settlementNumber", DbType.Int32, _voucher.SettlementNumber));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@date", DbType.DateTime, _voucher.Date));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _voucher.Amount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@payMoodCash", DbType.Decimal, _voucher.PayMoodCash));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@payMoodBankTerminal", DbType.Decimal, _voucher.PayMoodBankTerminal));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _brnachId));
                
                cmd.ExecuteNonQuery();

                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
    }
}
