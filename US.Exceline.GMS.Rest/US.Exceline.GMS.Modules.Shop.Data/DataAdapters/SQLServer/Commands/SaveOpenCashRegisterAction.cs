﻿// --------------------------------------------------------------------------
// Copyright(c) <2018> "Exceline.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer.Commands;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class SaveOpenCashRegisterAction : USDBActionBase<int>
    {

        private CashRegisterDC _cashRegister = new CashRegisterDC();
        private string _user = string.Empty;
        //private string _gymCode = string.Empty;
        private int _branchId = -1;

        public SaveOpenCashRegisterAction(int branchId, CashRegisterDC cashRegister, string user, string gymCode)
        {
            _cashRegister = cashRegister;
            _branchId = branchId;
            //_gymCode = gymCode;
            _user = user;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int id = -1;
            try
            {
                string storedProcedure = "USExceGMSShopSaveOpenCashRegister";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _cashRegister.EntitiyId));                
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRole", DbType.String, "MEM"));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@machinename", DbType.String, _cashRegister.MachineName));

                if (_cashRegister.SalesPointId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@salePointId", DbType.Int32, _cashRegister.SalesPointId));
                }
                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 50;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();

                if (output.Value != null)
                {
                    id = Convert.ToInt32(output.Value);
                }
                else
                {
                    id = -1;
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return id;
        }
    }
}
