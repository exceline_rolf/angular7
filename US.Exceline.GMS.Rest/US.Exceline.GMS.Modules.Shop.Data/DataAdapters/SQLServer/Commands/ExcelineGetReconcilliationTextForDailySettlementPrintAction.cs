﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class ExcelineGetReconcilliationTextForDailySettlementPrintAction : USDBActionBase<String>
    {
        private int dSetId = -1;
        private String Result = String.Empty;

        public ExcelineGetReconcilliationTextForDailySettlementPrintAction(int dailySettlementId)
        {
            dSetId = dailySettlementId;
        }

        protected override String Body(DbConnection connection)
        {
            string StoredProcedureName = "ExcelineGetReconcilliationTextForDailySettlementPrint";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DailySettlmentId", DbType.Int32, dSetId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    Result = Convert.ToString(reader["ReconciliationText"]);
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Result;
        }
    }
}

