﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;


namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetSalesPointListAction : USDBActionBase<List<SalePointDC>>
    {
        private int _branchID;

        public GetSalesPointListAction(int branchID)
        {
            _branchID = branchID;
        }

        protected override List<SalePointDC> Body(DbConnection connection)
        {
            List<SalePointDC> SalesPointList = new List<SalePointDC>();

            string storedProcedureName = "USExceGMSShopGetSalePoints";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", DbType.Int32, _branchID));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    SalePointDC salePoint = new SalePointDC();

                    salePoint.ID = Convert.ToInt32(reader["ID"].ToString());
                    salePoint.Name = reader["Name"].ToString();
                    salePoint.HardwareProfileId = Convert.ToInt32(reader["HardwareProfileId"].ToString());
                    salePoint.MachineName = reader["MachineName"].ToString();
                    salePoint.BranchID = _branchID;

                    SalesPointList.Add(salePoint);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return SalesPointList;
        }
    }
}
