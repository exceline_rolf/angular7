﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;
using System.Data;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    class GetVouchersListAction : USDBActionBase<List<VoucherDC>>
    {
        private int _branchId;

        public GetVouchersListAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<VoucherDC> Body(DbConnection connection)
        {
            List<VoucherDC> getVouchersList = new List<VoucherDC>();
            string storedProcedureName = "USExceGMSShopGetVouchers";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    VoucherDC voucher = new VoucherDC();

                    voucher.Id = Convert.ToInt32(reader["ID"].ToString());
                    voucher.SettlementNumber = Convert.ToInt32(reader["SettlementNumber"].ToString());
                    voucher.Date = Convert.ToDateTime(reader["Date"].ToString());
                    voucher.Amount = Convert.ToDecimal(reader["Amount"].ToString());
                    voucher.PayMoodCash = Convert.ToDecimal(reader["PayMoodCash"].ToString());
                    voucher.PayMoodBankTerminal = Convert.ToDecimal(reader["PayMoodBankTerminal"].ToString());

                    getVouchersList.Add(voucher);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return getVouchersList;
        }
    }
}
