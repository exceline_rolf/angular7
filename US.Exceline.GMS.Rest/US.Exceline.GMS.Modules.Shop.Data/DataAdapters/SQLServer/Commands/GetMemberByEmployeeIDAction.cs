﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberByEmployeeIDAction : USDBActionBase<int>
    {
        private int _empId = -1;
        private int _branchId = -1;
        private string _user = string.Empty;

        public GetMemberByEmployeeIDAction(int empId, int branchId,string user)
        {
            _empId = empId;
            _branchId = branchId;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            int memberId = -1;
            string StoredProcedureName = "USExceGMSShopGetMemberByEmployeeID";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@empId", DbType.Int32, _empId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.Int32, _user));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@memberId";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                memberId = Convert.ToInt32(output.Value);
                return memberId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
