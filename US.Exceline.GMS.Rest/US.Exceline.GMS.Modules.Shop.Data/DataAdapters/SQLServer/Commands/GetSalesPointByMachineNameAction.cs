﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetSalesPointByMachineNameAction : USDBActionBase<SalePointDC>
    {
        private int _branchID = -1;
        private string _machineName = string.Empty;
        private SalePointDC _salePoint = new SalePointDC();

        public GetSalesPointByMachineNameAction(int branchID, string machineName)
        {
            _branchID = branchID;
            _machineName = machineName;
        }

        protected override SalePointDC Body(DbConnection connection)
        {
            string storedProcedureName = "USExceGMSShopGetSalesPointByMachineName";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", DbType.Int32, _branchID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MachineName", DbType.String, _machineName));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    _salePoint.ID = Convert.ToInt32(reader["ID"].ToString());
                    _salePoint.Name = reader["Name"].ToString();
                    _salePoint.BranchName= reader["BranchName"].ToString();
                    _salePoint.HardwareProfileId = Convert.ToInt32(reader["HardwareProfileId"].ToString());
                    _salePoint.MachineName = reader["MachineName"].ToString();
                    _salePoint.BranchID = _branchID;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _salePoint;
        }
    }
}
