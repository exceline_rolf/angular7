﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageShop;
using System.Data;
using System.Data.SqlClient;
using US.GMS.Data.DataAdapters.SQLServer.Commands;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Collections.ObjectModel;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class AddShopSalesAction : USDBActionBase<SaleResultDC>
    {
        private ShopSalesDC _saleDetails;
        private InstallmentDC _installment; 
        private PaymentDetailDC _paymentDetails;
        private int _memberBranchID = -1;
        private int _loggedBranchID = -1;
        private string _user;
        private string _gymCode = string.Empty;
        private bool _isBookingPayment = false;
        private string _cardType;

        public AddShopSalesAction(String cardType, InstallmentDC installment, ShopSalesDC shopSaleDetails, PaymentDetailDC paymentDetails, int memberBranchID, int loggedbranchID, string user, string gymCode, bool isBookingPayment)
        {
            _cardType = cardType;
            _saleDetails = shopSaleDetails;
            _gymCode = gymCode;
            _paymentDetails = paymentDetails;
            _installment = installment;
            _user = user;
            _memberBranchID = memberBranchID;
            _loggedBranchID = loggedbranchID;
            _isBookingPayment = isBookingPayment;
        }

        protected override SaleResultDC Body(DbConnection connection)
        {
            try
            {
                var payMode = _paymentDetails.PayModes.FirstOrDefault(X => X.PaymentTypeCode == "NEXTORDER");
                if (payMode != null)
                {
                    var nextOrderAction = new AddNextOrderSale(_installment.MemberId, payMode.Amount, _saleDetails, _memberBranchID, _loggedBranchID, _user);
                    return nextOrderAction.Execute(EnumDatabase.Exceline, _gymCode);
                }
                else
                {
                    var paymentAction = new RegisterShopInstallmentPaymentAction(_cardType, _memberBranchID, _loggedBranchID, _user, _installment, _paymentDetails, _gymCode, _saleDetails, _isBookingPayment);
                    return paymentAction.Execute(EnumDatabase.Exceline, _gymCode);
                }
            }
            catch
            {
                throw;
            }
        }

        
    }
}
