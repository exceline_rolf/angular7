﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageShop;


namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetInvoicesForArticleReturnAction : USDBActionBase<List<InvoiceForReturnDC>>
    {  

    private readonly int _memberId;
    private readonly int _branchId;
    private readonly string _articleNo;
    private readonly int _shopOnly;

        public GetInvoicesForArticleReturnAction(int memberID, int branchId, string articleNo, int shopOnly)
        {
        _memberId = memberID;
        _branchId = branchId;
        _articleNo = articleNo;
        _shopOnly = shopOnly;
}

        protected override List<InvoiceForReturnDC> Body(System.Data.Common.DbConnection dbConnection)
        {
            var invoiceList = new List<InvoiceForReturnDC>();

            try
            {
                string storedProcedure = "GetInvoicesForReturn";
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                command.Parameters.Add(DataAcessUtils.CreateParam("@article_no", DbType.String, _articleNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branch_id", DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@member_id", DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@shop_only", DbType.Int32, _shopOnly));


                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var invoice = new InvoiceForReturnDC();
                    invoice.CreditorInvoiceId = Convert.ToInt32(reader["Id"]);
                    invoice.CreditorInvoiceAmount = Convert.ToDecimal(reader["CreditorInvoiceAmount"]);
                    invoice.CreditOrderLineAmount = Convert.ToDecimal(reader["CreditOrderLineAmount"]);
                    invoice.NoOfItems = Convert.ToInt32(reader["NoOfItems"]);
                    invoice.ArItemNO = Convert.ToInt32(reader["ARItemNo"]);
                    invoice.UnitPrice = Convert.ToDecimal(reader["UnitPrice"]);           
                    invoice.ArticleNo = Convert.ToInt32(reader["ArticleNo"]);
                    invoice.ArticleText = Convert.ToString(reader["ArticleText"]);                
                    invoice.Balance = Convert.ToDecimal(reader["Balance"]);
                    invoice.Regdate = Convert.ToDateTime(reader["Regdate"]);
                    invoice.InvoiceNo = Convert.ToString(reader["InvoiceNo"]);
                    invoice.BranchId = Convert.ToInt32(reader["BranchNo"]);
                    invoice.IsCredited = Convert.ToBoolean(reader["IsCredited"]);
                    //if (invoice.IsCredited)
                    //{
                    //    invoice.CreditedQuantity = Convert.ToInt32(reader["CreditedQuantity"]);
                    //    invoice.CreditorNotesCreditedAmount = Convert.ToDecimal(reader["CreditedAmount"]);
                    //}
                    // invoice.CreditedQuantity = Convert.ToInt32(reader["CreditedQuantity"]);
                    // invoice.CreditorNotesCreditedAmount = Convert.ToDecimal(reader["CreditedAmount"]);

                    invoiceList.Add(invoice);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return invoiceList;
        }
    }
}
