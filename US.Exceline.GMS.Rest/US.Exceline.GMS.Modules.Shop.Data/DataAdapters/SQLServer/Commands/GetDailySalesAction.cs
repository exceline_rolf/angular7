﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetDailySalesAction:USDBActionBase<List<ShopSalesItemDC>>
    {
        private DateTime _salesDate;
        private int _branchId;

        public GetDailySalesAction(DateTime salesDate, int branchId)
        {
            this._salesDate = salesDate;
            this._branchId = branchId;
        }

        protected override List<ShopSalesItemDC> Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSShopGetSales";
            List<ShopSalesItemDC> salesList = new List<ShopSalesItemDC>();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@salesDate", DbType.Date, _salesDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId",  DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ShopSalesItemDC salesItem = new ShopSalesItemDC()
                      {
                          ItemCode = reader["InventoryId"].ToString(),
                          ItemName = reader["SaleItemName"].ToString(),
                          Quantity = Convert.ToInt32(reader["SaleItemQuantity"]),
                          Amount = Convert.ToDecimal(reader["SaleItemAmount"]),
                          DiscountString = Convert.ToInt32(reader["SaleItemDiscount"])+"%",
                          TotalAmount = Convert.ToDecimal(reader["SaleItemTotalAmount"]),
                          SaleTime = Convert.ToDateTime(reader["SalesDateTime"]),
                          SettlementNo = Convert.ToInt32(reader["SettlementNo"])
                      };
                    salesList.Add(salesItem);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return salesList;
        }
    }
}
