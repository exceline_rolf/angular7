﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateGiftVoucherAction : USDBActionBase<bool>
    {
        private readonly int _memberId;
        private readonly string _voucherNo;
        private readonly bool _isConsumed;

        public UpdateGiftVoucherAction(int memberId, string voucherNum, bool IsConsumed)
        {
            _memberId = memberId;
             _voucherNo = voucherNum;
             _isConsumed = IsConsumed;
        }
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            var isUpdated = false;
            const string storedProcedureName = "USExceGMSUpdateGiftVoucherArticle"; 
            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@VoucherNumber", DbType.String, _voucherNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsConsumed", DbType.Boolean, _isConsumed));
                command.ExecuteNonQuery();
                isUpdated = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isUpdated;
        }
    }
}
