﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Shop.Data.SystemObjects
{
    public interface IShopDataAdapter
    {
        #region Sales

        SaleResultDC AddShopSales(String cardType, InstallmentDC installment, ShopSalesDC shopSaleDetails, PaymentDetailDC paymentDetails, int memberBranchID, int loggedBranchID, string user, string gymCode, bool isBookingPayment);
        SaleResultDC AddSunBedShopSales(SunBedHelperObject sunbedHelperObject, SunBedBusinessHelperObject businessHelperObj);
        int ValidateArticleStockLevel(InstallmentDC installment, string gymCode, int branchID);
        List<ShopSalesItemDC> GetDailySales(DateTime salesDate, int branchId, string gymCode);
        int SaveShopInstallment(InstallmentDC installment, int branchId, string gymCode, String SessionKey);
        DailySettlementDC GetDailySettlementsBySalePointId(int salePointId, string gymCode);
        int AddDailySettlement(DailySettlementDC dailysettlement, string gymCode);
        List<WithdrawalDC> GetWithdrawalByMemberId(DateTime startDate, DateTime endDate, WithdrawalTypes type, int memberId, string gymCode, string user);
        int SaveWithdrawal(int branchId, WithdrawalDC withDrawal, string gymCode, string user);
        int SaveOpenCashRegister(int branchId, CashRegisterDC cashRegister, string gymCode, string user);
        SalePointDC GetSalesPointByMachineName(int branchID, string machineName, string gymCode);
        List<ShopSalesPaymentDC> GetShopBillSummary(DateTime fromDate, DateTime toDate, int branchId, string gymCode);
        ShopBillDetailDC GetShopSaleBillDetails(int saleId, int branchId, string gymCode);
        Dictionary<string, decimal> GetMemberEconomyBalances(string gymCode, int memberId);

        #endregion

        #region Manage Vouchers
        List<VoucherDC> GetVouchersList(int branchId, string gymCode);
        bool AddVoucher(int branchId, VoucherDC voucher, string gymCode);
        int EditVoucher(int branchId, VoucherDC voucher, string gymCode);
        #endregion

        #region Returned Items
        List<ReturnItemDC> GetReturnList(int branchId, string gymCode);
        int AddReturnitem(int branchId, ReturnItemDC returnedItem, string gymCode);
        int EditReturnitem(int branchId, ReturnItemDC returnedItem, string gymCode);
        #endregion

        List<SalePointDC> GetSalesPointList(int branchID, string gymCode);
        bool AddEditUserHardwareProfile(int branchId, int hardwareProfileId, string user, string gymCode);
        int GetSelectedHardwareProfile(int branchId, string user, string gymCode);
        int SavePointOfSale(int branchId, SalePointDC pointOfSale, string user, string gymCode);
        int GetMemberByEmployeeID(int memberId, int branchId, string gymCode,string user);
        int GetNextGiftVoucherNumberAction(string seqId, string subSeqId, string gymCode);

        List<ShopSalesPaymentDC> GetGiftVoucherSaleSummary(DateTime fromDate, DateTime toDate, int branchId,
                                                           string gymCode);

        int ValidateGiftVoucherNumber(string voucherNumber,decimal payment, string gymCode, int branchID);
        
        int SaveGiftVoucherArticle(ArticleDC article, string gymCode);
        Dictionary<string, string> GetGiftVoucherDetail(string gymCode);
        bool IsCashdrawerOpen(string machinename, string gymCode, string user);
        DailySettlementPrintData ExcelineGetDailySettlementDataForPrint(String dailySettlementId, String reconiliationId, String salePointId, String branchId, String mode, String startDate, String endDate, String gymCode, String countedCashDraft);
        HelpDataForDailySettlmentPrintData ExcelineGetHelpDataForDailySettlmentPrintData(String branchId, String FromDateTime, String ToDateTime ,String SalePointId, String gymCode);
        int CheckIfReceiptIsValidForPrint(String invoiceNo, int branchId,  String gymCode);
        List<InvoiceForReturnDC> GetInvoicesForArticleReturn(int memberID, int branchId, string articleNo, int shopOnly, string gymCode, string user);
        String ExcelineGetReconcilliationTextForDailySettlementPrint(int dailSettlementId, String gymCode);
    }
}
