﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace ExcelineHelpService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IExcelineHelpService
    {
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "GetHelpUrl/{namsepaceStr}/{version}")]
         ResponseObj GetExcelineHelpUrl(string namsepaceStr, string version);
        
    }
}
