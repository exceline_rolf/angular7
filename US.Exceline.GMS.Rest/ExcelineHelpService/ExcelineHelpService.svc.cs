﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using ExcelineHelpService.DataAccess;
using US_DataAccess;


namespace ExcelineHelpService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
   
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, IncludeExceptionDetailInFaults = true)]
    public class ExcelineHelpService : IExcelineHelpService
    {
        public ResponseObj GetExcelineHelpUrl(string namsepaceStr, string version)
        {
            var action = new GetExcelineHelpUrlAction(namsepaceStr, version);
            var url = action.Execute(EnumDatabase.WorkStation);
            var obj = new ResponseObj {Url = url};
            return obj;
        }
    }
}
