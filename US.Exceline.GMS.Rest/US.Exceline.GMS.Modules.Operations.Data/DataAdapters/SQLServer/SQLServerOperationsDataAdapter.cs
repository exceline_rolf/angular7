﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using US.Common.Web.UI.Core.SystemObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands;
using US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands;
using US.Exceline.GMS.Modules.Operations.Data.SystemObjects;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer.Commands;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer
{
    class SQLServerOperationsDataAdapter : IOperationsDataAdapter
    {
        public List<ExcACCCommand> GetCommands(int branchId, string gymCode)
        {
            GetCommandsAction action = new GetCommandsAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public ExceACCSettings GetSettings(int branchId, string gymCode)
        {
            try
            {
                ExceACCSettings setting = new ExceACCSettings();
                Dictionary<string, object> gymSettingDictionary = new Dictionary<string, object>();
                List<string> settings = new List<string>();
                settings.Add("AccBlockAccessNoOfUnpaidInvoices");
                settings.Add("AccBlockAccessDueBalance");
                settings.Add("AccBlockAccessOnAccountBalance");
                settings.Add("AccMinutesBetweenAccess");
                settings.Add("AccIsSendSms");
                settings.Add("SMSAccessInfoPunchesLowestNo");
                settings.Add("AccIsDisplayName");
                settings.Add("AccIsDisplayCustomerNumber");
                settings.Add("AccIsDisplayContractTemplate");
                settings.Add("AccIsDisplayHomeGym");
                settings.Add("AccIsDisplayContractEndDate");
                settings.Add("AccIsDisplayBirthDate");
                settings.Add("AccIsDisplayAge");
                settings.Add("AccIsDisplayLastVisit");
                settings.Add("AccIsDisplayCreditBalance");
                settings.Add("AccIsDisplayPicture");
                settings.Add("AccIsDisplayLastAccess");
                settings.Add("AccIsDisplayNotification");

                GetSelectedGymSettingsAction action = new GetSelectedGymSettingsAction(settings, true, branchId, true);
                gymSettingDictionary = action.Execute(EnumDatabase.Exceline, gymCode);
                foreach (KeyValuePair<string, object> pair in gymSettingDictionary)
                {
                    switch (pair.Key)
                    {
                        case "AccBlockAccessNoOfUnpaidInvoices":
                            setting.MinimumUnpaidInvoices = Convert.ToInt32(pair.Value);
                            break;

                        case "AccBlockAccessDueBalance":
                            setting.UnPaidDueBalance = Convert.ToDecimal(pair.Value);
                            break;

                        case "AccBlockAccessOnAccountBalance":
                            setting.MinimumOnAccountBalance = Convert.ToDecimal(pair.Value);
                            break;

                        case "AccMinutesBetweenAccess":
                            setting.MinutesBetweenSwipes = Convert.ToInt32(pair.Value);
                            break;

                        case "AccIsSendSms":
                            setting.IsSendSms = Convert.ToBoolean(pair.Value);
                            break;

                        case "AccIsDisplayName":
                            setting.IsDisplayName = Convert.ToBoolean(pair.Value);
                            break;

                        case "AccIsDisplayCustomerNumber":
                            setting.IsDisplayCustomerNumber = Convert.ToBoolean(pair.Value);
                            break;

                        case "AccIsDisplayContractTemplate":
                            setting.IsDisplayContractTemplate = Convert.ToBoolean(pair.Value);
                            break;

                        case "AccIsDisplayHomeGym":
                            setting.IsDisplayHomeGym = Convert.ToBoolean(pair.Value);
                            break;

                        case "AccIsDisplayContractEndDate":
                            setting.IsDisplayContractEndDate = Convert.ToBoolean(pair.Value);
                            break;

                        case "AccIsDisplayBirthDate":
                            setting.IsDisplayBirthDate = Convert.ToBoolean(pair.Value);
                            break;

                        case "AccIsDisplayAge":
                            setting.IsDisplayAge = Convert.ToBoolean(pair.Value);
                            break;

                        case "AccIsDisplayLastVisit":
                            setting.IsDisplayLastVisit = Convert.ToBoolean(pair.Value);
                            break;

                        case "AccIsDisplayCreditBalance":
                            setting.IsDisplayCreditBalance = Convert.ToBoolean(pair.Value);
                            break;

                        case "AccIsDisplayPicture":
                            setting.IsDisplayPicture = Convert.ToBoolean(pair.Value);
                            break;

                        case "AccIsDisplayLastAccess":
                            setting.IsDisplayLastAccess = Convert.ToBoolean(pair.Value);
                            break;

                        case "AccIsDisplayNotification":
                            setting.IsDisplayNotification = Convert.ToBoolean(pair.Value);
                            break;
                    }
                }

                UpdateStatusAction updateAction = new UpdateStatusAction(branchId, "SETTINGS", "TAKEN");
                updateAction.Execute(EnumDatabase.Exceline,gymCode);
                return setting;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ExceACCAccessProfileTime> GetAccessTimes(int branchId, string gymCode, string dateFormat)
        {
            try
            {       
                List<ExceAccessProfileDC> AccessProfiles = new List<ExceAccessProfileDC>();
                List<ExceACCAccessProfileTime> AccessTimes = new List<ExceACCAccessProfileTime>();
                GetAccessProfilesAction action = new GetAccessProfilesAction(gymCode, Gender.ALL);
                AccessProfiles = action.Execute(EnumDatabase.Exceline, gymCode);

                foreach (ExceAccessProfileDC profile in AccessProfiles)
                {
                    GetBranchesForAccessProfileAction action2 = new GetBranchesForAccessProfileAction(profile.Id);
                    List<int> branchIdList = action2.Execute(EnumDatabase.Exceline, gymCode);
                    profile.BranchIdList = branchIdList;

                    foreach (ExceAccessProfileTimeDC profileTime in profile.AccessTimeList)
                    {
                        ExceACCAccessProfileTime accessTime = new ExceACCAccessProfileTime();
                        accessTime.AccessProfileId = Convert.ToString(profileTime.AccessProfileId);
                        accessTime.AccessProfileName = profile.AccessProfileName;
                        accessTime.AllowExpressGym = Convert.ToString(profileTime.AllowExpressGym);
                        accessTime.AllowExtraActivity = Convert.ToString(profileTime.AllowExtraActivity);
                        accessTime.AssignedDays = profileTime.AssignedDays;
                        accessTime.Friday = Convert.ToString(profileTime.Friday);
                        accessTime.FromTime = profileTime.FromTime.Value.ToString(dateFormat);
                        accessTime.Monday = Convert.ToString(profileTime.Monday);
                        accessTime.Saturday = Convert.ToString(profileTime.Saturday);
                        accessTime.Sunday = Convert.ToString(profileTime.Sunday);
                        accessTime.Thursday = Convert.ToString(profileTime.Thursday);
                        accessTime.ToTime = profileTime.ToTime.Value.ToString(dateFormat);
                        accessTime.Tuesday = Convert.ToString(profileTime.Tuesday);
                        accessTime.Wednesday = Convert.ToString(profileTime.Wednesday);
                        accessTime.BranchIdList = profile.BranchIdList;
                        accessTime.Gender = profile.Gender.ToString();
                        AccessTimes.Add(accessTime);
                    }
                }
                return AccessTimes;
            }
            catch (Exception)
            {
                throw;
            }
        }

       

        public List<GymACCOpenTime> GetOpenTimes(int branchId, string gymCode)
        {
            try
            {
                List<GymOpenTimeDC> openTimes = new List<GymOpenTimeDC>();
                List<GymACCOpenTime> gymOpenTimes = new List<GymACCOpenTime>();
                GetGymOpenTimeAction action = new GetGymOpenTimeAction(branchId, gymCode);
                openTimes = action.Execute(EnumDatabase.Exceline, gymCode);
                foreach (GymOpenTimeDC openTime in openTimes)
                {
                    GymACCOpenTime oTime = new GymACCOpenTime();
                    oTime.BranchId = Convert.ToString(openTime.BranchId);
                    oTime.BranchName = Convert.ToString(openTime.BranchName);
                    oTime.Days = openTime.Days;
                    oTime.EndTime = openTime.EndTime.Value.ToString("yyyy.MM.dd HH:mm:ss"); ;
                    oTime.Id = Convert.ToString(openTime.Id);
                    oTime.IsExpress = Convert.ToString(openTime.IsExpress);
                    oTime.IsFriday = Convert.ToString(openTime.IsFriday);
                    oTime.IsMonday = Convert.ToString(openTime.IsMonday);
                    oTime.IsSaturday = Convert.ToString(openTime.IsSaturday);
                    oTime.IsSunday = Convert.ToString(openTime.IsSunday);
                    oTime.IsThursday = Convert.ToString(openTime.IsThursday);
                    oTime.IsTuesday = Convert.ToString(openTime.IsTuesday);
                    oTime.IsWednesday = Convert.ToString(openTime.IsWednesday);
                    oTime.Region = openTime.Region;
                    oTime.StartTime = openTime.StartTime.Value.ToString("yyyy.MM.dd HH:mm:ss"); ;
                    gymOpenTimes.Add(oTime);
                }

                UpdateStatusAction updateAction = new UpdateStatusAction(branchId, "GYMACESS", "TAKEN");
                updateAction.Execute(EnumDatabase.Exceline, gymCode);
                return gymOpenTimes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ExceACCGymClosedTime> GetClosedTimes(int branchId, string gymCode)
        {
            try
            {
                List<ActivityTimeDC> ClosedTimes = new List<ActivityTimeDC>();
                List<ExceACCGymClosedTime> unavailableTimes = new List<ExceACCGymClosedTime>();
                GetActivityUnavailableTimeAction action = new GetActivityUnavailableTimeAction(branchId, -1);
                ClosedTimes = action.Execute(EnumDatabase.Exceline, gymCode);
                foreach (ActivityTimeDC acvitiyTime in ClosedTimes)
                {
                    ExceACCGymClosedTime uTime = new ExceACCGymClosedTime();
                    uTime.ActivityId = Convert.ToString(acvitiyTime.ActivityId);
                    uTime.ActivityName = Convert.ToString(acvitiyTime.ActivityName);
                    uTime.BranchId = Convert.ToString(acvitiyTime.BranchId);
                    uTime.Edited = Convert.ToString(acvitiyTime.Edited);
                    uTime.EndDate = acvitiyTime.EndDate.Value.ToString("yyyy.MM.dd HH:mm:ss");
                    uTime.EndTime = acvitiyTime.EndTime.Value.ToString("yyyy.MM.dd HH:mm:ss");
                    uTime.Id = Convert.ToString(acvitiyTime.Id);
                    uTime.Reason = acvitiyTime.Reason;
                    uTime.Startdate = acvitiyTime.Startdate.Value.ToString("yyyy.MM.dd HH:mm:ss");
                    uTime.StartTime = acvitiyTime.StartTime.Value.ToString("yyyy.MM.dd HH:mm:ss");
                    unavailableTimes.Add(uTime);
                }

                UpdateStatusAction updateAction = new UpdateStatusAction(branchId, "UNTIMES", "TAKEN");
                updateAction.Execute(EnumDatabase.Exceline, gymCode);
                return unavailableTimes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ExceACCMember> GetMembers(int branchId, string gymCode)
        {
            try
            {
                List<ExceACCMember> members = new List<ExceACCMember>();
                GetMemberLoginDataPerDayAction action = new GetMemberLoginDataPerDayAction(DateTime.Today, gymCode, branchId);
                members = action.Execute(EnumDatabase.Exceline, gymCode);

                UpdateStatusAction updateAction = new UpdateStatusAction(branchId, "MEMBER", "TAKEN");
                updateAction.Execute(EnumDatabase.Exceline, gymCode);
                return members;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetImage(int branchId, string gymCode, string customerNo)
        {
            GetImageAction action = new GetImageAction(customerNo);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<person> GetARXMembers(int branchId, string gymCode, bool isAllMember)
        {
            GetARXMembersAction membersAction = new GetARXMembersAction(branchId, isAllMember);
            return membersAction.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int GetSystemId(string gymCode)
        {
            GetSystemIDAction action = new GetSystemIDAction(gymCode);
            return action.Execute(EnumDatabase.WorkStation, gymCode);
        }

        public int AddVisitList(string gymCode, List<EntityVisitDC> visitList)
        {
            AddVisitAction action = new AddVisitAction(visitList);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int RegisterControl(ExcACCAccessControl accessControl, string gymCode)
        {
            RegisterAccessControlAction action = new RegisterAccessControlAction(accessControl);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int PingServer(int terminalId, string gymCode)
        {
            PingServerAction action = new PingServerAction(terminalId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddEventLogs(ExcAccessEvent accessEvent, string gymCode)
        {
            AddAccessEventAction action = new AddAccessEventAction(accessEvent);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public ExceACCTerminal GetTerminalDetails(int terminalID, string gymCode)
        {
            GetTerminalDetailsAction action = new GetTerminalDetailsAction(terminalID, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int GetAvailableVisitByContractId(int contractId, string gymCode)
        {
            GetAvailableVisitByContractIdAction action = new GetAvailableVisitByContractIdAction(contractId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
    }
}
