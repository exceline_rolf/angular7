﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    class GetAvailableVisitByContractIdAction : USDBActionBase<int>
    {
        private readonly int _contractId;
        public GetAvailableVisitByContractIdAction(int contractId)
        {
            _contractId = contractId;
        }
        protected override int Body(DbConnection connection)
        {
            const string spName = "USExceGMACCGetAvailableVisitByContractId";
            int result = -1;
            try
            {

                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ContractId", DbType.Int32, _contractId));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    result = Convert.ToInt32(reader["AvailableVisits"]);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
