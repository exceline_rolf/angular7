﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    public class GetImageAction : USDBActionBase<string>
    {
        private string _customerNo = string.Empty;
        public GetImageAction(string customerNo)
        {
            _customerNo = customerNo;
        }

        protected override string Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSACCGetMemberImagePath";
            string imagePath = string.Empty;
            string imageDate = string.Empty;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@CustomerNo", System.Data.DbType.String, _customerNo));
                imagePath = command.ExecuteScalar().ToString();

                if (!string.IsNullOrEmpty(imagePath))
                {
                    if (File.Exists(imagePath))
                    {
                        imageDate  = System.IO.File.ReadAllText(imagePath);
                        return imageDate;
                    }
                    return imageDate;
                }
                return imageDate;
            }catch(Exception)
            {
                throw;
            }
        }
    }
}
