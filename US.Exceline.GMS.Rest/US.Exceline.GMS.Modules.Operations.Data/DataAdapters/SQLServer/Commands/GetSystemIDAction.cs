﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    public class GetSystemIDAction : USDBActionBase<int>
    {
        private string _gymCode = string.Empty;
        public GetSystemIDAction(string gymCode)
        {
            _gymCode = gymCode;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string SpName = "ExceWorkStationGetSystemIdByGymCode";
            int systemId = -1;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, SpName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@GymCode", System.Data.DbType.String, _gymCode));
                systemId = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception)
            {
                throw;
            }
            return systemId;
        }
    }
}
