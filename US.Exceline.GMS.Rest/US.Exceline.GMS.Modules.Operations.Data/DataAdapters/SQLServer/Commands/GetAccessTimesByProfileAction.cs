﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Web.UI.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    class GetAccessTimesByProfileAction : USDBActionBase<List<ExceAccessProfileTime>>
    {
        private int _branchId = -1;
        private int _profileID = -1;
        public GetAccessTimesByProfileAction(int profileID, int branchId)
        {
            this._profileID = profileID;
            this._branchId = branchId;
        }

        protected override List<ExceAccessProfileTime> Body(System.Data.Common.DbConnection connection)
        {
            string GetAccessProfileTimes = "USExceGMSGetAccessProfilesTimes";
            List<ExceAccessProfileTime> profileTimeList = new List<ExceAccessProfileTime>();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, GetAccessProfileTimes);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@profileId", System.Data.DbType.Int32, _profileID));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExceAccessProfileTime profileTime = new ExceAccessProfileTime();
                    profileTime.Id = Convert.ToInt32(reader["ID"]);
                    profileTime.AccessProfileId = Convert.ToInt32(reader["AccessProfileId"]);
                    profileTime.FromTime = DateTime.Parse(((reader["FromTime"].ToString())));
                    profileTime.ToTime = DateTime.Parse(((reader["ToTime"].ToString())));
                    profileTime.Monday = Convert.ToBoolean(reader["Monday"]);
                    profileTime.Tuesday = Convert.ToBoolean(reader["Tuesday"]);
                    profileTime.Wednesday = Convert.ToBoolean(reader["Wednesday"]);
                    profileTime.Thursday = Convert.ToBoolean(reader["Thursday"]);
                    profileTime.Friday = Convert.ToBoolean(reader["Friday"]);
                    profileTime.Saturday = Convert.ToBoolean(reader["Saturday"]);
                    profileTime.Sunday = Convert.ToBoolean(reader["Sunday"]);
                    profileTimeList.Add(profileTime);
                }
                reader.Close();
                if (profileTimeList.Count == 0)
                {
                    cmd.Cancel();
                    string GetGymTime = "USExceGMSGetGymAccessTime";

                    try
                    {
                        DbCommand cmd1 = CreateCommand(CommandType.StoredProcedure, GetGymTime);
                        cmd1.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                        DbDataReader reader1 = cmd1.ExecuteReader();

                        while (reader1.Read())
                        {
                            ExceAccessProfileTime profileTime = new ExceAccessProfileTime();
                            profileTime.Id = Convert.ToInt32(reader1["ID"]);
                            profileTime.FromTime = DateTime.Parse(((reader1["FromTime"].ToString())));
                            profileTime.ToTime = DateTime.Parse(((reader1["ToTime"].ToString())));
                            profileTime.Monday = true;
                            profileTime.Tuesday = true;
                            profileTime.Wednesday = true;
                            profileTime.Thursday = true;
                            profileTime.Friday = true;
                            profileTime.Saturday = true;
                            profileTime.Sunday = true;
                            profileTimeList.Add(profileTime);
                        }


                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
                return profileTimeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
