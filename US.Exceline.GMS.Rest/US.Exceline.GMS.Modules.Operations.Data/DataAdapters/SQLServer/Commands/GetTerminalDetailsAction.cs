﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    public class GetTerminalDetailsAction : USDBActionBase<ExceACCTerminal>
    {
        private int _terminalId = -1;
        private string _gymCode = string.Empty;
        public GetTerminalDetailsAction(int terminalId, string gymCode)
        {
            _terminalId = terminalId;
            _gymCode = gymCode;
        }

        protected override ExceACCTerminal Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceACCGetTerminalDetails";
            ExceACCTerminal terminal = null;
            try
            {
                
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@TerminalID", System.Data.DbType.Int32, _terminalId));
                DbDataReader reader = command.ExecuteReader();
               
                while (reader.Read())
                {
                    terminal = new ExceACCTerminal();
                    terminal.TerminalID = Convert.ToInt32(reader["Id"]);
                    terminal.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                }

               
                GetAccessProfilesForTerminalAction action = new GetAccessProfilesForTerminalAction(_terminalId);
                terminal.AccessProfileIDList = action.Execute(EnumDatabase.Exceline, _gymCode);
            }
            catch (Exception)
            {
                throw;
            }
            return terminal;
        }
    }
}
