﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    class SaveMemberVisitAction : USDBActionBase<int>
    {
        private EntityVisitDC _memberVisit = null;
      

        public SaveMemberVisitAction(EntityVisitDC memberVisit,string gymCode, int branchId)
        {
            this._memberVisit = memberVisit;
            OverwriteUser(gymCode);
        }

        protected override int Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSMemberSaveMemberVisit";
            int result = 0;
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberVisit.EntityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InTime", DbType.DateTime, _memberVisit.InTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VisitDate", DbType.DateTime, _memberVisit.InTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _memberVisit.BranchId));
                
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@outId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);


                cmd.ExecuteNonQuery(); 

                int taskId = Convert.ToInt32(param.Value);
                if (taskId > 0)
                    result = taskId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
