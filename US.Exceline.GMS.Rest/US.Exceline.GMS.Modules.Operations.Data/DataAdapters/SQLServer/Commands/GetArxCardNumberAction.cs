﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    public class GetArxCardNumberAction : USDBActionBase<List<card>>
    {
        private readonly int _memberId;
        public GetArxCardNumberAction(int memberId)
        {
            _memberId = memberId;
        }

        protected override List<card> Body(DbConnection connection)
        {
            const string spName = "USExceGMSGetArxCardNumber";
            var cards = new List<card>();
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberId));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var c = new card
                        {
                            number = Convert.ToString(reader["MemCardNo"]),
                            format_name = Convert.ToString(reader["FormatName"]),
                            IsDeleted = Convert.ToBoolean(reader["Deleted"])
                        };

                    cards.Add(c);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return cards;
        }
    }
}
