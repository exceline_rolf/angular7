﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    public class GetAccessProfilesForTerminalAction : USDBActionBase<List<int>>
    {
        private int _terminalId = -1;

        public GetAccessProfilesForTerminalAction(int terminalId)
        {
            _terminalId = terminalId;
        }

        protected override List<int> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMACCGetAccessProfilesforControl";
            List<int> accessProfileIds = new List<int>();
            try
            {

                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@TerminalID", System.Data.DbType.Int32, _terminalId));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    accessProfileIds.Add(Convert.ToInt32(reader["AccessProfileId"]));
                }
                return accessProfileIds;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
