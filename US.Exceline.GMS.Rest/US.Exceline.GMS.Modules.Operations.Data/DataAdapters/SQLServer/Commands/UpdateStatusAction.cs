﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    class UpdateStatusAction : USDBActionBase<bool>
    {
        private int _branchId = -1;
        private string _entityName = string.Empty;
        private string _commond = string.Empty;

        public UpdateStatusAction(int branchId, string entityName, string command)
        {
            _branchId = branchId;
            _entityName = entityName;
            _commond = command;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSACCUpdateStatus";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@EntityName", System.Data.DbType.String, _entityName));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Command", System.Data.DbType.String, _commond));

                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                // Do nothing
            }
            return false;
        }
    }
}
