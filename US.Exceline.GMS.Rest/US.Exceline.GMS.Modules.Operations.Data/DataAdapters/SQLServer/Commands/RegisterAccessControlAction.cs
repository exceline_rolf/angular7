﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    public class RegisterAccessControlAction : USDBActionBase<int>
    {
        private ExcACCAccessControl _accessControl;
        public RegisterAccessControlAction(ExcACCAccessControl accessControl)
        {
            _accessControl = accessControl;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string Spname = "ExceGMSACCRegisterAccessControl";
            int addedId = -1;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, Spname);
                command.Parameters.Add(DataAcessUtils.CreateParam("@Id", System.Data.DbType.Int32, _accessControl.ID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Name", System.Data.DbType.String, _accessControl.Name));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _accessControl.BranchId));
                addedId = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception)
            {
                throw;
            }
            return addedId;
        }
    }
}
