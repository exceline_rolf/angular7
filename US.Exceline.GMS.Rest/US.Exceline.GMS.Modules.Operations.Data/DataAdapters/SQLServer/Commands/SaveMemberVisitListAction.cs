﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    class SaveMemberVisitListAction : USDBActionBase<int>
    {

        private DataTable _dataTable;

        public SaveMemberVisitListAction(List<EntityVisitDC> memberVisit, string gymCode, int branchId)
        {
            _dataTable = MemberVisits(memberVisit);
            OverwriteUser(gymCode);
        }


        private DataTable MemberVisits(List<EntityVisitDC> memberVisit)
        {
            _dataTable = new DataTable();

            _dataTable.Columns.Add(new DataColumn("MemberId", Type.GetType("System.Int32")));
            _dataTable.Columns.Add(new DataColumn("InTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("VisitDate", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("BranchId", Type.GetType("System.Int32")));

            foreach (EntityVisitDC visit in memberVisit)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["MemberId"] = visit.EntityId;
                _dataTableRow["InTime"] = visit.InTime;
                _dataTableRow["VisitDate"] = visit.InTime;
                _dataTableRow["BranchId"] = visit.BranchId;
                _dataTable.Rows.Add(_dataTableRow);
            }
            return _dataTable;
        }

        protected override int Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSMemberSaveMemberVisitList";
            int result = 0;
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberVisits", SqlDbType.Structured, _dataTable));

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@outId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);


                cmd.ExecuteNonQuery();

                int id = Convert.ToInt32(param.Value);
                if (id > 0)
                    result = id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}