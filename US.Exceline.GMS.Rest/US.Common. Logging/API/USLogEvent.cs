﻿// --------------------------------------------------------------------------
// Copyright(c) 2012 UnicornSolutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Common.Logging
// Coding Standard   : US Coding Standards
// Author            : AAB
// Created Timestamp : 05/01/2012 
// --------------------------------------------------------------------------
using System;
using System.IO;
using System.Collections.Generic;

namespace US.Common.Logging.API
{
    public static class USLogEvent
    {
        private static USLogConfiguration _usLogConfiguration = new USLogConfiguration();
        /// <summary>
        /// In configuration= 0-nothing; 1-Text; 2-DB; 3-Db and Text
        /// </summary>
        /// <param name="message"></param>
        /// <param name="trace"></param>
        /// <param name="user"></param>
        /// <param name="applicationName"></param>
        /// <param name="category"></param>
        /// <param name="eventDescription1"></param>
        /// <param name="eventDescription2"></param>
        /// <param name="eventDescription3"></param>
        public static void LogEvent(string message, string user, string applicationName, string category = "", string eventDescription1 = "", string eventDescription2 = "", string eventDescription3 = "")
        {
            _usLogConfiguration = USLogConfigurationReader.GetUSPCongfiuration();

            switch (_usLogConfiguration.EnableEventLog)
            {
                case 0:
                    {
                        break;
                    }
                case 1:
                    {
                        WriteToFile(message, user);
                        break;
                    }
                case 2:
                    {
                        WriteToFile( message,  user);
                        break;
                    }
                case 3:
                    {
                        WriteToFile(message, user);
                        WriteToFile(message, user);
                        break;
                    }
                default:
                    break;
            }


        }

        public static void WriteToFile(string message, string user)
        {

            try
            {
                _usLogConfiguration = USLogConfigurationReader.GetUSPCongfiuration();
                if (_usLogConfiguration.EnableEventLog == 1)
                {
                    string filePath = _usLogConfiguration.UsLogRootFolder + "\\EventLog";
                    string fileName = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + ".txt";
                    string fileNameWithPath = filePath + "\\" + fileName;

                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }

                    string eventMessage = DateTime.Now.ToShortDateString()+":"+DateTime.Now.ToShortTimeString() + " : " + user + " : " + message;

                    StreamWriter sw = new StreamWriter(new FileStream(fileNameWithPath, FileMode.Append, FileAccess.Write));
                    sw.WriteLine(eventMessage);
                    sw.Close();
                   
                }
            }
            catch (Exception)
            {

              
            }

        }
        //public static void WriteToDB(string message, string user, string applicationName, string category = "", string eventDescription1 = "", string eventDescription2 = "", string eventDescription3 = "")
        //{
        //    try
        //    {
        //        _usLogConfiguration = USLogConfigurationReader.GetUSPCongfiuration();
        //        if (_usLogConfiguration.EnableEventLog == 1)
        //        {

        //            LogDBManager logManager = new LogDBManager();
        //            logManager.WrightEventToSQLDB(message, user, applicationName, category, eventDescription1, eventDescription2, eventDescription3);

        //        }
        //    }
        //    catch 
        //    {
                
               
        //    }
            
        //}
        public static void SendEmailNotification(List<string> messages, Exception ex, string user)
        {

            //TO DO
        }
    }
}
