﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class SMSEmailInvoiceDetails
    {
        public int invoiceBranchId { get; set; }
        public InstallmentDC selectedOrder { get; set; }
        public PaymentDetailDC paymentDetails { get; set; }
        public bool isSMSselected { get; set; }
        public bool isEmailSelected { get; set; }
        public string SMStext { get; set; }
        public string EmailText { get; set; }
        public string EmailAddress { get; set; }
        public string SMSNo { get; set; }
        public int memberBranchID {get; set;}
    }
}