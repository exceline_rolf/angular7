﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class UpdateNotificationStatus
    {
        public List<int> selectedIds { get; set; }
        public int status { get; set; }
     
    }
}