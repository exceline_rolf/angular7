﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class DeletePayment
    {
        public int PaymentId { get; set; }
        public bool KeepPayment { get; set; }
        public string Comment { get; set; }
    }
}