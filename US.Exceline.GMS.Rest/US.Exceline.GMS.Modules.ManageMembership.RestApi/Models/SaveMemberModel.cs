﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class SaveMemberModel
    {
        public OrdinaryMemberDC Member { get; set; }
        public int BranchId { get; set; }
        public string NotificationTitle { get; set; }
    }
}