﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class UpdateIntroducedMembers
    {
        public int memberId { get; set; }
        public DateTime? creditedDate { get; set; }
        public string creditedText { get; set; }
        public bool isDelete { get; set; }
    }
}