﻿using IO.Swagger.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class RootObject
    {
        [JsonProperty("brukerDtos")]
        public List<BrukerDto> Results { get; set; }
    }
}