﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class MemberFeeGenerationDetails
    {
        public List<int> gyms { get; set; }
        public int month { get; set; }
    }
}