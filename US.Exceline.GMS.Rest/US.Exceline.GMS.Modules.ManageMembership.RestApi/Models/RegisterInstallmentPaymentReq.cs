﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class RegisterInstallmentPaymentReq
    {
        // int memberBranchID, int loggedbranchID, string user, InstallmentDC installment, 
        private int _memberBranchID;

        public int MemberBranchID
        {
            get { return _memberBranchID; }
            set { _memberBranchID = value; }
        }

        private int _loggedbranchID;

        public int LoggedbranchID
        {
            get { return _loggedbranchID; }
            set { _loggedbranchID = value; }
        }

        private InstallmentDC _installment;

        public InstallmentDC Installment
        {
            get { return _installment; }
            set { _installment = value; }
        }
        //PaymentDetailDC paymentDetail, string gymCode, int salePointID, ShopSalesDC salesDetails

        private PaymentDetailDC _paymentDetail;

        public PaymentDetailDC PaymentDetail
        {
            get { return _paymentDetail; }
            set { _paymentDetail = value; }
        }

        private int _salePointID;

        public int SalePointID
        {
            get { return _salePointID; }
            set { _salePointID = value; }
        }

        private ShopSalesDC _salesDetails;

        public ShopSalesDC SalesDetails
        {
            get { return _salesDetails; }
            set { _salesDetails = value; }
        }



    }
}