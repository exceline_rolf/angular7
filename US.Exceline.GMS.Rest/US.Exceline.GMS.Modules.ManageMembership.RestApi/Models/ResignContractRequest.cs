﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class ResignContractRequest
    {

        private ContractResignDetailsDC _resignDetail;
        public ContractResignDetailsDC ResignDetail
        {
            get { return _resignDetail; }
            set { _resignDetail = value; }
        }

        private string _notificationMethod = string.Empty;
        public string NotificationMethod
        {
            get { return _notificationMethod; }
            set { _notificationMethod = value; }
        }

        private string _senderDescription;
        public string SenderDescription
        {
            get { return _senderDescription; }
            set { _senderDescription = value; }
        }

        private int _branchId;
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }
    }
}