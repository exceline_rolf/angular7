﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class DeleteOrderDetails
    {
        public List<int> installmentIdList { get; set; }
        public int memberContractId { get; set; }
    }
}