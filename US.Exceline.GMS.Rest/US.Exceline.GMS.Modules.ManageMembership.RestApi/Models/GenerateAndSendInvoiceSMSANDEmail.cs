﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class GenerateAndSendInvoiceSMSANDEmail
    {
        public int invoiceBranchId { get; set; }
        public InstallmentDC selectedOrder { get; set; }
        public PaymentDetailDC paymentDetails { get; set; }
        public Dictionary<string, bool> isSelected { get; set; }
        public Dictionary<string, string> text { get; set; }
        public Dictionary<string, string> senderDetail { get; set; }
        public int memberBranchID { get; set; }
    }
}