﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class MemberSMS
    {
        public int memberID { get; set; }
        public int branchId { get; set; }
        public string message { get; set; }
        public string mobileNo { get; set; }

    }
}