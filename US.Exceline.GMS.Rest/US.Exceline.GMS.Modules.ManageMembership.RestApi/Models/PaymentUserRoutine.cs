﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class PaymentUserRoutine
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}