﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging.API;
using US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Controllers
{
    [RoutePrefix("api/Member/Sponsor")]
    public class SponsorController : ApiController
    {

        [HttpGet]
        [Route("GetSponsorSetting")]
        [Authorize]
        public HttpResponseMessage GetSponsorSetting(int branchId, int sponsorId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();

                OperationResult<SponsorSettingDC> sponsorSetting = GMSManageMembership.GetSponsorSetting(branchId, user, sponsorId, ExceConnectionManager.GetGymCode(user));
                if (sponsorSetting.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in sponsorSetting.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {

                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(sponsorSetting.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetSponsorDiscount")]
        [Authorize]
        public HttpResponseMessage GetGroupDiscountByType(int branchId, int discountTypeId, int sponsorId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();

                OperationResult<List<DiscountDC>> discountList = GMSManageMembership.GetGroupDiscountByType(branchId, user, discountTypeId, ExceConnectionManager.GetGymCode(user), sponsorId);
                if (discountList.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in discountList.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {

                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(discountList.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpGet]
        [Route("GetEmployeeLevelList")]
        [Authorize]
        public HttpResponseMessage GetSponsorEmployeeCategoryList(int sponsorId, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();

                OperationResult<List<EmployeeCategoryDC>> empCategoryList = GMSManageMembership.GetSponsorEmployeeCategoryList(branchId, ExceConnectionManager.GetGymCode(user), sponsorId);
                if (empCategoryList.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in empCategoryList.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {

                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(empCategoryList.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetEmployeeCategoryBySponsorId")]
        [Authorize]
        public HttpResponseMessage GetEmployeeCategoryBySponsorId(int branchId, int sponsorId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<OrdinaryMemberDC> empCategoryList = GMSManageMembership.GetEmployeeCategoryBySponsorId(branchId, ExceConnectionManager.GetGymCode(user), sponsorId);
                if (empCategoryList.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in empCategoryList.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {

                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(empCategoryList.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpPost]
        [Route("SaveSponsorSetting")]
        [Authorize]
        public HttpResponseMessage SaveSponsorSetting(SponsorSettingDC sponsorSetting)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                int branchId = -1;
                if (sponsorSetting != null)
                {
                    branchId = sponsorSetting.BranchId;
                }

                OperationResult<bool> result = GMSManageMembership.SaveSponsorSetting(sponsorSetting, branchId, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {

                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }




        [HttpPost]
        [Route("AddSponserShipForMember")]
        [Authorize]
        public HttpResponseMessage AddSponserShipForMember(int memContractId, int sponserContractId, int employeeCategotyID, int actionType, string employeeRef)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();

                OperationResult<bool> result = GMSManageMembership.AddSponserShipForMember(memContractId, sponserContractId, employeeCategotyID, actionType, employeeRef, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {

                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("DeleteDiscountCategory")]
        [Authorize]
        public HttpResponseMessage DeleteDiscountCategory(int categoryID)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var discountList = new List<int>();
                discountList.Add(categoryID);

                OperationResult<bool> deleteResult = GMSManageMembership.DeleteDiscount(discountList, ExceConnectionManager.GetGymCode(user));
                if (deleteResult.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in deleteResult.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {

                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(deleteResult.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpGet]
        [Route("DeleteEmployeeCategory")]
        [Authorize]
        public HttpResponseMessage DeleteEmployeeCategory(int categoryID)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<bool> deleteResult = GMSManageMembership.DeleteEmployeeCategory(categoryID, ExceConnectionManager.GetGymCode(user));
                if (deleteResult.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in deleteResult.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(deleteResult.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("RemoveSponsorshipOfMember")]
        [Authorize]
        public HttpResponseMessage RemoveSponsorshipOfMember(int sponsoredRecordId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<bool> deleteResult = GMSManageMembership.RemoveSponsorshipOfMember(sponsoredRecordId, ExceConnectionManager.GetGymCode(user), user);
                if (deleteResult.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in deleteResult.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(deleteResult.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }


    }
}
