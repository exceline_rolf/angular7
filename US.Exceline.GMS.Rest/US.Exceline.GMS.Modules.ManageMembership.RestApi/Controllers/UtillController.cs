﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging.API;
using US.Exceline.GMS.Modules.Admin.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Controllers
{
    [RoutePrefix("api/Member/Utill")]
    public class UtillController : ApiController
    {
        [HttpGet]
        [Route("GetActivities")]
        [Authorize]
        public HttpResponseMessage GetActivities(int branchID)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = US.Exceline.GMS.API.GMSContract.GetActivities(branchID, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ActivityDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));  
                }
            }
            catch(Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ActivityDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetGymCompanySettings")]
        [Authorize]
        public HttpResponseMessage GetGymCompanySettings(GymCompanySettingType gymCompanySettingType)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();

                if (gymCompanySettingType == GymCompanySettingType.INFO)
                {
                    var infoResult = GMSSystemSettings.GetGymCompanyInfoSettings(ExceConnectionManager.GetGymCode(user));
                    if (infoResult.ErrorOccured)
                    {
                        List<string> errorMsg = new List<string>();

                        foreach (NotificationMessage message in infoResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            errorMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(infoResult.OperationReturnValue, ApiResponseStatus.OK));

                }
                else if (gymCompanySettingType == GymCompanySettingType.VAT)
                {
                    var vatResult = GMSSystemSettings.GetGymCompanyVATSettings(ExceConnectionManager.GetGymCode(user));
                    if (vatResult.ErrorOccured)
                    {
                        List<string> errorMsg = new List<string>();

                        foreach (NotificationMessage message in vatResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            errorMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(vatResult.OperationReturnValue, ApiResponseStatus.OK));

                }

                else if (gymCompanySettingType == GymCompanySettingType.ECONOMY)
                {
                    var economyResult = GMSSystemSettings.GetGymCompanyEconomySettings(ExceConnectionManager.GetGymCode(user));
                    if (economyResult.ErrorOccured)
                    {
                        List<string> errorMsg = new List<string>();

                        foreach (NotificationMessage message in economyResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            errorMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(economyResult.OperationReturnValue, ApiResponseStatus.OK));

                }

                else if (gymCompanySettingType == GymCompanySettingType.OTHER)
                {
                    var otherResult = GMSSystemSettings.GetGymCompanyOtherSettings(ExceConnectionManager.GetGymCode(user));
                    if (otherResult.ErrorOccured)
                    {
                        List<string> errorMsg = new List<string>();

                        foreach (NotificationMessage message in otherResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            errorMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(otherResult.OperationReturnValue, ApiResponseStatus.OK));

                }
                else if (gymCompanySettingType == GymCompanySettingType.CONTRACT)
                {
                    var conditionResult = GMSSystemSettings.GetGymCompanyContractConditions(ExceConnectionManager.GetGymCode(user));
                    if (conditionResult.ErrorOccured)
                    {
                        List<string> errorMsg = new List<string>();

                        foreach (NotificationMessage message in conditionResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            errorMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(conditionResult.OperationReturnValue, ApiResponseStatus.OK));

                }
                else
                {
                    OperationResult<string> OperatioResult = new OperationResult<string>();
                    OperatioResult.OperationReturnValue = "Invalid  Setting  Type";
                    if (OperatioResult.ErrorOccured)
                    {
                        List<string> errorMsg = new List<string>();

                        foreach (NotificationMessage message in OperatioResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            errorMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(OperatioResult.OperationReturnValue, ApiResponseStatus.OK));

                }


            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }
    }


}
