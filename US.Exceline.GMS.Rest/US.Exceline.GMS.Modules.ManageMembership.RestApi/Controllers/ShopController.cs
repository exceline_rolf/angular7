﻿using SessionManager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging.API;
using US.Common.Notification.API;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Communication.API;
using US.Communication.Core.SystemCore.DomainObjects;
using US.Exceline.GMS.Modules.Admin.API.ManageEmployees;
using US.Exceline.GMS.Modules.Admin.API.ManageResources;
using US.Exceline.GMS.Modules.Admin.API.ManageSystemSettings;
using US.Exceline.GMS.Modules.Login.API;
using US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership;
using US.Exceline.GMS.Modules.ManageMembership.RestApi.Models;
using US.GMS.API;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.Payment.Core.Enums;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Controllers
{
    [RoutePrefix("api/Member/Shop")]
    public class ShopController : ApiController
    {
        [HttpGet]
        [Route("GetMemberPurchaseHistory")]
        [Authorize]
        public HttpResponseMessage GetMemberPurchaseHistory(int memberId, DateTime fromDate, DateTime toDate, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                                                                                    //GetMemberPerchaseHistory( memberId,  fromDate,  toDate,  gymCode,  branchId,  user)
                OperationResult<ShopSalesDC> result = GMSManageMembership.GetMemberPerchaseHistory(memberId, fromDate, toDate, ExceConnectionManager.GetGymCode(user), branchId, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ContractSummaryDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ContractSummaryDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

    }
}
