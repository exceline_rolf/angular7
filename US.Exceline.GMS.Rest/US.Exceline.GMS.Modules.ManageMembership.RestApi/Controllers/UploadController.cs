﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace FileUpload_WebAPI_Angular2.Controllers
{
    [RoutePrefix("api/Member")]
    public class UploadFileApiController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage UploadJsonFile()
        {
            try
            {
                HttpResponseMessage response = new HttpResponseMessage();
                var httpRequest = HttpContext.Current.Request;

                if (httpRequest.Files.Count > 0)
                {
                    var filePath = "";
                    foreach (string file in httpRequest.Files)
                    {
                        string rootpath = ConfigurationSettings.AppSettings["UploadScanDocRoot"];
                        string BranchId = httpRequest.Headers["BranchId"].ToString();
                        string CustId = httpRequest.Headers["CustId"].ToString();
                        string UserName = httpRequest.Headers["UserName"].ToString();
                        string gymCode = ExceConnectionManager.GetGymCode(UserName).ToString();
                        string fileFolder = rootpath + @"\" + gymCode + @"\" + BranchId + @"\" + CustId + @"\";
                        if (!Directory.Exists(fileFolder))
                        {
                            Directory.CreateDirectory(fileFolder);
                        }
                        var postedFile = httpRequest.Files[file];
                        filePath = fileFolder + postedFile.FileName;
                        postedFile.SaveAs(filePath);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(filePath, ApiResponseStatus.OK));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }


        }
    }
}