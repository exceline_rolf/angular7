﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.Utills
{
    public static class InvoiceTypeConverter
    {
        public static string ConvertInvoiceTypeToItemTypeID(InvoiceTypes invoiceType)
        {

            switch (invoiceType)
            {
                case InvoiceTypes.DirectDeduct:
                    return "1";
                case InvoiceTypes.InvoiceForPrint:
                    return "2";
                case InvoiceTypes.DebtWarning:
                    return "4";
                case InvoiceTypes.Sponser:
                    return "7";
                case InvoiceTypes.Invoice:
                    return "3";
                case InvoiceTypes.PaymentMemo:
                    return "10";
                case InvoiceTypes.DB:
                    return "11";
                case InvoiceTypes.SM:
                    return "12";
                case InvoiceTypes.PaymentDocumentForPrint:
                    return "14";
                case InvoiceTypes.ClassInvoice:
                    return "100";
                case InvoiceTypes.ShopInvoice:
                    return "101";
                case InvoiceTypes.DRPInvoice:
                    return "102";
                default:
                    throw new Exception("Invoice type [" + invoiceType.ToString() + "] not supported.", null);
            }
        }
    }
}
