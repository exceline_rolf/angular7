﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.Utills
{
    public static class FileManager
    {
        private const string SUCCEEDED_FOLDER = "Succeeded";
        private const string FAILED_FOLDER = "Failed";

        public static List<string> GetFileLines(string fileName)
        {

            List<string> lines = new List<string>();

            FileStream fileStream = null;
            try
            {
                using (fileStream = new FileStream(fileName, FileMode.Open
                        , FileAccess.Read))
                {
                    StreamReader ss = null;
                    using (ss = new StreamReader(fileStream, Encoding.UTF7))
                    {
                        string line = ss.ReadLine();
                        while (!string.IsNullOrEmpty(line))
                        {
                            lines.Add(line);
                            line = ss.ReadLine();
                        }
                    }
                    if (ss != null)
                    {
                        ss.Close();
                    }

                }
                if (fileStream != null)
                {
                    fileStream.Close();
                }

                return lines;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to read file : " + ex.Message);
            }

            #region Commented on 2010.03.08. For fixing bug of disapperaing of moved files
            //List<string> lines = new List<string>();

            //using (TextReader txReader = new StreamReader(new FileStream(fileName, FileMode.Open
            //   , FileAccess.Read), Encoding.UTF7))
            //{
            //    string line = txReader.ReadLine();
            //    while (!string.IsNullOrEmpty(line))
            //    {
            //        lines.Add(line);
            //        line = txReader.ReadLine();
            //    }
            //}
            //return lines;
            #endregion
        }

        public static string ReadEntireFile(string fileName)
        {
            string fileContents = string.Empty;
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException(fileName + " could not be found.");
            }
            try
            {
                //create a new TextReader and open our file
                using (StreamReader sr = new StreamReader(new FileStream(fileName, FileMode.Open)))
                {
                    fileContents = sr.ReadToEnd();
                }
                return fileContents;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to read file " + fileName + " : " + ex.Message);
            }

        }
        public static void WriteToLogFile(string fileName, string message)
        {
            try
            {
                if (!string.IsNullOrEmpty(fileName))
                {
                    WriteToFile(fileName, message);
                }
            }
            catch (Exception)
            {
                // do nothing
            }
        }
        private static void WriteToFile(string fileName, string message)
        {
            using (StreamWriter sw = new StreamWriter
                (new FileStream(fileName, FileMode.Append, FileAccess.Write)))
            {
                sw.WriteLine(message);
            }
        }

        public static void MoveSucceededFile(string sourceFile)
        {
            string destination = Path.GetDirectoryName(sourceFile) + "\\" + SUCCEEDED_FOLDER + "\\" + DateTime.Today.ToString("yyyyMMdd");

            try
            {
                if (!Directory.Exists(destination))
                    Directory.CreateDirectory(destination);
                destination += "\\" + Path.GetFileName(sourceFile);

                #region Removed to fix bug (Bug No.) 62
                //if (File.Exists(destination))
                //    File.Delete(destination);
                //File.Move(sourceFile, destination);
                #endregion

                File.Copy(sourceFile, destination, true);
                if (File.Exists(destination))
                {
                    if (File.Exists(sourceFile))
                    {
                        File.Delete(sourceFile);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to move failed file. " + ex.Message);
            }
        }
        public static void MoveFailedFile(string sourceFile)
        {
            string destination = Path.GetDirectoryName(sourceFile) + "\\" + FAILED_FOLDER + "\\" + DateTime.Today.ToString("yyyyMMdd");

            try
            {
                if (!Directory.Exists(destination))
                    Directory.CreateDirectory(destination);
                destination += "\\" + Path.GetFileName(sourceFile);
                #region Removed to fix bug (Bug No.) 62
                //if (File.Exists(destination))
                //    File.Delete(destination);
                //File.Move(sourceFile, destination);
                #endregion

                File.Copy(sourceFile, destination, true);
                if (File.Exists(destination))
                {
                    if (File.Exists(sourceFile))
                    {
                        File.Delete(sourceFile);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to move failed file. " + ex.Message);
            }
        }
    }
}
