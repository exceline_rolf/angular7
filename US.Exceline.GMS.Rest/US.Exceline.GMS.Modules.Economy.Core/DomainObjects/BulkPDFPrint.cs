﻿using System;
using System.Runtime.Serialization;

namespace US.Exceline.GMS.Modules.Economy.Core.DomainObjects
{
    [DataContract]
    public class BulkPDFPrint
    {
        private int _id = -1;
        private string _docType = string.Empty;
        private string _pdfPath = string.Empty;
        private int _noOfDoc = -1;
        private int _failCount = -1;
        private string _createdUser = string.Empty;
        private DateTime _createdDateTime = DateTime.Now;
        private int _branchId = -1;

        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember]
        public string DocType
        {
            get { return _docType; }
            set { _docType = value; }
        }

        [DataMember]
        public string PDFPath
        {
            get { return _pdfPath; }
            set { _pdfPath = value; }
        }

        [DataMember]
        public int NoOfDoc
        {
            get { return _noOfDoc; }
            set { _noOfDoc = value; }
        }

        [DataMember]
        public int FailCount
        {
            get { return _failCount; }
            set { _failCount = value; }
        }

        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        [DataMember]
        public DateTime CreatedDateTime
        {
            get { return _createdDateTime; }
            set { _createdDateTime = value; }
        }

        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { value = _branchId; }
        }
    }
}
