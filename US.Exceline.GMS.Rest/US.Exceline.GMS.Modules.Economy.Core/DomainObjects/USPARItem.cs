﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.DomainObjects
{
    [DataContract]
    public class USPARItem
    {
        private int _arNo = -1;
        [DataMember]
        public int ArNo
        {
            get { return _arNo; }
            set { _arNo = value; }
        }


        private string _invocieNumber = string.Empty;
        [DataMember]
        public string InvoiceNumber
        {
            get { return _invocieNumber; }
            set { _invocieNumber = value; }
        }

        private DateTime _invoiceDate = DateTime.Now;
        [DataMember]
        public DateTime InvoicedDate
        {
            get { return _invoiceDate; }
            set { _invoiceDate = value; }
        }

        private string _strDueDate = string.Empty;
        [DataMember]
        public string StrDueDate
        {
            get { return _strDueDate; }
            set { _strDueDate = value; }
        }

        private DateTime _dudeDate = DateTime.Now;
        [DataMember]
        public DateTime DueDate
        {
            get { return _dudeDate; }
            set { _dudeDate = value; }
        }


        private string _strPaidDate = string.Empty;
        [DataMember]
        public string StrPaidDate
        {
            get { return _strPaidDate; }
            set { _strPaidDate = value; }
        }
        private DateTime _paidDate = DateTime.MinValue;
        [DataMember]
        public DateTime PaidDate
        {
            get { return _paidDate; }
            set { _paidDate = value; }
        }

        private decimal _amount = 0;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }


        private DateTime _regDate;
        [DataMember]
        public DateTime RegDate
        {
            get { return _regDate; }
            set { _regDate = value; }
        }

        private string _inkassoID = string.Empty;
        [DataMember]
        public string InkassoID
        {
            get { return _inkassoID; }
            set { _inkassoID = value; }
        }

        private string _creditorName = string.Empty;
        [DataMember]
        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }

        }

        private string _cusID = string.Empty;
        [DataMember]
        public string cusID
        {
            get { return _cusID; }
            set { _cusID = value; }
        }

        private string _debtorName = string.Empty;
        [DataMember]
        public string DebtorName
        {
            get { return _debtorName; }
            set { _debtorName = value; }
        }

        private string _arItemNo = string.Empty;
        [DataMember]
        public string ARItemNo
        {
            get { return _arItemNo; }
            set { _arItemNo = value; }
        }

        private string _creditor = string.Empty;
        [DataMember]
        public string Creditor
        {
            get { return _creditor; }
            set { _creditor = value; }
        }

        private string _debtor = string.Empty;
        [DataMember]
        public string Debtor
        {
            get { return _debtor; }
            set { _debtor = value; }
        }

        private string _kid = string.Empty;
        [DataMember]
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }

        private bool _isSelected = false;
        [DataMember]
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private string _hit = string.Empty;
        [DataMember]
        public string Hit
        {
            get { return _hit; }
            set { _hit = value; }
        }

        private string _creditorAccountNumber = string.Empty;
        [DataMember]
        public string CreditorAccountNumber
        {
            get { return _creditorAccountNumber; }
            set { _creditorAccountNumber = value; }
        }


        private bool _apportionmentStatus = false;
        [DataMember]
        public bool ApportionmentStatus
        {
            get { return _apportionmentStatus; }
            set { _apportionmentStatus = value; }
        }

        //Fileds Required for Styling the grid and button
        private string _btnContent = string.Empty;
        [DataMember]
        public string BtnContect
        {
            get { return _btnContent; }
            set { _btnContent = value; }
        }

        private string _btnStyleName = string.Empty;
        [DataMember]
        public string BtnStyleName
        {
            get { return _btnStyleName; }
            set { _btnStyleName = value; }
        }

        private string _rowStyleName = string.Empty;
        [DataMember]
        public string RowStyleName
        {
            get { return _rowStyleName; }
            set { _rowStyleName = value; }
        }

        private string _itemType = string.Empty;
        [DataMember]
        public string ItemType
        {
            get { return _itemType; }
            set { _itemType = value; }
        }

        private int _itemTypeId = 0;
        [DataMember]
        public int ItemTypeId
        {
            get { return _itemTypeId; }
            set { _itemTypeId = value; }
        }


        private string _transType = string.Empty;
        [DataMember]
        public string TransType
        {
            get { return _transType; }
            set { _transType = value; }
        }

        private string _transDescription = string.Empty;
        [DataMember]
        public string TransDescription
        {
            get { return _transDescription; }
            set { _transDescription = value; }
        }

        private string _text = string.Empty;
        [DataMember]
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private string _rFee = string.Empty;
        [DataMember]
        public string RFee
        {
            get { return _rFee; }
            set { _rFee = value; }
        }

        private int _delayed = 0;
        [DataMember]
        public int Delayed
        {
            get { return _delayed; }
            set { _delayed = value; }
        }

        private string _paid = string.Empty;
        [DataMember]
        public string Paid
        {
            get { return _paid; }
            set { _paid = value; }
        }

        private int _caseNo = 0;
        [DataMember]
        public int CaseNo
        {
            get { return _caseNo; }
            set { _caseNo = value; }
        }

        private int _subCaseNo = 0;
        [DataMember]
        public int SubCaseNo
        {
            get { return _subCaseNo; }
            set { _subCaseNo = value; }
        }

        private string _invoicePath = string.Empty;
        [DataMember]
        public string InvoicePath
        {
            get { return _invoicePath; }
            set { _invoicePath = value; }
        }

        private bool _isPrinted = false;
        [DataMember]
        public bool IsPrinted
        {
            get { return _isPrinted; }
            set { _isPrinted = value; }
        }

        private string _transferDate = string.Empty;
        [DataMember]
        public string TransferDate
        {
            get { return _transferDate; }
            set { _transferDate = value; }
        }

        private bool _isValidCancel = false;
        [DataMember]
        public bool IsValidCancel
        {
            get { return _isValidCancel; }
            set { _isValidCancel = value; }
        }

        private decimal _balance = 0;
        [DataMember]
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        private bool _isCheckBoxEnabled = true;
        [DataMember]
        public bool IsCheckBoxEnabled
        {
            get { return _isCheckBoxEnabled; }
            set { _isCheckBoxEnabled = value; }
        }

        private string _fileName = string.Empty;
        [DataMember]
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        private int _paymentID = -1;
        [DataMember]
        public int PaymentID
        {
            get { return _paymentID; }
            set { _paymentID = value; }
        }

        private int _creditorGroupId = -1;
        [DataMember]
        public int CreditorGroupId
        {
            get { return _creditorGroupId; }
            set { _creditorGroupId = value; }
        }

        private string _creditorGroupName = string.Empty;
        [DataMember]
        public string CreditorGroupName
        {
            get { return _creditorGroupName; }
            set { _creditorGroupName = value; }
        }

        private int _subCaseState = 0;
        [DataMember]
        public int SubCaseState
        {
            get { return _subCaseState; }
            set { _subCaseState = value; }
        }
        private int _arState = -1;
        [DataMember]
        public int ArState
        {
            get { return _arState; }
            set { _arState = value; }
        }
    }
}
