﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.DomainObjects
{
    public class USPNewOrCancelledFileRow
    {

        public string BatchId { get; set; }
        public string BNumber { get; set; }
        public string CID { get; set; }
        public string DataSource { get; set; }
        public int ID { get; set; }
        public string KID { get; set; }
        public string PID { get; set; }
        public string RecordType { get; set; }
        public int RefereceId { get; set; }
        public int Status { get; set; }
        public string StatusDate { get; set; }
        public string AccountNo { get; set; }
    }
}
