﻿using System;

namespace US.Exceline.GMS.Modules.Economy.Core.DomainObjects
{
    public class EconomyLogFileHelper
    {
        private DateTime _fromDate;
        public DateTime FromDate
        {
            get { return _fromDate; }
            set { _fromDate = value; }
        }

        private DateTime _toDate;
        public DateTime ToDate
        {
            get { return _toDate; }
            set { _toDate = value; }
        }

        private string _source;
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        private string _filterBy;
        public string FilterBy
        {
            get { return _filterBy; }
            set { _filterBy = value; }
        }

        private string _uniqueId;
        public string UniqueId
        {
            get { return _uniqueId; }
            set { _uniqueId = value; }
        }

        private string _paymentType;
        public string PaymentType
        {
            get { return _paymentType; }
            set { _paymentType = value; }
        }

        private string _gymCode;
        public string GymCode
        {
            get { return _gymCode; }
            set { _gymCode = value; }
        }

        private string _user;
        public string User
        {
            get { return _user; }
            set
            {
                _user = value;
            }
        }
    }
}
