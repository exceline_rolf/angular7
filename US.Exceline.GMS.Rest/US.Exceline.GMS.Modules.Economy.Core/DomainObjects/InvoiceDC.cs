﻿using System;
using System.Runtime.Serialization;

namespace US.Exceline.GMS.Modules.Economy.Core.DomainObjects
{
    [DataContract]
    public class InvoiceDC
    {
        private int _arItemNo = -1;
        private string _custNo = string.Empty;
        private string _custName = string.Empty;
        private string _ref = string.Empty;
        private string _kID = string.Empty;
        private DateTime _invocieDate;
        private DateTime _dueDate;
        private decimal _invoiceAmount = 0;
        private decimal _balance = 0;
        private string _contractNo = string.Empty;
        private string _gymName = string.Empty;
        private string _invoiceCategory = string.Empty;
        private bool _isCheck = false;

        [DataMember]
        public int ArItemNo
        {
            get { return _arItemNo; }
            set { _arItemNo = value; }
        }

        [DataMember]
        public string CustNo
        {
            get { return _custNo; }
            set { _custNo = value; }
        }

        [DataMember]
        public string CustName
        {
            get { return _custName; }
            set { _custName = value; }
        }

        [DataMember]
        public string Ref
        {
            get { return _ref; }
            set { _ref = value; }
        }

        [DataMember]
        public string KId
        {
            get { return _kID; }
            set { _kID = value; }
        }

        [DataMember]
        public DateTime InvocieDate
        {
            get { return _invocieDate; }
            set { _invocieDate = value; }
        }

        [DataMember]
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        [DataMember]
        public decimal InvoiceAmount
        {
            get { return _invoiceAmount; }
            set { _invoiceAmount = value; }
        }

        [DataMember]
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        [DataMember]
        public string ContractNo
        {
            get { return _contractNo; }
            set { _contractNo = value; }
        }

        [DataMember]
        public string GymName
        {
            get { return _gymName; }
            set { _gymName = value; }
        }

        [DataMember]
        public string InvoiceCategory
        {
            get { return _invoiceCategory; }
            set { _invoiceCategory = value; }
        }

        [DataMember]
        public bool IsCheck
        {
            get { return _isCheck; }
            set { _isCheck = value; }
        }
    }
}
