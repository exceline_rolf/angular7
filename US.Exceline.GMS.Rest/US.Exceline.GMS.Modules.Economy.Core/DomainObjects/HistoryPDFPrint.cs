﻿using System;
using System.Runtime.Serialization;

namespace US.Exceline.GMS.Modules.Economy.Core.DomainObjects
{
    [DataContract]
    public class HistoryPDFPrint
    {
        private int _id = -1;
        private DateTime _startDateTime = DateTime.Now;
        private DateTime _endDateTime = DateTime.Now;
        private string _createdUser = string.Empty;
        private int _noOfInvoice = -1;
        private int _failCount = -1;
        private string _filePath = string.Empty;
        private bool _isPDFPrinted;

        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember]
        public DateTime StartDateTime
        {
            get { return _startDateTime; }
            set { _startDateTime = value; }
        }

        [DataMember]
        public DateTime EndDateTime
        {
            get { return _endDateTime; }
            set { _endDateTime = value; }
        }

        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        [DataMember]
        public int NoOfInvoice
        {
            get { return _noOfInvoice; }
            set { _noOfInvoice = value; }
        }

        [DataMember]
        public int FailCount
        {
            get { return _failCount; }
            set { _failCount = value; }
        }

        [DataMember]
        public string FilePath
        {
            get { return _filePath; }
            set { _filePath = value; }
        }

        [DataMember]
        public bool IsPDFPrinted
        {
            get { return _isPDFPrinted; }
            set { _isPDFPrinted = value; }
        }
    }
}
