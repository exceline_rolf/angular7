﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.DomainObjects
{
    public class USPNewOrCancelledFile
    {
        public USPNewOrCancelledFile() { }
        public List<USPNewOrCancelledFileRow> FileRowList { get; set; }
    }
}
