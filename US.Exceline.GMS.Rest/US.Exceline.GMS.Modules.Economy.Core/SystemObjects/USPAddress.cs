﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class USPAddress : IUSPAddress
    {
        #region IUSPAddress Members


        private int _addrNo = -1;
        public int addrNo
        {
            get
            {
                return _addrNo;
            }
            set
            {
                _addrNo = value;
            }
        }


        private string _Address1 = string.Empty;
        public string Address1
        {
            get
            {
                return _Address1;
            }
            set
            {
                _Address1 = value;
            }
        }


        private string _Address2 = string.Empty;
        public string Address2
        {
            get
            {
                return _Address2;
            }
            set
            {
                _Address2 = value;
            }
        }


        private string _Address3 = string.Empty;
        public string Address3
        {
            get
            {
                return _Address3;
            }
            set
            {
                _Address3 = value;
            }
        }


        private string _ZipCode = string.Empty;
        public string ZipCode
        {
            get
            {
                return _ZipCode;
            }
            set
            {
                _ZipCode = value;
            }
        }

        private string _CountryCode = string.Empty;
        public string CountryCode
        {
            get
            {
                return _CountryCode;
            }
            set
            {
                _CountryCode = value;
            }
        }

        private string _AddSource = string.Empty;
        public string AddSource
        {
            get
            {
                return _AddSource;
            }
            set
            {
                _AddSource = value;
            }
        }


        private string _City = string.Empty;
        public string City
        {
            get
            {
                return _City;
            }
            set
            {
                _City = value;
            }
        }


        private string _TelMobile = string.Empty;
        public string TelMobile
        {
            get
            {
                return _TelMobile;
            }
            set
            {
                _TelMobile = value;
            }
        }


        private string _TelWork = string.Empty;
        public string TelWork
        {
            get
            {
                return _TelWork;
            }
            set
            {
                _TelWork = value;
            }
        }


        private string _TelHome = string.Empty;
        public string TelHome
        {
            get
            {
                return _TelHome;
            }
            set
            {
                _TelHome = value;
            }
        }


        private string _Email = string.Empty;
        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                _Email = value;
            }
        }


        private string _Fax = string.Empty;
        public string Fax
        {
            get
            {
                return _Fax;
            }
            set
            {
                _Fax = value;
            }
        }


        private string _MSN = string.Empty;
        public string MSN
        {
            get
            {
                return _MSN;
            }
            set
            {
                _MSN = value;
            }
        }


        private string _Skype = string.Empty;
        public string Skype
        {
            get
            {
                return _Skype;
            }
            set
            {
                _Skype = value;
            }
        }


        private string _SMS = string.Empty;
        public string SMS
        {
            get
            {
                return _SMS;
            }
            set
            {
                _SMS = value;
            }
        }


        private bool _IsDefault = new bool();
        public bool IsDefault
        {
            get
            {
                return _IsDefault;
            }
            set
            {
                _IsDefault = value;
            }
        }


        private string _AddressName = string.Empty;
        public string AddressName
        {
            get
            {
                return _AddressName;
            }
            set
            {
                _AddressName = value;
            }
        }


        private string _Branch = string.Empty;
        public string Branch
        {
            get
            {
                return _Branch;
            }
            set
            {
                _Branch = value;
            }
        }


        private string _PostCode = string.Empty;
        public string PostCode
        {
            get
            {
                return _PostCode;
            }
            set
            {
                _PostCode = value;
            }
        }


        private string _Municipality = string.Empty;
        public string Municipality
        {
            get
            {
                return _Municipality;
            }
            set
            {
                _Municipality = value;
            }
        }

        #endregion
    }
}
