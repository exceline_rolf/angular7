﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class AR
    {
        /// <summary>
        /// Unique number idendifies the account revcievable.
        /// This field is auto generated in database logics(SPs).
        /// </summary>
        public int ARNo { get; set; }
        /// <summary>
        /// ID of relevent Customer
        /// </summary>
        public string CID { get; set; }
        /// <summary>
        /// Name of the account recievable
        /// </summary>
        private string _name = string.Empty;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _subCaseNo = string.Empty;

        public string SubCaseno
        {
            get { return _subCaseNo; }
            set { _subCaseNo = value; }
        }

        private string _transferDate = string.Empty;
        public string TransferDate
        {
            get { return _transferDate; }
            set { _transferDate = value; }
        }

    }
}
