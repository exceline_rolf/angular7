﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class ExlineClaimValidationRecord
    {
        public bool HasCriticalError { get; set; }

        public string CreditorNo { get; set; }

        public string CriticalErrorMessage { get; set; }
        /// <summary>
        /// Constructor of ExlineClaimValidationRecord
        /// </summary>
        /// <param name="lineIndex">if the record is not related vith specific line index,pass -1</param>
        public ExlineClaimValidationRecord(int lineIndex)
        {
            LineIndex = lineIndex;
            IsValidRecord = true;
            HasCriticalError = false;
        }

        public decimal Amount { get; set; }

        public int LineIndex { get; set; }

        public ValidationRecordTypes ValidationRecordType { get; set; }

        public bool IsValidRecord { get; set; }

        public string ErrorMessage { get; set; }

    }
}
