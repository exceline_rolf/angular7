﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums
{
    public enum ReverseConditions
    {
        ERRROR,
        SUCCESS,
        APPREMITTED,
        TRANSFERED,
        NOAPPORTIONMENTS
    }
}
