﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public interface IUSPClaim : ITagEnabled
    {
        string NameInContract { get; set; }  // Exline File field ID - 6
        string AccountNumber { get; set; }  // Exline File field ID - 15 
        string ContractNumber { get; set; } // Exline File field ID - 16
        string ContractExpire { get; set; }  // Exline File field ID - 17
        string LastVisit { get; set; }  // Exline File field ID - 18
        string LastContract { get; set; }  // Exline File field ID - 19
        string ContractKID { get; set; }// Exline File field ID - 20
        DirectDeductStatus Status { get; set; }// Exline File field ID - 21
        string InstallmentNumber { get; set; }// Exline File field ID - 22
        string InvoiceNumber { get; set; }// Exline File field ID - 23
        string Text { get; set; }// Exline File field ID - 24
        string InvoicedDate { get; set; }  // Exline File field ID - 25
        string DueDate { get; set; }  // Exline File field ID - 26
        string PaidDate { get; set; }  // Exline File field ID - 27
        string PrintDate { get; set; }  // Exline File field ID - 28
        string OriginalDueDate { get; set; }  // Exline File field ID - 29
        decimal Amount { get; set; }  // Exline File field ID - 30
        decimal InvoiceCharge/*InvoiceAmount*/ { get; set; }  // Exline File field ID - 31
        decimal ReminderFee { get; set; }  // Exline File field ID - 32
        string KID { get; set; } // Exline File field ID - 33
        string TransmissionNumberBBS { get; set; }// Exline File field ID - 34
        string Balance { get; set; }// Exline File field ID - 35
        string DueBalance { get; set; }// Exline File field ID - 36
        string LastReminder { get; set; }// Exline File field ID - 37
        CollectingStatus CollectingStatus { get; set; }// Exline File field ID - 38
        string InkassoTest { get; set; }// Exline File field ID - 39
        string BranchNumber { get; set; }// Exline File field ID - 40
        int IsPrinted { get; set; }
        string InvoicePath { get; set; }
        string InvoicePrintDate { get; set; }
        IUSPDebtor Debtor { get; set; }
        IUSPCreditor Creditor { get; set; }
        List<IUSPOrderLine> OrderLines { get; set; }
        InvoiceTypes InvoiceType { get; set; }
        int InvoiceTypeId { get; set; }
        int ARItemNo { get; set; }
        string OrganizationNo { get; set; }
        string InstallmentKID { get; set; }
        string InvoiceRef { get; set; }
        string Group { get; set; }
        List<IUSPCustomSetting> CustomSettings { get; set; }
        DateTime ManulaMapDueDate { get; set; }  // Exline File field ID - 26
        //string InvoiceTotalControlDigit { get; set; }
        //string CaseBalanceControlDigit { get; set; }
        DateTime TransferDate { get; set; }
    }
}
