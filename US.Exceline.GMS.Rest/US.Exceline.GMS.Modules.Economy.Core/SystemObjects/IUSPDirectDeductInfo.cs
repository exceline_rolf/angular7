﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public interface IUSPDirectDeductInfo
    {
        string FileName { get; set; }
        string TotalAmount { get; set; }
        string FirstDueDate { get; set; }
        string LastDueDate { get; set; }
        List<IUSPStandingOrderItem> Ddobj { get; set; }
    }
}
