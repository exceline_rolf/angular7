﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Ccx
{
    public class ServiceResponse
    {
        public int Status { get; set; } //1 for success 0 for fail
        public string ErrorMessage { get; set; }
        public object Data { get; set; } //any data that should return from the service method
    }
}
