﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Ccx
{
    public class Invoice
    {
        public int Cid { get; set; }
        public int ReferenceId { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public string CustId { get; set; }
        public string InvoiceKid { get; set; }
        public DateTime? NewDueDate { get; set; }
        public string GuId { get; set; }
        public string BatchNumber { get; set; }
    }
}
