﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class OCRTransactionRecord : IUSPOCRTransactionRecord
    {
        public int LineIndexOfFirstPart
        {
            get;
            set;
        }

        public int LineIndexOfSecondPart
        {
            get;
            set;
        }
    }
}
