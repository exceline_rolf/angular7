﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class Debitor
    {
        /// <summary>
        /// Unique id identifies the Debitor in the system. Stored in the Ent Table. 
        /// This is selfsame to KUNDENR field in the file
        /// </summary>
        /// 

        public Debitor()
        {
            LastName = string.Empty;
            FirstName = string.Empty;
            ARNo = string.Empty;
            ZIPCode = string.Empty;
            ZIPCode = string.Empty;
            PostPlace = string.Empty;
            Born = string.Empty;
            PersonNo = string.Empty;
            TlfMobile = string.Empty;
            TlfPrivate = string.Empty;
            TlfWork = string.Empty;

        }

        public int EntID { get; set; }

        public int EntRoleID { get; set; }

        /// <summary>
        /// NAVN1
        /// Last name or company name   
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// NAVN2
        /// First Name 
        /// </summary>
        public string FirstName { get; set; }
        public string ARNo { get; set; }

        /// <summary>
        /// ADRESSE
        /// Address of the debitor
        /// </summary>
        public int AddrNo { get; set; }

        /// <summary>
        /// POSTNR
        /// Postle code or the ZIP code
        /// </summary>
        public string ZIPCode { get; set; }

        /// <summary>
        /// POSTSTED
        /// Post place 
        /// </summary>
        public string PostPlace { get; set; }

        /// <summary>
        /// FODT
        /// Birth day
        /// </summary>
        public string Born { get; set; }

        /// <summary>
        /// TLF PRIVAT
        /// Tlf Private
        /// </summary>
        public string TlfPrivate { get; set; }

        /// <summary>
        /// TLF ARBEID
        /// Tlf Work
        /// </summary>
        public string TlfWork { get; set; }

        /// <summary>
        /// TLF MOBIL
        /// Tlf mobile
        /// </summary>
        public string TlfMobile { get; set; }

        /// <summary>
        /// E-Post
        /// E-mail
        /// </summary>
        public string EMail { get; set; }

        /// <summary>
        /// PersonNo
        /// PersonNo
        /// </summary>
        public string PersonNo { get; set; }

        private string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { title = value; }

        }
        private string CID = string.Empty;

        public string cid
        {
            get { return CID; }
            set { CID = value; }
        }
        private string contactMobile = string.Empty;

        public string ContactMobile
        {
            get { return contactMobile; }
            set { contactMobile = value; }
        }

        private string contactWork = string.Empty;

        public string ContactWork
        {
            get { return contactWork; }
            set { contactWork = value; }
        }

        private int noOfCreditors;

        public int NoOfCreditors
        {
            get { return noOfCreditors; }
            set { noOfCreditors = value; }
        }

        private string fax = string.Empty;

        public string Fax
        {
            get { return fax; }
            set { fax = value; }
        }

        private string msn = string.Empty;

        public string MSN
        {
            get { return msn; }
            set { msn = value; }
        }

        private string skype = string.Empty;

        public string Skype
        {
            get { return skype; }
            set { skype = value; }
        }

        private string sms = string.Empty;

        public string SMS
        {
            get { return sms; }
            set { sms = value; }
        }

        private string incaso = string.Empty;

        public string IncassoId
        {
            get { return incaso; }
            set { incaso = value; }
        }

        private string roleId = string.Empty;

        public string RoleId
        {
            get { return roleId; }
            set { roleId = value; }
        }

        private string _ref = string.Empty;

        public string Reference
        {
            get { return _ref; }
            set { _ref = value; }
        }

        private string _address1 = string.Empty;

        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }
        private string _address2 = string.Empty;

        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }
        private string _address3 = string.Empty;

        public string Address3
        {
            get { return _address3; }
            set { _address3 = value; }
        }



        private string _userName = string.Empty;

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        private bool _IsAddressKnown;
        public bool IsAddressKnown
        {
            get { return _IsAddressKnown; }
            set { _IsAddressKnown = value; }
        }





    }
}
