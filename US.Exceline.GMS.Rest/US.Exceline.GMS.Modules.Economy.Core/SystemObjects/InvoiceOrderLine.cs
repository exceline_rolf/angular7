﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    [DataContract]
    public class InvoiceOrderLine
    {
        private String _articleDescription = String.Empty;
        public String ArticleDescription
        {
            get { return _articleDescription; }
            set { _articleDescription = value; }
        }

        private String _periodSpan = String.Empty;
        public String PeriodSpan
        {
            get { return _periodSpan; }
            set { _periodSpan = value; }
        }

        private String _numberOfUnits = String.Empty;
        public String NumberOfUnits
        {
            get { return Convert.ToDecimal(_numberOfUnits.Replace('.', ',')).ToString("F0"); }
            set { _numberOfUnits = value; }
        }

        private String _unitPrice = String.Empty;
        public String UnitPrice
        {
            get { return (Math.Round(Convert.ToDecimal(_unitPrice.Replace('.', ',')), 2)).ToString("F"); }
            set { _unitPrice = value; }
        }

        private Decimal _VATRate = 0;
        public Decimal VATRate
        {
            get { return _VATRate; }
            set { _VATRate = value; }
        }

        private String _VATAddition = String.Empty;
        public String VATAddition
        {
            get { return _VATAddition = (Math.Round( (Convert.ToDecimal(UnitPrice.Replace('.', ',')) - Convert.ToDecimal(UnitPrice.Replace('.', ','))/((100 + VATRate)/100)), 2)).ToString(); }
        }

        private String _unitPriceNoVAT = String.Empty;
        public String UnitPriceNoVAT
        {
            get { return (Convert.ToDecimal(UnitPrice.Replace('.', ',')) - Convert.ToDecimal(VATAddition.Replace('.', ','))).ToString(); }
        }

        private String _totalPrice;
        public String TotalPrice
        {
            get
            {
                return _totalPrice =(Math.Round(Convert.ToDecimal(NumberOfUnits.Replace('.', ',')) * (Convert.ToDecimal(UnitPrice.Replace('.', ','))),2)).ToString("F");
            }
        }

        private String _totalPriceNoVAT;
        public String TotalPriceNoVAT
        {
            get
            {
                return _totalPriceNoVAT = Convert.ToString(Math.Round(Convert.ToDecimal(NumberOfUnits.Replace('.', ',')) * (Convert.ToDecimal(UnitPriceNoVAT.Replace('.', ',')))));
            }
        }

        private String _note = String.Empty;
        public String Note
        {
            get { return _note; }
            set { _note = value; }
        }
    }
}
