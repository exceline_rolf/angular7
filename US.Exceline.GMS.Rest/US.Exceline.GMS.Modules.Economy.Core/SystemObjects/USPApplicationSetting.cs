﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class USPApplicationSetting : IUSPApplicationSetting
    {
        private string _Name = string.Empty;
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }

        private string _TrancemeterID = string.Empty;
        public string TrancemeterID
        {
            get
            {
                return _TrancemeterID;
            }
            set
            {
                _TrancemeterID = value;
            }
        }
    }
}
