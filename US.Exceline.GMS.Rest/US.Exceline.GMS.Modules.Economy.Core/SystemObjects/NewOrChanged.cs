﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class NewOrChanged : IUSPNewOrChanged
    {
        #region IUSPNewOrChanged Members

        public string FBOCounter
        {
            get;
            set;

        }

        public string RegisterType
        {
            get;
            set;

        }

        public string KID
        {
            get;
            set;

        }

        public string Warning
        {
            get;
            set;

        }

        #endregion

        #region ITagEnabled Members

        public string Tag1
        {
            get;
            set;
        }

        public string Tag2
        {
            get;
            set;
        }

        public string Tag3
        {
            get;
            set;
        }

        #endregion
    }
}
