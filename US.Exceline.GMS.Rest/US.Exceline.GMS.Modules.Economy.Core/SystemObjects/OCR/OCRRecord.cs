﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class OCRRecord : IUSPOCRRecord
    {

        public IUSPOCRStartTransmissionRecord StartSendingReocrd
        {
            get;
            set;
        }

        public List<IUSPOCRAssignmentRecord> AssignmentRecords
        {
            get;
            set;
        }

        public IUSPOCREndTransmissionRecord EndSendingRecord
        {
            get;
            set;
        }

    }
}
