﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class ValidationMessage : IMessage
    {
        public string ID
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }
    }
}
