﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class Payment : IUSPPayment
    {
        public string Txt
        {
            get;
            set;
        }

        public int ItemType
        {
            get;
            set;
        }

        public string Amount
        {
            get;
            set;
        }

        public string VouDate
        {
            get;
            set;
        }

        public string DueDate
        {
            get;
            set;
        }

        public string DStatus
        {
            get;
            set;
        }

        public int ARNo
        {
            get;
            set;
        }

        public string Ref
        {
            get;
            set;
        }

        public string KID
        {
            get;
            set;
        }

        public string CustId
        {
            get;
            set;
        }

        public int ARItemNo
        {
            get;
            set;
        }

        public string CreditorId
        {
            get;
            set;
        }

        public string ExtTransNo
        {
            get;
            set;
        }

        public string ExtCaseNo
        {
            get;
            set;
        }

        public string Reference2
        {
            get;
            set;
        }


        public string DebtorAccountNumber
        {
            get;
            set;
        }

        public string Tag1
        {
            get;
            set;
        }

        public string Tag2
        {
            get;
            set;
        }

        public string Tag3
        {
            get;
            set;
        }

        public int ErrorCode
        {
            get;
            set;
        }

        public string ErrorDescription
        {
            get;
            set;
        }

        public string FileName
        {
            get;
            set;
        }

        public int FileId
        {
            get;
            set;
        }

        public string Source
        {
            get;
            set;
        }

        public int VoucherId
        {
            get;
            set;
        }

        public int VoucherDetailId
        {
            get;
            set;
        }

        public string CreditorAccountNo
        {
            get;
            set;
        }
    }
}
