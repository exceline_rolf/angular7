﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public interface IUSPPayment : ITagEnabled
    {
        string Txt { get; set; }
        int ItemType { get; set; }
        string Amount { get; set; }
        string VouDate { get; set; }
        string DueDate { get; set; }
        string DStatus { get; set; }
        int ARNo { get; set; }
        string Ref { get; set; }
        string KID { get; set; }
        string CustId { get; set; }
        int ARItemNo { get; set; }
        string CreditorId { get; set; }
        string ExtTransNo { get; set; }
        string ExtCaseNo { get; set; }
        string Reference2 { get; set; }
        string DebtorAccountNumber { get; set; }
        int ErrorCode { get; set; }
        string ErrorDescription { get; set; }
        string FileName { get; set; }
        int FileId { get; set; }
        string Source { get; set; }
        int VoucherId { get; set; }
        int VoucherDetailId { get; set; }
        string CreditorAccountNo { get; set; }
    }
}
