﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public interface IUSPOCREndAssignmentRecord
    {
        string FormatCode
        {
            get;
            set;
        }

        string ServiceCode
        {
            get;
            set;
        }
        string TaskType
        {
            get;
            set;
        }

        OCRRecordTypes RecordType
        {
            get;
            set;
        }

        string NumberOfTrans
        {
            get;
            set;
        }

        string NumberOfRecords
        {
            get;
            set;
        }

        string TotalAmount
        {
            get;
            set;
        }

        string DateOfSettlement
        {
            get;
            set;
        }

        string FirstDate
        {
            get;
            set;
        }

        string LastDate
        {
            get;
            set;
        }
    }
    
}
