﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public interface IUSPOCRRecord
    {
        IUSPOCRStartTransmissionRecord StartSendingReocrd
        {
            get;
            set;
        }

        List<IUSPOCRAssignmentRecord> AssignmentRecords
        {
            get;
            set;
        }

        IUSPOCREndTransmissionRecord EndSendingRecord
        {
            get;
            set;
        }
    }
}
