﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public interface IUSPNewOrChanged : ITagEnabled
    {
        string FBOCounter { get; set; }
        string RegisterType { get; set; }
        string KID { get; set; }
        string Warning { get; set; }

    }
}
