﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public interface IMessage
    {
        string ID { get; set; }
        string Text { get; set; }
    }
}
