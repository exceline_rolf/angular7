﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class OCRStartAssignmentRecord : IUSPOCRStartAssignmentRecord
    {
        public string FormatCode
        {
            get;
            set;
        }

        public string ServiceCode
        {
            get;
            set;
        }

        public string TaskType
        {
            get;
            set;
        }

        public OCRRecordTypes RecordType
        {
            get;
            set;
        }

        public string AgreementID
        {
            get;
            set;
        }

        public string TaskNumber
        {
            get;
            set;
        }

        public string RecipientAccountNumber
        {
            get;
            set;
        }
    }
}
