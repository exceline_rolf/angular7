﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class USPInitialData : IUSPInitialData
    {
        private string[][] _ItemTypes;
        public string[][] ItemTypes
        {
            get
            {
                return _ItemTypes;
            }
            set
            {
                _ItemTypes = value;
            }
        }
    }
}
