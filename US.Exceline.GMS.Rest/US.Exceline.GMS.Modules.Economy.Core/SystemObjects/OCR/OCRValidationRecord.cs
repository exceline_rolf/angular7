﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class OCRValidationRecord
    {
        public bool HasCriticalError { get; set; }

        public string CreditorInkassoID { get; set; }

        public string CriticalErrorMessage { get; set; }

        public OCRValidationRecord(int lineIndex)
        {
            LineIndex = lineIndex;
            IsValidRecord = true;
            HasCriticalError = false;
        }

        public decimal Amount { get; set; }

        public int LineIndex { get; set; }

        public OCRValidationRecordTypes ValidationRecordType { get; set; }

        public bool IsValidRecord { get; set; }

        public string ErrorMessage { get; set; }
    }
}
