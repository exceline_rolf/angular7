﻿using System;
using System.Runtime.Serialization;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    [DataContract]
    public class ExcelineSponsorMembersForPrint
    {
        private int _orderLineNo = -1;
        public int OrderLineNo
        {
            get { return _orderLineNo; }
            set { _orderLineNo = value; }
        }

        private int _aRItemNo = -1;
        public int ARItemNo
        {
            get { return _aRItemNo; }
            set { _aRItemNo = value; }
        }

        private String _creditorInkassoID = String.Empty;
        public String CreditorInkassoID
        {
            get { return _creditorInkassoID; }
            set { _creditorInkassoID = value; }
        }

        private String _custId = String.Empty;
        public String CustId
        {
            get { return _custId; }
            set { _custId = value; }
        }

        private String _firstName = String.Empty;
        public String FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private String _lastName = String.Empty;
        public String LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private String _nameOnContract = String.Empty;
        public String NameOnContract
        {
            get { return _nameOnContract; }
            set { _nameOnContract = value; }
        }

        private String _address = String.Empty;
        public String Address
        {
            get { return _address; }
            set { _address = value; }
        }

        private String _zipCode = String.Empty;
        public String ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }

        private String _postPlace = String.Empty;
        public String PostPlace
        {
            get { return _postPlace; }
            set { _postPlace = value; }
        }

        private double _discountAmount = -1.00;
        public double DiscountAmount
        {
            get { return _discountAmount; }
            set { _discountAmount = value; }
        }

        private String _employeeNo = String.Empty;
        public String EmployeeNo
        {
            get { return _employeeNo; }
            set { _employeeNo = value; }
        }

        private String _sponsorRef = String.Empty;
        public String SponsorRef
        {
            get { return _sponsorRef; }
            set { _sponsorRef = value; }
        }

        private int _visits = -1;
        public int Visits
        {
            get { return _visits; }
            set { _visits = value; }
        }

        private Double _invoiceAmount = -1.00;
        public Double InvoiceAmount
        {
            get { return _invoiceAmount; }
            set { _invoiceAmount = value; }
        }

        private Double _amountOrderline = -1.00;
        public Double AmountOrderline
        {
            get { return _amountOrderline; }
            set { _amountOrderline = value; }
        }

        private DateTime? _lastVisit = null;
        public DateTime? LastVisit
        {
            get { return _lastVisit; }
            set { _lastVisit = value; }
        }

        private double _amount = -1.00;
        public double Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private DateTime? _trainingPeriodStart = null;
        public DateTime? TrainingPeriodStart
        {
            get { return _trainingPeriodStart; }
            set { _trainingPeriodStart = value; }
        }

        private DateTime? _trainingPeriodEnd = null;
        public DateTime? TrainingPeriodEnd
        {
            get { return _trainingPeriodEnd; }
            set { _trainingPeriodEnd = value; }
        }


    }
}

