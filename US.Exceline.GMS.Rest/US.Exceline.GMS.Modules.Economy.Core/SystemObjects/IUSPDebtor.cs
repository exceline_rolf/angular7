﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public interface IUSPDebtor
    {
        string DebtorFirstName { get; set; }// Exline File field ID - 4
        string DebtorSecondName { get; set; }// Exline File field ID - 5
        string DebtorcustumerID { get; set; }// Exline File field ID - 3
        string DebtorBirthDay { get; set; }// Exline File field ID - 10
        string DebtorPersonNo { get; set; }
        List<IUSPAddress> DebtorAddressList { get; set; }
        int DebtorEntityID { get; set; }
        int DebtorEntityRoleID { get; set; }
        string DebtorTitle { get; set; }
    }
}
