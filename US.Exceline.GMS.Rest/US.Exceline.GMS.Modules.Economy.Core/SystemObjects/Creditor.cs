﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class Creditor
    {
        /// <summary>
        /// Unique id that identified the Creditor. Stored in the Ent Table.
        /// </summary>
        /// 
        private string entID = string.Empty;
        public string EntID
        {
            get { return entID; }
            set { entID = value; }
        }


        private string name = string.Empty;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string born = string.Empty;
        public string Born
        {
            get { return born; }
            set { born = value; }
        }

        private string personNo = string.Empty;
        public string PersonNo
        {
            get { return personNo; }
            set { personNo = value; }
        }

        private string addressNo = string.Empty;
        public string AddressNo
        {
            get { return addressNo; }
            set { addressNo = value; }
        }

        private string accountNo = string.Empty;
        public string AccountNo
        {
            get { return accountNo; }
            set { accountNo = value; }
        }

        private string notify = string.Empty;
        public string Notify
        {
            get { return notify; }
            set { notify = value; }
        }

        private string aRNo = string.Empty;
        public string ARNo
        {
            get { return aRNo; }
            set { aRNo = value; }
        }

        private int crCount;

        public int CreditorCount
        {
            get { return crCount; }
            set { crCount = value; }
        }

        private string CID;

        public string cid
        {
            get { return CID; }
            set { CID = value; }
        }
        private string contactMobile;

        public string ContactMobile
        {
            get { return contactMobile; }
            set { contactMobile = value; }
        }

        private string contactWork;

        public string ContactWork
        {
            get { return contactWork; }
            set { contactWork = value; }
        }

        private int noOfDebtors;

        public int NoOfDebtors
        {
            get { return noOfDebtors; }
            set { noOfDebtors = value; }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private string fax;

        public string Fax
        {
            get { return fax; }
            set { fax = value; }
        }

        private string msn;

        public string MSN
        {
            get { return msn; }
            set { msn = value; }
        }

        private string skype;

        public string Skype
        {
            get { return skype; }
            set { skype = value; }
        }

        private string sms;

        public string SMS
        {
            get { return sms; }
            set { sms = value; }
        }

        private string inkasso;

        public string InkassoId
        {
            get { return inkasso; }
            set { inkasso = value; }
        }

        private int entNo;
        public int EntNo
        {
            get { return entNo; }
            set { entNo = value; }
        }

        private int entRoleId;
        public int EntRoleId
        {
            get { return entRoleId; }
            set { entRoleId = value; }
        }

        private int _printAllow;
        public int printAllow
        {
            get { return _printAllow; }
            set { _printAllow = value; }
        }

        private string _invoPrintStatus = string.Empty;
        public string InvoPrintStatus
        {
            get
            {
                return _invoPrintStatus;
            }
            set
            {
                _invoPrintStatus = value;
            }
        }

        private bool _isHelpAccount = false;
        public bool IsHelpAccount
        {
            get { return _isHelpAccount; }
            set { _isHelpAccount = value; }
        }

        private List<FileDetail> _filesToPrint;
        public List<FileDetail> FilesToPrint
        {
            get { return _filesToPrint; }
            set { _filesToPrint = value; }
        }

        private string _accountSource = string.Empty;
        public string AccountSource
        {
            get { return _accountSource; }
            set { _accountSource = value; }
        }

    }
}
