﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class EstimatedInvoiceDetails
    {
        private string _gymCode = string.Empty;
        public string GymCode
        {
            get { return _gymCode; }
            set { _gymCode = value; }
        }

        private int _gymId = -1;
        public int GymId
        {
            get { return _gymId; }
            set { _gymId = value; }
        }

        private decimal _ddAmount = 0;
        public decimal DDAmount
        {
            get { return _ddAmount; }
            set { _ddAmount = value; }
        }

        private decimal _ipAmount = 0;
        public decimal IpAmount
        {
            get { return _ipAmount; }
            set { _ipAmount = value; }
        }

        private decimal _ppAmount = 0;
        public decimal PpAmount
        {
            get { return _ppAmount; }
            set { _ppAmount = value; }
        }

        private decimal _inAmount = 0;
        public decimal InAmount
        {
            get { return _inAmount; }
            set { _inAmount = value; }
        }

        private decimal _spAmount = 0;
        public decimal SpAmount
        {
            get { return _spAmount; }
            set { _spAmount = value; }
        }

        private decimal _totalAmount = 0;
        public decimal TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }

        private decimal _invoiceFee = 0;
        public decimal InvoiceFee
        {
            get { return _invoiceFee; }
            set { _invoiceFee = value; }
        }

        private int _itemsCount = 0;
        public int ItemsCount
        {
            get { return _itemsCount; }
            set { _itemsCount = value; }
        }

        private decimal _spDDAmount = 0;
        public decimal SpDDAmount
        {
            get { return _spDDAmount; }
            set { _spDDAmount = value; }
        }
    }
}
