﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class InternalPerson
    {
        private string name = string.Empty;


        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string _RoleID = string.Empty;
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }

        private string _Born = string.Empty;
        public string Born
        {
            get { return _Born; }
            set { _Born = value; }
        }

        private int _AddressNo;
        public int AddressNo
        {
            get { return _AddressNo; }
            set { _AddressNo = value; }
        }



        private int entNo;


        public int EntNo
        {
            get { return entNo; }
            set { entNo = value; }
        }


        private int entRoleId;


        public int EntRoleId
        {
            get { return entRoleId; }
            set { entRoleId = value; }
        }


        private string personNo = string.Empty;


        public string PersonNo
        {
            get { return personNo; }
            set { personNo = value; }
        }

        private string notification = string.Empty;


        public string Notification
        {
            get { return notification; }
            set { notification = value; }
        }


        //private string mobile = string.Empty;


        //public string Mobile
        //{
        //    get { return mobile; }
        //    set { mobile = value; }
        //}

        //private string direct = string.Empty;


        //public string Direct
        //{
        //    get { return direct; }
        //    set { direct = value; }
        //}

        //private string work = string.Empty;


        //public string Work
        //{
        //    get { return work; }
        //    set { work = value; }
        //}



        //private string email = string.Empty;


        //public string Email
        //{
        //    get { return email; }
        //    set { email = value; }
        //}

        //private string fax = string.Empty;


        //public string Fax
        //{
        //    get { return fax; }
        //    set { fax = value; }
        //}

        //private string msn = string.Empty;


        //public string MSN
        //{
        //    get { return msn; }
        //    set { msn = value; }
        //}

        //private string skype = string.Empty;


        //public string Skype
        //{
        //    get { return skype; }
        //    set { skype = value; }
        //}

        //private string sms = string.Empty;


        //public string SMS
        //{
        //    get { return sms; }
        //    set { sms = value; }
        //}



        private List<CAddress> address;


        public List<CAddress> Address
        {
            get { return address; }
            set { address = value; }
        }
    }
}
