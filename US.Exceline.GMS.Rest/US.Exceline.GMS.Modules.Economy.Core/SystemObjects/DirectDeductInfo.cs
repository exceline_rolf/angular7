﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class DirectDeductInfo
    {
        private string Seq = string.Empty;
        public string seq
        {
            get { return Seq; }
            set { Seq = value; }
        }

        private string lastDue = string.Empty;
        public string LastDue
        {
            get { return lastDue; }
            set { lastDue = value; }
        }

        private string FirstDue = string.Empty;
        public string firstDue
        {
            get { return FirstDue; }
            set { FirstDue = value; }
        }

        private string TotalAmount = string.Empty;
        public string totalAmount
        {
            get { return TotalAmount; }
            set { TotalAmount = value; }
        }
        public List<DirectDeduct> ddObj { get; set; }
    }
}
