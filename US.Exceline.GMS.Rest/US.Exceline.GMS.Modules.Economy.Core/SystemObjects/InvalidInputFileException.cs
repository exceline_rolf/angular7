﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class InvalidInputFileException : ApplicationException
    {
        public string FileName { get; set; }

        public InvalidInputFileException(string fileName, string msg)
            : base(msg)
        {
            FileName = fileName;
        }
    }
}
