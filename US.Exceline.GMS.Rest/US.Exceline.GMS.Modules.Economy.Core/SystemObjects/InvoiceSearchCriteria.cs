﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    [DataContract]
    public class InvoiceSearchCriteria
    {
        private string _kid = string.Empty;
        [DataMember]
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }

        private string _invoiceNumber = string.Empty;
        [DataMember]
        public string InvoiceNumber
        {
            get { return _invoiceNumber; }
            set { _invoiceNumber = value; }
        }

        public string _creditorNumber = string.Empty;
        [DataMember]
        public string CreditorNumber
        {
            get { return _creditorNumber; }
            set { _creditorNumber = value; }
        }

        private string _creditorName = string.Empty;
        [DataMember]
        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }
        }

        private string _customerNumber = string.Empty;
        [DataMember]
        public string CustomerNumber
        {
            get { return _customerNumber; }
            set { _customerNumber = value; }
        }

        private string _customerName = string.Empty;
        [DataMember]
        public string CustomerName
        {
            get { return _customerName; }
            set { _customerName = value; }
        }

        private decimal _amount = 0;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private int _arNumber = 0;
        [DataMember]
        public int ARNumber
        {
            get { return _arNumber; }
            set { _arNumber = value; }
        }

        private int _subCaseNumber = 0;
        [DataMember]
        public int SubCaseNumber
        {
            get { return _subCaseNumber; }
            set { _subCaseNumber = value; }
        }

        private string _subCaseName = string.Empty;
        [DataMember]
        public string SubCaseName
        {
            get { return _subCaseName; }
            set { _subCaseName = value; }
        }

        private int _invoiceType = 0;
        [DataMember]
        public int InvoiceType
        {
            get { return _invoiceType; }
            set { _invoiceType = value; }
        }

        private DateTime _startDate = DateTime.Parse("1/1/2000");
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime _endDate = DateTime.Now;
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private int _creditorGroupID = -1;
        [DataMember]
        public int CreditorGroupID
        {
            get { return _creditorGroupID; }
            set { _creditorGroupID = value; }
        }
    }
}
