﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    [DataContract]
    public class USPApportionmentTransaction
    {
        private string _itemType = string.Empty;
        [DataMember]
        public string ItemType
        {
            get { return _itemType; }
            set { _itemType = value; }
        }

        private int _arNo = 0;
        [DataMember]
        public int ArNo
        {
            get { return _arNo; }
            set { _arNo = value; }
        }

        private int _apportionID = 0;
        [DataMember]
        public int ApportionID
        {
            get { return _apportionID; }
            set { _apportionID = value; }
        }

        private int _arItemNo = 0;
        [DataMember]
        public int ArItemNo
        {
            get { return _arItemNo; }
            set { _arItemNo = value; }
        }

        private string _text = string.Empty;
        [DataMember]
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private string _invoiceNo = string.Empty;
        [DataMember]
        public string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }

        private string _kid = string.Empty;
        [DataMember]
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }


        private int _caseNo = 0;
        [DataMember]
        public int CaseNo
        {
            get { return _caseNo; }
            set { _caseNo = value; }
        }

        private string _creditorNo = string.Empty;
        [DataMember]
        public string CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }

        private int _subcaseNo = 0;
        [DataMember]
        public int SubCaseNo
        {
            get { return _subcaseNo; }
            set { _subcaseNo = value; }
        }

        private int itemTypeId = 0;
        [DataMember]
        public int ItemTypeId
        {
            get { return itemTypeId; }
            set { itemTypeId = value; }
        }

        private bool _isRemainingAmount = false;
        [DataMember]
        public bool IsRemainingAmount
        {
            get { return _isRemainingAmount; }
            set { _isRemainingAmount = value; }
        }

        private decimal _balance = 0;
        [DataMember]
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        private decimal _totalBalance = 0;
        [DataMember]
        public decimal TotalBalance
        {
            get { return _totalBalance; }
            set { _totalBalance = value; }
        }


        private string _fileName = string.Empty;
        [DataMember]
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        private int _paymentID = -1;
        [DataMember]
        public int PaymentID
        {
            get { return _paymentID; }
            set { _paymentID = value; }
        }


        private decimal _transactionAmount = 0;
        [DataMember]
        public decimal TransactionAmount
        {
            get { return _transactionAmount; }
            set { _transactionAmount = value; }
        }

        private decimal _amount = 0;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private decimal _creditorSum;
        [DataMember]
        public decimal CreditorSum
        {
            get { return _creditorSum; }
            set { _creditorSum = value; }
        }

        private decimal _bureauSum;
        [DataMember]
        public decimal BureauSum
        {
            get { return _bureauSum; }
            set { _bureauSum = value; }
        }

        private decimal _debitorSum = 0;
        [DataMember]
        public decimal DebitorSum
        {
            get { return _debitorSum; }
            set { _debitorSum = value; }
        }


        private int _profileId = 0;
        [DataMember]
        public int ProfileId
        {
            get { return _profileId; }
            set { _profileId = value; }
        }

        private DateTime _regDate;
        [DataMember]
        public DateTime RegDate
        {
            get { return _regDate; }
            set { _regDate = value; }
        }

        private DateTime _dueDate;
        [DataMember]
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }


        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private decimal _vatAmount = 0;
        [DataMember]
        public decimal VATAmount
        {
            get { return _vatAmount; }
            set { _vatAmount = value; }
        }

        private int _vatCode = -1;
        [DataMember]
        public int VATCode
        {
            get { return _vatCode; }
            set
            {
                _vatCode = value;
                if (value != -1)
                    _vatCodeString = value.ToString();
                else
                    _vatCodeString = "N/A";
            }
        }

        private string _vatCodeString = string.Empty;
        [DataMember]
        public string VATCodeString
        {
            get { return _vatCodeString; }
            set { _vatCodeString = value; }
        }

        private decimal _vatBasis = 0;
        [DataMember]
        public decimal VATBasis
        {
            get { return _vatBasis; }
            set { _vatBasis = value; }
        }

        private decimal _vatPaidByDebitor = 0;
        [DataMember]
        public decimal VATPaidByDebitor
        {
            get { return _vatPaidByDebitor; }
            set { _vatPaidByDebitor = value; }
        }

        private decimal _vatDeducted = 0;
        [DataMember]
        public decimal VATDeducted
        {
            get { return _vatDeducted; }
            set { _vatDeducted = value; }
        }

        private decimal _totalAmount = 0;
        [DataMember]
        public decimal TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }

        private string _jurnalId = string.Empty;
        [DataMember]
        public string JurnalId
        {
            get { return _jurnalId; }
            set { _jurnalId = value; }
        }

        private int _remitVoucherNo = 0;
        [DataMember]
        public int RemitVoucherNo
        {
            get { return _remitVoucherNo; }
            set { _remitVoucherNo = value; }
        }

        private int _paymentVoucherNo = -1;
        [DataMember]
        public int PaymentVoucherNo
        {
            get { return _paymentVoucherNo; }
            set { _paymentVoucherNo = value; }
        }

        private string _voucherHeader = string.Empty;
        [DataMember]
        public string VoucherHeader
        {
            get { return _voucherHeader; }
            set { _voucherHeader = value; }
        }

        private string _headerText = string.Empty;
        [DataMember]
        public string HeaderText
        {
            get { return _headerText; }
            set { _headerText = value; }
        }

        private bool _isError = false;
        [DataMember]
        public bool IsError
        {
            get { return _isError; }
            set { _isError = value; }
        }

        private int _rowNo = 0;
        [DataMember]
        public int RowNo
        {
            get { return _rowNo; }
            set { _rowNo = value; }
        }

        private int _exceedPaymentAritemNo = -1;
        [DataMember]
        public int ExceedPaymentAritemNo
        {
            get { return _exceedPaymentAritemNo; }
            set { _exceedPaymentAritemNo = value; }
        }

        private int _exceedPaymentApportionId = -1;
        [DataMember]
        public int ExceedPaymentApportionId
        {
            get { return _exceedPaymentApportionId; }
            set { _exceedPaymentApportionId = value; }
        }

        private string _errorType = string.Empty;
        [DataMember]
        public string ErrorType
        {
            get { return _errorType; }
            set { _errorType = value; }
        }

    }
}
