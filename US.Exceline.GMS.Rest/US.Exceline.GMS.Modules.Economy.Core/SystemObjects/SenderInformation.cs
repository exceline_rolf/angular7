﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class SenderInformation
    {
        private String _senderName = string.Empty;
        public String SenderName
        {
            get { return _senderName; }
            set { _senderName = value; }
        }

        private String _senderAdress;
        public String SenderAdress
        {
            get { return _senderAdress; }
            set { _senderAdress = value; }
        }

        private String _senderZipCode = String.Empty;
        public String SenderZipCode
        {
            get { return _senderZipCode; }
            set { _senderZipCode = value; }
        }

        private String _senderZipName = String.Empty;
        public String SenderZipName
        {
            get { return _senderZipName; }
            set { _senderZipName = value; }
        }

        private String _senderPhoneNumber;
        public String SenderPhoneNumber
        {
            get { return _senderPhoneNumber; }
            set { _senderPhoneNumber = value; }
        }

        private String _senderWebSite;
        public String SenderWebSite
        {
            get { return _senderWebSite; }
            set { _senderWebSite = value; }
        }

        private String _senderSwift;
        public String SenderSwift
        {
            get { return _senderSwift; }
            set { _senderSwift = value; }
        }

        private String _senderIBAN;
        public String SenderIBAN
        {
            get { return _senderIBAN; }
            set { _senderIBAN = value; }
        }


        private String _senderOrganizationNumber;
        public String SenderOrganiztionNumber
        {
            get { return _senderOrganizationNumber; }
            set { _senderOrganizationNumber = value; }
        }
    }
}
