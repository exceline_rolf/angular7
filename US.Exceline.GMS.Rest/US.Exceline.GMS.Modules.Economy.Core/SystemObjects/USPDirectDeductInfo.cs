﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class USPDirectDeductInfo : IUSPDirectDeductInfo
    {
        private string _FileName = string.Empty;
        public string FileName
        {
            get
            {
                return _FileName;
            }
            set
            {
                _FileName = value;
            }
        }


        private string _TotalAmount = string.Empty;
        public string TotalAmount
        {
            get
            {
                return _TotalAmount;
            }
            set
            {
                _TotalAmount = value;
            }
        }


        private string _FirstDueDate = string.Empty;
        public string FirstDueDate
        {
            get
            {
                return _FirstDueDate;
            }
            set
            {
                _FirstDueDate = value;
            }
        }


        private string _LastDueDate = string.Empty;
        public string LastDueDate
        {
            get
            {
                return _LastDueDate;
            }
            set
            {
                _LastDueDate = value;
            }
        }


        private List<IUSPStandingOrderItem> _Ddobj = new List<IUSPStandingOrderItem>();
        public List<IUSPStandingOrderItem> Ddobj
        {
            get
            {
                return _Ddobj;
            }
            set
            {
                _Ddobj = value;
            }
        }
    }
}
