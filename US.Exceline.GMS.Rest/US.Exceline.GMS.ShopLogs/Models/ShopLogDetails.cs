﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.ShopLogs.Models
{
    public class ShopLogDetails
    {
        public string BranchId { get; set; }
        public string Reference { get; set; }
        public string Time { get; set; }
        public string Message { get; set; }
    }
}