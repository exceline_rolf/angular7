﻿using System;
using System.Collections.Generic;
using US.Common.Notification.Core.DomainObjects;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Admin.ManageClassSettings;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.SystemObjects;
using US.Payment.Core.BusinessDomainObjects;
using US.USDF.Core.DomainObjects;

namespace US.GMS.Data.SystemObjects
{
    public interface IDataAdapter
    {
        List<PackageDC> GetContracts(int branchId, string searchText, string gymCode);

        #region Setting
        decimal GetSessionTimeOutAfter(int branchId, string gymCode);
        #endregion

        #region Task

        int AddEntityTask(ExcelineTaskDC excelineTask, string gymCode);

        #endregion

        #region Category

        /// <summary>
        /// Get category types
        /// </summary>
        /// <param name="name">name/code of type</param>
        /// <returns></returns>
        List<CategoryTypeDC> GetCategoryTypes(string name, string user, int branchId, string gymCode);

        /// <summary>
        /// Save new category
        /// </summary>
        /// <param name="category"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        int SaveCategory(CategoryDC category, string user, int branchId, string gymCode);
        List<ArticleDC> GetArticlesSearch(int branchId, string keyWord, string gymCode);

        /// <summary>
        /// Save new region
        /// </summary>
        /// <param name="region"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        int SaveRegion(RegionDC region, string user, int branchId, string gymCode);

        /// <summary>
        /// Get categories by type
        /// </summary>
        /// <param name="type">category type name</param>
        /// <returns></returns>
        List<CategoryDC> GetCategoriesByType(string type, string user, string gymCode);

        /// <summary>
        /// Get regions
        /// </summary>
        /// <returns></returns>
        List<RegionDC> GetRegions(int branchId, string gymCode, string countryId);

        /// <summary>
        /// Update category
        /// </summary>
        /// <param name="category"></param>
        /// <param name="user"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        bool UpdateCategory(CategoryDC category, string user, int branchId, string gymCode);

        /// <summary>
        /// Delete Category
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="user"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        int DeleteCategory(int categoryId, string user, int branchId, string gymCode);

        /// <summary>
        /// Search categories
        /// </summary>
        /// <param name="searchText"></param>
        /// <param name="searchType"></param>
        /// <returns></returns>
        List<CategoryDC> SearchCategories(string searchText, string searchType, string user, int branchId, string gymCode);

        List<ArticleDC> GetArticlesForContract(int categoryID, string gymCode, int branchID);


        OrdinaryMemberDC GetMembersById(int branchId, int memberId, string gymCode);
        #endregion

        #region Exceline Task
        int SaveExcelineTask(ExcelineTaskDC excelineTask, string gymCode);
        int EditExcelineTask(ExcelineTaskDC excelineTask, string gymCode);
        List<ExcelineTaskDC> GetExcelineTasks(int branchId, bool isFxied, int templateId, int assignTo, string roleType, int followupMemberId, string gymCode);
        List<ExcelineTaskDC> SearchExcelineTask(string searchText, TaskTemplateType type, int followUpMemId, int branchId, bool isFxied, string gymCode);
        bool SaveTaskWithOutSchedule(ExcelineTaskDC excelineTask, string gymCode);
        #endregion

        #region Task Template
        bool SaveTaskTemplate(TaskTemplateDC taskTemplate, bool isEdit, string gymCode);
        bool UpdateTaskTemplate(TaskTemplateDC taskTemplate, string gymCode);
        bool DeleteTaskTemplate(int taskTemplateId, string gymCode);
        bool DeActivateTaskTemplate(int taskTemplateId, string gymCode);
        List<TaskTemplateDC> GetTaskTemplates(TaskTemplateType templateType, int branchId, int templateId, string gymCode);
        List<ExtendedTaskTemplateDC> GetTaskTemplateExtFields(int extFieldId, string gymCode);
        List<ExtendedFieldDC> GetExtFieldsByCategory(int categoryId, string categoryType, string gymCode);
        #endregion

        #region Exceline TaskCategory
        bool SaveTaskCategory(ExcelineTaskCategoryDC taskCategory, bool isEdit, string gymCode);
        List<ExcelineTaskCategoryDC> GetTaskCategories(int branchId, string gymCode);
        bool DeleteTaskCategory(int taskCategoryId, string gymCode);
        #endregion

        #region Exceline JobCategory
        bool SaveJobCategory(ExcelineJobCategoryDC jobCategory, bool isEdit, string gymCode);
        List<ExcelineJobCategoryDC> GetJobCategories(int branchId, string gymCode);
        bool DeleteJobCategory(int taskCategoryId, string gymCode);
        #endregion

        #region Schedule
        bool UpdateActiveTime(EntityActiveTimeDC activeTime, bool isDelete,int branchId,string user, string gymCode);
        bool UpdateCalenderActiveTimes(List<EntityActiveTimeDC> activeTimeList, int branchId, string user, string gymCode);
        bool AddEventToNotifications(USCommonNotificationDC notification, string gymCode);
        bool UpdateSheduleItems(ScheduleDC schedule, string gymCode);
        List<EntityActiveTimeDC> GetEntityActiveTimes(int branchId, DateTime startDate, DateTime endDate, List<int> entityList, string entityType, string gymCode, string user);
        ScheduleDC GetEntitySchedule(int entityId, string entityRoleType, string user, string gymCode);
        bool DeleteScheduleItem(int scheduleItemId, bool activeStatus, string user, string gymCode);
        bool SaveShedule(int id, string name, string roleId, int branchId, ScheduleDC sheduleDc, string user, string gymCode);
        List<ScheduleItemDC> GetScheduleItemsByScheduleId(int scheduleId, string gymCode);
        int CheckScheduleOverLapForEmployee(ScheduleItemDC scheduleItem, string gymCode);
        #endregion

        #region Member
        List<OrdinaryMemberDC> GetMembersForCommonBooking(int branchId, string searchText, string gymCode);
        List<ActivityDC> GetActivitiesForCommonBooking(int entityId, string entityType, int memberId, string gymCode);
        bool ManageMemberShopAccount(MemberShopAccountDC memberShopAccount, string mode, string user, int branchId, int salePointId, string gymCode);
        MemberShopAccountDC GetMemberShopAccounts(int memberId, int hit, string gymCode, bool itemsNeeded);
        List<MemberShopAccountItemDC> GetMemberShopAccountItems(int shopAccountId, string gymCode, int branchId);
        #endregion

        #region Article

        List<ArticleDC> GetArticles(int articleId, int branchId, ArticleTypes articleType, string keyWord, CategoryDC category, int activityId, bool? isObsalate, bool activeState, string gymCode, bool isFilterByBranch);
        int SaveArticle(ArticleDC articleDc, string user, int branchID, int activityCategoryId, string gymCode);

        #endregion

        #region Booking

        bool CheckCommonBookingAvailability(int activeTimeId, int entityId, int branchId, string entityType, DateTime startDateTime, DateTime endDateTime, string gymCode);
        bool SaveCommonBookingDetails(CommonBookingDC commonBookingDC, ScheduleDC scheduleDC, int branchId, string user, string scheduleCategoryType, string gymCode);

        #endregion

        #region Payments
        string RegisterPayment(int branchId, int memberId, int articleNo, decimal paymentAmount, decimal discount, string createdUser, decimal invoiceAmount, PaymentTypes paymentType, string gymCode);
        SaleResultDC RegisterInstallmentPayment(int memberBranchID, int loggedBranchID, string user, Core.DomainObjects.ManageMemberships.InstallmentDC installment, PaymentDetailDC paymentDetail, string gymCode, int salePointId, ShopSalesDC salesDetails);
        SaleResultDC GenerateInvoice(int invocieBranchId, string user, InstallmentDC installment, PaymentDetailDC paymentAmount, string invoiceType, string gymCode, int memberBranchID);
        SaleResultDC RegisterInvoicePayment(PaymentDetailDC paymentDetails, string gymCode, int branchID, ShopSalesDC salesDetails, string user);
        #endregion

        #region Invoicing
        List<OrderLineDC> GetOrderLinesForAutoCreditNote(int branchId, int priority, string gymCode);
        bool ScheduleOrdersForInvoicing(int branchId, string gymCode);
        bool ScheduleSponsorOrdersForInvoicing(int branchId, string gymCode);
        string GetCreditorForBranch(int branchId, string gymCode);
        bool CancelInvoice(int arItemNo, string user, string gymCode);
        List<MemberInvoiceDC> GetMemberInvoices(int memberID, int hit, string gymCode, string user);
        ExcelineInvoiceDetailDC GetInvoiceDetails(int arItemNo, int branchId, string gymCode);
        bool UpdateInvoice(int arItemNo, string comment, DateTime DueDate, string gymCode);
        #endregion

        #region Utility
        string GetCityForPostalCode(string postalCode, string gymCode);
        List<CalendarHoliday> GetHolidays(DateTime calendarDate, int branchId, string gymCode);
        List<EntityVisitDC> GetVistsByDate(DateTime selectedDate, int branchId, string systemName, string gymCode, string user);
        #endregion

        #region Activity

        List<ActivityDC> GetActivities(int branchId, string gymCode);
        List<ActivityDC> GetActivitiesForEntity(int entityId, string entityType, string gymCode, int branchId);

        #endregion

        List<ExcelineRoleDc> GetUserRole(string gymCode);

        #region Notification
        List<NotificationTemplateDC> GetNotificationTemplatesByType(int templateTypeId, NotifyMethodType notifyMethod, int branchId, string gymCode);
        #endregion

        #region CheckEmployee
        int CheckEmployeeByEntNo(int entityNO, string gymCode);

        #endregion

        #region Gym Settings
        #region Gym Access Time Settings
        //List<GymAccessTimeSettingsDC> GetGymAccessTimeSettings(int branchId, string gymCode, string userName );
        //bool SaveGymAccessTimeSettings(int branchId, GymAccessTimeSettingsDC accessTimeSettings, string gymCode);

        List<GymAccessSettingDC> GetGymAccessSettings(int branchId, string gymCode, string userName);
        bool SaveGymAccessSettings(int branchId, GymAccessSettingDC accessTimeSettings, string gymCode);

        List<GymOpenTimeDC> GetGymOpenTimes(int branchId, string gymCode, string userName);
        bool SaveGymOpenTimes(int branchId, List<GymOpenTimeDC> gymOpenTimesList, string gymCode);
        #endregion

        #region Gym Required Settings
        List<GymRequiredSettingsDC> GetGymRequiredSettings(int branchId, string gymCode, string userName);
        bool SaveGymRequiredSettings(int branchId, GymRequiredSettingsDC gymRequiredSettings, string gymCode);
        #endregion

        #region Gym Member Search Settings
        List<GymMemberSearchSettingsDC> GetGymMemberSearchSettings(int branchId, string gymCode, string userName);
        bool SaveGymMemberSearchSettings(int branchId, GymMemberSearchSettingsDC gymMemberSearchSettings, string gymCode);
        #endregion

        #region Gym Hardware Profile Settings
        List<ExceHardwareProfileDC> GetHardwareProfiles(int branchId, string gymCode, string userName);
        bool SaveHardwareProfile(int branchId, List<ExceHardwareProfileDC> hardwareProfileList, string gymCode);
        #endregion

        #region Gym Economy Settings
        List<GymEconomySettingsDC> GetGymEconomySettings(int branchId, string gymCode, string userName);
        bool SaveGymEconomySettings(int branchId, GymEconomySettingsDC gymEconomySettings, string gymCode);
        #endregion

        #region Gym Sms Settings
        List<GymSmsSettingsDC> GetGymSmsSettings(int branchId, string gymCode, string userName);
        bool SaveGymSmsSettings(int branchId, GymSmsSettingsDC gymSmsSettings, string gymCode);
        #endregion

        #region Gym Other Settings
        List<GymOtherSettingsDC> GetGymOtherSettings(int branchId, string gymCode, string userName);
        bool SaveGymOtherSettings(int branchId, GymOtherSettingsDC gymOtherSettings, string gymCode);
        #endregion

        #region GetSelectedGymSettings
        Dictionary<string, object> GetSelectedGymSettings(List<string> settingColumnList, bool isGymSettings, int branchId, string gymCode);
        List<GymIntegrationSettingsDC> GetGymIntegrationSettings(int branchId, string gymCode);
        List<GymIntegrationExcSettingsDC> GetGymIntegrationExcSettings(int branchId, string gymCode);
        List<TerminalType> GetTerminalTypes(string gymCode);
        OtherIntegrationSettingsDC GetOtherIntegrationSettings(int branchId, string gymCode, string systemName);
        bool AddUpdateOtherIntegrationSettings(int branchId, OtherIntegrationSettingsDC otherIntegrationSettings, string gymCode);
        #endregion
        #endregion

        #region CCX
        USDFInvoiceInfo GetAllInvoices(int chunkSize, string gymCode, int creditorNo, int batchsequenceNo, List<string> invoiceTypes);
        USDFInvoiceInfo GetDebtWarningInvoices(int chunkSize, string gymCode, int creditorNo, int batchsequenceNo);
        string GetGymCodeForCreditor(int creditorNo);
        USDFInvoiceInfo GetDirectPayments(int chunkSize, string gymCode, int creditorNo, int batchSequenceNo);
        int AddPaymentStatus(USPPaymentStatusFileRow paymentStatus, string gymCode);
        int AddNewCancelledStatus(US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPNewOrCancelledFileRow newOrCancelledStatus, string gymCode);
        int AddDataImportReceipt(DataImportReceiptDetail importReceipt, string gymCode);
        int ImportNewCancelledStatus(string gymCode);
        int ImportPaymentStatus(string gymCode);
        int ImportDataImportReceipt(string gymCode);
        int UpdateDWStatus(string gymCode);
        int UpdateTemplateStatus(string gymCode);
        List<GymCompanyDC> GetGymCompanies();
        bool UpdateExportStatus(string gymCode);
        List<ImportStatusDC> GetClaimStatus(string gymCode, int hit, int status, DateTime date, ImportStatusDC advancedData);
        bool ReScheduleClaim(string gymCode, int itemNo);
        PaymentImportStatusDC GetPaymentImportStatus(string gymCode, int hit, DateTime RegDate);
        List<ImportStatusDC> GetClaimExportStatusForMember(string gymCode, int memberID, int hit, DateTime? transferedDate,  string type, string user);
        bool MoveToShopAccount(int statusID, bool isImportStatus, string gymCode);
        #endregion

        List<CountryDC> GetCountryDetails(string gymCode);
        List<LanguageDC> GetLangugeDetails(string gymCode);
        int SavePostalArea(string user, string postalCode, string postalArea, Int64 population, Int64 houseHold, string gymCode);
        ArticleDC GetInvoiceFee(int branchId, string gymCode);
        int UpdateMemberStatus(string gymCode, int branchId);
        List<TrainingProgramVisitDetailDC> GetMemberVisitForTrainingProgram(string gymCode, int branchId, string systemName, DateTime lastUpdatedDateTime);
        List<TrainingProgramClassVisitDetailDC> GetMemberClassVisitForTrainingProgram(string gymCode, int branchId, string systemName, DateTime lastUpdatedDateTime);
        bool ImportArticleList(List<ArticleDC> articleList, string user, int branchID, string gymCode);
        #region GymDetails
        string GetGymConnection(string gymCode);
        int GetGymCompanyId(string gymCode);
        List<BranchDetailsDC> GetBranchDetails();
        #endregion

        #region ClassTypeSetting

        List<ExceClassTypeDC> GetClassTypes(string gymCode);

        List<ExceClassTypeDC> GetClassTypesByBranch(int branchId, string gymCode);

        bool AddEditClassType(ExceClassTypeDC classType, string gymCode);

        int DeleteClassType(int classTypeId, string gymCode);


        #endregion

        List<ArticleDC> GetArticlesForVisits(string gymCode, int branchID);
        int AddCreditorNote(CreditNoteDC creditNote, int branchId, bool isReturn, string gymCode);
        List<ExceAccessProfileDC> GetAccessProfiles(string gymCode, Gender gender);
        int UpdateAccessProfiles(ExceAccessProfileDC accessProfile, string user, string gymCode);

        bool SaveGymIntegrationSettings(int branchId, GymIntegrationSettingsDC integrationSettings, string gymCode);
        bool SaveGymIntegrationExcSettings(int branchId, GymIntegrationExcSettingsDC integrationSettings, string gymCode);
        bool DeleteIntegrationSettingById(int id, string gymCode);

        #region Manage Vouchers
        //List<VoucherDC> GetGiftVouchersList(int branchId, string gymCode);
        //int AddUpdateGiftVoucher(int branchId, VoucherDC voucher, string gymCode);
        //int GetNextGiftVoucherNo(int branchId, string gymCode);
        #endregion

        List<String> ActGetFileData(string gymCode);

        bool AddRegionDetails(RegionDC region, string gymCode);
        bool AddCountryDetails(CountryDC country, string gymCode);
        bool ManageTemplate(string gymCode);
        int PayCreditNote(int arItemno, List<PayModeDC> paymodes, int memberId, int shopAccoutID, string gymCode, string user, int salePointId, int loggedbranchID);
        List<string> GetGymCodes();
        List<BranchDetailsDC> GetBranches(string gymCode);
        List<ExceUserBranchDC> GetGymsforAccountNumber(string accountNumber, string gymCode);
        MemberFeeGenerationResultDC GenerateMemberFee(List<int> gyms, int month, string gymCode, string user);

        List<ExcelineMemberDC> GetMembersForShop(int branchId, string searchText, int status, MemberRole memberRole,
                                                 int hit, string gymCode);


        List<ExcelineMemberDC> GetAllMemberAndCompany(int branchId, string searchText, int status, int hit,
                                                      string gymCode);

        bool ValidateGenerateMemberFeeWithMemberFeeMonth(List<int> gyms, string gymCode, string user);
        List<string> ConfirmationDetailForGenerateMemberFee(List<int> gyms, int month, string gymCode, string user);
        bool UploadArticle(List<ArticleDC> articleList, string user, int branchId, string gymCode);
        int CancelPayment(int paymentID, bool keepthePayment, string user, string comment, string gymCode);
        bool SaveTaskStatus(string taskName, string description, int branchId, int status, string gymCode);
        List<GymEmployeeDC> GetEmployee(int aciveStatus, string gymCode);
        bool UpdateSettingForUserRoutine(string key, string value, string user, string gymCode);
        MemberShopAccountDC GetMemberShopAccounts(int memberId, string entityType, int hit, string gymCode, bool itemsNeeded);
        List<MemberInvoiceDC> GetMemberInvoicesForContract(int memberID, int contractid, int hit, string gymCode, string user);
        bool ValidateEmail(string email, string gymCode);
        bool ValidateMemberCard(string memberCard, int memberId, string gymCode);
        bool ValidateGatCardNo(string gatCardNo, string gymCode);
        int SaveShopXMLData(String xmlData, String sessionKey, string gymCode);
        Tuple<int, string> ValidateMobile(string mobile, string mobilePrefix, string gymCode);
    }
}
