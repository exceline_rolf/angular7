﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Data.SystemObjects;

namespace US.GMS.Data.DataAdapters
{
    public class InvoiceFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static bool ScheduleOrdersForInvoicing(int branchId, string gymCode)
        {
            return GetDataAdapter().ScheduleOrdersForInvoicing(branchId, gymCode);
        }

        public static bool ScheduleSponsorOrdersForInvoicing(int branchId, string gymCode)
        {
            return GetDataAdapter().ScheduleSponsorOrdersForInvoicing(branchId, gymCode);
        }

        public static string GetCreditorForBranch(int branchId, string gymCode)
        {
            return GetDataAdapter().GetCreditorForBranch(branchId, gymCode);
        }

        public static bool CancelInvoice(int arItemNo, string user, string gymCode)
        {
            return GetDataAdapter().CancelInvoice(arItemNo, user, gymCode);
        }


        public static List<OrderLineDC> GetOrderLinesForAutoCreditNote(int branchId, int priority, string gymCode)
        {
            return GetDataAdapter().GetOrderLinesForAutoCreditNote(branchId, priority, gymCode);
        }

        public static List<MemberInvoiceDC> GetMemberInvoices(int memberID, int hit, string gymCode, string user)
        {
            return GetDataAdapter().GetMemberInvoices(memberID, hit, gymCode, user);
        }

        public static ExcelineInvoiceDetailDC GetInvoiceDetails(int arItemNo, int branchId, string gymCode)
        {
            return GetDataAdapter().GetInvoiceDetails(arItemNo, branchId, gymCode);
        }

        public static bool UpdateInvoice(int arItemNo, string comment, DateTime DueDate, string gymCode)
        {
            return GetDataAdapter().UpdateInvoice(arItemNo, comment, DueDate, gymCode);
        }

        public static int AddCreditorNote(CreditNoteDC creditNote, int branchId, bool isReturn,string gymCode)
        {
            return GetDataAdapter().AddCreditorNote(creditNote, branchId, isReturn,gymCode);
        }

        public static int PayCreditNote(int arItemno, List<PayModeDC> paymodes, int memberId, int shopAccoutID, string gymCode, string user, int salePointId, int loggedbranchID)
        {
            return GetDataAdapter().PayCreditNote(arItemno, paymodes, memberId, shopAccoutID, gymCode, user, salePointId, loggedbranchID);
        }

        public static List<MemberInvoiceDC> GetMemberInvoicesForContract(int memberID, int contractid,int hit, string gymCode, string user)
        {
            return GetDataAdapter().GetMemberInvoicesForContract(memberID,contractid, hit, gymCode, user);
        }
    }
}
