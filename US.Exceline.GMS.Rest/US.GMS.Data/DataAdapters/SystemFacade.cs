﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Data.SystemObjects;

namespace US.GMS.Data.DataAdapters
{
    public class SystemFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static List<ExceAccessProfileDC> GetAccessProfiles(string gymCode, Gender gender)
        {
            return GetDataAdapter().GetAccessProfiles(gymCode, gender);
        }

        public static int UpdateAccessProfiles(ExceAccessProfileDC AccessProfile, string user, string gymCode)
        {
            return GetDataAdapter().UpdateAccessProfiles(AccessProfile, user, gymCode);
        }

        public static bool SaveTaskStatus(string taskName, string description, int branchId, int status, string gymCode)
        {
            return GetDataAdapter().SaveTaskStatus(taskName, description, branchId, status, gymCode);
        }

        public static List<String> ActGetFileData(string gymCode)
        {
            return GetDataAdapter().ActGetFileData(gymCode);
        }


        public static bool ValidateEmail(string email, string gymCode)
        {
            return GetDataAdapter().ValidateEmail(email, gymCode);
        }

        public static bool ValidateMemberCard(string memberCard, int memberId, string gymCode)
        {
            return GetDataAdapter().ValidateMemberCard(memberCard, memberId, gymCode);
        }

        public static bool ValidateGatCardNo(string gatCardNo, string gymCode)
        {
            return GetDataAdapter().ValidateGatCardNo(gatCardNo, gymCode);
        }

        public static Tuple<int, string> ValidateMobile(string mobile, string mobilePrefix, string gymCode)
        {
            return GetDataAdapter().ValidateMobile(mobile, mobilePrefix, gymCode);
        }
    }
}
