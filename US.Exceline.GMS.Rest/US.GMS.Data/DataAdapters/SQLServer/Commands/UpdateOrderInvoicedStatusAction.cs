﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateOrderInvoicedStatusAction : USDBActionBase<bool>
    {
        private int _installmentID = -1;

        public UpdateOrderInvoicedStatusAction(int installmentID)
        {
            _installmentID = installmentID;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipUpdateInstallmentStatus";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentID", System.Data.DbType.Int32, _installmentID));
                command.ExecuteNonQuery();

            }
            catch (Exception)
            { }
            return true;
        }
    }
}
