﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/9/2012 2:42:14 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class CheckCommonBookingAvailabilityAction : USDBActionBase<bool>
    {
        private int _activeTimeId;
        private int _entityId;
        private int _branchId;
        private string _entityType;
        private DateTime _startDateTime;
        private DateTime _endDateTime;
        private int _bookingCount;


        public CheckCommonBookingAvailabilityAction(int activeTimeId, int entityId, int branchId, string entityType, DateTime startDateTime, DateTime endDateTime)
        {
            this._activeTimeId = activeTimeId;
            this._entityId = entityId;
            this._branchId = branchId;
            this._entityType = entityType;
            this._startDateTime = startDateTime;
            this._endDateTime = endDateTime;
        }

        protected override bool Body(DbConnection connection)
        {
            string storedProcedureName = "USExceGMSCheckCommonkBookingAvailability";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeTimeId", DbType.Int32, _activeTimeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityType", DbType.String, _entityType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDateTime", DbType.DateTime, _startDateTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDateTime", DbType.DateTime, _endDateTime));



                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    _bookingCount = Convert.ToInt32(reader["BookingCount"]);
                }
                if (_bookingCount == 0)
                {
                    //booking time available
                    return true;
                }
                else
                {
                    //booking time not available
                    return false;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
