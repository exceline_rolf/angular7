﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
   public class ValidateGatCardNoAction : USDBActionBase<bool>
    {
        private string _gatCardNo = string.Empty;
        public ValidateGatCardNoAction(string gatCardNo)
        {
            _gatCardNo = gatCardNo;
        }
        protected override bool Body(DbConnection connection)
        {
            bool result ;
            string spName = "USExceGMSValidateGatCardNo";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@GatCardNo", DbType.String, _gatCardNo));
                result = Convert.ToBoolean(command.ExecuteScalar());
                return result;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
    }
}
