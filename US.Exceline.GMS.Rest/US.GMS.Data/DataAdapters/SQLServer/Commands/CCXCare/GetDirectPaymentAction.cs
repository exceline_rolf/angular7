﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.USDF.Core.DomainObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    public class GetDirectPaymentAction : USDBActionBase<USDFInvoiceInfo>
    {
        private int _chunkSize = 0;
        private int _creditorNo = 0;
        private int _batchSequenceNo = 0;
        public GetDirectPaymentAction(int chukSize,int creditorNo,int batchSequecenNo)
        {
            _chunkSize = chukSize;
            _creditorNo = creditorNo;
            _batchSequenceNo = batchSequecenNo;
        }
        protected override USDFInvoiceInfo Body(System.Data.Common.DbConnection connection)
        {
            USDFInvoiceInfo invoiceInfo = new USDFInvoiceInfo();
            USDFHeaderInfo headerInfor = new USDFHeaderInfo();
            headerInfor.MessageID = Guid.NewGuid().ToString();
            headerInfor.FileCreator = "Exceline";
            headerInfor.FileName = _creditorNo + "_DirectPayment_" + US.GMS.Core.Utils.Common.GetDate() + "_" + US.GMS.Core.Utils.Common.GetCurrentTime() + "_" + _chunkSize + "_" + _batchSequenceNo + ".txt";
            invoiceInfo.HeaderInfo = headerInfor;
            try
            {
                List<USDFTransaction> transactionList = new List<USDFTransaction>();
                USDFTransaction transaction = null;
                string spName = "dbo.USExceGMSGetPayments";
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ChunkSize", System.Data.DbType.Int32, _chunkSize));
                DbDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    transaction = new USDFTransaction();
                    transaction.CreditorRef = Convert.ToString(dataReader["CustId"]); ;
                    transaction.CreditorExtenalId = Convert.ToString(dataReader["CreditorInkassoID"]);
                    transaction.ArItemNo = Convert.ToInt32(dataReader["ARItemNo"]);
                    if ((dataReader["DueDate"]) != DBNull.Value)
                    {
                        transaction.DueDate = (Convert.ToDateTime(dataReader["DueDate"])).ToString("yyyy/MM/dd");
                    }
                    transaction.Amount = Convert.ToDecimal(dataReader["Amount"]);
                    transaction.TransText = Convert.ToString(dataReader["Txt"]);
                    transaction.TransType = Convert.ToString(dataReader["ItemTypeId"]).Trim();
                    transaction.TransKid = Convert.ToString(dataReader["KID"]);
                    if ((dataReader["VoucherDate"]) != DBNull.Value)
                    {
                        transaction.VoucherDate = (Convert.ToDateTime(dataReader["VoucherDate"])).ToString("yyyy/MM/dd");
                    }
                    transaction.InvoiceNumber = Convert.ToString(dataReader["Ref"]);
                    transaction.InvoiceAmount = Convert.ToDouble(dataReader["InvoiceAmount"]);
                    transaction.TKid = Convert.ToString(dataReader["PaymentKID"]);
                    transactionList.Add(transaction);
                }
                invoiceInfo.Transactions = transactionList;
                return invoiceInfo;
            }
            catch (Exception)
            {
                
                throw;
            }
           
        }
       
    }
}
