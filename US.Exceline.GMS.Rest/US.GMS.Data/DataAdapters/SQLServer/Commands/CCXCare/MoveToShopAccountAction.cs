﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    public class MoveToShopAccountAction : USDBActionBase<bool>
    {
        private int _itemId = -1;
        private bool _isPaymentstatus = false;
        public MoveToShopAccountAction(int itemId, bool isPaymentStatus)
        {
            _itemId = itemId;
            _isPaymentstatus = isPaymentStatus;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSCCXMoveToShopAccount";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ItemNo", System.Data.DbType.Int32, _itemId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IspyamentStatus", System.Data.DbType.Boolean, _isPaymentstatus));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
