﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.USDF.Core.DomainObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    class GetInvoiceOrderlinesAction : USDBActionBase<List<USDFOrderLine>>
    {
        private int _arItemNo = -1;
        public GetInvoiceOrderlinesAction(int arItemNo)
        {
            _arItemNo = arItemNo;
        }

        protected override List<USDFOrderLine> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSCCXGetInvoiceOrderlines";
            DbDataReader reader = null;
            List<USDFOrderLine> orderlineList = new List<USDFOrderLine>();
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNo", System.Data.DbType.Int32, _arItemNo));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    USDFOrderLine orderline = new USDFOrderLine();
                    orderline.IText1 = reader["ArticleNo"].ToString();
                    orderline.IText2 = reader["Text"].ToString();
                    orderline.IItemprice = Convert.ToDouble(reader["UnitPrice"]);
                    orderline.INumberOfItems = Convert.ToInt32(reader["NoOfItems"]);
                    orderline.DiscountAmount = Convert.ToDouble(reader["Discount"]);
                    orderline.AmountOrderline = Convert.ToString(reader["Amount"]);
                    orderline.NameOnContract = Convert.ToString(reader["NameOnContract"]);
                    orderlineList.Add(orderline);
                }
                return orderlineList;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }
    }
}
