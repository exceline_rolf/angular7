﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    public class GetClaimExportStatusForMember : USDBActionBase<List<ImportStatusDC>>
    {
        private int _memberID = -1;
        private int _hit = 1;
        private DateTime? _transferedDate;
        private readonly string _user = string.Empty;
        private string _type = string.Empty;

        public GetClaimExportStatusForMember(int memberId, int hit, DateTime? transferDate, string type, string user)
        {
            _memberID = memberId;
            _hit = hit;
            _transferedDate = transferDate;
            _user = user;
            _type = type; 
        }

        protected override List<ImportStatusDC> Body(DbConnection connection)
        {
            var statusList = new List<ImportStatusDC>();
            const string spName = "USExceGMSCCXGetImportStatusDataForMember";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", DbType.Int32, _memberID));
                if(_transferedDate.HasValue)
                   command.Parameters.Add(DataAcessUtils.CreateParam("@TransferDate", System.Data.DbType.DateTime, _transferedDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Hit", System.Data.DbType.Int32, _hit));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Type", System.Data.DbType.String, _type));
                command.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _user));

                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ImportStatusDC status = new ImportStatusDC();
                    status.ItemNo = Convert.ToInt32(reader["ID"]);
                    if (reader["TransferedDate"] != DBNull.Value)
                    {
                        status.TransferedDate = Convert.ToDateTime(reader["TransferedDate"]);
                        status.TransferedTime = Convert.ToDateTime(reader["TransferedDate"]);
                    }
                    status.Category = Convert.ToString(reader["Category"]).Trim();
                    status.ContractNo = Convert.ToString(reader["ContractNo"]);
                    status.Ref = Convert.ToString(reader["InvoiceNumber"]);
                    status.KID = Convert.ToString(reader["KID"]);
                    status.AtgStatus = Convert.ToInt32(reader["ATGStatus"]);
                    status.Amount = Convert.ToDecimal(reader["Amount"]);
                    if(reader["UpdatedDate"] != DBNull.Value)
                        status.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"]);
                    status.TransferStatus = Convert.ToInt32(reader["TransferStatus"]);
                    status.Direction = Convert.ToInt32(reader["Direction"]);
                    statusList.Add(status);
                }
                return statusList;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
