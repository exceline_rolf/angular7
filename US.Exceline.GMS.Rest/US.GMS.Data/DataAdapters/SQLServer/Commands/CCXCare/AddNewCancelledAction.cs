﻿using System;
using System.Data;
using System.Data.Common;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    public class AddNewCancelledAction : USDBActionBase<int>
    {
        private USPNewOrCancelledFileRow _newCancelledFileRow = new USPNewOrCancelledFileRow();
        public AddNewCancelledAction(USPNewOrCancelledFileRow newCancelledFileRow)
        {
            _newCancelledFileRow = newCancelledFileRow;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int result = -1;
            try
            {
                string spName = "dbo.USExceGMSCCXAddNewCancelledStatus";
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@RecordType", DbType.String, _newCancelledFileRow.RecordType));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PID", DbType.String, _newCancelledFileRow.PID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BNumber", DbType.String, _newCancelledFileRow.BNumber));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CID", DbType.String, _newCancelledFileRow.CID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@KID", DbType.String, _newCancelledFileRow.KID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Status", DbType.Int32, _newCancelledFileRow.Status));
                command.Parameters.Add(DataAcessUtils.CreateParam("@StatusDate", DbType.String, _newCancelledFileRow.StatusDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ReferenceID", DbType.Int32, _newCancelledFileRow.RefereceId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BatchID", DbType.String, _newCancelledFileRow.BatchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@DataSource", DbType.String, _newCancelledFileRow.DataSource));
                //command.Parameters.Add(DataAcessUtils.CreateParam("@AccountNo", DbType.String, _newCancelledFileRow.AccountNo));
                command.ExecuteNonQuery();
                result = 1;
            }
            catch (Exception)
            {
                
                throw;
            }
           
            return result;
        }
    }
}
