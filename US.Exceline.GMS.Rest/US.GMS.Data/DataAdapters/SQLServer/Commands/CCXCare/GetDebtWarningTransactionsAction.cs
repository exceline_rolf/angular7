﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.USDF.Core.DomainObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    class GetDebtWarningTransactionsAction : USDBActionBase<List<USDFTransaction>>
    { private int _caseNo = -1;
        private string _creditorRef = string.Empty;
        private string _creditorExternalId = string.Empty;
        public GetDebtWarningTransactionsAction(int caseNo, string creditorRef, string CreditorExternalID)
        {
            _caseNo = caseNo;
            _creditorRef = creditorRef;
            _creditorExternalId = CreditorExternalID;
        }

        protected override List<USDFTransaction> Body(System.Data.Common.DbConnection connection)
        {
            List<USDFTransaction> transactionList = new List<USDFTransaction>();
            try
            {
                string storedProcedureName = "dbo.USExceGMSGetDebtWarningTrasactions";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CaseNo", DbType.Int32, _caseNo));
                DbDataReader dataReader = cmd.ExecuteReader();
                USDFTransaction transaction = null;

                while (dataReader.Read())
                {
                    transaction = new USDFTransaction();
                    
                    transaction.CreditorRef = _creditorRef;
                    transaction.CreditorExtenalId = _creditorExternalId;
                    transaction.ArItemNo = Convert.ToInt32(dataReader["ARItemNo"]);
                    if (dataReader["DueDate"] != DBNull.Value)
                         transaction.DueDate = (Convert.ToDateTime(dataReader["DueDate"])).ToString("yyyy/MM/dd");
                    transaction.Amount = Convert.ToDecimal(dataReader["Amount"]);
                    transaction.TransText = Convert.ToString(dataReader["Txt"]);
                    transaction.TransType = Convert.ToString(dataReader["ItemType"]).Trim();
                    transaction.TransKid = Convert.ToString(dataReader["KID"]);
                    if (dataReader["VoucherDate"] != DBNull.Value)
                       transaction.VoucherDate = (Convert.ToDateTime(dataReader["VoucherDate"])).ToString("yyyy/MM/dd");
                    transaction.ContractNo = Convert.ToString(dataReader["MemberContractNo"]);
                    if (dataReader["ContractExpireDate"] != DBNull.Value)
                       transaction.ContractExpire = (Convert.ToDateTime(dataReader["ContractExpireDate"])).ToString("yyyy/MM/dd");
                    transaction.ContractKID = Convert.ToString(dataReader["ContractKID"]);
                    transaction.InstallmentNo = Convert.ToString(dataReader["InstallmentNo"]);
                    transaction.InstallmentKID = Convert.ToString(dataReader["InstallmentKID"]);
                    transaction.Balance = Convert.ToDouble(dataReader["Balance"]);
                    transaction.InvoiceNumber = Convert.ToString(dataReader["InvoiceNo"]);
                    transaction.BranchNo = Convert.ToString(dataReader["BranchNo"]);
                    transaction.NameOnContract = Convert.ToString(dataReader["NameOnContract"]);
                    transaction.InvoiceAmount = Convert.ToDouble(dataReader["InvoiceAmount"]);
                    transaction.InvoiceRef = Convert.ToString(dataReader["InvoiceRef"]);
                    if(dataReader["PayDate"] != DBNull.Value)
                       transaction.PaidDate = (Convert.ToDateTime(dataReader["PayDate"])).ToString("yyyy/MM/dd");

                    transaction.CollectingStatus = Convert.ToString(dataReader["CollectingStatus"]);
                    transaction.CollectingText = Convert.ToString(dataReader["CollText"]);
                    transaction.ReminderFee = Convert.ToDouble(dataReader["ReminderFee"]);
                         

                    transactionList.Add(transaction);
                }

                return transactionList;
                }catch(Exception ex)
                {
                    throw ex;
                }
        }
    }
}
