﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.USDF.Core.DomainObjects;
using System.Linq;
using System.Configuration;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    public class GetAllInvoicesAction : USDBActionBase<USDFInvoiceInfo>
    {
        private int _chunkSize = 0;
        private string _gymCode = string.Empty;
        private int _creditorNo = -1;
        private int _sequenceNo = -1;
        List<string> _invoiceTypes = new List<string>();
        public GetAllInvoicesAction(int chunkSize,string gymCode,int creditorNo,int batchsequenceNo, List<string> invoiceTypes)
        {
            _chunkSize = chunkSize;
            _gymCode = gymCode;
            _creditorNo = creditorNo;
            _sequenceNo = batchsequenceNo;
            _invoiceTypes = invoiceTypes;
        }

        protected override USDFInvoiceInfo Body(System.Data.Common.DbConnection connection)
        {
            USDFInvoiceInfo invoiceInfo = new USDFInvoiceInfo();
            USDFHeaderInfo headerInfor = new USDFHeaderInfo();
            headerInfor.MessageID = Guid.NewGuid().ToString();
            headerInfor.FileCreator = "Exceline";
            headerInfor.FileName = _creditorNo + "_Claim_" + US.GMS.Core.Utils.Common.GetDate() + "_" + US.GMS.Core.Utils.Common.GetCurrentTime() + "_" + _chunkSize + "_" + _sequenceNo + ".txt";
            invoiceInfo.HeaderInfo = headerInfor;
            List<USDFCase> caseList = new List<USDFCase>();
            
            try
            {
                GetCaseDataAction caseAction = new GetCaseDataAction(_chunkSize, _creditorNo);
                caseList = caseAction.Execute(EnumDatabase.USP, _gymCode);
                foreach (USDFCase usdfCase in caseList)
                {
                    List<USDFTransaction> transactions = GetTransactions(usdfCase.CaseNo, usdfCase.CreditorRef, usdfCase.CreditorExternalID, _gymCode);
                    foreach (USDFTransaction transaction in transactions)
                    {
                        if (transaction.TransType.Equals("SP") || transaction.TransType.Equals("SPD"))
                        {
                            transaction.OrderLines.AddRange(GetOrderlines(transaction.ArItemNo, _gymCode));
                        }
                        else if (_invoiceTypes.Contains(transaction.TransType))
                        {
                            transaction.OrderLines.AddRange(GetInvoiceOrderlines(transaction.ArItemNo, _gymCode));
                        }
                    }
                    usdfCase.Transactions.AddRange(transactions);
                }
                invoiceInfo.Cases = caseList;

                UpdateInvoiceTransferStatusAction updateAction = new UpdateInvoiceTransferStatusAction(caseList, invoiceInfo.HeaderInfo.FileName);
                updateAction.Execute(EnumDatabase.USP,_gymCode);
                return invoiceInfo;
            }
            catch (Exception )
            {
                throw ;
            }
        }

        private List<USDFTransaction> GetTransactions(string caseNo,string creditorRef, string creditorInkassoId,string gymCode)
        {
            GetCaseTransactionsAction transactionsAction = new GetCaseTransactionsAction(Convert.ToInt32(caseNo), creditorRef, creditorInkassoId);
            return transactionsAction.Execute(EnumDatabase.USP, gymCode);
        }

        private List<USDFOrderLine> GetOrderlines(int arItemNo,string gymCode)
        {
            GetOrderlinesAction orderlineAction = new GetOrderlinesAction(arItemNo);
            return orderlineAction.Execute(EnumDatabase.USP, gymCode);
        }

        private List<USDFOrderLine> GetInvoiceOrderlines(int arItemNo, string gymCode)
        {
            GetInvoiceOrderlinesAction action = new GetInvoiceOrderlinesAction(arItemNo);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
    }
}
