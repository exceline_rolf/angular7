﻿using System;
using System.Data;
using System.Data.Common;
using US.Payment.Core.BusinessDomainObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    class AddDataImportReceiptAction : USDBActionBase<int>
    {
        DataImportReceiptDetail _importReceipt = new DataImportReceiptDetail();
        public AddDataImportReceiptAction(DataImportReceiptDetail importReceipt)
        {
            _importReceipt = importReceipt;
        }
        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int result = -1;
            try
            {   string spName = "dbo.USExceGMSCCXAddDataExportReceipt";
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@PID", DbType.String, _importReceipt.PID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CID", DbType.String, _importReceipt.CID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@KID", DbType.String, _importReceipt.KID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@TNumber", DbType.String, _importReceipt.TNumber));
                command.Parameters.Add(DataAcessUtils.CreateParam("@RecordType", DbType.String, _importReceipt.RecordType));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ReferenceID", DbType.String, _importReceipt.ReferenceId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BatchID", DbType.String, _importReceipt.BatchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@DataSource", DbType.String, _importReceipt.DataSource));              
                command.ExecuteNonQuery();
                result = 1;
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
    }
}
