﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    public class GetClaimImportStatusAction : USDBActionBase<List<ImportStatusDC>>
    {
        private DateTime _date;
        private int _hit = -1;
        private int _status = -1;
        private ImportStatusDC _advancedData = null;
        public GetClaimImportStatusAction(DateTime date, int hit, int status,ImportStatusDC advancedData )
        {
            _date = date;
            _hit = hit;
            _status = status;
            _advancedData = advancedData;
        }

        protected override List<ImportStatusDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ImportStatusDC> statusList = new List<ImportStatusDC>();
            string spName = "USExceGMSCCXGetImportStatusData";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@Date", System.Data.DbType.DateTime, _date));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Status", System.Data.DbType.Int32, _status));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Hit", System.Data.DbType.Int32, _hit));
                if (_advancedData == null)
                {
                    command.Parameters.Add(DataAcessUtils.CreateParam("@IsAdvancedSearch", System.Data.DbType.Boolean, false));
                }
                else
                {
                   command.Parameters.Add(DataAcessUtils.CreateParam("@IsAdvancedSearch", System.Data.DbType.Boolean, true));
                   if(!string.IsNullOrEmpty(_advancedData.Ref))
                       command.Parameters.Add(DataAcessUtils.CreateParam("@Ref", System.Data.DbType.String, _advancedData.Ref));

                   //if(!string.IsNullOrEmpty(_advancedData.PID))
                   //    command.Parameters.Add(DataAcessUtils.CreateParam("@PID", System.Data.DbType.String, _advancedData.PID));

                    if(!string.IsNullOrEmpty(_advancedData.KID))
                        command.Parameters.Add(DataAcessUtils.CreateParam("@KID", System.Data.DbType.String, _advancedData.KID));
                }

                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ImportStatusDC status = new ImportStatusDC();
                    status.ItemNo = Convert.ToInt32(reader["ID"]);
                    status.TransferedDate = Convert.ToDateTime(reader["TransferedDate"]);
                    status.TransferedTime = Convert.ToDateTime(reader["TransferedDate"]);
                    status.Category = Convert.ToString(reader["Category"]);
                    status.ContractNo = Convert.ToString(reader["ContractNo"]);
                    status.Ref = Convert.ToString(reader["InvoiceNumber"]);
                    status.KID = Convert.ToString(reader["KID"]);
                    status.AtgStatus = Convert.ToInt32(reader["ATGStatus"]);
                    status.Amount = Convert.ToDecimal(reader["Amount"]);
                    if (reader["UpdatedDate"] != DBNull.Value)
                        status.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"]);
                    status.TransferStatus = Convert.ToInt32(reader["TransferStatus"]);
                    statusList.Add(status);
                }
                return statusList;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
