﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.USDF.Core.DomainObjects;
using System.Data.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    public class GetOrderlinesAction : USDBActionBase<List<USDFOrderLine>>
    {
        private int _arItemNo = -1;
        public GetOrderlinesAction(int arItemNo)
        {
            _arItemNo = arItemNo;
        }

        protected override List<USDFOrderLine> Body(System.Data.Common.DbConnection connection)
        {
            List<USDFOrderLine> orderlineList = new List<USDFOrderLine>();
            string _spName = "USExceGMSGetOrderlines";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, _spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNo", System.Data.DbType.Int32, _arItemNo));
                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    USDFOrderLine orderline = new USDFOrderLine();
                    orderline.CreditorInkassoID = Convert.ToString(reader["CreditorInkassoID"]);
                    orderline.CusId = Convert.ToString(reader["CusId"]);
                    orderline.FirstName = Convert.ToString(reader["FirstName"]);
                    orderline.LastName = Convert.ToString(reader["LastName"]);
                    orderline.NameOnContract = Convert.ToString(reader["NameOnContract"]);
                    orderline.Address = Convert.ToString(reader["Address"]);
                    orderline.ZipCode = Convert.ToString(reader["ZipCode"]);
                    orderline.PostPlace = Convert.ToString(reader["PostPlace"]);
                    orderline.DiscountAmount = Convert.ToDouble(reader["Discount"]);
                    orderline.EmployeeNo = Convert.ToString(reader["EmployeeNo"]);
                    orderline.SponsorRef = Convert.ToString(reader["SponsorRef"]);
                    orderline.Visits = Convert.ToInt32(reader["Visits"]);
                    orderline.Amount = Convert.ToDouble(reader["Amount"]);
                    orderline.AmountOrderline = Convert.ToString(reader["AmountOrderline"]);
                    orderline.LastVisit = Convert.ToString(reader["LastVisit"]);
                    orderline.IText2 = Convert.ToString(reader["OrderText"]);
                    orderlineList.Add(orderline);
                }
                return orderlineList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
