﻿using System;
using System.Collections.Generic;
using US.USDF.Core.DomainObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    class GetDebtWarningInvoicesAction : USDBActionBase<USDFInvoiceInfo>
    {
        private int _chunkSize = 0;
        private string _gymCode = string.Empty;
        private int _creditorNo = -1;
        private int _sequenceNo = -1;

        public GetDebtWarningInvoicesAction(int chunkSize, string gymCode, int creditorNo, int batchsequenceNo)
        {
            _chunkSize = chunkSize;
            _gymCode = gymCode;
            _creditorNo = creditorNo;
            _sequenceNo = batchsequenceNo;
        }

        protected override USDFInvoiceInfo Body(System.Data.Common.DbConnection connection)
        {
            USDFInvoiceInfo invoiceInfo = new USDFInvoiceInfo();
            USDFHeaderInfo headerInfor = new USDFHeaderInfo();
            headerInfor.MessageID = Guid.NewGuid().ToString();
            headerInfor.FileCreator = "Exceline";
            headerInfor.FileName = _creditorNo + "_DebtWarning_" + US.GMS.Core.Utils.Common.GetDate() + "_" + US.GMS.Core.Utils.Common.GetCurrentTime() + "_" + _chunkSize + "_" + _sequenceNo + ".txt";
            invoiceInfo.HeaderInfo = headerInfor;
            List<USDFCase> caseList = new List<USDFCase>();

            try
            {
                GetDebtWarningCasesAction caseAction = new GetDebtWarningCasesAction(_chunkSize, _creditorNo);
                caseList = caseAction.Execute(EnumDatabase.USP, _gymCode);
                foreach (USDFCase usdfCase in caseList)
                {
                    List<USDFTransaction> transactions = GetTransactions(usdfCase.CaseNo, usdfCase.CreditorRef, usdfCase.CreditorExternalID, _gymCode);                   
                    usdfCase.Transactions.AddRange(transactions);
                }
                invoiceInfo.Cases = caseList;

                UpdateDWTransferStatusAction updateAction = new UpdateDWTransferStatusAction(caseList, invoiceInfo.HeaderInfo.FileName);
                updateAction.Execute(EnumDatabase.USP, _gymCode);
                return invoiceInfo;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private List<USDFTransaction> GetTransactions(string caseNo, string creditorRef, string creditorInkassoId, string gymCode)
        {
            GetDebtWarningTransactionsAction transactionsAction = new GetDebtWarningTransactionsAction(Convert.ToInt32(caseNo), creditorRef, creditorInkassoId);
            return transactionsAction.Execute(EnumDatabase.USP, gymCode);
        }      
    }
}
