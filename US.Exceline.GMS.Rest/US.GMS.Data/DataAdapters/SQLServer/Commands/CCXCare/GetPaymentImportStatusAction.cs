﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    public class GetPaymentImportStatusAction : USDBActionBase<PaymentImportStatusDC>
    {
        private int _hit = 1;
        private DateTime _regDate;
        public GetPaymentImportStatusAction(int hit, DateTime regDate)
        {
            _hit = hit;
            _regDate = regDate;
        }

        protected override PaymentImportStatusDC Body(System.Data.Common.DbConnection connection)
        {
            PaymentImportStatusDC status = new PaymentImportStatusDC();
            status.ErrorPaymentsList = new List<ErrorPaymentDC>();
            string spName = "USExceGMSCCXGetErrorPayments";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@Hit", System.Data.DbType.Int32, _hit));
                command.Parameters.Add(DataAcessUtils.CreateParam("@RegDate", System.Data.DbType.DateTime, _regDate));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ErrorPaymentDC erroprPayment = new ErrorPaymentDC();
                    erroprPayment.Id = Convert.ToInt32(reader["STATUSID"]);
                    erroprPayment.CreditorNo = Convert.ToString(reader["PID"]);
                    erroprPayment.Amount = Convert.ToDecimal(reader["AMOUNT"]);
                    erroprPayment.CustomerNo = Convert.ToString(reader["CID"]);
                    erroprPayment.GymName = Convert.ToString(reader["GYMNAME"]);
                    erroprPayment.KID = Convert.ToString(reader["KID"]);
                    erroprPayment.AddtoInvoice = Convert.ToBoolean(reader["MOVEINVOICE"]);
                    status.TotalImported = Convert.ToInt32(reader["TOTALCOUNT"]);
                    status.ErrorCount = Convert.ToInt32(reader["ERRORCOUNT"]);
                    if (erroprPayment.AddtoInvoice)
                        erroprPayment.AddToAccount = false;
                    else
                        erroprPayment.AddToAccount = true;

                    status.ErrorPaymentsList.Add(erroprPayment);
                }
                status.SuccessCount = status.TotalImported - status.ErrorCount;
                return status;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
