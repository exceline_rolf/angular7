﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    public class GeyGymCompaniesAction : USDBActionBase<List<GymCompanyDC>>
    {

        protected override List<GymCompanyDC> Body(DbConnection connection)
        {
            string spName = "ExceWorkStationGetCompanyDetails";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                DbDataReader reader = command.ExecuteReader();
                List<GymCompanyDC> companies = new List<GymCompanyDC>();
                while (reader.Read())
                {
                    GymCompanyDC company = new GymCompanyDC();
                    company.GymCode = Convert.ToString(reader["GymCode"]);
                    company.CompanyName = Convert.ToString(reader["CompanyName"]);
                    companies.Add(company);
                }
                return companies;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
