﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    class AddCountryDetailsAction : USDBActionBase<bool>
    {
        private CountryDC _country;

        public AddCountryDetailsAction(CountryDC country)
        {
            _country = country;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedure = "USExceGMSAddUpdateCountry";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _country.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@countryId", DbType.String, _country.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CountryCode", DbType.String, _country.CountryCode));

                SqlParameter output = new SqlParameter("@outId", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return true;
        }
    }
}
