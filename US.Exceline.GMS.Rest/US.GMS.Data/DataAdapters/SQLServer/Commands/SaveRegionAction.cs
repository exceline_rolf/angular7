﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "4/19/2012 3:10:37 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveRegionAction : USDBActionBase<int>
    {
        private RegionDC _region;
        private int _branchId;

        public SaveRegionAction(RegionDC regionDC, int branchId)
        {
            this._region = regionDC;
            this._branchId = branchId;
        }

        protected override int Body(DbConnection connection)
        {
            int regionId = -1;
            string storedProcedureName = "USExceGMSAddUpdateRegion";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);  
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@regionId", DbType.Int32, _region.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@code", DbType.String, _region.Code));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _region.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@description", DbType.String, _region.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@countryId", DbType.String, _region.CountryId));
                object obj = cmd.ExecuteScalar();
                regionId = Convert.ToInt32(obj);
            }
            catch 
            {
                regionId = -1;
                throw ;
            }
            return regionId;
        }
    }
}
