﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class ScheduleOrdersForInvoicingAction : USDBActionBase<bool>
    {
        private int _branchId = -1;
        public ScheduleOrdersForInvoicingAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string SpName = "USExceGMSScheduleForGeneratingInvoicesAutomatically";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, SpName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _branchId));

                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
