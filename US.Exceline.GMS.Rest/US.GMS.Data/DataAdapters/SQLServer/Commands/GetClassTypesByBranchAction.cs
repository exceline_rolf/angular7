﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Admin.ManageClassSettings;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetClassTypesByBranchAction : USDBActionBase<List<ExceClassTypeDC>>
    {
        int _branchId; 

        public GetClassTypesByBranchAction(int branchId)
        {
            _branchId = branchId;

        }

        protected override List<ExceClassTypeDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ExceClassTypeDC> _classTypeList = new List<ExceClassTypeDC>();
            string StoredProcedureName = "USExceGMSAdminGetClassTypesByBranch";
            DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));

            DbDataReader reader = cmd.ExecuteReader();

            try
            {
                while (reader.Read())
                {
                    ExceClassTypeDC classType = new ExceClassTypeDC();
                    classType.ClassKeywordList = new List<CategoryDC>();
                    classType.Id = Convert.ToInt32(reader["Id"]);
                    classType.Name = Convert.ToString(reader["Name"]);
                    classType.Code = Convert.ToInt32(reader["Code"]);
                    classType.ClassGroupId = Convert.ToInt32(reader["ClassGroupId"]);
                    classType.ClassGroup = Convert.ToString(reader["ClassGroup"]);
                    if (reader["ClassLevelId"] != DBNull.Value)
                        classType.ClassLevelId = Convert.ToInt32(reader["ClassLevelId"]);
                    if (reader["ClassLevel"] != DBNull.Value)
                        classType.ClassLevel = Convert.ToString(reader["ClassLevel"]);
                    if (reader["Colour"] != DBNull.Value)
                        classType.Colour = Convert.ToString(reader["Colour"]);
                    if (reader["Comment"] != DBNull.Value)
                        classType.Comment = Convert.ToString(reader["Comment"]);
                    if (reader["TimeDuration"] != DBNull.Value)
                        classType.TimeDuration = Convert.ToInt32(reader["TimeDuration"]);
                    if (reader["Obsolete"] != DBNull.Value)
                        classType.ObsoleteLevel = Convert.ToBoolean(reader["Obsolete"]);
                    if (reader["IsLocal"] != DBNull.Value)
                        classType.IsLocal = Convert.ToBoolean(reader["IsLocal"]);
                    classType.ClassKeywordString = Convert.ToString(reader["ClassKeywords"]);
                    if (reader["TimeCategory"] != DBNull.Value)
                        classType.TimeCategory = Convert.ToString(reader["TimeCategory"]);
                    if (reader["TimeCategoryId"] != DBNull.Value)
                        classType.TimeCategoryId = Convert.ToInt32(reader["TimeCategoryId"]);
                    classType.MaxNoOfBookings = Convert.ToInt32(reader["MaxNoOfBookings"]);
                    
                    _classTypeList.Add(classType);
                }
                reader.Close();
            }
            catch (Exception)
            {
                reader.Close();
            }

            try
            {
                string StoredProcedureName2 = "USExceGMSGetClassTypeClassCategory";
                foreach (ExceClassTypeDC classType in _classTypeList)
                {
                    DbCommand cmd2 = CreateCommand(CommandType.StoredProcedure, StoredProcedureName2);
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@ClassTypeId", DbType.Int32, classType.Id));
                    DbDataReader reader2 = cmd2.ExecuteReader();
                    while (reader2.Read())
                    {
                        CategoryDC category = new CategoryDC();
                        category.Id = Convert.ToInt32(reader2["ClassCategoryId"]);
                        category.Name = Convert.ToString(reader2["Name"]);
                        category.Code = Convert.ToString(reader2["Code"]);
                        classType.ClassCategoryList.Add(category);
                        classType.ClassCategoryStringList.Add(reader2["Name"].ToString());
                    }
                    reader2.Close();


                }
                foreach (ExceClassTypeDC classType in _classTypeList)
                {
                    foreach (CategoryDC cat in classType.ClassCategoryList)
                    {
                        classType.ClassCategoryString += cat.Name + ",";
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {

                string StoredProcedureName3 = "USExceGMSAdminGetClassTypeBranch";

                foreach (ExceClassTypeDC classType in _classTypeList)
                {
                    DbCommand cmd3 = CreateCommand(CommandType.StoredProcedure, StoredProcedureName3);
                    cmd3.Parameters.Add(DataAcessUtils.CreateParam("@ClassTypeId", DbType.Int32, classType.Id));

                    DbDataReader reader3 = cmd3.ExecuteReader();

                    while (reader3.Read())
                    {
                        ExcelineBranchDC branch = new ExcelineBranchDC();
                        branch.Id = Convert.ToInt32(reader3["BranchId"]);
                        branch.BranchName = Convert.ToString(reader3["FirstName"]);
                        classType.ExcelineBranchList.Add(branch);
                    }
                    reader3.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                string keyWordSpName = "USExceGMSAdminGetClassKeywords";
                foreach (ExceClassTypeDC classType in _classTypeList)
                {
                    DbCommand command = CreateCommand(CommandType.StoredProcedure, keyWordSpName);
                    command.Parameters.Add(DataAcessUtils.CreateParam("@ClassTypeId", DbType.Int32, classType.Id));
                    DbDataReader keyReader = command.ExecuteReader();
                    while (keyReader.Read())
                    {
                        CategoryDC keyword = new CategoryDC();
                        keyword.Id = Convert.ToInt32(keyReader["ID"]);
                        keyword.Name = Convert.ToString(keyReader["Name"]);
                        keyword.Code = Convert.ToString(keyReader["Code"]);
                        classType.ClassKeywordList.Add(keyword);
                    }
                    keyReader.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _classTypeList;
        }




    }
}
