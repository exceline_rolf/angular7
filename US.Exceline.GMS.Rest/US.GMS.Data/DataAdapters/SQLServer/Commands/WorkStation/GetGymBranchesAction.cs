﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.WorkStation
{
    public class GetGymBranchesAction : USDBActionBase<List<BranchDetailsDC>>
    {
        private string _gymCode = string.Empty;
        public GetGymBranchesAction(string gymCode)
        {
            _gymCode = gymCode;
        }

        protected override List<BranchDetailsDC> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceWorkStationGetBranchDetailsfirGymCode";
            DbDataReader reader = null;
            List<BranchDetailsDC> branchList = new List<BranchDetailsDC>();
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@GymCode", System.Data.DbType.String, _gymCode));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BranchDetailsDC branch = new BranchDetailsDC();
                    branch.BranchId = Convert.ToInt32(reader["BranchId"]);
                    branch.BranchName = branch.BranchId.ToString() +" - " + Convert.ToString(reader["BranchName"]);
                    branchList.Add(branch);
                }
                return branchList;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
