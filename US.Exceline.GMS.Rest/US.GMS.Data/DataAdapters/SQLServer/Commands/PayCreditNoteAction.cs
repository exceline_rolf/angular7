﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class PayCreditNoteAction : USDBActionBase<int>
    {
        private int _aritemNo = -1;
        private List<PayModeDC> _payModes;
        private int _memberId = -1;
        private int _shopAccountId = -1;
        private string _user = string.Empty;
        private int _salePointId = -1;
        private int _loggedBranchID = -1;

        public PayCreditNoteAction(int aritemNo, List<PayModeDC> paymodes, int memberId, int shopAccountId, string user, int salePointId, int loggedBranchiD)
        {
            _aritemNo = aritemNo;
            _payModes = paymodes;
            _memberId = memberId;
            _shopAccountId = shopAccountId;
            _user = user;
            _loggedBranchID = loggedBranchiD;
            _salePointId = salePointId;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spname = "USExceGMSSettleCreditNote";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, spname);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNo", DbType.Int32, _aritemNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ShopAccountId", DbType.Int32, _shopAccountId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@loggedBranchID", DbType.Int32, _loggedBranchID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SalePointID", DbType.Int32, _salePointId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@payModes", SqlDbType.Structured, GetDataTable()));
                cmd.ExecuteNonQuery();
                return 1;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private DataTable GetDataTable()
        {
            try
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add(new DataColumn("PaymodeId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("Amount", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("PaymentType", typeof(string)));
                dataTable.Columns.Add(new DataColumn("PaymentSourceId", typeof(string)));
                dataTable.Columns.Add(new DataColumn("PaymentSource", typeof(string)));
                dataTable.Columns.Add(new DataColumn("CreatedUser", typeof(string)));

                foreach (PayModeDC mode in _payModes)
                {
                    DataRow row = dataTable.NewRow();
                    row["PaymodeId"] = mode.PaymentTypeId;
                    row["Amount"] = mode.Amount;
                    row["PaymentType"] = GetTransType(mode.PaymentTypeCode);
                    row["PaymentSourceId"] = "";
                    row["PaymentSource"] = "";
                    row["CreatedUser"] = _user;
                    dataTable.Rows.Add(row);
                }
                return dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetTransType(string paymentType)
        {
            switch (paymentType)
            {
                case "CASH":
                    return "CSH";

                case "BANKTERMINAL":
                    return "TMN";

                case "OCR":
                    return "LOP";

                case "BANKSTATEMENT":
                    return "BS";

                case "DEBTCOLLECTION":
                    return "DC";

                case "ONACCOUNT":
                    return "OA";

                case "GIFTCARD":
                    return "GC";

                case "BANK":
                    return "BNK";

                case "PREPAIDBALANCE":
                    return "PB";

            }
            return string.Empty;
        }

    }

 
}
