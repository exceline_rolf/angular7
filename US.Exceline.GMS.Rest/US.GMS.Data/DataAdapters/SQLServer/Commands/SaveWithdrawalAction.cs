﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveWithdrawalAction : USDBActionBase<int>
    {

        private WithdrawalDC _withDrawal = new WithdrawalDC();
        private string _user = string.Empty;
        private int _branchId = -1;


        public SaveWithdrawalAction(int branchId, WithdrawalDC withDrawal, string user)
        {
            _withDrawal = withDrawal;
            _branchId = branchId;
            _user = user;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int id = -1;
            try
            {
                string storedProcedure = "USExceGMSShopSaveWithdrawal";

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _withDrawal.PaymentAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, _withDrawal.Comment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, _withDrawal.WithdrawalType.ToString()));

                if (_withDrawal.WithdrawalType == WithdrawalTypes.CASHWITHDRAWAL)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _withDrawal.MemberId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRole", DbType.String, "MEM"));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@withdrwalFrom", DbType.String, _withDrawal.WithdrawalFrom));
                    if (_withDrawal.PaymentType != null)
                    {
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@paymentMode", DbType.String, _withDrawal.PaymentType.Code));
                    }

                }

                else if (_withDrawal.WithdrawalType == WithdrawalTypes.PETTYCASH)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _withDrawal.EmployeeId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRole", DbType.String, "EMP"));

                }
                if (_withDrawal.SalePointId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@salePointId", DbType.Int32, _withDrawal.SalePointId));
                }

                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 50;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                id = Convert.ToInt32(output.Value);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return id;
        }

        public int RunOnTransaction(DbTransaction trasaction)
        {
            int id = -1;
            try
            {
                string storedProcedure = "USExceGMSShopSaveWithdrawal";

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Connection = trasaction.Connection;
                cmd.Transaction = trasaction;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _withDrawal.PaymentAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, _withDrawal.Comment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, _withDrawal.WithdrawalType.ToString()));

                if (_withDrawal.WithdrawalType == WithdrawalTypes.CASHWITHDRAWAL)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _withDrawal.MemberId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRole", DbType.String, "MEM"));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@withdrwalFrom", DbType.String, _withDrawal.WithdrawalFrom));
                    if (_withDrawal.PaymentType != null)
                    {
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@paymentMode", DbType.String, _withDrawal.PaymentType.Code));
                    }

                }

                else if (_withDrawal.WithdrawalType == WithdrawalTypes.PETTYCASH)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _withDrawal.EmployeeId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRole", DbType.String, "EMP"));

                }
                if (_withDrawal.SalePointId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@salePointId", DbType.Int32, _withDrawal.SalePointId));
                }

                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 50;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                id = Convert.ToInt32(output.Value);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return id;
        }
    }
}
