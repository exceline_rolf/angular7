﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    internal class DeleteGATIntegrationSettingByIdAction : USDBActionBase<bool>
    {
        private readonly int _settingId = -1;

        public DeleteGATIntegrationSettingByIdAction(int id)
        {
            _settingId = id;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSAdminDeleteIntegrationSetting";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.String, _settingId));
                DbParameter outputPara = new SqlParameter();
                outputPara.DbType = DbType.Int32;
                outputPara.ParameterName = "@Result";
                outputPara.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputPara);
                cmd.ExecuteNonQuery();
                var result = Convert.ToInt32(outputPara.Value);
                if (result > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}