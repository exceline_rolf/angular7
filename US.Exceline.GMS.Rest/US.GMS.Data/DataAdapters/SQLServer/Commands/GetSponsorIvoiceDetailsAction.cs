﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetSponsorIvoiceDetailsAction : USDBActionBase<List<SponsorshipItemDC>>
    {
        private int _arItemNo = -1;
        public GetSponsorIvoiceDetailsAction(int arItemNo)
        {
            _arItemNo = arItemNo;
        }


        protected override List<SponsorshipItemDC> Body(System.Data.Common.DbConnection connection)
        {
            string _storedProcedureName = "USExceGMSManageMembershipGetSponsorInvoiceDetails";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, _storedProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@arItemNo", System.Data.DbType.Int32, _arItemNo));
                DbDataReader reader = command.ExecuteReader();
                List<SponsorshipItemDC> spInstallmentItemList = new List<SponsorshipItemDC>();
                while (reader.Read())
                {
                    SponsorshipItemDC spItem = new SponsorshipItemDC();
                    if (reader["CusId"] != DBNull.Value)
                        spItem.CusId = Convert.ToString(reader["CusId"]);
                    if (reader["LastName"] != DBNull.Value)
                        spItem.LastName = Convert.ToString(reader["LastName"]);
                    if (reader["FirstName"] != DBNull.Value)
                        spItem.FirstName = Convert.ToString(reader["FirstName"]);
                    spItem.Name = spItem.FirstName + spItem.LastName;
                    if (reader["Amount"] != DBNull.Value)
                        spItem.Amount = Convert.ToDecimal(reader["Amount"]);
                    if (reader["Visits"] != DBNull.Value)
                        spItem.NoOfVisit = Convert.ToInt32(reader["Visits"]);
                    spInstallmentItemList.Add(spItem);
                }

                reader.Close();
                return spInstallmentItemList;
            }
            catch
            {
                throw;
            }
        }
    }
}
