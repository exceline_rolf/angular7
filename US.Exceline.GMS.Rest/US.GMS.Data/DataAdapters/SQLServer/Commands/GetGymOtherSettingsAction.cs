﻿using System;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymOtherSettingsAction : USDBActionBase<List<GymOtherSettingsDC>>
    {
        private readonly int _branchId = -1;
        private string _userName;

        public GetGymOtherSettingsAction(int branchId, string userName)
        {
            _branchId = branchId;
            _userName = userName;
        }

        protected override List<GymOtherSettingsDC> Body(DbConnection connection)
        {
            List<GymOtherSettingsDC> gymOtherSettingList = new List<GymOtherSettingsDC>();
            
            const string storedProcedureName = "USExceGMSGetGymOtherSetting";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _userName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.String, _branchId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    GymOtherSettingsDC gymOtherSetting = new GymOtherSettingsDC();

                    gymOtherSetting.Id                          = Convert.ToInt32(reader["Id"]);
                    gymOtherSetting.BranchId                    = Convert.ToInt32(reader["BranchId"]);
                    gymOtherSetting.BranchName                  = Convert.ToString(reader["BranchName"]);
                    gymOtherSetting.Region                      = Convert.ToString(reader["region"]);
                    gymOtherSetting.IsExpress                   = Convert.ToBoolean(reader["IsExpressGym"]);
                    gymOtherSetting.TimeOutAfter                = Convert.ToInt32(reader["TimeOutAfter"]);
                    gymOtherSetting.PhoneCode                   = Convert.ToString(reader["PhoneCode"]);
                    gymOtherSetting.IsShopAvailable             = Convert.ToBoolean(reader["IsShopAvailable"]);
                    gymOtherSetting.IsAntiDopingRequired        = Convert.ToBoolean(reader["IsAntiDopingRequired"]);
                    gymOtherSetting.CategoryFollowUpNoVisits    = Convert.ToBoolean(reader["CategoryFollowUpNoVisits"]);
                    gymOtherSetting.FollowUpNewPerson           = Convert.ToBoolean(reader["FollowUpNewPerson"]);
                    gymOtherSetting.IsPrintATGAuthorization     = Convert.ToBoolean(reader["IsPrintATGAuthorization"]);
                    gymOtherSetting.IsFreezeConfirmationPopUp   = Convert.ToBoolean(reader["IsFreezeConfirmationPopUp"]);
                    gymOtherSetting.IsLoginShop                 = Convert.ToBoolean(reader["IsLoginShop"]);
                    gymOtherSetting.IsLogoutFromShopAfterSale   = Convert.ToBoolean(reader["OtherIsLogoutFromShopAfterSale"]);
                    gymOtherSetting.Gmt                         = Convert.ToDecimal(reader["Gmt"]);
                    gymOtherSetting.IsBookingBackDate           = Convert.ToBoolean(reader["IsBookingBackDate"]);
                    gymOtherSetting.IsPrieviewWhenPrint         = Convert.ToBoolean(reader["IsPrieviewWhenPrint"]);
                    gymOtherSetting.AnonymizingTimePeriod       = Convert.ToInt32(reader["AnonymizingTimeDuration"]);                    
                    gymOtherSetting.IsCalendarPriority          = Convert.ToBoolean(reader["IsCalendarPriortiy"]);
                    gymOtherSetting.IsIBookingIntegrated        = Convert.ToBoolean(reader["IBookingIntegrated"]);
                    gymOtherSetting.IsValidateShopGymId         = Convert.ToBoolean(reader["OtherIsValidateShopGymId"]);
                    gymOtherSetting.IsUsingIntroducedBy         = Convert.ToBoolean(reader["IsUsingIntroducedBy"]);
                    gymOtherSetting.SortBranchesById            = Convert.ToBoolean(reader["SortBranchesById"]);
                    gymOtherSetting.IsUsingGantner = Convert.ToBoolean(reader["IsUsingGantner"]);
                    gymOtherSettingList.Add(gymOtherSetting);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return gymOtherSettingList;
        }
    }
}



