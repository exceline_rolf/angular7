﻿using System;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveGymRequiredSettingAction : USDBActionBase<bool>
    {
        private GymRequiredSettingsDC _gymRequiredSettings = new GymRequiredSettingsDC();
        private int _branchId;
        private bool _returnValue;

        public SaveGymRequiredSettingAction(int branchId, GymRequiredSettingsDC gymRequiredSettings)
        {
            _gymRequiredSettings = gymRequiredSettings;
            _branchId = branchId;
        }

        protected override bool Body(DbConnection connection)
        {
            string storedProcedure = "USExceGMSSaveGymRequiredSetting";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _gymRequiredSettings.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberRequired", DbType.Boolean, _gymRequiredSettings.MemberRequired));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MobileRequired", DbType.Boolean, _gymRequiredSettings.MobileRequired));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AddressRequired", DbType.Boolean, _gymRequiredSettings.AddressRequired));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ZipCodeRequired", DbType.Boolean, _gymRequiredSettings.ZipCodeRequired));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.ExecuteNonQuery();
                _returnValue = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _returnValue;
        }
    }
}
