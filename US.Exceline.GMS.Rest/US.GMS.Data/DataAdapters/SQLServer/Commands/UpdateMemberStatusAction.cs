﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateMemberStatusAction : USDBActionBase<int>
    {
        private int _branchId = -1;
        public UpdateMemberStatusAction(int branchID)
        {
            _branchId = branchID;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string _spName = "USExceGMSManageMembershipUpdateMemberStatus";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, _spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                command.ExecuteNonQuery();
                return 1;
            }
            catch
            {
                throw;
            }
        }
    }
}
