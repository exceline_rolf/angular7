﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class CheckEmployeeByEntNoAction : USDBActionBase<int>
    {

        private int _entityNO;

        public CheckEmployeeByEntNoAction(int entityNo)
        {
            this._entityNO = entityNo;
        }

        protected override int Body(DbConnection connection)
        {
            int result = -1;
            string storedProcedureName = "USExceGMSCheckEmployeeByEntNo";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _entityNO));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result = Convert.ToInt32(reader["EntNo"]);
                }


                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
