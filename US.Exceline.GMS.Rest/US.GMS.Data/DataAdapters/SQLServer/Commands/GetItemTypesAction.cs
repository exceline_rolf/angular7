﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetItemTypesAction : USDBActionBase<int>
    {
        private string _typeName = string.Empty;
        public GetItemTypesAction(string typeName)
        {
            _typeName = typeName;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string SpName = "USExceGMSGetItemTypeId";
            int _typeId = -1;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure,SpName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@TypeName", System.Data.DbType.Int32, _typeName));

                _typeId = Convert.ToInt32(command.ExecuteScalar());
                return _typeId;
            }
            catch (Exception )
            {
                return -1;
            }
        }
    }
}
