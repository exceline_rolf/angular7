﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US_DataAccess;
using US.Exceline.GMS.Modules.Economy.API;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageShop;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    class RegisterInvoiceAction : USDBActionBase<SaleResultDC>
    {
        private int _invoiceBranchId = -1;
        private int _memberbranchID = -1;
        private string _user = string.Empty;
        private InstallmentDC _installment = null;
        private string _invoiceType = string.Empty;
        private bool _isSMSInvoice = false;

        public RegisterInvoiceAction(int invoiceBranchId, string user, InstallmentDC installment, PaymentDetailDC paymentDetail, string invoiceType, string gymCode, int memberBranchID)
        {
            _invoiceBranchId = invoiceBranchId;
            _memberbranchID = memberBranchID;
            _user = user;
            _installment = installment;
            _invoiceType = invoiceType;

            if (_invoiceType == "SMS" || _invoiceType == "EMAIL")
                _isSMSInvoice = true;
        }

        protected override SaleResultDC Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSGenerateInvoiceForOrder";
           SaleResultDC saleResult = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentID", System.Data.DbType.Int32, _installment.Id));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsInvoiceFeeAdded", System.Data.DbType.Boolean, _installment.IsInvoiceFeeAdded));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceType", System.Data.DbType.String, _invoiceType));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsShopInvoice", System.Data.DbType.Boolean, false));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SMSInvoice", System.Data.DbType.Boolean, _isSMSInvoice));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberBranchID", System.Data.DbType.Int32, _memberbranchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoicBranchID", System.Data.DbType.Int32, _invoiceBranchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SettleWithPrepaid", System.Data.DbType.Boolean, true));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SponsoredAmount", System.Data.DbType.Decimal, _installment.SponsoredAmount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Discount", System.Data.DbType.Decimal, _installment.Discount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsSponsored", System.Data.DbType.Boolean, _installment.IsSponsored));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", System.Data.DbType.Int32, _installment.MemberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@NofoVisists", System.Data.DbType.Int32, _installment.NoOfVisits));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentText", System.Data.DbType.String, _installment.Text));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberContractID", System.Data.DbType.Int32, _installment.MemberContractId));

                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    saleResult = new SaleResultDC();
                    saleResult.InvoiceNo = Convert.ToString(reader["Ref"]);
                    saleResult.AritemNo = Convert.ToInt32(reader["Aritemno"]);
                }
                saleResult.SaleStatus = SaleErrors.SUCCESS;
                return saleResult;
            }
            catch (Exception)
            {
                saleResult = new SaleResultDC();
                saleResult.AritemNo = -1;
                saleResult.InvoiceNo = string.Empty;
                saleResult.SaleStatus = SaleErrors.ERROR;
                return saleResult;
            }
        }

        //private SaleResultDC GenerateInvoice(DbTransaction transaction)
        //{
        //    int generatedArItemNo = -1;
        //    SaleResultDC saleResult = new SaleResultDC();
        //    try
        //    {
        //        List<int> orderlineList = new List<int>();
        //        List<string> orderlineErrors = new List<string>();
        //        GetClaimInfoForInstallmentPaymentAction action = new GetClaimInfoForInstallmentPaymentAction(_installment.MemberId, _invoiceBranchId, _installment.Id, _installment.InstallmentNo, _installment.MemberContractId);
        //        IUSPClaim claim = action.RunOnTransaction(transaction);
        //        claim.InvoiceType = GetInvoiceType(claim.InvoiceTypeId);
        //        claim.KID = _installment.KID;
        //        claim.Amount = _payementDetail.InvoiceAmount;
        //        claim.BranchNumber = _invoiceBranchId.ToString();
        //        claim.InvoicedDate = DateTime.Now.ToString("MM-dd-yyyy");
        //        claim.DueDate = DateTime.Now.AddDays(14).ToString("MM-dd-yyyy");
        //        claim.DueBalance = "0";
        //        claim.Balance = "0";
        //        if (claim != null)
        //        {
        //            if (_installment.IsInvoiceFeeAdded)
        //            {
        //                GetInvoiceFeeAction invoieFeeAction = new GetInvoiceFeeAction(_invoiceBranchId, _invoceiType);
        //                CreditorOrderLine orderline = invoieFeeAction.RunOnTransaction(transaction);

        //                if (orderline != null)
        //                {
        //                    orderline.BranchNo = _invoiceBranchId;
        //                    orderline.CoRelationId = Convert.ToString(_installment.Id);
        //                    orderline.CreatedUser = _user;
        //                    orderline.CreditorInvoiceType = CreditorInvoiceType.CREDITOR;
        //                    orderline.CreditorNo = Convert.ToInt32(claim.Creditor.CreditorInkassoID);
        //                    orderline.CreditorName = claim.Creditor.CreditorName;
        //                    orderline.CustomerNo = claim.Debtor.DebtorEntityID;
        //                    orderline.DebtorEntNo = claim.Debtor.DebtorEntityID;
        //                    orderline.Discount = 0;
        //                    orderline.DueDate = _installment.DueDate;
        //                    orderline.InvoiceNo = _installment.InvoiceNo;
        //                    orderline.NoOfItems = 1;
        //                    orderline.OrderType = "E";
        //                    orderline.RegDate = DateTime.Now;
        //                    orderline.ModifiedUser = _user;
        //                    orderline.IsInvocieFee = true;

        //                    OperationResult<int> oResult = Invoice.AddCreditorOrderLine(orderline, _user, transaction);
        //                    if (oResult.ErrorOccured)
        //                    {
        //                        foreach (NotificationMessage message in oResult.Notifications)
        //                        {
        //                            orderlineErrors.Add(message.Message + "Installment : " + _installment.Id.ToString() + " article no : " + orderline.ArticleNo);
        //                        }

        //                        transaction.Rollback();
        //                        saleResult.AritemNo = -1;
        //                        saleResult.InvoiceNo = string.Empty;
        //                        saleResult.SaleStatus = SaleErrors.ORDERLINEADDERROR;
        //                        return saleResult;
        //                    }
        //                    else
        //                    {
        //                        orderlineList.Add(oResult.OperationReturnValue);
        //                        claim.Amount += orderline.Amount;
        //                    }
        //                }
        //            }

        //            foreach (ContractItemDC contractItem in _installment.AddOnList)
        //            {
        //                CreditorOrderLine orderline = new CreditorOrderLine();
        //                orderline.Amount = contractItem.Price;
        //                orderline.ArticleNo = Convert.ToString(contractItem.ArticleId);
        //                orderline.ArticleText = contractItem.ItemName;
        //                orderline.Balance = contractItem.Price;
        //                orderline.BranchNo = _invoiceBranchId;
        //                orderline.CoRelationId = Convert.ToString(_installment.Id);
        //                orderline.CreatedUser = _user;
        //                orderline.CreditorInvoiceType = CreditorInvoiceType.CREDITOR;
        //                if (claim.Creditor.CreditorInkassoID.Trim().Length > 0)
        //                    orderline.CreditorNo = Convert.ToInt32(claim.Creditor.CreditorInkassoID);
        //                orderline.CreditorName = claim.Creditor.CreditorName;
        //                orderline.CustomerNo = claim.Debtor.DebtorEntityID;
        //                orderline.DebtorEntNo = claim.Debtor.DebtorEntityID;
        //                orderline.Discount = Convert.ToDecimal(contractItem.Discount);
        //                orderline.DueDate = _installment.DueDate;
        //                orderline.InvoiceNo = _installment.InvoiceNo;
        //                orderline.NoOfItems = contractItem.Quantity;
        //                orderline.OrderType = "E";
        //                orderline.RegDate = DateTime.Now;
        //                orderline.ModifiedUser = _user;
        //                orderline.Text = contractItem.ItemName;
        //                orderline.UnitPrice = contractItem.UnitPrice;
        //                orderline.IsInvocieFee = false;
        //                orderline.ExpiryDate = contractItem.ExpiryDate;
        //                orderline.VoucherNo = contractItem.VoucherNo;
        //                orderline.Description = contractItem.Description;
        //                OperationResult<int> oResult = Invoice.AddCreditorOrderLine(orderline, _user, transaction);
        //                if (oResult.ErrorOccured)
        //                {
        //                    foreach (NotificationMessage message in oResult.Notifications)
        //                    {
        //                        orderlineErrors.Add(message.Message + "Installment : " + _installment.Id.ToString() + " article no : " + orderline.ArticleNo);
        //                    }

        //                    transaction.Rollback();
        //                    saleResult.AritemNo = -1;
        //                    saleResult.InvoiceNo = string.Empty;
        //                    saleResult.SaleStatus = SaleErrors.ORDERLINEADDERROR;
        //                    return saleResult;
        //                }
        //                else
        //                {
        //                    orderlineList.Add(oResult.OperationReturnValue);
        //                }
        //            }

        //            if (orderlineList.Count > 0)
        //            {
        //                OperationResult<List<IUSPClaim>> Iresult = Invoice.SelectedOrderLineInvoiceGenerator(orderlineList, string.Empty, "E", _user, transaction, _installment.Id, Convert.ToInt32(claim.Creditor.CreditorInkassoID), _gymCode);
        //                if (Iresult.ErrorOccured)
        //                {
        //                    transaction.Rollback();
        //                    saleResult.AritemNo = -1;
        //                    saleResult.InvoiceNo = string.Empty;
        //                    saleResult.SaleStatus = SaleErrors.INVOICEADDINGERROR;
        //                    return saleResult;
        //                }
        //                else
        //                {
        //                    if (Iresult.OperationReturnValue.Count > 0)
        //                    {
        //                        int invoiceType = 0;
        //                        switch (_invoceiType)
        //                        {
        //                            case "PRINT":
        //                                invoiceType = 4;
        //                                break;

        //                            case "SMS":
        //                                invoiceType = 2;
        //                                break;

        //                            case "EMAIL":
        //                                invoiceType = 3;
        //                                break;
        //                        }

        //                        UpdateOrderInvoiceTypeAction action1 = new UpdateOrderInvoiceTypeAction(_installment.Id, _installment.SponsoredAmount, _installment.Discount, invoiceType, Iresult.OperationReturnValue[0].ARItemNo);
        //                        if (action1.RunOnTransaction(transaction))
        //                        {
        //                            generatedArItemNo = Iresult.OperationReturnValue[0].ARItemNo;
        //                            if (_memberbranchID == _invoiceBranchId)
        //                            {
        //                                SettleInvoicesWithPrepaidBalanceAction settleAction = new SettleInvoicesWithPrepaidBalanceAction(generatedArItemNo, _user);
        //                                settleAction.RunOnTransaction(transaction);
        //                            }

        //                            transaction.Commit();
        //                            saleResult.AritemNo = generatedArItemNo;
        //                            saleResult.InvoiceNo = Iresult.OperationReturnValue[0].InvoiceNumber;

        //                            UpdateOrderInvoicedStatusAction updateaction = new UpdateOrderInvoicedStatusAction(_installment.Id);
        //                            updateaction.Execute(EnumDatabase.Exceline, _gymCode);

        //                            saleResult.SaleStatus = SaleErrors.SUCCESS;


        //                            return saleResult;
        //                        }
        //                        else
        //                        {
        //                            transaction.Rollback();
        //                            saleResult.AritemNo = -1;
        //                            saleResult.InvoiceNo = string.Empty;
        //                            saleResult.SaleStatus = SaleErrors.ERROR;
        //                            return saleResult;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        transaction.Rollback();
        //                        saleResult.AritemNo = -1;
        //                        saleResult.InvoiceNo = string.Empty;
        //                        saleResult.SaleStatus = SaleErrors.INVOICEADDINGERROR;
        //                        return saleResult;
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                transaction.Rollback();
        //                saleResult.AritemNo = -1;
        //                saleResult.InvoiceNo = string.Empty;
        //                saleResult.SaleStatus = SaleErrors.ORDERLINEADDERROR;
        //                return saleResult;
        //            }
        //        }
        //        else
        //        {
        //            transaction.Rollback();
        //            saleResult.AritemNo = -1;
        //            saleResult.InvoiceNo = string.Empty;
        //            saleResult.SaleStatus = SaleErrors.ERROR;
        //            return saleResult;
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //private InvoiceTypes GetInvoiceType(int itemType)
        //{
        //    switch (itemType)
        //    {
        //        case 1:
        //            return InvoiceTypes.DirectDeduct;

        //        case 2:
        //            return InvoiceTypes.InvoiceForPrint;

        //        case 3:
        //            return InvoiceTypes.Invoice;

        //        case 4:
        //            return InvoiceTypes.DebtWarning;

        //        case 7:
        //            return InvoiceTypes.Sponser;

        //        case 14:
        //            return InvoiceTypes.PaymentDocumentForPrint;

        //        default:
        //            return InvoiceTypes.Invoice;
        //    }
        //}

    }
}

