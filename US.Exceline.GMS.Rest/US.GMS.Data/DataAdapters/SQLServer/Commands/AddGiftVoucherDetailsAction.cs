﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class AddGiftVoucherDetailsAction : USDBActionBase<int>
    {
        private ShopSalesItemDC _salesItem;
        private int _aritemno = -1;
        private string _user = string.Empty;
        private int _memberID = -1;
        private int _branchID = -1;
        public AddGiftVoucherDetailsAction(ShopSalesItemDC saledItems, int aritemno, string user, int memberId, int branchID)
        {
           _salesItem = saledItems;
            _aritemno = aritemno;
            _user = user;
            _memberID = memberId;
            _branchID = branchID;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            var voucherId = -1;
            const string storedProcedureName = "USExceGMSSaveGiftVoucherDetails";

            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArticleID", DbType.Int32, _salesItem.ArticleId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VoucherNumber", DbType.String, _salesItem.VoucherNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VoucherPrice", DbType.Decimal, _salesItem.DefaultAmount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@DiscountPrice", DbType.Decimal, _salesItem.Discount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ExpiryDate", DbType.DateTime, _salesItem.ExpiryDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceArItemNO", DbType.Int32, _aritemno));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.Int32, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchID));

                var obj = command.ExecuteScalar();
                voucherId = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return voucherId;
        }

        public void RunOnTransaction(DbTransaction transacton)
        {
            string spName = "USExceGMSSaveGiftVoucherDetails";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Connection = transacton.Connection;
                cmd.Transaction = transacton;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleID", DbType.Int32, _salesItem.ArticleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VoucherNumber", DbType.String, _salesItem.VoucherNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VoucherPrice", DbType.Decimal, _salesItem.DefaultAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DiscountPrice", DbType.Decimal, _salesItem.Discount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ExpiryDate", DbType.DateTime, _salesItem.ExpiryDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceArItemNO", DbType.Int32, _aritemno));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchID));

                cmd.ExecuteScalar();
            }
            catch (Exception)
            {
                throw;
            }
          
        }
    }
}
