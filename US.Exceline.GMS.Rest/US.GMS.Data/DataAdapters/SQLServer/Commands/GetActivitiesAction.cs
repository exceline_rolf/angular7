﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/18/2012 09:26:28
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetActivitiesAction : USDBActionBase<List<ActivityDC>>
    {
        private int _branchId = -1;
        public GetActivitiesAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<ActivityDC> Body(DbConnection connection)
        {
            List<ActivityDC> activityList = new List<ActivityDC>();
            string storedProcedure = "USExceGMSAdminGetActivities ";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ActivityDC activity = new ActivityDC();

                    activity.Id = Convert.ToInt32(reader["ID"].ToString());
                    activity.Name = reader["Name"].ToString();
                    activity.Code = reader["Code"].ToString();
                    activity.BranchId = Convert.ToInt32(reader["BranchId"].ToString());
                    activity.IsActive = Convert.ToBoolean(reader["IsActive"].ToString());

                    ActivitySettingDC activitySetting = new ActivitySettingDC();
                    activitySetting.ActivityId = activity.Id;
                    activitySetting.ActivityName = activity.Name;
                    activitySetting.IsContractTimeLimited = Convert.ToBoolean(reader["IsContractTimeLimited"]);
                    activitySetting.IsPunchCardLimited = Convert.ToBoolean(reader["IsPunchCardLimited"]);
                    activitySetting.IsTrial = Convert.ToBoolean(reader["IsTrial"]);
                    activitySetting.IsSMSRemindered = Convert.ToBoolean(reader["IsSMSRemindered"]);
                    activitySetting.DaysInAdvanceForBooking = Convert.ToInt32(reader["DaysInAdvanceForBooking"]);
                    activitySetting.IsContractBooking = Convert.ToBoolean(reader["IsContractBooking"]);
                    activitySetting.IsBookViaIbooking = Convert.ToBoolean(reader["BookVIAIBooking"]);
                    activitySetting.IsBookingActivity = Convert.ToBoolean(reader["IsBookingActivity"]);
                    activitySetting.IsBasicActivity = Convert.ToBoolean(reader["IsBasicActivity"]);
                    activitySetting.BranchId = _branchId;
                    activity.ActivitySetting = activitySetting;

                    activityList.Add(activity);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return activityList;
        }
    }
}
