﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymIntegrationSettingsAction : USDBActionBase<List<GymIntegrationSettingsDC>>
    {
        private readonly int _branchId = -1;
        private string _gymCode = string.Empty;

        public GetGymIntegrationSettingsAction(int branchId, string gymCode)
        {
            _branchId = branchId;
            _gymCode = gymCode;
        }

        protected override List<GymIntegrationSettingsDC> Body(DbConnection connection)
        {
            var integrationSettingsList = new List<GymIntegrationSettingsDC>();
            const string spName = "dbo.USExceGMSAdminGetGymGATIntegrationSettings";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var integrationSetting = new GymIntegrationSettingsDC();
                    integrationSetting.Id = Convert.ToInt32(reader["ID"]);
                    integrationSetting.BranchId = Convert.ToInt32(reader["BranchId"]);
                    integrationSetting.TerminalName = (reader["TerminalName"] != DBNull.Value) ? Convert.ToString(reader["TerminalName"]) : "";
                    integrationSetting.IsOnline = (reader["IsOnline"] != DBNull.Value) && Convert.ToBoolean(reader["IsOnline"]);
                    integrationSetting.IsSecondIdentification = (reader["IsSecondIdentification"] != DBNull.Value) && Convert.ToBoolean(reader["IsSecondIdentification"]);
                    integrationSetting.Location = (reader["Location"] != DBNull.Value) ? Convert.ToString(reader["Location"]) : "";
                    integrationSetting.Port = (reader["Port"] != DBNull.Value) ? Convert.ToInt32(reader["Port"]) : -1;
                    integrationSetting.TerminalType = new TerminalType
                        {
                                    TypeId = (reader["TypeID"] != DBNull.Value) ? Convert.ToString(reader["TypeID"]) : "",
                                    TypeName = (reader["TypeName"] != DBNull.Value) ? Convert.ToString(reader["TypeName"]) : ""
                        };
                    integrationSetting.AccessContolTypes = (reader["AccessControlTypes"] != DBNull.Value) ? Convert.ToString(reader["AccessControlTypes"]) : "";
                    integrationSetting.IsPaid = Convert.ToBoolean(reader["IsPaid"]);
                    integrationSetting.ArticleNo = Convert.ToString(reader["ArticleNo"]);
                    integrationSetting.ArticleName = Convert.ToString(reader["ArticleName"]);
                    integrationSetting.ActivityId = Convert.ToInt32(reader["ActivityId"]);

                    var action = new GetGymIntegrationAccessProfileAction(integrationSetting.Id, "GAT");
                    integrationSetting.AccessProfileList = action.Execute(EnumDatabase.Exceline, _gymCode);
                   

                    integrationSettingsList.Add(integrationSetting);
                }

                return integrationSettingsList;
            }
            catch
            {
                throw;
            }
        }
    }
}
