﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Data.Common;
using System.Data;
using System.IO;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetEntityActiveTimesAction : USDBActionBase<List<EntityActiveTimeDC>>
    {
        private int _branchId;
        private DateTime _startDate;
        private DateTime _endDate;
        private List<int> _entityList = new List<int>();
        private string _entityType = string.Empty;
        private string _gymCode = string.Empty;
        private string _user = string.Empty;
        public GetEntityActiveTimesAction(int branchId, DateTime startDate, DateTime endDate, List<int> entityList, string entityType, string gymCode, string user)
        {
            _branchId = branchId;
            _startDate = startDate;
            _endDate = endDate;
            _entityList = entityList;
            _entityType = entityType;
            _gymCode = gymCode;
            _user = user;
        }
        protected override List<EntityActiveTimeDC> Body(System.Data.Common.DbConnection connection)
        {
            List<EntityActiveTimeDC> activeTimesList = new List<EntityActiveTimeDC>();
            string spName = "USExceGMSGetEntityActiveTimes";
            try
            {
                if(_entityList == null)
                {
                    return null;
                }
                foreach (int entityId in _entityList)
                {
                    DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                    command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@sstartDate", DbType.DateTime, _startDate));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@sendDate", DbType.DateTime, _endDate));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@entityNo", DbType.Int32, entityId));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@entityType", DbType.String, _entityType));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                    DbDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        EntityActiveTimeDC activeTime = new EntityActiveTimeDC();
                        activeTime.Id = (reader["ActiveTimeID"] != DBNull.Value) ? Convert.ToInt32(reader["ActiveTimeID"]) : -1;
                        activeTime.EntityId = Convert.ToInt32(reader["EntityId"]);
                        activeTime.EntityRoleType = Convert.ToString(reader["EntityRoleType"]);
                        if (reader["ParentId"] != DBNull.Value)
                            activeTime.ScheduleParentId = Convert.ToInt32(reader["ParentId"]);
                        activeTime.EntityName = Convert.ToString(reader["EntityName"]);
                        if (reader["StartDateTime"] != DBNull.Value)
                        {
                            DateTime startDateTime = Convert.ToDateTime(reader["StartDateTime"]);
                            activeTime.StartDateTime = startDateTime;
                            activeTime.Day = GetFilterDayOfWeek(startDateTime.DayOfWeek);
                            activeTime.DisplayDay = startDateTime.Date.DayOfWeek.ToString();
                            activeTime.StartDate = startDateTime;
                        }
                        if (reader["EndDateTime"] != DBNull.Value)
                            activeTime.EndDateTime = Convert.ToDateTime(reader["EndDateTime"]);
                        if (reader["ScheduleItemId"] != DBNull.Value)
                            activeTime.SheduleItemId = Convert.ToInt32(reader["ScheduleItemId"]);
                        if (reader["ScheduleId"] != DBNull.Value)
                            activeTime.ScheduleId = Convert.ToInt32(reader["ScheduleId"]);
                        if (reader["ScheduleName"] != DBNull.Value)
                            activeTime.Name = Convert.ToString(reader["ScheduleName"]);
                        activeTime.ImagePath = Convert.ToString(reader["ImagePath"]);
                        if (!string.IsNullOrEmpty(activeTime.ImagePath))
                        {
                            activeTime.ProfilePicture = GetMemberProfilePicture(activeTime.ImagePath);
                        }
                        if (_entityType == "CLS")
                        {
                            activeTime.MaxNoOfBookings = Convert.ToInt32(reader["MaxNoOfBookings"]);
                            activeTime.NumberOfMembers = Convert.ToInt32(reader["NoOfMembers"]);
                            activeTime.QueueCount = Convert.ToInt32(reader["QueueCount"]);
                            activeTime.ClassTypeId = Convert.ToInt32(reader["ClassTypeId"]);
                            activeTime.IsFixed = Convert.ToBoolean(reader["IsFixed"]);
                            activeTime.ClassGroupId = Convert.ToInt32(reader["ClassGroupId"]);
                            activeTime.ClassLevelId = Convert.ToInt32(reader["ClassLevelId"]);
                            activeTime.IsIBookingIntegrated = Convert.ToBoolean(reader["IBookingIntegrated"]);
                            var actionRes = new GetEntitiesByActiveTimeIdAction(activeTime.Id, "RES");
                            activeTime.ResourceLst = actionRes.Execute(EnumDatabase.Exceline, _gymCode);
                           
                            var actionIns = new GetEntitiesByActiveTimeIdAction(activeTime.Id, "INS");
                            activeTime.InstructorList = actionIns.Execute(EnumDatabase.Exceline, _gymCode);

                        }

                        if (_entityType == "RES")
                        {
                            activeTime.RoleType = reader["RoleType"].ToString();

                            
                            if (activeTime.RoleType.Equals("RESSCHEDULE"))
                            {
                                if (reader["BookingParentId"] != DBNull.Value && Convert.ToInt32(reader["BookingParentId"]) > 0)
                                {
                                    activeTime.IsAdditionalResActiveTime = true;
                                    int bookingParentId = Convert.ToInt32(reader["BookingParentId"]);
                                    var actionRes = new GetEntityForActiveTimeIdAction(bookingParentId, "RES", activeTime.RoleType.Trim());
                                    activeTime.BookingResourceList = actionRes.Execute(EnumDatabase.Exceline, _gymCode);

                                    var action = new GetEntityForActiveTimeIdAction(bookingParentId, "MEM", activeTime.RoleType.Trim());
                                    activeTime.BookingMemberList = action.Execute(EnumDatabase.Exceline, _gymCode);
                                }
                                else
                                {
                                    var actionRes = new GetEntityForActiveTimeIdAction(activeTime.SheduleItemId, "RES", activeTime.RoleType.Trim());
                                    activeTime.BookingResourceList = actionRes.Execute(EnumDatabase.Exceline, _gymCode);

                                    var action = new GetEntityForActiveTimeIdAction(activeTime.SheduleItemId, "MEM", activeTime.RoleType.Trim());
                                    activeTime.BookingMemberList = action.Execute(EnumDatabase.Exceline, _gymCode);
                                }

                            }
                            else if (activeTime.RoleType.Equals("CONTRACT"))
                            {
                                var action = new GetEntityForActiveTimeIdAction(activeTime.Id, "MEM", activeTime.RoleType.Trim());
                                activeTime.BookingMemberList = action.Execute(EnumDatabase.Exceline, _gymCode);
                            }

                            activeTime.Comment = reader["Comment"].ToString();
                            activeTime.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                            activeTime.ActivityName = reader["ActivityName"].ToString();
                            activeTime.ArticleNoOfMinutes = Convert.ToInt32(reader["NoOfMinutes"]);
                            activeTime.CategoryId = Convert.ToInt32(reader["CategoryId"]);
                            activeTime.CategoryName = reader["CategoryName"].ToString();
                            activeTime.Color = reader["Color"].ToString();
                            activeTime.ArticleId = Convert.ToInt32(reader["ArticleId"]);
                            activeTime.ArticleName = reader["ArticleName"].ToString();
                            if (reader["SMSRemindered"] != DBNull.Value)
                                activeTime.IsSmsReminder = Convert.ToBoolean(reader["SMSRemindered"]);
                            if (reader["IsPaid"] != DBNull.Value)
                                activeTime.IsPaid = Convert.ToBoolean(reader["IsPaid"]);
                            activeTime.IsPunchCard = Convert.ToBoolean(reader["PunchCard"]);
                            activeTime.IsContractBooking = Convert.ToBoolean(reader["ContractBooking"]);
                            if (reader["IsUnAvailable"] != DBNull.Value)
                                activeTime.IsUnAvailableActiveTime = Convert.ToBoolean(reader["IsUnAvailable"]);
                            if (reader["ARItemNo"] != DBNull.Value)
                                activeTime.ArItemNo = Convert.ToInt32(reader["ARItemNo"]);
                            if (reader["Amount"] != DBNull.Value)
                                activeTime.TotalPaid = Convert.ToDecimal(reader["Amount"]);
                            if (reader["ArrivalDateTime"] != DBNull.Value)
                            {
                                activeTime.ArrivedDate = Convert.ToDateTime(reader["ArrivalDateTime"]);
                                activeTime.IsArrived = true;
                            }
                            if (reader["BookingCategoryId"] != DBNull.Value)
                                activeTime.BookingCategoryId = Convert.ToInt32(reader["BookingCategoryId"]);
                            if (reader["BookingCategoryName"] != DBNull.Value)
                                activeTime.BookingCategoryName = Convert.ToString(reader["BookingCategoryName"]);
                            if (reader["BookingCategoryColor"] != DBNull.Value)
                                activeTime.BookingCategoryColor = Convert.ToString(reader["BookingCategoryColor"]);
                            if (reader["Task"] != DBNull.Value)
                                activeTime.IsTask = Convert.ToBoolean(reader["Task"]);
                            if (reader["ClassType"] != DBNull.Value)
                                activeTime.ClassType = Convert.ToString(reader["ClassType"]);

                        }
                        if (_entityType == "EMP")
                        {
                            activeTime.TaskType = Convert.ToInt32(reader["TaskType"]);
                            activeTime.Text = Convert.ToString(reader["TaskName"]);
                            activeTime.ResName = Convert.ToString(reader["ResName"]);
                            if (reader["WeekType"] != DBNull.Value)
                                activeTime.WeekType = Convert.ToInt32(reader["WeekType"]);

                            activeTime.ClassType = Convert.ToString(reader["ClassType"]);
                            if (reader["BookingCategoryId"] != DBNull.Value)
                                activeTime.BookingCategoryId = Convert.ToInt32(reader["BookingCategoryId"]);
                            if (reader["BookingCategoryName"] != DBNull.Value)
                                activeTime.BookingCategoryName = Convert.ToString(reader["BookingCategoryName"]);
                            if (reader["BookingCategoryColor"] != DBNull.Value)
                                activeTime.BookingCategoryColor = Convert.ToString(reader["BookingCategoryColor"]);


                            var actionMemList = new GetMembersForBookingAction(activeTime.SheduleItemId);
                            activeTime.BookingMemberList = actionMemList.Execute(EnumDatabase.Exceline, _gymCode);
                            if (reader["IsBookingTask"] != DBNull.Value)
                                activeTime.IsTask = Convert.ToBoolean(reader["IsBookingTask"]);
                        

                            //if (reader["MemberName"] != DBNull.Value && reader["MemberId"] != DBNull.Value && reader["MemberRole"] != DBNull.Value)
                            //{
                            //    string name = reader["MemberName"].ToString().Trim();
                            //    string role = reader["MemberRole"].ToString().Trim();
                            //    int id = Convert.ToInt32(reader["MemberId"]);
                            //    activeTime.MemberLst.Add(id, role + "-" + name);
                            //}

                        }
                        activeTimesList.Add(activeTime);
                    }
                    reader.Close();
                }
                return activeTimesList;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private byte[] GetMemberProfilePicture(string p)
        {
            if (File.Exists(p))
            {
                byte[] byteArray = System.IO.File.ReadAllBytes(p);
                return byteArray;
            }
            return new byte[0];
        }

        private int GetFilterDayOfWeek(DayOfWeek dayOfWeek)
        {
            int filterDayOfWeek = -1;
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    filterDayOfWeek = 1;
                    break;
                case DayOfWeek.Tuesday:
                    filterDayOfWeek = 2;
                    break;
                case DayOfWeek.Wednesday:
                    filterDayOfWeek = 3;
                    break;
                case DayOfWeek.Thursday:
                    filterDayOfWeek = 4;
                    break;
                case DayOfWeek.Friday:
                    filterDayOfWeek = 5;
                    break;
                case DayOfWeek.Saturday:
                    filterDayOfWeek = 6;
                    break;
                case DayOfWeek.Sunday:
                    filterDayOfWeek = 7;
                    break;
            }
            return filterDayOfWeek;
        }
    }
}
