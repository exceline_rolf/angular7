﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetCountryDetailsAction : USDBActionBase<List<CountryDC>>
    {
        protected override List<CountryDC> Body(DbConnection connection)
        {
            List<CountryDC> cantryList = new List<CountryDC>();
            string storedProcedure = "USP_GetCountryList";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    CountryDC country = new CountryDC();
                    country.Id = reader["CountryId"].ToString();
                    country.Name = reader["Name"].ToString();
                    if (reader["CountryCode"] != DBNull.Value)
                        country.CountryCode = reader["CountryCode"].ToString();

                    cantryList.Add(country);
                  
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            var cuList = new List<CountryDC>();
            var cu1 = cantryList.FirstOrDefault(x => x.Id.ToLower() == "no");
            if(cu1 != null)
                cuList.Add(cu1);
            var cu2 = cantryList.FirstOrDefault(x => x.Id.ToLower() == "se");
            if (cu2 != null)
                cuList.Add(cu2);
            var cu3 = cantryList.FirstOrDefault(x => x.Id.ToLower() == "dk");
            if (cu3 != null)
                cuList.Add(cu3);
            var cu4 = cantryList.FirstOrDefault(x => x.Id.ToLower() == "fi");
            if (cu4 != null)
                cuList.Add(cu4);

            cantryList.Where(x => x.Id.ToLower() != "no" || x.Id.ToLower() != "se" || x.Id.ToLower() != "dk" || x.Id.ToLower() != "fi").ToList().ForEach(cuList.Add);


            return cuList;  
            
        }
    }
}
