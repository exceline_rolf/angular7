﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveHardwareProfileAction : USDBActionBase<bool>
    {
        #region private variable 
        private ExceHardwareProfileDC _hardwareProfile;
        private int _branchId;
        private int _outputId;
        #endregion

        #region Constructor
        public SaveHardwareProfileAction(ExceHardwareProfileDC hardwareProfile, int branchId)
        {
            _hardwareProfile = hardwareProfile;
            _branchId = branchId;
        } 
        #endregion

        #region Body
        protected override bool Body(DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USExceGMSSaveHardwareProfile";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ID", DbType.Int32, _hardwareProfile.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _hardwareProfile.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ChangeMoney",DbType.Decimal, _hardwareProfile.ChangeMoney)); 
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GymHardwareProfileItem", SqlDbType.Structured, GetHardwareProfileItem(_hardwareProfile.HardwareProfileItemList )));
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@OutputId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
                if (param.Value != null) _outputId = Convert.ToInt32(param.Value);
                return _outputId > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get Hardware Profile Item Table
        private DataTable GetHardwareProfileItem(List<ExceHardwareProfileItemDC> hardwareProfileItemList)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(Int32)));
            dataTable.Columns.Add(new DataColumn("HardwareProfileId", typeof(Int32)));
            dataTable.Columns.Add(new DataColumn("CategoryId", typeof(Int32)));

            foreach (ExceHardwareProfileItemDC hardwareProfileItem in hardwareProfileItemList)
            {
                DataRow row = dataTable.NewRow();
                row["ID"] = hardwareProfileItem.Id;
                row["HardwareProfileId"] = hardwareProfileItem.HardwareProfileId;
                row["CategoryId"] = hardwareProfileItem.CategoryId;
                dataTable.Rows.Add(row);
            }
            return dataTable;
        }
        #endregion
    }
}
