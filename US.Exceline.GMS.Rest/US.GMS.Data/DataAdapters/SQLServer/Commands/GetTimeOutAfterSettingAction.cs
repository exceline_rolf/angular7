﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetTimeOutAfterSettingAction:USDBActionBase<decimal>
    {
        private int _branchId;
        private decimal _timeOutAfter = -1;

        public GetTimeOutAfterSettingAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override decimal Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSGetSessionTimeOut";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                object obj = cmd.ExecuteScalar();
                _timeOutAfter = Convert.ToDecimal(obj);
            }
            catch (Exception ex)
            {                
                throw ex;
            }
            return _timeOutAfter;
        } 
    }
}
