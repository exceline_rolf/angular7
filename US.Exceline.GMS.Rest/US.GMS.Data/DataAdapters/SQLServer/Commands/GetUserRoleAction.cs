﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;
using US.Payment.API;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetUserRoleAction : USDBActionBase<List<ExcelineRoleDc>>
    {
        public GetUserRoleAction()
        {
            
        }
        protected override List<ExcelineRoleDc> Body(DbConnection connection)
        {
            var excelineRoleDcs = new List<ExcelineRoleDc>();
            const string storedProcedure = "GetAllUserRoles";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var excelineRoleDc = new ExcelineRoleDc
                        {
                            Id = Convert.ToInt32(reader["ID"]),
                            RoleName = reader["RoleName"].ToString()
                        };

                    excelineRoleDcs.Add(excelineRoleDc);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return excelineRoleDcs;
        }
    }
}
