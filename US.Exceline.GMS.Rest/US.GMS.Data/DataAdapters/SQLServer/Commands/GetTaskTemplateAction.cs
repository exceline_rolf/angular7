﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "6/20/2012 6:06:49 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetTaskTemplateAction : USDBActionBase<List<TaskTemplateDC>>
    {
        //private int _branchId = -1;
        private TaskTemplateType _templateType;
        private int _templateId = -1;

        public GetTaskTemplateAction(TaskTemplateType templateType, int branchId, int templateId)
        {
            // this._branchId = branchId;
            this._templateType = templateType;
            this._templateId = templateId;
        }

        protected override List<TaskTemplateDC> Body(DbConnection connection)
        {
            List<TaskTemplateDC> smsTemplateList = new List<TaskTemplateDC>();
            string storedProcedureName = "USExceGMSAdminGetTaskTemplates";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                //  cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                if (!_templateType.Equals(TaskTemplateType.NONE))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateType", DbType.String, _templateType.ToString()));
                if (_templateId != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateId", DbType.Int32, _templateId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    TaskTemplateDC taskTemplate = new TaskTemplateDC();
                    taskTemplate.Id = Convert.ToInt32(reader["ID"]);
                    taskTemplate.Name = reader["Name"].ToString();
                    taskTemplate.BranchId = Convert.ToInt32(reader["BranchId"]);
                    taskTemplate.TemplateType = (TaskTemplateType)Enum.Parse(typeof(TaskTemplateType), reader["TemplateType"].ToString(), true);
                    taskTemplate.IsAssignTo = Convert.ToBoolean(reader["IsAssignTo"]);
                    taskTemplate.IsStartDate = Convert.ToBoolean(reader["IsStartDate"]);
                    taskTemplate.IsEndDate = Convert.ToBoolean(reader["IsEndDate"]);
                    taskTemplate.IsStartTime = Convert.ToBoolean(reader["IsStartTime"]);
                    taskTemplate.IsEndTime = Convert.ToBoolean(reader["IsEndTime"]);
                    taskTemplate.IsFoundDate = Convert.ToBoolean(reader["IsFoundDate"]);
                    taskTemplate.IsReturnDate = Convert.ToBoolean(reader["IsReturnDate"]);
                    taskTemplate.IsNoOfDays = Convert.ToBoolean(reader["IsNoOfDays"]);
                    taskTemplate.IsPopUp = Convert.ToBoolean(reader["IsPopUp"]);
                    taskTemplate.IsDueTime = Convert.ToBoolean(reader["IsDueTime"]);
                    taskTemplate.IsRepeatable = Convert.ToBoolean(reader["IsRepeatable"]);
                    taskTemplate.IsDueDate = Convert.ToBoolean(reader["IsDueDate"]);
                    taskTemplate.IsPhoneNo = Convert.ToBoolean(reader["IsPhoneNo"]);
                    taskTemplate.IsSms = Convert.ToBoolean(reader["IsSms"]);
                    taskTemplate.IsFollowupMember = Convert.ToBoolean(reader["IsFollowupMember"]);
                    taskTemplate.IsBelongsToMember = Convert.ToBoolean(reader["IsBelongsToMember"]);
                    taskTemplate.IsDefault = Convert.ToBoolean(reader["IsDefault"]);

                    smsTemplateList.Add(taskTemplate);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return smsTemplateList;
        }
    }
}
