﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using System.Data;
using System.Data.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymAccessTimeSettingsAction : USDBActionBase<List<GymAccessTimeSettingsDC>>
    {
        private int _branchId = -1;
        private string _gymCode = string.Empty;
        private string _userName;

        public GetGymAccessTimeSettingsAction(int branchId, string gymCode, string userName)
        {
            _branchId = branchId;
            _gymCode = gymCode;
            _userName = userName;
        }

        protected override List<GymAccessTimeSettingsDC> Body(DbConnection connection)
        {
            List<GymAccessTimeSettingsDC> gymAccessTimeSettingsList = new List<GymAccessTimeSettingsDC>();
            GymAccessTimeSettingsDC gymAccessTimeSettings = new GymAccessTimeSettingsDC();
            string GetAccessProfileSP = "USExceGMSGetAccessProfiles";
            string GymOpenTimeSP = "USExceGMSGetGymOpenTimes";

            try
            {
                DbCommand cmd2 = CreateCommand(CommandType.StoredProcedure, GetAccessProfileSP);
                //cmd2.Parameters.Add(DataAcessUtils.CreateParam("@barnchId", DbType.Int32, _branchId));
                DbDataReader reader2 = cmd2.ExecuteReader();


                while (reader2.Read())
                {
                    gymAccessTimeSettings = new GymAccessTimeSettingsDC();
                    ExceAccessProfileDC profile = new ExceAccessProfileDC();
                    profile.Id = Convert.ToInt32(reader2["ID"]);
                    profile.AccessProfileName = Convert.ToString(reader2["Name"]);
                    profile.IsActive = Convert.ToBoolean(reader2["ActiveStatus"]);
                    GetAccessProfileTimeAction action = new GetAccessProfileTimeAction(profile.Id);
                    profile.AccessTimeList = action.Execute(EnumDatabase.Exceline, _gymCode);
                    gymAccessTimeSettings.AccessProfileList.Add(profile);
                }

                reader2.Close();
                gymAccessTimeSettings.GymOpenTimes = new List<GymOpenTimeDC>();

                DbCommand openTimeCommand = CreateCommand(CommandType.StoredProcedure, GymOpenTimeSP);
                openTimeCommand.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", DbType.Int32, _branchId));
                DbDataReader openTimeReader = openTimeCommand.ExecuteReader();

                while (openTimeReader.Read())
                {
                     
                    GymOpenTimeDC openTime = new GymOpenTimeDC();
                    openTime.Id = Convert.ToInt32(openTimeReader["Id"]);
                    openTime.StartTime = DateTime.Parse(openTimeReader["FromTime"].ToString());
                    openTime.EndTime = DateTime.Parse(openTimeReader["ToTime"].ToString());
                    openTime.IsMonday = Convert.ToBoolean(openTimeReader["Monday"]);
                    if (openTime.IsMonday)
                        openTime.Days = "Mon,";
                    openTime.IsTuesday = Convert.ToBoolean(openTimeReader["Tuesday"]);
                    if (openTime.IsTuesday)
                        openTime.Days += "Tue,";
                    openTime.IsWednesday = Convert.ToBoolean(openTimeReader["Wednesday"]);
                    if (openTime.IsWednesday)
                        openTime.Days += "Wed,";
                    openTime.IsThursday = Convert.ToBoolean(openTimeReader["Thursday"]);
                    if (openTime.IsThursday)
                        openTime.Days += "Thu,";
                    openTime.IsFriday = Convert.ToBoolean(openTimeReader["Friday"]);
                    if (openTime.IsFriday)
                        openTime.Days += "Fri,";
                    openTime.IsSaturday = Convert.ToBoolean(openTimeReader["Saturday"]);
                    if (openTime.IsSaturday)
                        openTime.Days += "Sat,";
                    openTime.IsSunday = Convert.ToBoolean(openTimeReader["Sunday"]);
                    if (openTime.IsSunday)
                        openTime.Days += "Sun";
                    openTime.BranchId = Convert.ToInt32(openTimeReader["BranchId"]);
                    openTime.Days = openTime.Days.Trim(new char[] { ',' });
                    gymAccessTimeSettings.GymOpenTimes.Add(openTime);
                }
                reader2.Close();
                gymAccessTimeSettingsList.Add(gymAccessTimeSettings);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return gymAccessTimeSettingsList;
        }
    }
}


