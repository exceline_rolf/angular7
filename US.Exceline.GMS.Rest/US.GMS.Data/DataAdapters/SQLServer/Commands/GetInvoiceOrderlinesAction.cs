﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetInvoiceOrderlinesAction : USDBActionBase<List<ArticleSummaryDC>>
    {
        private int _arItemNo = -1;
        public GetInvoiceOrderlinesAction(int arItemNo)
        {
            _arItemNo = arItemNo;
        }

        protected override List<ArticleSummaryDC> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipGetInvoiceOrderlines";
            DbDataReader reader = null;
            List<ArticleSummaryDC> orderlineList = new List<ArticleSummaryDC>();
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNo", System.Data.DbType.Int32, _arItemNo));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ArticleSummaryDC orderline = new ArticleSummaryDC();
                    orderline.Id = Convert.ToInt32(reader["Id"]);
                    orderline.ArticleId = Convert.ToInt32(reader["ArticleNo"]);
                    orderline.Name = Convert.ToString(reader["Text"]);
                    orderline.UnitPrice = Convert.ToDecimal(reader["UnitPrice"]);
                    orderline.Quantity = Convert.ToInt32(reader["NoOfItems"]);
                    orderline.Discount = Convert.ToDecimal(reader["Discount"]);
                    orderline.Price = Convert.ToDecimal(reader["Amount"]);
                    orderline.RegDate = Convert.ToDateTime(reader["RegDate"]);
                    orderline.RegTime = Convert.ToString(reader["RegTime"]);
                    orderline.CreditedAmount = Convert.ToDecimal(reader["CreditedAmount"]);
                    if(reader["VoucherExpireDate"] != DBNull.Value)
                    {
                        orderline.ExpiryDate = Convert.ToDateTime(reader["VoucherExpireDate"]);
                    }
                    orderline.VoucherNo = Convert.ToString(reader["VoucherNo"]);
                    orderline.ArticleBalance = orderline.Price - orderline.CreditedAmount;

                    if (orderline.Price < 0)
                        orderline.IsEnabled = false;

                    orderlineList.Add(orderline);
                }

                if (reader != null)
                    reader.Close();
                return orderlineList;
            }
            catch
            {
                if (reader != null)
                    reader.Close();
                throw;
            }

        }
    }
}
