﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateShopAccountItemAndGatPurchaseAction:USDBActionBase<bool>
    {
        private GatpurchaseShopItem _gatPurchaseShopItem;

        public UpdateShopAccountItemAndGatPurchaseAction(GatpurchaseShopItem gatPurchaseShopItem)
        {
            this._gatPurchaseShopItem = gatPurchaseShopItem;
        }
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool _isUpdated = false;
            try
            {
                const string storedProcedureName = "USExceGMSUpdateSchopAccountItemAndGatPurchase";

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", SqlDbType.Int, this._gatPurchaseShopItem.MemberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@itemPrice", SqlDbType.Decimal, this._gatPurchaseShopItem.ItemPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@cardNumber", SqlDbType.VarChar, this._gatPurchaseShopItem.CardNumber));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", SqlDbType.Int, this._gatPurchaseShopItem.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@itemCode", SqlDbType.VarChar, this._gatPurchaseShopItem.ItemCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isNextOrder", SqlDbType.Bit, this._gatPurchaseShopItem.IsNextOrder));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@invoiceNo", SqlDbType.VarChar, this._gatPurchaseShopItem.InvoiceCode));
                
                cmd.ExecuteNonQuery();
                _isUpdated = true;

            }
            catch (Exception ex)
            {
                throw ex;                
            }
            return _isUpdated;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            bool _isUpdated = false;
            try
            {
                const string storedProcedureName = "USExceGMSUpdateSchopAccountItemAndGatPurchase";

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", SqlDbType.Int, this._gatPurchaseShopItem.MemberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@itemPrice", SqlDbType.Decimal, this._gatPurchaseShopItem.ItemPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@cardNumber", SqlDbType.VarChar, this._gatPurchaseShopItem.CardNumber));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", SqlDbType.Int, this._gatPurchaseShopItem.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@itemCode", SqlDbType.VarChar, this._gatPurchaseShopItem.ItemCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isNextOrder", SqlDbType.Bit, this._gatPurchaseShopItem.IsNextOrder));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@invoiceNo", SqlDbType.VarChar, this._gatPurchaseShopItem.InvoiceCode));

                cmd.ExecuteNonQuery();
                _isUpdated = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _isUpdated;
        }
    }
}
