﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using US_DataAccess;
using System.Collections.ObjectModel;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveScheduleAction : USDBActionBase<bool>
    {
       private int _branchId;
       ScheduleDC _scheduleDc;
       private DataTable _dataTable = null;
        public SaveScheduleAction(EmployeeDC employeeDc, int branchId)
        {
            _branchId = branchId;
            _scheduleDc = employeeDc.Schedule;
            if (_scheduleDc != null)
                _dataTable = GetScheduleItemTypeLst(_scheduleDc.SheduleItemList);
        }

       private int _id;
       private string _name = string.Empty;
       private string _roleId = string.Empty;
       private string _user = string.Empty;

        public SaveScheduleAction(int id, string name, string roleId, int branchId,ScheduleDC sheduleDc,string user)
        {
            _id = id;
            _name = name;
            _roleId = roleId;
            _branchId = branchId;
            _scheduleDc = sheduleDc;
            _user = user;
            if (_scheduleDc != null)
                _dataTable = GetScheduleItemTypeLst(_scheduleDc.SheduleItemList);
        }

        private DataTable GetScheduleItemTypeLst(ObservableCollection<ScheduleItemDC> scheduleLst)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("Id", Type.GetType("System.Int32")));
            _dataTable.Columns.Add(new DataColumn("ScheduleId", Type.GetType("System.Int32")));
            _dataTable.Columns.Add(new DataColumn("Occurrence", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("Day", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("Week", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("Month", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("Year", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("StartTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("EndTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("StartDate", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("EndDate", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("LastModifiedDateTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("CreatedDateTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("CreatedUser", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("LastModifiedUser", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("ActiveStatus", Type.GetType("System.Boolean")));
            _dataTable.Columns.Add(new DataColumn("IsFixed", Type.GetType("System.Boolean")));
            _dataTable.Columns.Add(new DataColumn("LastActiveTimesGeneratedDate", Type.GetType("System.DateTime")));


            foreach (ScheduleItemDC scheduleitem in scheduleLst)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["Id"] = scheduleitem.Id;
                _dataTableRow["ScheduleId"] = scheduleitem.ScheduleId;
                _dataTableRow["Occurrence"] = scheduleitem.Occurrence;
                _dataTableRow["Day"] = scheduleitem.Day;
                _dataTableRow["Week"] = scheduleitem.Week;
                _dataTableRow["Month"] = scheduleitem.Month;
                _dataTableRow["Year"] = scheduleitem.Year;
                _dataTableRow["StartTime"] = scheduleitem.StartTime;
                _dataTableRow["EndTime"] = scheduleitem.EndTime;
                _dataTableRow["StartDate"] = scheduleitem.StartDate;
                _dataTableRow["EndDate"] = scheduleitem.EndDate;
                _dataTableRow["CreatedDateTime"] = DateTime.Now;
                _dataTableRow["LastModifiedDateTime"] = DateTime.Now;
                _dataTableRow["CreatedUser"] = scheduleitem.CreatedUser;
                _dataTableRow["LastModifiedUser"] = scheduleitem.LastModifiedUser;
                _dataTableRow["ActiveStatus"] = scheduleitem.ActiveStatus;
                _dataTableRow["IsFixed"] = scheduleitem.IsFixed;
                _dataTableRow["LastActiveTimesGeneratedDate"] = DBNull.Value;
                _dataTable.Rows.Add(_dataTableRow);
            }

            return _dataTable;
        }

        protected override bool Body(DbConnection connection)
        {
            bool _isSaved = false;
            string StoredProcedureName = "USExceGMSAdminAddSchedule";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);

                if (_scheduleDc != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityId", DbType.Int32, _id));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _name));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@role", DbType.String, _roleId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _user));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@startdate", DbType.DateTime, _scheduleDc.StartDate));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@enddate", DbType.DateTime, _scheduleDc.EndDate));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemList", SqlDbType.Structured, _dataTable));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@startdate", DbType.DateTime, DBNull.Value));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@enddate", DbType.DateTime, DBNull.Value));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemList", SqlDbType.Structured, null));
                }
                cmd.ExecuteNonQuery();
                _isSaved = true;
            }

            catch (Exception ex)
            {
                _isSaved = false;
                throw ex;
            }
            return _isSaved;
        }
    }
}
