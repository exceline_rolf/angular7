﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "4/24/2012 2:51:43 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateCategoryAction : USDBActionBase<bool>
    {
        private CategoryDC _category;
        private int _branchId;
        public UpdateCategoryAction(CategoryDC categoryDC, int branchId)
        {
            this._category = categoryDC;
            _branchId = branchId;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string storedProcedureName = "USExceGMSUpdateCategory";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryId", DbType.Int32, _category.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@code", DbType.String, _category.Code));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _category.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@description", DbType.String, _category.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@image", DbType.Byte, _category.CategoryImage));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastModifiedUser", DbType.String, _category.LastModifiedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Color", DbType.String, _category.Color));
                cmd.ExecuteNonQuery();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
