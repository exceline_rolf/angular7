﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetOrderLinesForAutoCreditNoteAction : USDBActionBase<List<OrderLineDC>>
    {
        private int _branchId = -1; 
         private int _priority = -1;

         public GetOrderLinesForAutoCreditNoteAction(int branchId, int priority)
        {
            _branchId = branchId;
            _priority = priority;
        }

        protected override List<OrderLineDC> Body(System.Data.Common.DbConnection connection)
        {
            List<OrderLineDC> creditNoteList = new List<OrderLineDC>();
            string spName = "USExceGMSGetOrderLinesForAutoCreditNote";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@priority", System.Data.DbType.Int32, _priority));
                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    OrderLineDC orderline = new OrderLineDC();
                    orderline.Id = Convert.ToInt32(reader["id"]);
                    orderline.CusId = Convert.ToInt32(reader["custId"]);
                    orderline.CreditNoteAmount = Convert.ToDecimal(reader["creditNoteAmount"]);
                    orderline.Price = Convert.ToDecimal(reader["price"]);
                    orderline.ArItemNo = Convert.ToInt32(reader["arItemNo"]);
                    orderline.Name = Convert.ToString(reader["articleText"]);
                   
                    creditNoteList.Add(orderline);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return creditNoteList;
        }
    }
}
