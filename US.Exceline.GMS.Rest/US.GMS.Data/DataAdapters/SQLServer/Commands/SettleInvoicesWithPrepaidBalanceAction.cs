﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SettleInvoicesWithPrepaidBalanceAction : USDBActionBase<bool>
    {
        private int _arItemNo = -1;
        private string _user = string.Empty;

        public SettleInvoicesWithPrepaidBalanceAction(int arItemNo, string user)
        {
            _arItemNo = arItemNo;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSSettleInvoicesWithPrepaidBalance";

            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNo", System.Data.DbType.Int32, _arItemNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            string spName = "USExceGMSSettleInvoicesWithPrepaidBalance";

            try
            {
                DbCommand command = new SqlCommand();
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.CommandText = spName;
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNo", System.Data.DbType.Int32, _arItemNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
