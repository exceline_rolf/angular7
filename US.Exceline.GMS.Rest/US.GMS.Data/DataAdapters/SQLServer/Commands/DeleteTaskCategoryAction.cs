﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : DKA
// Created Timestamp : "12/10/2013 12:00:41 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteTaskCategoryAction : USDBActionBase<bool>
    {
        private bool _isDeleted;
        private readonly int _taskCategoryId = -1;
        public DeleteTaskCategoryAction(int taskCategoryId)
        {
            _taskCategoryId = taskCategoryId;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSAdminDeleteTaskCategory";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TaskCategoryId", DbType.Int32, _taskCategoryId));

                object obj = cmd.ExecuteScalar();
                int categoryId = Convert.ToInt32(obj);
                if (categoryId == 0)
                    _isDeleted = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _isDeleted;
        }
    }
}
