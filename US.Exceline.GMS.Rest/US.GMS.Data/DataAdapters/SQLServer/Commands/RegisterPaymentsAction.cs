﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : "6/22/2012 3:00:07 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using US.Payment.Core.BusinessDomainObjects;
using US.Payment.API;
using US.Payment.Core.ResultNotifications;
using US.Common.Logging.API;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class RegisterPaymentsAction : USDBActionBase<string>
    {
        private int _branchID = -1;
        private int _memberID = -1;
        private decimal _paymentAmount = 0;
        private decimal _invoiceAmount = 0;
        private string _createdUser = string.Empty;
        private PaymentTypes _paymentType = PaymentTypes.CLASS;
        private bool _isMember = true;

        public RegisterPaymentsAction(int branchId, int memberId, int articleNo, decimal paymentAmount, decimal discount, string createdUser, decimal invoiceAmount, PaymentTypes paymentType, bool isMember, string gymCode)
        {
            _branchID = branchId;
            _memberID = memberId;
            _paymentAmount = paymentAmount;
            _invoiceAmount = invoiceAmount;
            _createdUser = createdUser;
            _paymentType = paymentType;
            _isMember = isMember;
        }
        protected override string Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                GetClaimInfoForPaymentAction action = new GetClaimInfoForPaymentAction(_memberID, _branchID, _paymentType, _isMember);
                IUSPClaim uspClaim = action.Execute(EnumDatabase.USP);
                uspClaim.InvoicedDate = DateTime.Now.ToString("MM-dd-yyyy");
                uspClaim.DueDate = DateTime.Now.ToString("MM-dd-yyyy");
                uspClaim.Amount = _invoiceAmount;
                uspClaim.DueBalance = "0";
                uspClaim.Balance = "0";

                USImportResult result = US.Payment.API.Claims.AddClaim(uspClaim, -1, string.Empty,_createdUser);
                if (result.ErrorOccured)
                {
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), _createdUser);
                    }
                    return string.Empty;
                }
                
                uspClaim.Amount = _paymentAmount;
                uspClaim.InvoiceType = Payment.Core.Enums.InvoiceTypes.PaymentMemo;
               
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
