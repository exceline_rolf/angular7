﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "5/8/2012 12:00:41 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteTaskTemplateAction : USDBActionBase<bool>
    {
        private bool _isDeleted;
        private int _templateId = -1;
        public DeleteTaskTemplateAction(int templateId)
        {
            this._templateId = templateId;
        }

        protected override bool Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminDeleteTaskTemplates";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateId", DbType.Int32, _templateId));

                object obj = cmd.ExecuteScalar();
                int templateId = Convert.ToInt32(obj);
                if (templateId > 0)
                    _isDeleted = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _isDeleted;
        }
    }
}
