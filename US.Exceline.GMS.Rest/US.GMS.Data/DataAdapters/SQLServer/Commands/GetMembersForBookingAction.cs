﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    class GetMembersForBookingAction : USDBActionBase<List<BookingEntityDc>>
    {
        private readonly int _activeTimeId;
        public GetMembersForBookingAction(int activeTimeId)
        {
            _activeTimeId = activeTimeId;
        }
        protected override List<BookingEntityDc> Body(DbConnection connection)
        {
            var entityLst = new List<BookingEntityDc>();
            const string spName = "USExceGMSGetMembersForBooking";
            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _activeTimeId));

                 var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var item = new BookingEntityDc
                            {
                                CustId = Convert.ToString(reader["CustId"]),
                                Name = Convert.ToString(reader["Name"])
                            };
                        entityLst.Add(item);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return entityLst;
        }
    }
}
