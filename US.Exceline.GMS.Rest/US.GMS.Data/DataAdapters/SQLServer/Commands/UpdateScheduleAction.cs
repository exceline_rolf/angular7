﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "6/20/2012 6:06:12 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Data;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Data.SqlClient;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateScheduleAction : USDBActionBase<bool>
    {
        private ScheduleDC _scheduleDC;
        private DataTable _dataTable = null;

        public UpdateScheduleAction(ScheduleDC scheduleDC)
        {
            this._scheduleDC = scheduleDC;
            if (_scheduleDC != null)
                _dataTable = GetScheduleItemTypeLst(_scheduleDC.SheduleItemList);
        }


        private DataTable GetScheduleItemTypeLst(ObservableCollection<ScheduleItemDC> scheduleLst)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("Id", Type.GetType("System.Int32")));
            _dataTable.Columns.Add(new DataColumn("ScheduleId", Type.GetType("System.Int32")));
            _dataTable.Columns.Add(new DataColumn("Occurrence", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("Day", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("Week", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("Month", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("Year", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("StartTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("EndTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("StartDate", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("EndDate", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("CreatedDateTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("LastModifiedDateTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("CreatedUser", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("LastModifiedUser", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("ActiveStatus", Type.GetType("System.Boolean")));
            _dataTable.Columns.Add(new DataColumn("IsFixed", Type.GetType("System.Boolean")));
            _dataTable.Columns.Add(new DataColumn("LastActiveTimesGeneratedDate", Type.GetType("System.DateTime")));

            foreach (ScheduleItemDC scheduleitem in scheduleLst)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["Id"] = scheduleitem.Id;
                _dataTableRow["ScheduleId"] = scheduleitem.ScheduleId;
                _dataTableRow["Occurrence"] = scheduleitem.Occurrence;
                _dataTableRow["Day"] = scheduleitem.Day;
                if (scheduleitem.Week == -1)
                {
                    _dataTableRow["Week"] = null;
                }
                else
                {
                    _dataTableRow["Week"] = scheduleitem.Week;
                }
                _dataTableRow["Month"] = scheduleitem.Month;
                if (scheduleitem.Year == -1)
                {
                    _dataTableRow["Year"] = null;
                }
                else
                {
                    _dataTableRow["Year"] = scheduleitem.Year;
                };
                _dataTableRow["StartTime"] = scheduleitem.StartTime;
                _dataTableRow["EndTime"] = scheduleitem.EndTime;
                _dataTableRow["StartDate"] = scheduleitem.StartDate;
                _dataTableRow["EndDate"] = scheduleitem.EndDate;
                _dataTableRow["CreatedDateTime"] = DateTime.Now;
                _dataTableRow["LastModifiedDateTime"] = DateTime.Now;
                _dataTableRow["CreatedUser"] = scheduleitem.CreatedUser;
                _dataTableRow["LastModifiedUser"] = scheduleitem.LastModifiedUser;
                _dataTableRow["ActiveStatus"] = scheduleitem.ActiveStatus;
                _dataTableRow["IsFixed"] = scheduleitem.IsFixed;
                _dataTableRow["LastActiveTimesGeneratedDate"] = DBNull.Value;
                _dataTable.Rows.Add(_dataTableRow);
            }

            return _dataTable;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool _isSaved = false;
            string StoredProcedureName = "USExceGMSUpdateTaskSchedule";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleId", DbType.Int32, _scheduleDC.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Startdate", DbType.DateTime, _scheduleDC.StartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Enddate", DbType.DateTime, _scheduleDC.EndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityId", DbType.Int32, _scheduleDC.EntityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemList", SqlDbType.Structured, _dataTable));

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@outId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);


                cmd.ExecuteNonQuery();

                int TaskID = Convert.ToInt32(param.Value);
                if (TaskID > 0)
                    _isSaved = true;

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _isSaved;
        }
    }
}
