﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetClaimInfoForInstallmentPaymentAction : USDBActionBase<IUSPClaim>
    {
        private int _memberID = -1;
        private int _branchID = -1;
        private int _installmentID = -1;
        private int _memberContractId = -1;
        public GetClaimInfoForInstallmentPaymentAction(int memberID, int brnachID, int installmentID, int installmentNo, int memberContractId)
        {
            _memberID = memberID;
            _branchID = brnachID;
            _installmentID = installmentID;
            _memberContractId = memberContractId;
        }

        protected override IUSPClaim Body(System.Data.Common.DbConnection connection)
        {
            IUSPClaim claim = new USPClaim();
            IUSPDebtor debtor = new USPDebtor();
            IUSPCreditor creditor = new USPCreditor();
            IUSPAddress debtorAddress = new USPAddress();
            DbDataReader reader = null;
            string spName = "USExceGMSGetClaimInfoForInstallmentPayments";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", System.Data.DbType.Int32, _memberID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@installmentID", System.Data.DbType.Int32, _installmentID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberContractId", System.Data.DbType.Int32, _memberContractId));

                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    debtor.DebtorEntityID = Convert.ToInt32(reader["DebtorEntNo"]);
                    debtor.DebtorcustumerID = Convert.ToString(reader["CustId"]);
                    debtor.DebtorFirstName = Convert.ToString(reader["FirstName"]);
                    debtor.DebtorSecondName = Convert.ToString(reader["LastName"]);
                    claim.NameInContract = Convert.ToString(reader["NameOnContract"]);
                    debtor.DebtorEntityRoleID = Convert.ToInt32(reader["DebtorEntityRoleID"]);
                    debtor.DebtorBirthDay = Convert.ToDateTime(reader["DebtorBirthDay"]).ToString("MM-dd-yyyy");

                    debtorAddress.Address1 = Convert.ToString(reader["Addr1"]);
                    debtorAddress.Address2 = Convert.ToString(reader["Addr2"]);
                    debtorAddress.Address3 = Convert.ToString(reader["Addr3"]);
                    debtorAddress.Email = Convert.ToString(reader["Email"]);
                    debtorAddress.TelHome = Convert.ToString(reader["TelHome"]);
                    debtorAddress.TelMobile = Convert.ToString(reader["TelMobile"]);
                    debtorAddress.TelWork = Convert.ToString(reader["TelWork"]);

                    debtor.DebtorAddressList.Add(debtorAddress);

                    creditor.CreditorInkassoID = Convert.ToString(reader["CreditorInkassoID"]);
                    creditor.CreditorName = Convert.ToString(reader["CreditorName"]);
                    creditor.CreditorAccountNo = Convert.ToString(reader["CreditorAccountNo"]);
                    creditor.CreditorEntityRoleID = Convert.ToInt32(reader["CreditorRoleId"]);

                    claim.InvoiceTypeId = Convert.ToInt32(reader["InvoiceType"]);
                    claim.InvoiceRef = Convert.ToString(reader["InvoiceRef"]);
                    claim.Group = Convert.ToString(reader["GroupId"]);
                    if (!string.IsNullOrEmpty(reader["ContractExpire"].ToString()))
                    {
                        claim.ContractExpire = Convert.ToDateTime(reader["ContractExpire"]).ToString("MM-dd-yyyy");
                    }
                    if (!string.IsNullOrEmpty(reader["ContractKID"].ToString()))
                    {
                        claim.ContractKID = Convert.ToString(reader["ContractKID"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ContractNo"].ToString()))
                    {
                        claim.ContractNumber = Convert.ToString(reader["ContractNo"]);
                    }
                    claim.InstallmentNumber = Convert.ToString(reader["InstallmentNo"]);
                    claim.InvoiceType = InvoiceTypes.Invoice;
                    claim.BranchNumber = _branchID.ToString();
                    claim.Debtor = debtor;
                    claim.Creditor = creditor;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return claim;
        }

        public IUSPClaim RunOnTransaction(DbTransaction transaction)
        {
            IUSPClaim claim = new USPClaim();
            IUSPDebtor debtor = new USPDebtor();
            IUSPCreditor creditor = new USPCreditor();
            IUSPAddress debtorAddress = new USPAddress();
            DbDataReader reader = null;
            string spName = "USExceGMSGetClaimInfoForInstallmentPayments";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Transaction = transaction;
                command.Connection = transaction.Connection;

                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", System.Data.DbType.Int32, _memberID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@installmentID", System.Data.DbType.Int32, _installmentID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberContractId", System.Data.DbType.Int32, _memberContractId));

                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    debtor.DebtorEntityID = Convert.ToInt32(reader["DebtorEntNo"]);
                    debtor.DebtorcustumerID = Convert.ToString(reader["CustId"]);
                    debtor.DebtorFirstName = Convert.ToString(reader["FirstName"]);
                    debtor.DebtorSecondName = Convert.ToString(reader["LastName"]);
                    claim.NameInContract = Convert.ToString(reader["NameOnContract"]);
                    debtor.DebtorEntityRoleID = Convert.ToInt32(reader["DebtorEntityRoleID"]);
                    debtor.DebtorBirthDay = Convert.ToDateTime(reader["DebtorBirthDay"]).ToString("MM-dd-yyyy");

                    debtorAddress.Address1 = Convert.ToString(reader["Addr1"]);
                    debtorAddress.Address2 = Convert.ToString(reader["Addr2"]);
                    debtorAddress.Address3 = Convert.ToString(reader["Addr3"]);
                    debtorAddress.Email = Convert.ToString(reader["Email"]);
                    debtorAddress.TelHome = Convert.ToString(reader["TelHome"]);
                    debtorAddress.TelMobile = Convert.ToString(reader["TelMobile"]);
                    debtorAddress.TelWork = Convert.ToString(reader["TelWork"]);

                    debtor.DebtorAddressList.Add(debtorAddress);

                    creditor.CreditorInkassoID = Convert.ToString(reader["CreditorInkassoID"]);
                    creditor.CreditorName = Convert.ToString(reader["CreditorName"]);
                    creditor.CreditorAccountNo = Convert.ToString(reader["CreditorAccountNo"]);
                    creditor.CreditorEntityRoleID = Convert.ToInt32(reader["CreditorRoleId"]);

                    claim.InvoiceTypeId = Convert.ToInt32(reader["InvoiceType"]);
                    claim.InvoiceRef = Convert.ToString(reader["InvoiceRef"]);
                    claim.Group = Convert.ToString(reader["GroupId"]);
                    if (!string.IsNullOrEmpty(reader["ContractExpire"].ToString()))
                    {
                        claim.ContractExpire = Convert.ToDateTime(reader["ContractExpire"]).ToString("MM-dd-yyyy");
                    }
                    if (!string.IsNullOrEmpty(reader["ContractKID"].ToString()))
                    {
                        claim.ContractKID = Convert.ToString(reader["ContractKID"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ContractNo"].ToString()))
                    {
                        claim.ContractNumber = Convert.ToString(reader["ContractNo"]);
                    }
                    claim.InstallmentNumber = Convert.ToString(reader["InstallmentNo"]);
                    claim.InvoiceType = InvoiceTypes.Invoice;
                    claim.BranchNumber = _branchID.ToString();
                    claim.Debtor = debtor;
                    claim.Creditor = creditor;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return claim;
        }
    }
}
