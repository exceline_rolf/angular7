using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateGiftVoucherExpiryDateAction : USDBActionBase<bool>
    {
        private readonly string _voucherNumber;
        private readonly int _consumedBy;
        private readonly int _paidArItemNo;

        public UpdateGiftVoucherExpiryDateAction(string voucherNumber,int consumedBy, int paidArItemNo)
        {
            _voucherNumber = voucherNumber;
            _consumedBy = consumedBy;
            _paidArItemNo = paidArItemNo;
        }
        protected override bool Body(System.Data.Common.DbConnection dbConnection)
        {
            var isUpdated = false;
            const string spName = "USExceGMSShopSetGiftVoucherExpiryDate";

            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VoucherNumber", DbType.String, _voucherNumber));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ConsumedBy", DbType.Int32, _consumedBy));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PaidArItemNo", DbType.Int32, _paidArItemNo));
                cmd.ExecuteNonQuery();
                isUpdated = true;
            }
            catch (Exception ex)
            {
               throw ex;
            }
            return isUpdated;
        }

        public bool RunOnTranaction(DbTransaction transaction)
        {
            var isUpdated = false;
            const string spName = "USExceGMSShopSetGiftVoucherExpiryDate";

            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, spName);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VoucherNumber", DbType.String, _voucherNumber));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ConsumedBy", DbType.Int32, _consumedBy));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PaidArItemNo", DbType.Int32, _paidArItemNo));
                cmd.ExecuteNonQuery();
                isUpdated = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isUpdated;
        }
    }
}
