﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    class UpdateAccessProfilesAction : USDBActionBase<int>
    {
        private ExceAccessProfileDC _accessProfile = new ExceAccessProfileDC();
        private string _user = string.Empty; 
        private int _outputId = -1;

        public UpdateAccessProfilesAction(ExceAccessProfileDC accessProfile, string user)
        {
            _accessProfile = accessProfile;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
                //DbTransaction transaction = connection.BeginTransaction();
                //foreach (ExceAccessProfileDC profile in _accessProfileList)
                //{
                //    _accessProfile = profile;
                //    _outputId = SaveAccessProfile(transaction);
                //}
                //transaction.Commit();
                //return 1;

                const string storedProcedureName = "USExceGMSSaveAccessProfile";
                try
                {
                    DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                   
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _accessProfile.Id));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccessProfileName", DbType.String, _accessProfile.AccessProfileName));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus", DbType.Boolean, true));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, -1));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Gender", DbType.String, _accessProfile.Gender.ToString()));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@GymAccessProfileTime", SqlDbType.Structured, GetAccessProfileTime(_accessProfile.AccessTimeList)));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsEdited", DbType.Boolean, _accessProfile.IsEdited));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDeleted", DbType.Boolean, _accessProfile.IsDeleted));

                    DataTable branches = new DataTable();
                    DataColumn col = new DataColumn("ID", typeof(Int32));
                    branches.Columns.Add(col);
                    if (_accessProfile.BranchIdList.Count > 0)
                    {
                        foreach (var id in _accessProfile.BranchIdList)
                        {
                            branches.Rows.Add(id);
                        }
                    }
                    else
                    {
                        branches.Rows.Add(0);
                    }
                    SqlParameter parameter = new SqlParameter();
                    parameter.ParameterName = "@Branches";
                    parameter.SqlDbType = SqlDbType.Structured;
                    parameter.Value = branches;
                    cmd.Parameters.Add(parameter);

                    DataTable regions = new DataTable();
                    DataColumn col2 = new DataColumn("ID", typeof(Int32));
                    regions.Columns.Add(col2);
                    if (_accessProfile.RegionIdList.Count > 0)
                    {
                        foreach (var id in _accessProfile.RegionIdList)
                        {
                            regions.Rows.Add(id);
                        }
                    }
                    else
                    {
                        regions.Rows.Add(0);
                    }
                    SqlParameter parameter2 = new SqlParameter();
                    parameter2.ParameterName = "@Regions";
                    parameter2.SqlDbType = SqlDbType.Structured;
                    parameter2.Value = regions;
                    cmd.Parameters.Add(parameter2);

                    DataTable countries = new DataTable();
                    DataColumn col3 = new DataColumn("ID", typeof(string));
                    countries.Columns.Add(col3);
                    if (_accessProfile.CountryIdList.Count > 0)
                    {
                        foreach (var id in _accessProfile.CountryIdList)
                        {
                            countries.Rows.Add(id);
                        }
                    }
                    else
                    {
                        countries.Rows.Add(0);
                    }
                    SqlParameter parameter3 = new SqlParameter();
                    parameter3.ParameterName = "@Countries";
                    parameter3.SqlDbType = SqlDbType.Structured;
                    parameter3.Value = countries;
                    cmd.Parameters.Add(parameter3);

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@OutputId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(param);
                    cmd.ExecuteNonQuery();
                    return Convert.ToInt32(param.Value);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }     


        //private int SaveAccessProfile(DbTransaction transaction)
        //{
        //    const string storedProcedureName = "USExceGMSSaveAccessProfile";
        //    try
        //    {
        //        DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
        //        cmd.Connection = transaction.Connection;
        //        cmd.Transaction = transaction;
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _accessProfile.Id));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccessProfileName", DbType.String, _accessProfile.AccessProfileName));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus", DbType.Boolean, true));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, -1));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Gender", DbType.String, _accessProfile.Gender.ToString()));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@GymAccessProfileTime", SqlDbType.Structured, GetAccessProfileTime(_accessProfile.AccessTimeList)));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsEdited", DbType.Boolean, _accessProfile.IsEdited));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDeleted", DbType.Boolean, _accessProfile.IsDeleted));

        //        DataTable branches = new DataTable();
        //        DataColumn col = new DataColumn("ID", typeof(Int32));
        //        branches.Columns.Add(col);
        //        if (_accessProfile.BranchIdList.Count > 0)
        //        {
        //            foreach (var id in _accessProfile.BranchIdList)
        //            {
        //                branches.Rows.Add(id);
        //            }
        //        }
        //        else
        //        {
        //            branches.Rows.Add(0);
        //        }
        //        SqlParameter parameter = new SqlParameter();
        //        parameter.ParameterName = "@Branches";
        //        parameter.SqlDbType = SqlDbType.Structured;
        //        parameter.Value = branches;
        //        cmd.Parameters.Add(parameter);

        //        DataTable regions = new DataTable();
        //        DataColumn col2 = new DataColumn("ID", typeof(Int32));
        //        regions.Columns.Add(col2);
        //        if (_accessProfile.RegionIdList.Count > 0)
        //        {
        //            foreach (var id in _accessProfile.RegionIdList)
        //            {
        //                regions.Rows.Add(id);
        //            }
        //        }
        //        else
        //        {
        //            regions.Rows.Add(0);
        //        }
        //        SqlParameter parameter2 = new SqlParameter();
        //        parameter2.ParameterName = "@Regions";
        //        parameter2.SqlDbType = SqlDbType.Structured;
        //        parameter2.Value = regions;
        //        cmd.Parameters.Add(parameter2);

        //        DataTable countries = new DataTable();
        //        DataColumn col3 = new DataColumn("ID", typeof(string));
        //        countries.Columns.Add(col3);
        //        if (_accessProfile.CountryIdList.Count > 0)
        //        {
        //            foreach (var id in _accessProfile.CountryIdList)
        //            {
        //                countries.Rows.Add(id);
        //            }
        //        }
        //        else
        //        {
        //            countries.Rows.Add(0);
        //        }
        //        SqlParameter parameter3 = new SqlParameter();
        //        parameter3.ParameterName = "@Countries";
        //        parameter3.SqlDbType = SqlDbType.Structured;
        //        parameter3.Value = countries;
        //        cmd.Parameters.Add(parameter3);

        //        SqlParameter param = new SqlParameter();
        //        param.ParameterName = "@OutputId";
        //        param.SqlDbType = SqlDbType.Int;
        //        param.Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add(param);
        //        cmd.ExecuteNonQuery();
        //        return Convert.ToInt32(param.Value);
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}

        private DataTable GetAccessProfileTime(List<ExceAccessProfileTimeDC> profileTimeList)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(Int32)));
            dataTable.Columns.Add(new DataColumn("AccessProfileId", typeof(Int32)));
            dataTable.Columns.Add(new DataColumn("FromTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("ToTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("Monday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Tuesday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Wednesday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Thursday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Friday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Saturday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Sunday", typeof(bool)));

            foreach (ExceAccessProfileTimeDC profileTime in profileTimeList)
            {
                DataRow row = dataTable.NewRow();
                row["ID"] = profileTime.Id;
                row["AccessProfileId"] = profileTime.AccessProfileId;
                row["FromTime"] = profileTime.FromTime;
                row["ToTime"] = profileTime.ToTime;
                row["Monday"] = profileTime.Monday;
                row["Tuesday"] = profileTime.Tuesday;
                row["Wednesday"] = profileTime.Wednesday;
                row["Thursday"] = profileTime.Thursday;
                row["Friday"] = profileTime.Friday;
                row["Saturday"] = profileTime.Saturday;
                row["Sunday"] = profileTime.Sunday;
                dataTable.Rows.Add(row);
            }
            return dataTable;
        }
    }
}
