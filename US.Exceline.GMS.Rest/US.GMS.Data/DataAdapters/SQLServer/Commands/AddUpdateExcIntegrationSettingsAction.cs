﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class AddUpdateExcIntegrationSettingsAction : USDBActionBase<bool>
    {
        private readonly int _branchId;
        private readonly GymIntegrationExcSettingsDC _gymIntegrationExc;
        private DataTable _dataTable;

        public AddUpdateExcIntegrationSettingsAction(int branchId, GymIntegrationExcSettingsDC gymIntegrationExc)
        {
            _branchId = branchId;
            _gymIntegrationExc = gymIntegrationExc;
            _dataTable = GetAccessProfileLst(_gymIntegrationExc.AccessProfileList);
        }


        private DataTable GetAccessProfileLst(List<ExceAccessProfileDC> accessProfiles)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("AccessProfileId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("TerminalId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("DrawPunch", typeof(Boolean)));
            if (accessProfiles != null && accessProfiles.Any())
                foreach (var item in accessProfiles)
                {
                    DataRow dataTableRow = _dataTable.NewRow();
                    dataTableRow["AccessProfileId"] = item.Id;
                    dataTableRow["TerminalId"] = _gymIntegrationExc.Id;
                    dataTableRow["DrawPunch"] = item.IsDrawPuncheSelect;
                    _dataTable.Rows.Add(dataTableRow);
                }
            return _dataTable;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "dbo.USExceGMSAdminUpdateExcIntegrationSettings";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.String, _gymIntegrationExc.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", DbType.Int32, _gymIntegrationExc.ActivityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccessControlTypes", DbType.String, _gymIntegrationExc.AccessContolTypes));
                cmd.Parameters.Add(_dataTable != null
                                           ? DataAcessUtils.CreateParam("@ExceAccessProfileControls", SqlDbType.Structured,
                                                                        _dataTable)
                                           : DataAcessUtils.CreateParam("@ExceAccessProfileControls", SqlDbType.Structured, null));

                DbParameter para1 = new SqlParameter();
                para1.DbType = DbType.Int32;
                para1.ParameterName = "@outPutID";
                para1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para1);
                cmd.ExecuteNonQuery();
                var outputId = Convert.ToInt32(para1.Value);
                if (outputId > 0)
                {
                    return true;
                }
                return false;
            }

            catch
            {
                throw;
            }
        }
    }
}
