﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "7/12/2012 9:54:05 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class ManageMemberShopAccountAction : USDBActionBase<bool>
    {
        private readonly MemberShopAccountDC _memberShopAccount;
        private readonly string _mode;
        private readonly string _user;
        private readonly int _branchId;
        private readonly int _salePointId = -1;
        private readonly string _gymCode = string.Empty;


        public ManageMemberShopAccountAction(MemberShopAccountDC memberShopAccount, string mode, string user, int branchId, int salePointId, string gymCode)
        {
            _memberShopAccount = memberShopAccount;
            _mode = mode;
            _user = user;
            _gymCode = gymCode;
            _salePointId = salePointId;
            _branchId = branchId;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSManageMembershipManageMemberShopAccount";
            try
            {
                if (_memberShopAccount.CreditaccountItemList != null)
                {
                    foreach (MemberShopAccountItemDC item in _memberShopAccount.CreditaccountItemList)
                    {
                        DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@mode", DbType.String, _mode));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@shopAccountId", DbType.Int32, _memberShopAccount.Id));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberShopAccount.MemberId));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@payment", DbType.Decimal, item.PaymentAmount));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@payMode", DbType.Int32, item.PayModeId));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@payModeCode", DbType.String, item.PayMode));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Boolean, _memberShopAccount.ActiveStatus));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@deactivateReason", DbType.String, _memberShopAccount.DeactivateReason));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@isManual", DbType.Boolean, 1));
                        cmd.ExecuteScalar();

                        if (_mode == "ADD")
                        {
                            //------------------save shop transations data -------------------------------------------------
                            ShopTransactionDC _shopTrans = new ShopTransactionDC();
                            _shopTrans.BranchId = _branchId;
                            _shopTrans.CreatedUser = _user;
                            _shopTrans.CreatedDate = DateTime.Now;
                            try
                            {
                                _shopTrans.Mode = (TransactionTypes)Enum.Parse(typeof(TransactionTypes), item.PayMode, true);
                            }
                            catch
                            {
                                _shopTrans.Mode = TransactionTypes.NONE;
                                _shopTrans.ModeText = TransactionTypes.ONACCOUNTCREDIT.ToString();
                            }
                            try
                            {
                                _shopTrans.Amount = (decimal)item.PaymentAmount;
                            }
                            catch
                            {
                                _shopTrans.Amount = 0;
                            }
                            _shopTrans.SalePointId = _salePointId;
                            _shopTrans.EntityId = _memberShopAccount.MemberId;
                            _shopTrans.EntityRoleType = "MEM";
                            SaveShopTransactionAction shopTransactionAction = new SaveShopTransactionAction(_shopTrans);
                            shopTransactionAction.Execute(EnumDatabase.Exceline, _gymCode);
                        }
                        //------------------------------------------------------------------------------------------------



                    }
                }
                
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
