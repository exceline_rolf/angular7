﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 13/12/2012 12:03:07 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Notification;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetNotificationTemplatesByTypeId : USDBActionBase<List<NotificationTemplateDC>>
    {
        private int _templateTypeId = -1;
        private NotifyMethodType _notifyMethod = NotifyMethodType.NONE;
        private int _branchId = -1;

        public GetNotificationTemplatesByTypeId(int templateTypeId, NotifyMethodType notifyMethod, int branchId)
        {
            _templateTypeId = templateTypeId;
            _notifyMethod = notifyMethod;
            _branchId = branchId;
        }

        protected override List<NotificationTemplateDC> Body(DbConnection connection)
        {
            List<NotificationTemplateDC> _notificationTemplateList = new List<NotificationTemplateDC>();
            string StoredProcedureName = "USExceGMSGetNotificationTemplateByType";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@templateTypeId", DbType.Int32, _templateTypeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@notifyMethod", DbType.String, _notifyMethod.ToString()));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    NotificationTemplateDC template = new NotificationTemplateDC();
                    template.Id = Convert.ToInt32(reader["Id"]);
                    template.Text = reader["TemplateText"].ToString();
                    // template.NotifyMethod= Convert.ToInt32(reader["NotifyMethodId"]);
                    // template.TypeId = Convert.ToInt32(reader["TypeId"].ToString());
                    // template.CreatedUser = reader["CreatedUser"].ToString();
                    _notificationTemplateList.Add(template);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _notificationTemplateList;
        }
    }
}
