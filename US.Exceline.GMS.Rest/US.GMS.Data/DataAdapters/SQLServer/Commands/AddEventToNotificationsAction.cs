﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class AddEventToNotificationsAction : USDBActionBase<bool>
    {
        private USCommonNotificationDC _notification;

        public AddEventToNotificationsAction(USCommonNotificationDC notification)
        {
            _notification = notification;
        }
        protected override bool Body(DbConnection connection)
        {
            // bool result = false;
            string spName = "ExceGMSAddNotificationEvent";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@Title", DbType.String, _notification.Title));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _notification.Description));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _notification.CreatedUser));
                command.Parameters.Add(DataAcessUtils.CreateParam("@RoleId", DbType.String, _notification.Role));
                command.Parameters.Add(DataAcessUtils.CreateParam("@TypeId", DbType.Int32, _notification.TypeId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SeverityId", DbType.Int32, _notification.SeverityId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@StatusId", DbType.Int32, _notification.StatusId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _notification.BranchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _notification.MemberID));
                // command.Parameters.Add(DataAcessUtils.CreateParam("@IsReplyseleted", DbType.String, false));
                command.Parameters.Add(DataAcessUtils.CreateParam("@RecordType", DbType.String, _notification.RecordType));
                command.ExecuteScalar();
                // result = Convert.ToBoolean(command.ExecuteScalar());
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
