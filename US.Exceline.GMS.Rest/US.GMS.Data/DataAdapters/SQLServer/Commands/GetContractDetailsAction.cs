﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;
using System.Data; 
using System.Data.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractDetailsAction : USDBActionBase<List<PackageDC>>
    {
        private int _branchId = 0;
        private string _searchText = string.Empty;

        public GetContractDetailsAction(int branchId, string user)
        {
            _branchId = branchId;
            _searchText = user;
        }

        protected override List<PackageDC> Body(System.Data.Common.DbConnection connection)
        {
            List<PackageDC> packageList = new List<PackageDC>();
            string StoredProcedureName = "USExceGMSAdminGetContracts";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@searchText", DbType.String, _searchText));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    PackageDC package = new PackageDC();
                    package.ContractTypeValue = new Core.DomainObjects.Common.CategoryDC();
                    package.PackageId = Convert.ToInt32(reader["Id"]);
                    package.PackageName = reader["Name"].ToString();
                    package.BranchId = Convert.ToInt32(reader["BranchId"]);
                    package.ContractTypeValue.Code = Convert.ToString(reader["PackageType"]);
                    package.ContractTypeValue.Id = Convert.ToInt32(reader["PackageTypeId"]);
                    package.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    package.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    package.PackagePrice = Convert.ToDecimal(reader["PackagePrice"]);
                    package.NumOfInstallments = Convert.ToInt32(reader["NumberOfInstallments"]);
                    package.RatePerInstallment = Convert.ToDecimal(reader["RatePerInstallment"]);
                    package.EnrollmentFee = Convert.ToDecimal(reader["EnrollmentFee"]);
                    package.NumOfVisits = Convert.ToInt32(reader["NumberOfVisits"]);
                    package.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);

                    packageList.Add(package);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return packageList;
        }
    }
}
