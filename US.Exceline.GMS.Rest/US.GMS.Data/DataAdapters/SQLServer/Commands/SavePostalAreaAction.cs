﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
   public class SavePostalAreaAction : USDBActionBase<int>
    {
       private string _postalCode = string.Empty;
       private string _postalArea = string.Empty;
       private Int64 _population = 0;
       private Int64 _houseHold = 0;
       public SavePostalAreaAction(string user, string postalCode, string postalArea, Int64 population, Int64 houseHold)
       {
           _postalCode = postalCode;
           _postalArea = postalArea;
           _population = population;
           _houseHold = houseHold;
       }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string storedProcedure = "USExceGMSSavePostalArea";
            int returnValue = -1;

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@postalCode", DbType.String, _postalCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@postalArea", DbType.String, _postalArea));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@population", DbType.Int64, _population));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@houseHold", DbType.Int64, _houseHold));
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@outId";
                output.Size = 100;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);

                cmd.ExecuteNonQuery();
                returnValue = Convert.ToInt32(output.Value);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnValue;
        }
    }
}
