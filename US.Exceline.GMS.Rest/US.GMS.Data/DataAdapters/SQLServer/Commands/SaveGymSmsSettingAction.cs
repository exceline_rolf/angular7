﻿using System;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveGymSmsSettingAction : USDBActionBase<bool>
    {
        private readonly GymSmsSettingsDC _gymSmsSetting = new GymSmsSettingsDC();
        private bool _returnValue;

        public SaveGymSmsSettingAction(int branchId, GymSmsSettingsDC gymSmsSetting)
        {
            _gymSmsSetting = gymSmsSetting;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedure = "USExceGMSSaveGymSmsSetting";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _gymSmsSetting.Id));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SMSID", DbType.Int32, _gymSmsSetting.SMSID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SenderName", DbType.String, _gymSmsSetting.SenderName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastvisitInterval", DbType.Int32, _gymSmsSetting.LastvisitInterval));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsBirthday", DbType.Boolean, _gymSmsSetting.IsBirthday));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsBookingReminder", DbType.Boolean, _gymSmsSetting.IsBookingReminder));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ATGNotApprovedInterval1", DbType.Int32, _gymSmsSetting.ATGNotApprovedInterval1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ATGNotApprovedInterval2", DbType.Int32, _gymSmsSetting.ATGNotApprovedInterval2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ATGNotApprovedInterval3", DbType.Int32, _gymSmsSetting.ATGNotApprovedInterval3));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractEnding", DbType.Int32, _gymSmsSetting.ContractEnding));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReminderDaysInterval", DbType.Int32, _gymSmsSetting.ReminderDaysInterval));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NovisitsIntervalMessage1", DbType.Int32, _gymSmsSetting.NovisitsIntervalMessage1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NovisitsIntervalMessage2", DbType.Int32, _gymSmsSetting.NovisitsIntervalMessage2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DaysBeforeFreezeEnds", DbType.Int32, _gymSmsSetting.DaysBeforeFreezeEnds));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPriceChange", DbType.Boolean, _gymSmsSetting.IsPriceChange));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsReorderingShop", DbType.Boolean, _gymSmsSetting.IsReorderingShop));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAccessFreeze", DbType.Boolean, _gymSmsSetting.IsAccessFreeze));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAccessOutsideProfile", DbType.Boolean, _gymSmsSetting.IsAccessOutsideProfile));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAccessClosedPeriod", DbType.Boolean, _gymSmsSetting.IsAccessClosedPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAccessClosedOutsideOpenHours", DbType.Boolean, _gymSmsSetting.IsAccessClosedOutsideOpenHours));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAccessDuebalance", DbType.Boolean, _gymSmsSetting.IsAccessDuebalance));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAccessDueUnpaid", DbType.Boolean, _gymSmsSetting.IsAccessDueUnpaid));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAccessDueOnAccount", DbType.Boolean, _gymSmsSetting.IsAccessDueOnAccount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAccessRecentlyVisit", DbType.Boolean, _gymSmsSetting.IsAccessRecentlyVisit));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAccessInfoAntiDoping", DbType.Boolean, _gymSmsSetting.IsAccessInfoAntiDoping));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAccessInfoContractEnds", DbType.Boolean, _gymSmsSetting.IsAccessInfoContractEnds));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccessInfoPunchesLowestNo", DbType.Int32, _gymSmsSetting.AccessInfoPunchesLowestNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAccessInfoIsFollowUpPopUp", DbType.Boolean, _gymSmsSetting.IsAccessInfoIsFollowUpPopUp));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAccessInfoInformTrainer", DbType.Boolean, _gymSmsSetting.IsAccessInfoInformTrainer));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", DbType.Int32, _gymSmsSetting.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SendingTimes", SqlDbType.Structured, GetSendingTimes()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ProhibittedTimes", SqlDbType.Structured, GetProhibittedTimes()));

                cmd.ExecuteNonQuery();
                _returnValue = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _returnValue;
        }

        private DataTable GetSendingTimes()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("BranchId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("FromTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("Monday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Tuesday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Wednesday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Thursday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Friday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Saturday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Sunday", typeof(bool)));

            foreach (ExceSMSSendingTimeDC sendingTime in _gymSmsSetting.SendingTimes)
            {
                DataRow tableRow = dataTable.NewRow();
                tableRow["ID"] = sendingTime.Id;
                tableRow["BranchId"] = sendingTime.BranchId;
                tableRow["FromTime"] = sendingTime.SendAfterTime;
                tableRow["Monday"] = sendingTime.IsMonday;
                tableRow["Tuesday"] = sendingTime.IsTuesday;
                tableRow["Wednesday"] = sendingTime.IsWednesday;
                tableRow["Thursday"] = sendingTime.IsThursday;
                tableRow["Friday"] = sendingTime.IsFriday;
                tableRow["Saturday"] = sendingTime.IsSaturday;
                tableRow["Sunday"] = sendingTime.IsSunday;
                dataTable.Rows.Add(tableRow);
            }
            return dataTable;
        }

        private DataTable GetProhibittedTimes()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("BranchId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("FromDate", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("ToDate", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("Comment", typeof(string)));

            foreach (ExceSMSProhibitedDayDC prohibittedDay in _gymSmsSetting.ProhibittedTimes)
            {
                DataRow tableRow = dataTable.NewRow();
                tableRow["ID"] = prohibittedDay.Id;
                tableRow["BranchId"] = prohibittedDay.BranchId;
                tableRow["FromDate"] = prohibittedDay.StartDate;
                if(prohibittedDay.EndDate.HasValue)
                   tableRow["ToDate"] = prohibittedDay.EndDate;

                tableRow["Comment"] = prohibittedDay.Comment;
                dataTable.Rows.Add(tableRow);
            }
            return dataTable;   
        }
    }
}
