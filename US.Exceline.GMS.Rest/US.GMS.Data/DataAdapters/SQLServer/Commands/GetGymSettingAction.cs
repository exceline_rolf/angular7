﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using System.Data;
using System.Data.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymSettingAction : USDBActionBase<ExceGymSettingDC>
    {
        private int _branchId = -1;
        private string _gymCode = string.Empty;

        public GetGymSettingAction(int branchId, string gymCode)
        {
            this._branchId = branchId;
            this._gymCode = gymCode;
        }


        protected override ExceGymSettingDC Body(DbConnection connection)
        {
            ExceGymSettingDC gymSetting = new ExceGymSettingDC();
            string StoredProcedureName = "USExceGMSGetGymSetting";
            string GetAccessProfileSP = "USExceGMSGetAccessProfiles";
            string GymOpenTimeSP = "USExceGMSGetGymOpenTimes";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    gymSetting.Id = Convert.ToInt32(reader["Id"]);
                    gymSetting.ServiceSMSFreq = Convert.ToInt32(reader["ServiceSMSFreq"]);
                    gymSetting.ReceiveServiceSMSStart = DateTime.Parse((reader["ServiceSMSStart"].ToString()));
                    gymSetting.ReceiveServiceSMSEnd = DateTime.Parse(((reader["ServiceSMSEnd"].ToString())));

                    gymSetting.MemberSMSFreq = Convert.ToInt32(reader["MemberSMSFreq"]);
                    gymSetting.ReceiveMemberSMSStart = DateTime.Parse((reader["MemberSMSStart"].ToString()));
                    gymSetting.ReceiveMemberSMSEnd = DateTime.Parse((reader["MemberSMSEnd"].ToString()));

                    gymSetting.RemainderSMSFreq = Convert.ToInt32(reader["RemainderSMSFreq"]);
                    gymSetting.ReceiveReminderSMSStart = DateTime.Parse((reader["RemaindeSMSStart"].ToString()));
                    gymSetting.ReceiveReminderSMSEnd = DateTime.Parse((reader["RemaindeSMSEnd"].ToString()));

                    gymSetting.MaxSMS = Convert.ToInt32(reader["MaxSMS"]);
                    gymSetting.SMSBookingReminder = Convert.ToInt32(reader["SMSBookingReminder"]);
                    gymSetting.MemberRequired = Convert.ToBoolean(reader["MemberRequired"]);
                    gymSetting.MobileRequired = Convert.ToBoolean(reader["MobileRequired"]);
                    gymSetting.AddressRequired = Convert.ToBoolean(reader["AddressRequired"]);
                    gymSetting.ZipCodeRequired = Convert.ToBoolean(reader["ZipCodeRequired"]);
                    gymSetting.CountVisitAfter = (int)Convert.ToDecimal(reader["CountVisitAfter"]);
                    gymSetting.PlanIn = Convert.ToInt32(reader["PlanIn"]);
                    gymSetting.TimeOut = Convert.ToInt32(reader["TimeOutAfter"]);
                    gymSetting.BarcodeAvailable = Convert.ToBoolean(reader["BarcodeAvailable"]);
                    gymSetting.PrintReceipt = Convert.ToBoolean(reader["PrintReceipt"]);
                    gymSetting.PhoneCode = reader["PhoneCode"].ToString();
                    gymSetting.CreditPeriod = Convert.ToInt32(reader["CreditPeriod"]);
                    gymSetting.InvoiceCancellationPeriod = Convert.ToInt32(reader["InvoiceCancellationPeriod"]);
                    gymSetting.IsShopAvailable = Convert.ToBoolean(reader["IsShopAvailable"]);
                    gymSetting.DebtWarningdate = Convert.ToInt32(reader["DebtWarningDate"]);
                    gymSetting.ReminderDate = Convert.ToInt32(reader["ReminderDate"]);
                    gymSetting.CreditDueDate = Convert.ToInt32(reader["CreditDueDate"]);
                    gymSetting.UseTodayAsDueDate = Convert.ToBoolean(reader["UseTodayAsDueDate"]);
                    gymSetting.IsAntiDopingRequired = Convert.ToBoolean(reader["IsAntiDopingRequired"]);
                    gymSetting.GymStartTime = DateTime.Parse((reader["GymStartTime"].ToString()));
                    gymSetting.GymEndTime = DateTime.Parse((reader["GymEndTime"].ToString()));


                    //Economy Settings
                    gymSetting.RestPlusMonth = Convert.ToBoolean(reader["RestPlusMonth"]);
                    gymSetting.InvoiceAccountNo = reader["InvoiceAccountNo"].ToString();
                    gymSetting.PaymentAccountNo = reader["PaymentAccountNo"].ToString();
                    gymSetting.JanEnrollment = Convert.ToDecimal(reader["JanEnrollment"]);
                    gymSetting.FebEnrollment = Convert.ToDecimal(reader["FebEnrollment"]);
                    gymSetting.MarEnrollment = Convert.ToDecimal(reader["MarEnrollment"]);
                    gymSetting.AprEnrollment = Convert.ToDecimal(reader["AprEnrollment"]);
                    gymSetting.MayEnrollment = Convert.ToDecimal(reader["MayEnrollment"]);
                    gymSetting.JunEnrollment = Convert.ToDecimal(reader["JunEnrollment"]);
                    gymSetting.JulEnrollment = Convert.ToDecimal(reader["JulEnrollment"]);
                    gymSetting.AugEnrollment = Convert.ToDecimal(reader["AugEnrollment"]);
                    gymSetting.SepEnrollment = Convert.ToDecimal(reader["SepEnrollment"]);
                    gymSetting.OctEnrollment = Convert.ToDecimal(reader["OctEnrollment"]);
                    gymSetting.NovEnrollment = Convert.ToDecimal(reader["NovEnrollment"]);
                    gymSetting.DecEnrollment = Convert.ToDecimal(reader["DecEnrollment"]);
                    gymSetting.IsMinTotalPayable = Convert.ToBoolean(reader["IsMinTotalPayable"]);
                    gymSetting.IsIgnoreInvoiceFee = Convert.ToBoolean(reader["IsIgnoreInvoiceFee"]);
                    gymSetting.IsShopLoginNeeded = Convert.ToBoolean(reader["ShopLoginNeeded"]);
                    gymSetting.CreditSaleLimit = Convert.ToDecimal(reader["CreditSaleLimit"]);
                    gymSetting.IsXtraUsed = Convert.ToBoolean(reader["IsXtraUSed"]);
                    gymSetting.CreditPeriod = Convert.ToInt32(reader["CreditPeriod"]);
                    gymSetting.CreditDueDate = Convert.ToInt32(reader["CreditDueDate"]);
                    gymSetting.InvoiceCancellationPeriod = Convert.ToInt32(reader["InvoiceCancellationPeriod"]);
                    gymSetting.UseTodayAsDueDate = Convert.ToBoolean(reader["UseTodayAsDueDate"]);
                    gymSetting.IsMemCardAsGuestCard = Convert.ToBoolean(reader["MemCardAsGuestCard"]);
                    gymSetting.CreditNoteMinInvoiceFee = Convert.ToDecimal(reader["CreditNoteMinInvoiceFee"]);
                    gymSetting.MoveExceededPayToInvoice = Convert.ToBoolean(reader["MoveExceededPayToInvoice"]);

                    //Member Search Settings
                    gymSetting.MemberSearchText = Convert.ToString(reader["MemberSearchText"]);
               
                    // Is Shop Login Needed
                    gymSetting.IsShopLoginNeeded = Convert.ToBoolean(reader["ShopLoginNeeded"]);
               
                }

                reader.Close();

                DbCommand cmd2 = CreateCommand(CommandType.StoredProcedure, GetAccessProfileSP);
                cmd2.Parameters.Add(DataAcessUtils.CreateParam("@barnchId", System.Data.DbType.Int32, _branchId));
                DbDataReader reader2 = cmd2.ExecuteReader();

                while (reader2.Read())
                {
                    ExceAccessProfileDC profile = new ExceAccessProfileDC();
                    profile.Id = Convert.ToInt32(reader2["ID"]);
                    profile.AccessProfileName = Convert.ToString(reader2["Name"]);
                    profile.IsActive = Convert.ToBoolean(reader2["ActiveStatus"]);
                    GetAccessProfileTimeAction action = new GetAccessProfileTimeAction(profile.Id);
                    profile.AccessTimeList = action.Execute(EnumDatabase.Exceline, _gymCode);
                    ////gymSetting.AccessProfileList.Add(profile);
                }

                reader2.Close();
                ////gymSetting.GymOpenTimes = new List<GymOpenTimeDC>();

                DbCommand openTimeCommand = CreateCommand(CommandType.StoredProcedure, GymOpenTimeSP);
                openTimeCommand.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", System.Data.DbType.Int32, _branchId));
                DbDataReader openTimeReader = openTimeCommand.ExecuteReader();

                //gma
                // List<GymOpenTimeDC> tempTimeList = new List<GymOpenTimeDC>();

                while (openTimeReader.Read())
                {
                    GymOpenTimeDC openTime = new GymOpenTimeDC();
                    openTime.Id = Convert.ToInt32(openTimeReader["Id"]);
                    openTime.StartTime = DateTime.Parse(openTimeReader["FromTime"].ToString());
                    openTime.EndTime = DateTime.Parse(openTimeReader["ToTime"].ToString());
                    openTime.IsMonday = Convert.ToBoolean(openTimeReader["Monday"]);
                    if (openTime.IsMonday)
                        openTime.Days = "Mon,";
                    openTime.IsTuesday = Convert.ToBoolean(openTimeReader["Tuesday"]);
                    if (openTime.IsTuesday)
                        openTime.Days += "Tue,";
                    openTime.IsWednesday = Convert.ToBoolean(openTimeReader["Wednesday"]);
                    if (openTime.IsWednesday)
                        openTime.Days += "Wed,";
                    openTime.IsThursday = Convert.ToBoolean(openTimeReader["Thursday"]);
                    if (openTime.IsThursday)
                        openTime.Days += "Thu,";
                    openTime.IsFriday = Convert.ToBoolean(openTimeReader["Friday"]);
                    if (openTime.IsFriday)
                        openTime.Days += "Fri,";
                    openTime.IsSaturday = Convert.ToBoolean(openTimeReader["Saturday"]);
                    if (openTime.IsSaturday)
                        openTime.Days += "Sat,";
                    openTime.IsSunday = Convert.ToBoolean(openTimeReader["Sunday"]);
                    if (openTime.IsSunday)
                        openTime.Days += "Sun";

                    openTime.BranchID = Convert.ToInt32(openTimeReader["BranchId"]);
                    //gma
                    //string[] dayparts = openTime.Days.Split(new char[] { ',' });
                    openTime.Days = openTime.Days.Trim(new char[] { ',' });

                    ////gymSetting.GymOpenTimes.Add(openTime);
                }
                reader2.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return gymSetting;
        }

    }
}


