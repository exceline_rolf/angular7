﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.Payment.Modules.Economy.Core.DomainObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetOrderDetailsByInstallmentId : USDBActionBase<CreditorOrderLine>
    {
        private string _user = string.Empty;
        private int _installmentID = -1;
        private int _branchID = -1;

        public GetOrderDetailsByInstallmentId(int installmentID, string gymCode, int branchID, string user)
        {
            _installmentID = installmentID;
            _branchID = branchID;
            _user = user;
        }

        protected override CreditorOrderLine Body(System.Data.Common.DbConnection connection)
        {

            string spName = "USExceGMSManageMembershipGetInstallmentByID";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@installmentID", System.Data.DbType.Int32, _installmentID));
                DbDataReader reader = command.ExecuteReader();
                CreditorOrderLine orderLine = new CreditorOrderLine();
                while (reader.Read())
                {

                    if (reader["Price"] != DBNull.Value)
                        orderLine.Amount = Convert.ToDecimal(reader["Price"]);
                    if (reader["ArticleId"] != DBNull.Value)
                        orderLine.ArticleNo = Convert.ToString(reader["ArticleId"]);
                    if (reader["ItemName"] != DBNull.Value)
                        orderLine.ArticleText = Convert.ToString(reader["ItemName"]);
                    if (reader["Price"] != DBNull.Value)
                        orderLine.Balance = Convert.ToDecimal(reader["Price"]);
                    orderLine.BranchNo = _branchID;
                    if (reader["Id"] != DBNull.Value)
                        orderLine.CoRelationId = Convert.ToString(reader["Id"]);
                    orderLine.CreatedUser = _user;
                    orderLine.CreditorInvoiceType = Payment.Modules.Economy.Core.Enums.CreditorInvoiceType.CREDITOR;
                    if (reader["Discount"] != DBNull.Value)
                        orderLine.Discount = Convert.ToDecimal(reader["Discount"]);
                    if (reader["DueDate"] != DBNull.Value)
                        orderLine.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    if (reader["InvoiceNo"] != DBNull.Value)
                        orderLine.InvoiceNo = Convert.ToString(reader["InvoiceNo"]);
                    orderLine.NoOfItems = 1;
                    orderLine.OrderType = "E";
                    orderLine.RegDate = DateTime.Now;
                    orderLine.ModifiedUser = _user;
                    if (reader["ItemName"] != DBNull.Value)
                        orderLine.Text = Convert.ToString(reader["ItemName"]);
                    if (reader["UnitPrice"] != DBNull.Value)
                        orderLine.UnitPrice = Convert.ToDecimal(reader["UnitPrice"]);
                }
                reader.Close();
                return orderLine;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
