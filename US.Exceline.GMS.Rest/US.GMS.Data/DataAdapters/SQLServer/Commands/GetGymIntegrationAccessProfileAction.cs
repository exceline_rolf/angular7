﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymIntegrationAccessProfileAction : USDBActionBase<List<ExceAccessProfileDC>>
    {
        private readonly int _controlId;
        private string _type = string.Empty;
        public GetGymIntegrationAccessProfileAction(int terminalId,string type)
        {
            _controlId = terminalId;
            _type = type;
        }

        protected override List<ExceAccessProfileDC> Body(DbConnection connection)
        {
            string storedProcedureName = string.Empty;

            switch (_type)
            {
                case "GAT":
                    storedProcedureName = "dbo.USExceGMSAdminGetGymGATIntegrationAccessProfile";
                    break;
                case "EXC":
                    storedProcedureName = "dbo.USExceGMSAdminGetGymExcIntegrationAccessProfile";
                    break;
            }

            var exceAccessProfileList =  new List<ExceAccessProfileDC>();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ControlId ", DbType.Int32, _controlId));


                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var accessProfile = new ExceAccessProfileDC
                        {
                            Id = Convert.ToInt32(reader["AccessProfileId"]),
                            AccessProfileName = reader["Name"].ToString(),
                            IsDrawPuncheSelect = Convert.ToBoolean(reader["DrawPunch"])
                        };
                    exceAccessProfileList.Add(accessProfile);
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return exceAccessProfileList;
        }
    }
}
