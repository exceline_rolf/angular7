﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveShopTransactionAction : USDBActionBase<int>
    {
        private ShopTransactionDC _shopTransction = new ShopTransactionDC();

        public SaveShopTransactionAction(ShopTransactionDC shopTransction)
        {
            _shopTransction = shopTransction;
        }

        protected override int Body(DbConnection connection)
        {
            int transId = -1;
            string storedProcedureName = "USExceGMSShopAddShopTransaction";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Mode", DbType.String, _shopTransction.Mode.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Amount", DbType.Decimal, _shopTransction.Amount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _shopTransction.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _shopTransction.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SalePointId", DbType.Int32, _shopTransction.SalePointId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityId", DbType.Int32, _shopTransction.EntityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityType", DbType.String, _shopTransction.EntityRoleType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceArItemNo", DbType.Int32, _shopTransction.InvoiceArItemNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VoucherNo", DbType.String, _shopTransction.VoucherNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CardType", DbType.String, _shopTransction.CardType));

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@OutId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);

                cmd.ExecuteNonQuery();

                transId = Convert.ToInt32(param.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return transId;
        }

        public int RunOnTransaction(DbTransaction transaction)
        {

            string spName = "USExceGMSShopAddShopTransaction";
            int transId = -1;
            try
            {

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Mode", DbType.String, _shopTransction.Mode.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Amount", DbType.Decimal, _shopTransction.Amount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _shopTransction.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _shopTransction.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SalePointId", DbType.Int32, _shopTransction.SalePointId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityId", DbType.Int32, _shopTransction.EntityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityType", DbType.String, _shopTransction.EntityRoleType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceArItemNo", DbType.Int32, _shopTransction.InvoiceArItemNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VoucherNo", DbType.String, _shopTransction.VoucherNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CardType", DbType.String, _shopTransction.CardType));

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@OutId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);

                cmd.ExecuteNonQuery();

                transId = Convert.ToInt32(param.Value);
                return transId;
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }
    }
}
