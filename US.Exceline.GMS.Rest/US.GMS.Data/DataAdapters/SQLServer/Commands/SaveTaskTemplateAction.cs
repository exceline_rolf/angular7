﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "6/20/2012 6:06:28 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Collections.ObjectModel;
using System.Data.SqlClient;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveTaskTemplateAction : USDBActionBase<bool>
    {
        private bool _isSaved = false;
        private TaskTemplateDC _taskTemplate;
        private DataTable _dataTable;

        public SaveTaskTemplateAction(TaskTemplateDC taskTemplate, bool isEdit)
        {
            _taskTemplate = taskTemplate;
            _dataTable = GetExtendedFieldList(taskTemplate.ExtendedFieldsList);
        }

        private DataTable GetExtendedFieldList(ObservableCollection<ExtendedTaskTemplateDC> extendedFieldList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("Title", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("TemplateId", Type.GetType("System.Int32")));
            _dataTable.Columns.Add(new DataColumn("FieldType", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("CreatedDateTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("LastModifiedDateTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("CreatedUser", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("LastModifiedUser", Type.GetType("System.String")));


            foreach (ExtendedTaskTemplateDC fieldItem in extendedFieldList)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["Title"] = fieldItem.Title;
                _dataTableRow["TemplateId"] = fieldItem.TemplateId;
                _dataTableRow["FieldType"] = fieldItem.FieldType.ToString();
                _dataTableRow["CreatedDateTime"] = DateTime.Now;
                _dataTableRow["LastModifiedDateTime"] = DateTime.Now;
                _dataTableRow["CreatedUser"] = fieldItem.CreatedUser;
                _dataTableRow["LastModifiedUser"] = fieldItem.LastModifiedUser;
                
                _dataTable.Rows.Add(_dataTableRow);
            }

            return _dataTable;
        }

      
        protected override bool Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminSaveTaskTemplates";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _taskTemplate.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _taskTemplate.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ExtendedFieldsList", SqlDbType.Structured, _dataTable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateId", DbType.String, _taskTemplate.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateType", DbType.String, _taskTemplate.TemplateType.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAssignTo", DbType.Boolean, _taskTemplate.IsAssignTo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsStartDate", DbType.Boolean, _taskTemplate.IsStartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsEndDate", DbType.Boolean, _taskTemplate.IsEndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsStartTime", DbType.Boolean, _taskTemplate.IsStartTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsEndTime", DbType.Boolean, _taskTemplate.IsEndTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsFoundDate", DbType.Boolean, _taskTemplate.IsFoundDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsReturnDate", DbType.Boolean, _taskTemplate.IsReturnDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDueDate", DbType.Boolean, _taskTemplate.IsDueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsNoOfDays", DbType.Boolean, _taskTemplate.IsNoOfDays));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPhoneNo", DbType.Boolean, _taskTemplate.IsPhoneNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsSms", DbType.Boolean, _taskTemplate.IsSms));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsFollowupMember", DbType.Boolean, _taskTemplate.IsFollowupMember));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsBelongsToMember", DbType.Boolean, _taskTemplate.IsBelongsToMember));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPopUp", DbType.Boolean, _taskTemplate.IsPopUp));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDueTime", DbType.Boolean, _taskTemplate.IsDueTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsRepeatable", DbType.Boolean, _taskTemplate.IsRepeatable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _taskTemplate.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedUser", DbType.String, _taskTemplate.LastModifiedUser));
                

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@outId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);


                cmd.ExecuteNonQuery();

                int templateId = Convert.ToInt32(param.Value);
                if (templateId > 0)
                    _isSaved = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _isSaved;
        }
    }
}
