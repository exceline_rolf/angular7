﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/9/2012 9:13:57 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Common;
using System.IO;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetMembersForCommonBookingAction : USDBActionBase<List<OrdinaryMemberDC>>
    {
        private int _branchId = 0;
        private string _searchText = string.Empty;

        public GetMembersForCommonBookingAction(int branchId, string searchText)
        {
            _branchId = branchId;
            _searchText = searchText;
        }

        protected override List<OrdinaryMemberDC> Body(System.Data.Common.DbConnection connection)
        {
            List<OrdinaryMemberDC> ordinaryMemberLst = new List<OrdinaryMemberDC>();
            string StoredProcedureName = "USExceGMSGetMembersForCommonBooking";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@searchText", DbType.String, _searchText));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    OrdinaryMemberDC ordinaryMember = new OrdinaryMemberDC();
                    ordinaryMember.EntNo = Convert.ToInt32(reader["EntNo"]);
                    ordinaryMember.Id = Convert.ToInt32(reader["MemberId"]);
                    ordinaryMember.BranchId = Convert.ToInt32(reader["BranchId"]);
                    ordinaryMember.ImagePath = Convert.ToString(reader["ImagePath"]);
                    ordinaryMember.FirstName = reader["FirstName"].ToString();
                    ordinaryMember.LastName = reader["LastName"].ToString();
                    ordinaryMember.Name = reader["Name"].ToString();

                    if (!String.IsNullOrEmpty(reader["BirthDate"].ToString()))
                        ordinaryMember.BirthDate = Convert.ToDateTime(reader["BirthDate"]);
                    ordinaryMember.Address1 = reader["Address1"].ToString();
                    ordinaryMember.Address2 = reader["Address2"].ToString();
                    ordinaryMember.Address3 = reader["Address3"].ToString();
                    ordinaryMember.PostCode = reader["ZipCode"].ToString();
                    ordinaryMember.PostPlace = reader["ZipName"].ToString();
                    ordinaryMember.Mobile = reader["Mobile"].ToString();
                    ordinaryMember.WorkTeleNo = reader["TeleWork"].ToString();
                    ordinaryMember.PrivateTeleNo = reader["TeleHome"].ToString();
                    ordinaryMember.Email = reader["Email"].ToString();
                    ordinaryMember.CustId = reader["CustId"].ToString();
                    if (!String.IsNullOrEmpty(reader["Gender"].ToString()))
                    {
                        ordinaryMember.Gender = (Gender)Enum.Parse(typeof(Gender), reader["Gender"].ToString());
                    }

                    if (!string.IsNullOrEmpty(ordinaryMember.ImagePath))
                    {
                        ordinaryMember.ProfilePicture = GetMemberProfilePicture(ordinaryMember.ImagePath);
                    }

                    ordinaryMember.MemberCategory = new CategoryDC();
                    ordinaryMember.MemberCategory.Id = Convert.ToInt32(reader["CategoryId"]);
                    ordinaryMemberLst.Add(ordinaryMember);
                }
            }

            catch (Exception ex)
            {
                throw new NotImplementedException("", ex);
            }
            return ordinaryMemberLst;
        }

        private byte[] GetMemberProfilePicture(string p)
        {
            if (File.Exists(p))
            {
                byte[] byteArray = System.IO.File.ReadAllBytes(p);
                return byteArray;
            }
            return new byte[0];
        }
    }
}
