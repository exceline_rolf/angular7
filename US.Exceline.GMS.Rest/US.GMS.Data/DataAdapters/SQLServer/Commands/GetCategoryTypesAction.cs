﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "4/19/2012 3:10:43 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetCategoryTypesAction : USDBActionBase<List<CategoryTypeDC>>
    {
        private readonly string _name;

        public GetCategoryTypesAction(string name)
        {
            _name = name;
        }

        protected override List<CategoryTypeDC> Body(DbConnection connection)
        {
            List<CategoryTypeDC> categoryTypeList = new List<CategoryTypeDC>();
            const string storedProcedureName = "USExceGMSGetCategoryTypes";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _name));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    CategoryTypeDC categoryType = new CategoryTypeDC();
                    categoryType.Id = Convert.ToInt32(reader["ID"]);
                    categoryType.Code = reader["Code"].ToString();
                    categoryType.Description = reader["Description"] != null ? reader["Description"].ToString() : null;
                    categoryType.Name = reader["Name"].ToString();
                    categoryTypeList.Add(categoryType);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return categoryTypeList;
        }
    }
}
