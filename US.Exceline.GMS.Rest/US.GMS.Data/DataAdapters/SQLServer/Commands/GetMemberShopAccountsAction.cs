﻿using System;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberShopAccountsAction : USDBActionBase<MemberShopAccountDC>
    {
        private readonly int _memberId;
        private readonly int _hit = -1;
        private readonly string _gymCode = string.Empty;
        private readonly bool _itemsNeeded;
        private readonly string _entityType = string.Empty;
        public GetMemberShopAccountsAction(int memberId, string enyityType, int hit, string gymCode, bool itemsNeeded)
        {
            _memberId = memberId;
            _hit = hit;
            _gymCode = gymCode;
            _itemsNeeded = itemsNeeded;
            _entityType = enyityType;
        }

        protected override MemberShopAccountDC Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSManageMembershipGetMemberShopAccounts";
            MemberShopAccountDC memberAccount = new MemberShopAccountDC();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityType", DbType.String, _entityType));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    memberAccount.Id = Convert.ToInt32(reader["ShopAccountId"]);
                    memberAccount.MemberId = Convert.ToInt32(reader["MemberId"]);
                    memberAccount.MemberName = reader["MemberName"].ToString().Trim();
                    if (reader["Balance"] != DBNull.Value)
                    {
                        memberAccount.Balance = Convert.ToDecimal(reader["Balance"]);
                    }
                    memberAccount.DecativateDate = reader["DeactivateDate"] != DBNull.Value ? Convert.ToDateTime(reader["DeactivateDate"]) : (DateTime?)null;
                    memberAccount.DeactivateReason = reader["DeactivateReason"].ToString();
                    memberAccount.CreatedDateTime = Convert.ToDateTime(reader["CreatedDateTime"]);
                    memberAccount.CreatedUser = reader["CreatedUser"].ToString();
                    memberAccount.ActiveStatus = reader["ActiveStatus"] != DBNull.Value && Convert.ToBoolean(reader["ActiveStatus"]);
                    memberAccount.MemberBranchID = Convert.ToInt32(reader["BranchId"]);
                }

                if (memberAccount != null && _itemsNeeded)
                {
                    GetMemberShopAccountItemsAction itemAction = new GetMemberShopAccountItemsAction(memberAccount.Id, _hit);
                    List<MemberShopAccountItemDC> itemsList = new List<MemberShopAccountItemDC>();
                    itemsList = itemAction.Execute(EnumDatabase.Exceline, _gymCode);

                    if (itemsList != null)
                    {
                        try
                        {
                            memberAccount.CreditaccountItemList = itemsList.Where(x => x.Type == "CRE").OrderByDescending(y => y.CreatedDateTime).ToList();
                            memberAccount.DebitaccountItemList = itemsList.Where(x => x.Type == "DEB").OrderByDescending(y => y.CreatedDateTime).ToList();
                        }
                        catch
                        {
                        }
                    }
                }

                //Getting the prepaidBalance 
                GetPrepaidBalanceAction prepaidAction = new GetPrepaidBalanceAction(_memberId, _entityType);
                memberAccount.PrepaidBalance = prepaidAction.Execute(EnumDatabase.Exceline, _gymCode);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return memberAccount;
        }
    }
}
