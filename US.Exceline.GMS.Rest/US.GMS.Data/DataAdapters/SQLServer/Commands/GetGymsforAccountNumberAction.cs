﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymsforAccountNumberAction : USDBActionBase<List<ExceUserBranchDC>>
    {
        private string _accountNo = string.Empty;
        public GetGymsforAccountNumberAction(string accoutNumber)
        {
            _accountNo = accoutNumber;
        }

        protected override List<ExceUserBranchDC> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSGetGymsForAcccountNo";
            List<ExceUserBranchDC> gyms = new List<ExceUserBranchDC>();
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccountNo", System.Data.DbType.String, _accountNo));
                DbDataReader reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    ExceUserBranchDC gym = new ExceUserBranchDC();
                    gym.BranchId = Convert.ToInt32(reader["ID"]);
                    gym.BranchName = Convert.ToString(reader["Name"]);
                    gyms.Add(gym);

                }
                return gyms;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
