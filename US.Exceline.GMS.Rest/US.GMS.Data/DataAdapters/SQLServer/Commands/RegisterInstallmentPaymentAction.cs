﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : "6/26/2012 2:59:53 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using System.Data.Common;
using System.Linq;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.GMS.Core.ResultNotifications;
using US.Exceline.GMS.Modules.Economy.API;
using US.GMS.Core.Utils;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class RegisterInstallmentPaymentAction : USDBActionBase<SaleResultDC>
    {
        private int _memberbranchId = -1;
        private int _loggedBranchID = -1;
        private int _salePointId = -1;
        private string _user = string.Empty;
        private InstallmentDC _installment = null;
        private PaymentDetailDC _payementDetail = null;
        private string _gymCode = string.Empty;
        private ShopSalesDC _salesDetails = null;
        private string _state = string.Empty;

        public RegisterInstallmentPaymentAction(int memberBranchID, int loggedBranchID, string user, InstallmentDC installment, PaymentDetailDC paymentDetail, string gymCode, int salePointId, ShopSalesDC salesDetails)
        {
            _memberbranchId = memberBranchID;
            _loggedBranchID = loggedBranchID;
            _salePointId = salePointId;
            _user = user;
            _installment = installment;
            _payementDetail = paymentDetail;
            _gymCode = gymCode;
            _salesDetails = salesDetails;
        }

        protected override SaleResultDC Body(System.Data.Common.DbConnection connection)
        {
            DbTransaction transaction = null;
            int generatedArItemNo = -1;
            string spName = "ExceGMSGenerateInvoiceForOrder";
            SaleResultDC saleResult = new SaleResultDC();
            try
            {
                _state = "INVOICE";
                    DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                    
                    command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentID", System.Data.DbType.Int32, _installment.Id));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@IsInvoiceFeeAdded", System.Data.DbType.Boolean, false));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceType", System.Data.DbType.String, "PRINT"));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@IsShopInvoice", System.Data.DbType.Boolean, false));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@SMSInvoice", System.Data.DbType.Boolean, false));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@MemberBranchID", System.Data.DbType.Int32, _memberbranchId));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@InvoicBranchID", System.Data.DbType.Int32, _loggedBranchID));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@SettleWithPrepaid", System.Data.DbType.Boolean, false));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@SponsoredAmount", System.Data.DbType.Decimal, _installment.SponsoredAmount));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@Discount", System.Data.DbType.Decimal, _installment.Discount));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@IsSponsored", System.Data.DbType.Boolean, _installment.IsSponsored));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", System.Data.DbType.Int32, _installment.MemberId));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@NofoVisists", System.Data.DbType.Int32, _installment.NoOfVisits));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentText", System.Data.DbType.String, _installment.Text));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@MemberContractID", System.Data.DbType.Int32, _installment.MemberContractId));

                    DbDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        saleResult = new SaleResultDC();
                        saleResult.InvoiceNo = Convert.ToString(reader["Ref"]);
                        saleResult.AritemNo = Convert.ToInt32(reader["Aritemno"]);
                        saleResult.InkassoID = Convert.ToInt32(reader["CreditorNo"]);
                        saleResult.CustId = Convert.ToString(reader["CustID"]);
                        saleResult.KID = Convert.ToString(reader["KID"]);
                    }

                    reader.Close();
                    reader.Dispose();

                    if (saleResult.AritemNo > 0)
                    {
                        generatedArItemNo = saleResult.AritemNo;
                        saleResult.AritemNo = generatedArItemNo;
                        saleResult.InvoiceNo = saleResult.InvoiceNo;


                            if (_payementDetail.PaidAmount > 0)
                            {
                                PaymentProcessResult paymentResult = null;
                                List<IUSPTransaction> transactions = new List<IUSPTransaction>();
                        _state = "PAYMENT";
                                foreach (PayModeDC payMode in _payementDetail.PayModes)
                                {
                                    transactions.Add(GetTransaction(saleResult, payMode.Amount, payMode.PaymentTypeCode, payMode.PaidDate));
                                }
                                transaction = connection.BeginTransaction();
                                paymentResult = Transaction.RegsiterTransaction(transactions, transaction);
                                if (!paymentResult.ResultStatus)
                                {
                                    transaction.Rollback();
                                    saleResult.SaleStatus = SaleErrors.PAYMENTADDINGERROR;
                                    return saleResult;
                                }
                                else
                                {
                                    PayModeDC smsInvoiceEmail = _payementDetail.PayModes.FirstOrDefault(X => X.PaymentTypeCode == "EMAILSMSINVOICE");
                            _state = "ADD INVOICE TYPE";
                                    if (smsInvoiceEmail != null)
                                    {

                                        UpdateOrderInvoiceTypeAction orderTypeAction = new UpdateOrderInvoiceTypeAction(_installment.Id, _installment.SponsoredAmount, _installment.Discount, 2, generatedArItemNo);// set as printed invoice
                                        orderTypeAction.RunOnTransaction(transaction);
                                    }
                                    else
                                    {
                                        UpdateOrderInvoiceTypeAction orderTypeAction = new UpdateOrderInvoiceTypeAction(_installment.Id, _installment.SponsoredAmount, _installment.Discount, 4, generatedArItemNo);// set as printed invoice
                                        orderTypeAction.RunOnTransaction(transaction);
                                    }

                                    //------------------save shop transations data -------------
                            _state = "SHOP TRANSACTIONS";
                                    foreach (PayModeDC payMode in _payementDetail.PayModes)
                                    {
                                        ShopTransactionDC _shopTrans = new ShopTransactionDC();
                                        _shopTrans.BranchId = _loggedBranchID;
                                        _shopTrans.CreatedUser = _user;
                                        _shopTrans.CreatedDate = DateTime.Now;
                                        try
                                        {
                                            _shopTrans.Mode = (US.GMS.Core.SystemObjects.TransactionTypes)Enum.Parse(typeof(US.GMS.Core.SystemObjects.TransactionTypes), payMode.PaymentTypeCode, true);
                                        }
                                        catch
                                        {
                                            _shopTrans.Mode = US.GMS.Core.SystemObjects.TransactionTypes.NONE;
                                            _shopTrans.ModeText = payMode.PaymentTypeCode;
                                        }
                                        _shopTrans.Amount = payMode.Amount;
                                        _shopTrans.SalePointId = _salePointId;
                                        _shopTrans.EntityId = _installment.MemberId;
                                        _shopTrans.EntityRoleType = "MEM";
                                        _shopTrans.VoucherNo = payMode.VoucherNo;
                                        _shopTrans.InvoiceArItemNo = generatedArItemNo;
                                        SaveShopTransactionAction shopTransactionAction = new SaveShopTransactionAction(_shopTrans);
                                        shopTransactionAction.RunOnTransaction(transaction);
                                    }

                                    // Register Sale----------
                            _state = "SALE";
                                    if (_installment.PayerId > 0) //register sales for payer
                                        _salesDetails.EntitiyId = _installment.PayerId;
                            AddSalesDetailsAction salesDetailAction = new AddSalesDetailsAction(_salesDetails, _loggedBranchID, _user, generatedArItemNo, -1, string.Empty);
                                    salesDetailAction.RunOnTransaction(transaction);
                            _state = "GIFT CARD";
                                    List<ShopSalesItemDC> giftvouchers = new List<ShopSalesItemDC>();
                                    giftvouchers = _salesDetails.ShopSalesItemList.Where(X => string.IsNullOrEmpty(X.VoucherNo) == false).ToList();
                                    foreach (ShopSalesItemDC voucher in giftvouchers)
                                    {
                                        AddGiftVoucherDetailsAction voucherAction = new AddGiftVoucherDetailsAction(voucher, generatedArItemNo, _user, _installment.MemberId, _loggedBranchID);
                                        voucherAction.RunOnTransaction(transaction);
                                    }
                                            
                                            
                                    transaction.Commit();
                                    //update installment to set as invoiced
                            _state = "INVOICE STATUS";
                                    UpdateOrderInvoicedStatusAction updateaction = new UpdateOrderInvoicedStatusAction(_installment.Id);
                                    updateaction.Execute(EnumDatabase.Exceline, _gymCode);

                                    saleResult.SaleStatus = SaleErrors.SUCCESS;
                                    return saleResult;
                                }
                            }
                            else
                            {
                                transaction.Commit();
                        _state = "INVOICE STATUS";
                                //update installment to set as invoiced
                                UpdateOrderInvoicedStatusAction updateaction = new UpdateOrderInvoicedStatusAction(_installment.Id);
                                updateaction.Execute(EnumDatabase.Exceline, _gymCode);
                                saleResult.SaleStatus = SaleErrors.SUCCESS;
                                return saleResult;
                            }
                        }
                else if (saleResult.AritemNo == -2) //duplicate invoice 
                        {
                    saleResult.SaleStatus = SaleErrors.DUPLICATEINVOICE;
                            return saleResult;
                        }
                    else
                    {
                        saleResult.SaleStatus = SaleErrors.INVOICEADDINGERROR;
                        return saleResult;
                    }
            }
            catch (Exception ex)
            {
                if(transaction != null)
                transaction.Rollback();

                //add the records
                string installmentData = XMLUtils.SerializeDataContractObjectToXML(_installment);
                string saleData = XMLUtils.SerializeDataContractObjectToXML(_salesDetails);
                string paymentData = XMLUtils.SerializeDataContractObjectToXML(_payementDetail);
                AddShopSaleSummaryAction saleAction = new AddShopSaleSummaryAction(installmentData, saleData, paymentData, _memberbranchId, _loggedBranchID, "MEM", _salesDetails.TryNumber, _installment.Id, _state + ": " + ex.Message);
                int summaryId = saleAction.Execute(EnumDatabase.Exceline, _gymCode);

                saleResult.AritemNo = -1;
                saleResult.InvoiceNo = string.Empty;
                if (_salesDetails.TryNumber <= 2)
                    saleResult.SaleStatus = SaleErrors.RETRY;
                else
                    saleResult.SaleStatus = SaleErrors.ERROR;

                saleResult.Recover = true;
                saleResult.SummaryID = summaryId;
                return saleResult;
            }
        }

        private IUSPTransaction GetTransaction(SaleResultDC result, decimal paymentAmount, string paymentSourceName, DateTime paidDate)
        {
            IUSPTransaction transaction = new USPTransaction();
            transaction.TransType = GetTransType(paymentSourceName);
            transaction.PID = result.InkassoID.ToString();
            transaction.CID = result.CustId;
            if ((transaction.TransType == "OP" || transaction.TransType == "LOP" || transaction.TransType == "BS" || transaction.TransType == "DC") && paidDate != null && paidDate != DateTime.MinValue)
            {
                transaction.VoucherDate = paidDate;
            }
            else
            {
                transaction.VoucherDate = DateTime.Today;
            }
            transaction.DueDate = _installment.DueDate;
            transaction.ReceivedDate = DateTime.Today;
            transaction.RegDate = DateTime.Today;
            transaction.Amount = paymentAmount;
            transaction.Source = paymentSourceName;
            transaction.KID = result.KID;
            transaction.DebtorAccountNo = "";//TODO
            transaction.InvoiceNo = result.InvoiceNo;
            transaction.CreditorAccountNo = "";

            transaction.User = _user;
            return transaction;
        }

        private string GetTransType(string paymentType)
        {
            switch (paymentType)
            {
                case "CASH":
                    return "CSH";

                case "BANKTERMINAL":
                    return "TMN";

                case "OCR":
                    return "LOP";

                case "BANKSTATEMENT":
                    return "BS";

                case "DEBTCOLLECTION":
                    return "DC";

                case "ONACCOUNT":
                    return "OA";

                case "GIFTCARD":
                    return "GC";

                case "BANK":
                    return "BNK";

                case "PREPAIDBALANCE":
                    return "PB";

                case "AGRESSO":
                    return "AGR";

                case "STDTERMINAL":
                    return "STMN";

                case "VIPPS":
                    return "VIPP";
            }
            return string.Empty;
        }
    }
}

