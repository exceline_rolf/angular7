﻿using System;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymSmsSettingAction : USDBActionBase<List<GymSmsSettingsDC>>
    {
        private readonly int _branchId = -1;
        private string _userName;
        private string _gymCode = string.Empty;
        public GetGymSmsSettingAction(int branchId,string userName, string gymCode)
        {
            _branchId = branchId;
            _userName = userName;
            _gymCode = gymCode;
        }

        protected override List<GymSmsSettingsDC> Body(DbConnection connection)
        {
            List<GymSmsSettingsDC> gymSmsSettingList = new List<GymSmsSettingsDC>();            
            const string storedProcedureName = "USExceGMSGetGymSmsSetting";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _userName));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    GymSmsSettingsDC gymSmsSetting = new GymSmsSettingsDC();
                    gymSmsSetting.Id                            = Convert.ToInt32(reader["Id"]);
                    gymSmsSetting.BranchId                      = Convert.ToInt32(reader["BranchId"]);
                    gymSmsSetting.BranchName                    = Convert.ToString(reader["BranchName"]);
                    gymSmsSetting.IsExpress                     = Convert.ToBoolean(reader["IsExpressGym"]);
                    gymSmsSetting.Region                        = Convert.ToString(reader["region"]);
                    gymSmsSetting.SMSID                         = Convert.ToInt32(reader["SMSID"]);
                    gymSmsSetting.SenderName                    = Convert.ToString(reader["SenderName"]);                   gymSmsSetting.LastvisitInterval             = Convert.ToInt32(reader["LastvisitInterval"]);
                    gymSmsSetting.IsBirthday                    = Convert.ToBoolean(reader["IsBirthday"]);
                    gymSmsSetting.IsBookingReminder             = Convert.ToBoolean(reader["IsBookingReminder"]);
                    gymSmsSetting.ATGNotApprovedInterval1       = Convert.ToInt32(reader["ATGNotApprovedInterval1"]);
                    gymSmsSetting.ATGNotApprovedInterval2        = Convert.ToInt32(reader["ATGNotApprovedInterval2"]);
                    gymSmsSetting.ATGNotApprovedInterval3       = Convert.ToInt32(reader["ATGNotApprovedInterval3"]);
                    gymSmsSetting.ContractEnding                = Convert.ToInt32(reader["ContractEnding"]);
                    gymSmsSetting.ReminderDaysInterval          = Convert.ToInt32(reader["ReminderDaysInterval"]);
                    gymSmsSetting.NovisitsIntervalMessage1      = Convert.ToInt32(reader["NovisitsIntervalMessage1"]);
                    gymSmsSetting.NovisitsIntervalMessage2      = Convert.ToInt32(reader["NovisitsIntervalMessage2"]);
                    gymSmsSetting.DaysBeforeFreezeEnds          = Convert.ToInt32(reader["DaysBeforeFreezeEnds"]);
                    gymSmsSetting.IsPriceChange                 = Convert.ToBoolean(reader["IsPriceChange"]);
                    gymSmsSetting.IsReorderingShop              = Convert.ToBoolean(reader["IsReorderingShop"]);

                    gymSmsSetting.IsAccessFreeze                = Convert.ToBoolean(reader["IsAccessFreeze"]);
                    gymSmsSetting.IsAccessOutsideProfile        = Convert.ToBoolean(reader["IsAccessOutsideProfile"]);
                    gymSmsSetting.IsAccessClosedPeriod          = Convert.ToBoolean(reader["IsAccessClosedPeriod"]);
                    gymSmsSetting.IsAccessClosedOutsideOpenHours = Convert.ToBoolean(reader["IsAccessClosedOutsideOpenHours"]);
                    gymSmsSetting.IsAccessDuebalance            = Convert.ToBoolean(reader["IsAccessDuebalance"]);
                    gymSmsSetting.IsAccessDueUnpaid             = Convert.ToBoolean(reader["IsAccessDueUnpaid"]);
                    gymSmsSetting.IsAccessDueOnAccount          = Convert.ToBoolean(reader["IsAccessDueOnAccount"]);
                    gymSmsSetting.IsAccessRecentlyVisit         = Convert.ToBoolean(reader["IsAccessRecentlyVisit"]);
                    gymSmsSetting.IsAccessInfoAntiDoping        = Convert.ToBoolean(reader["IsAccessInfoAntiDoping"]);
                    gymSmsSetting.IsAccessInfoContractEnds      = Convert.ToBoolean(reader["IsAccessInfoContractEnds"]);
                    gymSmsSetting.AccessInfoPunchesLowestNo     = Convert.ToInt32(reader["AccessInfoPunchesLowestNo"]);
                    gymSmsSetting.IsAccessInfoIsFollowUpPopUp   = Convert.ToBoolean(reader["IsAccessInfoIsFollowUpPopUp"]);
                    gymSmsSetting.IsAccessInfoInformTrainer     = Convert.ToBoolean(reader["IsAccessInfoInformTrainer"]);
                    gymSmsSettingList.Add(gymSmsSetting);
                }
                reader.Close();

                if (gymSmsSettingList.Count > 0)
                {
                    GetSMSSendingTimesAction action = new GetSMSSendingTimesAction(_branchId);
                    gymSmsSettingList[0].SendingTimes = action.Execute(EnumDatabase.Exceline, _gymCode);

                    GetSMSProhibittedTimesAction prohibitTimesAction = new GetSMSProhibittedTimesAction(_branchId);
                    gymSmsSettingList[0].ProhibittedTimes = prohibitTimesAction.Execute(EnumDatabase.Exceline, _gymCode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return gymSmsSettingList;
        }
    }
}




