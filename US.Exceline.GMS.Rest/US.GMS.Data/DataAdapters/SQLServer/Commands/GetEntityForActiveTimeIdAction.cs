﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US_DataAccess;
using System.Linq;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetEntityForActiveTimeIdAction : USDBActionBase<List<BookingEntityDc>>
  {
      private int _activeTimeId;
      private string _roleId = string.Empty;
        private string _roleType = string.Empty;
      public GetEntityForActiveTimeIdAction(int activeTimeId, string roleId, string roleType)
      {
          _activeTimeId = activeTimeId;
          _roleId = roleId;
          _roleType = roleType;
      }

      protected override List<BookingEntityDc> Body(DbConnection connection)
        {
            var entityLst = new List<BookingEntityDc>();
            const string spName = "USExceGMSGetEntityForActiveTimeId";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _activeTimeId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@RoleId", DbType.String, _roleId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@RoleType", DbType.String, _roleType));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var bookingEntity = new BookingEntityDc {Id = Convert.ToInt32(reader["ID"])};
                    if (_roleId.Equals("MEM"))
                    {
                        bookingEntity.CustId = reader["Number"].ToString();
                        bookingEntity.Name = reader["Name"].ToString();
                        bookingEntity.MemberRole = reader["RoleId"].ToString();
                        if (reader["ArrivalDateTime"] != DBNull.Value)
                        {
                            bookingEntity.ArrivalDateTime = Convert.ToDateTime(reader["ArrivalDateTime"]);
                            bookingEntity.IsArrived = true;
                        }
                        if (reader["Paid"] != DBNull.Value)
                        bookingEntity.IsPaid = Convert.ToBoolean(reader["Paid"]);
                        if (reader["ARItemNo"] != DBNull.Value)
                        bookingEntity.ArItemNo = Convert.ToInt32(reader["ARItemNo"]);
                        if (reader["Amount"] != DBNull.Value)
                        bookingEntity.Amount = Convert.ToDecimal(reader["Amount"]);
                        if (reader["AvailableVisit"] != DBNull.Value)
                            bookingEntity.AvailableVisit = Convert.ToInt32(reader["AvailableVisit"]);
                        if (reader["BranchId"] != DBNull.Value)
                            bookingEntity.BranchId = Convert.ToInt32(reader["BranchId"]);
                        if (reader["ItemType"] != DBNull.Value)
                            bookingEntity.ItemTypeList = reader["ItemType"].ToString().Trim().Split(',').ToList();
                        if (reader["TelMobile"] != DBNull.Value)
                            bookingEntity.Mobile = reader["TelMobile"].ToString().Trim();
                        if (reader["ParentId"] != DBNull.Value)
                            bookingEntity.GuardianId = Convert.ToInt32(reader["ParentId"]);


                        entityLst.Add(bookingEntity);
                    }
                    else
                    {
                        bookingEntity.Name = reader["Name"].ToString();
                        entityLst.Add(bookingEntity);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return entityLst;
        }
    }
}
