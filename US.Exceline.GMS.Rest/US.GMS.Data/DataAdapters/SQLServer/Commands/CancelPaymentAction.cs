﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class CancelPaymentAction : USDBActionBase<int>
    {
        private int _paymentId = -1;
        private bool _keeppayment = false;
        private string _user = string.Empty;
        private string _comment = string.Empty;

        public CancelPaymentAction(int paymentId, bool keeppayment, string user, string comment)
        {
            _paymentId = paymentId;
            _keeppayment = keeppayment;
            _user = user;
            _comment = comment;
        }
        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSCancelPayment";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNo", System.Data.DbType.Int32, _paymentId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@KeepPayment", System.Data.DbType.Boolean, _keeppayment));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Comment", System.Data.DbType.String, _comment));

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@Result";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                command.Parameters.Add(param);
                command.ExecuteNonQuery();
                return Convert.ToInt32(param.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
