﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "6/20/2012 6:06:42 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetTaskTemplateExtendedFieldsAction : USDBActionBase<List<ExtendedTaskTemplateDC>>
    {
        private int _taskCategoryId = -1;

        public GetTaskTemplateExtendedFieldsAction(int taskCategoryId)
        {
            this._taskCategoryId = taskCategoryId;
        }
        protected override List<ExtendedTaskTemplateDC> Body(DbConnection connection)
        {
            List<ExtendedTaskTemplateDC> extFieldTemplateList = new List<ExtendedTaskTemplateDC>();
            string storedProcedureName = "USExceGMSAdminGetTaskCategoryExtFields";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TaskCategoryId", DbType.Int32, _taskCategoryId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedTaskTemplateDC extTaskTemplate = new ExtendedTaskTemplateDC();
                    extTaskTemplate.Id = Convert.ToInt32(reader["Id"]);
                    extTaskTemplate.Title = reader["Name"].ToString();
                    extTaskTemplate.FieldType = (CommonUITypes)Enum.Parse(typeof(CommonUITypes), reader["FieldType"].ToString(), true);
                    
                    extFieldTemplateList.Add(extTaskTemplate);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return extFieldTemplateList;
        }
    }
}
