﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateShopAccountAction : USDBActionBase<bool>
    {
        private int _branchId = -1;
        private string _createdUser = string.Empty;
        private int _memeberId = -1;
        private decimal _shopPaymentAmount = 0.00M;

        public UpdateShopAccountAction(int memeberId, decimal shopPaymentAmount, int branchId, string createdUser)
        {
            _memeberId = memeberId;
            _createdUser = createdUser;
            _branchId = branchId;
            _shopPaymentAmount = shopPaymentAmount;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSUpdateShopAccount";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memeberId", System.Data.DbType.Int32, _memeberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", System.Data.DbType.String, _createdUser));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@shopPaymentAmount", System.Data.DbType.Decimal, _shopPaymentAmount));

                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            string spName = "USExceGMSUpdateShopAccount";
            try
            {
                DbCommand command = new SqlCommand();
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.CommandText = spName;
               
                command.Parameters.Add(DataAcessUtils.CreateParam("@memeberId", System.Data.DbType.Int32, _memeberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", System.Data.DbType.String, _createdUser));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@shopPaymentAmount", System.Data.DbType.Decimal ,_shopPaymentAmount));

                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
