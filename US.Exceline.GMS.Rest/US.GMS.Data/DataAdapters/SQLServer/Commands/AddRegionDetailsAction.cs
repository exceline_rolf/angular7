﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    class AddRegionDetailsAction :USDBActionBase<bool>
    {
        private RegionDC _region;

        public AddRegionDetailsAction(RegionDC region)
        {
            _region = region;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedure = "USExceGMSAddUpdateRegion";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@code", DbType.String, _region.Code));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _region.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@countryId", DbType.String, _region.CountryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@description", DbType.String, _region.Description));

                SqlParameter output = new SqlParameter("@outId", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return true;
        }
    }
}
