﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class AddCreditNoteForReturnItemAction : USDBActionBase<int>
    {
        private readonly int _memberId = -1;
        private readonly int _branchId = -1;
        private readonly string _user = string.Empty;
        private readonly ContractItemDC _contractItem;

        public AddCreditNoteForReturnItemAction(int memberId, ContractItemDC contractItem, int branchId, string user)
        {
            _contractItem = contractItem;
            _memberId = memberId;
            _branchId = branchId;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            try
            {
                const string storedProcedureName = "USExceGMSShopAddCreditNoteForReturnItem";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@articleId", DbType.Int32, _contractItem.ArticleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@quantity", DbType.Int32, _contractItem.Quantity));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@creditNoteAmount", DbType.Decimal, _contractItem.Price));
                if (_contractItem.ReturnComment == null)
                {
                    _contractItem.ReturnComment = string.Empty;
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@returnComment", DbType.String, _contractItem.ReturnComment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@outId";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();

                int result = -1;
                if (output.Value != null)
                {
                    result = (Convert.ToInt32(output.Value));
                }
                else
                {
                    result = -1;
                }

                return result;
            }
            catch
            {
                throw;
            }
        }
    }
}