﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.Exceline.GMS.Modules.Economy.Core;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetClaimInfoForShopInstallmentPaymentAction : USDBActionBase<IUSPClaim>
    {
        private int _memberID = -1;
        private int _memberBranchID = -1;
        private int _loggedBranchID = -1;
        private int _installmentID = -1;

        public GetClaimInfoForShopInstallmentPaymentAction(int memberID, int memberBranchID, int loggedBranchID, int installmentID)
        {
            _memberID = memberID;
            _memberBranchID = memberBranchID;
            _installmentID = installmentID;
            _loggedBranchID = loggedBranchID;
        }

        protected override IUSPClaim Body(System.Data.Common.DbConnection connection)
        {
            IUSPClaim claim = new USPClaim();
            IUSPDebtor debtor = new USPDebtor();
            IUSPCreditor creditor = new USPCreditor();
            IUSPAddress debtorAddress = new USPAddress();
            DbDataReader reader = null;
            string spName = "USExceGMSGetClaimInfoForShopInstallmentPayments";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", System.Data.DbType.Int32, _memberID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberBranchID", System.Data.DbType.Int32, _memberBranchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@loggedBranchID", System.Data.DbType.Int32, _loggedBranchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@installmentID", System.Data.DbType.Int32, _installmentID));

                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    debtor.DebtorEntityID = Convert.ToInt32(reader["DebtorEntNo"]);
                    debtor.DebtorcustumerID = Convert.ToString(reader["CustId"]);
                    debtor.DebtorFirstName = Convert.ToString(reader["FirstName"]);
                    debtor.DebtorSecondName = Convert.ToString(reader["LastName"]);
                    claim.NameInContract = Convert.ToString(reader["NameOnContract"]);
                    debtor.DebtorEntityRoleID = Convert.ToInt32(reader["DebtorEntityRoleID"]);
                    debtor.DebtorBirthDay = Convert.ToDateTime(reader["DebtorBirthDay"]).ToString("MM-dd-yyyy");

                    debtorAddress.Address1 = Convert.ToString(reader["Addr1"]);
                    debtorAddress.Address2 = Convert.ToString(reader["Addr2"]);
                    debtorAddress.Address3 = Convert.ToString(reader["Addr3"]);
                    debtorAddress.Email = Convert.ToString(reader["Email"]);
                    debtorAddress.TelHome = Convert.ToString(reader["TelHome"]);
                    debtorAddress.TelMobile = Convert.ToString(reader["TelMobile"]);
                    debtorAddress.TelWork = Convert.ToString(reader["TelWork"]);

                    debtor.DebtorAddressList.Add(debtorAddress);

                    creditor.CreditorInkassoID = Convert.ToString(reader["CreditorInkassoID"]);
                    creditor.CreditorName = Convert.ToString(reader["CreditorName"]);
                    creditor.CreditorAccountNo = Convert.ToString(reader["CreditorAccountNo"]);
                    creditor.CreditorEntityRoleID = Convert.ToInt32(reader["CreditorRoleId"]);

                    claim.InvoiceTypeId = Convert.ToInt32(reader["InvoiceType"]);
                    claim.InvoiceRef = Convert.ToString(reader["InvoiceRef"]);
                    claim.Group = Convert.ToString(reader["GroupId"]);
                    //claim.ContractExpire = Convert.ToDateTime(reader["ContractExpire"]).ToString("MM-dd-yyyy");
                    claim.InstallmentKID = Convert.ToString(reader["KID"]);
                    //claim.ContractNumber = Convert.ToString(reader["ContractNo"]);
                    claim.InstallmentNumber = Convert.ToString(reader["InstallmentNo"]);
                    claim.InvoiceType = InvoiceTypes.Invoice;
                    claim.BranchNumber = _memberBranchID.ToString();
                    claim.Debtor = debtor;
                    claim.Creditor = creditor;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return claim;
        }

        public IUSPClaim RunOnTransaction(DbTransaction transaction)
        {
            IUSPClaim claim = new USPClaim();
            IUSPDebtor debtor = new USPDebtor();
            IUSPCreditor creditor = new USPCreditor();
            IUSPAddress debtorAddress = new USPAddress();
            DbDataReader reader = null;
            string spName = "USExceGMSGetClaimInfoForShopInstallmentPayments";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Connection = transaction.Connection;
                command.Transaction = transaction;

                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", System.Data.DbType.Int32, _memberID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberBranchID", System.Data.DbType.Int32, _memberBranchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@loggedBranchID", System.Data.DbType.Int32, _loggedBranchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@installmentID", System.Data.DbType.Int32, _installmentID));

                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    debtor.DebtorEntityID = Convert.ToInt32(reader["DebtorEntNo"]);
                    debtor.DebtorcustumerID = Convert.ToString(reader["CustId"]);
                    debtor.DebtorFirstName = Convert.ToString(reader["FirstName"]);
                    debtor.DebtorSecondName = Convert.ToString(reader["LastName"]);
                    claim.NameInContract = Convert.ToString(reader["NameOnContract"]);
                    debtor.DebtorEntityRoleID = Convert.ToInt32(reader["DebtorEntityRoleID"]);
                    debtor.DebtorBirthDay = Convert.ToDateTime(reader["DebtorBirthDay"]).ToString("MM-dd-yyyy");

                    debtorAddress.Address1 = Convert.ToString(reader["Addr1"]);
                    debtorAddress.Address2 = Convert.ToString(reader["Addr2"]);
                    debtorAddress.Address3 = Convert.ToString(reader["Addr3"]);
                    debtorAddress.Email = Convert.ToString(reader["Email"]);
                    debtorAddress.TelHome = Convert.ToString(reader["TelHome"]);
                    debtorAddress.TelMobile = Convert.ToString(reader["TelMobile"]);
                    debtorAddress.TelWork = Convert.ToString(reader["TelWork"]);

                    debtor.DebtorAddressList.Add(debtorAddress);

                    creditor.CreditorInkassoID = Convert.ToString(reader["CreditorInkassoID"]);
                    creditor.CreditorName = Convert.ToString(reader["CreditorName"]);
                    creditor.CreditorAccountNo = Convert.ToString(reader["CreditorAccountNo"]);
                    creditor.CreditorEntityRoleID = Convert.ToInt32(reader["CreditorRoleId"]);

                    claim.InvoiceTypeId = Convert.ToInt32(reader["InvoiceType"]);
                    claim.InvoiceRef = Convert.ToString(reader["InvoiceRef"]);
                    claim.Group = Convert.ToString(reader["GroupId"]);
                    //claim.ContractExpire = Convert.ToDateTime(reader["ContractExpire"]).ToString("MM-dd-yyyy");
                    claim.InstallmentKID = Convert.ToString(reader["KID"]);
                    //claim.ContractNumber = Convert.ToString(reader["ContractNo"]);
                    claim.InstallmentNumber = Convert.ToString(reader["InstallmentNo"]);
                    claim.InvoiceType = InvoiceTypes.Invoice;
                    claim.BranchNumber = _memberBranchID.ToString();
                    claim.Debtor = debtor;
                    claim.Creditor = creditor;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return claim;
        }
    }
}
