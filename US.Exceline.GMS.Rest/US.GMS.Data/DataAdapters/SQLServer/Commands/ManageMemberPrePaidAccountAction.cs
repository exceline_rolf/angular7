﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class ManageMemberPrePaidAccountAction : USDBActionBase<bool>
    {
        private readonly MemberShopAccountDC _memberShopAccount;
        private readonly string _mode;
        private readonly string _user;
        private readonly int _branchId;
        private readonly int _salePointId = -1;
        private readonly string _gymCode = string.Empty;

        public ManageMemberPrePaidAccountAction(MemberShopAccountDC memberShopAccount, string mode, string user, int branchId, int salePointId, string gymCode)
        {
            _memberShopAccount = memberShopAccount;
            _mode = mode;
            _user = user;
            _gymCode = gymCode;
            _salePointId = salePointId;
            _branchId = branchId;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSManageMembershipManageMemberPrepaidAccount";
            try
            {
                if (_memberShopAccount.CreditaccountItemList.Count > 0)
                {
                    foreach (MemberShopAccountItemDC item in _memberShopAccount.CreditaccountItemList)
                    {
                        DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@mode", DbType.String, _mode));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberShopAccount.MemberId));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@payment", DbType.Decimal, item.PaymentAmount));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@payMode", DbType.Int32, item.PayModeId));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@payModeCode", DbType.String, item.PayMode));                        
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Boolean, _memberShopAccount.ActiveStatus));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@deactivateReason", DbType.String, _memberShopAccount.DeactivateReason));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                        cmd.ExecuteScalar();

                    }
                }


                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
