﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : "6/26/2012 2:59:53 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
   public class UpdateSponsoredAmoutAction : USDBActionBase<bool>
    {
     
        private int _installmentId = -1;
        private decimal _sposredAmount = 0.00M;

        public UpdateSponsoredAmoutAction(int installmentId, decimal sponsorAmount)
        {
            _installmentId = installmentId;
            _sposredAmount = sponsorAmount;          
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSUpdateSponserdAmountByInstallment";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@installmentId", System.Data.DbType.Int32, _installmentId)); ;
                command.Parameters.Add(DataAcessUtils.CreateParam("@sponsorAmount", System.Data.DbType.Decimal, _sposredAmount));

                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            string spName = "USExceGMSUpdateSponserdAmountByInstallment";
            try
            {
                DbCommand command = new SqlCommand();
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.CommandText = spName;
                command.Parameters.Add(DataAcessUtils.CreateParam("@installmentId", System.Data.DbType.Int32, _installmentId)); ;
                command.Parameters.Add(DataAcessUtils.CreateParam("@sponsorAmount", System.Data.DbType.Decimal, _sposredAmount));

                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
