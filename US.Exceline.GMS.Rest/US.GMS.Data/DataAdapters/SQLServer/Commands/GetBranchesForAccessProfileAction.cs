﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetBranchesForAccessProfileAction : USDBActionBase<List<int>>
    {
        private readonly int _accessProfileId;

        public GetBranchesForAccessProfileAction(int accessProfileId)
        {
            _accessProfileId = accessProfileId;
        }

        protected override List<int> Body(DbConnection connection)
        {
            List<int> branchIdList = new List<int>();
            string sp = "USExceGMSAdminGetBranchesByAccessProfileId";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, sp);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@accessProfileId", DbType.Int32, _accessProfileId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int branchId = Convert.ToInt32(reader["ID"]);
                    branchIdList.Add(branchId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return branchIdList;
        }
    }
}
