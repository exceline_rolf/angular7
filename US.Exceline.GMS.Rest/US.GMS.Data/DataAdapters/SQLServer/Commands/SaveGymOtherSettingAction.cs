﻿using System;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveGymOtherSettingAction : USDBActionBase<bool>
    {
        private GymOtherSettingsDC _gymOtherSetting = new GymOtherSettingsDC();
        private bool _returnValue;

        public SaveGymOtherSettingAction(int branchId, GymOtherSettingsDC gymOtherSetting)
        {
            _gymOtherSetting = gymOtherSetting;
            _gymOtherSetting.BranchId = branchId;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedure = "USExceGMSSaveGymOtherSetting";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ID", DbType.Int32, _gymOtherSetting.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherTimeOutAfter", DbType.Int32, _gymOtherSetting.TimeOutAfter));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherPhoneCode", DbType.String, _gymOtherSetting.PhoneCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherIsShopAvailable", DbType.Boolean, _gymOtherSetting.IsShopAvailable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherIsAntiDopingRequired", DbType.Boolean, _gymOtherSetting.IsAntiDopingRequired));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherCategoryFollowUpNoVisits", DbType.Boolean, _gymOtherSetting.CategoryFollowUpNoVisits));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherIsFollowUpNewPerson", DbType.Boolean, _gymOtherSetting.FollowUpNewPerson));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherIsPrintATGAuthorization", DbType.Boolean, _gymOtherSetting.IsPrintATGAuthorization));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherIsFreezeConfirmationPopUp", DbType.Boolean, _gymOtherSetting.IsFreezeConfirmationPopUp));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherLoginShop", DbType.Boolean, _gymOtherSetting.IsLoginShop));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherIsLogoutFromShopAfterSale", DbType.Boolean, _gymOtherSetting.IsLogoutFromShopAfterSale));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherGmt", DbType.Decimal, _gymOtherSetting.Gmt));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsBookingBackDate", DbType.Boolean, _gymOtherSetting.IsBookingBackDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPrieviewWhenPrint", DbType.Boolean, _gymOtherSetting.IsPrieviewWhenPrint));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AnonymizingTimePeriod", DbType.Int32, _gymOtherSetting.AnonymizingTimePeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsCalendarPriority", DbType.Boolean, _gymOtherSetting.IsCalendarPriority));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsIBookingIntegrated", DbType.Boolean, _gymOtherSetting.IsIBookingIntegrated));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherIsValidateShopGymId", DbType.Boolean, _gymOtherSetting.IsValidateShopGymId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsUsingIntroducedBy", DbType.Boolean, _gymOtherSetting.IsUsingIntroducedBy));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SortBranchesById", DbType.Boolean, _gymOtherSetting.SortBranchesById));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsUsingGantner", DbType.Boolean, _gymOtherSetting.IsUsingGantner));

                cmd.ExecuteNonQuery();
                _returnValue = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _returnValue;
        }
    }
}
