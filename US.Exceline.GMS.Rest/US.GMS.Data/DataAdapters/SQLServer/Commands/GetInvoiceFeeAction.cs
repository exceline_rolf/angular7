﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetInvoiceFeeAction : USDBActionBase<CreditorOrderLine>
    {
        private int _branchId = -1;
        private string _invoiceType = string.Empty;
        public GetInvoiceFeeAction(int branchId, string invoiceType)
        {
            _invoiceType = invoiceType;
            _branchId = branchId;
        }

        protected override CreditorOrderLine Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSGetInvoiceFee";
            CreditorOrderLine orderline = null;
            DbDataReader reader = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceType", System.Data.DbType.String, _invoiceType));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));

                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    orderline = new CreditorOrderLine();
                    orderline.Amount = Convert.ToDecimal(reader["DefaultPrice"]);
                    orderline.UnitPrice = Convert.ToDecimal(reader["DefaultPrice"]);
                    orderline.ArticleNo = Convert.ToString(reader["ID"]);
                    orderline.Text = Convert.ToString(reader["Description"]);
                    orderline.ArticleText = Convert.ToString(reader["Description"]);
                    orderline.Balance = Convert.ToDecimal(reader["DefaultPrice"]);
                }

                if (reader != null)
                    reader.Close();
                return orderline;
            }
            catch
            {
                if (reader != null)
                    reader.Close();
                return null;
            }
        }

        public CreditorOrderLine RunOnTransaction(DbTransaction transaction)
        {
            string spName = "USExceGMSGetInvoiceFee";
            CreditorOrderLine orderline = null;
            DbDataReader reader = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Transaction = transaction;
                command.Connection = transaction.Connection;
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceType", System.Data.DbType.String, _invoiceType));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));

                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    orderline = new CreditorOrderLine();
                    orderline.Amount = Convert.ToDecimal(reader["DefaultPrice"]);
                    orderline.UnitPrice = Convert.ToDecimal(reader["DefaultPrice"]);
                    orderline.ArticleNo = Convert.ToString(reader["ID"]);
                    orderline.Text = Convert.ToString(reader["Description"]);
                    orderline.ArticleText = Convert.ToString(reader["Description"]);
                    orderline.Balance = Convert.ToDecimal(reader["DefaultPrice"]);
                }

                if (reader != null)
                    reader.Close();
                return orderline;
            }
            catch
            {
                if (reader != null)
                    reader.Close();
                return null;
            }
        }


    }
}
