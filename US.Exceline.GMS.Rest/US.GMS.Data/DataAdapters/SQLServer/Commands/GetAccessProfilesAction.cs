﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using System.Data.Common;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetAccessProfilesAction : USDBActionBase<List<ExceAccessProfileDC>>
    {
        private readonly string _gymCode = string.Empty;
        private Gender _gender = Gender.ALL;
        public GetAccessProfilesAction(string gymCode,  Gender gender)
        {
            _gymCode = gymCode;
            _gender = gender;
        }

        protected override List<ExceAccessProfileDC> Body(DbConnection connection)
        {
            string spName = "USExceGMSGetAccessProfiles";
            List<ExceAccessProfileDC> accessProfileList = new List<ExceAccessProfileDC>();
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@Gender", DbType.String, _gender.ToString()));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExceAccessProfileDC profile = new ExceAccessProfileDC();
                    profile.Id = Convert.ToInt32(reader["ID"]);
                    profile.AccessProfileName = Convert.ToString(reader["Name"]);
                    profile.IsActive = Convert.ToBoolean(reader["ActiveStatus"]);
                    profile.AllowExpressGym = Convert.ToBoolean(reader["AllowExpressGym"]);
                    profile.AllowExtraActivity = Convert.ToBoolean(reader["AllowExtraActivity"]);
                    profile.Gender = (Gender)Enum.Parse(typeof(Gender), reader["Gender"].ToString(), true);
                    accessProfileList.Add(profile);
                }
                reader.Close();

                foreach (ExceAccessProfileDC accProfile in accessProfileList)
                {
                    GetAccessProfileTimeAction action = new GetAccessProfileTimeAction(accProfile.Id);
                    accProfile.AccessTimeList = action.Execute(EnumDatabase.Exceline, _gymCode);
                }

                return accessProfileList;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
