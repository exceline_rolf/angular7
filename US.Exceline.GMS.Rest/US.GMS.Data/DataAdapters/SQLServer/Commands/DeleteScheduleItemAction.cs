﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteScheduleItemAction : USDBActionBase<bool>
    {
        private int _scheduleItemId;
        private bool _activeStatus;
        private string _user = string.Empty;
        public DeleteScheduleItemAction(int scheduleItemId, bool activeStatus, string user)
        {
            _scheduleItemId = scheduleItemId;
            _activeStatus = activeStatus;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string storedProcedureName = "USExceGMSDeleteScheduleItem";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleItemId", DbType.Int32, _scheduleItemId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Int32, _activeStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _user));

                cmd.ExecuteNonQuery();
                return true;
            }

            catch
            {
                return false;
            }
        }
    }
}

