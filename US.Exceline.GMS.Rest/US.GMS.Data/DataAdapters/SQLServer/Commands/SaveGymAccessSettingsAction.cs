﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters
{
    class SaveGymAccessSettingsAction : USDBActionBase<bool>
    {
        private int _branchId = -1;
        private GymAccessSettingDC _accessTimeSettings = new GymAccessSettingDC();
        private string _gymCode = string.Empty;

        public SaveGymAccessSettingsAction(int branchId, GymAccessSettingDC accessTimeSettings, string gymCode)
        {
            _branchId = branchId;
            _accessTimeSettings = accessTimeSettings;
            _gymCode = gymCode;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedure = "USExceGMSSaveGymAccessSettings";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", System.Data.DbType.Int32, _accessTimeSettings.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CountVisitAfter", System.Data.DbType.Int32, _accessTimeSettings.CountVisitAfter));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BlockAccessNoOfUnpaidInvoices", System.Data.DbType.Int32, _accessTimeSettings.BlockAccessNoOfUnpaidInvoices));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BlockAccessDueBalance", System.Data.DbType.Decimal, _accessTimeSettings.BlockAccessDueBalance));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsMemCardExportAsGuestCard", System.Data.DbType.Boolean, _accessTimeSettings.IsMemCardExportAsGuestCard));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BlockAccessOnAccountBalance", System.Data.DbType.Decimal, _accessTimeSettings.BlockAccessOnAccountBalance));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsReceptionAllowCustomerScreen", System.Data.DbType.Boolean, _accessTimeSettings.IsReceptionAllowCustomerScreen));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MinutesBetweenAccess", System.Data.DbType.Int32, _accessTimeSettings.MinutesBetweenAccess));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ComPort", System.Data.DbType.String, _accessTimeSettings.ComPort));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SecondsViewCustomerScreen", System.Data.DbType.Int32, _accessTimeSettings.SecondsViewCustomerScreen));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsExtraActivity", System.Data.DbType.Boolean, _accessTimeSettings.IsExtraActivity));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CheckFingerPrint", System.Data.DbType.Boolean, _accessTimeSettings.CheckFingerPrint));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsSendSms", DbType.Boolean, _accessTimeSettings.IsSendSms));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccIsDisplayName", DbType.Boolean, _accessTimeSettings.AccIsDisplayName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccIsDisplayCustomerNumber", DbType.Boolean, _accessTimeSettings.AccIsDisplayCustomerNumber));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccIsDisplayContractTemplate", DbType.Boolean, _accessTimeSettings.AccIsDisplayContractTemplate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccIsDisplayHomeGym", DbType.Boolean, _accessTimeSettings.AccIsDisplayHomeGym));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccIsDisplayContractEndDate", DbType.Boolean, _accessTimeSettings.AccIsDisplayContractEndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccIsDisplayBirthDate", DbType.Boolean, _accessTimeSettings.AccIsDisplayBirthDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccIsDisplayAge", DbType.Boolean, _accessTimeSettings.AccIsDisplayAge));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccIsDisplayLastVisit", DbType.Boolean, _accessTimeSettings.AccIsDisplayLastVisit));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccIsDisplayCreditBalance", DbType.Boolean, _accessTimeSettings.AccIsDisplayCreditBalance));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccIsDisplayPicture", DbType.Boolean, _accessTimeSettings.AccIsDisplayPicture));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccIsDisplayLastAccess", DbType.Boolean, _accessTimeSettings.AccIsDisplayLastAccess));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccIsDisplayNotification", DbType.Boolean, _accessTimeSettings.AccIsDisplayNotification));

                cmd.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
            
        }
    }
}
