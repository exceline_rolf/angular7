﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    class DeleteAccessProfileTimeAction : USDBActionBase<bool>
    {
        private ExceAccessProfileTimeDC _accessProfileTime = null;

        public DeleteAccessProfileTimeAction(int branchId, ExceAccessProfileTimeDC accessProfileTime)
        {
            this._accessProfileTime = accessProfileTime;
        }
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string storedProcedureName = "USExceGMSDeleteAccessTimeItem";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@profTimeItemId", DbType.Int32, _accessProfileTime.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@profileId", DbType.Int32, _accessProfileTime.AccessProfileId));

                cmd.ExecuteNonQuery();
                return true;
            }

            catch
            {
                return false;
            }
        }
    }
}
