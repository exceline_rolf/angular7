﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    class GetAccessProfileTimeAction : USDBActionBase<List<ExceAccessProfileTimeDC>>
    {
        private int _profileId = -1;
        public GetAccessProfileTimeAction(int profileId)
        {
            this._profileId = profileId;
        }
        protected override List<ExceAccessProfileTimeDC> Body(System.Data.Common.DbConnection connection)
        {
            string GetAccessProfileTimes = "USExceGMSGetAccessProfilesTimes";
            List<ExceAccessProfileTimeDC> profileTimeList = new List<ExceAccessProfileTimeDC>();
            try
            {
                DbCommand cmd3 = CreateCommand(CommandType.StoredProcedure, GetAccessProfileTimes);
                cmd3.Parameters.Add(DataAcessUtils.CreateParam("@profileId", System.Data.DbType.Int32, _profileId));
                DbDataReader reader3 = cmd3.ExecuteReader();

                while (reader3.Read())
                {
                    ExceAccessProfileTimeDC profileTime = new ExceAccessProfileTimeDC();
                    profileTime.Id = Convert.ToInt32(reader3["ID"]);
                    profileTime.AccessProfileId = Convert.ToInt32(reader3["AccessProfileId"]);
                    profileTime.FromTime = DateTime.Parse(((reader3["FromTime"].ToString())));
                    profileTime.ToTime = DateTime.Parse(((reader3["ToTime"].ToString())));
                    profileTime.Monday = Convert.ToBoolean(reader3["Monday"]);
                    profileTime.Tuesday = Convert.ToBoolean(reader3["Tuesday"]);
                    profileTime.Wednesday = Convert.ToBoolean(reader3["Wednesday"]);
                    profileTime.Thursday = Convert.ToBoolean(reader3["Thursday"]);
                    profileTime.Friday = Convert.ToBoolean(reader3["Friday"]);
                    profileTime.Saturday = Convert.ToBoolean(reader3["Saturday"]);
                    profileTime.Sunday = Convert.ToBoolean(reader3["Sunday"]);
                    profileTimeList.Add(profileTime);
                }
                reader3.Close();
                return profileTimeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
