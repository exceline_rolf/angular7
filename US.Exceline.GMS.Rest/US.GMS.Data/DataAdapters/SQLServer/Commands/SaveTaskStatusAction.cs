﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveTaskStatusAction : USDBActionBase<bool>
    {
        private readonly string _taskName = string.Empty;
        private readonly string _description = string.Empty;
        private readonly int _branchId = -1;
        private readonly int _status = 1;
        public SaveTaskStatusAction(string taskName, string description, int branchId, int status)
        {
            _taskName = taskName;
            _description = description;
            _branchId = branchId;
            _status = status;
            
        }
        protected override bool Body(DbConnection connection)
        {
            const string spName = "USExceGMSAddAutomatedTaskHistory";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@TaskName", System.Data.DbType.String, _taskName));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Description", System.Data.DbType.String, _description));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ProcessStatus", System.Data.DbType.Int32, _status));

                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
