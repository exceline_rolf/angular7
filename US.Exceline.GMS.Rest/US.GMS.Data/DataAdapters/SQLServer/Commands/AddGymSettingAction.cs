﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Data.Common;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using System.Data;
using System.Data.SqlClient;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class AddGymSettingAction : USDBActionBase<bool>
    {
        private ExceGymSettingDC _gymSetting = new ExceGymSettingDC();
        private bool _returnValue = false;
        private ExceAccessProfileDC _accessProfile = new ExceAccessProfileDC();
        private string _gymCode = string.Empty;

        public AddGymSettingAction(int branchId, ExceGymSettingDC gymSetting, string gymCode)
        {
            this._gymSetting = gymSetting;
            this._gymCode = gymCode;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string storedProcedure = "USExceGMSAdminAddGymSetting";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", System.Data.DbType.Int32, _gymSetting.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReceiveServiceSMS", System.Data.DbType.Int32, _gymSetting.ServiceSMSFreq));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReceiveServiceSMSStart", System.Data.DbType.String, _gymSetting.ReceiveServiceSMSStart.ToShortTimeString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReceiveServiceSMSEnd", System.Data.DbType.String, _gymSetting.ReceiveServiceSMSEnd.ToShortTimeString()));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReceiveMemberSMS", System.Data.DbType.Int32, _gymSetting.MemberSMSFreq));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReceiveMemberSMSStart", System.Data.DbType.String, _gymSetting.ReceiveMemberSMSStart.ToShortTimeString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReceiveMemberSMSEnd", System.Data.DbType.String, _gymSetting.ReceiveMemberSMSEnd.ToShortTimeString()));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReceiveReminderSMS", System.Data.DbType.Int32, _gymSetting.RemainderSMSFreq));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReceiveReminderSMSStart", System.Data.DbType.String, _gymSetting.ReceiveReminderSMSStart.ToShortTimeString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReceiveReminderSMSEnd", System.Data.DbType.String, _gymSetting.ReceiveReminderSMSEnd.ToShortTimeString()));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MaxSMS", System.Data.DbType.Int32, _gymSetting.MaxSMS));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SMSFreqDaysBeforeFreeze", System.Data.DbType.Int32, _gymSetting.SMSFreqDaysBeforeFreeze));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberRequired", System.Data.DbType.Boolean, _gymSetting.MemberRequired));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MobileRequired", System.Data.DbType.Boolean, _gymSetting.MobileRequired));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AddressRequired", System.Data.DbType.Boolean, _gymSetting.AddressRequired));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ZipCodeRequired", System.Data.DbType.Boolean, _gymSetting.ZipCodeRequired));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SuspendAfter", System.Data.DbType.Int32, _gymSetting.SuspendedDays));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RestPlusMonth", System.Data.DbType.Int32, _gymSetting.RestPlusMonth));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SMSBookingReminder", System.Data.DbType.Int32, _gymSetting.SMSBookingReminder));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CountVisitAfter", System.Data.DbType.Decimal, _gymSetting.CountVisitAfter));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PlanIn", System.Data.DbType.Int32, _gymSetting.PlanIn));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TimeOut", System.Data.DbType.Int32, _gymSetting.TimeOut));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceAccountNo", System.Data.DbType.String, _gymSetting.InvoiceAccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PaymentAccountNo", System.Data.DbType.String, _gymSetting.PaymentAccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@JanEnrollment", System.Data.DbType.Decimal, _gymSetting.JanEnrollment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FebEnrollment", System.Data.DbType.Decimal, _gymSetting.FebEnrollment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MarEnrollment", System.Data.DbType.Decimal, _gymSetting.MarEnrollment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AprEnrollment", System.Data.DbType.Decimal, _gymSetting.AprEnrollment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MayEnrollment", System.Data.DbType.Decimal, _gymSetting.MayEnrollment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@JunEnrollment", System.Data.DbType.Decimal, _gymSetting.JunEnrollment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@JulEnrollment", System.Data.DbType.Decimal, _gymSetting.JulEnrollment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AugEnrollment", System.Data.DbType.Decimal, _gymSetting.AugEnrollment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SepEnrollment", System.Data.DbType.Decimal, _gymSetting.SepEnrollment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OctEnrollment", System.Data.DbType.Decimal, _gymSetting.OctEnrollment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NovEnrollment", System.Data.DbType.Decimal, _gymSetting.NovEnrollment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DecEnrollment", System.Data.DbType.Decimal, _gymSetting.DecEnrollment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PhoneCode", System.Data.DbType.String, _gymSetting.PhoneCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BarcodeAvailable", System.Data.DbType.Decimal, _gymSetting.BarcodeAvailable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PrintReceipt", System.Data.DbType.Decimal, _gymSetting.PrintReceipt));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DuePaymentAfter", System.Data.DbType.Decimal, _gymSetting.DuePaymentAfter));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceCharge", System.Data.DbType.Decimal, _gymSetting.InvoiceCharge));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _gymSetting.BranchId));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditPeriod", System.Data.DbType.Int32, _gymSetting.CreditPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditDueDate", System.Data.DbType.Int32, _gymSetting.CreditDueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsShopAvailable", System.Data.DbType.Boolean, _gymSetting.IsShopAvailable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvCancellationPeriod", System.Data.DbType.Int32, _gymSetting.InvoiceCancellationPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@usetodayasDueDate", System.Data.DbType.Boolean, _gymSetting.UseTodayAsDueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditNoteMinInvoiceFee", System.Data.DbType.Decimal, _gymSetting.CreditNoteMinInvoiceFee));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberSearchText", System.Data.DbType.String, _gymSetting.MemberSearchText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ResignPeriod", System.Data.DbType.Int32, _gymSetting.ResignPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsIgnoreInvoiceFee", System.Data.DbType.Boolean, _gymSetting.IsIgnoreInvoiceFee));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsMinTotalPayable", System.Data.DbType.Boolean, _gymSetting.IsMinTotalPayable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAntiDopingRequired", System.Data.DbType.Boolean, _gymSetting.IsAntiDopingRequired));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MoveExceededPayToInvoice", System.Data.DbType.Boolean, _gymSetting.MoveExceededPayToInvoice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditSaleLimit", System.Data.DbType.Decimal, _gymSetting.CreditSaleLimit));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SmsFreqDaysBeforeCashContEnd", System.Data.DbType.Int32, _gymSetting.SmsFreqDaysBeforeCashContEnd));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DebtWarningdate", DbType.Int32, _gymSetting.DebtWarningdate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReminderDate", DbType.Int32, _gymSetting.ReminderDate));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GymStartTime", DbType.String, _gymSetting.GymStartTime.ToShortTimeString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GymEndTime", DbType.String, _gymSetting.GymEndTime.ToShortDateString()));
                
                cmd.ExecuteNonQuery();
                _returnValue = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        
            DbTransaction transaction = null;

            try
            {
                transaction = connection.BeginTransaction();

                foreach (ExceAccessProfileDC profile in _gymSetting.AccessProfileList)
                {
                    _accessProfile = profile;
                    _returnValue = SaveAccessProfile(transaction);
                }

                transaction.Commit();
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return _returnValue;
        }


        private bool SaveAccessProfile(DbTransaction transaction)
        {
            string StoredProcedureName = "USExceGMSSaveAccessProfile";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _accessProfile.Id));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccessProfileName", DbType.String, _accessProfile.AccessProfileName));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus", DbType.Boolean, true));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _gymSetting.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AllowExpressGym", DbType.Boolean, _accessProfile.AllowExpressGym));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AllowExtraActivity", DbType.Boolean, _accessProfile.AllowExtraActivity));
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@SavedId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(param);

                    cmd.ExecuteNonQuery();
                    int savedMsgId = Convert.ToInt32(param.Value);

                    if (savedMsgId > 0)
                    {
                    //gma
                    //foreach (ExceAccessProfileTimeDC profiletime in _accessProfile.AccessTimeList)
                    //{
                    //    profiletime.AccessProfileId = savedMsgId;
                    //    SaveAccessProfileTimeAction action = new SaveAccessProfileTimeAction(profiletime, transaction);
                    //    action.Execute(EnumDatabase.Exceline, _gymCode);
                    //}
                    SaveAccessProfileTimeAction action = new SaveAccessProfileTimeAction(_accessProfile.AccessTimeList, savedMsgId, transaction);
                            action.Execute(EnumDatabase.Exceline, _gymCode);
                        }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
                return false;
            }
        }



    }
}
