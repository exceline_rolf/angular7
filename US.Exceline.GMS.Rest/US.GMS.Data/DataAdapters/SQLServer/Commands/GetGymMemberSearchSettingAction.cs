﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymMemberSearchSettingAction : USDBActionBase<List<GymMemberSearchSettingsDC>>
    {
        private int _branchId = -1;
        private string _userName;

        public GetGymMemberSearchSettingAction(int branchId, string userName)
        {
            _branchId = branchId;
            _userName = userName;

        }

        protected override List<GymMemberSearchSettingsDC> Body(DbConnection connection)
        {
            List<GymMemberSearchSettingsDC> gymMemberSearchSettingsList = new List<GymMemberSearchSettingsDC>();
            const string storedProcedureName = "USExceGMSGetGymMemberSearchSetting";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String , _userName));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    GymMemberSearchSettingsDC gymMemberSearchSettings = new GymMemberSearchSettingsDC();                    
                    gymMemberSearchSettings.Id = Convert.ToInt32(reader["Id"]);
                    gymMemberSearchSettings.BranchId = Convert.ToInt32(reader["BranchId"]);
                    gymMemberSearchSettings.BranchName = Convert.ToString(reader["BranchName"]);
                    gymMemberSearchSettings.Region = Convert.ToString(reader["region"]);
                    gymMemberSearchSettings.IsExpress = Convert.ToBoolean(reader["IsExpressGym"]);
                    gymMemberSearchSettings.MemberSearchText = Convert.ToString(reader["MemberSearchText"]);
                    gymMemberSearchSettingsList.Add(gymMemberSearchSettings);

                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return gymMemberSearchSettingsList;
        }
    }
}