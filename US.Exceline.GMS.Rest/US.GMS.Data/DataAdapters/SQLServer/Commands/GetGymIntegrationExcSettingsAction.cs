﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
   public class GetGymIntegrationExcSettingsAction : USDBActionBase<List<GymIntegrationExcSettingsDC>>
   {
       private readonly int _branchId;
       private readonly string _gymCode = string.Empty;
       public GetGymIntegrationExcSettingsAction(int branchId, string gymCode)
       {
           _branchId = branchId;
           _gymCode = gymCode;
       }
        protected override List<GymIntegrationExcSettingsDC> Body(DbConnection connection)
        {
            var integrationExcSettingsList = new List<GymIntegrationExcSettingsDC>();
            const string spName = "dbo.USExceGMSAdminGetGymEXCIntegrationSettings";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var integrationExcSetting = new GymIntegrationExcSettingsDC();
                    integrationExcSetting.Id = Convert.ToInt32(reader["Id"]);
                    integrationExcSetting.Name = reader["Name"].ToString();
                    if (reader["Status"] != DBNull.Value)
                    integrationExcSetting.Status = Convert.ToInt32(reader["Status"]);
                    if (reader["InstalledDate"] != DBNull.Value)
                    integrationExcSetting.InstalledDate = Convert.ToDateTime(reader["InstalledDate"]);
                    if (reader["LastPingTime"] != DBNull.Value)
                    integrationExcSetting.LastPingTime = Convert.ToDateTime(reader["LastPingTime"]);
                    integrationExcSetting.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    integrationExcSetting.ActivityName = reader["ActivityName"].ToString();
                    integrationExcSetting.AccessContolTypes = (reader["AccessControlTypes"] != DBNull.Value) ? Convert.ToString(reader["AccessControlTypes"]) : "";


                    var action = new GetGymIntegrationAccessProfileAction(integrationExcSetting.Id,"EXC");
                    integrationExcSetting.AccessProfileList = action.Execute(EnumDatabase.Exceline, _gymCode);


                    integrationExcSettingsList.Add(integrationExcSetting);
                }

                return integrationExcSettingsList;
            }
            catch
            {
                throw;
            }
        }
    }
}
