﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : DKA
// Created Timestamp : "12/10/2013 12:00:41 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteJobCategoryAction : USDBActionBase<bool>
    {
        private bool _isDeleted;
        private int _jobCategoryId = -1;
        public DeleteJobCategoryAction(int jobCategoryId)
        {
            this._jobCategoryId = jobCategoryId;
        }

        protected override bool Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminDeleteJobCategory";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@JobCategoryId", DbType.Int32, _jobCategoryId));

                object obj = cmd.ExecuteScalar();
                int categoryId = Convert.ToInt32(obj);
                if (categoryId > 0)
                    _isDeleted = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _isDeleted;
        }
    }
}
