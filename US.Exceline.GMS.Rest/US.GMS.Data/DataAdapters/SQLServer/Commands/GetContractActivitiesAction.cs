﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    class GetContractActivitiesAction : USDBActionBase<List<ActivityDC>>
    {
        private int _branchId = -1;
        public GetContractActivitiesAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<ActivityDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ActivityDC> activityList = new List<ActivityDC>();
            string storedProcedure = "USExceGMSAdminGetActivities ";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ActivityDC activity = new ActivityDC();
                    activity.Id = Convert.ToInt32(reader["ID"]);
                    activity.Name = reader["Name"].ToString();
                    activity.Code = reader["Code"].ToString();
                    activity.BranchId = _branchId;
                    ActivitySettingDC activitySetting = new ActivitySettingDC();
                    activitySetting.IsContractTimeLimited = Convert.ToBoolean(reader["IsContractTimeLimited"]);
                    activitySetting.IsPunchCardLimited = Convert.ToBoolean(reader["IsPunchCardLimited"]);
                    activitySetting.IsFreeTimeAllowed = Convert.ToBoolean(reader["IsFreeTimeAllowed"]);
                    activitySetting.IsBooking = Convert.ToBoolean(reader["IsBooking"]);
                    activitySetting.IsSMSRemindered = Convert.ToBoolean(reader["IsSMSRemindered"]);
                    activitySetting.IsUpdateVisits = Convert.ToBoolean(reader["IsUpdateVisits"]);
                    activitySetting.DaysInAdvanceForBooking = Convert.ToInt32(reader["DaysInAdvanceForBooking"]);
                    activitySetting.ApproximatelyDays = Convert.ToInt32(reader["ApproximatelyDay"]);
                    activitySetting.CanContractBooking = Convert.ToBoolean(reader["IsContractBooking"]);
                    activitySetting.IsContract = Convert.ToBoolean(reader["IsContract"]);
                    activitySetting.IsPunchCard = Convert.ToBoolean(reader["IsPunchCard"]);
                    activity.ActivitySetting = activitySetting;
                    activityList.Add(activity);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return activityList;     
        }
    }
}
