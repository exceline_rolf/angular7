﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetCreditorNoForBranchAction : USDBActionBase<string>
    {
        private int _branchId = -1;
        public GetCreditorNoForBranchAction(int branchId)
        {
            _branchId = branchId;
        }
        protected override string Body(System.Data.Common.DbConnection connection)
        {
            DbDataReader reader = null;
            string spName = "USExceGMSGetCreditorNo";
            string creditorNo = string.Empty;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    creditorNo = reader["CreditorInkassoID"].ToString();
                }
            }
            catch (Exception )
            {
                if (reader != null)
                    reader.Close();
                throw;
            }
            return creditorNo;
        }
    }
}
