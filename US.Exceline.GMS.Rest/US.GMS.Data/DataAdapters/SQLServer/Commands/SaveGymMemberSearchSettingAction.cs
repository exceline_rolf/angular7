﻿using System;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{

    public class SaveGymMemberSearchSettingAction : USDBActionBase<bool>
    {
        private GymMemberSearchSettingsDC _gymMemberSearchSettings = new GymMemberSearchSettingsDC();
        private bool _returnValue = false;
        

        public SaveGymMemberSearchSettingAction(int branchId, GymMemberSearchSettingsDC gymMemberSearchSettings )
        {
            _gymMemberSearchSettings = gymMemberSearchSettings;
            _gymMemberSearchSettings.BranchId = branchId;             
        }

        protected override bool Body(DbConnection connection)
        {
            string storedProcedure = "USExceGMSAdminSaveGymMemberSearchSetting";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", System.Data.DbType.Int32, _gymMemberSearchSettings.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberSearchText", System.Data.DbType.String, _gymMemberSearchSettings.MemberSearchText));

                cmd.ExecuteNonQuery();
                _returnValue = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _returnValue;
        }
    }
}
