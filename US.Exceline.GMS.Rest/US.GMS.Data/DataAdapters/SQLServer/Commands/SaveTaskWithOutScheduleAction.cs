﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "6/20/2012 6:06:17 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using US.GMS.Core.Utils;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveTaskWithOutScheduleAction : USDBActionBase<bool>
    {
        private ExcelineTaskDC _excelineTask;

        public SaveTaskWithOutScheduleAction(ExcelineTaskDC excelineTask)
        {
            _excelineTask = excelineTask;
        }
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool _isSaved = false;
            string StoredProcedureName = "USExceGMSUpdateTaskWithOutSchedule";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TaskId", DbType.Int32, _excelineTask.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _excelineTask.Title));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _excelineTask.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _excelineTask.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateId", DbType.Int32, _excelineTask.TaskTemplateId));
                if (_excelineTask.AssignTo != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Assignto", DbType.Int32, _excelineTask.AssignTo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AssigntoEntityRole", DbType.String, _excelineTask.AssignEntityRole));
                if (_excelineTask.FollowUpMemberId != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _excelineTask.FollowUpMemberId));
                if (_excelineTask.BelongsToMemberId != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@BelongsToMemberId", DbType.Int32, _excelineTask.BelongsToMemberId));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _excelineTask.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedUser", DbType.String, _excelineTask.LastModifiedUser));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NumOfDays", DbType.Int32, _excelineTask.NumOfDays));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPopup", DbType.Boolean, _excelineTask.IsPopup));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsFxied", DbType.Boolean, _excelineTask.IsFxied));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PhoneNo", DbType.String, _excelineTask.PhoneNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Sms", DbType.String, _excelineTask.Sms));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ExtenedInfo", DbType.Xml, XMLUtils.SerializeDataContractObjectToXML(
                    new ExtendedFieldInfoDC() { ExtendedFieldsList = _excelineTask.ExtendedFieldsList })));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus", DbType.Boolean, _excelineTask.ActiveStatus));

                if (_excelineTask.StartDate == DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Startdate", DbType.DateTime, DBNull.Value));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Startdate", DbType.DateTime, _excelineTask.StartDate));
                if (_excelineTask.EndDate == DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Enddate", DbType.DateTime, DBNull.Value));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Enddate", DbType.DateTime, _excelineTask.EndDate));
                if (_excelineTask.StartTime == DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Starttime", DbType.DateTime, DBNull.Value));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Starttime", DbType.DateTime, _excelineTask.StartTime));
                if (_excelineTask.EndTime == DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Endtime", DbType.DateTime, DBNull.Value));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Endtime", DbType.DateTime, _excelineTask.EndTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityId", DbType.Int32, null));


                if (_excelineTask.DueDate == DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@FoundDate", DbType.DateTime, DBNull.Value));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@FoundDate", DbType.DateTime, _excelineTask.DueDate));
                if (_excelineTask.ReturnDate == DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReturnDate", DbType.DateTime, DBNull.Value));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReturnDate", DbType.DateTime, _excelineTask.ReturnDate));

                if (_excelineTask.DueDate == DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Duedate", DbType.DateTime, DBNull.Value));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@DueDate", DbType.DateTime, _excelineTask.DueDate));
                if (_excelineTask.DueTime == DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Duetime", DbType.DateTime, DBNull.Value));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Duetime", DbType.DateTime, _excelineTask.DueTime));


                SqlParameter param = new SqlParameter();
                param.ParameterName = "@outId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);


                cmd.ExecuteNonQuery();

                int TaskID = Convert.ToInt32(param.Value);
                if (TaskID > 0)
                    _isSaved = true;

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _isSaved;
        }
    }
}
