﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters
{
    class GetGymAccessSettingsAction : USDBActionBase<List<GymAccessSettingDC>>
    {
        private int _branchId = -1;
        private string _gymCode = string.Empty;
        private string _userName = string.Empty;

        public GetGymAccessSettingsAction(int branchId, string gymCode, string userName)
        {
            _branchId = branchId;
            _gymCode = gymCode;
            _userName = userName;
        }

        protected override List<GymAccessSettingDC> Body(DbConnection connection)
        {

            List<GymAccessSettingDC> GymAccessSettingList = new List<GymAccessSettingDC>();

            const string storedProcedureName = "USExceGMSGetGymAccessSettings";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@userName", DbType.String, _userName));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    GymAccessSettingDC GymAccessSetting = new GymAccessSettingDC();
                    GymAccessSetting.Id = Convert.ToInt32(reader["Id"]);
                    GymAccessSetting.BranchId = Convert.ToInt32(reader["BranchId"]);
                    GymAccessSetting.BranchName = Convert.ToString(reader["BranchName"]);
                    GymAccessSetting.IsExpress = Convert.ToBoolean(reader["ExpressGym"]);
                    GymAccessSetting.Region = Convert.ToString(reader["region"]);
                    GymAccessSetting.CountVisitAfter = Convert.ToInt32(reader["CountVisitAfter"]);
                    GymAccessSetting.BlockAccessNoOfUnpaidInvoices = Convert.ToInt32(reader["BlockAccessNoOfUnpaidInvoices"]);
                    GymAccessSetting.BlockAccessDueBalance = Convert.ToDecimal(reader["BlockAccessDueBalance"]);
                    GymAccessSetting.IsMemCardExportAsGuestCard = Convert.ToBoolean(reader["IsMemCardExportAsGuestCard"]);
                    GymAccessSetting.BlockAccessOnAccountBalance = Convert.ToDecimal(reader["BlockAccessOnAccountBalance"]);
                    GymAccessSetting.IsReceptionAllowCustomerScreen = Convert.ToBoolean(reader["IsReceptionAllowCustomerScreen"]);
                    GymAccessSetting.MinutesBetweenAccess = Convert.ToInt32(reader["MinutesBetweenAccess"]);
                    GymAccessSetting.ComPort = Convert.ToString(reader["ComPort"]);
                    GymAccessSetting.SecondsViewCustomerScreen = Convert.ToInt32(reader["SecondsViewCustomerScreen"]);
                    GymAccessSetting.IsExtraActivity = Convert.ToBoolean(reader["IsExtraActivity"]);
                    GymAccessSetting.CheckFingerPrint = Convert.ToBoolean(reader["CheckFingerPrint"]);
                    GymAccessSetting.IsSendSms = Convert.ToBoolean(reader["IsSendSms"]);
                    GymAccessSetting.AccIsDisplayName = Convert.ToBoolean(reader["AccIsDisplayName"]);
                    GymAccessSetting.AccIsDisplayCustomerNumber = Convert.ToBoolean(reader["AccIsDisplayCustomerNumber"]);
                    GymAccessSetting.AccIsDisplayContractTemplate = Convert.ToBoolean(reader["AccIsDisplayContractTemplate"]);
                    GymAccessSetting.AccIsDisplayHomeGym = Convert.ToBoolean(reader["AccIsDisplayHomeGym"]);
                    GymAccessSetting.AccIsDisplayContractEndDate = Convert.ToBoolean(reader["AccIsDisplayContractEndDate"]);
                    GymAccessSetting.AccIsDisplayBirthDate = Convert.ToBoolean(reader["AccIsDisplayBirthDate"]);
                    GymAccessSetting.AccIsDisplayAge = Convert.ToBoolean(reader["AccIsDisplayAge"]);
                    GymAccessSetting.AccIsDisplayLastVisit = Convert.ToBoolean(reader["AccIsDisplayLastVisit"]);
                    GymAccessSetting.AccIsDisplayCreditBalance = Convert.ToBoolean(reader["AccIsDisplayCreditBalance"]);
                    GymAccessSetting.AccIsDisplayPicture = Convert.ToBoolean(reader["AccIsDisplayPicture"]);
                    GymAccessSetting.AccIsDisplayLastAccess = Convert.ToBoolean(reader["AccIsDisplayLastAccess"]);
                    GymAccessSetting.AccIsDisplayNotification = Convert.ToBoolean(reader["AccIsDisplayNotification"]);
                    GymAccessSettingList.Add(GymAccessSetting);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return GymAccessSettingList;
        }
    }
}
