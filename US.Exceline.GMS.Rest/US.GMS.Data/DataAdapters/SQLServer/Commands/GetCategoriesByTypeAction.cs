﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "4/20/2012 10:29:59 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetCategoriesByTypeAction : USDBActionBase<List<CategoryDC>>
    {
        private readonly string _type;

        public GetCategoriesByTypeAction(string type)
        {
            _type = type;
        }

        protected override List<CategoryDC> Body(DbConnection connection)
        {
            List<CategoryDC> categoryList = new List<CategoryDC>();
            const string storedProcedureName = "USExceGMSGetCategoriesByType";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, _type));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    CategoryDC category = new CategoryDC();
                    category.Id = Convert.ToInt32(reader["ID"]);
                    category.TypeId = Convert.ToInt32(reader["CategoryTypeId"]);
                    if (!string.IsNullOrEmpty(_type))
                    {
                        if (reader["CategoryTypeCode"] != DBNull.Value)
                        {
                            category.CategoryTypeCode = Convert.ToString(reader["CategoryTypeCode"]);
                        }
                    }
                    category.Code = reader["Code"].ToString();
                    category.Name = reader["Description"].ToString();
                    category.Description = reader["Details"].ToString();
                    category.CreatedUser = reader["CreatedUser"].ToString();
                    category.LastModifiedUser = reader["LastModifiedUser"].ToString();

                    if (!string.IsNullOrEmpty(reader["CreatedDateTime"].ToString()))
                    {
                        category.CreatedDate = Convert.ToDateTime(reader["CreatedDateTime"]);
                    }
                    if (!string.IsNullOrEmpty(reader["LastModifiedDateTime"].ToString()))
                    {
                        category.CreatedDate = Convert.ToDateTime(reader["LastModifiedDateTime"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ActiveStatus"].ToString()))
                    {
                        category.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    }
                    if (!string.IsNullOrEmpty(reader["Color"].ToString()))
                    {
                        category.Color = reader["Color"].ToString();
                    }
                    categoryList.Add(category);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return categoryList;
        }
    }
}
