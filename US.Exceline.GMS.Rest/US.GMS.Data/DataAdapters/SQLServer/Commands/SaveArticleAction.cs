﻿using System;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveArticleAction : USDBActionBase<int>
    {
        private readonly ArticleDC _articleDc;
        private readonly int _branchId = -1;
        private string _user;


        public SaveArticleAction(ArticleDC articleDc, int brachID, int activityCategoryId, string user)
        {
            _articleDc = articleDc;
            _branchId = brachID;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSAdminSaveArticle";
            var articleId = -1;
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@articleID", DbType.Int32, _articleDc.Id));
                if (_articleDc.ArticleSettingId > 0)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleSettingId", DbType.Int32, _articleDc.ArticleSettingId));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@defaultPrice", DbType.Decimal, _articleDc.DefaultPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@description", DbType.String, _articleDc.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Boolean, _articleDc.ActiveStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryId", DbType.Int32, _articleDc.CategoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LoginBranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeePrice", DbType.Decimal, _articleDc.EmployeePrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@purchasedPrice", DbType.Decimal, _articleDc.PurchasedPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PurchasePriceWithoutVat", DbType.Decimal, _articleDc.PurchasePriceWithoutVat));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@barCode", DbType.String, _articleDc.BarCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@typeCode", DbType.String, _articleDc.ArticleType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@reOrderLevel", DbType.Int32, _articleDc.ReOrderLevel));
                if (_articleDc.SortCutKey >= 0)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sortCutKey", DbType.String, _articleDc.SortCutKey));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@stockLevel", DbType.Int32, _articleDc.StockLevel));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@venderId", DbType.Int32, _articleDc.VenderId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@revenueAccountID", DbType.Int32, (_articleDc.RevenueAccountId == 0) ? ((int?)null) : (_articleDc.RevenueAccountId)));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@vatCodeID", DbType.Int32, (_articleDc.VatCodeID == 0) ? (int?)null : _articleDc.VatCodeID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsSave", DbType.Boolean, _articleDc.IsSave));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsUpdate", DbType.Boolean, _articleDc.IsUpdate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAdminUser", DbType.Boolean, _articleDc.IsAdminUser));
                

                if (_articleDc.NoOfMinutes != 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@NoOfMinutes", DbType.Int32, _articleDc.NoOfMinutes));
                }

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StockCategoryId", DbType.Int32, _articleDc.StockCategoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsVatAdded", DbType.Boolean, _articleDc.IsVatAdded));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UnitId", DbType.Int32, _articleDc.UnitId));
                if (_articleDc.ArticleType == ArticleTypes.ITEM.ToString())
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@obsoleteStatus", DbType.Boolean, _articleDc.ObsoleteStatus));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsVoucher", DbType.Boolean, _articleDc.IsVoucher));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@stockStatus", DbType.Boolean, _articleDc.StockStatus));
                }

                if (_articleDc.ArticleType == ArticleTypes.SERVICE.ToString())
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@PunchCard", DbType.Boolean, _articleDc.IsPunchCard));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractBooking", DbType.Boolean, _articleDc.IsContractBooking));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityCategoryId", DbType.Int32, _articleDc.ActivityId));
                }

                var branches = new DataTable();
                var col = new DataColumn("ID", typeof(Int32));
                branches.Columns.Add(col);
                foreach (var id in _articleDc.BranchIdList)
                {
                    branches.Rows.Add(id);
                }
                var parameter = new SqlParameter
                    {
                        ParameterName = "@Branches",
                        SqlDbType = SqlDbType.Structured,
                        Value = branches
                    };

                cmd.Parameters.Add(parameter);

                var obj = cmd.ExecuteScalar();
                articleId = Convert.ToInt32(obj);
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return articleId;
        }
    }
}

