﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    class LogShopReturnAction : USDBActionBase<int>
    {
        private string _invoiceRef;
        private int _quantity;
        private string _comment;
        private int _articleId;

        public LogShopReturnAction(ContractItemDC contractItem)
        {
            _invoiceRef = contractItem.InvoiceNo;
            _quantity = contractItem.Quantity;
            _comment = contractItem.ReturnComment;
            _articleId = contractItem.ArticleId;
        }

        protected override int Body(DbConnection connection)
        {
            int result = -1;
            string StoredProcedureName = "addShopReturnInfoToExceReturned";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleNo", DbType.String, _articleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Quantity", DbType.String, _quantity));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Comment", DbType.String, _comment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceRef", DbType.String, _invoiceRef));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                result = -1;
                throw ex;
            }
            return result;
        }


    }
}
