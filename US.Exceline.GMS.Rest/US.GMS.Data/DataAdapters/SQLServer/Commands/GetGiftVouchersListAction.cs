﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters
{
    class GetGiftVouchersListAction : USDBActionBase<List<VoucherDC>>
    {
        private readonly int _branchId;

        public GetGiftVouchersListAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<VoucherDC> Body(DbConnection connection)
        {
            List<VoucherDC> getVouchersList = new List<VoucherDC>();
            string storedProcedureName = "USExceGMSShopGetGiftVouchers";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    VoucherDC voucher = new VoucherDC();

                    voucher.Id = Convert.ToInt32(reader["Id"].ToString());
                    voucher.VoucherNo = Convert.ToInt32(reader["VoucherNo"]);
                    voucher.IssuedDate = Convert.ToDateTime(reader["IssuedDate"]);
                    voucher.VoucherPrice = Convert.ToDecimal(reader["VoucherPrice"]);
                    voucher.PayableAmount = Convert.ToDecimal(reader["PaidAmount"]);
                    voucher.ExpiryDate = Convert.ToDateTime(reader["ExpiryDate"]);
                    voucher.CustomerNo = Convert.ToInt32(reader["CustomerNo"]);
                    voucher.CustomerName = Convert.ToString(reader["CustomerName"]);
                    voucher.RemainingAmount = Convert.ToDecimal(reader["RemainingAmount"]);
                    getVouchersList.Add(voucher);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return getVouchersList;
        }
    }
}
