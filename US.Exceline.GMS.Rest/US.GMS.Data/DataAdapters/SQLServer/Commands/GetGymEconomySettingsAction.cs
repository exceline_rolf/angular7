﻿using System;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymEconomySettingsAction : USDBActionBase<List<GymEconomySettingsDC>>
    {
        private int _branchId = -1;
        private string _userName;

        public GetGymEconomySettingsAction(int branchId, string userName)
        {
            _branchId = branchId;
            _userName = userName;
        }

        protected override List<GymEconomySettingsDC> Body(DbConnection connection)
        {
            List<GymEconomySettingsDC> gymEconomySettingList = new List<GymEconomySettingsDC>();
            const string storedProcedureName = "USExceGMSGetGymEconomySetting";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _userName));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    GymEconomySettingsDC gymEconomySetting = new GymEconomySettingsDC();
                    gymEconomySetting.Id = Convert.ToInt32(reader["Id"]);
                    gymEconomySetting.BranchId = Convert.ToInt32(reader["BranchId"]);
                    gymEconomySetting.BranchName = Convert.ToString(reader["BranchName"]);
                    gymEconomySetting.IsExpress = Convert.ToBoolean(reader["IsExpressGym"]);
                    gymEconomySetting.Region = Convert.ToString(reader["region"]);
                    gymEconomySetting.RestPlusMonth = Convert.ToBoolean(reader["RestPlusMonth"]);
                    gymEconomySetting.MoveExceededPayToInvoice = Convert.ToBoolean(reader["MoveExceededPayToInvoice"]);
                    gymEconomySetting.InvoiceAccountNo = Convert.ToString(reader["InvoiceAccountNo"]);
                    gymEconomySetting.PaymentAccountNo = Convert.ToString(reader["PaymentAccountNo"]);
                    gymEconomySetting.DebtCollectionPeriod = Convert.ToInt32(reader["DebtCollectionPeriod"]);
                    gymEconomySetting.InvoiceCancellationPeriod = Convert.ToInt32(reader["InvoiceCancellationPeriod"]);
                    gymEconomySetting.RemainderPeriod = Convert.ToInt32(reader["ReminderPeriod"]);
                    gymEconomySetting.RemainderFee = Convert.ToDecimal(reader["ReminderFee"]);
                    gymEconomySetting.SmsRemainderFee = Convert.ToDecimal(reader["SMSReminderFee"]);
                    gymEconomySetting.InvoiceCharge = Convert.ToDecimal(reader["InvoiceCharge"]);
                    gymEconomySetting.InvoiceCancellationPeriod = Convert.ToInt32(reader["InvoiceCancellationPeriod"]);
                    gymEconomySetting.SuspendedDays = Convert.ToInt32(reader["SuspendedAfter"]);
                    gymEconomySetting.NoOfOrdersWhenResign = Convert.ToInt32(reader["NoOrdersResigning"]);
                    gymEconomySetting.EnrollmentFeeFirstOrderIfSponsor = Convert.ToBoolean(reader["EnrollmentFeeFirstOrderIfSponsor"]);
                    gymEconomySetting.CreditUnpaidInvoiceFee = Convert.ToBoolean(reader["CreditUnpaidInvoiceFee"]);
                    gymEconomySetting.CreditUnpaidRemainderFee = Convert.ToBoolean(reader["CreditUnpaidRemainderFee"]);
                    gymEconomySetting.CreditPaidDeviationSettableAmount = Convert.ToDecimal(reader["CreditPaidDeviationSettableAmount"]);
                    gymEconomySetting.ShopOnNextOrder = Convert.ToBoolean(reader["ShopOnNextOrder"]);
                    gymEconomySetting.OnAccount = Convert.ToBoolean(reader["OnAccount"]);
                    gymEconomySetting.OnNegativeAccount = Convert.ToDecimal(reader["NegativeOnAccount"]);
                    gymEconomySetting.IsDefaultCustomerAvailable = Convert.ToBoolean(reader["IsDefaultCustomerAvailable"]);
                    gymEconomySetting.IsPayButtonAvailable = Convert.ToBoolean(reader["IsPayButtonsAvailable"]);
                    gymEconomySetting.IsPettyCashAllowedOnUser = Convert.ToBoolean(reader["PettyCashAllowedOnUser"]);
                    gymEconomySetting.NetsCustomerId = Convert.ToString(reader["NetsCustomerId"]);
                    gymEconomySetting.NetsAssignmentNo = Convert.ToString(reader["NetsAssignmentNo"]);
                    gymEconomySetting.NetsCancellationPeriod = Convert.ToInt32(reader["NetsCancelationPeriod"]);
                    gymEconomySetting.NetsAssignmentNo = Convert.ToString(reader["NetsAssignmentNo"]);
                    gymEconomySetting.NetsCancellationPeriod = Convert.ToInt32(reader["NetsCancelationPeriod"]);
                    gymEconomySetting.IsBankTerminalIntegrated = Convert.ToBoolean(reader["IsBankTerminalIntegrated"]);
                    gymEconomySetting.IsPrintReceiptInShop = Convert.ToBoolean(reader["IsPrintReceiptInShop"]);
                    gymEconomySetting.IsPrintInvoiceInShop = Convert.ToBoolean(reader["IsPrintInvoiceInShop"]);
                    gymEconomySetting.SmsInvoice = Convert.ToBoolean(reader["SMSInvoice"]);
                    gymEconomySetting.CreditSaleLimit = Convert.ToDecimal(reader["CreditSaleLimit"]);
                    gymEconomySetting.IsShopOnNextOrder = Convert.ToBoolean(reader["IsShopOnNextOrder"]);
                    gymEconomySetting.IsSmsInvoiceShop = Convert.ToBoolean(reader["SMSInvoiceShope"]);
                    gymEconomySetting.IsDailySettlementForAll = Convert.ToBoolean(reader["IsDailySettlementForAll"]);
                    gymEconomySetting.PaidAccessMode = Convert.ToByte(reader["PaidAccessMode"]);
                    gymEconomySetting.SmsReminderPeriod = Convert.ToInt32(reader["EcoSMSReminderPeriod"]);
                    gymEconomySetting.SmsInvoiceCharge = Convert.ToDecimal(reader["EcoSMSInvoiceCharge"]);
                    gymEconomySetting.EditRemainingPunches = Convert.ToBoolean(reader["EcoEditRemainingPunches"]);

                    gymEconomySetting.IsMemberFee = Convert.ToBoolean(reader["EcoIsMemberFee"]);
                    gymEconomySetting.IsMemberFeeMonth = Convert.ToBoolean(reader["EcoIsMemberFeeMonth"]);
                    gymEconomySetting.MemberFeeBranchID = Convert.ToInt32(reader["EcoMemberFeeBranchID"]);
                    gymEconomySetting.IsMemberFeeAutoGenerate = Convert.ToBoolean(reader["EcoIsMemberFeeAutoGenerate"]);
                    gymEconomySetting.MemberFeeGenerateDay = Convert.ToInt32(reader["EcoMemberFeeGenerateDay"]);
                    gymEconomySetting.IsMemberFeeATG = Convert.ToBoolean(reader["EcoIsMemberFeeATG"]);
                    if (Convert.ToInt32(reader["MoveExccedPaymentsTo"]) == 1)
                        gymEconomySetting.MoveToPrepaidBalanace = true;
                    else
                        gymEconomySetting.MoveToShopAccount = true;

                    gymEconomySetting.CCXEnabled = Convert.ToBoolean(reader["CCXEnabled"]);
                    gymEconomySetting.PrintIpccx = Convert.ToBoolean(reader["PrintIPCCX"]);
                    gymEconomySetting.PrintPpccx = Convert.ToBoolean(reader["PrintPPCCX"]);
                    gymEconomySetting.PrintSpccx = Convert.ToBoolean(reader["PrintSPCCX"]);
                    gymEconomySetting.SendSmSforPriceChange = Convert.ToBoolean(reader["EcoSendSmSforPriceChange"]);
                    gymEconomySetting.PostPay = Convert.ToBoolean(reader["EcoPostPay"]);
                    gymEconomySetting.BasisX = Convert.ToBoolean(reader["BasisX"]);
                    gymEconomySetting.IsCashSalesActivated = Convert.ToBoolean(reader["IsCashSalesActived"]);
                    gymEconomySetting.NotEditableVoucher = Convert.ToBoolean(reader["NotEditableVoucher"]);
                    gymEconomySetting.ShopReturnOnSelectedBranchOnly = Convert.ToBoolean(reader["ShopReturnOnSelectedBranchOnly"]);
                    gymEconomySettingList.Add(gymEconomySetting);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return gymEconomySettingList;
        }
    }
}


