﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberInvoicesForContractAction : USDBActionBase<List<MemberInvoiceDC>>
    {
        private readonly int _memberId = -1;
        private readonly int _hit = 1;
        private int _contractid = -1;
        private readonly string _user = string.Empty;

        public GetMemberInvoicesForContractAction(int memberId, int contractid, int hit, string user)
        {
            _memberId = memberId;
            _hit = hit;
            _user = user;
            _contractid = contractid;
        }

        protected override List<MemberInvoiceDC> Body(DbConnection connection)
        {
            DbDataReader reader = null;
            var invoiceList = new List<MemberInvoiceDC>();
            const string spName = "USExceGMSManageMembershipGetMemberInvoicesForContract";
            try
            {
                var command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberID", DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@contractID", DbType.Int32, _contractid));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Hit", DbType.Int32, _hit));
                command.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _user));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var memberInvoice = new MemberInvoiceDC();
                    memberInvoice.Id = Convert.ToInt32(reader["ARItemNo"]);
                    memberInvoice.InvoiceNo = Convert.ToString(reader["InvoiceNo"]);
                    memberInvoice.MembeInvoiceId = Convert.ToInt32(reader["InvoiceID"]);
                    memberInvoice.InvoiceType = Convert.ToString(reader["InvoiceType"]);
                    memberInvoice.PayDirection = Convert.ToInt32(reader["PayDirection"]);
                    memberInvoice.PayeePayerName = Convert.ToString(reader["PayeePayerName"]);
                    memberInvoice.ContractId = Convert.ToString(reader["MemberContractNo"]);
                    if (reader["VoucherDate"] != DBNull.Value)
                        memberInvoice.InvoiceDate = Convert.ToDateTime(reader["VoucherDate"]);
                    switch (Convert.ToString(reader["InstallmentType"]))
                    {
                        case "SP":
                            memberInvoice.IsSponsorInvoice = true;
                            memberInvoice.IsShopInvoice = false;
                            break;
                        case "S":
                            memberInvoice.IsSponsorInvoice = false;
                            memberInvoice.IsShopInvoice = true;
                            break;
                        default:
                            memberInvoice.IsSponsorInvoice = false;
                            memberInvoice.IsShopInvoice = false;
                            break;
                    }

                    if (reader["DueDate"] != DBNull.Value)
                        memberInvoice.InvoiceDueDate = Convert.ToDateTime(reader["DueDate"]);

                    memberInvoice.InvoiceAmount = Convert.ToDecimal(reader["Amount"]);
                    memberInvoice.DebtCollectionStatus = Convert.ToString(reader["DebtColStatus"]);
                    memberInvoice.InvoiceBalance = Convert.ToDecimal(reader["Blanace"]);
                    memberInvoice.PDFFileParth = Convert.ToString(reader["PDFFileParth"]);
                    memberInvoice.CanCancel = Convert.ToBoolean(reader["CanCancel"]);
                    memberInvoice.PaymentTypes = Convert.ToString(reader["PaymentTypes"]);
                    memberInvoice.ReferencedInvoiceNo = Convert.ToString(reader["ReferencedInvoiceNo"]);
                    memberInvoice.RemindCollStatus = Convert.ToString(reader["ReminderCollStatus"]);
                    
                    invoiceList.Add(memberInvoice);
                }
            }
            catch
            {
                if (reader != null)
                    reader.Close();
                throw;
            }
            return invoiceList;
        }

    }
}
