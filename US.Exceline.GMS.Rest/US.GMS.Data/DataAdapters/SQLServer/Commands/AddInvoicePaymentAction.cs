﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using US_DataAccess;
using US.GMS.Core.SystemObjects;
using US.Payment.Core.BusinessDomainObjects;
using System.Data.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class AddInvoicePaymentAction : USDBActionBase<bool>
    {
        private InvoiceBasicInfoDC _invoiceData = null;
        private PaymentDetailDC _paymentDetails = null;
        private string _user = string.Empty;
        private int _branchID = -1;

        public AddInvoicePaymentAction(InvoiceBasicInfoDC invoiceData, PaymentDetailDC paymentAmount, string user, string gymCode, int brnachId)
        {
            _invoiceData = invoiceData;
            _paymentDetails = paymentAmount;
            _user = user;
            _branchID = brnachId;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            DbTransaction transaction = null;
            try
            {
                IUSPClaim claim = new USPClaim();
                claim.KID = _invoiceData.KID;
                claim.Amount = _paymentDetails.PaidAmount;
                claim.Debtor.DebtorcustumerID = _invoiceData.DebtorNumber;
                claim.Creditor.CreditorInkassoID = _invoiceData.CreditorNo;
                claim.InvoicedDate = DateTime.Now.ToString("MM-dd-yyyy");
                claim.InvoiceType = Payment.Core.Enums.InvoiceTypes.DB;
                claim.DueDate = DateTime.Now.ToString("MM-dd-yyyy");
                claim.InvoiceNumber = _invoiceData.TRef;
                _invoiceData.Text = _invoiceData.Text;
                claim.DueBalance = "0";
                claim.Balance = "0";

                transaction = connection.BeginTransaction();
                AddPaymentModeDetailsAction paymentDetailAction = new AddPaymentModeDetailsAction(_paymentDetails, _user, _branchID);
                paymentDetailAction.RunOnTransaction(transaction);

                if (_invoiceData.ItemTypeId == 1 || _invoiceData.ItemTypeId == 3)
                {
                    UpdateInstallmentBalanceForInvoicePaymentsAction insAction = new UpdateInstallmentBalanceForInvoicePaymentsAction(_invoiceData.TRef, _paymentDetails.PaidAmount);
                    insAction.RunOntransaction(transaction);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
