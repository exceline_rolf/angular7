﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymOpenTimeAction : USDBActionBase<List<GymOpenTimeDC>>
    {
        private readonly int _branchId = -1;
        private string _userName;
        readonly List<GymOpenTimeDC> _gymOpenTimesList = new List<GymOpenTimeDC>();
        public GetGymOpenTimeAction(int branchId, string userName)
        {
            _branchId = branchId;
            _userName = userName;
        }

        protected override List<GymOpenTimeDC> Body(DbConnection connection)
        {
            const string gymOpenTimeSp = "USExceGMSGetGymOpenTimes";
            try
            {
                DbCommand openTimeCommand = CreateCommand(CommandType.StoredProcedure, gymOpenTimeSp);
                openTimeCommand.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", DbType.Int32, _branchId));
                openTimeCommand.Parameters.Add(DataAcessUtils.CreateParam("@UserName",DbType.String,_userName));
                DbDataReader openTimeReader = openTimeCommand.ExecuteReader();

                while (openTimeReader.Read())
                {
                    GymOpenTimeDC openTime = new GymOpenTimeDC();
                    
                    openTime.BranchId = Convert.ToInt32(openTimeReader["BranchId"]);
                    openTime.BranchName = Convert.ToString(openTimeReader["BranchName"]);
                    openTime.Region = Convert.ToString(openTimeReader["region"]);
                    openTime.IsExpress = Convert.ToBoolean(openTimeReader["IsExpressGym"]);
                   
                    if (_branchId != -2)
                    {
                        openTime.Id = Convert.ToInt32(openTimeReader["Id"]);
                        if (openTimeReader["FromTime"] != DBNull.Value)
                        {
                            openTime.StartTime = DateTime.Parse(openTimeReader["FromTime"].ToString());
                        }
                        if (openTimeReader["ToTime"] != DBNull.Value)
                        {
                            openTime.EndTime = DateTime.Parse(openTimeReader["ToTime"].ToString());
                        }
                        openTime.IsMonday = Convert.ToBoolean(openTimeReader["Monday"]);
                        if (openTime.IsMonday)
                            openTime.Days = "Mon,";
                        openTime.IsTuesday = Convert.ToBoolean(openTimeReader["Tuesday"]);
                        if (openTime.IsTuesday)
                            openTime.Days += "Tue,";
                        openTime.IsWednesday = Convert.ToBoolean(openTimeReader["Wednesday"]);
                        if (openTime.IsWednesday)
                            openTime.Days += "Wed,";
                        openTime.IsThursday = Convert.ToBoolean(openTimeReader["Thursday"]);
                        if (openTime.IsThursday)
                            openTime.Days += "Thu,";
                        openTime.IsFriday = Convert.ToBoolean(openTimeReader["Friday"]);
                        if (openTime.IsFriday)
                            openTime.Days += "Fri,";
                        openTime.IsSaturday = Convert.ToBoolean(openTimeReader["Saturday"]);
                        if (openTime.IsSaturday)
                            openTime.Days += "Sat,";
                        openTime.IsSunday = Convert.ToBoolean(openTimeReader["Sunday"]);
                        if (openTime.IsSunday)
                            openTime.Days += "Sun";
                        openTime.BranchId = Convert.ToInt32(openTimeReader["BranchId"]);
                        openTime.Days = openTime.Days.Trim(new char[] { ',' });                        
                    }
                    _gymOpenTimesList.Add(openTime);
                }
                openTimeReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _gymOpenTimesList;
        }
    }
}
