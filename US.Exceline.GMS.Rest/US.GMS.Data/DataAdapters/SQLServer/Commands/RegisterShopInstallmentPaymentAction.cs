﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.Common.Logging.API;
using US.Common.Notification.API;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Exceline.GMS.Modules.Economy.API;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class RegisterShopInstallmentPaymentAction : USDBActionBase<SaleResultDC>
    {
        private int _memberBranchID = -1;
        private int _loggedBranchId = -1;
        private string _user = string.Empty;
        private InstallmentDC _installment = new InstallmentDC();
        private PaymentDetailDC _payementDetail = null;
        private string _gymCode = string.Empty;
        private ShopSalesDC _salesDetails = null;
        private bool _isBookingPayment = false;
        private bool _isContainReturn = false;
        private List<ContractItemDC> _contractItemReturnList = new List<ContractItemDC>();
        private string _state = string.Empty;
        private string _cardType;

        public RegisterShopInstallmentPaymentAction(string cardType, int memberBranchId, int loggedBranchID, string user, InstallmentDC installment, PaymentDetailDC paymentDetail, string gymCode, ShopSalesDC salesDetails, bool isBookingPayment)
        {
            _cardType = cardType;
            _memberBranchID = memberBranchId;
            _loggedBranchId = loggedBranchID;
            if (salesDetails.UserName != null) _user = salesDetails.UserName;
            _installment = installment;
            _payementDetail = paymentDetail;
            _gymCode = gymCode;
            _salesDetails = salesDetails;
            _isBookingPayment = isBookingPayment;
            
        }

        protected override SaleResultDC Body(System.Data.Common.DbConnection connection)
        {
            DbTransaction transaction = null;
            SaleResultDC saleResult = new SaleResultDC();
            saleResult.InstallmentID = _installment.Id;
            try
            {
                if (_installment.AddOnList.Where(x => x.Quantity > 0).ToList().Count > 0)
                {
                    _state = "INVOICE";
                    string spName = "ExceGMSGenerateInvoiceForOrder";
                    DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);

                    command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentID", System.Data.DbType.Int32, _installment.Id));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@IsInvoiceFeeAdded", System.Data.DbType.Boolean, false));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceType", System.Data.DbType.String, "PRINT"));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@IsShopInvoice", System.Data.DbType.Boolean, true));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@SMSInvoice", System.Data.DbType.Boolean, false));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@MemberBranchID", System.Data.DbType.Int32, _memberBranchID));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@InvoicBranchID", System.Data.DbType.Int32, _loggedBranchId));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@SettleWithPrepaid", System.Data.DbType.Boolean, false));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@SponsoredAmount", System.Data.DbType.Decimal, _installment.SponsoredAmount));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@Discount", System.Data.DbType.Decimal, _installment.Discount));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@IsSponsored", System.Data.DbType.Boolean, _installment.IsSponsored));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", System.Data.DbType.Int32, _installment.MemberId));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@NofoVisists", System.Data.DbType.Int32, _installment.NoOfVisits));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentText", System.Data.DbType.String, _installment.Text));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@MemberContractID", System.Data.DbType.Int32, _installment.MemberContractId));

                    DbDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        saleResult = new SaleResultDC();
                        saleResult.InvoiceNo = Convert.ToString(reader["Ref"]);
                        saleResult.AritemNo = Convert.ToInt32(reader["Aritemno"]);
                        saleResult.InkassoID = Convert.ToInt32(reader["CreditorNo"]);
                        saleResult.CustId = Convert.ToString(reader["CustID"]);
                        saleResult.KID = Convert.ToString(reader["KID"]);
                    }
                    reader.Close();
                    reader.Dispose();
                }

                USLogError.WriteToFile(_state + " " + _installment.Id.ToString(), new Exception(), "system");
                saleResult.MemberId = _installment.MemberId;

                if (_installment.AddOnList.Where(x => x.Quantity < 0).ToList().Count > 0)
                {
                    saleResult = AddReturn(saleResult);
                }

                if (saleResult.AritemNo > 0)
                {
                    //Add payments
                    _state = "ADD PAYMENT";
                    USLogError.WriteToFile(_state +" " + _installment.Id.ToString(), new Exception(), "system");
                    PaymentProcessResult paymentResult = null;
                    List<IUSPTransaction> transactions = new List<IUSPTransaction>();
                    if (_payementDetail.PaidAmount > 0)
                    {
                        transaction = connection.BeginTransaction();
                        foreach (PayModeDC payMode in _payementDetail.PayModes)
                        {
                            //------------------save shop transations data -----------------------------------------------------------------
                            ShopTransactionDC _shopTrans = new ShopTransactionDC();
                            _shopTrans.CardType = _cardType;
                            _shopTrans.BranchId = _loggedBranchId;
                            _shopTrans.CreatedUser = _user;
                            _shopTrans.InvoiceArItemNo = saleResult.AritemNo;
                            _shopTrans.CreatedDate = DateTime.Now;
                            try
                            {
                                _shopTrans.Mode = (US.GMS.Core.SystemObjects.TransactionTypes)Enum.Parse(typeof(US.GMS.Core.SystemObjects.TransactionTypes), payMode.PaymentTypeCode, true);
                            }
                            catch
                            {
                                _shopTrans.Mode = US.GMS.Core.SystemObjects.TransactionTypes.NONE;
                                _shopTrans.ModeText = payMode.PaymentTypeCode;
                            }
                            _shopTrans.Amount = payMode.Amount;
                            _shopTrans.SalePointId = _salesDetails.SalesPointId;
                            _shopTrans.EntityId = _salesDetails.EntitiyId;
                            _shopTrans.EntityRoleType = _salesDetails.EntityRoleType;
                            _shopTrans.VoucherNo = payMode.VoucherNo;
                            _shopTrans.InvoiceArItemNo = saleResult.AritemNo;
                            SaveShopTransactionAction shopTransactionAction = new SaveShopTransactionAction(_shopTrans);
                            shopTransactionAction.RunOnTransaction(transaction);
                            USLogError.WriteToFile("Add shop transactions " + _installment.Id.ToString(), new Exception(), "system");
                            //---------------------------------------------------------------------------------------------------------------------
                            if (payMode.PaymentTypeCode != "EMAILSMSINVOICE" && payMode.PaymentTypeCode != "INVOICE")
                            {
                                transactions.Add(GetTransaction(saleResult, payMode.Amount, payMode.PaymentTypeCode, payMode.PaidDate));
                            }
                        }

                        paymentResult = Transaction.RegsiterTransaction(transactions, transaction);
                        if (!paymentResult.ResultStatus)
                        {
                            StringBuilder paymentMessages = new StringBuilder();
                            foreach (PaymentProcessStepResult step in paymentResult.ProcessStepResultList)
                            {
                                foreach (string message in step.MessageList)
                                {
                                    paymentMessages.Append(message);
                                    USLogError.WriteToFile(message, new Exception(), _user);
                                }
                            }

                            transaction.Rollback();
                            AddSaleSummary("PAYMENTADDINGERROR " + paymentMessages.ToString());
                            USLogError.WriteToFile("PAYMENTADDINGERROR " + _installment.Id.ToString(), new Exception(), "system");
                            saleResult.SaleStatus = SaleErrors.PAYMENTADDINGERROR;
                            return saleResult;
                        }
                        else
                        {
                            //commit the payment transaction
                            transaction.Commit();

                            //Add the sale
                            var tempEML = string.Empty;
                            if (_payementDetail.PayModes.Any(mode => mode.PaymentTypeCode == "EMAILSMSINVOICE"))
                                tempEML = "EML";
                            _state = "ADD SALE";
                            //begin the sale transaction
                            transaction = connection.BeginTransaction();

                            AddSalesDetailsAction salesDetailAction = new AddSalesDetailsAction(_salesDetails, _loggedBranchId, _user, saleResult.AritemNo, -1, tempEML);
                            salesDetailAction.RunOnTransaction(transaction);

                            USLogError.WriteToFile(_state + " " + _installment.Id.ToString(), new Exception(), "system");

                            //ShopSalesItemDC giftvoucher = _salesDetails.ShopSalesItemList.FirstOrDefault(X => string.IsNullOrEmpty(X.VoucherNo) == false);

                            foreach (ShopSalesItemDC item in _salesDetails.ShopSalesItemList)
                            {
                                if (!String.IsNullOrEmpty(item.VoucherNo))
                                {
                                    _state = "ADD GIFTVOUCHER";
                                    AddGiftVoucherDetailsAction voucherAction = new AddGiftVoucherDetailsAction(item, saleResult.AritemNo, _user, _installment.MemberId, _loggedBranchId);
                                    voucherAction.RunOnTransaction(transaction);
                                    USLogError.WriteToFile(_state + " " + _installment.Id.ToString(), new Exception(), "system");
                                }
                            }

                            //if (giftvoucher != null)
                            //{
                            //    _state = "ADD GIFTVOUCHER";
                            //    AddGiftVoucherDetailsAction voucherAction = new AddGiftVoucherDetailsAction(giftvoucher, saleResult.AritemNo, _user, _installment.MemberId, _loggedBranchId);
                            //    voucherAction.RunOnTransaction(transaction);
                            //    USLogError.WriteToFile(_state + " " + _installment.Id.ToString(), new Exception(), "system");
                            //}

                            //commit the sale transaction
                            transaction.Commit();
                            saleResult.SaleStatus = SaleErrors.SUCCESS;

                            //add the withdrawal
                            PayModeDC terminalMode = _payementDetail.PayModes.FirstOrDefault(X => X.PaymentTypeCode == "BANKTERMINAL");
                            if (terminalMode != null && _installment.Balance < 0)
                            {
                                WithdrawalDC withdrawal = new WithdrawalDC();
                                try
                                {
                                    _state = "WITHDRAW";
                                    withdrawal.WithdrawalType = WithdrawalTypes.CASHWITHDRAWAL;
                                    withdrawal.WithdrawalFrom = "CARD";
                                    withdrawal.PaymentType = new CategoryDC();
                                    withdrawal.PaymentType.Code = "CASH";
                                    withdrawal.SalePointId = _salesDetails.SalesPointId;
                                    withdrawal.MemberId = _installment.MemberId;
                                    withdrawal.PaymentAmount = _installment.Balance * -1;
                                    SaveWithdrawalAction saveWithdrawalAction = new SaveWithdrawalAction(_loggedBranchId, withdrawal, _user);
                                    saveWithdrawalAction.Execute(EnumDatabase.Exceline, _gymCode);
                                    USLogError.WriteToFile(_state, new Exception(), "system");
                                }
                                catch (Exception ex)
                                {
                                    USLogError.WriteToFile("WITHDRAWALERROR " + ex.Message, new Exception(), "system");
                                    AddSaleSummary("WITHDRAWALERROR " + withdrawal.PaymentAmount.ToString() + " " + withdrawal.MemberId.ToString() + ex.Message);
                                }
                            }
                        }

                        USLogError.WriteToFile("Payment Added " + _installment.Id.ToString(), new Exception(), "system");
                    }
                    else
                    {
                        if (!_isBookingPayment)
                        {
                            //Add the sale
                            var tempEML = string.Empty;
                            if (_payementDetail.PayModes.Any(mode => mode.PaymentTypeCode == "EMAILSMSINVOICE"))
                                tempEML = "EML";
                            _state = "ADD SALE";
                            //begin the sale transaction
                            transaction = connection.BeginTransaction();

                            AddSalesDetailsAction salesDetailAction = new AddSalesDetailsAction(_salesDetails, _loggedBranchId, _user, saleResult.AritemNo, -1, tempEML);
                            salesDetailAction.RunOnTransaction(transaction);
                            USLogError.WriteToFile(_state, new Exception(), "system");

                            ShopSalesItemDC giftvoucher = _salesDetails.ShopSalesItemList.FirstOrDefault(X => string.IsNullOrEmpty(X.VoucherNo) == false);
                            if (giftvoucher != null)
                            {
                                _state = "ADD GIFTVOUCHER";
                                AddGiftVoucherDetailsAction voucherAction = new AddGiftVoucherDetailsAction(giftvoucher, saleResult.AritemNo, _user, _installment.MemberId, _loggedBranchId);
                                voucherAction.RunOnTransaction(transaction);
                                USLogError.WriteToFile(_state + " " + _installment.Id.ToString(), new Exception(), "system");
                            }

                            transaction.Commit();
                            saleResult.SaleStatus = SaleErrors.SUCCESS;
                            USLogError.WriteToFile(_state, new Exception(), "system");
                        }
                    }


                    //update installment to set as invoiced
                    UpdateOrderInvoicedStatusAction updateaction = new UpdateOrderInvoicedStatusAction(_installment.Id);
                    updateaction.Execute(EnumDatabase.Exceline, _gymCode);


                }
                else if (saleResult.AritemNo == -2) //duplicate invoice 
                {
                    saleResult.SaleStatus = SaleErrors.DUPLICATEINVOICE;
                    AddSaleSummary("DUPLICATEINVOICE");
                    USLogError.WriteToFile("DUPLICATEINVOICE " + _installment.Id.ToString(), new Exception(), "system");
                    return saleResult;
                }
                else if (_installment.AddOnList.Where(x => x.Quantity > 0).ToList().Count > 0) // only if invoice add tried
                {
                    if (_salesDetails.TryNumber <= 1)
                        saleResult.SaleStatus = SaleErrors.RETRY;
                    else
                    {
                        saleResult.SaleStatus = SaleErrors.INVOICEADDINGERROR;
                        AddSaleSummary("INVOICEADDINGERROR");
                    }

                    return saleResult;
                }

                AddSaleSummary("SUCCESS " + saleResult.SaleStatus.ToString());
                return saleResult;
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();

                USLogError.WriteToFile(ex.Message, new Exception(), _user);
                //add the records
                int summaryId = AddSaleSummary(ex.Message);

                saleResult.AritemNo = -1;
                saleResult.InvoiceNo = string.Empty;
                if (_salesDetails.TryNumber <= 1)
                    saleResult.SaleStatus = SaleErrors.RETRY;
                else
                    saleResult.SaleStatus = SaleErrors.ERROR;

                saleResult.Recover = true;
                saleResult.SummaryID = summaryId;
                return saleResult;

                throw;
            }
        }

        private SaleResultDC AddReturn(SaleResultDC saleResult)
        {
            DbTransaction transaction = null;
            try
            {
                //Add Credit note 
                //Add Return 
                foreach (ContractItemDC contractItem in _installment.AddOnList)
                {
                    if (contractItem.Quantity < 0)
                    {
                        // first add to ExceReturnTable

                        LogShopReturnAction logShopReturnAction = new LogShopReturnAction(contractItem);
                        logShopReturnAction.Execute(EnumDatabase.Exceline, _gymCode);

                        //end 


                        _state = "ADD RETURN";
                        int creditedQuantity = contractItem.Quantity * -1;
                        USLogError.WriteToFile(_state, new Exception(), "system");
                        while (creditedQuantity > 0)
                        {
                            _state = "GET RETURN";
                            GetShopCreditNoteDetailsAction creditNoteDetailsAction = new GetShopCreditNoteDetailsAction(_installment.MemberId, contractItem.ArticleId, _loggedBranchId);
                            ShopCreditNoteDetail creditNoteDetails = creditNoteDetailsAction.Execute(EnumDatabase.Exceline, _gymCode);
                            USLogError.WriteToFile(_state, new Exception(), "system");
                            decimal creditedAmount = 0;
                            int creditedQty = 0;
                            if (creditNoteDetails != null)
                            {
                                transaction = connection.BeginTransaction();
                                PaymentProcessResult creditNOteResult;
                                List<IUSPTransaction> creditNotetransactions = new List<IUSPTransaction>();
                                if ((contractItem.Price * -1) > 0)
                                {
                                    if (creditedQuantity <= creditNoteDetails.SumQuanltity)
                                    {
                                        creditedAmount = ((contractItem.Price * -1) / (contractItem.Quantity * -1)) * creditedQuantity;
                                        creditedQty = creditedQuantity;
                            }
                                    else
                                    {
                                        creditedAmount = ((contractItem.Price * -1) / (contractItem.Quantity * -1)) * creditNoteDetails.SumQuanltity;
                                        creditedQty = creditNoteDetails.SumQuanltity;
                        }
                    }

                                creditNotetransactions.Add(GetTransaction(creditNoteDetails.CID, creditNoteDetails.PID, creditNoteDetails.KID, creditNoteDetails.Ref, (contractItem.Price * -1), _user, creditNoteDetails.CaseNo));
                                _state = "ADD CREDITNOTE";
                                creditNOteResult = Transaction.RegsiterTransaction(creditNotetransactions, transaction);
                                USLogError.WriteToFile(_state, new Exception(), "system");
                                if (!creditNOteResult.ResultStatus)
                    {
                        transaction.Rollback();
                                    saleResult.SaleStatus = SaleErrors.ADDCREDITNOTEERROR;
                                    StringBuilder creditSummary = new StringBuilder();
                                    foreach (PaymentProcessStepResult step in creditNOteResult.ProcessStepResultList)
                                    {
                                        foreach (string message in step.MessageList)
                                        {
                                            creditSummary.Append(step.StepName + " " + message);
                                        }
                                    }
                                    USLogError.WriteToFile("ADDCREDITNOTEERROR " + creditSummary.ToString(), new Exception(), "system");
                                    AddSaleSummary("ADDCREDITNOTEERROR " + creditSummary.ToString());
                        return saleResult;
                    }
                                else
                                {
                                    transaction.Commit();
                                    string StoredProcedureName = "USExceGMSManageMembershipAddCreditNote";
                                    DbCommand cmd = connection.CreateCommand();
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.CommandText = StoredProcedureName;
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@custid", DbType.String, creditNoteDetails.CID));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceNo", DbType.String, creditNoteDetails.Ref));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@kid", DbType.String, creditNoteDetails.KID));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Amount", DbType.Decimal, (contractItem.Price * -1)));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _loggedBranchId));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsReturn", DbType.Boolean, true));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, contractItem.ReturnComment));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@SaleiID", DbType.Int32, creditNoteDetails.SaleId));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@creditNoteDetails", SqlDbType.Structured, GetDetailsTable(contractItem, creditNoteDetails, creditedQty, creditedAmount)));
                                    cmd.ExecuteNonQuery();
                                    USLogError.WriteToFile("Credit note added", new Exception(), "system");
                                    
                                }
                                creditedQuantity = creditedQuantity - creditedQty;
                }
                else
                {
                    saleResult.InvoiceNo = string.Empty;
                                saleResult.SaleStatus = SaleErrors.NORETURN;
                                AddSaleSummary("NORETURN");
                    return saleResult;
                }
                        }
                        _isContainReturn = true;
                    }
                }

                ////------------------save shop transations data -------------
                if (_payementDetail.PaidAmount <= 0 && _isContainReturn)
                {
                    _state = "ADD TRANSACTION";
                    ShopTransactionDC _shopTrans = new ShopTransactionDC();
                    _shopTrans.CardType = _cardType;
                    _shopTrans.BranchId = _loggedBranchId;
                    _shopTrans.CreatedUser = _user;
                    _shopTrans.CreatedDate = DateTime.Now;
                    _shopTrans.Mode = US.GMS.Core.SystemObjects.TransactionTypes.RETURN;
                    _shopTrans.Amount = _payementDetail.PaidAmount * -1;
                    _shopTrans.SalePointId = _salesDetails.SalesPointId;
                    _shopTrans.EntityId = _salesDetails.EntitiyId;
                    _shopTrans.EntityRoleType = _salesDetails.EntityRoleType;
                    SaveShopTransactionAction shopTransactionAction = new SaveShopTransactionAction(_shopTrans);
                    shopTransactionAction.Execute(EnumDatabase.Exceline, _gymCode);
                    USLogError.WriteToFile(_state, new Exception(), "system");
                }
                saleResult.SaleStatus = SaleErrors.SUCCESS;
                return saleResult;
            }
            catch (Exception ex)
            {
                if (transaction != null)
                transaction.Rollback();

                AddSaleSummary(_state + " " + ex.Message);
                saleResult.SaleStatus = SaleErrors.ADDCREDITNOTEERROR;
                return saleResult;
            }
            }          

        private int AddSaleSummary(string message)
        {
            string installmentData = XMLUtils.SerializeDataContractObjectToXML(_installment);
            string saleData = XMLUtils.SerializeDataContractObjectToXML(_salesDetails);
            string paymentData = XMLUtils.SerializeDataContractObjectToXML(_payementDetail);
            AddShopSaleSummaryAction saleAction = new AddShopSaleSummaryAction(installmentData, saleData, paymentData, _memberBranchID, _loggedBranchId, "MEM", _salesDetails.TryNumber, _installment.Id, _state + ": " + message);
            int summaryId = saleAction.Execute(EnumDatabase.Exceline, _gymCode);
            return summaryId;
        }

       
        private IUSPTransaction GetTransaction(SaleResultDC result, decimal paymentAmount, string paymentSourceName, DateTime paidDate)
        {
            IUSPTransaction transaction = new USPTransaction();
            transaction.TransType = GetTransType(paymentSourceName);
            transaction.PID = result.InkassoID.ToString();
            transaction.CID = result.CustId;
            if ((transaction.TransType == "OP" || transaction.TransType == "LOP" || transaction.TransType == "BS" || transaction.TransType == "DC") && paidDate != null && paidDate != DateTime.MinValue)
            {
                transaction.VoucherDate = paidDate;
            }
            else
            {
                transaction.VoucherDate = DateTime.Today;
            }
            transaction.DueDate = _installment.DueDate;
            transaction.ReceivedDate = DateTime.Today;
            transaction.RegDate = DateTime.Today;
            transaction.Amount = paymentAmount;
            transaction.Source = paymentSourceName;
            transaction.KID = result.KID;
            transaction.DebtorAccountNo = "";//TODO
            transaction.InvoiceNo = result.InvoiceNo;
            transaction.CreditorAccountNo = "";

            transaction.User = _user;
            return transaction;
        }

        private string GetTransType(string paymentType)
        {
            switch (paymentType)
            {
                case "CASH":
                    return "CSH";

                case "BANKTERMINAL":
                    return "TMN";

                case "OCR":
                    return "LOP";

                case "BANKSTATEMENT":
                    return "BS";

                case "DEBTCOLLECTION":
                    return "DC";

                case "ONACCOUNT":
                    return "OA";

                case "GIFTCARD":
                    return "GC";

                case "BANK":
                    return "BNK";

                case "PREPAIDBALANCE":
                    return "PB";

                case "AGRESSO":
                    return "AGR";

                case "STDTERMINAL":
                    return "STMN";

                case "VIPPS":
                    return "VIPP";
            }
            return string.Empty;
        }

        private IUSPTransaction GetTransaction(string CID, string PID, string KID, string InvoiceNo, decimal creditNoteAmount, string user, int caseNo)
        {
            IUSPTransaction transaction = new USPTransaction();
            transaction.TransType = "CN";
            transaction.PID = PID;
            transaction.CID = CID;
            transaction.VoucherDate = DateTime.Today;
            transaction.ReceivedDate = DateTime.Today;
            transaction.RegDate = DateTime.Today;
            transaction.Amount = creditNoteAmount;
            transaction.Source = "";
            transaction.KID = KID;
            transaction.DebtorAccountNo = "";//TODO
            transaction.InvoiceNo = InvoiceNo;
            transaction.CreditorAccountNo = "";
            transaction.User = user;
            transaction.SubCaseNo = caseNo;

            return transaction;

        }
      
        private DataTable GetDetailsTable(ContractItemDC contractItem, ShopCreditNoteDetail creditNoteDetails, int creditedQuantity, decimal creditedAmount)
        {
            try
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add(new DataColumn("CreditorNumber", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Date", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("EditUser", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Note", typeof(string)));
                dataTable.Columns.Add(new DataColumn("ARItemNo", typeof(int)));
                dataTable.Columns.Add(new DataColumn("ArticleText", typeof(string)));
                dataTable.Columns.Add(new DataColumn("OrderLineId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("CreditedQuantity", typeof(int)));
                dataTable.Columns.Add(new DataColumn("CreditedAmount", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("ArticleID", typeof(int)));

                DataRow dataRow = dataTable.NewRow();
                dataRow["CreditorNumber"] = creditNoteDetails.CID;
                dataRow["Date"] = DateTime.Today;
                dataRow["EditUser"] = _user;
                dataRow["Note"] = contractItem.ReturnComment;
                dataRow["ARItemNo"] = -1;
                dataRow["ArticleText"] = contractItem.ItemName;
                dataRow["OrderLineId"] = creditNoteDetails.OrderlineID;
                dataRow["CreditedQuantity"] = creditedQuantity;
                dataRow["CreditedAmount"] = creditedAmount;
                dataRow["ArticleID"] = contractItem.ArticleId;
                dataTable.Rows.Add(dataRow);

                return dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
