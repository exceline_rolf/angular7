﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetHoliDaysAction : USDBActionBase<List<CalendarHoliday>>
    {
        private DateTime _calndarDate;
        private int _branchId = -1;
        public GetHoliDaysAction(DateTime calendatDate, int branchId)
        {
            _calndarDate = calendatDate;
            _branchId = branchId;
        }

        protected override List<CalendarHoliday> Body(System.Data.Common.DbConnection connection)
        {
            List<CalendarHoliday> holidaysList = new List<CalendarHoliday>();
            string spName = "USExceGMSGetHolidays";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@CalendarStartDate", System.Data.DbType.Date, _calndarDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                DbDataReader reader = command.ExecuteReader();
                while(reader.Read())
                {
                    CalendarHoliday holiday = new CalendarHoliday();
                    holiday.Holiday = Convert.ToDateTime(reader["Holiday"]);
                    holiday.Name = Convert.ToString(reader["Name"]);
                    holidaysList.Add(holiday);
                }
                return holidaysList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
