﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetInvoiceDetailsAction : USDBActionBase<ExcelineInvoiceDetailDC>
    {
        private int _arItemNo = -1;
        private int _branchId = -1;
        private bool _isSponsorInvoice = false;
        private string _gymCode = string.Empty;
        public GetInvoiceDetailsAction(int arItemNo, int branchId, string gymCode)
        {
            _arItemNo = arItemNo;
            _gymCode = gymCode;
            _branchId = branchId;
        }
        protected override ExcelineInvoiceDetailDC Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipGetInvoiceDetails";
            ExcelineInvoiceDetailDC invoiceDetails = null;
            DbDataReader reader = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNo", System.Data.DbType.Int32, _arItemNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    invoiceDetails = new ExcelineInvoiceDetailDC();
                    _isSponsorInvoice = false;
                    InvoiceBasicDetailsDC BasicDetails = new InvoiceBasicDetailsDC();
                    InvoiceOrderDetailsDC OtherDetails = new InvoiceOrderDetailsDC();
                    invoiceDetails.BasicDetails = BasicDetails;
                    invoiceDetails.OtherDetails = OtherDetails;
                    invoiceDetails.ArItemNO = Convert.ToInt32(reader["ARItemNo"]);
                    if (reader["CustId"] != DBNull.Value)
                        invoiceDetails.CustId = Convert.ToInt32(reader["CustId"]);
                    invoiceDetails.CustomerName = Convert.ToString(reader["Name"]);
                    if (reader["CaseNo"] != DBNull.Value)
                        invoiceDetails.SubCaseNo = Convert.ToInt32(reader["CaseNo"]);
                    if (reader["InvoiceAmount"] != DBNull.Value)
                        invoiceDetails.InvoiceAmount = Convert.ToDecimal(reader["InvoiceAmount"]);
                    if (reader["DueDate"] != DBNull.Value)
                        invoiceDetails.InvoiceDueDate = Convert.ToDateTime(reader["DueDate"]);

                    invoiceDetails.InvoiceNo = Convert.ToString(reader["InvoiceNo"]);
                    if (reader["Blanace"] != DBNull.Value)
                        invoiceDetails.InvoiceBalance = Convert.ToDecimal(reader["Blanace"]);
                    if (reader["InvoiceType"] != DBNull.Value)
                        invoiceDetails.InvoiceType = Convert.ToInt32(reader["InvoiceType"]);

                    invoiceDetails.DebtCollectionStatus = Convert.ToString(reader["DebtColStatus"]);
                    invoiceDetails.ContractNo = Convert.ToString(reader["MemberContractNo"]);
                    invoiceDetails.OrderNo = Convert.ToString(reader["OrderNo"]);

                    if (reader["InvoiceID"] != DBNull.Value)
                        invoiceDetails.InvoiceId = Convert.ToInt32(reader["InvoiceID"]);
                    if (reader["CreditorNo"] != DBNull.Value)
                        invoiceDetails.CreditorNo = Convert.ToInt32(reader["CreditorNo"]);
                    if (reader["ActivityPrice"] != DBNull.Value)
                        BasicDetails.ActivityPrice = Convert.ToDecimal(reader["ActivityPrice"]);
                    if (reader["AddOnPrice"] != DBNull.Value)
                        BasicDetails.AdonPrice = Convert.ToDecimal(reader["AddOnPrice"]);
                    if (reader["TotalPrice"] != DBNull.Value)
                        BasicDetails.TotalPrice = Convert.ToDecimal(reader["TotalPrice"]);
                    if (reader["IDueDate"] != DBNull.Value)
                        BasicDetails.DueDate = Convert.ToDateTime(reader["IDueDate"]);

                    BasicDetails.KID = Convert.ToString(reader["KID"]);
                    if (reader["Discount"] != DBNull.Value)
                        BasicDetails.DiscountAmount = Convert.ToDecimal(reader["Discount"]);
                    if (reader["SponsoredAmount"] != DBNull.Value)
                        BasicDetails.SponsoredAmount = Convert.ToDecimal(reader["SponsoredAmount"]);
                    if (reader["PartiallyPaidAmount"] != DBNull.Value)
                        BasicDetails.PartiallyPaidAmount = Convert.ToDecimal(reader["PartiallyPaidAmount"]);
                    if (reader["InvoiceAmount"] != DBNull.Value)
                        BasicDetails.InvoiceAmount = Convert.ToDecimal(reader["InvoiceAmount"]);
                    if (reader["TrainingPeriodStart"] != DBNull.Value)
                        OtherDetails.TrainingStartDate = Convert.ToDateTime(reader["TrainingPeriodStart"]);
                    if (reader["TrainingPeriodEnd"] != DBNull.Value)
                        OtherDetails.TrainingEnddate = Convert.ToDateTime(reader["TrainingPeriodEnd"]);

                    OtherDetails.TemplateName = Convert.ToString(reader["TemplateName"]);
                    if (reader["VoucherDate"] != DBNull.Value)
                        OtherDetails.InvoiceDate = Convert.ToDateTime(reader["VoucherDate"]);
                    if (reader["OriginalMatuarity"] != DBNull.Value)
                        OtherDetails.OriginalDueDate = Convert.ToDateTime(reader["OriginalMatuarity"]);
                    if (reader["PrintedDate"] != DBNull.Value)
                        OtherDetails.InvoicePrintDate = Convert.ToDateTime(reader["PrintedDate"]);
                    if (reader["SMSInvoiceDate"] != DBNull.Value)
                        OtherDetails.SmsEmailInvoiceDate = Convert.ToDateTime(reader["SMSInvoiceDate"]);
                    if (reader["FullyPaidDate"] != DBNull.Value)
                        OtherDetails.FullyPaidDate = Convert.ToDateTime(reader["FullyPaidDate"]);

                    OtherDetails.BranchName = Convert.ToString(reader["BranchName"]);
                    OtherDetails.BranchAccountNo = Convert.ToString(reader["BranchAccountNo"]);
                    OtherDetails.OriginalCustomer = Convert.ToString(reader["OriginalCustomerName"]);

                    if (reader["IsSPInvoice"] != DBNull.Value)
                        OtherDetails.IsSPInvoice = Convert.ToBoolean(reader["IsSPInvoice"]);
                    _isSponsorInvoice = OtherDetails.IsSPInvoice;
                    if (reader["SMSReminderDate"] != DBNull.Value)
                        OtherDetails.SmsReminderDate = Convert.ToDateTime(reader["SMSReminderDate"]);
                    if (reader["DWDate"] != DBNull.Value)
                        OtherDetails.DebtWarningSendDate = Convert.ToDateTime(reader["DWDate"]);
                    if (reader["IsATG"] != DBNull.Value)
                        OtherDetails.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    if (reader["DeleteRequest"] != DBNull.Value)
                        OtherDetails.DeleteRequest = Convert.ToBoolean(reader["DeleteRequest"]);
                    if (reader["ProcessingByNets"] != DBNull.Value)
                        OtherDetails.ProcessingByNets = Convert.ToBoolean(reader["ProcessingByNets"]);

                    OtherDetails.SendingNo = Convert.ToString(reader["SendingNo"]);
                    if (reader["SendingDate"] != DBNull.Value)
                        OtherDetails.SendingDate = Convert.ToDateTime(reader["SendingDate"]);

                    OtherDetails.AddOnText = Convert.ToString(reader["AdonText"]);

                    OtherDetails.Comment = Convert.ToString(reader["Comment"]);
                    if (reader["InvoicedATGStatus"] != DBNull.Value)
                        OtherDetails.AtgInvoiceStatus = Convert.ToInt32(reader["InvoicedATGStatus"]);

                    OtherDetails.CollectingStatus = Convert.ToString(reader["CollectingStatus"]);

                    if (reader["PrintReminderDate"] != DBNull.Value)
                        OtherDetails.LastReminderPrintedDate = Convert.ToDateTime(reader["PrintReminderDate"]);

                    OtherDetails.NoOfReminders = Convert.ToInt32(reader["NoOfReminders"]);
                    invoiceDetails.Transfered = Convert.ToBoolean(reader["Transfered"]);
                    invoiceDetails.GymID = Convert.ToInt32(reader["GymID"]);
                    if (reader["DWTransferedDate"] != DBNull.Value)
                        invoiceDetails.DebtSentCollectionDate = Convert.ToDateTime(reader["DWTransferedDate"]);

                }
                if (reader != null)
                    reader.Close();

                if (invoiceDetails != null)
                {
                    if (invoiceDetails.InvoiceBalance == invoiceDetails.InvoiceAmount)
                        invoiceDetails.InvoiceStatus = "NP";
                    else if (invoiceDetails.InvoiceBalance < invoiceDetails.InvoiceAmount && invoiceDetails.InvoiceBalance > 0)
                        invoiceDetails.InvoiceStatus = "PP";
                    else if (invoiceDetails.InvoiceBalance == 0 || invoiceDetails.InvoiceBalance < 0)
                        invoiceDetails.InvoiceStatus = "FP";

                    if (_isSponsorInvoice)
                    {
                        GetSponsorIvoiceDetailsAction sponsorAction = new GetSponsorIvoiceDetailsAction(_arItemNo);
                        invoiceDetails.BasicDetails.SponsorItemList = sponsorAction.Execute(EnumDatabase.Exceline, _gymCode);
                        GetInvoiceOrderlinesAction orderlineAction = new GetInvoiceOrderlinesAction(invoiceDetails.ArItemNO);
                        invoiceDetails.BasicDetails.ArticleList = orderlineAction.Execute(EnumDatabase.Exceline, _gymCode);
                    }
                    else
                    {
                        GetInvoiceOrderlinesAction orderlineAction = new GetInvoiceOrderlinesAction(invoiceDetails.ArItemNO);
                        invoiceDetails.BasicDetails.ArticleList = orderlineAction.Execute(EnumDatabase.Exceline, _gymCode);
                    }

                    GetInvoicePaymentSummaryAction paymentSummaryAction = new GetInvoicePaymentSummaryAction(invoiceDetails.SubCaseNo);
                    invoiceDetails.OtherDetails.Paymentlist = paymentSummaryAction.Execute(EnumDatabase.Exceline, _gymCode);
                }
                return invoiceDetails;
            }
            catch
            {
                if (reader != null)
                    reader.Close();
                throw;
            }
        }
    }
}
