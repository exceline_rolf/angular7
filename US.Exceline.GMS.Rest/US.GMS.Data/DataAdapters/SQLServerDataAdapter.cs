﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Admin.ManageClassSettings;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer.Commands;
using US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare;
using US.GMS.Data.SystemObjects;
using US.Payment.Core.BusinessDomainObjects;
using US.Payment.Modules.Economy.Core.DomainObjects;
using US.USDF.Core.DomainObjects;
using US_DataAccess;
using US.GMS.Data.DataAdapters;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Data.DataAdapters.SQLServer.Commands.WorkStation;
using US.GMS.Core.DomainObjects.Admin;
using US.Common.Notification.Core.DomainObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands;

namespace US.GMS.Data.DataAdapters.SQLServer
{
    class SQLServerDataAdapter : IDataAdapter
    {
        public List<PackageDC> GetContracts(int branchId, string searchText, string gymCode)
        {
            GetContractDetailsAction action = new GetContractDetailsAction(branchId, searchText);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ArticleDC> GetArticlesForContract(int categoryID, string gymCode, int branchID)
        {
            GetArticlesForContractAction action = new GetArticlesForContractAction(categoryID, branchID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        public int GetGymCompanyId(string gymCode)
        {
            GetGymCompanyIdAction action = new GetGymCompanyIdAction(gymCode);
            return action.Execute(EnumDatabase.WorkStation);
        }


        #region Exceline Task

        public int AddEntityTask(ExcelineTaskDC excelineTask, string gymCode)
        {
            SaveExcelineTaskAction action = new SaveExcelineTaskAction(excelineTask);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<OrderLineDC> GetOrderLinesForAutoCreditNote(int branchId, int priority, string gymCode)
        {
            try
            {
                GetOrderLinesForAutoCreditNoteAction action = new GetOrderLinesForAutoCreditNoteAction(branchId, priority);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveExcelineTask(ExcelineTaskDC excelineTask, string gymCode)
        {
            SaveExcelineTaskAction action = new SaveExcelineTaskAction(excelineTask);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int EditExcelineTask(ExcelineTaskDC excelineTask, string gymCode)
        {
            SaveExcelineTaskAction action = new SaveExcelineTaskAction(excelineTask);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineTaskDC> GetExcelineTasks(int branchId, bool isFxied, int templateId, int assignTo, string roleType, int followupMemberId, string gymCode)
        {
            GetExcelineTasksAction action = new GetExcelineTasksAction(branchId, isFxied, templateId, assignTo, roleType, followupMemberId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineTaskDC> SearchExcelineTask(string searchText, TaskTemplateType type, int followUpMemId, int branchId, bool isFxied, string gymCode)
        {
            SearchExcelineTaskAction action = new SearchExcelineTaskAction(searchText, type, followUpMemId, branchId, isFxied);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveTaskWithOutSchedule(ExcelineTaskDC excelineTask, string gymCode)
        {
            SaveTaskWithOutScheduleAction action = new SaveTaskWithOutScheduleAction(excelineTask);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Category

        /// <summary>
        /// Get category types
        /// </summary>
        /// <param name="name">name/code of type</param>
        /// <returns></returns>
        public List<CategoryTypeDC> GetCategoryTypes(string name, string user, int branchId, string gymCode)
        {
            GetCategoryTypesAction action = new GetCategoryTypesAction(name);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        /// <summary>
        /// Save new category
        /// </summary>
        /// <param name="category"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public int SaveCategory(CategoryDC category, string user, int branchId, string gymCode)
        {
            SaveCategoryAction action = new SaveCategoryAction(category);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        /// <summary>
        /// Save new region
        /// </summary>
        /// <param name="region"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public int SaveRegion(RegionDC region, string user, int branchId, string gymCode)
        {
            SaveRegionAction action = new SaveRegionAction(region, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        /// <summary>
        /// Get categories by type
        /// </summary>
        /// <param name="type">type name</param>
        /// <returns></returns>
        public List<CategoryDC> GetCategoriesByType(string type, string user, string gymCode)
        {
            GetCategoriesByTypeAction action = new GetCategoriesByTypeAction(type);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        /// <summary>
        /// Get regions
        /// </summary>
        /// <param name="type">type name</param>
        /// <returns></returns>
        public List<RegionDC> GetRegions(int branchId, string gymCode, string countryId)
        {
            GetRegionsAction action = new GetRegionsAction(countryId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        /// <summary>
        /// Update Category
        /// </summary>
        /// <param name="category"></param>
        /// <param name="user"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        public bool UpdateCategory(CategoryDC category, string user, int branchId, string gymCode)
        {
            UpdateCategoryAction action = new UpdateCategoryAction(category, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        /// <summary>
        /// Delete category
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="user"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        public int DeleteCategory(int categoryId, string user, int branchId, string gymCode)
        {
            DeleteCategoryAction action = new DeleteCategoryAction(categoryId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        /// <summary>
        /// Search Categories
        /// </summary>
        /// <param name="searchText"></param>
        /// <param name="searchType"></param>
        /// <returns></returns>
        public List<CategoryDC> SearchCategories(string searchText, string searchType, string user, int branchId, string gymCode)
        {
            SearchCategoriesAction action = new SearchCategoriesAction(searchText, searchType, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Task Template
        public bool SaveTaskTemplate(TaskTemplateDC taskTemplate, bool isEdit, string gymCode)
        {
            SaveTaskTemplateAction action = new SaveTaskTemplateAction(taskTemplate, isEdit);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool UpdateTaskTemplate(TaskTemplateDC taskTemplate, string gymCode)
        {
            throw new NotImplementedException();
        }

        public bool DeleteTaskTemplate(int taskTemplateId, string gymCode)
        {
            DeleteTaskTemplateAction action = new DeleteTaskTemplateAction(taskTemplateId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool DeActivateTaskTemplate(int taskTemplateId, string gymCode)
        {
            throw new NotImplementedException();
        }

        public List<TaskTemplateDC> GetTaskTemplates(TaskTemplateType templateType, int branchId, int templateId, string gymCode)
        {
            GetTaskTemplateAction action = new GetTaskTemplateAction(templateType, branchId, templateId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExtendedTaskTemplateDC> GetTaskTemplateExtFields(int extFieldId, string gymCode)
        {
            GetTaskTemplateExtendedFieldsAction action = new GetTaskTemplateExtendedFieldsAction(extFieldId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExtendedFieldDC> GetExtFieldsByCategory(int categoryId, string categoryType, string gymCode)
        {
            GetExtFieldsByCategoryAction action = new GetExtFieldsByCategoryAction(categoryId, categoryType);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        #region Task Category
        public bool SaveTaskCategory(ExcelineTaskCategoryDC taskCategory, bool isEdit, string gymCode)
        {
            SaveTaskCategoryAction action = new SaveTaskCategoryAction(taskCategory, isEdit);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineTaskCategoryDC> GetTaskCategories(int branchId, string gymCode)
        {
            GetTaskCategoryAction action = new GetTaskCategoryAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool DeleteTaskCategory(int taskCategoryId, string gymCode)
        {
            DeleteTaskCategoryAction action = new DeleteTaskCategoryAction(taskCategoryId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Job Category
        public bool SaveJobCategory(ExcelineJobCategoryDC jobCategory, bool isEdit, string gymCode)
        {
            SaveJobCategoryAction action = new SaveJobCategoryAction(jobCategory, isEdit);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineJobCategoryDC> GetJobCategories(int branchId, string gymCode)
        {
            GetJobCategoryAction action = new GetJobCategoryAction(branchId, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool DeleteJobCategory(int jobCategoryId, string gymCode)
        {
            DeleteJobCategoryAction action = new DeleteJobCategoryAction(jobCategoryId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Schedule
        public bool UpdateActiveTime(EntityActiveTimeDC activeTime, bool isDelete, int branchId, string user, string gymCode)
        {
            UpdateActiveTimeAction action = new UpdateActiveTimeAction(activeTime, branchId, isDelete, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool UpdateCalenderActiveTimes(List<EntityActiveTimeDC> activeTimeList, int branchId, string user, string gymCode)
        {
            bool isSaved = false;
            foreach (EntityActiveTimeDC activeTime in activeTimeList)
            {
                UpdateActiveTimeAction action = new UpdateActiveTimeAction(activeTime, branchId, activeTime.IsDeleted, user);
                isSaved = action.Execute(EnumDatabase.Exceline, gymCode);
            }
            return isSaved;
        }

        public bool UpdateSheduleItems(ScheduleDC schedule, string gymCode)
        {
            UpdateScheduleAction action = new UpdateScheduleAction(schedule);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<EntityActiveTimeDC> GetEntityActiveTimes(int branchId, DateTime startDate, DateTime endDate, List<int> entityList, string entityType, string gymCode, string user)
        {
            GetEntityActiveTimesAction action = new GetEntityActiveTimesAction(branchId, startDate, endDate, entityList, entityType, gymCode, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public ScheduleDC GetEntitySchedule(int entityId, string entityRoleType, string user, string gymCode)
        {
            GetEntityScheduleAction action = new GetEntityScheduleAction(entityId, entityRoleType);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool DeleteScheduleItem(int scheduleItemId, bool activeStatus, string user, string gymCode)
        {
            DeleteScheduleItemAction action = new DeleteScheduleItemAction(scheduleItemId, activeStatus, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveShedule(int id, string name, string roleId, int branchId, ScheduleDC sheduleDc, string user, string gymCode)
        {
            SaveScheduleAction action = new SaveScheduleAction(id, name, roleId, branchId, sheduleDc, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ScheduleItemDC> GetScheduleItemsByScheduleId(int scheduleId, string gymCode)
        {
            GetSheduleItemsAction action = new GetSheduleItemsAction(scheduleId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        #region Member

        public List<OrdinaryMemberDC> GetMembersForCommonBooking(int branchId, string searchText, string gymCode)
        {
            GetMembersForCommonBookingAction action = new GetMembersForCommonBookingAction(branchId, searchText);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public OrdinaryMemberDC GetMembersById(int branchId, int memberId, string gymCode)
        {
            GetMemberByIDAction action = new GetMemberByIDAction(branchId, memberId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ActivityDC> GetActivitiesForCommonBooking(int entityId, string entityType, int memberId, string gymCode)
        {
            GetActivitiesForCommonBookingAction action = new GetActivitiesForCommonBookingAction(entityId, entityType, memberId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ManageMemberShopAccount(MemberShopAccountDC memberShopAccount, string mode, string user, int branchId, int salePointId, string gymCode)
        {
            ManageMemberShopAccountAction action = new ManageMemberShopAccountAction(memberShopAccount, mode, user, branchId, salePointId, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public MemberShopAccountDC GetMemberShopAccounts(int memberId, int hit, string gymCode, bool itemsNeeded)
        {
            GetMemberShopAccountsAction action = new GetMemberShopAccountsAction(memberId,"MEM", hit, gymCode, itemsNeeded);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<MemberShopAccountItemDC> GetMemberShopAccountItems(int shopAccountId, string gymCode, int branchId)
        {
            GetMemberShopAccountItemsAction action = new GetMemberShopAccountItemsAction(shopAccountId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Article
        public List<ArticleDC> GetArticles(int articleId, int branchId, ArticleTypes articleType, string keyWord, CategoryDC category, int activityId, bool? isObsalate, bool activeState, string gymCode, bool IsFilerByGym)
        {
            GetArticlesAction action = new GetArticlesAction(articleId, branchId, articleType, keyWord, category, activityId, isObsalate, activeState, IsFilerByGym);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }



        public List<ArticleDC> GetArticlesSearch(int branchId, string keyWord, string gymCode)
        {
            GetArticlesSearchAction action = new GetArticlesSearchAction(branchId, keyWord);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveArticle(ArticleDC articleDc, string user, int brnachID, int activityCategoryId, string gymCode)
        {
            var action = new SaveArticleAction(articleDc, brnachID, activityCategoryId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ImportArticleList(List<ArticleDC> articleList, string user, int branchID, string gymCode)
        {
            ImportArticleListAction action = new ImportArticleListAction(articleList, user, branchID, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Booking

        /// <summary>
        /// Check availability of booking time
        /// </summary>
        /// <param name="activeTimeId"></param>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns>true:available false:not available</returns>
        public bool CheckCommonBookingAvailability(int activeTimeId, int entityId, int branchId, string entityType, DateTime startDateTime, DateTime endDateTime, string gymCode)
        {
            CheckCommonBookingAvailabilityAction action = new CheckCommonBookingAvailabilityAction(activeTimeId, entityId, branchId, entityType, startDateTime, endDateTime);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        /// <summary>
        /// Save common booking details
        /// </summary>
        /// <param name="commonBookingDC"></param>
        /// <param name="branchId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool SaveCommonBookingDetails(CommonBookingDC commonBookingDC, ScheduleDC scheduleDC, int branchId, string user, string scheduleCategoryType, string gymCode)
        {
            SaveCommonBookingDetailsAction action = new SaveCommonBookingDetailsAction(commonBookingDC, scheduleDC, branchId, user, scheduleCategoryType);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        #region Payments

        public string RegisterPayment(int branchId, int memberId, int articleNo, decimal paymentAmount, decimal discount, string createdUser, decimal invoiceAmount, PaymentTypes paymentType, string gymCode)
        {
            RegisterPaymentsAction action = new RegisterPaymentsAction(branchId, memberId, articleNo, paymentAmount, discount, createdUser, invoiceAmount, paymentType, true, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public SaleResultDC RegisterInstallmentPayment(int memberBranchId, int loggedbranchID, string user, InstallmentDC installment, PaymentDetailDC paymentAmount, string gymCode, int salePointID, ShopSalesDC salesDetails)
        {
            RegisterInstallmentPaymentAction action = new RegisterInstallmentPaymentAction(memberBranchId, loggedbranchID, user, installment, paymentAmount, gymCode, salePointID, salesDetails);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public SaleResultDC GenerateInvoice(int invoiceBranchId, string user, InstallmentDC installment, PaymentDetailDC paymentAmount, string invoiceType, string gymCode, int memberBranchID)
        {
            RegisterInvoiceAction action = new RegisterInvoiceAction(invoiceBranchId, user, installment, paymentAmount, invoiceType, gymCode, memberBranchID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public SaleResultDC RegisterInvoicePayment(PaymentDetailDC paymentDetails, string gymCode, int branchId, ShopSalesDC salesDetails, string user)
        {
            RegisterInvoicePaymentAction action = new RegisterInvoicePaymentAction(paymentDetails, gymCode, branchId, salesDetails, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        #region Invoice
        public bool ScheduleOrdersForInvoicing(int branchId, string gymCode)
        {
            ScheduleOrdersForInvoicingAction scheduleAction = new ScheduleOrdersForInvoicingAction(branchId);
            return scheduleAction.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ScheduleSponsorOrdersForInvoicing(int branchId, string gymCode)
        {
            ScheduleSponsorOrdersForInvoicingAction scheduleAction = new ScheduleSponsorOrdersForInvoicingAction(branchId);
            return scheduleAction.Execute(EnumDatabase.Exceline, gymCode);
        }


        public string GetCreditorForBranch(int branchId, string gymCode)
        {
            GetCreditorNoForBranchAction action = new GetCreditorNoForBranchAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);

        }

        public bool CancelInvoice(int arItemNo, string user, string gymCode)
        {
            CancelInvoiceAction action = new CancelInvoiceAction(arItemNo, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<MemberInvoiceDC> GetMemberInvoices(int memberID, int hit, string gymCode, string user)
        {
            GetMemberInvoicesAction action = new GetMemberInvoicesAction(memberID, hit, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public ExcelineInvoiceDetailDC GetInvoiceDetails(int arItemNo, int branchId, string gymCode)
        {
            GetInvoiceDetailsAction action = new GetInvoiceDetailsAction(arItemNo, branchId, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool UpdateInvoice(int arItemNo, string comment, DateTime DueDate, string gymCode)
        {
            UpdateInvoiceAction action = new UpdateInvoiceAction(arItemNo, DueDate, comment);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Utility
        public string GetCityForPostalCode(string postalCode, string gymCode)
        {
            GetCityForPostalCodeAction action = new GetCityForPostalCodeAction(postalCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<CalendarHoliday> GetHolidays(DateTime calendarDate, int branchId, string gymCode)
        {
            GetHoliDaysAction action = new GetHoliDaysAction(calendarDate, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Activity

        public List<ActivityDC> GetActivities(int branchId, string gymCode)
        {
            GetActivitiesAction action = new GetActivitiesAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ActivityDC> GetActivitiesForEntity(int entityId, string entityType, string gymCode, int branchId)
        {
            GetActivitiesForEntityAction action = new GetActivitiesForEntityAction(entityId, entityType, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineRoleDc> GetUserRole(string gymCode)
        {
            var action = new GetUserRoleAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        #region Notification
        public List<NotificationTemplateDC> GetNotificationTemplatesByType(int templateTypeId, NotifyMethodType notifyMethod, int branchId, string gymCode)
        {
            GetNotificationTemplatesByTypeId action = new GetNotificationTemplatesByTypeId(templateTypeId, notifyMethod, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        #region Gym Settings
        #region Gym Access Time Settings
        public List<GymAccessSettingDC> GetGymAccessSettings(int branchId, string gymCode, string userName)
        {
            GetGymAccessSettingsAction action = new GetGymAccessSettingsAction(branchId, gymCode, userName);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveGymAccessSettings(int branchId, GymAccessSettingDC accessTimeSettings, string gymCode)
        {
            SaveGymAccessSettingsAction saveGymAccessTimeSetting = new SaveGymAccessSettingsAction(branchId, accessTimeSettings, gymCode);
            return saveGymAccessTimeSetting.Execute(EnumDatabase.Exceline, gymCode);
        }

        //public List<GymAccessTimeSettingsDC> GetGymAccessTimeSettings(int branchId, string gymCode, string userName)
        //{
        // GetGymAccessTimeSettingsAction action = new GetGymAccessTimeSettingsAction(branchId, gymCode, userName);
        //    return action.Execute(EnumDatabase.Exceline, gymCode);
        //}

        //public bool SaveGymAccessTimeSettings(int branchId, GymAccessTimeSettingsDC accessTimeSettings, string gymCode)
        //{
        //    SaveGymAccessTimeSettingsAction saveGymAccessTimeSetting = new SaveGymAccessTimeSettingsAction(branchId, accessTimeSettings, gymCode);
        //    return saveGymAccessTimeSetting.Execute(EnumDatabase.Exceline, gymCode);
        //}

        #endregion

        #region Gym Open Time
        public List<GymOpenTimeDC> GetGymOpenTimes(int branchId, string gymCode, string userName)
        {
            GetGymOpenTimeAction action = new GetGymOpenTimeAction(branchId, userName);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveGymOpenTimes(int branchId, List<GymOpenTimeDC> gymOpenTimesList, string gymCode)
        {
            SaveGymOpenTimeAction action = new SaveGymOpenTimeAction(branchId, gymOpenTimesList);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Gym Required Settings
        public List<GymRequiredSettingsDC> GetGymRequiredSettings(int branchId, string gymCode, string userName)
        {
            GetGymRequiredSettingAction action = new GetGymRequiredSettingAction(branchId, userName);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveGymRequiredSettings(int branchId, GymRequiredSettingsDC requiredSettings, string gymCode)
        {
            SaveGymRequiredSettingAction saveGymRequiredSetting = new SaveGymRequiredSettingAction(branchId, requiredSettings);
            return saveGymRequiredSetting.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Gym Member Search Settings
        public List<GymMemberSearchSettingsDC> GetGymMemberSearchSettings(int branchId, string gymCode, string userName)
        {
            GetGymMemberSearchSettingAction action = new GetGymMemberSearchSettingAction(branchId, userName);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveGymMemberSearchSettings(int branchId, GymMemberSearchSettingsDC memberSearchSettings, string gymCode)
        {
            SaveGymMemberSearchSettingAction saveGymMemberSearchSetting = new SaveGymMemberSearchSettingAction(branchId, memberSearchSettings);
            return saveGymMemberSearchSetting.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Gym Hardware Profile Settings
        public List<ExceHardwareProfileDC> GetHardwareProfiles(int branchId, string gymCode, string userName)
        {
            GetHardwareProfileAction action = new GetHardwareProfileAction(branchId, userName);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveHardwareProfile(int branchId, List<ExceHardwareProfileDC> hardwareProfileList, string gymCode)
        {
            bool isSave = false;
            foreach (ExceHardwareProfileDC hardwareProfile in hardwareProfileList)
            {
                if (hardwareProfile.IsDelete)
                {
                    DeleteHardwareProfileAction action = new DeleteHardwareProfileAction(hardwareProfile);
                    isSave = action.Execute(EnumDatabase.Exceline, gymCode);
                }
                else
                {
                    SaveHardwareProfileAction saveHardwareProfileSetting = new SaveHardwareProfileAction(hardwareProfile, branchId);
                    isSave = saveHardwareProfileSetting.Execute(EnumDatabase.Exceline, gymCode);
                }
            }
            return isSave;
        }
        #endregion

        #region Gym Economy Settings
        public List<GymEconomySettingsDC> GetGymEconomySettings(int branchId, string gymCode, string userName)
        {
            GetGymEconomySettingsAction action = new GetGymEconomySettingsAction(branchId, userName);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveGymEconomySettings(int branchId, GymEconomySettingsDC economySettings, string gymCode)
        {
            SaveGymEconomySettingAction saveGymEconomySetting = new SaveGymEconomySettingAction(branchId, economySettings);
           bool settingStattus = saveGymEconomySetting.Execute(EnumDatabase.Exceline, gymCode);

            AddWSCCXStatusAction workStationAction = new AddWSCCXStatusAction(gymCode, branchId, economySettings.CCXEnabled);
           bool wsStatut = workStationAction.Execute(EnumDatabase.WorkStation);

           if (settingStattus && wsStatut)
               return true;
           else
               return false;

        }
        #endregion

        #region Gym Sms Settings
        public List<GymSmsSettingsDC> GetGymSmsSettings(int branchId, string gymCode, string userName)
        {
            GetGymSmsSettingAction action = new GetGymSmsSettingAction(branchId, userName, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveGymSmsSettings(int branchId, GymSmsSettingsDC smsSettings, string gymCode)
        {
            SaveGymSmsSettingAction saveGymSmsSetting = new SaveGymSmsSettingAction(branchId, smsSettings);
            return saveGymSmsSetting.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Gym Other Settings
        public List<GymOtherSettingsDC> GetGymOtherSettings(int branchId, string gymCode, string userName)
        {
            GetGymOtherSettingsAction action = new GetGymOtherSettingsAction(branchId, userName);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveGymOtherSettings(int branchId, GymOtherSettingsDC otherSettings, string gymCode)
        {
            SaveGymOtherSettingAction saveGymOtherSetting = new SaveGymOtherSettingAction(branchId, otherSettings);
            return saveGymOtherSetting.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region GetSelectedGymSettings
        public Dictionary<string, object> GetSelectedGymSettings(List<String> gymSetColNameList, bool isGymSettings, int branchId, string gymCode)
        {
            GetSelectedGymSettingsAction seleGymSetting = new GetSelectedGymSettingsAction(gymSetColNameList, isGymSettings, branchId, false);
            return seleGymSetting.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion
        #endregion

        #region CCX

        public USDFInvoiceInfo GetAllInvoices(int chunkSize, string gymCode, int creditorNo, int batchsequenceNo, List<string> invoiceTypes)
        {
            GetAllInvoicesAction invoiceAction = new GetAllInvoicesAction(chunkSize, gymCode, creditorNo, batchsequenceNo, invoiceTypes);
            return invoiceAction.Execute(EnumDatabase.Exceline, gymCode);

        }

        public USDFInvoiceInfo GetDebtWarningInvoices(int chunkSize, string gymCode, int creditorNo, int batchsequenceNo)
        {
            GetDebtWarningInvoicesAction invoiceAction = new GetDebtWarningInvoicesAction(chunkSize, gymCode, creditorNo, batchsequenceNo);
            return invoiceAction.Execute(EnumDatabase.Exceline, gymCode);

        }


        public USDFInvoiceInfo GetDirectPayments(int chunkSize, string gymCode, int creditorNo, int batchSequenceNo)
        {
            GetDirectPaymentAction invoiceAction = new GetDirectPaymentAction(chunkSize, creditorNo, batchSequenceNo);
            return invoiceAction.Execute(EnumDatabase.Exceline, gymCode);
        }

        public string GetGymCodeForCreditor(int creditorNo)
        {
            GetGymCodeForCreditorNoAction action = new GetGymCodeForCreditorNoAction(creditorNo);
            return action.Execute(EnumDatabase.WorkStation);
        }

        public int AddPaymentStatus(USPPaymentStatusFileRow paymentStatus, string gymCode)
        {
            AddPaymentStatusAction action = new AddPaymentStatusAction(paymentStatus);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public int AddNewCancelledStatus(US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPNewOrCancelledFileRow newOrCancelledStatus, string gymCode)
        {
            AddNewCancelledAction action = new AddNewCancelledAction(newOrCancelledStatus);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddDataImportReceipt(DataImportReceiptDetail importReceipt, string gymCode)
        {
            AddDataImportReceiptAction action = new AddDataImportReceiptAction(importReceipt);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int ImportNewCancelledStatus(string gymCode)
        {
            ImportNewCancelledStatusAction action = new ImportNewCancelledStatusAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int ImportPaymentStatus(string gymCode)
        {
            ImportPaymentStatusAction action = new ImportPaymentStatusAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int ImportDataImportReceipt(string gymCode)
        {
            ImportDataImportReceiptAction action = new ImportDataImportReceiptAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int UpdateDWStatus(string gymCode)
        {
            UpdateDWStatusAction action = new UpdateDWStatusAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int UpdateTemplateStatus(string gymCode)
        {
            UpdateTemplateStatusAction action = new UpdateTemplateStatusAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<GymCompanyDC> GetGymCompanies()
        {
            GeyGymCompaniesAction action = new GeyGymCompaniesAction();
            return action.Execute(EnumDatabase.WorkStation);
        }

        public bool UpdateExportStatus(string gymCode)
        {
            UpdateExportStatusAction action = new UpdateExportStatusAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ImportStatusDC> GetClaimStatus(string gymCode, int hit, int status, DateTime date, ImportStatusDC advancedData)
        {
            GetClaimImportStatusAction action = new GetClaimImportStatusAction(date, hit, status, advancedData);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ReScheduleClaim(string gymCode, int itemNo)
        {
            RescheduleClaimAction action = new RescheduleClaimAction(itemNo);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public PaymentImportStatusDC GetPaymentImportStatus(string gymCode, int hit, DateTime RegDate)
        {
            GetPaymentImportStatusAction action = new GetPaymentImportStatusAction(hit, RegDate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ImportStatusDC> GetClaimExportStatusForMember(string gymCode, int memberID, int hit, DateTime? transferDate, string type, string user)
        {
            GetClaimExportStatusForMember action = new GetClaimExportStatusForMember(memberID, hit, transferDate, type, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool MoveToShopAccount(int statusID, bool isImportStatus, string gymCode)
        {
            MoveToShopAccountAction action = new MoveToShopAccountAction(statusID, isImportStatus);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        #region Settings
        public decimal GetSessionTimeOutAfter(int branchId, string gymCode)
        {
            GetTimeOutAfterSettingAction action = new GetTimeOutAfterSettingAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        public int CheckEmployeeByEntNo(int entityNO, string gymCode)
        {
            CheckEmployeeByEntNoAction action = new CheckEmployeeByEntNoAction(entityNO);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<EntityVisitDC> GetVistsByDate(DateTime selectedDate, int branchId, string systemName, string gymCode, string user)
        {
            GetVistsByDateAction action = new GetVistsByDateAction( selectedDate,  branchId,  systemName, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<CountryDC> GetCountryDetails(string gymCode)
        {
            GetCountryDetailsAction action = new GetCountryDetailsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<LanguageDC> GetLangugeDetails(string gymCode)
        {
            GetLangugeDetailsAction action = new GetLangugeDetailsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SavePostalArea(string user, string postalCode, string postalArea, Int64 population, Int64 houseHold, string gymCode)
        {
            SavePostalAreaAction action = new SavePostalAreaAction(user, postalCode, postalArea, population, houseHold);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #region gymDetails
        public string GetGymConnection(string gymCode)
        {
            GetGymConnectionAction action = new GetGymConnectionAction(gymCode);
            return action.Execute(EnumDatabase.WorkStation);
        }

        public List<BranchDetailsDC> GetBranchDetails()
        {
            GetBranchDetailsAction action = new GetBranchDetailsAction();
            return action.Execute(EnumDatabase.WorkStation);
        }

        public List<GymIntegrationSettingsDC> GetGymIntegrationSettings(int branchId, string gymCode)
        {
            GetGymIntegrationSettingsAction action = new GetGymIntegrationSettingsAction(branchId, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<GymIntegrationExcSettingsDC> GetGymIntegrationExcSettings(int branchId, string gymCode)
        {
            GetGymIntegrationExcSettingsAction action = new GetGymIntegrationExcSettingsAction(branchId, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveGymIntegrationSettings(int branchId, GymIntegrationSettingsDC integrationSettings, string gymCode)
        {
            AddUpdateGATIntegrationSettingsAction action = new AddUpdateGATIntegrationSettingsAction(branchId,integrationSettings);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveGymIntegrationExcSettings(int branchId, GymIntegrationExcSettingsDC integrationExcSettings, string gymCode)
        {
            AddUpdateExcIntegrationSettingsAction action = new AddUpdateExcIntegrationSettingsAction(branchId, integrationExcSettings);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool DeleteIntegrationSettingById(int id, string gymCode)
        {
            DeleteGATIntegrationSettingByIdAction action = new DeleteGATIntegrationSettingByIdAction(id);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<TerminalType> GetTerminalTypes(string gymCode)
        {
            GetTerminalTypeAction action = new GetTerminalTypeAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public OtherIntegrationSettingsDC GetOtherIntegrationSettings(int branchId, string gymCode, string systemName)
        {
            GetOtherIntegrationSettingsAction action = new GetOtherIntegrationSettingsAction(branchId, systemName);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool AddUpdateOtherIntegrationSettings(int branchId, OtherIntegrationSettingsDC otherIntegrationSettings, string gymCode)
        {
            AddUpdateOtherIntegrationSettingsAction action = new AddUpdateOtherIntegrationSettingsAction(branchId, otherIntegrationSettings);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        public ArticleDC GetInvoiceFee(int branchId, string gymCode)
        {
            GetInvoiceFeeAction action = new GetInvoiceFeeAction(branchId, "PRINT");
            US.Exceline.GMS.Modules.Economy.Core.SystemObjects.CreditorOrderLine invoiceFee = action.Execute(EnumDatabase.Exceline, gymCode);
            if (invoiceFee != null)
            {
                ArticleDC article = new ArticleDC();
                article.Price = invoiceFee.Amount;
                article.DefaultPrice = invoiceFee.UnitPrice;
                article.ArticleNo = Convert.ToString(invoiceFee.ArticleNo);
                article.Id = Convert.ToInt32(invoiceFee.ArticleNo);
                article.Description = invoiceFee.Text;
                return article;
            }
            return null;
        }

        public int UpdateMemberStatus(string gymCode, int branchId)
        {
            UpdateMemberStatusAction action = new UpdateMemberStatusAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #region ClassTypeSetting

        public List<ExceClassTypeDC> GetClassTypes(string gymCode)
        {
            GetClassTypesAction action = new GetClassTypesAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceClassTypeDC> GetClassTypesByBranch(int branchId, string gymCode)
        {
            GetClassTypesByBranchAction action = new GetClassTypesByBranchAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool AddEditClassType(ExceClassTypeDC classType, string gymCode)
        {
            AddEditClassTypeAction action = new AddEditClassTypeAction(classType);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int DeleteClassType(int classTypeId, string gymCode)
        {
            DeleteClassTypeAction action = new DeleteClassTypeAction(classTypeId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        public List<ArticleDC> GetArticlesForVisits(string gymCode, int brnachID)
        {
            GetArticlesForVisitsAction action = new GetArticlesForVisitsAction(brnachID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddCreditorNote(CreditNoteDC creditNote, int branchId, bool isReturn, string gymCode)
        {
            AddCreditorNoteAction action = new AddCreditorNoteAction(creditNote, branchId, isReturn, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public List<ExceAccessProfileDC> GetAccessProfiles(string gymCode, Gender gender)
        {
            GetAccessProfilesAction action1 = new GetAccessProfilesAction(gymCode, gender);
            List<ExceAccessProfileDC> accessProfileList = action1.Execute(EnumDatabase.Exceline, gymCode);

            foreach (ExceAccessProfileDC accessProfile in accessProfileList)
            {
                GetBranchesForAccessProfileAction action2 = new GetBranchesForAccessProfileAction(accessProfile.Id);
                List<int> branchIdList = action2.Execute(EnumDatabase.Exceline, gymCode);
                accessProfile.BranchIdList = branchIdList;

                GetRegionsForAccessProfileAction action3 = new GetRegionsForAccessProfileAction(accessProfile.Id);
                List<int> regionIdList = action3.Execute(EnumDatabase.Exceline, gymCode);
                accessProfile.RegionIdList = regionIdList;

                GetCountriesForAccessProfileAction action4 = new GetCountriesForAccessProfileAction(accessProfile.Id);
                List<string> countryIdList = action4.Execute(EnumDatabase.Exceline, gymCode);
                accessProfile.CountryIdList = countryIdList;
            }
            
            return accessProfileList;
        }

        public int UpdateAccessProfiles(ExceAccessProfileDC accessProfile, string user, string gymCode)
        {
            UpdateAccessProfilesAction action = new UpdateAccessProfilesAction(accessProfile, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<String> ActGetFileData(string gymCode)
        {
            ActGetFileDataAction action = new ActGetFileDataAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<TrainingProgramVisitDetailDC> GetMemberVisitForTrainingProgram(string gymCode, int branchId, string systemName, DateTime lastUpdatedDateTime)
        {
            GetMemberVisitForTrainingProgramAction action = new GetMemberVisitForTrainingProgramAction(branchId, systemName, lastUpdatedDateTime);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<TrainingProgramClassVisitDetailDC> GetMemberClassVisitForTrainingProgram(string gymCode, int branchId, string systemName, DateTime lastUpdatedDateTime)
        {
            GetMemberClassVisitForTrainingProgramAction action = new GetMemberClassVisitForTrainingProgramAction(branchId, systemName, lastUpdatedDateTime);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #region Gift Vouchers
        //public List<VoucherDC> GetGiftVouchersList(int branchId, string gymCode)
        //{
        //    GetGiftVouchersListAction action = new GetGiftVouchersListAction(branchId);
        //    return action.Execute(EnumDatabase.Exceline, gymCode);
        //}

        //public int AddUpdateGiftVoucher(int branchId, VoucherDC voucher, string gymCode)
        //{
        //    AddUpdateGiftVoucherAction action = new AddUpdateGiftVoucherAction(branchId, voucher);
        //    return action.Execute(EnumDatabase.Exceline, gymCode);
        //}

        //public int GetNextGiftVoucherNo(int branchId, string gymCode)
        //{
        //    GetNextGiftVoucherNoAction action = new GetNextGiftVoucherNoAction(branchId);
        //    return action.Execute(EnumDatabase.Exceline, gymCode);
        //}
        #endregion

        public bool AddRegionDetails(RegionDC region, string gymCode)
        {
            AddRegionDetailsAction action = new AddRegionDetailsAction(region);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool AddCountryDetails(CountryDC country, string gymCode)
        {
            AddCountryDetailsAction action = new AddCountryDetailsAction(country);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ManageTemplate(string gymCode)
        {
            ManageTemplateforRenewalsAction action = new ManageTemplateforRenewalsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int PayCreditNote(int arItemno, List<PayModeDC> paymodes, int memberId, int shopAccoutID, string gymCode, string user, int salePointId, int loggedBranchID)
        {
            PayCreditNoteAction action = new PayCreditNoteAction(arItemno, paymodes, memberId, shopAccoutID, user, salePointId, loggedBranchID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<string> GetGymCodes()
        {
            GetGymCodesAction action = new GetGymCodesAction();
            return action.Execute(EnumDatabase.WorkStation);
        }

        public List<BranchDetailsDC> GetBranches(string gymCode)
        {
            GetGymBranchesAction action = new GetGymBranchesAction(gymCode);
            return action.Execute(EnumDatabase.WorkStation);
        }

        public List<ExceUserBranchDC> GetGymsforAccountNumber(string accountNumber, string gymCode)
        {
            GetGymsforAccountNumberAction action = new GetGymsforAccountNumberAction(accountNumber);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int CheckScheduleOverLapForEmployee(ScheduleItemDC scheduleItem, string gymCode)
        {
            var actionInstance = new CheckActiveTimeOverlapAction(scheduleItem);
            return actionInstance.Execute(EnumDatabase.Exceline,gymCode);
        }

        public MemberFeeGenerationResultDC GenerateMemberFee(List<int> gyms, int month, string gymCode, string user)
        {
            GenerateMemberFeeAction action = new GenerateMemberFeeAction(gyms, month, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineMemberDC> GetMembersForShop(int branchId, string searchText, int status, MemberRole memberRole, int hit,string gymCode)
        {
            var action = new GetMembersForShopAction(branchId,searchText,status,memberRole,hit);
            return action.Execute(EnumDatabase.Exceline, gymCode);

        }

        public List<ExcelineMemberDC> GetAllMemberAndCompany(int branchId, string searchText, int status, int hit, string gymCode)
        {
            var action = new GetAllMemberAndCompanyAction(branchId, searchText, status, hit);
            return action.Execute(EnumDatabase.Exceline, gymCode);

        }

        public bool ValidateGenerateMemberFeeWithMemberFeeMonth(List<int> gyms, string gymCode, string user)
        {
            ValidateGenerateMemberFeeWithMemberFeeMonthAction action = new ValidateGenerateMemberFeeWithMemberFeeMonthAction(gyms, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<string> ConfirmationDetailForGenerateMemberFee(List<int> gyms, int month, string gymCode, string user)
        {
            ConfirmationDetailForGenerateMemberFeeAction action = new ConfirmationDetailForGenerateMemberFeeAction(gyms, month);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public bool UploadArticle(List<ArticleDC> articleList, string user,int branchId, string gymCode)
        {
            UploadArticleAction action = new UploadArticleAction(articleList,branchId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int CancelPayment(int paymentID, bool keepthePayment,string user, string comment, string gymCode)
        {
            CancelPaymentAction action = new CancelPaymentAction(paymentID, keepthePayment, user, comment);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveTaskStatus(string taskName, string description, int branchId, int status, string gymCode)
        {
            SaveTaskStatusAction action = new SaveTaskStatusAction(taskName, description, branchId, status);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<GymEmployeeDC> GetEmployee(int aciveStatus, string gymCode)
        {
            GetEmployeeAction action = new GetEmployeeAction(aciveStatus);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        
        #region USER
        public bool UpdateSettingForUserRoutine(string key, string value, string user, string gymCode)
        {
            try
            {
                var action = new UpdateSettingForUserRoutineAction(key, value, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public MemberShopAccountDC GetMemberShopAccounts(int memberId, string entityType, int hit, string gymCode, bool itemsNeeded)
        {
            GetMemberShopAccountsAction action = new GetMemberShopAccountsAction(memberId, entityType, hit, gymCode, itemsNeeded);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<MemberInvoiceDC> GetMemberInvoicesForContract(int memberID, int contractid, int hit, string gymCode, string user)
        {
            GetMemberInvoicesForContractAction action = new GetMemberInvoicesForContractAction(memberID, contractid, hit, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public bool ValidateEmail(string email, string gymCode)
        {
            ValidateEmailAction action = new ValidateEmailAction(email);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ValidateMemberCard(string memberCard, int memberId, string gymCode)
        {
            ValidateMemberCardAction action = new ValidateMemberCardAction(memberCard, memberId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public Tuple<int, string> ValidateMobile(string mobile, string mobilePrefix, string gymCode)
        {
            ValidateMobileAction action = new ValidateMobileAction(mobile, mobilePrefix);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ValidateGatCardNo(string gatCardNo, string gymCode)
        {
            ValidateGatCardNoAction action = new ValidateGatCardNoAction(gatCardNo);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool AddEventToNotifications(USCommonNotificationDC notification, string gymCode)
        {
            AddEventToNotificationsAction action = new AddEventToNotificationsAction(notification);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #region Logging
        public int SaveShopXMLData(String xmlData, String sessionKey, string gymCode)
        {
            SaveShopXMLDataAction action = new SaveShopXMLDataAction(xmlData, sessionKey);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion
    }
}
