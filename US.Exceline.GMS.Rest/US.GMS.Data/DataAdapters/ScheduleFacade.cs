﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Data.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Data.DataAdapters
{
    public class ScheduleFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static bool UpdateActiveTime(EntityActiveTimeDC activeTime, bool isDelete,int branchId,string user, string gymCode)
        {
            return GetDataAdapter().UpdateActiveTime(activeTime, isDelete,branchId,user, gymCode);
        }

        public static bool UpdateCalenderActiveTimes(List<EntityActiveTimeDC> activeTimeList,int branchId,string user, string gymCode)
        {
            return GetDataAdapter().UpdateCalenderActiveTimes(activeTimeList,branchId,user, gymCode);
        }

        public static bool UpdateSheduleItems(ScheduleDC schedule, string gymCode)
        {
            return GetDataAdapter().UpdateSheduleItems(schedule, gymCode);
        }

        public static List<EntityActiveTimeDC> GetEntityActiveTimes(int branchId, DateTime startDate, DateTime endDate, List<int> entityList, string entityType, string gymCode, string user)
        {
            return GetDataAdapter().GetEntityActiveTimes(branchId, startDate, endDate, entityList, entityType, gymCode, user);
        }

        public static ScheduleDC GetEntitySchedule(int entityId, string entityRoleType, string user, string gymCode)
        {
            return GetDataAdapter().GetEntitySchedule(entityId, entityRoleType, user, gymCode);
        }

        public static bool DeleteScheduleItem(int scheduleItemId, bool activeStatus, string user, string gymCode)
        {
            return GetDataAdapter().DeleteScheduleItem(scheduleItemId, activeStatus, user, gymCode);
        }

        public static bool SaveShedule(int id, string name, string roleId, int branchId, ScheduleDC sheduleDc, string user,string gymCode)
        {
            return GetDataAdapter().SaveShedule(id, name, roleId, branchId, sheduleDc, user, gymCode);
        }

        public static List<ScheduleItemDC> GetScheduleItemsByScheduleId(int scheduleId, string gymCode)
        {
            return GetDataAdapter().GetScheduleItemsByScheduleId(scheduleId, gymCode);
        }
        public static int CheckScheduleOverlapForEmployee(ScheduleItemDC scheduleItem, string gymCode)
        {
            return GetDataAdapter().CheckScheduleOverLapForEmployee(scheduleItem,gymCode);
        }

       
    }
}

