﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "4/19/2012 3:20:29 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Data.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Data.DataAdapters
{
    public class CategoryFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        /// <summary>
        /// Get category types
        /// </summary>
        /// <param name="name">name/code of type</param>
        /// <returns></returns>
        public static List<CategoryTypeDC> GetCategoryTypes(string name, string user, int branchId, string gymCode)
        {
            return GetDataAdapter().GetCategoryTypes(name, user, branchId,  gymCode);
        }

        /// <summary>
        /// Save new category
        /// </summary>
        /// <param name="category"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static int SaveCategory(CategoryDC category, string user, int branchId, string gymCode)
        {
            return GetDataAdapter().SaveCategory(category, user, branchId,  gymCode);
        }

        /// <summary>
        /// Save new region
        /// </summary>
        /// <param name="region"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static int SaveRegion(RegionDC region, string user, int branchId, string gymCode)
        {
            return GetDataAdapter().SaveRegion(region, user, branchId, gymCode);
        }

        /// <summary>
        /// Get categories by type
        /// </summary>
        /// <param name="type">type name</param>
        /// <returns></returns>
        public static List<CategoryDC> GetCategoriesByType(string type, string user, string gymCode)
        {
            return GetDataAdapter().GetCategoriesByType(type, user, gymCode);
        }

        /// <summary>
        /// Update category
        /// </summary>
        /// <param name="category"></param>
        /// <param name="user"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        public static bool UpdateCategory(CategoryDC category, string user, int branchId, string gymCode)
        {
            return GetDataAdapter().UpdateCategory(category, user, branchId, gymCode);
        }

        /// <summary>
        /// Delete category
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="user"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        public static int DeleteCategory(int categoryId, string user, int branchId, string gymCode)
        {
            return GetDataAdapter().DeleteCategory(categoryId, user, branchId, gymCode);
        }

        /// <summary>
        /// Get categories by type
        /// </summary>
        /// <param name="type">type name</param>
        /// <returns></returns>
        public static List<CategoryDC> SearchCategories(string searchText, string searchType, string user, int branchId, string gymCode)
        {
            return GetDataAdapter().SearchCategories(searchText, searchType, user, branchId, gymCode);
        }

        /// <summary>
        /// Get regions
        /// </summary>
        /// <returns></returns>
        public static List<RegionDC> GetRegions(int branchId, string gymCode, string countryId)
        {
            return GetDataAdapter().GetRegions(branchId, gymCode, countryId);
        }
    }
}
