﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Data.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;

namespace US.GMS.Data.DataAdapters
{
    public class ContractFacade
    {

        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static List<PackageDC> GetContracts(int branchId, string searchText, string gymCode)
        {
            return GetDataAdapter().GetContracts(branchId, searchText,  gymCode);
        }

        public static List<ActivityDC> GetActivites(int branchId, string gymCode)
        {
            return GetDataAdapter().GetActivities(branchId, gymCode);
        }

        public static List<ArticleDC> GetArticlesForContract(int categoryID, string gymCode, int branchID)
        {
            return GetDataAdapter().GetArticlesForContract(categoryID, gymCode, branchID);
        }

        public static List<ArticleDC> GetArticlesForVisits(string gymCode, int branchID)
        {
            return GetDataAdapter().GetArticlesForVisits(gymCode, branchID);
        }

        public static bool ManageTemplate(string gymCode)
        {
            return GetDataAdapter().ManageTemplate(gymCode);
        }
    }
}
