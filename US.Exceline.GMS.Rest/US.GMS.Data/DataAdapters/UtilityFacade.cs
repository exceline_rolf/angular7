﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Data.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;

namespace US.GMS.Data.DataAdapters
{
    public class UtilityFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static string GetCityForPostalCode(string postalCode, string gymCode)
        {
            return GetDataAdapter().GetCityForPostalCode(postalCode, gymCode);
        }

        public static List<CalendarHoliday> GetHolidays(DateTime calendarStartDate, int branchId, string gymCode)
        {
            return GetDataAdapter().GetHolidays(calendarStartDate, branchId, gymCode);
        }
    }
}
