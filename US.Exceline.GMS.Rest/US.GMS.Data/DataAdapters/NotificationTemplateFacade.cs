﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Data.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;


namespace US.GMS.Data.DataAdapters
{
    public class NotificationTemplateFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        #region Notification Template
        public static List<NotificationTemplateDC> GetNotificationTemplatesByType(int templateTypeId, NotifyMethodType notifyMethod, int branchId, string gymCode)
        {
            return GetDataAdapter().GetNotificationTemplatesByType(templateTypeId, notifyMethod, branchId, gymCode);
        }
        #endregion
    }
}
