﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Data.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;

namespace US.GMS.Data.DataAdapters
{
    public class PaymentFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static string RegisterPayment(int branchId, int memberId, int articleNo, decimal paymentAmount, decimal discount, string createdUser, decimal invoiceAmount, PaymentTypes paymentType, string gymCode)
        {
            return GetDataAdapter().RegisterPayment(branchId, memberId, articleNo, paymentAmount, discount, createdUser, invoiceAmount, paymentType, gymCode);
        }

        public static SaleResultDC RegisterInstallmentPayment(int memberBranchID, int loggedbranchID, string user, InstallmentDC installment, PaymentDetailDC paymentDetail, string gymCode, int salePointID, ShopSalesDC salesDetails)
        {
            return GetDataAdapter().RegisterInstallmentPayment(memberBranchID, loggedbranchID, user, installment, paymentDetail, gymCode, salePointID, salesDetails);
        }

        public static SaleResultDC GenerateInvoice(int invoiceBranchID, string user, InstallmentDC installment, PaymentDetailDC paymentAmount, string invoiceType, string gymCode, int memberBranchID)
        {
            return GetDataAdapter().GenerateInvoice(invoiceBranchID, user, installment, paymentAmount, invoiceType, gymCode, memberBranchID);
        }

        public static SaleResultDC RegisterInvoicePayment(PaymentDetailDC paymentDetails, string gymCode, int branchID, ShopSalesDC salesDetails, string user)
        {
            return GetDataAdapter().RegisterInvoicePayment(paymentDetails, gymCode, branchID, salesDetails, user);
        }

        public static int CancelPayment(int paymentID, bool keepthePayment, string user, string comment, string gymCode)
        {
            return GetDataAdapter().CancelPayment(paymentID, keepthePayment,user,comment, gymCode);
        }

       
    }
}
