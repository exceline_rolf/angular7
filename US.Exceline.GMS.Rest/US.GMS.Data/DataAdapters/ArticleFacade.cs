﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Data.SystemObjects;

namespace US.GMS.Data.DataAdapters
{
    public class ArticleFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static int SaveArticle(ArticleDC articleDc, string user, int branchID, int activityCategoryId, string gymCode)
        {
            return GetDataAdapter().SaveArticle(articleDc, user, branchID, activityCategoryId, gymCode);
        }

        public static bool ImportArticleList(List<ArticleDC> articleList, string user, int branchID, string gymCode)
        {
            return GetDataAdapter().ImportArticleList(articleList,  user,  branchID,  gymCode);
        }

        public static List<ArticleDC> GetArticles(int articleID, int branchId, string user, ArticleTypes articleType, string keyWord, CategoryDC category, int activityId, bool? isObsalate, bool activeState, string gymCode, bool isFilterByBranch)
        {
            return GetDataAdapter().GetArticles(articleID, branchId, articleType, keyWord, category, activityId, isObsalate, activeState, gymCode, isFilterByBranch);
        }

        public static ArticleDC GetInvoiceFee(int branchId, string gymCode)
        {
            return GetDataAdapter().GetInvoiceFee(branchId, gymCode);
        }

        public static bool UploadArticle(List<ArticleDC> articleList, string user, int branchId, string gymCode)
        {
            return GetDataAdapter().UploadArticle(articleList, user,branchId, gymCode);
        }

        public static List<ArticleDC> GetArticlesSearch(int branchId, string keyWord, string gymCode)
        {
            return GetDataAdapter().GetArticlesSearch(branchId, keyWord, gymCode);
        }
    }
}
