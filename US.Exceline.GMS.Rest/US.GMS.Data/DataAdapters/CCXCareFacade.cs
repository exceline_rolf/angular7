﻿using US.USDF.Core.DomainObjects;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Data.SystemObjects;
using US.Payment.Core.BusinessDomainObjects;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Common;
using System;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Data.DataAdapters
{
    public class CCXCareFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static USDFInvoiceInfo GetAllInvoices(int chunkSize, string gymCode, int creditorNo, int batchsequenceNo, List<string> invoiceTypes)
        {
            return GetDataAdapter().GetAllInvoices(chunkSize, gymCode, creditorNo, batchsequenceNo, invoiceTypes);
        }
        public static USDFInvoiceInfo GetDebtWarningInvoices(int chunkSize, string gymCode, int creditorNo, int batchsequenceNo)
        {
            return GetDataAdapter().GetDebtWarningInvoices(chunkSize, gymCode, creditorNo, batchsequenceNo);
        }

        public static string GetGymCodeForCreditor(int creditorNo)
        {
            return GetDataAdapter().GetGymCodeForCreditor(creditorNo);
        }

        public static USDFInvoiceInfo GetDirectPayments(int chunkSize, string gymCode, int creditorNo, int batchSequenceNo)
        {
            return GetDataAdapter().GetDirectPayments(chunkSize, gymCode, creditorNo, batchSequenceNo);
        }

        public static int AddPaymentStatus(USPPaymentStatusFileRow paymentStatus, string gymCode)
        {
            return GetDataAdapter().AddPaymentStatus(paymentStatus,gymCode);
        }

        public static int AddNewCancelledStatus(US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPNewOrCancelledFileRow newCancelledStatus, string gymCode)
        {
            return GetDataAdapter().AddNewCancelledStatus(newCancelledStatus, gymCode);
        }

        public static int AddDataImportReceipt(DataImportReceiptDetail importReceipt, string gymCode)
        {
            return GetDataAdapter().AddDataImportReceipt(importReceipt, gymCode);
        }

        public static int ImportPaymentStatus( string gymCode)
        {
            return GetDataAdapter().ImportPaymentStatus(gymCode);
        }

        public static int ImportDataImportReceipt(string gymCode)
        {
            return GetDataAdapter().ImportDataImportReceipt(gymCode);
        }

        public static int ImportNewCancelledStatus( string gymCode)
        {
            return GetDataAdapter().ImportNewCancelledStatus(gymCode);
        }


        public static int UpdateDWStatus(string gymCode)
        {
            return GetDataAdapter().UpdateDWStatus(gymCode);
        }

        public static int UpdateTemplateStatus(string gymCode)
        {
            return GetDataAdapter().UpdateTemplateStatus(gymCode);
        }

        public static List<GymCompanyDC> GetGymCompanies()
        {
            return GetDataAdapter().GetGymCompanies();
        }

        public static bool UpdateExportStatus(string gymCode)
        {
            return GetDataAdapter().UpdateExportStatus(gymCode);
        }

        public static List<Core.SystemObjects.ImportStatusDC> GetClaimStatus(string gymCode, int hit, int status, DateTime date, ImportStatusDC advancedData)
        {
            return GetDataAdapter().GetClaimStatus(gymCode, hit, status, date, advancedData);
        }

        public static bool ReScheduleClaim(string gymCode, int itemNo)
        {
            return GetDataAdapter().ReScheduleClaim(gymCode, itemNo);
        }

        public static PaymentImportStatusDC GetPaymentImportStatus(string gymCode, int hit, DateTime regDate)
        {
            return GetDataAdapter().GetPaymentImportStatus(gymCode, hit, regDate);
        }

        public static List<ImportStatusDC> GetClaimExportStatusForMember(string gymCode, int memberID, int hit, DateTime? transferDate, string type, string user)
        {
            return GetDataAdapter().GetClaimExportStatusForMember(gymCode, memberID, hit, transferDate, type, user);
        }

        public static bool MoveToShopAccount(int statusID, bool isImportStatus, string gymCode)
        {
            return GetDataAdapter().MoveToShopAccount(statusID, isImportStatus, gymCode);
        }
    }
}
