﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "6/28/2012 12:31:07 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Data.SystemObjects;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Data.DataAdapters
{
    public class ActivityFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static List<ActivityDC> GetActivitiesForEntity(int entityId, string entityType, string gymCode, int branchId)
        {
            return GetDataAdapter().GetActivitiesForEntity(entityId, entityType, gymCode, branchId);
        }
    }
}
