﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Data.SystemObjects;

namespace US.GMS.Data.DataAdapters
{
    public class UserFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static bool UpdateSettingForUserRoutine(string key, string value, string user, string gymCode)
        {
            try
            {
                return GetDataAdapter().UpdateSettingForUserRoutine(key, value, user, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
