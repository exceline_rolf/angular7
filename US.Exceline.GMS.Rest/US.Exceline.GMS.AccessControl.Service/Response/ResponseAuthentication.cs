﻿using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.AccessControl;

namespace US.Exceline.GMS.AccessControl.Service.Response
{
    [DataContract]
    public class ResponseAuthentication
    {
        public ResponseAuthentication() { }
        public ResponseAuthentication(ExceAccessControlAuthenticationDC authentication, ResponseStatus status)
        {
            Authentication = authentication;
            Status = status;
        }

        [DataMember(Order = 1)]
        public ExceAccessControlAuthenticationDC Authentication { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}