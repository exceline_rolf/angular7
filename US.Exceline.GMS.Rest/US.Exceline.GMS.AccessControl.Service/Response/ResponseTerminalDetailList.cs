﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.AccessControl;

namespace US.Exceline.GMS.AccessControl.Service.Response
{
    [DataContract]
    public class ResponseTerminalDetailList
    {
        public ResponseTerminalDetailList() { }
        public ResponseTerminalDetailList(List<ExceAccessControlTerminalDetailDC> terminalDetailList, ResponseStatus status)
        {
            TerminalDetailList = terminalDetailList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceAccessControlTerminalDetailDC> TerminalDetailList { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}