﻿using System.ServiceModel;
using System.ServiceModel.Web;
using US.Exceline.GMS.AccessControl.Service.Response;
using US.GMS.Core.DomainObjects.AccessControl;

namespace US.Exceline.GMS.AccessControl.Service
{
    [ServiceContract]
    public interface IAccessControlService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "ChkAuth/{branchId}/{cardNo}/{companyId}/{terminalId}")]
        ResponseAuthentication CheckAuthentication(string branchId, string cardNo, string companyId, string terminalId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "ChkAuthForGatPurchase/{branchId}/{cardNo}/{companyId}/{terminalId}/{durtaion}/{price}")]
        ResponseAuthentication CheckAuthenticationForGatPurchase(string branchId, string cardNo, string companyId, string terminalId,string durtaion,string price);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "GetTermDetail/{branchId}/{companyId}")]
        ResponseTerminalDetailList GetTerminalDetails(string branchId, string companyId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/TerminalStatus/Update/{companyId}")]
        ResponseStatus UpdateTerminalStatus(string companyId, ExceAccessControlUpdateTerminalStatusDC terminalStatusDetail);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/RegPurchase/Update/{companyId}")]
        ResponseStatus RegisterPurchase(string companyId, ExceAccessControlRegisterPurchaseDC purchaseDetail);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/RegVerdingEvent/Update/{companyId}")]
        ResponseStatus RegisterVendingEvent(string companyId, ExceAccessControlVendingEventDC vendingEventDetail);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/RegSunBedPurchase/Update/{companyId}")]
        ResponseStatus RegisterSubBedPurchase(string companyId, ExceAccessControlRegisterPurchaseDC purchaseDetail);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/RegMemVisit/Update/{companyId}")]
        ResponseStatus RegisterMemberVisit(string companyId, ExceAccessControlRegisterMemberVisitDC visit);

    }
}
