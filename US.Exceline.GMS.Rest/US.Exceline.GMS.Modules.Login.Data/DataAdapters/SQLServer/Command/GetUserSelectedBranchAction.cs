﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin;
using System.Data.Common;
using US_DataAccess;
using System.Data;

namespace US.Exceline.GMS.Modules.Login.Data.DataAdapters.SQLServer.Command
{
    public class GetUserSelectedBranchAction : USDBActionBase<List<ExceUserBranchDC>>
    {
        private string _userName = string.Empty;
        private string _loggedUser = string.Empty;
        public GetUserSelectedBranchAction(string userName,string loggedUser)
        {
            this._userName = userName;
            _loggedUser = loggedUser;
            OverwriteUser(_loggedUser);
        }

        protected override List<ExceUserBranchDC> Body(DbConnection connection)
        {
            List<ExceUserBranchDC> selectedBranchList = new List<ExceUserBranchDC>();

            try
            {
                string storedProcedure = "USP_AUT_GetSelectedBranches";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _userName));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExceUserBranchDC branch = new ExceUserBranchDC();
                    if (!string.IsNullOrEmpty(reader["Id"].ToString()))
                    {
                        branch.Id = Convert.ToInt32(reader["Id"]);
                    }

                    branch.BranchId = Convert.ToInt32(reader["BranchId"]);
                    branch.UserId = Convert.ToInt32(reader["UserId"]);
                    branch.BranchName = reader["BranchName"].ToString();
                    branch.AccountNo = reader["AccountNo"].ToString().Trim();
                    branch.IsDeleted = false;

                    selectedBranchList.Add(branch);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return selectedBranchList;
        }
    }
}
