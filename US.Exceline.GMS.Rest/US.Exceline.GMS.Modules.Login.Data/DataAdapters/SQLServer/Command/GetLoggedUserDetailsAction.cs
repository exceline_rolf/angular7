﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Login;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Login.Data.DataAdapters.SQLServer.Command
{
    public class GetLoggedUserDetailsAction : USDBActionBase<List<ExecUserDC>>
    {
        private string _userName = string.Empty;
        private GetGymLicensAction _getGymLicens;
        public GetLoggedUserDetailsAction(string userName)
        {
            this._userName = userName;
            OverwriteUser(userName);
        }

        protected override List<ExecUserDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ExecUserDC> userhList = new List<ExecUserDC>();

            try
            {
                string storedProcedure = "USExceGMSGetUserDetails";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _userName));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExecUserDC exceUser = new ExecUserDC();
                    if (!string.IsNullOrEmpty(reader["Id"].ToString()))
                    {
                        exceUser.UserID = Convert.ToInt32(reader["Id"]);
                    }

                    if (!string.IsNullOrEmpty(reader["UserName"].ToString()))
                    {
                        exceUser.UserName = reader["UserName"].ToString();
                    }

                    if (!string.IsNullOrEmpty(reader["DisplayName"].ToString()))
                    {
                        exceUser.DisplayName = reader["DisplayName"].ToString();
                    }

                    if (!string.IsNullOrEmpty(reader["Email"].ToString()))
                    {
                        exceUser.Email = reader["Email"].ToString();
                    }

                    if (!string.IsNullOrEmpty(reader["IsLoginFirstTime"].ToString()))
                    {
                        exceUser.IsLoginFirstTime = Convert.ToBoolean(reader["IsLoginFirstTime"].ToString());
                    }

                    if (!string.IsNullOrEmpty(reader["CardNumber"].ToString()))
                    {
                        exceUser.CardNumber = Convert.ToString(reader["CardNumber"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["HardwareProfileId"].ToString()))
                    {
                        exceUser.HardwareProfileId = Convert.ToInt32(reader["HardwareProfileId"]);
                    }
                    if (!string.IsNullOrEmpty(reader["EmployeeId"].ToString()))
                    {
                        exceUser.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
                    }

                    if (!string.IsNullOrEmpty(reader["RoleId"].ToString()))
                    {
                        exceUser.RoleId = Convert.ToInt32(reader["RoleId"]);
                    }
                    if (reader["Culture"] != DBNull.Value)
                    exceUser.Culture = reader["Culture"].ToString();

                    _getGymLicens = new GetGymLicensAction(exceUser,_userName);
                    exceUser = _getGymLicens.Execute(EnumDatabase.USP);
                    userhList.Add(exceUser);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return userhList;
        }
    }
}
