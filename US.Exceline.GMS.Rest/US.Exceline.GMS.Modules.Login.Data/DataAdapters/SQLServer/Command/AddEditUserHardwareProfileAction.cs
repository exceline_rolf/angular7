﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Login.Data.DataAdapters.SQLServer.Command
{
    public class AddEditUserHardwareProfileAction : USDBActionBase<bool>
    {
        private int _userId;
        private int _hardwareProfileId;

        public AddEditUserHardwareProfileAction(int userId, int hardwareProfileId, string user)
        {
            _userId = userId;
            _hardwareProfileId = hardwareProfileId;
            OverwriteUser(user);
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool isSuccess = false;

            try
            {
                string storedProcedure = "USExceGMSAddEditUserHardwareProfile";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@userId", DbType.Int32 , _userId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@hardwareProfileId", DbType.Int32, _hardwareProfileId));
                cmd.ExecuteNonQuery();
                isSuccess = true;          
                  
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSuccess;
        }
    }
}
