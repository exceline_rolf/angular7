﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Login;
using System.Data;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Login.Data.DataAdapters.SQLServer.Command
{
    public class GetEmailFromUsernameAction : USDBActionBase<String>
    {
        private String _userName = string.Empty;
        public GetEmailFromUsernameAction(string userName)
        {
            _userName = userName;
        }


        protected override String Body(DbConnection connection)
        {
            String _email = string.Empty;
            try
            {
                const string storedProcedure = "USExceGMSManageLoginGetPotentialUserEmail";
                DbCommand command = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                command.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _userName));
                DbDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    if ( !string.IsNullOrEmpty(reader["Email"].ToString()) )
                    {
                        _email = reader["Email"].ToString();
                    }
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return _email;
        }
    }


}
