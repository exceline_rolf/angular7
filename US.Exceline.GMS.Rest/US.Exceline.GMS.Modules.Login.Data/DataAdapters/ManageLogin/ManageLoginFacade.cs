﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Login.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Login;

namespace US.Exceline.GMS.Modules.Login.Data.DataAdapters.ManageLogin
{
    public class ManageLoginFacade
    {
        private static IManageLoginDataAdapter GetDataAdapter()
        {
            return new SQLServerManageLoginDataAdapter();
        }

        public static bool SetNewUserPassword(ExcePWSetRecDC userData)
        {
            return GetDataAdapter().SetNewUserPassword(userData);
        }

        public static List<ExceUserBranchDC> GetUserSelectedBranches(string userName, string user)
        {
            return GetDataAdapter().GetUserSelectedBranches(userName, user);
        }

        public static List<ExecUserDC> GetLoggedUserDetails(string userName)
        {
            return GetDataAdapter().GetLoggedUserDetails(userName);
        }

        public static bool AddEditUserHardwareProfile(int userId, int hardwareProfileId, string user)
        {
            return GetDataAdapter().AddEditUserHardwareProfile(userId, hardwareProfileId, user);
        }
        public static ExcePWRecDC AnonymousUserPasswordReset(string gymCode, string potentialUserName)
        {
            return GetDataAdapter().AnonymousUserPasswordReset(gymCode, potentialUserName);
        }
    }
}
