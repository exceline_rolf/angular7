﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageShop;
using US.Exceline.GMS.Modules.Shop.Data.DataAdapters;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Shop.BusinessLogic.ManageShop
{
    public class ShopManager
    {
        #region Voucher
        public static OperationResult<List<VoucherDC>> GetVouchersList(int branchId, string gymCode)
        {
            OperationResult<List<VoucherDC>> result = new OperationResult<List<VoucherDC>>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetVouchersList(branchId, gymCode);
            }
            catch (Exception ex)
            {
                {
                    result.CreateMessage("Error in getting vouchers list " + ex.Message, MessageTypes.ERROR);
                }
            }
            return result;
        }

        public static OperationResult<bool> AddVoucher(int branchId, VoucherDC voucher, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ShopFacade.AddVoucher(branchId, voucher, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in adding voucher " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> EditVoucher(int branchId, VoucherDC voucher, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ShopFacade.EditVoucher(branchId, voucher, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in editing voucher " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region Return Item
        public static OperationResult<List<ReturnItemDC>> GetReturnItemList(int branchId, string gymCode)
        {
            OperationResult<List<ReturnItemDC>> result = new OperationResult<List<ReturnItemDC>>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetReturnItemList(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting return list " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<List<ShopSalesPaymentDC>> GetShopBillSummary(DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            OperationResult<List<ShopSalesPaymentDC>> result = new OperationResult<List<ShopSalesPaymentDC>>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetShopBillSummary(fromDate, toDate, branchId, gymCode);
            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in getting  shop  " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<ShopBillDetailDC> GetShopSaleBillDetail(int saleId, int branchId, string gymCode)
        {
            OperationResult<ShopBillDetailDC> result = new OperationResult<ShopBillDetailDC>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetShopSaleBillDetail(saleId, branchId, gymCode);
            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in getting  shop bill  details  " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }



        public static OperationResult<int> AddReturnedItem(int branchId, ReturnItemDC returnedItem, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ShopFacade.AddReturnedItem(branchId, returnedItem, gymCode);
            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in adding a return item " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> EditReturnedItem(int branchId, ReturnItemDC editReturnedItem, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ShopFacade.EditReturnedItem(branchId, editReturnedItem, gymCode);
            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in editing a return item " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        public static OperationResult<List<ShopSalesItemDC>> GetDailySales(DateTime salesDate, int branchId, string gymCode)
        {
            OperationResult<List<ShopSalesItemDC>> result = new OperationResult<List<ShopSalesItemDC>>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetDailySales(salesDate, branchId, gymCode);
            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in getting detais" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<SalePointDC>> GetSalesPointList(int branchID, string gymCode)
        {
            OperationResult<List<SalePointDC>> result = new OperationResult<List<SalePointDC>>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetSalesPointList(branchID, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Sales Point List" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }




        public static OperationResult<List<WithdrawalDC>> GetWithdrawalByMemberId(DateTime startDate, DateTime endDate, WithdrawalTypes type, int memberId, string gymCode, string user)
        {
            OperationResult<List<WithdrawalDC>> result = new OperationResult<List<WithdrawalDC>>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetWithdrawalByMemberId(startDate, endDate, type, memberId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting withdrawals by member " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<SalePointDC> GetSalesPointByMachineName(int branchID, string machineName, string gymCode)
        {
            OperationResult<SalePointDC> result = new OperationResult<SalePointDC>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetSalesPointByMachineName(branchID, machineName, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Sales Point " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }



        public static OperationResult<int> SaveWithdrawal(int branchId, WithdrawalDC withDrawal, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ShopFacade.SaveWithdrawal(branchId, withDrawal, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in saving withdrawal" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> IsCashdrawerOpen(string machinename, string gymCode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ShopFacade.IsCashdrawerOpen(machinename, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error when checking if cashdrawer is open" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveOpenCashRegister(int branchId, CashRegisterDC cashRegister, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ShopFacade.SaveOpenCashRegister(branchId, cashRegister, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in saving withdrawal" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<InvoiceForReturnDC>> GetInvoicesForArticleReturn(int memberID, int branchId, string articleNo, int shopOnly, string gymCode, string user)
        {
            OperationResult<List<InvoiceForReturnDC>> result = new OperationResult<List<InvoiceForReturnDC>>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetInvoicesForArticleReturn(memberID, branchId, articleNo, shopOnly, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error when getting invoices for return selection" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SavePointOfSale(int branchId, SalePointDC pointOfSale, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ShopFacade.SavePointOfSale(branchId, pointOfSale, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in saving point of sale" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }



        public static OperationResult<bool> AddEditUserHardwareProfile(int branchId, int hardwareProfileId, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ShopFacade.AddEditUserHardwareProfile(branchId, hardwareProfileId, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving selected hardware profile" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> GetSelectedHardwareProfile(int branchId, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetSelectedHardwareProfile(branchId, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Get selecting " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<Dictionary<string, decimal>> GetMemberEconomyBalances(string gymCode, int memberId)
        {
            OperationResult<Dictionary<string, decimal>> result = new OperationResult<Dictionary<string, decimal>>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetMemberEconomyBalances(gymCode, memberId);
            }
            catch (Exception ex)
            {
                {
                    result.CreateMessage("Error in getting member economy  balances " + ex.Message, MessageTypes.ERROR);
                }
            }
            return result;
        }

        public static OperationResult<int> GetNextGiftVoucherNumber(string seqId, string subSeqId, string gymCode)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetNextGiftVoucherNo(seqId, subSeqId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting member economy  balances " + ex.Message, MessageTypes.ERROR);
                throw ex;
            }
            return result;
        }

        public static OperationResult<List<ShopSalesPaymentDC>> GetGiftVoucherSaleSummary(DateTime fromDate, DateTime toDate,
                                                                         int branchId, string gymCode)
        {
            var result = new OperationResult<List<ShopSalesPaymentDC>>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetGiftVoucherSaleSummary(fromDate, toDate, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting gift voucher sales " + ex.Message, MessageTypes.ERROR);
                throw ex;
            }
            return result;
        }

        public static OperationResult<int> ValidateGiftVoucherNumber(string voucherNumber,decimal payment, string gymCode, int branchID)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ShopFacade.ValidateGiftVoucherNumber(voucherNumber, payment, gymCode, branchID);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error validating voucher number " + ex.Message, MessageTypes.ERROR);
                throw ex;
            }
            return result;
        }
        public static OperationResult<int> SaveGiftVoucherArticle(ArticleDC article, string gymCode)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ShopFacade.SaveGiftVoucherArticle(article, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error Saving Gift Voucher " + ex.Message, MessageTypes.ERROR);
                throw ex;
            }
            return result;
        }

        public static OperationResult<Dictionary<string, string>> GetGiftVoucherDetail(string gymCode)
        {
            var result = new OperationResult<Dictionary<string, string>>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetGiftVoucherDetail(gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static DailySettlementPrintData ExcelineGetDailySettlementDataForPrint(String dailySettlementId, String reconiliationId, String SalePointId, String branchId, String mode, String startDate, String endDate, String gymCode, String countedCashDraft)
        {
            var result = new DailySettlementPrintData();
            try
            {
                result = ShopFacade.ExcelineGetDailySettlementDataForPrint(dailySettlementId, reconiliationId, SalePointId, branchId, mode, startDate, endDate, gymCode, countedCashDraft);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static HelpDataForDailySettlmentPrintData ExcelineGetHelpDataForDailySettlmentPrintData(String branchId, String FromDateTime , String ToDateTime, String SalePointId, String gymCode)
        {
            var result = new HelpDataForDailySettlmentPrintData();
            try
            {
                result = ShopFacade.ExcelineGetHelpDataForDailySettlmentPrintData(branchId, FromDateTime, ToDateTime, SalePointId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static int CheckIfReceiptIsValidForPrint(String invoiceNo, int branchId,  String gymCode)
        {
            int result = -1;
            try
            {
                result = ShopFacade.CheckIfReceiptIsValidForPrint(invoiceNo, branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static String ExcelineGetReconcilliationTextForDailySettlementPrint(int dailSettlementId, String gymCode)
        {
            String result = String.Empty;
            try
            {
                result = ShopFacade.ExcelineGetReconcilliationTextForDailySettlementPrint(dailSettlementId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
