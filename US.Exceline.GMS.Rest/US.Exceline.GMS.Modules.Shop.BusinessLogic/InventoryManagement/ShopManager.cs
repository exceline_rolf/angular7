﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using US.Common.Logging.API;
using US.Common.Notification.API;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Communication.API;
using US.Communication.Core.SystemCore.DomainObjects;
using US.GMS.Core.ResultNotifications;
using US.Exceline.GMS.Modules.Shop.Data.DataAdapters;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.Utils;
using US.Payment.Core.Enums;
using US.Payment.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Shop.BusinessLogic.InventoryManagement
{
    public class ShopManager
    {
        public static OperationResult<SaleResultDC> AddShopSales(String cardType, InstallmentDC installment, ShopSalesDC shopSaleDetails, PaymentDetailDC paymentDetails, int memberBranchID, int loggedbranchID, string user, string gymCode, bool isBookingpayment, string custRoleType, String sessionKey = "")
        {
            OperationResult<SaleResultDC> result = new OperationResult<SaleResultDC>();
            try
            {
                int returnVal = 1;
                if (installment.AddOnList != null && installment.AddOnList.Any(x => x.StockStatus && x.Quantity > 0))
                {
                    returnVal = ShopFacade.ValidateArticleStockLevel(installment, gymCode, loggedbranchID);
                }

                if (returnVal > 0)
                {
                    if (custRoleType == "EMP")
                    {
                        var memberId = -1;
                        memberId = ShopFacade.GetMemberByEmployeeID(installment.MemberId, memberBranchID, gymCode, installment.CreatedUser);
                        installment.MemberId = memberId;
                        shopSaleDetails.EntitiyId = memberId;
                        paymentDetails.PaidMemberId = memberId;
                    }
                    PayModeDC NEXTORDERpayMode = paymentDetails.PayModes.FirstOrDefault(X => X.PaymentTypeCode == "NEXTORDER");
                    if (NEXTORDERpayMode == null)
                    {
                        if (installment.AddOnList.Where(x => x.Quantity >= 0).ToList().Count > 0)
                        {
                            installment.Id = ShopFacade.SaveShopInstallment(installment, memberBranchID, gymCode, sessionKey);
                        }
                        paymentDetails.PaymentSourceId = installment.Id.ToString();
                    }
                    result.OperationReturnValue = ShopFacade.AddShopSales(cardType, installment, shopSaleDetails, paymentDetails, memberBranchID, loggedbranchID, user, gymCode, isBookingpayment);

                    foreach (var payMode in paymentDetails.PayModes)
                    {
                        if (payMode.PaymentTypeCode == "PREPAIDBALANCE" || payMode.PaymentTypeCode == "INVOICE" || payMode.PaymentTypeCode == "ONACCOUNT" || payMode.PaymentTypeCode == "NEXTORDER")
                        {
                            // create parameter string for notification
                            Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                            paraList.Add(TemplateFieldEnum.DATE, DateTime.Now.ToString());
                            paraList.Add(TemplateFieldEnum.MEMBERNAME, "");
                            paraList.Add(TemplateFieldEnum.MEMBERNO, result.OperationReturnValue.CustId.ToString());
                            paraList.Add(TemplateFieldEnum.AMOUNT, payMode.Amount.ToString());
                            paraList.Add(TemplateFieldEnum.PAYMENTMETHOD, payMode.PaymentType.ToString());


                            // create common notification
                            USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                            commonNotification.IsTemplateNotification = true;
                            commonNotification.ParameterString = paraList;
                            commonNotification.BranchID = memberBranchID;
                            commonNotification.CreatedUser = user;
                            commonNotification.Role = "MEM";
                            commonNotification.NotificationType = NotificationTypeEnum.Messages;
                            commonNotification.Severity = NotificationSeverityEnum.Minor;
                            commonNotification.MemberID = result.OperationReturnValue.MemberId;
                            commonNotification.Status = NotificationStatusEnum.New;

                            // FOR SMS **************************************************************
                            commonNotification.Type = TextTemplateEnum.PAYMENTNOTIFICATION;
                            commonNotification.Method = NotificationMethodType.SMS;
                            commonNotification.Title = "SHOP PAYMENT NOTIFICATION";

                            //add SMS notification into notification tables

                            USImportResult<int> notification = US.Common.Notification.API.NotificationAPI.AddNotification(commonNotification);

                            if (!result.ErrorOccured)
                            {
                                var SMS_SETTING = ConfigurationManager.AppSettings["USC_Notification_SMS"];
                                if (SMS_SETTING != null)
                                {
                                    string application = SMS_SETTING.ToString();
                                    string outputFormat = "SMS";

                                    try
                                    {
                                        // send SMS through the USC API
                                        IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), notification.MethodReturnValue.ToString());

                                        NotificationStatusEnum status = NotificationStatusEnum.Attended;

                                        if (smsStatus != null)
                                            status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                        var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                                        // update status in notification table
                                        USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(notification.MethodReturnValue, user, status, statusMessage);

                                        if (updateResult.ErrorOccured)
                                        {
                                            foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                            {
                                                if (message.ErrorLevel == ErrorLevels.Error)
                                                {
                                                    USLogError.WriteToFile(message.Message, new Exception(), user);
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        USLogError.WriteToFile(e.Message, new Exception(), user);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    result.OperationReturnValue.SaleStatus = SaleErrors.NOSTOCK;
                }

            }
            catch (Exception ex)
            {
                result.OperationReturnValue = new SaleResultDC();
                result.OperationReturnValue.SaleStatus = SaleErrors.ERROR;
                result.CreateMessage("Error in Adding sales" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<SaleResultDC> AddSunBedShopSales(SunBedHelperObject sunBedHelperObject, SunBedBusinessHelperObject businessHelperObj)
        {                     
            PaymentDetailDC paymentDetails;
            int memberBranchID;           
            string user;                 
            OperationResult<SaleResultDC> result = new OperationResult<SaleResultDC>();

            if (sunBedHelperObject.Installments.Count == 1)
            {                               
                paymentDetails = sunBedHelperObject.PaymentDetails.FirstOrDefault();
                memberBranchID = sunBedHelperObject.MemberBranchID;                
                user = sunBedHelperObject.User;                        

                try
                {
                    int returnVal = 1;
                    if (returnVal > 0)
                    {                       
                        result.OperationReturnValue = ShopFacade.AddSunBedShopSales(sunBedHelperObject,businessHelperObj);

                        foreach (var payMode in paymentDetails.PayModes)
                        {
                            if (payMode.PaymentTypeCode == "PREPAIDBALANCE" || payMode.PaymentTypeCode == "INVOICE" || payMode.PaymentTypeCode == "ONACCOUNT" || payMode.PaymentTypeCode == "NEXTORDER")
                            {
                                // create parameter string for notification
                                Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                                paraList.Add(TemplateFieldEnum.DATE, DateTime.Now.ToString());
                                paraList.Add(TemplateFieldEnum.MEMBERNAME, "");
                                paraList.Add(TemplateFieldEnum.MEMBERNO, result.OperationReturnValue.CustId.ToString());
                                paraList.Add(TemplateFieldEnum.AMOUNT, payMode.Amount.ToString());
                                paraList.Add(TemplateFieldEnum.PAYMENTMETHOD, payMode.PaymentType.ToString());


                                // create common notification
                                USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                                commonNotification.IsTemplateNotification = true;
                                commonNotification.ParameterString = paraList;
                                commonNotification.BranchID = memberBranchID;
                                commonNotification.CreatedUser = user;
                                commonNotification.Role = "MEM";
                                commonNotification.NotificationType = NotificationTypeEnum.Messages;
                                commonNotification.Severity = NotificationSeverityEnum.Minor;
                                commonNotification.MemberID = result.OperationReturnValue.MemberId;
                                commonNotification.Status = NotificationStatusEnum.New;

                                // FOR SMS **************************************************************
                                commonNotification.Type = TextTemplateEnum.PAYMENTNOTIFICATION;
                                commonNotification.Method = NotificationMethodType.SMS;
                                commonNotification.Title = "SHOP PAYMENT NOTIFICATION";

                                //add SMS notification into notification tables

                                USImportResult<int> notification = US.Common.Notification.API.NotificationAPI.AddNotification(commonNotification);

                                if (!result.ErrorOccured)
                                {
                                    var SMS_SETTING = ConfigurationManager.AppSettings["USC_Notification_SMS"];
                                    if (SMS_SETTING != null)
                                    {
                                        string application = SMS_SETTING.ToString();
                                        string outputFormat = "SMS";

                                        try
                                        {
                                            // send SMS through the USC API
                                            IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), notification.MethodReturnValue.ToString());

                                            NotificationStatusEnum status = NotificationStatusEnum.Attended;

                                            if (smsStatus != null)
                                                status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                            var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                                            // update status in notification table
                                            USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(notification.MethodReturnValue, user, status, statusMessage);

                                            if (updateResult.ErrorOccured)
                                            {
                                                foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                                {
                                                    if (message.ErrorLevel == ErrorLevels.Error)
                                                    {
                                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            USLogError.WriteToFile(e.Message, new Exception(), user);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        result.OperationReturnValue.SaleStatus = SaleErrors.NOSTOCK;
                    }

                }
                catch (Exception ex)
                {
                    result.OperationReturnValue = new SaleResultDC();
                    result.OperationReturnValue.SaleStatus = SaleErrors.ERROR;
                    result.CreateMessage("Error in Adding sales" + ex.Message, MessageTypes.ERROR);
                }
            }
            //OnAccount and the Next order sale this ocurres rarly
            else if (sunBedHelperObject.Installments.Count == 2)
            {                              
                paymentDetails = sunBedHelperObject.PaymentDetails[0];
                memberBranchID = sunBedHelperObject.MemberBranchID;                
                user = sunBedHelperObject.User;                      

                try
                {
                    int returnVal = 1;
                    if (returnVal > 0)
                    {                        
                        result.OperationReturnValue = ShopFacade.AddSunBedShopSales(sunBedHelperObject,businessHelperObj);

                        foreach (var payMode in paymentDetails.PayModes)
                        {
                            if (payMode.PaymentTypeCode == "PREPAIDBALANCE" || payMode.PaymentTypeCode == "INVOICE" || payMode.PaymentTypeCode == "ONACCOUNT" || payMode.PaymentTypeCode == "NEXTORDER")
                            {
                                // create parameter string for notification
                                Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                                paraList.Add(TemplateFieldEnum.DATE, DateTime.Now.ToString());
                                paraList.Add(TemplateFieldEnum.MEMBERNAME, "");
                                paraList.Add(TemplateFieldEnum.MEMBERNO, result.OperationReturnValue.CustId.ToString());
                                paraList.Add(TemplateFieldEnum.AMOUNT, payMode.Amount.ToString());
                                paraList.Add(TemplateFieldEnum.PAYMENTMETHOD, payMode.PaymentType.ToString());


                                // create common notification
                                USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                                commonNotification.IsTemplateNotification = true;
                                commonNotification.ParameterString = paraList;
                                commonNotification.BranchID = memberBranchID;
                                commonNotification.CreatedUser = user;
                                commonNotification.Role = "MEM";
                                commonNotification.NotificationType = NotificationTypeEnum.Messages;
                                commonNotification.Severity = NotificationSeverityEnum.Minor;
                                commonNotification.MemberID = result.OperationReturnValue.MemberId;
                                commonNotification.Status = NotificationStatusEnum.New;

                                // FOR SMS **************************************************************
                                commonNotification.Type = TextTemplateEnum.PAYMENTNOTIFICATION;
                                commonNotification.Method = NotificationMethodType.SMS;
                                commonNotification.Title = "SHOP PAYMENT NOTIFICATION";

                                //add SMS notification into notification tables

                                USImportResult<int> notification = US.Common.Notification.API.NotificationAPI.AddNotification(commonNotification);

                                if (!result.ErrorOccured)
                                {
                                    var SMS_SETTING = ConfigurationManager.AppSettings["USC_Notification_SMS"];
                                    if (SMS_SETTING != null)
                                    {
                                        string application = SMS_SETTING.ToString();
                                        string outputFormat = "SMS";

                                        try
                                        {
                                            // send SMS through the USC API
                                            IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), notification.MethodReturnValue.ToString());

                                            NotificationStatusEnum status = NotificationStatusEnum.Attended;

                                            if (smsStatus != null)
                                                status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                            var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                                            // update status in notification table
                                            USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(notification.MethodReturnValue, user, status, statusMessage);

                                            if (updateResult.ErrorOccured)
                                            {
                                                foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                                {
                                                    if (message.ErrorLevel == ErrorLevels.Error)
                                                    {
                                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            USLogError.WriteToFile(e.Message, new Exception(), user);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        result.OperationReturnValue.SaleStatus = SaleErrors.NOSTOCK;
                    }

                }
                catch (Exception ex)
                {
                    result.OperationReturnValue = new SaleResultDC();
                    result.OperationReturnValue.SaleStatus = SaleErrors.ERROR;
                    result.CreateMessage("Error in Adding sales" + ex.Message, MessageTypes.ERROR);
                }
            }

            return result;
        }

        public static OperationResult<DailySettlementDC> GetDailySettlements(int dailySettlementId, string gymCode)
        {
            OperationResult<DailySettlementDC> result = new OperationResult<DailySettlementDC>();
            try
            {
                result.OperationReturnValue = ShopFacade.GetDailySettlements(dailySettlementId, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting daily settlements" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> AddDailySettlements(DailySettlementDC dailySettlement, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ShopFacade.AddDailySettlements(dailySettlement, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Adding daily settlements" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}

