﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class AutomatedPrintPDFUpdateItemListAction : USDBActionBase<bool>
    {
        private List<ExceNotificationScheduleItem> _itemList;
        private string _user;

        public AutomatedPrintPDFUpdateItemListAction(List<ExceNotificationScheduleItem> itemList, string user)
        {
            _itemList = itemList;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            try
            {
                const string storedProcedure = "US_AutomatedPrintPDFUpdateItemList";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@itemList", SqlDbType.Structured, GetItemList()));
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DataTable GetItemList()
        {
            try
            {
                var dataTable = new DataTable();
                dataTable.Columns.Add(new DataColumn("KeyData", typeof(int)));
                dataTable.Columns.Add(new DataColumn("ValueData", typeof(string)));

                foreach (var item in _itemList)
                {
                    var dataTableRow = dataTable.NewRow();
                    dataTableRow["KeyData"] = item.ScheduleID;
                    dataTableRow["ValueData"] = item.NotificationId;
                    dataTable.Rows.Add(dataTableRow);
                }

                return dataTable;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
