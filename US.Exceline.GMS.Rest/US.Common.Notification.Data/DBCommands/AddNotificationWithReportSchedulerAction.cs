﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    class AddNotificationWithReportSchedulerAction : USDBActionBase<bool>
    {
        private readonly string _user = string.Empty;

        public AddNotificationWithReportSchedulerAction(string user)
        {
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            try
            {
                const string storedProcedure = "US_AddNotificationWithReportScheduler";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                DbParameter count = new SqlParameter() { DbType = DbType.Int32, ParameterName = "@outPutCount", Direction = ParameterDirection.Output };
                cmd.Parameters.Add(count);
                DbParameter error = new SqlParameter() { DbType = DbType.Int32, ParameterName = "@errorCount", Direction = ParameterDirection.Output };
                cmd.Parameters.Add(error);
                cmd.ExecuteNonQuery();

                if (Convert.ToInt32(error.Value) > 0)
                    new Exception("AddNotificationWithReportScheduler processed with error: " + error.Value + " out of " + count.Value + " had errors.");

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
