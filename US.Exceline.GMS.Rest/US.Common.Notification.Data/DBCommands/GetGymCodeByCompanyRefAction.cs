﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class GetGymCodeByCompanyRefAction : USDBActionBase<string>
    {
        private string _gymCode;
        private readonly string _gymCompanyRef;

        public GetGymCodeByCompanyRefAction(string gymCompanyRef)
        {
            _gymCompanyRef = gymCompanyRef;
        }

        protected override string Body(DbConnection connection)
        {
            const string storedProcedure = "ExceWorkStationGetGymCodeByGymCompanyRef";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gymCompanyRef", DbType.String, _gymCompanyRef));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    _gymCode = reader["GymCode"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _gymCode;
        }
    }
}
