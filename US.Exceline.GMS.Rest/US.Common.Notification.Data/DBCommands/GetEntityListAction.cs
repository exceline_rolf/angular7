﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class GetEntityListAction : USDBActionBase<List<EntityDC>>
    {
        protected override List<EntityDC> Body(DbConnection connection)
        {
            DbDataReader reader = null;
            var entityList = new List<EntityDC>();
            try
            {
                const string storedProcedure = "US_GetNotificationEntityList";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var entity = new EntityDC()
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        DisplayName = Convert.ToString(reader["DisplayName"]),
                        BasedOn = Convert.ToString(reader["BasedOn"])
                    };
                    entityList.Add(entity);
                }
                return entityList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }
    }
}
