﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class UpdateNotificationWithReceiveSMSAction : USDBActionBase<bool>
    {
        private readonly string _notificationCode = string.Empty;
        private readonly string _message = string.Empty;
        private readonly string _sender = string.Empty;
        private readonly string _user = string.Empty;

        public UpdateNotificationWithReceiveSMSAction(string notificationCode, string message, string sender, string user)
        {
            _notificationCode = notificationCode;
            _message = message;
            _sender = sender;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedure = "US_NotificationCommonAddReceivedSMS";
            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@notificationCode", DbType.String, _notificationCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@message", DbType.String, _message));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sender", DbType.String, _sender));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
