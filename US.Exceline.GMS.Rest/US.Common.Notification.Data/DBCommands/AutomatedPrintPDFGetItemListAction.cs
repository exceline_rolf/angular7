﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class AutomatedPrintPDFGetItemListAction : USDBActionBase<List<ExceNotificationScheduleItem>>
    {
        private string _user;

        public AutomatedPrintPDFGetItemListAction(string user)
        {
            _user = user;
        }

        protected override List<ExceNotificationScheduleItem> Body(DbConnection connection)
        {
            try
            {
                var itemList = new List<ExceNotificationScheduleItem>();
                const string storedProcedure = "US_AutomatedPrintPDFGetItemList";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var item = new ExceNotificationScheduleItem();

                    item.Id = Convert.ToInt32(reader["Id"]);
                    item.ScheduleID = Convert.ToInt32(reader["ScheduleID"]);
                    item.Template = Convert.ToString(reader["Template"]);
                    item.Code = Convert.ToString(reader["TemplateCode"]);
                    item.Type = Convert.ToString(reader["TemplateType"]);
                    item.EntityId = Convert.ToString(reader["EntityId"]);
                    item.EntityType = Convert.ToString(reader["EntityType"]);
                    item.NotificationId = Convert.ToString(reader["NotificationId"]);

                    itemList.Add(item);
                }
                return itemList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
