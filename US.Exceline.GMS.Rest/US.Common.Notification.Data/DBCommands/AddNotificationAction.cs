﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    class AddNotificationAction : USDBActionBase<int>
    {
        private USNotificationTree _notification = new USNotificationTree();
        private string _loggedUser = string.Empty;

        public AddNotificationAction(USNotificationTree notification)
        {
            _notification = notification;
            if (_notification != null && _notification.Notification != null)
            {
                _loggedUser = _notification.Notification.CreatedUser;
            }
            OverwriteUser(_loggedUser);
        }

        protected override int Body(DbConnection connection)
        {
            int _result = -1;
            try
            {
                string storedProcedure = "dbo.US_AddNotification";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Module", DbType.String, _notification.Notification.Module));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Title", DbType.String, _notification.Notification.Title));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _notification.Notification.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _notification.Notification.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Role", DbType.String, _notification.Notification.CreatedUserRoleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TypeId", DbType.Int32, _notification.Notification.TypeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SeverityId", DbType.Int32, _notification.Notification.SeverityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _notification.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TextTemplate", DbType.String, _notification.Notification.TextTemplate.ToString()));

                if (_notification.MemberId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _notification.MemberId));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DueDate", DbType.DateTime, _notification.Notification.DueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastAttendDate", DbType.DateTime, _notification.Notification.LastAttendDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Status", DbType.Int32, _notification.Notification.StatusId));

                DbParameter para3 = new SqlParameter();
                para3.DbType = DbType.Int32;
                para3.ParameterName = "@NotificationId";
                para3.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para3);
                cmd.ExecuteNonQuery();

                string actionProcedure = "dbo.US_AddNotificationAction";
                DbCommand action = CreateCommand(System.Data.CommandType.StoredProcedure, actionProcedure);
                action.Parameters.Add(DataAcessUtils.CreateParam("@Comment", DbType.String, _notification.Action.Comment));
                action.Parameters.Add(DataAcessUtils.CreateParam("@Assign", DbType.String, _notification.Action.AssgnId));
                action.Parameters.Add(DataAcessUtils.CreateParam("@ReAssign", DbType.String, _notification.Action.ReAssignId));
                action.Parameters.Add(DataAcessUtils.CreateParam("@TimeSpent", DbType.Double, _notification.Action.TimeSpent));
                action.Parameters.Add(DataAcessUtils.CreateParam("@NotificationId", DbType.Int32, Convert.ToInt32(para3.Value)));


                DbParameter para4 = new SqlParameter();
                para4.DbType = DbType.Int32;
                para4.ParameterName = "@ActionId";
                para4.Direction = ParameterDirection.Output;
                action.Parameters.Add(para4);
                action.ExecuteNonQuery();

                foreach (USNotificationChannel method in _notification.NotificationMethods)
                {
                    string subProcedure = "dbo.US_AddNotificationChannel";
                    DbCommand subCmd = CreateCommand(System.Data.CommandType.StoredProcedure, subProcedure);
                    subCmd.Parameters.Add(DataAcessUtils.CreateParam("@DueDate", DbType.Date, method.ChannelDueDate));
                    subCmd.Parameters.Add(DataAcessUtils.CreateParam("@Occurance", DbType.String, method.OccuranceType));
                    subCmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, method.ChannelMessage));
                    subCmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _notification.BranchId));
                    subCmd.Parameters.Add(DataAcessUtils.CreateParam("@NotificationID", DbType.Int32, Convert.ToInt32(para3.Value)));
                    if (method.ChannelNextDueDate == DateTime.MinValue)
                    {
                        subCmd.Parameters.Add(DataAcessUtils.CreateParam("@NextDueDate", DbType.Date, null));
                    }
                    else
                    {
                        subCmd.Parameters.Add(DataAcessUtils.CreateParam("@NextDueDate", DbType.Date, method.ChannelNextDueDate));
                    }
                    subCmd.Parameters.Add(DataAcessUtils.CreateParam("@Assign", DbType.String, method.ChannelAssign));
                    subCmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus", DbType.Boolean, method.ChannelStatus));
                    subCmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateName", DbType.String, method.TemplateName));
                    subCmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateCategory", DbType.String, method.TemplateCategory));
                    subCmd.Parameters.Add(DataAcessUtils.CreateParam("@MessageType", DbType.String, method.MessageType));

                    subCmd.ExecuteNonQuery();
                }

                _result = Convert.ToInt32(para3.Value);
            }
            catch
            {
                throw;
            }
            return _result;
        }
    }
}
