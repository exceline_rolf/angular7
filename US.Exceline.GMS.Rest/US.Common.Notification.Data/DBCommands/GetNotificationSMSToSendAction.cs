﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    class GetNotificationSMSToSendAction : USDBActionBase<List<NotificationSMS>>
    {
        private readonly int _notificationID = -1;
        private readonly string _gymCode = string.Empty;
        private readonly List<NotificationSMS> _smsList = new List<NotificationSMS>();

        public GetNotificationSMSToSendAction(int notificationID, string gymCode)
        {
            _notificationID = notificationID;
            _gymCode = gymCode;
        }

        protected override List<NotificationSMS> Body(DbConnection connection)
        {
            DbDataReader reader = null;
            try
            {
                const string storedProcedure = "US_GetNotificationSMSDetails";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@notificationID", DbType.Int32, _notificationID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gymCode", DbType.String, _gymCode));

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var sms = new NotificationSMS
                        {
                            NotificationID = Convert.ToInt32(reader["NotificationID"]),
                            Message = Convert.ToString(reader["Message"]),
                            Sender = Convert.ToString(reader["Sender"]),
                            Receiver = Convert.ToString(reader["Receiver"]),
                            batchId = Convert.ToString(reader["batchId"]),
                            StatusCode = "Attended"
                        };
                    _smsList.Add(sms);
                }
                return _smsList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }
    }
}
