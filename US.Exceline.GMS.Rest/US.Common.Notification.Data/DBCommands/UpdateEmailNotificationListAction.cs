﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    class UpdateEmailNotificationListAction : USDBActionBase<bool>
    {
        private readonly List<NotificationEmail> _emailList = new List<NotificationEmail>();
        private readonly string _user = string.Empty;

        public UpdateEmailNotificationListAction(List<NotificationEmail> smsList, string user)
        {
            _emailList = smsList;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            try
            {
                const string storedProcedure = "US_UpdateNotificationStatusList";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@notificationStatusList", SqlDbType.Structured, GetNotificationList()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                DbParameter para = new SqlParameter();
                para.DbType = DbType.Boolean;
                para.ParameterName = "@outPut";
                para.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para);
                cmd.ExecuteNonQuery();

                return Convert.ToBoolean(para.Value);
            }
            catch (Exception)
            {
                throw;
            }
        }


        private DataTable GetNotificationList()
        {
            try
            {
                var dataTable = new DataTable();
                dataTable.Columns.Add(new DataColumn("NotificationID", typeof(int)));
                dataTable.Columns.Add(new DataColumn("StatusCode", typeof(string)));
                dataTable.Columns.Add(new DataColumn("StatusMessage", typeof(string)));

                foreach (var email in _emailList)
                {
                    var dataTableRow = dataTable.NewRow();
                    dataTableRow["NotificationID"] = email.NotificationID;
                    dataTableRow["StatusCode"] = email.StatusCode;
                    dataTableRow["StatusMessage"] = email.StatusMessage;
                    dataTable.Rows.Add(dataTableRow);
                }

                return dataTable;
            }
            catch
            {
                throw;
            }
        }
    }
}
