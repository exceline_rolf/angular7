﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Common.Notification.Data
{
    class AddAutomatedSMSNotificationAction :USDBActionBase<string>
    {
        private int _branchID;
        private string _comment = "Automated SMS Notification";
        private string _user = "System";
        private string _result = string.Empty;

        public AddAutomatedSMSNotificationAction(int branchID)
        {
            _branchID = branchID;
        } 

        protected override string Body(DbConnection connection)
        {

            //if(AddATGNotApproved() > 0)
            //    _result = _result + "ATGNotApproved:Success ";
            //else
            //    _result = _result + "ATGNotApproved:Fail ";

            //if(AddBookingReminder() > 0)
            //    _result = _result + "BookingReminder:Success ";
            //else
            //    _result = _result + "BookingReminder:Fail ";

            //if(AddBrithday() > 0)
            //    _result = _result + "Brithday:Success ";
            //else
            //    _result = _result + "Brithday:Fail ";

            //if(AddContractEndingReminder() > 0)
            //    _result = _result + "ContractEndingReminder:Success ";
            //else
            //    _result = _result + "ContractEndingReminder:Fail ";

            //if(AddFreezeEndReminder() > 0)
            //    _result = _result + "FreezeEndReminder:Success ";
            //else
            //    _result = _result + "FreezeEndReminder:Fail ";

            //if(AddNoVisitReminder() > 0)
            //    _result = _result + "NoVisitReminder:Success ";
            //else
            //    _result = _result + "NoVisitReminder:Fail ";

            //if(AddReorderingShop() > 0)
            //    _result = _result + "ReorderingShop:Success ";
            //else
            //    _result = _result + "ReorderingShop:Fail ";

            if (AddAutomatedSmsAddBookingReminder() > 0)
                _result = _result + "BookingReminder:Success ";
            else
                _result = _result + "BookingReminder:Fail";

            return _result;
        }

        //private int AddATGNotApproved()
        //{
        //    try
        //    {
        //        string storedProcedure = "US_Notification_AutomatedSMSAddATGNotApproved";

        //        DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchID));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Comment", DbType.String, _comment));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
               
        //        DbParameter para = new SqlParameter();
        //        para.DbType = DbType.Int32;
        //        para.ParameterName = "@outPut";
        //        para.Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add(para);
        //        cmd.ExecuteNonQuery();

        //        return Convert.ToInt32(para.Value);
        //    }
        //    catch(Exception)
        //    {
        //        return -1;
        //    }
        //}

        //private int AddBookingReminder()
        //{
        //    try
        //    {
        //        string storedProcedure = "US_Notification_AutomatedSMSAddBookingReminder";

        //        DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchID));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Comment", DbType.String, _comment));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

        //        DbParameter para = new SqlParameter();
        //        para.DbType = DbType.Int32;
        //        para.ParameterName = "@outPut";
        //        para.Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add(para);
        //        cmd.ExecuteNonQuery();

        //        return Convert.ToInt32(para.Value);
        //    }
        //    catch(Exception)
        //    {
        //        return -1;
        //    }
        //}

        //private int AddBrithday()
        //{
        //    try
        //    {
        //        string storedProcedure = "US_Notification_AutomatedSMSAddBrithday";

        //        DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchID));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Comment", DbType.String, _comment));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

        //        DbParameter para = new SqlParameter();
        //        para.DbType = DbType.Int32;
        //        para.ParameterName = "@outPut";
        //        para.Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add(para);
        //        cmd.ExecuteNonQuery();

        //        return Convert.ToInt32(para.Value);
        //    }
        //    catch(Exception)
        //    {
        //        return -1;
        //    }
        //}

        //private int AddContractEndingReminder()
        //{
        //    try
        //    {
        //        string storedProcedure = "US_Notification_AutomatedSMSAddContractEndingReminder";

        //        DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchID));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Comment", DbType.String, _comment));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

        //        DbParameter para = new SqlParameter();
        //        para.DbType = DbType.Int32;
        //        para.ParameterName = "@outPut";
        //        para.Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add(para);
        //        cmd.ExecuteNonQuery();

        //        return Convert.ToInt32(para.Value);
        //    }
        //    catch(Exception)
        //    {
        //        return -1;
        //    }
        //}

        //private int AddFreezeEndReminder()
        //{
        //    try
        //    {
        //        string storedProcedure = "US_Notification_AutomatedSMSAddFreezeEndReminder";

        //        DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchID));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Comment", DbType.String, _comment));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

        //        DbParameter para = new SqlParameter();
        //        para.DbType = DbType.Int32;
        //        para.ParameterName = "@outPut";
        //        para.Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add(para);
        //        cmd.ExecuteNonQuery();

        //        return Convert.ToInt32(para.Value);
        //    }
        //    catch(Exception)
        //    {
        //        return -1;
        //    }
        //}

        //private int AddNoVisitReminder()
        //{
        //    try
        //    {
        //        string storedProcedure = "US_Notification_AutomatedSMSAddNoVisitReminder";

        //        DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchID));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Comment", DbType.String, _comment));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

        //        DbParameter para = new SqlParameter();
        //        para.DbType = DbType.Int32;
        //        para.ParameterName = "@outPut";
        //        para.Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add(para);
        //        cmd.ExecuteNonQuery();

        //        return Convert.ToInt32(para.Value);
        //    }
        //    catch(Exception)
        //    {
        //        return -1;
        //    }
        //}

        //private int AddReorderingShop()
        //{
        //    try
        //    {
        //        string storedProcedure = "US_Notification_AutomatedSMSAddReorderingShop";

        //        DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchID));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Comment", DbType.String, _comment));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

        //        DbParameter para = new SqlParameter();
        //        para.DbType = DbType.Int32;
        //        para.ParameterName = "@outPut";
        //        para.Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add(para);
        //        cmd.ExecuteNonQuery();

        //        return Convert.ToInt32(para.Value);
        //    }
        //    catch(Exception)
        //    {
        //        return -1;
        //    }
        //}

        private int AddAutomatedSmsAddBookingReminder()
        {
            try
            {
                string storedProcedure = "US_Notification_AutomatedSMSAddBookingReminder";

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Comment", DbType.String, _comment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                DbParameter para = new SqlParameter();
                para.DbType = DbType.Int32;
                para.ParameterName = "@outPut";
                para.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para);
                cmd.ExecuteNonQuery();

                return Convert.ToInt32(para.Value);
            }
            catch (Exception)
            {
                return -1;
            }
        }
                                                            
    }
}
