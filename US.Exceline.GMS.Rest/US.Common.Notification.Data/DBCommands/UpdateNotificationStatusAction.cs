﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class UpdateNotificationStatusAction : USDBActionBase<bool>
    {
        private readonly List<int> _selectedIds;
        private readonly int _status;
        private readonly string _user;

        public UpdateNotificationStatusAction(List<int> selectedIds, int status, string user)
        {
            _selectedIds = selectedIds;
            _status = status;
            _user = user;
            OverwriteUser(_user);
        }

        protected override bool Body(DbConnection connection)
        {
            bool status;
            try
            {
                const string storedProcedure = "dbo.US_UpdateNotificationStatus";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@status", DbType.Int32, _status));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                DataTable notificationIdListTable = new DataTable();
                DataColumn col = null;
                col = new DataColumn("NotificationId", typeof(Int32));
                notificationIdListTable.Columns.Add(col);

                foreach (var notificationId in _selectedIds)
                {
                    notificationIdListTable.Rows.Add(notificationId);
                }
                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@notificationIdList";
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.Value = notificationIdListTable;
                cmd.Parameters.Add(parameter);
                cmd.ExecuteNonQuery();
                status = true;
            }
            catch (Exception)
            {
                status = false;
            }
            return status;
        }
    }
}
