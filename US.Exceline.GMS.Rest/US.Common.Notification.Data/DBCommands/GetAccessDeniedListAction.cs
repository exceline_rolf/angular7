﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class GetAccessDeniedListAction : USDBActionBase<List<AccessDeniedLog>>
    {
        private readonly DateTime _fromDate;
        private readonly DateTime _toDate;
        private readonly int _branchId;

        public GetAccessDeniedListAction(DateTime fromDate, DateTime toDate, int branchId)
        {
            _fromDate = fromDate;
            _toDate = toDate;
            _branchId = branchId;
        }

        protected override List<AccessDeniedLog> Body(DbConnection connection)
        {
            try
            {
                var accessList = new List<AccessDeniedLog>();
                const string storedProcedureName = "US_GetAccessDeniedLogDetail";

                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", DbType.DateTime, _fromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@toDate", DbType.DateTime, _toDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var item = new AccessDeniedLog
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        MemberNo = Convert.ToInt32(reader["CustId"]),
                        TerminalId = Convert.ToInt32(reader["TerminalId"]),
                        TerminalName = Convert.ToString(reader["TerminalName"]),
                        BranchId = Convert.ToInt32(reader["BranchId"]),
                        CardNo = reader["CardNo"].ToString(),
                        Message = reader["Message"].ToString(),
                        CreatedDate = Convert.ToDateTime(reader["CreatedDate"])
                    };
                    accessList.Add(item);
                }
                return accessList;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
