﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.Common.Notification.Core.DomainObjects;
using US.GMS.Core.DomainObjects.Notification;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    class GetChangeLogNotificationListAction : USDBActionBase<List<USNotificationSummaryInfo>>
    {
        private string _user = string.Empty;
        private int _branchId = -1;
        private DateTime _fromDate = DateTime.Now.AddDays(-7);
        private DateTime _toDate = DateTime.Now;
        private NotifyMethodType _notifyMethod = NotifyMethodType.NONE;

        public GetChangeLogNotificationListAction(string user, int branchId, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod)
        {
            _user = user;
            _branchId = branchId;
            _fromDate = fromDate;
            _toDate = toDate;
            _notifyMethod = notifyMethod;
            OverwriteUser(_user);
        }

        protected override List<USNotificationSummaryInfo> Body(DbConnection connection)
        {
            try
            {
                string storedProcedureName = "dbo.US_GetChangeLogNotification";

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@notifyMethod", DbType.String, _notifyMethod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", DbType.DateTime, _fromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@toDate", DbType.DateTime, _toDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));

                DbDataReader reader = cmd.ExecuteReader();

                List<USNotificationSummaryInfo> notificationList = new List<USNotificationSummaryInfo>();

                while (reader.Read())
                {
                    USNotificationSummaryInfo notificationSummary = new USNotificationSummaryInfo();
                    notificationSummary.Id = Convert.ToInt32(reader["NotificationId"]);
                    notificationSummary.SeverityId = Convert.ToInt32(reader["SeverityId"]);
                    notificationSummary.Severity = reader["Severity"].ToString();
                    notificationSummary.TypeId = Convert.ToInt32(reader["TypeId"]);
                    notificationSummary.Type = reader["Type"].ToString();
                    notificationSummary.Title = reader["Title"].ToString();
                    notificationSummary.Description = reader["Description"].ToString();
                    notificationSummary.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                    notificationSummary.DueDate = (reader["DueDate"] != DBNull.Value) ? Convert.ToDateTime(reader["DueDate"]) : (DateTime?)null;
                    notificationSummary.AssignTo = reader["AssignTo"].ToString();
                    notificationSummary.LastAttendDate = (reader["LastAttendDate"] != DBNull.Value) ? Convert.ToDateTime(reader["LastAttendDate"]) : (DateTime?)null;
                    notificationSummary.StatusId = Convert.ToInt32(reader["StatusId"]);
                    notificationSummary.Status = reader["Status"].ToString();
                    notificationSummary.Category = reader["Category"].ToString();
                    if (notificationSummary.StatusId == 1)
                    {
                        notificationSummary.IsNew = true;
                    }

                    switch (notificationSummary.Type)
                    {
                        case "Errors":
                            notificationSummary.ErrorsIconVisibility = "Visible";
                            break;
                        case "Warning":
                            notificationSummary.WarningIconVisibility = "Visible";
                            break;
                        case "TODO":
                            notificationSummary.TodoIconVisibility = "Visible";
                            break;
                        case "Messages":
                            notificationSummary.MessagesIconVisibility = "Visible";
                            break;
                    }

                    switch (notificationSummary.Severity)
                    {
                        case "Critical":
                            notificationSummary.CriticalIconVisibility = "Visible";
                            break;
                        case "Moderate":
                            notificationSummary.ModerateIconVisibility = "Visible";
                            break;
                        case "Minor":
                            notificationSummary.MinorIconVisibility = "Visible";
                            break;
                    }
                    notificationList.Add(notificationSummary);
                }

                return notificationList;
            }
            catch
            {
                throw;
            }
        }
    }
}
