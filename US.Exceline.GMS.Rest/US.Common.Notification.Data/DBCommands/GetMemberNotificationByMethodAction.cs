﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.IO;
using System.Text;
using US.Common.Notification.Core.DomainObjects;
using US.GMS.Core.DomainObjects.Notification;
using US_DataAccess;
using System.Web;

namespace US.Common.Notification.Data.DBCommands
{
    public class GetMemberNotificationByMethodAction : USDBActionBase<List<USNotificationSummaryInfo>>
    {

        private readonly string _user = string.Empty;
        private readonly NotifyMethodType _notifyMethod = NotifyMethodType.NONE;
        private readonly DateTime _fromDate = DateTime.Now;
        private readonly DateTime _toDate = DateTime.Now;
        private readonly int _memberId = -1;
        private readonly int _branchId;

        public GetMemberNotificationByMethodAction(int memberId, string user, int branchId, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod)
        {
            _notifyMethod = notifyMethod;
            _user = user;
            _branchId = branchId;
            _memberId = memberId;
            _fromDate = fromDate;
            _toDate = toDate;
            OverwriteUser(_user);
        }


        protected override List<USNotificationSummaryInfo> Body(DbConnection connection)
        {
            try
            {
                const string storedProcedureName = "dbo.US_GetMemberNotificationByMethod";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@notifyMethod", DbType.String, _notifyMethod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", DbType.DateTime, _fromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@toDate", DbType.DateTime, _toDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _user));

                var reader = cmd.ExecuteReader();
                var notificationSummaryList = new List<USNotificationSummaryInfo>();

                while (reader.Read())
                {
                    var notificationSummary = new USNotificationSummaryInfo();
                    notificationSummary.Id = Convert.ToInt32(reader["NotificationId"]);
                    notificationSummary.SeverityId = Convert.ToInt32(reader["SeverityId"]);
                    notificationSummary.Severity = reader["Severity"].ToString();
                    notificationSummary.TypeId = Convert.ToInt32(reader["TypeId"]);
                    notificationSummary.Type = reader["Type"].ToString();
                    notificationSummary.Title = reader["Title"].ToString();
                    notificationSummary.Description = reader["Description"].ToString();
                    notificationSummary.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                    notificationSummary.Category = Convert.ToString(reader["Category"]);
                    notificationSummary.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    notificationSummary.ActionDate = Convert.ToDateTime(reader["ActionDate"]);
                    notificationSummary.MobileNo = Convert.ToString(reader["MobileNo"]);
                    notificationSummary.GymName = Convert.ToString(reader["GymName"]);
                    notificationSummary.DueDate = (reader["DueDate"] != DBNull.Value) ? Convert.ToDateTime(reader["DueDate"]) : (DateTime?)null;
                    notificationSummary.AssignTo = Convert.ToString(reader["AssignTo"]);
                    notificationSummary.LastAttendDate = (reader["LastAttendDate"] != DBNull.Value) ? Convert.ToDateTime(reader["LastAttendDate"]) : (DateTime?)null;
                    notificationSummary.StatusId = Convert.ToInt32(reader["StatusId"]);
                    notificationSummary.Comment = Convert.ToString(reader["Comment"]);
                    if (notificationSummary.StatusId == 1)
                    {
                        notificationSummary.IsNew = true;
                    }
                    notificationSummary.Status = reader["Status"].ToString();

                    switch (notificationSummary.Type)
                    {
                        case "Errors":
                            notificationSummary.ErrorsIconVisibility = "Visible";
                            break;
                        case "Warning":
                            notificationSummary.WarningIconVisibility = "Visible";
                            break;
                        case "TODO":
                            notificationSummary.TodoIconVisibility = "Visible";
                            break;
                        case "Messages":
                            notificationSummary.MessagesIconVisibility = "Visible";
                            break;
                    }

                    switch (notificationSummary.Severity)
                    {
                        case "Critical":
                            notificationSummary.CriticalIconVisibility = "Visible";
                            break;
                        case "Moderate":
                            notificationSummary.ModerateIconVisibility = "Visible";
                            break;
                        case "Minor":
                            notificationSummary.MinorIconVisibility = "Visible";
                            break;
                    }
                    notificationSummaryList.Add(notificationSummary);
                }

                List<string> code = new List<string>( new string[] {"A1", "A3", "A4", "A5", "A7", "C4", "C5", "E0", "E4", "E5", "C6", "C7", "C9", "E8", "E9", "EC", "D1", "F1", "F2", "F6", "D8", "D6", "DC", "F9", "FC", "DF" });
                List<char> decode = new List<char>( new char[] {'¡','£','¤','¥','§','Ä','Å','à','ä','å','Æ','Ç','É','è','é','ì','Ñ','ñ','ò','ö','Ø','Ö','Ü','ù','ü','ß'});
    
                foreach (var element in notificationSummaryList)
                {
                    string decodedString = Uri.UnescapeDataString(element.Description).ToString();
                    var str = new StringBuilder();
                    for (var i = 0; i < decodedString.Length; i++)
                    {
                        //Debug.WriteLine("i "+ i);
                        char c = decodedString[i];
                        //Debug.WriteLine("c: " + c);
                        if (c == '%')
                        {
                            string val = decodedString[i + 1] + "" + decodedString[i + 2];
                            //Debug.WriteLine("Val: " + val);
                            int j = code.IndexOf(val);
                            //Debug.WriteLine("J: " + j);
                            if (j == -1)
                            {
                                str.Append(c);
                            }
                            else
                            {
                                str.Append(decode[j]);
                                i = i + 2;
                            }
                        }
                        else
                        {
                            str.Append(c);
                        }
                        //Debug.WriteLine("*************************");
                    }
                    element.Description = str.ToString();
                }

                return notificationSummaryList;
            }
            catch
            {
                throw;
            }

        }
    }
}
