﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;
using System.Collections.Generic;
using US.Common.Notification.Core.Enums;

namespace US.Common.Notification.Data.DBCommands
{
    class AddCommonNotificationAction : USDBActionBase<int>
    {
        private USCommonNotificationDC _commonNotification;
        private string _storedProcedure = string.Empty;

            public AddCommonNotificationAction(USCommonNotificationDC commonNotification)
        {
            _commonNotification = commonNotification;
        }

        protected override int Body(DbConnection connection)
        {
            try
            {
                string _parameterString = GetParameterString(_commonNotification.ParameterString);

                if(string.IsNullOrEmpty(_commonNotification.TempateText))
                    _storedProcedure = "US_NotificationCommon";
                else
                    _storedProcedure = "US_NotificationCommonWithTemplate";

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, _storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DirectOrTemplatedNotification", DbType.Boolean, _commonNotification.IsTemplateNotification));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, _commonNotification.Type.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@method", DbType.String, _commonNotification.Method.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@parameterString", DbType.String, _parameterString));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _commonNotification.BranchID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NTitle", DbType.String, _commonNotification.Title));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NDescription", DbType.String, _commonNotification.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NCreatedUser", DbType.String, _commonNotification.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NRole", DbType.String, _commonNotification.Role));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NType", DbType.String, _commonNotification.NotificationType.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NSeverity", DbType.String, _commonNotification.Severity.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NmemberId", DbType.Int32, _commonNotification.MemberID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NStatus", DbType.String, _commonNotification.Status.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AComment", DbType.String, _commonNotification.Comment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CMessageType", DbType.String, _commonNotification.Method.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NAttachmentFileParth", DbType.String, _commonNotification.AttachmentFileParth));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CSenderDescription", DbType.String, _commonNotification.SenderDescription));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@templateText", DbType.String, _commonNotification.TempateText));

                DbParameter para3 = new SqlParameter();
                para3.DbType = DbType.Int32;
                para3.ParameterName = "@NotificationId";
                para3.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para3);
                cmd.ExecuteNonQuery();

                return Convert.ToInt32(para3.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetParameterString(Dictionary<TemplateFieldEnum, string> ParameterList)
        {
            string parameterString = string.Empty;

            foreach (var Parameter in ParameterList)
            {
                parameterString = parameterString + Parameter.Key + ":" + Parameter.Value + "|";
            }

            return parameterString;
        }
    }
}
