﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class GetSMSNotificationTemplateByEntityAction : USDBActionBase<List<ExceNotificationTepmlateDC>>
    {
        private readonly string _entity;

        public GetSMSNotificationTemplateByEntityAction(string entity)
        {
            _entity = entity;
        }

        protected override List<ExceNotificationTepmlateDC> Body(DbConnection connection)
        {
            var templateList = new List<ExceNotificationTepmlateDC>();
            const string storedProcedureName = "GetSMSNotificationTemplateByEntity";
            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entity", DbType.String, _entity));
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var template = new ExceNotificationTepmlateDC();
                    template.ID = Convert.ToInt32(reader["ID"]);
                    template.MethodID = Convert.ToInt32(reader["MethodID"]);
                    template.TypeID = Convert.ToInt32(reader["TypeID"]);
                    template.Name = Convert.ToString(reader["Name"]);
                    template.Text = Convert.ToString(reader["Text"]);
                    templateList.Add(template);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return templateList;
        }
    }
}
