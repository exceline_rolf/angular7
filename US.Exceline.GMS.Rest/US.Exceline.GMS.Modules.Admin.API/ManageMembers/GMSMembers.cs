﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.API
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "06/06/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageMembers;

namespace US.Exceline.GMS.Modules.Admin.API.ManageMembers
{
    public class GMSMembers
    {
        public static OperationResult<List<string>> GetAccessPoints(string gymCode)
        {
            return MemberManager.GetAccessPoints(gymCode);
        }

        public static OperationResult<List<EntityVisitDC>> GetMemberVisits(DateTime startDate, DateTime endDate, int branchId,string gymCode)
        {
            return MemberManager.GetMemberVisits(startDate, endDate, branchId, gymCode);
        }

        public static OperationResult<List<ExcelineMemberDC>> GetSponsorsforSponsoring(int branchId,DateTime startDate, DateTime endDate, string gymCode)
        {
            return MemberManager.GetSponsorsforSponsoring(branchId,startDate,endDate, gymCode);
        }

        public static OperationResult<bool> GenerateSponsorOrders(US.GMS.Core.SystemObjects.SponsorShipGenerationDetailsDC sponsorDetails, string gymCode, string user)
        {
            return MemberManager.GenerateSponsorOrders(sponsorDetails, gymCode,  user);
        }

        public static OperationResult<bool> GetSponsoringProcessStatus(string guiID, string gymCode, string user)
        {
            return MemberManager.GetSponsoringProcessStatus( guiID,  gymCode,  user);
        }
    }
}
