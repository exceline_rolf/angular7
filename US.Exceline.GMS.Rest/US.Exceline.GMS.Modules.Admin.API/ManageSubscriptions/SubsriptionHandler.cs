﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US_DataAccess;
using US.GMS.Core.ResultNotifications;


namespace US.Exceline.GMS.Modules.Admin.API.ManageSubscriptions
{
    public class SubsriptionHandler
    {
        public List<SubscriptionInfo> SubscriptionsForAccess(String gymCode)
        {

            var action = new GetSubscriptionsForAccess();
            List<SubscriptionInfo> testList = action.Execute(EnumDatabase.Exceline, gymCode);
            
            return testList;
        }

    }
}
