﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.API
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "5/29/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageEmployees;

namespace US.Exceline.GMS.Modules.Admin.API.ManageEmployees
{
    public class GMSTimeEntry
    {
        public static OperationResult<List<TimeEntryDC>> GetTimeEntriesForAdminView(DateTime startDate, DateTime endDate, string gymCode)
        {
            return TimeEntryManager.GetTimeEntriesForAdminView(startDate, endDate, gymCode);
        }
    }
}
