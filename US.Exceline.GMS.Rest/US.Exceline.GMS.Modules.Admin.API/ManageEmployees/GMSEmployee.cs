﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageEmployees;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;

namespace US.Exceline.GMS.Modules.Admin.API.ManageEmployees
{
   public class GMSEmployee
    {
       public static OperationResult<string> UpdateEmployees(EmployeeDC employeeDc, string imageFolderPath,string user, string gymCode)
       {
           return EmployeeManager.UpdateEmployees(employeeDc, imageFolderPath,user, gymCode);
       }

       public static OperationResult<string> UpdateMemeberActiveness(int trainerId, string rolId, int branchId, string user, bool isActive, string gymCode)
       {
           return EmployeeManager.UpdateMemeberActiveness(trainerId, rolId, branchId, user, isActive,  gymCode);
       }

       public static OperationResult<List<ExcelineTaskDC>> GetEntityTasks(int entityId, int branchId, string entityRoleType, string gymCode)
       {
           return EmployeeManager.GetEntityTasks(entityId, branchId, entityRoleType,  gymCode);
       }

       public static OperationResult<bool> SaveEntittTimeEntry(TimeEntryDC timeEntry, string gymCode)
       {
           return EmployeeManager.SaveEntityTimeEntry(timeEntry,  gymCode);
       }

       public static OperationResult<List<TimeEntryDC>> GetEntityTimeEntries(DateTime startDate, DateTime endDate, int entityId, string entityRoleType, string gymCode)
       {
           return EmployeeManager.GetEntityTimeEntries(startDate, endDate, entityId, entityRoleType,  gymCode);
       }

       public static OperationResult<bool> DeleteEntityTimeEntry(int entityTimeEntryId, int taskId, decimal timeSpent, string gymCode)
       {
           return EmployeeManager.DeleteEntityTimeEntry(entityTimeEntryId,taskId, timeSpent,  gymCode);
       }

       public static OperationResult<List<CategoryDC>> GetTaskStatusCategories(int branchId,string gymCode)
       {
           return EmployeeManager.GetTaskStatusCategories(branchId,gymCode);
       }

       public static OperationResult<bool> SaveEmployeesRoles(EmployeeDC employeeDc, string gymCode)
       {
           return EmployeeManager.SaveEmployeesRoles(employeeDc,  gymCode);
       }

       public static OperationResult<List<RoleActivityDC>> GetActivitiesForRoles(int memberId, string gymCode)
       {
           return EmployeeManager.GetActivitiesForRoles(memberId,  gymCode);
       }

       public static OperationResult<bool> DeleteGymEmployee(int memberId, int assignEmpId, string user, string gymCode)
       {
           return EmployeeManager.DeleteGymEmployee(memberId,assignEmpId, user, gymCode);
       }

       public static OperationResult<List<EmployeeClass>> GetEmployeeClasses(int empId, string gymCode)
       {
           return EmployeeManager.GetEmployeeClasses(empId, gymCode);
       }


       public static OperationResult<int> UpdateApproveStatus(bool IsApproved, int EntityActiveTimeID, string user)
       {
           return EmployeeManager.UpdateApproveStatus(IsApproved, EntityActiveTimeID, user);
       }

       public static OperationResult<List<ExcelineRoleDc>> GetGymEmployeeRolesById(int employeeId, int branchId, string user)
       {
           return EmployeeManager.GetGymEmployeeRolesById(employeeId, branchId, user);
       }
    }
}
