﻿using System;
using US.GMS.Core.ResultNotifications;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Common;
using US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageJobs;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.Exceline.GMS.Modules.Admin.API.ManageJobs
{
    public class GMSJobs
    {
        public static OperationResult<int> SaveExcelineJob(ScheduleItemDC scheduleItem, int branchId, string gymCode)
        {
            return JobManager.SaveExcelineJob(scheduleItem, branchId, gymCode);
        }

        public static OperationResult<List<ScheduleItemDC>> GetJobScheduleItems(int branchId, string gymCode)
        {
            return JobManager.GetJobScheduleItems(branchId, gymCode);
        }

        public static OperationResult<int> DeleteScheduleItem(List<int> scheduleItemIdList, string gymCode)
        {
            return JobManager.DeleteScheduleItem(scheduleItemIdList, gymCode);
        }

        public static OperationResult<List<ExcelineCommonTaskDC>> GetAllTasksByEmployeeId(int branchId, int employeeId, string gymCode, string user)
        {
            return JobManager.GetAllTasksByEmployeeId(branchId, employeeId, gymCode,user);
        }

        public static OperationResult<List<ExcelineCommonTaskDC>> GetFollowUpByEmployeeId(int branchId, int employeeId, string gymCode, string user, int hit)
        {
            return JobManager.GetFollowUpByEmployeeId(branchId, employeeId, gymCode, user, hit);
        }


        public static OperationResult<List<ExcelineCommonTaskDC>> GetJobsByEmployeeId(int branchId, int employeeId, string gymCode, string user, int hit)
        {
            return JobManager.GetJobsByEmployeeId(branchId, employeeId, gymCode, user, hit);
        }

        public static OperationResult<int> AssignTaskToEmployee(ExcelineCommonTaskDC commonTask, int employeeId, string gymCode, string user)
        {
            return JobManager.AssignTaskToEmployee(commonTask, employeeId, gymCode, user);
        }

    }
}
