﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "3/29/2012 8:24:35 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageResources;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.ResultNotifications;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using System.Diagnostics;

namespace US.Exceline.GMS.Modules.Admin.API.ManageResources
{
    public class GMSResources
    {
        public static OperationResult<int> UpdateContractVisits(int selectedContractId, int punchedContractId, int bookingId, string gymCode, string user)
        {
            return ResourcesManager.UpdateContractVisits(selectedContractId, punchedContractId, bookingId, gymCode, user);
        }
        public static OperationResult<List<ResourceBookingElemnt>> GetResourceBooking(int scheduleId, string _startDateTime, string _endDateTime, string gymCode, string user)
        {
            return ResourcesManager.GetResourceBooking(scheduleId, _startDateTime, _endDateTime, gymCode, user);
        }

        public static OperationResult<List<ContractInfo>> GetPunchcardContractsOnMember(int memberId, string activity, string gymCode, string user)
        {
            return ResourcesManager.GetPunchcardContractsOnMember(memberId, activity, gymCode, user);
        }

        public static OperationResult<int> IsArticlePunchcard(int articleId, string gymCode, string user)
        {
            return ResourcesManager.IsArticlePunchcard(articleId, gymCode, user);
        }

        public static OperationResult<int> SaveResources(ResourceDC resource, string user, string imageFolderPath, string gymCode)
        {
            return ResourcesManager.SaveResources(resource, user, imageFolderPath,  gymCode);
        }

        public static OperationResult<bool> UpdateResource(ResourceDC resource, string user, string gymCode)
        {
                return ResourcesManager.UpdateResource( resource,  user,  gymCode);
        }

        public static OperationResult<bool> DeleteResource(int branchId, string user, string gymCode)
        {
                return ResourcesManager.DeleteResource( branchId,  user,  gymCode);
        }

        public static OperationResult<bool> DeActivateResource(int resourceId, int branchId, string user, string gymCode)
        {
                return ResourcesManager.DeActivateResource(resourceId, branchId, user,  gymCode);
        }

        public static OperationResult<bool> SwitchResource(int resourceTd1, int resourceId2, DateTime startDate, DateTime endDate, string comment,int branchId, string user, string gymCode)
        {
            return ResourcesManager.SwitchResource(resourceTd1, resourceId2, startDate, endDate,comment, branchId, user, gymCode);
        }
        public static OperationResult<bool> SwitchResourceUndo(int resId, DateTime startDate, DateTime endDate, string user, int branchId, string gymCode)
        {
            return ResourcesManager.SwitchResourceUndo(resId, startDate, endDate, user,branchId, gymCode);
        }

        public static OperationResult<bool> ValidateScheduleWithResourceId(int resourceId, DateTime startDate, DateTime endDate, int branchId, string user, string gymCode)
        {
            return ResourcesManager.ValidateScheduleWithResourceId(resourceId, startDate, endDate, branchId, user, gymCode);
        }

        public static OperationResult<bool> ValidateScheduleWithSwitchResource(int resourceId1, int resourceId2, DateTime startDate, DateTime endDate, int branchId, string user, string gymCode)
        {
            return ResourcesManager.ValidateScheduleWithSwitchResource(resourceId1, resourceId2, startDate, endDate, branchId, user, gymCode);
        }

        public static OperationResult<List<ResourceDC>>GetResources(int branchId, string searchText, int categoryId, int activityId, int equipmentid, bool isActive, string gymCode)
        {
            return ResourcesManager.GetResources(branchId, searchText, categoryId, activityId,equipmentid, isActive, gymCode);
        }
        
        public static OperationResult<int> ChangeResourceOnBooking(int scheduleItemId, int resourceId,  string comment, string gymCode, string user, int branchId)
        {
            return ResourcesManager.ChangeResourceOnBooking(scheduleItemId, resourceId, comment, gymCode, user, branchId);
        }

        public static OperationResult<List<ScheduleItemDC>> GetBookingsOnDeletedScheduleItem(int resourceId, string gymCode)
        {
            return ResourcesManager.GetBookingsOnDeletedScheduleItem(resourceId, gymCode);
        }


        public static OperationResult<ResourceDC> GetResourceDetailsById(int resourceId, string gymCode)
        {
            return ResourcesManager.GetResourceDetailsById(resourceId, gymCode);
        }

        public static OperationResult<List<ResourceDC>> GetResourceCalender(int branchId, string gymCode, int hit)
        {
            return ResourcesManager.GetResourceCalender(branchId, gymCode, hit);
        }

        public static OperationResult<List<ResourceActiveTimeDC>> GetResourceActiveTimes(int branchId, DateTime startTime, DateTime endTime, int entNO,string gymCode)
        {
            return ResourcesManager.GetResourceActiveTimes(branchId, startTime, endTime, entNO, gymCode);
        }

        public static OperationResult<List<AvailableResourcesDC>> GetAvailableResources(int branchId,int resId, string gymCode)
        {
            return ResourcesManager.GetAvailableResources(branchId,resId, gymCode);
        }

        public static OperationResult<List<AvailableResourcesDC>> GetResourceAvailableTimeById(int branchId, int resId, string gymCode)
        {
            return ResourcesManager.GetResourceAvailableTimeById(branchId, resId, gymCode);
        }

        public static OperationResult<int> SaveScheduleItem(ScheduleItemDC scheduleItem, int resId, int branchId,string gymCode)
        {
            return ResourcesManager.SaveScheduleItem(scheduleItem,resId,branchId, gymCode);
        }

        public static OperationResult<ScheduleDC> GetResourceScheduleItems(int resId, string roleTpye, string gymCode)
        {
            return ResourcesManager.GetResourceScheduleItems(resId, roleTpye, gymCode);
        }

        public static OperationResult<List<ScheduleDC>> GetResourceCalenderScheduleItems(List<int> resIdList, string roleTpye, string gymCode)
        {
            return ResourcesManager.GetResourceCalenderScheduleItems(resIdList, roleTpye, gymCode);
        }

        public static OperationResult<List<string>> ValidateResourcesWithSchedule(string roleType, List<AvailableResourcesDC> resourceList, int branchId, string gymCode)
        {
            return ResourcesManager.ValidateResourcesWithSchedule(roleType, resourceList, branchId, gymCode);
        }

        public static OperationResult<bool> AddOrUpdateAvailableResources(List<AvailableResourcesDC> resourceList, int branchId, string user)
        {
            return ResourcesManager.AddOrUpdateAvailableResources(resourceList, branchId, user);
        }

        public static OperationResult<List<EntityActiveTimeDC>> GetMemberBookingDetails(int branchId, int scheduleItemId, string gymCode)
        {
            return ResourcesManager.GetMemberBookingDetails(branchId, scheduleItemId, gymCode);
        }

        public static OperationResult<bool> ValidateScheduleWithPaidBooking(int scheduleItemId,int resourceId, string gymCode)
        {
            return ResourcesManager.ValidateScheduleWithPaidBooking(scheduleItemId, resourceId,gymCode);
        }

        public static OperationResult<int> DeleteResourcesScheduleItem(int scheduleItemId, string gymCode)
        {
            return ResourcesManager.DeleteResourcesScheduleItem(scheduleItemId, gymCode);
        }

        public static OperationResult<int> CheckDuplicateResource(int sourceId, int destinationId,DateTime startDate,DateTime endDate, string gymCode)
        {
            return ResourcesManager.CheckDuplicateResource(sourceId,destinationId,startDate,endDate, gymCode);
        }

        public static OperationResult<string> CheckActiveTimeOverlap(EntityActiveTimeDC activeTimeItem, string gymCode)
        {
            return ResourcesManager.CheckActiveTimeOverlap(activeTimeItem, gymCode);
        }

        public static OperationResult<int> SaveResourceActiveTimePunchcard(EntityActiveTimeDC activeTimeItem, int contractId, string gymCode, int branchId, string user)
        {
            Debug.WriteLine("GSMResource");
            return ResourcesManager.SaveResourceActiveTimePunchcard(activeTimeItem, contractId, gymCode, branchId, user);
        }

        public static OperationResult<int> SaveResourceActiveTimeRepeatablePunchcard(List<EntityActiveTimeDC> activeTimeItemList, int contractId, string gymCode, string user)
        {
            return ResourcesManager.SaveResourceActiveTimeRepeatablePunchcard(activeTimeItemList, contractId, gymCode, user);
        }

        public static OperationResult<int> SaveResourceActiveTimeRepeatable(List<EntityActiveTimeDC> activeTimeItemList, string gymCode, string user)
        {
            return ResourcesManager.SaveResourceActiveTimeRepeatable(activeTimeItemList, gymCode, user);
        }

        public static OperationResult<int> SaveResourceActiveTime(EntityActiveTimeDC activeTimeItem, string gymCode, int branchId, string user)
        {
            return ResourcesManager.SaveResourceActiveTime(activeTimeItem, gymCode, branchId, user);
        }

        public static OperationResult<int> DeleteResourceActiveTime(int activeTimeId,string articleName,DateTime visitDateTime, string roleType, bool isArrived, bool isPaid,List<int> memberList , string gymCode)
        {
            return ResourcesManager.DeleteResourceActiveTime(activeTimeId,articleName,visitDateTime, roleType, isArrived, isPaid,memberList, gymCode);
        }


        public static OperationResult<List<ArticleDC>> GetArticleForResouceBooking(int activityId,int branchId, string gymCode)
        {
            return ResourcesManager.GetArticleForResouceBooking(activityId,branchId,gymCode);
        }

        public static OperationResult<List<ArticleDC>> GetArticleForResouce(int activityId, int branchId, string gymCode)
        {
            return ResourcesManager.GetArticleForResouce(activityId, branchId, gymCode);
        }

        public static OperationResult<Dictionary<string, List<int>>> CheckUnusedCancelBooking(int activityId,int branchId, string accountNo, List<int> memberIdList, DateTime bookingDate, string gymCode)
        {
            return ResourcesManager.CheckUnusedCancelBooking(activityId, branchId,accountNo, memberIdList, bookingDate, gymCode);
        }

        public static OperationResult<int> UnavailableResourceBooking(int scheduleItemId, int activeTimeId, DateTime startDate, DateTime endDate, string gymCode, string user)
        {
            return ResourcesManager.UnavailableResourceBooking(scheduleItemId, activeTimeId, startDate, endDate, gymCode, user);
        }

        public static OperationResult<Dictionary<string, List<int>>> ValidatePunchcardContractMember(int activityId,int branchId, string accountNo, List<int> memberIdList, DateTime startDate, string gymCode)
        {
            return ResourcesManager.ValidatePunchcardContractMember(activityId, branchId,accountNo, memberIdList, startDate, gymCode);
        }

        public static OperationResult<int> SavePaymentBookingMember(int activetimeId,int articleId, int memberId, bool paid, int aritemNo, decimal amount, string paymentType,string gymCode)
        {
            return ResourcesManager.SavePaymentBookingMember(activetimeId, articleId,memberId, paid, aritemNo, amount,paymentType, gymCode);
        }

        public static OperationResult<List<int>> GetEmpolyeeForResources(List<int> resIdList, string gymCode)
        {
            return ResourcesManager.GetEmpolyeeForResources(resIdList, gymCode);
        }

        public static OperationResult<List<int>> ValidateResWithSwitchDataRange(List<int> resourceIds, DateTime startDate, DateTime endDate, string gymCode)
        {
            return ResourcesManager.ValidateResWithSwitchDataRange(resourceIds, startDate, endDate, gymCode);
        }

        public static string GetResourceBookingViewMode(string user, string gymCode)
        {
            try
            {
                return ResourcesManager.GetResourceBookingViewMode(user, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static OperationResult<bool> ValidateArticleByActivityId(int activityId, int articleId, string gymCode)
        {
            return ResourcesManager.ValidateArticleByActivityId(activityId, articleId, gymCode);
        }
    }
}
