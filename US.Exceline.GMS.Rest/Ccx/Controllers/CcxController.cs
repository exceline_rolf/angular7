﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using US.Exceline.GMS.Modules.Economy.BusinessLogic;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Ccx;


namespace US.Exceline.GMS.CCX.DueDateUpdateService.Controllers
{
    [RoutePrefix("api/Ccx")]
    public class CcxController : ApiController
    {
        [Authorize]
        [Route("update-invoiceduedate")]
        public async Task<IHttpActionResult> Register(List<Invoice> invoices)
        {
            var result = InvoiceManager.UpdateCcxInvoice(invoices);
            return Ok(result);
        }

        [Authorize]
        [Route("get-invoice")]
        public IHttpActionResult GetInvoice(int batchId)
        {
            return Ok(InvoiceManager.GetInvoice(batchId));
        }
    }
}
