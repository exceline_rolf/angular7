﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.Service
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : 05/08/2011 HH:MM  PM
// --------------------------------------------------------------------------

using System.Collections.Generic;
using System.Configuration;
using US.Common.Logging;
using US.Exceline.GMS.Modules.Economy.Service.InvoiceSearch;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.Utils;
//using US.Payment.API.Search;
//using US.Payment.Core.BusinessDomainObjects.InvoiceSearch;
//using US.Payment.Core.Enums;
//using US.Payment.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.ExcEconomy.Service
{
    public partial class EconomyService : IInvoiceSearchService
    {
        #region IInvoiceSearchService Members

        /// <summary>
        /// Advanced Invoice Search with Search Criteria
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns></returns>
        public string SearchInvoices(US.Exceline.GMS.Modules.Economy.Core.SystemObjects.InvoiceSearchCriteria searchCriteria, US.Exceline.GMS.Modules.Economy.Core.SystemObjects.SearchModes searchMode, object constValue, string dbCode)
        {
            OperationResult<string> result = US.Exceline.GMS.Modules.Economy.API.Invoice.AdvanaceInvoiceSearchXML(searchCriteria, searchMode, constValue, dbCode);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                  USLogger.Log(message.Message, new List<string> { "Payment", "Service" }, SeverityFilter.Error, null);
                }
                return string.Empty;
            }
            return result.OperationReturnValue;
        }
        #endregion

        /// <summary>
        /// General Invoice Search
        /// </summary>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        public string SearchGeneralInvoiceData(string searchValue, int invoiceType, string fieldType, US.Exceline.GMS.Modules.Economy.Core.SystemObjects.SearchModes searchMode, object constValue, string user, int paymentID)
        {
            OperationResult<string> result = US.Exceline.GMS.Modules.Economy.API.Invoice.SearchGeneralInvoiceDataXML(searchValue, invoiceType, fieldType, searchMode, constValue, ExceConnectionManager.GetGymCode(user), paymentID);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogger.Log(message.Message, new List<string> { "Payment", "Service" }, SeverityFilter.Error, null);
                }
                return string.Empty;
            }
            return result.OperationReturnValue;
        }

        public void ToPassDummyObject(US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPARItem usparItem, InvoicesSelectionModes invoicesSelectionModes)
        {

        }

        public string GetConfigSettings(string type)
        {
            switch (type)
            {
                case "RPT":
                    return ConfigurationManager.AppSettings["ReportViewerURL"].ToString();
                case "EXL":
                    return ConfigurationManager.AppSettings["ExorLiveURL"].ToString();
            }
            return string.Empty;
        }
    }
}
