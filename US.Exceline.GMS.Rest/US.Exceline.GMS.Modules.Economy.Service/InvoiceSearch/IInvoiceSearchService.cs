﻿using System.ServiceModel;
using US.GMS.Core.DomainObjects.Economy;
//using US.Payment.Core.BusinessDomainObjects.InvoiceSearch;
//using US.Payment.Core.Enums;

namespace US.Exceline.GMS.Modules.Economy.Service.InvoiceSearch
{
    [ServiceContract(Name = "InvoiceSearch", Namespace = "US.Exceline.GMS.Modules.ExcEconomy.Service")]
    interface IInvoiceSearchService
    {
        [OperationContract]
        string SearchInvoices(US.Exceline.GMS.Modules.Economy.Core.SystemObjects.InvoiceSearchCriteria searchCriteria, US.Exceline.GMS.Modules.Economy.Core.SystemObjects.SearchModes searchMode, object constValue, string dbCode);

        [OperationContract]
        string SearchGeneralInvoiceData(string searchValue, int invoiceType, string fieldType, US.Exceline.GMS.Modules.Economy.Core.SystemObjects.SearchModes searchMode, object constValue, string dbCode, int paymentID);

        [OperationContract]
        void ToPassDummyObject(US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPARItem usparItem, InvoicesSelectionModes invoicesSelectionModes);

        [OperationContract]
        string GetConfigSettings(string type);
    }
}
