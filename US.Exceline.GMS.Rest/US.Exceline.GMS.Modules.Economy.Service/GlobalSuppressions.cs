// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes", Scope = "member", Target = "US.Exceline.GMS.Modules.ExcEconomy.Service.EconomyService.#US.Exceline.GMS.Modules.Economy.Service.Apportionment.IApportionment.Test()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes", Scope = "member", Target = "US.Exceline.GMS.Modules.ExcEconomy.Service.EconomyService.#US.Exceline.GMS.Modules.Economy.Service.Apportionment.IApportionment.GetName()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2200:RethrowToPreserveStackDetails", Scope = "member", Target = "US.Exceline.GMS.Modules.ExcEconomy.Service.EconomyService.#MergePDFFile(System.Collections.Generic.List`1<System.String>,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2200:RethrowToPreserveStackDetails", Scope = "member", Target = "US.Exceline.GMS.Modules.ExcEconomy.Service.EconomyService.#UpdateBulkPDFPrintDetails(System.Int32,System.String,System.String,System.String,System.Int32,System.Int32,System.Int32,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2200:RethrowToPreserveStackDetails", Scope = "member", Target = "US.Exceline.GMS.Modules.ExcEconomy.Service.EconomyService.#UpdateHistoryPDFPrintProcess(System.Int32,System.Int32,System.Int32,System.Int32,System.String,System.String)")]
