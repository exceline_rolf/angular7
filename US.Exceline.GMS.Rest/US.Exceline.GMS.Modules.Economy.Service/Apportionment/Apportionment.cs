﻿using System;
using System.Collections.Generic;
using US.Common.Logging.API;
using US.Exceline.GMS.Modules.Economy.Service.Apportionment;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.Utils;
//using US.Payment.Core.Enums;
//using US.Payment.Core.ResultNotifications;
//using US.Payment.Modules.Economy.API.Economy;
//using US.Payment.Modules.Economy.Core.DomainObjects;

namespace US.Exceline.GMS.Modules.ExcEconomy.Service
{
    public partial class EconomyService : IApportionment
    {
        #region IApportionment Members

        bool IApportionment.Test()
        {
            return true;
        }

        string IApportionment.GetName()
        {
            return "Apportionment";
        }

        public US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums.ReverseConditions ReverseApportionments(int paymentId, int arItem, string actionStatus, string user, string dbCode)
        {
            OperationResult<US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums.ReverseConditions> result = US.Exceline.GMS.Modules.Economy.API.Apportionment.ReverseApportionments(paymentId, arItem, actionStatus, user, dbCode);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums.ReverseConditions.ERRROR;
            }
            return result.OperationReturnValue;
        }

        public List<US.Exceline.GMS.Modules.Economy.Core.SystemObjects.USPApportionmentTransaction> GetInitialApportionmentData(string KID, string creditorNo, decimal amount, int arNo, int caseNo, string user)
        {
            OperationResult<List<US.Exceline.GMS.Modules.Economy.Core.SystemObjects.USPApportionmentTransaction>> result = US.Exceline.GMS.Modules.Economy.API.Apportionment.GetInitialApportionmentData(KID, creditorNo, amount, arNo, caseNo, 0, "", ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                US.Exceline.GMS.Modules.Economy.Core.SystemObjects.USPApportionmentTransaction trans = new US.Exceline.GMS.Modules.Economy.Core.SystemObjects.USPApportionmentTransaction();
                trans.IsError = true;
                return new List<US.Exceline.GMS.Modules.Economy.Core.SystemObjects.USPApportionmentTransaction>() { trans };
            }
            return result.OperationReturnValue;
        }

        public int AddApportionmentData(List<US.Exceline.GMS.Modules.Economy.Core.SystemObjects.USPApportionmentTransaction> transactionList, string user)
        {
            OperationResult<int> result = US.Exceline.GMS.Modules.Economy.API.Apportionment.AddNotApportionmentData(-1, "", -1, "", 0, -1, transactionList, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            return result.OperationReturnValue;
        }

        #endregion
    }
}
