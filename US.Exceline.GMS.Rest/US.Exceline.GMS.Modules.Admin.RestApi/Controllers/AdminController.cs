﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using System.Net;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.SystemObjects;
using System;
using US.Common.Logging.API;
using US.GMS.Core.Utils;
using US.GMS.Core.ResultNotifications;
using US.Exceline.GMS.Modules.Admin.API.ManageSystemSettings;
using US.GMS.API;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.DomainObjects.Common;
using US.Exceline.GMS.Modules.Admin.RestApi.Models;
using US.Exceline.GMS.Modules.Admin.API.ManageAnonymizing;
using US.GMS.Core.DomainObjects.Admin.ManageClassSettings;
using US.Exceline.GMS.Modules.Admin.API.ManageContract;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.Exceline.GMS.Modules.Admin.API.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.Common.Web.UI.Core.Util;
using US.Common.USSAdmin.API;
using US.Payment.Core.ResultNotifications;
using System.Configuration;
using US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership;
using US.Exceline.GMS.Modules.Admin.RestApi.Model;
using US.GMS.Core.DomainObjects.Admin;
using US.Exceline.GMS.Modules.Admin.API.ManageResources;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.Communication.Core.SystemCore.DomainObjects;
using US.Communication.API;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Common.Notification.API;
using US_DataAccess;
using System.Diagnostics;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.Exceline.GMS.Modules.Operations.API.Operations;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Controllers
{
    [RoutePrefix("api/Admin/Settings")]
    #region System Setting Related Methods
    public class AdminController : ApiController
    {
        [HttpGet]
        [Route("GetGymCompanySettings")]
        [Authorize]
        public HttpResponseMessage GetGymCompanySettings(GymCompanySettingType gymCompanySettingType)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();

                if (gymCompanySettingType == GymCompanySettingType.INFO)
                {
                    var infoResult = GMSSystemSettings.GetGymCompanyInfoSettings(ExceConnectionManager.GetGymCode(user));
                    if (infoResult.ErrorOccured)
                    {
                        List<string> errorMsg = new List<string>();

                        foreach (NotificationMessage message in infoResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            errorMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(infoResult.OperationReturnValue, ApiResponseStatus.OK));

                }
                else if (gymCompanySettingType == GymCompanySettingType.VAT)
                {
                    var vatResult = GMSSystemSettings.GetGymCompanyVATSettings(ExceConnectionManager.GetGymCode(user));
                    if (vatResult.ErrorOccured)
                    {
                        List<string> errorMsg = new List<string>();

                        foreach (NotificationMessage message in vatResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            errorMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(vatResult.OperationReturnValue, ApiResponseStatus.OK));

                }

                else if (gymCompanySettingType == GymCompanySettingType.ECONOMY)
                {
                    var economyResult = GMSSystemSettings.GetGymCompanyEconomySettings(ExceConnectionManager.GetGymCode(user));
                    if (economyResult.ErrorOccured)
                    {
                        List<string> errorMsg = new List<string>();

                        foreach (NotificationMessage message in economyResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            errorMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(economyResult.OperationReturnValue, ApiResponseStatus.OK));

                }

                else if (gymCompanySettingType == GymCompanySettingType.OTHER)
                {
                    var otherResult = GMSSystemSettings.GetGymCompanyOtherSettings(ExceConnectionManager.GetGymCode(user));
                    if (otherResult.ErrorOccured)
                    {
                        List<string> errorMsg = new List<string>();

                        foreach (NotificationMessage message in otherResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            errorMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(otherResult.OperationReturnValue, ApiResponseStatus.OK));

                }
                else if (gymCompanySettingType == GymCompanySettingType.CONTRACT)
                {
                    var conditionResult = GMSSystemSettings.GetGymCompanyContractConditions(ExceConnectionManager.GetGymCode(user));
                    if (conditionResult.ErrorOccured)
                    {
                        List<string> errorMsg = new List<string>();

                        foreach (NotificationMessage message in conditionResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            errorMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(conditionResult.OperationReturnValue, ApiResponseStatus.OK));

                }
                else
                {
                    OperationResult<string> OperatioResult = new OperationResult<string>();
                    OperatioResult.OperationReturnValue = "Invalid  Setting  Type";
                    if (OperatioResult.ErrorOccured)
                    {
                        List<string> errorMsg = new List<string>();

                        foreach (NotificationMessage message in OperatioResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            errorMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(OperatioResult.OperationReturnValue, ApiResponseStatus.OK));

                }


            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveGymCompanyBasicSettings")]
        [Authorize]
        public HttpResponseMessage SaveGymCompanyBasicSettings(SystemSettingBasicInfoDC gymCompanyBasicSetting)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.SaveGymCompanyBasicSettings(gymCompanyBasicSetting, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveGymCompanyVATSettings")]
        [Authorize]
        public HttpResponseMessage SaveGymCompanyVATSettings(VatCodeDC vatCodeDetail)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.SaveGymCompanyVATSettings(vatCodeDetail, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveGymCompanyEconomySettings")]
        [Authorize]
        public HttpResponseMessage SaveGymCompanyEconomySettings(SystemSettingEconomySettingDC companyEconomySettings)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.SaveGymCompanyEconomySettings(companyEconomySettings, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveGymCompanyOtherSettings")]
        [Authorize]
        public HttpResponseMessage SaveGymCompanyOtherSettings(SystemSettingOtherSettingDC gymCompanyOtherSetting)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.SaveGymCompanyOtherSettings(gymCompanyOtherSetting, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveGymCompanyContractSettings")]
        [Authorize]
        public HttpResponseMessage SaveGymCompanyContractSettings(US.GMS.Core.DomainObjects.Admin.ManageGymSettings.ContractCondition gymCompanyContractSetting)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.SaveGymCompanyContractSettings(gymCompanyContractSetting, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }



        //[HttpPost]
        //[Route("SaveGymCompanySettings")]
        //[Authorize]
        //public HttpResponseMessage AddUpdateGymCompanySettings(string settingsDetails, GymCompanySettingType gymCompanySettingType)
        //{
        //    try
        //    {
        //        string user = Request.Headers.GetValues("UserName").First();
        //        var result = GMSSystemSettings.AddUpdateGymCompanySettings(settingsDetails, user, ExceConnectionManager.GetGymCode(user), gymCompanySettingType);
        //        if (result.ErrorOccured)
        //        {
        //            List<string> errorMsg = new List<string>();

        //            foreach (NotificationMessage message in result.Notifications)
        //            {
        //                USLogError.WriteToFile(message.Message, new Exception(), user);
        //                errorMsg.Add(message.Message);
        //            }
        //            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
        //        }

        //        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        //    }
        //    catch (Exception ex)
        //    {
        //        List<string> exceptionMsg = new List<string>();
        //        exceptionMsg.Add(ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
        //    }
        //}


        [HttpGet]
        [Route("GetAccessProfiles")]
        [Authorize]
        public HttpResponseMessage GetAccessProfiles()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystem.GetAccessProfiles(ExceConnectionManager.GetGymCode(user), Gender.ALL);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExceAccessProfileDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExceAccessProfileDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("AddCountryDetails")]
        [Authorize]
        public HttpResponseMessage AddCountryDetails(CountryDC country)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSMember.AddCountryDetails(country, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("AddRegionDetails")]
        [Authorize]
        public HttpResponseMessage AddRegionDetails(RegionDC region)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSMember.AddRegionDetails(region, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetRevenueAccounts")]
        [Authorize]
        public HttpResponseMessage GetRevenueAccounts()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.GetRevenueAccounts(ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<AccountDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<AccountDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("AddUpdateRevenueAccounts")]
        [Authorize]
        public HttpResponseMessage AddUpdateRevenueAccounts(AccountDC revenueAccountDetail)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<int> result = GMSSystemSettings.AddUpdateRevenueAccounts(revenueAccountDetail, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch(Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("DeleteRevenueAccounts")]
        [Authorize]
        public HttpResponseMessage DeleteRevenueAccounts(AccountDC revenueAccountDetail)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<int> result = GMSSystemSettings.DeleteRevenueAccounts(revenueAccountDetail, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("UpdateAccessProfiles")]
        [Authorize]
        public HttpResponseMessage UpdateAccessProfiles(ExceAccessProfileDC accessProfile)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystem.UpdateAccessProfiles(accessProfile, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        #endregion
        //--------------------------------------------End of system  setting  region------------------------------------------------------  
        #region Gym Setting  Related Methods
        [HttpGet]
        [Route("GetGymSettings")]
        [Authorize]
        public HttpResponseMessage GetGymSettings(int branchId, GymSettingType gymSettingType, string systemName)
        {

            try
            {
                string user = Request.Headers.GetValues("UserName").First();

                switch (gymSettingType)
                {
                    case GymSettingType.ACCESSTIME:
                        var resultAT = GMSManageGymSetting.GetGymAccessTimeSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultAT.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultAT.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultAT.OperationReturnValue, ApiResponseStatus.OK));
                        break;
                    case GymSettingType.GYMOPENTIME:
                        var resultG = GMSManageGymSetting.GetGymOpenTimes(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultG.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultG.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultG.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.REQUIRED:
                        var resultR = GMSManageGymSetting.GetGymRequiredSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultR.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultR.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultR.OperationReturnValue, ApiResponseStatus.OK));



                        break;
                    case GymSettingType.MEMBERSEARCH:
                        var resultM = GMSManageGymSetting.GetGymMemberSearchSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultM.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultM.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultM.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.HARDWARE:
                        var resultH = GMSManageGymSetting.GetHardwareProfiles(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultH.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultH.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultH.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.ECONOMY:
                        var resultE = GMSManageGymSetting.GetGymEconomySettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultE.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultE.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultE.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.SMS:
                        var resultS = GMSManageGymSetting.GetGymSmsSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultS.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultS.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultS.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.OTHER:
                        var resultO = GMSManageGymSetting.GetGymOtherSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultO.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultO.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultO.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.INTEGRATION:
                        var resultI = GMSManageGymSetting.GetGymIntegrationSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultI.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultI.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultI.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.EXCINTEGRATION:
                        var resultEI = GMSManageGymSetting.GetGymIntegrationExcSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultEI.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultEI.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultEI.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.OTHERINTEGRATION:
                        var resultOI = GMSManageGymSetting.GetOtherIntegrationSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultOI.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultOI.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultOI.OperationReturnValue, ApiResponseStatus.OK));

                        break;
     
                }



            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }
            return null;
        }

        [HttpGet]
        [Route("GetTerminalTypes")]
        [Authorize]
        public HttpResponseMessage GetTerminalTypes()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.GetTerminalTypes(ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("UpdateStock")]
        [Authorize]
        public HttpResponseMessage UpdateStock(ArticleDC article)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.SaveStock(article, article.BranchId, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetCategories")]
        [Authorize]
        public HttpResponseMessage GetCategories(string type)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSCategory.GetCategoriesByType(type, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("GetArticles")]
        [Authorize]
        public HttpResponseMessage GetArticles(ArticleSearch articleSearch)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSArticle.GetArticles(articleSearch.BranchId, user, articleSearch.CategpryType, articleSearch.Keyword, articleSearch.Category, articleSearch.ActivityId, articleSearch.IsObsalate, articleSearch.IsActive, ExceConnectionManager.GetGymCode(user), articleSearch.FilterByGym);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("SaveArticle")]
        [Authorize]
        public HttpResponseMessage SaveArticle(ArticleDC articleDc)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSArticle.SaveArticle(articleDc, user, articleDc.BranchId, articleDc.CategoryId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("ImportArticleList")]
        [Authorize]
        public HttpResponseMessage ImportArticleList(string articleList, int branchID)
        {
            try
            {
                List<ArticleDC> articles = XMLUtilities.DesrializeXMLToObject(articleList, typeof(List<ArticleDC>)) as List<ArticleDC>;
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<bool> result = GMSArticle.ImportArticleList(articles, user, branchID, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("DeleteIntegrationSetting")]
        [Authorize]
        public HttpResponseMessage DeleteIntegrationSetting(int id)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.DeleteIntegrationSettingById(id, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("DeleteIntegrationSettingById")]
        [Authorize]
        public HttpResponseMessage DeleteIntegrationSettingById(int id)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.DeleteIntegrationSettingById(id, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }



        [HttpPost]
        [Route("SaveGymSettings")]
        [Authorize]
        public HttpResponseMessage SaveGymSettings(int branchId, string accessTimeDetails, GymSettingType gymSettingType)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.SaveGymSettings(branchId, accessTimeDetails, ExceConnectionManager.GetGymCode(user), gymSettingType);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveGymAccessSettings")]
        [Authorize]
        public HttpResponseMessage SaveGymAccessSettings(GymAccessSettingDC settingsDetails)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.SaveGymAccessSettings(settingsDetails.BranchId, settingsDetails, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }



        [HttpPost]
        [Route("SaveGymOpenTimes")]
        [Authorize]
        public HttpResponseMessage SaveGymOpenTimes(GymSettingsLists settingsDetails)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.SaveGymOpenTimes(settingsDetails.BranchId, settingsDetails.GymOpenTimeList, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveGymRequiredSettings")]
        [Authorize]
        public HttpResponseMessage SaveGymRequiredSettings(GymRequiredSettingsDC settingsDetails)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.SaveGymRequiredSettings(settingsDetails.BranchId, settingsDetails, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveGymMemberSearchSettings")]
        [Authorize]
        public HttpResponseMessage SaveGymMemberSearchSettings(GymMemberSearchSettingsDC settingsDetails)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.SaveGymMemberSearchSettings(settingsDetails.BranchId, settingsDetails, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveHardwareProfile")]
        [Authorize]
        public HttpResponseMessage SaveHardwareProfile(GymSettingsLists settingsDetails)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.SaveHardwareProfile(settingsDetails.BranchId, settingsDetails.HardwareProfileList, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveGymEconomySettings")]
        [Authorize]
        public HttpResponseMessage SaveGymEconomySettings(GymEconomySettingsDC settingsDetails)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.SaveGymEconomySettings(settingsDetails.BranchId, settingsDetails, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveGymSmsSettings")]
        [Authorize]
        public HttpResponseMessage SaveGymSmsSettings(GymSmsSettingsDC settingsDetails)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.SaveGymSmsSettings(settingsDetails.BranchId, settingsDetails, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveGymOtherSettings")]
        [Authorize]
        public HttpResponseMessage SaveGymOtherSettings(GymOtherSettingsDC settingsDetails)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.SaveGymOtherSettings(settingsDetails.BranchId, settingsDetails, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveGymIntegrationSettings")]
        [Authorize]
        public HttpResponseMessage SaveGymIntegrationSettings(GymIntegrationSettingsDC settingsDetails)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.SaveGymIntegrationSettings(settingsDetails.BranchId, settingsDetails, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveGymIntegrationExcSettings")]
        [Authorize]
        public HttpResponseMessage SaveGymIntegrationExcSettings(GymSettingsLists settingsDetails)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.SaveGymIntegrationExcSettings(settingsDetails.BranchId, settingsDetails.GymIntegrationSettings, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("AddUpdateOtherIntegrationSettings")]
        [Authorize]
        public HttpResponseMessage AddUpdateOtherIntegrationSettings(GymSettingsLists settingsDetails)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.AddUpdateOtherIntegrationSettings(settingsDetails.BranchId, settingsDetails.OtherIntegrationSettings, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetSelectedGymSettings")]
        [Authorize]
        public HttpResponseMessage GetSelectedGymSettings(List<string> gymSetColNames, bool isGymSettings, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.GetSelectedGymSettings(gymSetColNames, isGymSettings, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new Dictionary<string, object>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new Dictionary<string, object>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("GetSelectedGymSettings")]
        [Authorize]
        public HttpResponseMessage GetSelectedGymSettings(SelectedGymSettings settings)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.GetSelectedGymSettings(settings.GymSetColNames, settings.IsGymSettings, settings.BranchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetArxMembers")]
        [Authorize]
        public HttpResponseMessage GetArxMembers(bool isallmember, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSOperations.GetARXMembers(branchId, ExceConnectionManager.GetGymCode(user), isallmember);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetArxSettingDetail")]
        [Authorize]
        public HttpResponseMessage GetArxSettingDetail(int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.GetArxSettingDetail(branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetArxFormatType")]
        [Authorize]
        public HttpResponseMessage GetArxFormatType()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.GetArxFormatType(ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("SaveArxSettingDetail")]
        [Authorize]
        public HttpResponseMessage SaveArxSettingDetail(GymSettingsLists settingsDetails)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.SaveArxSettingDetail(settingsDetails.arxFormatDetail, settingsDetails.arxFormatDetail.TempCodeDeleteDate, settingsDetails.BranchId, user, ExceConnectionManager.GetGymCode(user), settingsDetails.accessContolTypes);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        #endregion
        //------------------------------------------------------------End  of  Gym  setting  Region-----------------------------------------------------------------




        [HttpGet]
        [Route("GetAnonymizingData")]
        [Authorize]
        public HttpResponseMessage GetAnonymizingData()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSAnonymizing.GetAnonymizingData(ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("DeleteAnonymizingData")]
        [Authorize]
        public HttpResponseMessage DeleteAnonymizingData(Annonymizing annonymizing)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSAnonymizing.DeleteAnonymizingData(annonymizing.Anonymizing, user, annonymizing.BranchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetClassTypes")]
        [Authorize]
        public HttpResponseMessage GetClassTypes()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = ManageClassTypeSetting.GetClassTypes(ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetClassTypesByBranch")]
        [Authorize]
        public HttpResponseMessage GetClassTypesByBranch(int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = ManageClassTypeSetting.GetClassTypesByBranch(branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExceClassTypeDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExceClassTypeDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("AddEditClassType")]
        [Authorize]
        public HttpResponseMessage AddEditClassType(ExceClassTypeDC classType)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = ManageClassTypeSetting.AddEditClassType(classType, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("DeleteClassType")]
        [Authorize]
        public HttpResponseMessage DeleteClassType(int classtypeId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = ManageClassTypeSetting.DeleteClassType(classtypeId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }



        [HttpPost]
        [Route("AddUpdateGymCompanySettings")]
        [Authorize]
        public HttpResponseMessage AddUpdateGymCompanySettings(Models.ContractCondition contractCondition)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.AddUpdateGymCompanySettings(contractCondition.settingsDetails, user, ExceConnectionManager.GetGymCode(user), contractCondition.gymCompanySettingType);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("GetContracts")]
        [Authorize]
        public HttpResponseMessage GetContracts(GetContracts getContracts)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSContract.GetContracts(getContracts.branchId, getContracts.searchText, getContracts.searchType, getContracts.searchCriteria, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<PackageDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<PackageDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetArticlesForCategory")]
        [Authorize]
        public HttpResponseMessage GetArticlesForCategory(int categoryID,int branchID)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = US.Exceline.GMS.API.GMSContract.GetArticlesForCommonUse(categoryID, ExceConnectionManager.GetGymCode(user), branchID);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ArticleDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ArticleDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetArticlesForCategory")]
        [Authorize]
        public HttpResponseMessage GetContractItems(int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSContract.GetContractItems(branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ContractItemDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ContractItemDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("SaveContract")]
        [Authorize]
        public HttpResponseMessage SaveContract(PackageDC package)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSContract.SaveContract(package, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else {

                    var _memberContractNotification = new NotificationDC();
                    _memberContractNotification.Source = "dbo.ExceContractTemplate";
                    _memberContractNotification.SourceID = result.OperationReturnValue;
                    _memberContractNotification.ReceiverEntityID = package.PackageId;
                    _memberContractNotification.Role = MemberRole.MEM;
                    var notificationMethodList = new List<NotificationMethodDC>();
                    var notificationMethod = new NotificationMethodDC();
                    notificationMethod.DueDate = DateTime.Now.AddDays(30);
                    notificationMethod.Method = NotifyMethodType.SMS;
                    notificationMethodList.Add(notificationMethod);
                    _memberContractNotification.MethodList = notificationMethodList;

                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }



        [HttpPost]
        [Route("UpdateContractPriority")]
        [Authorize]
        public HttpResponseMessage UpdateContractPriority(Dictionary<int, int> packages)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSContract.UpdateContractPriority(packages, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }




        [HttpGet]
        [Route("GetGymEmployees")]
        [Authorize]
        public HttpResponseMessage GetGymEmployees(int branchId, string searchText,bool isActive, int roleId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.GetGymEmployees(branchId, searchText, isActive, roleId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<GymEmployeeDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<GymEmployeeDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }




        [HttpGet]
        [Route("GetGymEmployeeByEmployeeId")]
        [Authorize]
        public HttpResponseMessage GetGymEmployeeByEmployeeId(int branchId,int employeeId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.GetGymEmployeeByEmployeeId(branchId, user, employeeId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new GymEmployeeDC(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new GymEmployeeDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }



        [HttpGet]
        [Route("GetGymEmployeeRolesById")]
        [Authorize]
        public HttpResponseMessage GetGymEmployeeRolesById(int employeeId, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSEmployee.GetGymEmployeeRolesById(employeeId, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExcelineRoleDc>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineRoleDc>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("SaveEmployeesRoles")]
        [Authorize]
        public HttpResponseMessage SaveEmployeesRoles(EmployeeDC employeeDc)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSEmployee.SaveEmployeesRoles(employeeDc, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }



        [HttpGet]
        [Route("GetExcelineEmpRoles")]
        [Authorize]
        public HttpResponseMessage GetExcelineEmpRoles()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = AdminUserSettings.GetUSPRoles("", true, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExcelineRoleDc>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue.Select(uspRole => new ExcelineRoleDc
                {
                    Id = uspRole.Id,
                    RoleName = uspRole.RoleName
                }).ToList(), ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineRoleDc>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }



        [HttpPost]
        [Route("SaveGymEmployee")]
        [Authorize]
        public HttpResponseMessage SaveGymEmployee(GymEmployeeDC employee)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.SaveGymEmployee(employee, GetConfigurations(user), ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


 

        private string GetConfigurations(string user)
        {
            string imageSavepath = string.Empty;
            try
            {
                imageSavepath = ConfigurationSettings.AppSettings["ImageFolderPath"].ToString();
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Error in getting Image folder path", ex, user);
            }
            return imageSavepath;
        }



        [HttpPost]
        [Route("ValidateEmployeeFollowUp")]
        [Authorize]
        public HttpResponseMessage ValidateEmployeeFollowUp(ValidateEmployeeFollowUp validateEmployeeFollowUp)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.ValidateEmployeeFollowUp(validateEmployeeFollowUp.employeeId,validateEmployeeFollowUp.endDate, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }



        [HttpPost]
        [Route("DeleteGymEmployee")]
        [Authorize]
        public HttpResponseMessage DeleteGymEmployee(DeleteGymEmployee deleteGymEmployee)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSEmployee.DeleteGymEmployee(deleteGymEmployee.memberId, deleteGymEmployee.assignEmpId, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetEmployeeEvents")]
        [Authorize]
        public HttpResponseMessage GetEmployeeEvents(int employeeId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.GetEmployeeEvents(employeeId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<EmployeeEventDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<EmployeeEventDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetJobCategories")]
        [Authorize]
        public HttpResponseMessage GetJobCategories(int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageTask.GetJobCategories(branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExcelineJobCategoryDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineJobCategoryDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }
        [HttpGet]
        [Route("GetEmployeeJobs")]
        [Authorize]
        public HttpResponseMessage GetEmployeeJobs(int employeeId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.GetEmployeeJobs(employeeId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<EmployeeJobDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<EmployeeJobDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetEmployeeClasses")]
        [Authorize]
        public HttpResponseMessage GetEmployeeClasses(int empId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSEmployee.GetEmployeeClasses(empId, user.Split('/')[0].Trim());
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<EmployeeClass>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<EmployeeClass>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("UpdateApproveStatus")]
        [Authorize]
        public HttpResponseMessage UpdateApproveStatus(UpdateApprovalStatus updateApprovalStatus)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSEmployee.UpdateApproveStatus(updateApprovalStatus.IsApproved, updateApprovalStatus.EntityActiveTimeID, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetFollowUpDetailByEmpId")]
        [Authorize]
        public HttpResponseMessage GetFollowUpDetailByEmpId(int empId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetFollowUpDetailByEmpId(empId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<FollowUpDetailDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<FollowUpDetailDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetEmployeeTimeEntries")]
        [Authorize]
        public HttpResponseMessage GetEmployeeTimeEntries(int employeeId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.GetEmployeeTimeEntries(employeeId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<EmployeeTimeEntryDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<EmployeeTimeEntryDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("SaveEntityTimeEntry")]
        [Authorize]
        public HttpResponseMessage SaveEntityTimeEntry(TimeEntryDC timeEntry)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSEmployee.SaveEntittTimeEntry(timeEntry, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetContractSummariesByEmployee")]
        [Authorize]
        public HttpResponseMessage GetContractSummariesByEmployee(int employeeID, int branchId, DateTime createdDate)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetContractSummariesByEmployee(employeeID, branchId, createdDate, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ContractSummaryDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ContractSummaryDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }



        [HttpPost]
        [Route("AddEmployeeTimeEntry")]
        [Authorize]
        public HttpResponseMessage AddEmployeeTimeEntry(EmployeeTimeEntryDC timeEntry)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.AddEmployeeTimeEntry(timeEntry, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetMembers")]
        [Authorize]
        public HttpResponseMessage GetMembers(int branchId, string searchText, int activeState, MemberSearchType searchType, MemberRole memberRole, int hit, string sortName, bool isAscending)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetMembers(branchId, searchText, activeState, searchType, memberRole, user, ExceConnectionManager.GetGymCode(user), hit, !string.IsNullOrEmpty(sortName), isAscending, sortName);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("DeleteArticle")]
        [Authorize]
        public HttpResponseMessage DeleteArticle(DeleteArticle deleteArticle)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.DeleteArticle(deleteArticle.ArticleId, deleteArticle.BranchId, ExceConnectionManager.GetGymCode(user), user, deleteArticle.IsAdminUser);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }




        [HttpPost]
        [Route("SaveInventory")]
        [Authorize]
        public HttpResponseMessage SaveInventory(InventoryItemDC inventoryItem)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.SaveInventory(inventoryItem, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveInventoryDetail")]
        [Authorize]
        public HttpResponseMessage SaveInventoryDetail(SaveInventory inventory)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.SaveInventoryDetail(inventory.ArticleList, inventory.InventoryId, ExceConnectionManager.GetGymCode(user)); ;
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("DeleteInventory")]
        [Authorize]
        public HttpResponseMessage DeleteInventory(int inventoryId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.DeleteInventory( inventoryId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }



        [HttpGet]
        [Route("GetInventory")]
        [Authorize]
        public HttpResponseMessage GetInventory(int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.GetInventory(branchId, ExceConnectionManager.GetGymCode(user)); 
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<InventoryItemDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InventoryItemDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetInventoryDetail")]
        [Authorize]
        public HttpResponseMessage GetInventoryDetail(int branchId, int inventoryId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.GetInventoryDetail(branchId, inventoryId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ArticleDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ArticleDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetNetValueForArticle")]
        [Authorize]
        public HttpResponseMessage GetNetValueForArticle(int inventoryId, int articleId, int counted, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.GetNetValueForArticle(inventoryId, articleId, counted, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new Dictionary<decimal, decimal>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new Dictionary<decimal, decimal>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetResourceBookingViewMode")]
        [Authorize]
        public HttpResponseMessage GetResourceBookingViewMode()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.GetResourceBookingViewMode(user, ExceConnectionManager.GetGymCode(user));
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new Dictionary<decimal, decimal>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetResourceCalender")]
        [Authorize]
        public HttpResponseMessage GetResourceCalender(int branchId, int hit)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.GetResourceCalender(branchId, ExceConnectionManager.GetGymCode(user), hit);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ResourceDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ResourceDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("GetResourceCalenderScheduleItems")]
        [Authorize]
        public HttpResponseMessage GetResourceCalenderScheduleItems(Schedule schedule)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.GetResourceCalenderScheduleItems(schedule.ResIdList, schedule.RoleTpye, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ScheduleDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ScheduleDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("PrintInventoryList")]
        [Authorize]
        public HttpResponseMessage PrintInventoryList(int branchId, int inventoryId)
        {
            PDFPrintResultDC printResult = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var appUrl = ConfigurationManager.AppSettings["USC_InventoryList_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "InventoryId", inventoryId.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) }, { "BranchId", branchId.ToString() } };
                printResult.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
                }
                printResult.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(printResult, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("GetEntityActiveTimes")]
        [Authorize]
        public HttpResponseMessage GetEntityActiveTimes(EntityActiveTime entityActiveTime)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSchedule.GetEntityActiveTimes(entityActiveTime.Branchid, entityActiveTime.StartDate, entityActiveTime.EndDate, entityActiveTime.EntityList, entityActiveTime.EntityRoleType, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<EntityActiveTimeDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<EntityActiveTimeDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }
        private string GetFilePathForPdf(string application, string OutputPlugging, string user, Dictionary<string, string> dataDictionary)
        {
            var filePath = string.Empty;
           
            try
            {
                string pdfViewerURL = ConfigurationManager.AppSettings["PDFViewerURL"];
                var list = ExcecuteWithUSC(application, OutputPlugging, dataDictionary, ExceConnectionManager.GetGymCode(user), user);
                foreach (var item in list)
                {
                    if (item.Messages != null)
                    {
                        var Message = item.Messages.Where(mssge => mssge.Header.ToLower().Equals("filepath")).First();
                        if (Message != null)
                        {
                            filePath = Message.Message;
                            //manipulate to get the PDF path
                            int indexOFRoot = filePath.IndexOf("PDF");
                            string restOfPath = filePath.Substring(indexOFRoot + 4, filePath.Length - (indexOFRoot + 4));
                            string[] pathItems = restOfPath.Split(new char[] { '\\' });
                            string path = string.Empty;

                            if (pathItems.Length > 0)
                            {
                                pathItems.ToList<string>().ForEach(X => path += "/" + X);
                            }
                            filePath = pdfViewerURL + path;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
            }
            return filePath;
        }

        private List<IProcessingStatus> ExcecuteWithUSC(string application, string OutputPlugging, Dictionary<string, string> dataDictionary, string gymCode, string user)
        {
            List<IProcessingStatus> lists = null;
            try
            {
                lists = TemplateManager.ExecuteTemplate(dataDictionary, application, OutputPlugging, gymCode);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
                throw ex;
            }
            return lists;
        }
       

        //[HttpPost]
        //[Route("SaveInventoryDetail")]
        //[Authorize]
        //public HttpResponseMessage SaveInventoryDetail(SaveInventory inventory)
        //{
        //    try
        //    {
        //        string user = Request.Headers.GetValues("UserName").First();
        //        var result = GMSSystemSettings.SaveInventoryDetail(inventory.ArticleList, inventory.InventoryId, ExceConnectionManager.GetGymCode(user)); ;
        //        if (result.ErrorOccured)
        //        {
        //            List<string> errorMsg = new List<string>();

        //            foreach (NotificationMessage message in result.Notifications)
        //            {
        //                USLogError.WriteToFile(message.Message, new Exception(), user);
        //                errorMsg.Add(message.Message);
        //            }
        //            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
        //        }

        //        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        //    }
        //    catch (Exception ex)
        //    {
        //        List<string> exceptionMsg = new List<string>();
        //        exceptionMsg.Add(ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
        //    }
        //}





        //public string GetMembers(int branchId, string user, string searchText, int activeState, MemberSearchType searchType, MemberRole memberRole, int hit, string sortName, bool isAscending)
        //{
        //    //if (searchType == MemberSearchType.ALLMEMANDCOM)
        //    //{
        //    //    activeState = 10; //all mem and com
        //    //    OperationResult<string> result = GMSMember.GetAllMemberAndCompany(branchId, searchText, activeState, hit, ExceConnectionManager.GetGymCode(user));
        //    //    if (result.ErrorOccured)
        //    //    {
        //    //        foreach (NotificationMessage message in result.Notifications)
        //    //        {
        //    //            USLogError.WriteToFile(message.Message, new Exception(), user);
        //    //        }
        //    //        return XMLUtils.SerializeDataContractObjectToXML(new List<ExcelineMemberDC>());
        //    //    }
        //    //    else
        //    //    {
        //    //        return result.OperationReturnValue;
        //    //    }
        //    //}
        //    //else
        //    //{
        //    OperationResult<string> result = GMSManageMembership.GetMembers(branchId, searchText, activeState, searchType, memberRole, user, ExceConnectionManager.GetGymCode(user), hit, !string.IsNullOrEmpty(sortName), isAscending, sortName);
        //    if (result.ErrorOccured)
        //    {
        //        foreach (NotificationMessage message in result.Notifications)
        //        {
        //            USLogError.WriteToFile(message.Message, new Exception(), user);
        //        }
        //        return XMLUtils.SerializeDataContractObjectToXML(new List<ExcelineMemberDC>());
        //    }
        //    else
        //    {
        //        return result.OperationReturnValue;
        //    }
        //    //}

        //}



        [HttpGet]
        [Route("GetEmployeeBookings")]
        [Authorize]
        public HttpResponseMessage GetEmployeeBookings(int employeeId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.GetEmployeeBookings(employeeId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<EmployeeBookingDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<EmployeeBookingDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }



        [HttpGet]
        [Route("GetGymEmployeeWorkPlan")]
        [Authorize]
        public HttpResponseMessage GetGymEmployeeWorkPlan(int employeeId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.GetGymEmployeeWorkPlan(employeeId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<GymEmployeeWorkPlanDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<GymEmployeeWorkPlanDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetGymEmployeeApprovals")]
        [Authorize]
        public HttpResponseMessage GetGymEmployeeApprovals(int employeeId, string approvalType)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.GetGymEmployeeApprovals(employeeId, approvalType, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<GymEmployeeApprovalDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<GymEmployeeApprovalDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }



      

        [HttpGet]
        [Route("GetResources")]
        [Authorize]
        public HttpResponseMessage GetResources(int branchId, string searchText, int categoryId, int activityId, int equipmentid, bool isActive)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.GetResources(branchId, searchText, categoryId, activityId, equipmentid, isActive, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ResourceDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ResourceDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetResourceScheduleItems")]
        [Authorize]
        public HttpResponseMessage GetResourceScheduleItems(int resId, string roleTpye)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.GetResourceScheduleItems(resId, roleTpye, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new ScheduleDC(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new ScheduleDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("UpdateContractVisits")]
        [Authorize]
        public HttpResponseMessage UpdateContractVisits(string selectedContractId, string punchedContractId, string bookingId)
        {
            // Update contract vsisits after punchard booking where choice is different than default choice

            int _selectedContractId = Int32.Parse(selectedContractId);
            int _punchedContractId = Int32.Parse(punchedContractId);
            int _bookingId = Int32.Parse(bookingId);
            try
            {
                Debug.WriteLine(_selectedContractId);
                Debug.WriteLine(_punchedContractId);
                Debug.WriteLine(_bookingId);
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.UpdateContractVisits(_selectedContractId, _punchedContractId, _bookingId, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetResourceBooking")]
        [Authorize]
        public HttpResponseMessage GetResourceBooking(int scheduleId,  string startDateTime, string endDateTime)
        {
            Debug.WriteLine(startDateTime);
            Debug.WriteLine(endDateTime);
            string _startDateTime = startDateTime + ".000";
            string _endDateTime = endDateTime + ".000";
            Debug.WriteLine(_startDateTime);
            Debug.WriteLine(_endDateTime);
            // scheduleId --> parentId in ExceMemberBoking
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.GetResourceBooking(scheduleId, _startDateTime, _endDateTime, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new ScheduleDC(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new ScheduleDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("SaveWorkItem")]
        [Authorize]
        public HttpResponseMessage SaveWorkItem(SaveWorkItemDC saveWorkItemDC)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.SaveWorkItem(saveWorkItemDC.work, saveWorkItemDC.branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("AdminApproveEmployeeWork")]
        [Authorize]
        public HttpResponseMessage AdminApproveEmployeeWork(GymEmployeeWorkDC work)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.AdminApproveEmployeeWork(work, work.IsApproved, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }



        [HttpPost]
        [Route("ApproveEmployeeTimes")]
        [Authorize]
        public HttpResponseMessage ApproveEmployeeTimes(EmployeeTimes time)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.ApproveAllEmployeeTimes(time.employeeId, time.isAllApproved, time.timeIdString, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetPunchcardContractsOnMember")]
        [Authorize]
        public HttpResponseMessage GetPunchcardContractsOnMember(int memberId, string activity)
        {

            Debug.WriteLine(memberId);
            Debug.WriteLine(activity);
            try
            {
                string user = Request.Headers.GetValues("UserName").First();

                var result = GMSResources.GetPunchcardContractsOnMember(memberId, activity, ExceConnectionManager.GetGymCode(user), user);

                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("IsArticlePunchcard")]
        [Authorize]
        public HttpResponseMessage IsArticlePunchcard(int articleId)
        {

            try { 
                string user = Request.Headers.GetValues("UserName").First();

                var result = GMSResources.IsArticlePunchcard(articleId, ExceConnectionManager.GetGymCode(user), user);

                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
        exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("SaveResourceActiveTimePunchcard")]
        [Authorize]
        public HttpResponseMessage SaveResourceActiveTimePunchcard(EntityActiveTimeDC activeTimeItem, string contractId)
        {
            Debug.WriteLine(activeTimeItem);
            Debug.WriteLine("ContractID: " + contractId);
            int _contractID = Int32.Parse(contractId);
            Debug.WriteLine(_contractID);
            try
            {
                Debug.WriteLine("try");
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.SaveResourceActiveTimePunchcard(activeTimeItem, _contractID, ExceConnectionManager.GetGymCode(user), activeTimeItem.BranchId, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveResourceActiveTimeRepeatablePunchcard")]
        [Authorize]
        public HttpResponseMessage SaveResourceActiveTimeRepeatablePunchcard(int contractId, List<EntityActiveTimeDC> activeTimeItemList)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.SaveResourceActiveTimeRepeatablePunchcard(activeTimeItemList, contractId, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("SaveResourceActiveTimeRepeatable")]
        [Authorize]
        public HttpResponseMessage SaveResourceActiveTimeRepeatable(List<EntityActiveTimeDC> activeTimeItemList)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.SaveResourceActiveTimeRepeatable(activeTimeItemList, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("SaveResourceActiveTime")]
        [Authorize]
        public HttpResponseMessage SaveResourceActiveTime(EntityActiveTimeDC activeTimeItem)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.SaveResourceActiveTime(activeTimeItem, ExceConnectionManager.GetGymCode(user), activeTimeItem.BranchId, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("GetArticleForResouceBooking")]
        [Authorize]
        public HttpResponseMessage GetArticleForResouceBooking(ArticleSearch articleSearch)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.GetArticleForResouceBooking(articleSearch.ActivityId, articleSearch.BranchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ArticleDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ArticleDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("ValidatePunchcardContractMember")]
        [Authorize]
        public HttpResponseMessage ValidatePunchcardContractMember(BookingValidation bookingValidation)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.ValidatePunchcardContractMember(bookingValidation.ActivityId, bookingValidation.BranchId, bookingValidation.AccountNo, bookingValidation.MemberIdLst, bookingValidation.StartDate, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ArticleDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ArticleDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("CheckUnusedCancelBooking")]
        [Authorize]
        public HttpResponseMessage CheckUnusedCancelBooking(BookingValidation bookingValidation)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.CheckUnusedCancelBooking(bookingValidation.ActivityId, bookingValidation.BranchId, bookingValidation.AccountNo, bookingValidation.MemberIdLst, bookingValidation.StartDate, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ArticleDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ArticleDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("DeleteResourceActiveTime")]
        [Authorize]
        public HttpResponseMessage DeleteResourceActiveTime(EntityActiveTime entityActiveTime)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.DeleteResourceActiveTime(entityActiveTime.ActiveTimeId, entityActiveTime.ArticleName, entityActiveTime.StartDate, entityActiveTime.RoleType, entityActiveTime.IsArrived, entityActiveTime.IsPaid, entityActiveTime.EntityList, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("SendSmsForBooking")]
        [Authorize]
        public HttpResponseMessage SendSmsForBooking(SendSms sendSms)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                commonNotification.IsTemplateNotification = false;
                commonNotification.Description = sendSms.Message;
                commonNotification.BranchID = sendSms.BranchId;
                commonNotification.CreatedUser = user;
                commonNotification.Role = "MEM";
                commonNotification.NotificationType = NotificationTypeEnum.Messages;
                commonNotification.Severity = NotificationSeverityEnum.Minor;
                commonNotification.MemberIdList = sendSms.MemberIdList;
                commonNotification.Status = NotificationStatusEnum.New;
                commonNotification.Method = NotificationMethodType.SMS;
                commonNotification.Title = "BOOKING TEXT SMS";
                commonNotification.SenderDescription = sendSms.MobileNo;

                var result = NotificationAPI.AddNotification(commonNotification);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(true, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("SavePaymentBookingMember")]
        [Authorize]
        public HttpResponseMessage SavePaymentBookingMember(BookingPaymnet bookingPayment)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.SavePaymentBookingMember(bookingPayment.ActivetimeId, bookingPayment.ArticleId, bookingPayment.MemberId, bookingPayment.Paid, bookingPayment.AritemNo, bookingPayment.Amount, bookingPayment.PaymentType, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }
     
        [HttpPost]
        [Route("AddFreeDefineNotification")]
        [Authorize]
        public HttpResponseMessage AddFreeDefineNotification(SendSms sendSms)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                paraList.Add(TemplateFieldEnum.MEMBERNAME, sendSms.MemberName);
                paraList.Add(TemplateFieldEnum.RESOURCENAME, sendSms.ResName);
                paraList.Add(TemplateFieldEnum.FROMTIME, sendSms.StartTime.ToShortTimeString());
                paraList.Add(TemplateFieldEnum.TOTIME, sendSms.EndTime.ToShortTimeString());
                paraList.Add(TemplateFieldEnum.DATE, sendSms.BookingDate.ToShortDateString());

                USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                commonNotification.IsTemplateNotification = true;
                commonNotification.ParameterString = paraList;
                commonNotification.BranchID = sendSms.BranchId;
                commonNotification.CreatedUser = user;
                commonNotification.Role = "MEM";
                commonNotification.NotificationType = NotificationTypeEnum.Messages;
                commonNotification.Severity = NotificationSeverityEnum.Minor;
                commonNotification.MemberIdList = sendSms.MemberIdList;
                commonNotification.Status = NotificationStatusEnum.New;
                // if wants to send sms
                commonNotification.Type = TextTemplateEnum.BOOKINGREMINDER;
                commonNotification.Method = NotificationMethodType.SMS;
                commonNotification.Title = "BOOKING SMS";

                var result = NotificationAPI.AddNotification(commonNotification);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(1, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }
        public int DeleteResourceActiveTime(int activeTimeId, string articleName, DateTime visitDateTime, string roleType, bool isArrived, bool isPaid, List<int> memberList, string user)
        {
            var result = GMSResources.DeleteResourceActiveTime(activeTimeId, articleName, visitDateTime, roleType, isArrived, isPaid, memberList, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        //[HttpPost]
        //[Route("DeleteInventory")]
        //[Authorize]
        //public HttpResponseMessage DeleteInventory(int inventoryId)
        //{
        //    try
        //    {
        //        string user = Request.Headers.GetValues("UserName").First();
        //        var result = GMSSystemSettings.DeleteInventory(inventoryId, ExceConnectionManager.GetGymCode(user));
        //        if (result.ErrorOccured)
        //        {
        //            List<string> errorMsg = new List<string>();

        //            foreach (NotificationMessage message in result.Notifications)
        //            {
        //                USLogError.WriteToFile(message.Message, new Exception(), user);
        //                errorMsg.Add(message.Message);
        //            }
        //            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
        //        }

        //        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        //    }
        //    catch (Exception ex)
        //    {
        //        List<string> exceptionMsg = new List<string>();
        //        exceptionMsg.Add(ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(1, ApiResponseStatus.ERROR, exceptionMsg));
        //    }
        //}

        //public int DeleteInventory(int inventoryId, string user)
        //{
        //    var result = GMSSystemSettings.DeleteInventory(inventoryId, ExceConnectionManager.GetGymCode(user));
        //    if (result.ErrorOccured)
        //    {
        //        foreach (NotificationMessage message in result.Notifications)
        //        {
        //            USLogError.WriteToFile(message.Message, new Exception(), user);
        //        }
        //        return -1;
        //    }
        //    else
        //    {
        //        return result.OperationReturnValue;
        //    }
        //}


        [HttpPost]
        [Route("ExcelineManualSponsoringGeneration")]
        [Authorize]
        public HttpResponseMessage ExcelineManualSponsoringGeneration(SponsorShipGenerationDetailsDC SponsorData)
        {
            string user = Request.Headers.GetValues("UserName").First();
            var result = API.ManageMembers.GMSMembers.GenerateSponsorOrders(SponsorData, ExceConnectionManager.GetGymCode(user), user);
        if (result.ErrorOccured)
        {
            foreach (NotificationMessage message in result.Notifications)
            {
                USLogError.WriteToFile(message.Message, new Exception(), user);
            }
            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR)); ;
        }
        else
        {
             return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }

        }
}
}
