﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging.API;
using US.Common.Notification.Core.DomainObjects;
using US.Exceline.GMS.Modules.Admin.API.ManageJobs;
using US.Exceline.GMS.Modules.Admin.RestApi.Model;
using US.Exceline.GMS.Modules.Admin.RestApi.Models;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Controllers
{
    [RoutePrefix("api/Admin/GMSJobs")]
    public class GMSJobsController : ApiController
    {

        [HttpGet]
        [Route("GetFollowUpByEmployeeId")]
        [Authorize]
        public HttpResponseMessage GetFollowUpByEmployeeId(int branchId, int employeeId, int hit)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSJobs.GetFollowUpByEmployeeId( branchId, employeeId, ExceConnectionManager.GetGymCode(user),  user, hit);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExcelineCommonTaskDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineCommonTaskDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetJobsByEmployeeId")]
        [Authorize]
        public HttpResponseMessage GetJobsByEmployeeId(int branchId, int employeeId, int hit)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSJobs.GetJobsByEmployeeId(branchId, employeeId, ExceConnectionManager.GetGymCode(user), user, hit);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExcelineCommonTaskDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineCommonTaskDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("AssignTaskToEmployee")]
        [Authorize]
        public HttpResponseMessage AssignTaskToEmployee(ExcelineCommonTaskDC commonTask)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSJobs.AssignTaskToEmployee(commonTask, commonTask.EntityId, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExcelineCommonTaskDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineCommonTaskDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("DeleteJobCategory")]
        [Authorize]
        public HttpResponseMessage DeleteJobCategory(int jobCategoryId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageTask.DeleteJobCategory(jobCategoryId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("SaveJobCategory")]
        [Authorize]
        public HttpResponseMessage SaveJobCategory(JobCategoryObj jobCategoryObj)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageTask.SaveExcelineJobCategory(jobCategoryObj.jobCategory, jobCategoryObj.isEdit, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("AddEventToNotifications")]
        [Authorize]
        public HttpResponseMessage AddEventToNotifications(USCommonNotificationDC Notification)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageTask.AddEventToNotifications(Notification, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetJobScheduleItems")]
        [Authorize]
        public HttpResponseMessage GetJobScheduleItems(int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSJobs.GetJobScheduleItems(branchId, ExceConnectionManager.GetGymCode(user));
                if(result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach(NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ScheduleItemDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch(Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ScheduleItemDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("DeleteJobScheduleItem")]
        [Authorize]
        public HttpResponseMessage DeleteJobScheduleItem(GetScheduleItemIdList list)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSJobs.DeleteScheduleItem(list.ScheduleItemIdList, ExceConnectionManager.GetGymCode(user));
                if(result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach(NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch(Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveExcelineJob")]
        [Authorize]
        public HttpResponseMessage SaveExcelineJob(JobScheduleItem jobScheduleItem)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSJobs.SaveExcelineJob(jobScheduleItem.scheduleItem, jobScheduleItem.branchId, ExceConnectionManager.GetGymCode(user));
                if(result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach(NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch(Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


    }
}
