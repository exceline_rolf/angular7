﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Common;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Models
{
    public class SaveWorkItemDC
    {
    
        public GymEmployeeWorkDC work { get; set; }
        public int branchId { get; set; }

    }
} 