﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.Common;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Models
{
    public class FollowUpTemplate
    {
        public FollowUpTemplateDC template { get; set; }
        public int branchId { get; set; }
    }
}