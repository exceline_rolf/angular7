﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Model
{
    public class DeleteArticle
    {
        public int ArticleId { get; set; }
        public int BranchId { get; set; }
        public bool IsAdminUser { get; set; }
    }
}