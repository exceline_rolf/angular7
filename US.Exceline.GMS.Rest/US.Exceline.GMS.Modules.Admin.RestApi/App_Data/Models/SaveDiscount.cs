﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Models
{
    public class SaveDiscount
    {
        public List<DiscountDC> discountList { get; set; }
        public int branchId { get; set; }
        public int contractSettingID { get; set; }

    }
} 