﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Model
{
    public class UnAvailableTime
    {
        public int ScheduleItemId { get; set; }
        public int ActiveTimeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}