﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Model
{
    public class BookingValidation
    {
        public int ActivityId { get; set; }
        public DateTime StartDate { get; set; }
        public string AccountNo { get; set; }
        public int BranchId { get; set; }
        public List<int> MemberIdLst { get; set; }
    }
}