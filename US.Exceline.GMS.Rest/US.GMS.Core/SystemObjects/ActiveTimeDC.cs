﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public abstract class ActiveTimeDC
    {
        [DataMember]
        public abstract int Id { get; set; }
        [DataMember]
        public abstract int SheduleItemId { get; set; }
        [DataMember]
        public abstract DateTime StartDateTime { get; set; }
        [DataMember]
        public abstract DateTime EndDateTime { get; set; }
        [DataMember]
        public abstract int EntityId { get; set; }
        [DataMember]
        public abstract string EntityRoleType  { get; set; }
        [DataMember]
        public abstract int EntNo { get; set; }
        [DataMember]
        public abstract string Name { get; set; }
        [DataMember]
        public abstract int NumberOfMembers { get; set; }
        [DataMember]
        public abstract string InstructorName { get; set; }
        
    }
}
