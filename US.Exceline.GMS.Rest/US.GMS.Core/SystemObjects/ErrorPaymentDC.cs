﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class ErrorPaymentDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }


        private string _creditorNo = string.Empty;
        [DataMember]
        public string CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }

        private string _gymName = string.Empty;
        [DataMember]
        public string GymName
        {
            get { return _gymName; }
            set { _gymName = value; }
        }

        private string _customerNo = string.Empty;
        [DataMember]
        public string CustomerNo
        {
            get { return _customerNo; }
            set { _customerNo = value; }
        }

        private decimal _amount = 0;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private string _kid = string.Empty;
        [DataMember]
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }

        private bool _addtoInvoice = false;
        [DataMember]
        public bool AddtoInvoice
        {
            get { return _addtoInvoice; }
            set { _addtoInvoice = value; }
        }

        private bool _addToAccount = false;
        [DataMember]
        public bool AddToAccount
        {
            get { return _addToAccount; }
            set { _addToAccount = value; }
        }

    }
}
