﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public class ShopCreditNoteDetail
    {
        private string _pid = string.Empty;
        public string PID
        {
            get { return _pid; }
            set { _pid = value; }
        }

        private string _cid = string.Empty;
        public string CID
        {
            get { return _cid; }
            set { _cid = value; }
        }

        private string _kid = string.Empty;
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }

        private string _ref = string.Empty;
        public string Ref
        {
            get { return _ref; }
            set { _ref = value; }
        }

        private int _caseNo = -1;
        public int CaseNo
        {
            get { return _caseNo; }
            set { _caseNo = value; }
        }

        private int _orderlineID = -1;
        public int OrderlineID
        {
            get { return _orderlineID; }
            set { _orderlineID = value; }
        }

        private int _sumQuanltity = 0;
        public int SumQuanltity
        {
            get { return _sumQuanltity; }
            set { _sumQuanltity = value; }
        }

        private int _saleId = -1;
        public int SaleId
        {
            get { return _saleId; }
            set { _saleId = value; }
        }
    }
}
