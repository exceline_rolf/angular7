﻿using System;
using System.Runtime.Serialization;
namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class SalesPerAccountData
    {
        private String _accountNo = String.Empty;
        [DataMember]
        public String AccountNo
        {
            get { return _accountNo; }
            set { _accountNo = value; }
        }

        private String _accountName = String.Empty;
        [DataMember]
        public String AccountName
        {
            get { return _accountName; }
            set { _accountName = value; }
        }

        private String _itemsSold = String.Empty;
        [DataMember]
        public String ItemsSold
        {
            get { return _itemsSold; }
            set { _itemsSold = value; }
        }

        private String _totalAmount = String.Empty;
        [DataMember]
        public String TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }

        private Char _debitOrKredit = 'X';
        [DataMember]
        public Char DebitOrKredit
        {
            get { return _debitOrKredit; }
            set { _debitOrKredit = value; }
        }

        private String _transaction = String.Empty;
        [DataMember]
        public String Transaction
        {
            get { return _transaction; }
            set { _transaction = value; }
        }

    }
}