﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class ContractSummaryDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _memberID = -1;
        [DataMember]
        public int MemberID
        {
            get { return _memberID; }
            set { _memberID = value; }
        }

        private string _contractNo = string.Empty;
        [DataMember]
        public string ContractNo
        {
            get { return _contractNo; }
            set { _contractNo = value; }
        }

        private bool _isATG = false;
         [DataMember]
        public bool IsATG
        {
            get { return _isATG; }
            set { _isATG = value; }
        }

         private string _templateName = string.Empty;
         [DataMember]
         public string TemplateName
         {
             get { return _templateName; }
             set { _templateName = value; }
         }

         private int _activityId = -1;
         [DataMember]
         public int ActivityId
         {
             get { return _activityId; }
             set { _activityId = value; }
         }

         private string _activityName = string.Empty;
         [DataMember]
         public string ActivityName
         {
             get { return _activityName; }
             set { _activityName = value; }
         }

         private DateTime? _signedDate;
        [DataMember]
         public DateTime? SignedDate
         {
             get { return _signedDate; }
             set { _signedDate = value; }
         }

        private DateTime? _startDate;
        [DataMember]
        public DateTime? StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime? _endDate;
         [DataMember]
        public DateTime? EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }


         private DateTime? _closedate;
         [DataMember]
         public DateTime? Closedate
         {
             get { return _closedate; }
             set { _closedate = value; }
         }

         private DateTime? _renewedDate;
         [DataMember]
         public DateTime? RenewedDate
         {
             get { return _renewedDate; }
             set { _renewedDate = value; }
         }

         private int _noOfOrders = 1;
         [DataMember]
         public int NoOfOrders
         {
             get { return _noOfOrders; }
             set { _noOfOrders = value; }
         }

         private decimal _startUpItemsPrice = 0;
         [DataMember]
         public decimal StartUpItemsPrice
         {
             get { return _startUpItemsPrice; }
             set { _startUpItemsPrice = value; }
         }

         private decimal _monthlyitemPrice = 0;
         [DataMember]
         public decimal MonthlyitemPrice
         {
             get { return _monthlyitemPrice; }
             set { _monthlyitemPrice = value; }
         }

         private decimal _totalPrice = 0;
         [DataMember]
         public decimal TotalPrice
         {
             get { return _totalPrice; }
             set { _totalPrice = value; }
         }

         private string _status = string.Empty;
         [DataMember]
         public string Status
         {
             get { return _status; }
             set { _status = value; }
         }

         private int _accessProfileId = -1;
         [DataMember]
         public int AccessProfileId
         {
             get { return _accessProfileId; }
             set { _accessProfileId = value; }
         }

         private List<ContractAccessTimeDC> _accesTimes = new List<ContractAccessTimeDC>();
         [DataMember]
         public List<ContractAccessTimeDC> AccesTimes
         {
             get { return _accesTimes; }
             set { _accesTimes = value; }
         }

         private int _branchId = -1;
         [DataMember]
         public int BranchId
         {
             get { return _branchId; }
             set { _branchId = value; }
         }

         private int _remainingVisits = 0;
         [DataMember]
         public int RemainingVisits
         {
             get { return _remainingVisits; }
             set { _remainingVisits = value; }
         }

         private string _contractTypeCode = string.Empty;
         [DataMember]
         public string ContractTypeCode
         {
             get { return _contractTypeCode; }
             set { _contractTypeCode = value; }
         }

         private bool _isGroupContract = false;
         [DataMember]
         public bool IsGroupContract
         {
             get { return _isGroupContract; }
             set { _isGroupContract = value; }
         }

         private bool _isFreezed = false;
         [DataMember]
         public bool IsFreezed
         {
             get { return _isFreezed; }
             set { _isFreezed = value; }
         }

         private decimal _serviceAmount = 0;
         [DataMember]
         public decimal ServiceAmount
         {
             get { return _serviceAmount; }
             set { _serviceAmount = value; }
         }

         private string _gymName = string.Empty;
         [DataMember]
         public string GymName
         {
             get { return _gymName; }
             set { _gymName = value; }
         }

         private bool _isGroup;
          [DataMember]
        public bool IsGroup
        {
            get { return _isGroup; }
            set { _isGroup = value; }
        }
        
        private List<ContractFreezeItemDC> _contractFreezList = new List<ContractFreezeItemDC>();
        [DataMember]
        public List<ContractFreezeItemDC> ContractFreezList
        {
            get { return _contractFreezList; }
            set { _contractFreezList = value; }
        }

        private int _employeeID = -1;
        [DataMember]
        public int EmployeeId
        {
            get { return _employeeID; }
            set { _employeeID = value; }
        }

    }
}
