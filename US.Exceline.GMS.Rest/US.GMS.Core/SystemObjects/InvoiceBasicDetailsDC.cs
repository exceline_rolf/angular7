﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class InvoiceBasicDetailsDC
    {
        private decimal _activityPrice = 0;
        [DataMember]
        public decimal ActivityPrice
        {
            get { return _activityPrice; }
            set { _activityPrice = value; }
        }

        private decimal _adonPrice = 0;
        [DataMember]
        public decimal AdonPrice
        {
            get { return _adonPrice; }
            set { _adonPrice = value; }
        }

        private decimal _totalPrice = 0;
        [DataMember]
        public decimal TotalPrice
        {
            get { return _totalPrice; }
            set { _totalPrice = value; }
        }

        private DateTime? _dueDate;
        [DataMember]
        public DateTime? DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        private string _kid = string.Empty;
        [DataMember]
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }

        private decimal _discountAmount = 0;
        [DataMember]
        public decimal DiscountAmount
        {
            get { return _discountAmount; }
            set { _discountAmount = value; }
        }

        private decimal _sponsoredAmount = 0;
        [DataMember]
        public decimal SponsoredAmount
        {
            get { return _sponsoredAmount; }
            set { _sponsoredAmount = value; }
        }

        private decimal _invoiceAmount = 0;
        [DataMember]
        public decimal InvoiceAmount
        {
            get { return _invoiceAmount; }
            set { _invoiceAmount = value; }
        }

        private decimal _partiallyPaidAmount = 0;
        [DataMember]
        public decimal PartiallyPaidAmount
        {
            get { return _partiallyPaidAmount; }
            set { _partiallyPaidAmount = value; }
        }

        private List<ArticleSummaryDC> _articleList = new List<ArticleSummaryDC>();
        [DataMember]
        public List<ArticleSummaryDC> ArticleList
        {
            get { return _articleList; }
            set { _articleList = value; }
        }

        private List<SponsorshipItemDC> _sponsorItemList = new List<SponsorshipItemDC>();
        [DataMember]
        public List<SponsorshipItemDC> SponsorItemList
        {
            get { return _sponsorItemList; }
            set { _sponsorItemList = value; }
        }

        
    }
}
