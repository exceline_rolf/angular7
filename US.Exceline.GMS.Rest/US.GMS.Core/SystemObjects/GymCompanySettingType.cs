﻿using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    public enum GymCompanySettingType
    {
        [EnumMember]
        VAT,
        [EnumMember]
        OTHER,
        [EnumMember]
        INFO,
        [EnumMember]
        ECONOMY,
        [EnumMember]
        ACCESS,
        [EnumMember]
        CONTRACT
    }
}
