﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{

    [DataContract]
    public class MonthlyServicesForContract
    {

        private String _ItemName = String.Empty;
        [DataMember]
        public String ItemName
        {
            get { return _ItemName; }
            set { _ItemName = value; }
        }

        private double _Price = 0.00;
        [DataMember]
        public double Price
        {
            get { return _Price; }
            set { _Price = value; }
        }

        private int _Units = -1;
        [DataMember]
        public int Units
        {
            get { return _Units; }
            set { _Units = value; }
        }

        private int _StartOrder = -1;
        [DataMember]
        public int StartOrder
        {
            get { return _StartOrder; }
            set { _StartOrder = value; }
        }

        private int _EndOrder = -1;
        [DataMember]
        public int EndOrder
        {
            get { return _EndOrder; }
            set { _EndOrder = value; }
        }

    }
}
