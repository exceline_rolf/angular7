﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public enum MemberSearchType
    {
        [EnumMember]
        SEARCH,
        [EnumMember]
        CLASS,
        [EnumMember]
        SWITCHPAYER,
        [EnumMember]
        INTRODUCEDBY,
        [EnumMember]
        INTRODUCER,
        [EnumMember]
        FAMILYMEMBER,
        [EnumMember]
        GUARDIAN,
        [EnumMember]
        SPONSORS,
        [EnumMember]
        CONTACTPERSON,
        [EnumMember]
        GROUP,
        [EnumMember]
        LINKCUSTOMER,
        [EnumMember]
        ALLMEMANDCOM,
        [EnumMember]
        LINKEMPLOYEE,
        [EnumMember]
        SHOP,


    }
}
