﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{

        [DataContract]
        public class MemberContractPrintInfo
        {

            private String _GymOrganizationNo = String.Empty;
            [DataMember]
            public String GymOrganizationNo
            {
                get { return _GymOrganizationNo; }
                set { _GymOrganizationNo = value; }
            }

            private String _GymAddress1 = String.Empty;
            [DataMember]
            public String GymAddress1
            {
                get { return _GymAddress1; }
                set { _GymAddress1 = value; }
            }

            private String _GymPostalAddress = String.Empty;
            [DataMember]
            public String GymPostalAddress
            {
                get { return _GymPostalAddress; }
                set { _GymPostalAddress = value; }
            }

            private String _GymName = String.Empty;
            [DataMember]
            public String GymName
            {
                get { return _GymName; }
                set { _GymName = value; }
            }

            private String _JuridicalName = String.Empty;
            [DataMember]
            public String JuridicalName
            {
                get { return _JuridicalName; }
                set { _JuridicalName = value; }
            }

            private String _GymAccountNo = String.Empty;
            [DataMember]
            public String GymAccountNo
            {
                get { return _GymAccountNo; }
                set { _GymAccountNo = value; }
            }

            private int _GymCompanyId = -1;
            [DataMember]
            public int GymCompanyId
            {
                get { return _GymCompanyId; }
                set { _GymCompanyId = value; }
            }

            private String _MemberContractNo = String.Empty;
            [DataMember]
            public String MemberContractNo
            {
                get { return _MemberContractNo; }
                set { _MemberContractNo = value; }
            }

            private String _SignedDate = String.Empty;
            [DataMember]
            public String SignedDate
            {
                get { return _SignedDate; }
                set { _SignedDate = value; }
            }

            private String _Template = String.Empty;
            [DataMember]
            public String Template
            {
                get { return _Template; }
                set { _Template = value; }
            }

            private String _StartLockPeriod = String.Empty;
            [DataMember]
            public String StartLockPeriod
            {
                get { return _StartLockPeriod; }
                set { _StartLockPeriod = value; }
            }

            private String _StopLockPeriod = String.Empty;
            [DataMember]
            public String StopLockPeriod
            {
                get { return _StopLockPeriod; }
                set { _StopLockPeriod = value; }
            }

            private double _StrtUpItemPrice = 0.00;
            [DataMember]
            public double StrtUpItemPrice
            {
                get { return _StrtUpItemPrice; }
                set { _StrtUpItemPrice = value; }
            }

            private String _MonthlyServicePrice = String.Empty;
            [DataMember]
            public String MonthlyServicePrice
            {
                get { return _MonthlyServicePrice; }
                set { _MonthlyServicePrice = value; }
            }

            private double _MonthlyAddonsPrice = 0.00;
            [DataMember]
            public double MonthlyAddonsPrice
            {
                get { return _MonthlyAddonsPrice; }
                set { _MonthlyAddonsPrice = value; }
            }

            private double _TotPriceLockPeriod = 0.00;
            [DataMember]
            public double TotPriceLockPeriod
            {
                get { return _TotPriceLockPeriod; }
                set { _TotPriceLockPeriod = value; }
            }

            private String _ContractCondition = "<html >\n<head>\n\t<title></title>\n</head>\n<body></body>\n</html>\n";
            [DataMember]
            public String ContractCondition
            {
                get { return _ContractCondition; }
                set { _ContractCondition = value; }
            }

            private String _FirstDeductionDate = String.Empty;
            [DataMember]
            public String FirstDeductionDate
            {
                get { return _FirstDeductionDate; }
                set { _FirstDeductionDate = value; }
            }

            private double _MaxmumATGAmount = 0.00;
            [DataMember]
            public double MaxmumATGAmount
            {
                get { return _MaxmumATGAmount; }
                set { _MaxmumATGAmount = value; }
            }

            private String _MemberNo = String.Empty;
            [DataMember]
            public String MemberNo
            {
                get { return _MemberNo; }
                set { _MemberNo = value; }
            }

            private String _FirstName = String.Empty;
            [DataMember]
            public String FirstName
            {
                get { return _FirstName; }
                set { _FirstName = value; }
            }

            private String _LastName = String.Empty;
            [DataMember]
            public String LastName
            {
                get { return _LastName; }
                set { _LastName = value; }
            }

            private String _Address1 = String.Empty;
            [DataMember]
            public String Address1
            {
                get { return _Address1; }
                set { _Address1 = value; }
            }

            private String _Address2 = String.Empty;
            [DataMember]
            public String Address2
            {
                get { return _Address2; }
                set { _Address2 = value; }
            }

            private String _PostalAddress = String.Empty;
            [DataMember]
            public String PostalAddress
            {
                get { return _PostalAddress; }
                set { _PostalAddress = value; }
            }

            private String _TelMobile = String.Empty;
            [DataMember]
            public String TelMobile
            {
                get { return _TelMobile; }
                set { _TelMobile = value; }
            }

            private String _TelPrivate = String.Empty;
            [DataMember]
            public String TelPrivate
            {
                get { return _TelPrivate; }
                set { _TelPrivate = value; }
            }

            private String _TelWork = String.Empty;
            [DataMember]
            public String TelWork
            {
                get { return _TelWork; }
                set { _TelWork = value; }
            }

            private String _Birthdate = String.Empty;
            [DataMember]
            public String Birthdate
            {
                get { return _Birthdate; }
                set { _Birthdate = value; }
            }

            private String _BankAccountNo = String.Empty;
            [DataMember]
            public String BankAccountNo
            {
                get { return _BankAccountNo; }
                set { _BankAccountNo = value; }
            }

            private String _ID = String.Empty;
            [DataMember]
            public String ID
            {
                get { return _ID; }
                set { _ID = value; }
            }

            private String _GymCode = String.Empty;
            [DataMember]
            public String GymCode
            {
                get { return _GymCode; }
                set { _GymCode = value; }
            }

            private int _ParentId = -1;
            [DataMember]
            public int ParentId
            {
               get { return _ParentId; }
               set { _ParentId = value; }
            }

        private String _BranchID = String.Empty;
            [DataMember]
            public String BranchID
            {
                get { return _BranchID; }
                set { _BranchID = value; }
            }

            private String _DocType = String.Empty;
            [DataMember]
            public String DocType
            {
                get { return _DocType; }
                set { _DocType = value; }
            }

            private String _Today = String.Empty;
            [DataMember]
            public String Today
            {
                get { return _Today; }
                set { _Today = value; }
            }

            private int _ContractItemsCount = -1;
            [DataMember]
            public int ContractItemsCount
            {
                get { return _ContractItemsCount; }
                set { _ContractItemsCount = value; }
            }
        }
}
