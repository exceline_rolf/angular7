﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public enum TransactionTypes
    {
        NONE,
        NEXTORDER,
        ONACCOUNT,
        ONACCOUNTDEBIT,
        ONACCOUNTCREDIT,
        BANKTERMINAL,
        CASH,
        PETTYCASH,
        CASHWITHDRAWAL,
        EMAILSMSINVOICE,
        GIFTCARD,
        LOCALOCR,
        OCR,
        RETURN,
        DEBTCOLLECTION,
        BANKSTATEMENT,
        INVOICE,
        PAYMENTRETURN,
        SHOPORDER,
        PREPAIDBALANCE,
        BANK,
        VIPPS
    }
}
