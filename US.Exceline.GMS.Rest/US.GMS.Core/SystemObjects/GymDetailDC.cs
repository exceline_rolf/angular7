﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class GymDetailDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _gymCode = string.Empty;
        [DataMember]
        public string GymCode
        {
            get { return _gymCode; }
            set { _gymCode = value; }
        }

        private string _appVersion = string.Empty;
        [DataMember]
        public string AppVersion
        {
            get { return _appVersion; }
            set { _appVersion = value; }
        }

        private string _dbConnection = string.Empty;
        [DataMember]
        public string DbConnection
        {
            get { return _dbConnection; }
            set { _dbConnection = value; }
        }

        private string _createdBy = string.Empty;
        [DataMember]
        public string CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        private string _modifiedBy = string.Empty;
        [DataMember]
        public string ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }

        private int _licenseId = -1;
        [DataMember]
        public int LicenseId
        {
            get { return _licenseId; }
            set { _licenseId = value; }
        }

        private bool _activeStatus = false;
        [DataMember]
        public bool ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private string _gymName = string.Empty;
        [DataMember]
        public string GymName
        {
            get { return _gymName; }
            set { _gymName = value; }
        }

        private List<BranchDetailsDC> _branched = new List<BranchDetailsDC>();
        [DataMember]
        public List<BranchDetailsDC> Branched
        {
            get { return _branched; }
            set { _branched = value; }
        }
    }
}
