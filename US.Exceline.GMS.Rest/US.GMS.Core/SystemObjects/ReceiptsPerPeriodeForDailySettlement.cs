﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class ReceiptsPerPeriodeForDailySettlement
    {
        private int _numberOfReceipts = -1;
        [DataMember]
        public int NumberOfReceipts
        {
            get { return _numberOfReceipts; }
            set { _numberOfReceipts = value; }
        }

        private double _receiptAmount = -1;
        [DataMember]
        public double ReceiptAmount
        {
            get { return _receiptAmount; }
            set { _receiptAmount = value; }
        }

        private int _numberOfCopys = -1;
        [DataMember]
        public int NumberOfCopys
        {
            get { return _numberOfCopys; }
            set { _numberOfCopys = value; }
        }

        private double _copyAmount = -1;
        [DataMember]
        public double CopyAmount
        {
            get { return _copyAmount; }
            set { _copyAmount = value; }
        }

        private int _numberOfReturns = -1;
        [DataMember]
        public int NumberOfReturns
        {
            get { return _numberOfReturns; }
            set { _numberOfReturns = value; }
        }

        private double _returnAmount = -1;
        [DataMember]
        public double ReturnAmount
        {
            get { return _returnAmount; }
            set { _returnAmount = value; }
        }
    }
}
