﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public enum CategoryTypes
    {
        NONE,
        CLASS,
        EMPLOYEE,
        MEMBER,
        TRAINER,
        INSTRUCTOR,
        CONTRACT,
        RESOURCE,
        PACKAGE,
        CONTRACTITEM,
        ACTIVITY,
        PAYMODE,
        DISCOUNT,
        ARTICLE,
        NOTIFYMETHOD,
        NOTIFICATIONTEMPLATE,
        FREEZE,
        DISCOUNTMODE,
        ADONTEXT,
        HARDWARE,
        SIGNUP,
        SEARCHMEMBERSETTING,
        CLASSGROUP,
        CLASSLEVEL,
        CLASSCATEGORY,
        BOOKINGCATEGORY
    }
}
