﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class InvoiceOrderDetailsDC
    {
        private DateTime? _trainingStartDate;
        [DataMember]
        public DateTime? TrainingStartDate
        {
            get { return _trainingStartDate; }
            set { _trainingStartDate = value; }
        }

        private DateTime? _trainingEnddate;
        [DataMember]
        public DateTime? TrainingEnddate
        {
            get { return _trainingEnddate; }
            set { _trainingEnddate = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private string _branchAccountNo = string.Empty;
        [DataMember]
        public string BranchAccountNo
        {
            get { return _branchAccountNo; }
            set { _branchAccountNo = value; }
        }

        private string _templateName = string.Empty;
        [DataMember]
        public string TemplateName
        {
            get { return _templateName; }
            set { _templateName = value; }
        }

        private DateTime? _InvoiceDate;
        [DataMember]
        public DateTime? InvoiceDate
        {
            get { return _InvoiceDate; }
            set { _InvoiceDate = value; }
        }

        private DateTime? _originalDueDate;
        [DataMember]
        public DateTime? OriginalDueDate
        {
            get { return _originalDueDate; }
            set { _originalDueDate = value; }
        }

        private DateTime? _invoicePrintDate;
        [DataMember]
        public DateTime? InvoicePrintDate
        {
            get { return _invoicePrintDate; }
            set { _invoicePrintDate = value; }
        }

        private DateTime? _smsEmailInvoiceDate;
        [DataMember]
        public DateTime? SmsEmailInvoiceDate
        {
            get { return _smsEmailInvoiceDate; }
            set { _smsEmailInvoiceDate = value; }
        }

        private DateTime? _fullyPaidDate;
        [DataMember]
        public DateTime? FullyPaidDate
        {
            get { return _fullyPaidDate; }
            set { _fullyPaidDate = value; }
        }

        private string _originalCustomer = string.Empty;
        [DataMember]
        public string OriginalCustomer
        {
            get { return _originalCustomer; }
            set { _originalCustomer = value; }
        }

        private bool _isSPInvoice = false;
        [DataMember]
        public bool IsSPInvoice
        {
            get { return _isSPInvoice; }
            set { _isSPInvoice = value; }
        }

        private DateTime? _smsReminderDate;
        [DataMember]
        public DateTime? SmsReminderDate
        {
            get { return _smsReminderDate; }
            set { _smsReminderDate = value; }
        }

        private int _noOfReminders = 0;
        [DataMember]
        public int NoOfReminders
        {
            get { return _noOfReminders; }
            set { _noOfReminders = value; }
        }

        private DateTime? _lastReminderPrintedDate;
        [DataMember]
        public DateTime? LastReminderPrintedDate
        {
            get { return _lastReminderPrintedDate; }
            set { _lastReminderPrintedDate = value; }
        }

        private DateTime? _debtWarningSendDate;
        [DataMember]
        public DateTime? DebtWarningSendDate
        {
            get { return _debtWarningSendDate; }
            set { _debtWarningSendDate = value; }
        }

        private bool _isATG = false;
        [DataMember]
        public bool IsATG
        {
            get { return _isATG; }
            set { _isATG = value; }
        }

        private bool _deleteRequest = false;
        [DataMember]
        public bool DeleteRequest
        {
            get { return _deleteRequest; }
            set { _deleteRequest = value; }
        }

        private bool _processingByNets = false;
        [DataMember]
        public bool ProcessingByNets
        {
            get { return _processingByNets; }
            set { _processingByNets = value; }
        }

        private string _sendingNo = string.Empty;
        [DataMember]
        public string SendingNo
        {
            get { return _sendingNo; }
            set { _sendingNo = value; }
        }

        private DateTime? sendingDate;
        [DataMember]
        public DateTime? SendingDate
        {
            get { return sendingDate; }
            set { sendingDate = value; }
        }

        private int _atgInvoiceStatus;
        [DataMember]
        public int AtgInvoiceStatus
        {
            get { return _atgInvoiceStatus; }
            set { _atgInvoiceStatus = value; }
        }

        private string _addOnText = string.Empty;
        [DataMember]
        public string AddOnText
        {
            get { return _addOnText; }
            set { _addOnText = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }
       
        

        private List<InvoicePaymentSummaryDC> _paymentlist = new List<InvoicePaymentSummaryDC>();
        [DataMember]
        public List<InvoicePaymentSummaryDC> Paymentlist
        {
            get { return _paymentlist; }
            set { _paymentlist = value; }
        }

        private string _collectingStatus = string.Empty;
        [DataMember]
        public string CollectingStatus
        {
            get { return _collectingStatus; }
            set { _collectingStatus = value; }
        }
    }
}
