﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class CalendarHoliday
    {
        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private DateTime _holiday;
        [DataMember]
        public DateTime Holiday
        {
            get { return _holiday; }
            set { _holiday = value; }
        }
    }
}
