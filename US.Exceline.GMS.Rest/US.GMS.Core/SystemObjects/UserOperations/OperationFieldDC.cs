﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects.UserOperations
{
    public class OperationFieldDC
    {
        public string DisplayName { get; set; }
        public string Status { get; set; }
    }
}
