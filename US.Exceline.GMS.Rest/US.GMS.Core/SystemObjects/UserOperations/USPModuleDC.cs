﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects.UserOperations
{
    public class USPModuleDC
    {
        private List<USPFeatureDC> _features = new List<USPFeatureDC>();
        public string ID { get; set; }
        public int ModuleId { get; set; }
        public string DisplayName { get; set; }
        public USPFeatureDC DeafaultFeature { get; set; }
        public string ThumbnailImage { get; set; }
        public string ModuleHome { get; set; }
        public string ModulePackage { get; set; }
        public string ModuleColor { get; set; }
        public List<USPFeatureDC> Features { get; set; }
        public List<OperationDC> Operations { get; set; }
        public string CulturalDisplayName { get; set; }
        public string ModuleHomeURL { get; set; }
    }
}
