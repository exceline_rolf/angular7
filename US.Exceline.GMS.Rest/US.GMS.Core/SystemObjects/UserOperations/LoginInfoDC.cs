﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects.USerOperations
{
    public class LoginInfoDC
    {
        public string Mode { get; set; }
        public string LoginModulePackage {get; set;}
        public string AssemblyName {get; set;}
        public string LoginView {get; set;}
    }
}
