﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.GMS.Core.SystemObjects.UserOperations
{
    public class SubOperationDC
    {
        public string ID { get; set; }
        public int OperationId { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string ThumbnailImage { get; set; }
        public string OperationColor { get; set; }
        public OperationExtendedInfo ExtendedInfo { get; set; }
        public string SubOperationHome { set; get; }
        public string SubOperationPackage { set; get; }
        public string CulturalDisplayName { get; set; }
    }
}
