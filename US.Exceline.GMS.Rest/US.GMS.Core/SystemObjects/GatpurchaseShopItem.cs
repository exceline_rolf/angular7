﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public class GatpurchaseShopItem
    {
        private int _memberId;

        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }
        private decimal _itemPrice;

        public decimal ItemPrice
        {
            get { return _itemPrice; }
            set { _itemPrice = value; }
        }
        private string _cardNumber;

        public string CardNumber
        {
            get { return _cardNumber; }
            set { _cardNumber = value; }
        }
        private int _branchId;

        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }
        private string _itemCode;

        public string ItemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }
        private bool _isNextOrder;

        public bool IsNextOrder
        {
            get { return _isNextOrder; }
            set { _isNextOrder = value; }
        }
        private string _invoiceCode=string.Empty;

        public string InvoiceCode
        {
            get { return _invoiceCode; }
            set { _invoiceCode = value; }
        }
        private string _gymCode;

        public string GymCode
        {
            get { return _gymCode; }
            set { _gymCode = value; }
        }
        private int _arItemId;

        public int ArItemId
        {
            get { return _arItemId; }
            set { _arItemId = value; }
        }
    }
}
