﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class ContractAccessTimeDC
    {
        private string _day = string.Empty;
        [DataMember]
        public string Day
        {
            get { return _day; }
            set { _day = value; }
        }

        private DateTime? _inTime;
        [DataMember]
        public DateTime? InTime
        {
            get { return _inTime; }
            set { _inTime = value; }
        }

        private DateTime? _outTime;
        [DataMember]
        public DateTime? OutTime
        {
            get { return _outTime; }
            set { _outTime = value; }
        }
    }
}
