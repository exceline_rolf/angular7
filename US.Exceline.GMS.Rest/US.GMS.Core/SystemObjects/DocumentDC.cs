﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class DocumentDC
    {
        private int _fileId = -1;
        [DataMember]
        public int FileId
        {
            get { return _fileId; }
            set { _fileId = value; }
        }
        private string _cusId = string.Empty;
        [DataMember]
        public string CusId
        {
            get { return _cusId; }
            set { _cusId = value; }
        }
        private string _fileName = string.Empty;
        [DataMember]
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }
        private int _documentType = -1;
        [DataMember]
        public int DocumentType
        {
            get { return _documentType; }
            set { _documentType = value; }
        }
        private string _documentCategory = string.Empty;
        [DataMember]
        public string DocumentCategory
        {
            get { return _documentCategory; }
            set { _documentCategory = value; }
        }
        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }
        private string _filePath = string.Empty;
        [DataMember]
        public string FilePath
        {
            get { return _filePath; }
            set { _filePath = value; }
        }
        private string _uploadedUser = string.Empty;
        [DataMember]
        public string UploadedUser
        {
            get { return _uploadedUser; }
            set { _uploadedUser = value; }
        }
        private DateTime _uploadedDate = DateTime.Now;
        [DataMember]
        public DateTime UploadedDate
        {
            get { return _uploadedDate; }
            set { _uploadedDate = value; }
        }
        private bool _isActive = false;
        [DataMember]
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        private int _contractNo = -1;
        [DataMember]
        public int ContractNo
        {
            get { return _contractNo; }
            set { _contractNo = value; }
        }

        private bool _isScanDoc;
        [DataMember]
        public bool IsScanDoc
        {
            get { return _isScanDoc; }
            set { _isScanDoc = value; }
        }


    }
}
