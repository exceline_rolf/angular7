﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class MemberFeeGenerationResultDC
    {
        private bool _status = false;
        [DataMember]
        public bool Status
        {
            get { return _status; }
            set { _status = value; }
        }

    }
}
