﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public enum WithdrawalTypes
    {
        CASHWITHDRAWAL = 1,
        PETTYCASH = 2,
        ALL = -1,
        NONE = 0
    }
}
