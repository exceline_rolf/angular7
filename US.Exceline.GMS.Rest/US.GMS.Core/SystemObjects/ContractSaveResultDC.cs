﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class ContractSaveResultDC
    {
        private int _memberContractId = -1;
        [DataMember]
        public int MemberContractId
        {
            get { return _memberContractId; }
            set { _memberContractId = value; }
        }

        private string _memberContractNo = string.Empty;
        [DataMember]
        public string MemberContractNo
        {
            get { return _memberContractNo; }
            set { _memberContractNo = value; }
        }

        private bool _isGymChanged = false;
        [DataMember]
        public bool IsGymChanged
        {
            get { return _isGymChanged; }
            set { _isGymChanged = value; }
        }
    }
}
