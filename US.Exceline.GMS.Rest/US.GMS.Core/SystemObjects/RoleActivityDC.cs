﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.GMS.Core.SystemObjects
{
   [DataContract]
  public  class RoleActivityDC
    {
        private string _roleId = string.Empty;
          [DataMember]
        public string RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }

          private List<ActivityDC> _activityList = new List<ActivityDC>();
          [DataMember]
          public List<ActivityDC> ActivityList
        {
            get { return _activityList; }
            set { _activityList = value; }
        }

          private int _activityId;
       [DataMember]
          public int ActivityId
          {
              get { return _activityId; }
              set { _activityId = value; }
          }

       private int _categoryId;
       [DataMember]
       public int CategoryId
       {
           get { return _categoryId; }
           set { _categoryId = value; }
       }
    }
}
