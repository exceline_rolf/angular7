﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class ImportStatusDC
    {
        private int _itemNo = -1;
        [DataMember]
        public int ItemNo
        {
            get { return _itemNo; }
            set { _itemNo = value; }
        }

        private DateTime? _transferedDate;
        [DataMember]
        public DateTime? TransferedDate
        {
            get { return _transferedDate; }
            set { _transferedDate = value; }
        }

        private DateTime? _transferedTime;
        [DataMember]
        public DateTime? TransferedTime
        {
            get { return _transferedTime; }
            set { _transferedTime = value; }
        }

        private string _category = string.Empty;
        [DataMember]
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        private string _contractNo = string.Empty;
        [DataMember]
        public string ContractNo
        {
            get { return _contractNo; }
            set { _contractNo = value; }
        }

        private string _ref = string.Empty;
        [DataMember]
        public string Ref
        {
            get { return _ref; }
            set { _ref = value; }
        }

        private string _kid = string.Empty;
        [DataMember]
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }

        private int _atgStatus = -1;
        [DataMember]
        public int AtgStatus
        {
            get { return _atgStatus; }
            set { _atgStatus = value; }
        }

        private decimal _amount = 0;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private DateTime? _updatedDate;
        [DataMember]
        public DateTime? UpdatedDate
        {
            get { return _updatedDate; }
            set { _updatedDate = value; }
        }

        private int _transferStatus = -1;
        [DataMember]
        public int TransferStatus
        {
            get { return _transferStatus; }
            set { _transferStatus = value; }
        }

        private int _direction = 0;
        [DataMember]
        public int Direction
        {
            get { return _direction; }
            set { _direction = value; }
        }

    }
}
