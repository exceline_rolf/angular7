﻿using System;
using System.Runtime.Serialization;
namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class SalesPerMVACodeData
    {
        private String _mVASats  = String.Empty;
        [DataMember]
        public String MVASats
        {
            get { return _mVASats; }
            set { _mVASats = value; }
        }

        private String  _itemsSold = String.Empty;
        [DataMember]
        public String ItemsSold
        {
            get { return _itemsSold; }
            set { _itemsSold = value; }
        }

        private String  _totalAmount = String.Empty;
        [DataMember]
        public String TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }
    }
}
