﻿using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    public enum GymSettingType
    {
        [EnumMember]
        ACCESSTIME,
        [EnumMember]
        GYMOPENTIME,
        [EnumMember]
        REQUIRED,
        [EnumMember]
        MEMBERSEARCH,
        [EnumMember]
        HARDWARE,
        [EnumMember]
        ECONOMY,
        [EnumMember]
        SMS,
        [EnumMember]
        OTHER,
        [EnumMember]
        GYMINFO,
        [EnumMember]
        SEARCH,
        [EnumMember]
        INTEGRATION,
        [EnumMember]
        EXCINTEGRATION,
        [EnumMember]
        OTHERINTEGRATION,
        [EnumMember]
        SMSPARAMETERS
    }
}
