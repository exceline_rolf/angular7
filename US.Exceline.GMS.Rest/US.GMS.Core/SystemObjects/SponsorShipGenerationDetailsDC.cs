﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public class SponsorShipGenerationDetailsDC
    {
        private DateTime? _startDueDate = DateTime.Today;
        public DateTime? StartDueDate
        {
            get { return _startDueDate; }
            set { _startDueDate = value; }
        }

        private DateTime? _endDueDate = DateTime.Today;
        public DateTime? EndDueDate
        {
            get { return _endDueDate; }
            set { _endDueDate = value; }
        }

        private DateTime? _sponsoringDueDate;
        public DateTime? SponsoringDueDate
        {
            get { return _sponsoringDueDate; }
            set { _sponsoringDueDate = value; }
        }

        private DateTime? _visitStartDate;
        public DateTime? VisitStartDate
        {
            get { return _visitStartDate; }
            set { _visitStartDate = value; }
        }

        private DateTime? _visitEndDate;
        public DateTime? VisitEndDate
        {
            get { return _visitEndDate; }
            set { _visitEndDate = value; }
        }

        private bool _showOrderlines = false;
        public bool ShowOrderlines
        {
            get { return _showOrderlines; }
            set { _showOrderlines = value; }
        }

        private int _userBranchID = -1;
        public int UserBranchID
        {
            get { return _userBranchID; }
            set { _userBranchID = value; }
        }

        private int _sposoringBranchID = -1;
        public int SposoringBranchID
        {
            get { return _sposoringBranchID; }
            set { _sposoringBranchID = value; }
        }

        private string _sposoringGuiId = string.Empty;
        public string SposoringGuiId
        {
            get { return _sposoringGuiId; }
            set { _sposoringGuiId = value; }
        }

        private DateTime? _paymentDueDate;
        public DateTime? PaymentDueDate
        {
            get { return _paymentDueDate; }
            set { _paymentDueDate = value; }
        }

        private List<int> _selectedSponsors = new List<int>();
        public List<int> SelectedSponsors
        {
            get { return _selectedSponsors; }
            set { _selectedSponsors = value; }
        }
    }
}
