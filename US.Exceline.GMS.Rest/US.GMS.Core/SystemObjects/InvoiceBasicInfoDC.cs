﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class InvoiceBasicInfoDC
    {
        private string _tRef = string.Empty;
        private int _arNo = -1;
        private int _arItemNo = -1;
        private int _subCaseNo = -1;
        private DateTime _invocieDate;
        private DateTime _dueDate;
        private string _debtorNumber = string.Empty;
        private string _debtorName = string.Empty;
        private string _creditorNo = string.Empty;
        private string _creditorName = string.Empty;
        private decimal _amount = 0;
        private string _kid = string.Empty;
        private decimal _invoiceAmount = 0;
        private string _text = string.Empty;
        private decimal _cost = 0;
        private decimal _paid = 0;
        private decimal _balance = 0;
        private DateTime _lastPaymentPaidDate;
        private decimal _lastPaymentAmount = 0;
        private DateTime _lastPaymentRegDate;
        private string _lastAction = string.Empty;
        private DateTime _lastReminderDate;
        private string _lastNote = string.Empty;
        private string _nextAction = string.Empty;
        private DateTime _lastNoteDate;
        private int _creditorGroupNo = -1;
        private string _creditorGroupName = string.Empty;
        private int _itemTypeId = -1;


        [DataMember]
        public string TRef
        {
            get { return _tRef; }
            set { _tRef = value; }
        }

        [DataMember]
        public int ArNo
        {
            get { return _arNo; }
            set { _arNo = value; }
        }

        [DataMember]
        public int ArItemNo
        {
            get { return _arItemNo; }
            set { _arItemNo = value; }
        }

        [DataMember]
        public int SubCaseNo
        {
            get { return _subCaseNo; }
            set { _subCaseNo = value; }
        }

        [DataMember]
        public DateTime InvocieDate
        {
            get { return _invocieDate; }
            set { _invocieDate = value; }
        }

        [DataMember]
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        [DataMember]
        public string DebtorNumber
        {
            get { return _debtorNumber; }
            set { _debtorNumber = value; }
        }

        [DataMember]
        public string DebtorName
        {
            get { return _debtorName; }
            set { _debtorName = value; }
        }

        [DataMember]
        public string CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }

        [DataMember]
        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }
        }

        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        [DataMember]
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }

        [DataMember]
        public decimal InvoiceAmount
        {
            get { return _invoiceAmount; }
            set { _invoiceAmount = value; }
        }

        [DataMember]
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        [DataMember]
        public decimal Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }

        [DataMember]
        public decimal Paid
        {
            get { return _paid; }
            set { _paid = value; }
        }

        [DataMember]
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        [DataMember]
        public DateTime LastPaymentPaidDate
        {
            get { return _lastPaymentPaidDate; }
            set { _lastPaymentPaidDate = value; }
        }

        [DataMember]
        public decimal LastPaymentAmount
        {
            get { return _lastPaymentAmount; }
            set { _lastPaymentAmount = value; }
        }

        [DataMember]
        public DateTime LastPaymentRegDate
        {
            get { return _lastPaymentRegDate; }
            set { _lastPaymentRegDate = value; }
        }

        [DataMember]
        public string LastAction
        {
            get { return _lastAction; }
            set { _lastAction = value; }
        }

        [DataMember]
        public DateTime LastReminderDate
        {
            get { return _lastReminderDate; }
            set { _lastReminderDate = value; }
        }

        [DataMember]
        public string LastNote
        {
            get { return _lastNote; }
            set { _lastNote = value; }
        }

        [DataMember]
        public string NextAction
        {
            get { return _nextAction; }
            set { _nextAction = value; }
        }

        [DataMember]
        public DateTime LastNoteDate
        {
            get { return _lastNoteDate; }
            set { _lastNoteDate = value; }
        }

        [DataMember]
        public int CreditorGroupNo
        {
            get { return _creditorGroupNo; }
            set { _creditorGroupNo = value; }
        }

        [DataMember]
        public string CreditorGroupName
        {
            get { return _creditorGroupName; }
            set { _creditorGroupName = value; }
        }

        [DataMember]
        public int ItemTypeId
        {
            get { return _itemTypeId; }
            set { _itemTypeId = value; }
        }
    }
}
