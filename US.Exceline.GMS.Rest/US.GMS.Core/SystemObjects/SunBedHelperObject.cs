﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;

namespace US.GMS.Core.SystemObjects
{
    public class SunBedHelperObject
    {
        private GatpurchaseShopItem _gatPuchaseShopItem;

        public GatpurchaseShopItem GatPuchaseShopItem
        {
            get { return _gatPuchaseShopItem; }
            set { _gatPuchaseShopItem = value; }
        }
        private List<InstallmentDC> _installments = new List<InstallmentDC>();

        public List<InstallmentDC> Installments
        {
            get { return _installments; }
            set { _installments = value; }
        }
        private List<ShopSalesDC> _shopSaleDetails = new List<ShopSalesDC>();

        public List<ShopSalesDC> ShopSaleDetails
        {
            get { return _shopSaleDetails; }
            set { _shopSaleDetails = value; }
        }
        private List<PaymentDetailDC> _paymentDetails = new List<PaymentDetailDC>();

        public List<PaymentDetailDC> PaymentDetails
        {
            get { return _paymentDetails; }
            set { _paymentDetails = value; }
        }
        private int _memberBranchID;

        public int MemberBranchID
        {
            get { return _memberBranchID; }
            set { _memberBranchID = value; }
        }
        private int _loggedbranchID;

        public int LoggedbranchID
        {
            get { return _loggedbranchID; }
            set { _loggedbranchID = value; }
        }
        private string _user;

        public string User
        {
            get { return _user; }
            set { _user = value; }
        }
        private string _gymCode;

        public string GymCode
        {
            get { return _gymCode; }
            set { _gymCode = value; }
        }
        private bool _isBookingPayment = false;

        public bool IsBookingPayment
        {
            get { return _isBookingPayment; }
            set { _isBookingPayment = value; }
        }
        private bool _onlyShopOrder = false;

        public bool OnlyShopOrder
        {
            get { return _onlyShopOrder; }
            set { _onlyShopOrder = value; }
        }
    }
}
