﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    [KnownType(typeof(TrainerDC))]
    [KnownType(typeof(InstructorDC))]
    [KnownType(typeof(GymEmployeeDC))]
    [KnownType(typeof(CategoryDC))]
    public abstract class EmployeeDC //: ICloneable

    {
        [DataMember]
        public abstract int Id { get; set; }
        [DataMember]
        public abstract int EntNo { get; set; }
        [DataMember]
        public abstract string EntityRoleType { get; set; }
        [DataMember]
        public abstract string FirstName { get; set; }
        [DataMember]
        public abstract string LastName { get; set; }
        [DataMember]
        public abstract string Name { get; set; }
        [DataMember]
        public abstract string Email { get; set; }
        [DataMember]
        public abstract string Mobile { get; set; }
        [DataMember]
        public abstract string Work { get; set; }
        [DataMember]
        public abstract string Private { get; set; }
        [DataMember]
        public abstract string Description { get; set; }
        [DataMember]
        public abstract string Gender { get; set; }
        [DataMember]
        public abstract DateTime BirthDay { get; set; }
        [DataMember]
        public abstract int Age { get; set; }
        [DataMember]
        public abstract string Address1 { get; set; }
        [DataMember]
        public abstract string Address2 { get; set; }
        [DataMember]
        public abstract string Address3 { get; set; }
        [DataMember]
        public abstract string PostCode { get; set; }
        [DataMember]
        public abstract string City { get; set; }
        [DataMember]
        public abstract byte[] ProfilePicture { get; set; }
        //public abstract ICategory Category { get; set; }
         [DataMember]
        public abstract int PersonNo { get; set; }
        [DataMember]
        public abstract string RoleId { get; set; }
        [DataMember]
        public abstract string PostPlace { get; set; }
        [DataMember]
        public abstract int BranchId { get; set; }
        [DataMember]
        public abstract DateTime CreatedDate { get; set; }
        [DataMember]
        public abstract DateTime LastModifiedDate { get; set; }
        [DataMember]
        public abstract string CreatedUser { get; set; }
        [DataMember]
        public abstract string LastModifiedUser { get; set; }
        [DataMember]
        public abstract bool ActiveStatus { get; set; }
        [DataMember]
        public abstract ScheduleDC Schedule { set; get; }
        [DataMember]
        public abstract bool IsSelected { set; get; }
        [DataMember]
        public abstract CategoryDC Category { set; get; }
        [DataMember]
        public abstract string ImagePath { get; set; }
        [DataMember]
        public abstract List<ActivityDC> ActivityList { get; set; }
        [DataMember]
        public abstract bool IsAssign { get; set; }
        [DataMember]
        public abstract List<string> RoleItemList { get; set; }
        [DataMember]
        public abstract List<RoleActivityDC> RoleActivityList { get; set; }
        [DataMember]
        public abstract int AssignedEntityId { get; set; }
        [DataMember]
        public abstract string CommentForInactive { get; set; }
        //[DataMember]
        //public abstract DateTime InactivatedStartDate { get; set; }
        //[DataMember]
        //public abstract DateTime InactivatedEndDate { get; set; }
        [DataMember]
        public abstract bool IsHasTask { get; set; }
        [DataMember]
        public abstract int SourceEntity { get; set; }
        [DataMember]
        public abstract string CustId { get; set; }
    }
}
