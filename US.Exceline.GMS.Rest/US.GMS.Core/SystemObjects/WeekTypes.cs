﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public enum WeekTypes
    {
        ALL = 2,
        ODD = 1,
        EVEN = 0
    }
}
