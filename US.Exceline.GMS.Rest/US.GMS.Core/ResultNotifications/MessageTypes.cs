﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : US.Exceline.GMS
// Project Name      : US.GMS.Core
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : 26/03/2012 HH:MM  PM
// Description       : Based on US.Payment.ImportResult
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.ResultNotifications
{
    public enum MessageTypes
    {
        ERROR,
        WARNING,
        INFO
    }
}
