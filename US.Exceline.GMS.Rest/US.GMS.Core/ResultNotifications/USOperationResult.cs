﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.ResultNotifications
{
    public class OperationResult<T> : OperationResult
    {
        private T _operationReturnValue;
        public T OperationReturnValue
        {
            get { return _operationReturnValue; }
            set { _operationReturnValue = value; }
        }
    }
}
