﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : US.Exceline.GMS
// Project Name      : US.GMS.Core
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : 26/03/2012 HH:MM  PM
// Description       : Based on US.Payment.ImportResult
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace US.GMS.Core.ResultNotifications
{
    public class OperationResult
    {
        private object _tag1 = null;
        public object Tag1
        {
            get { return _tag1; }
            set { _tag1 = value; }
        }

        private object _tag2 = null;
        public object Tag2
        {
            get { return _tag2; }
            set { _tag2 = value; }
        }

        private object _tag3 = null;
        public object Tag3
        {
            get { return _tag3; }
            set { _tag3 = value; }
        }

        private object _tagSavedID = null;
        public object TagSavedID
        {
            get { return _tagSavedID; }
            set { _tagSavedID = value; }
        }

        private bool _errorOccured = false;
        public bool ErrorOccured
        {
            get { return _errorOccured; }
            set { _errorOccured = value; }
        }

        public OperationResult() {}

        private List<NotificationMessage> _notifications = new List<NotificationMessage>();
        public ReadOnlyCollection<NotificationMessage> Notifications
        {
            get { return new ReadOnlyCollection<NotificationMessage>(_notifications); }
        }

        protected void AddNotofications(NotificationMessage notification)
        {
            if (notification.MessageType == MessageTypes.ERROR)
            {
                _errorOccured = true;
            }
            _notifications.Add(notification);
        }

        public void AddNotofications(List<NotificationMessage> notifications)
        {
            foreach (NotificationMessage notification in notifications)
            {
                if (notification.MessageType == MessageTypes.ERROR && !(_errorOccured))
                    _errorOccured = true;
                _notifications.Add(notification);
            }
        }

        public void CreateMessage(string message, MessageTypes messageType)
        {
            if(messageType == MessageTypes.ERROR)
            {
                _errorOccured = true;
            }
            _notifications.Add(new NotificationMessage(message, messageType));
        }
    }
}
