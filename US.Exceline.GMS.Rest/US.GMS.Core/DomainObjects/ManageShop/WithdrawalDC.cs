﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Core.DomainObjects.ManageShop
{
    [DataContract]
    public class WithdrawalDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }


        private string _cusId = string.Empty;
        [DataMember]
        public string CusId
        {
            get { return _cusId; }
            set { _cusId = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private DateTime _createdDate = DateTime.Now;
        [DataMember]
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        private DateTime _createdTime;
        [DataMember]
        public DateTime CreatedTime
        {
            get { return _createdTime; }
            set { _createdTime = value; }
        }




        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private decimal _paymentAmount = 0.00M;
        [DataMember]
        public decimal PaymentAmount
        {
            get { return _paymentAmount; }
            set { _paymentAmount = value; }
        }
        private int _employeeId = -1;
        [DataMember]
        public int EmployeeId
        {
            get { return _employeeId; }
            set { _employeeId = value; }
        }

        private int _memberId = -1;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }
        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }
        private int _salePointId = -1;
        [DataMember]
        public int SalePointId
        {
            get { return _salePointId; }
            set { _salePointId = value; }
        }
        private WithdrawalTypes _withdrawalType = WithdrawalTypes.NONE;
        [DataMember]
        public WithdrawalTypes WithdrawalType
        {
            get { return _withdrawalType; }
            set { _withdrawalType = value; }
        }

        private CategoryDC _paymentType = new CategoryDC();
        [DataMember]
        public CategoryDC PaymentType
        {
            get { return _paymentType; }
            set { _paymentType = value; }
        }


        private string _withdrawalFrom = string.Empty;
        [DataMember]
        public string WithdrawalFrom
        {
            get { return _withdrawalFrom; }
            set { _withdrawalFrom = value; }
        }


        private int _paymentTypeId = -1;
        [DataMember]
        public int PaymentTypeId
        {
            get { return _paymentTypeId; }
            set { _paymentTypeId = value; }
        }


    }
}
