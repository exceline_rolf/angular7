﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageShop
{
    [DataContract]
    public class ShopSalesItemDC
    {
        private DateTime _saleDate;
        [DataMember]
        public DateTime SaleDate
        {
            get { return _saleDate; }
            set { _saleDate = value; }
        }

        private DateTime _saleTime;
        [DataMember]
        public DateTime SaleTime
        {
            get { return _saleTime; }
            set { _saleTime = value; }
        }

        private int _articleId;
        [DataMember]
        public int ArticleId
        {
            get { return _articleId; }
            set { _articleId = value; }
        }

        private string _itemName;
        [DataMember]
        public string ItemName
        {
            get { return _itemName; }
            set { _itemName = value; }
        }

        private int _quantity;
        [DataMember]
        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        private decimal _unitPrice;
        [DataMember]
        public decimal UnitPrice
        {
            get { return _unitPrice; }
            set { _unitPrice = value; }
        }

        private decimal _discount;
        [DataMember]
        public decimal Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }

        private decimal _vatRate=0.00M;
        [DataMember]
        public decimal VatRate
        {
            get { return _vatRate; }
            set { _vatRate = value; }
        }

        private decimal _totalAmount;
        [DataMember]
        public decimal TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }

        private int _itemTypeId = -1;
        [DataMember]
        public int ItemTypeId
        {
            get { return _itemTypeId; }
            set { _itemTypeId = value; }
        }

        private string _itemType = string.Empty;
        [DataMember]
        public string ItemType
        {
            get { return _itemType; }
            set { _itemType = value; }
        }

        private string _discountString;
        [DataMember]
        public string DiscountString
        {
            get { return _discountString; }
            set { _discountString = value; }
        }

        private int _shopSalesItemId;
        [DataMember]
        public int ShopSalesItemId
        {
            get { return _shopSalesItemId; }
            set { _shopSalesItemId = value; }
        }

        private int _shopSalesId;
        [DataMember]
        public int ShopSalesId
        {
            get { return _shopSalesId; }
            set { _shopSalesId = value; }
        }

        private decimal _amount;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private decimal _saleAmount;
        [DataMember]
        public decimal SaleAmount
        {
            get { return _saleAmount; }
            set { _saleAmount = value; }
        }

        private decimal _defaultAmount;
        [DataMember]
        public decimal DefaultAmount
        {
            get { return _defaultAmount; }
            set { _defaultAmount = value; }
        }

        private string _itemCode;
        [DataMember]
        public string ItemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }

        private string _memberName;
        [DataMember]
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        private DateTime _purchaseDate;
        [DataMember]
        public DateTime PurchaseDate
        {
            get { return _purchaseDate; }
            set { _purchaseDate = value; }
        }

        private string _purchaseDatePart;
        [DataMember]
        public string PurchaseDatePart
        {
            get { return _purchaseDatePart; }
            set { _purchaseDatePart = value; }
        }

        private string _purchaseTime;
        [DataMember]
        public string PurchaseTime
        {
            get { return _purchaseTime; }
            set { _purchaseTime = value; }
        }

        private int _settlementNo;
        [DataMember]
        public int SettlementNo
        {
            get { return _settlementNo; }
            set { _settlementNo = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private string _returnComment = string.Empty;
        [DataMember]
        public string ReturnComment
        {
            get { return _returnComment; }
            set { _returnComment = value; }
        }

        private string _itemCategory = string.Empty;
        [DataMember]
        public string ItemCategory
        {
            get { return _itemCategory; }
            set { _itemCategory = value; }
        }

        private string _employeeName = string.Empty;
        [DataMember]
        public string EmployeeName
        {
            get { return _employeeName; }
            set { _employeeName = value; }
        }

        private int _employeeId = -1;
        [DataMember]
        public int EmployeeId
        {
            get { return _employeeId; }
            set { _employeeId = value; }
        }

        private bool _isCompaign = false;
        [DataMember]
        public bool IsCompaign
        {
            get { return _isCompaign; }
            set { _isCompaign = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private string _voucherNo = string.Empty;
        [DataMember]
        public string VoucherNo
        {
            get { return _voucherNo; }
            set { _voucherNo = value; }
        }

        private DateTime? _expiryDate;
        [DataMember]
        public DateTime? ExpiryDate
        {
            get { return _expiryDate; }
            set { _expiryDate = value; }
        }
    }
}
