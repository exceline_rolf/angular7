﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Core.DomainObjects.ManageShop
{
    [DataContract]
    public class ShopTransactionDC
    {
        private string _cardType = "";
        [DataMember]
        public string CardType
        {
            get { return _cardType; }
            set { _cardType = value; }
        }

        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _salePointId = -1;
        [DataMember]
        public int SalePointId
        {
            get { return _salePointId; }
            set { _salePointId = value; }
        }


        private int _entityID = -1;
        [DataMember]
        public int EntityId
        {
            get { return _entityID; }
            set { _entityID = value; }
        }
      
        private string _entityRoleType = string.Empty;
        [DataMember]
        public string EntityRoleType
        {
            get { return _entityRoleType; }
            set { _entityRoleType = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private decimal _amount;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private string _createdUser;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        private TransactionTypes _mode = TransactionTypes.NONE;
        [DataMember]
        public TransactionTypes Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }


        private string _modeText = string.Empty;
        [DataMember]
        public string ModeText
        {
            get { return _modeText; }
            set { _modeText = value; }
        }

        private int _invoiceArItemNo = -1;
        [DataMember]
        public int InvoiceArItemNo
        {
            get { return _invoiceArItemNo; }
            set { _invoiceArItemNo = value; }
        }

        private string _voucherNo = string.Empty;
        [DataMember]
        public string VoucherNo
        {
            get { return _voucherNo; }
            set { _voucherNo = value; }
        }

    }
}
