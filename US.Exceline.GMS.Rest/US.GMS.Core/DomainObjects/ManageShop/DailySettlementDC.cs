﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageShop
{
    [DataContract]
    public class DailySettlementDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _settlmentCode = -1;
        [DataMember]
        public int SettlmentCode
        {
            get { return _settlmentCode; }
            set { _settlmentCode = value; }
        }



        private int _salePointId = -1;
        [DataMember]
        public int SalePointId
        {
            get { return _salePointId; }
            set { _salePointId = value; }
        }

        private decimal _exchange = 0.00M;
        [DataMember]
        public decimal Exchange
        {
            get { return _exchange; }
            set { _exchange = value; }
        }


        private decimal _totalInvoice = 0.00M;
        [DataMember]
        public decimal TotalInvoice
        {
            get { return _totalInvoice; }
            set { _totalInvoice = value; }
        }

        private decimal _prePaidPayment = 0.00M;
        [DataMember]
        public decimal PrePaidPayment
        {
            get { return _prePaidPayment; }
            set { _prePaidPayment = value; }
        }

        private decimal _cashReturn = 0.00M;
        [DataMember]
        public decimal CashReturn
        {
            get { return _cashReturn; }
            set { _cashReturn = value; }
        }

        private decimal _cashIn = 0.00M;
        [DataMember]
        public decimal CashIn
        {
            get { return _cashIn; }
            set { _cashIn = value; }
        }

        private decimal _cashOut = 0.00M;
        [DataMember]
        public decimal CashOut
        {
            get { return _cashOut; }
            set { _cashOut = value; }
        }

        private decimal _cash = 0.00M;
        [DataMember]
        public decimal Cash
        {
            get { return _cash; }
            set { _cash = value; }
        }

        private DateTime _fromDateTime = DateTime.Now;
        [DataMember]
        public DateTime FromDateTime
        {
            get { return _fromDateTime; }
            set { _fromDateTime = value; }
        }

        private DateTime _toDateTime = DateTime.Now;
        [DataMember]
        public DateTime ToDateTime
        {
            get { return _toDateTime; }
            set { _toDateTime = value; }
        }


        private decimal _bankTerminal = 0.00M;
        [DataMember]
        public decimal BankTerminal
        {
            get { return _bankTerminal; }
            set { _bankTerminal = value; }
        }



        private decimal _giftCard = 0.00M;
        [DataMember]
        public decimal GiftCard
        {
            get { return _giftCard; }
            set { _giftCard = value; }
        }

        private decimal _onAccount = 0.00M;
        [DataMember]
        public decimal OnAccount
        {
            get { return _onAccount; }
            set { _onAccount = value; }
        }

        private decimal _shopReturn = 0.00M;
        [DataMember]
        public decimal ShopReturn
        {
            get { return _shopReturn; }
            set { _shopReturn = value; }
        }

        private decimal _cashWithdrawl = 0.00M;
        [DataMember]
        public decimal CashWithdrawl
        {
            get { return _cashWithdrawl; }
            set { _cashWithdrawl = value; }
        }

        private decimal _pettyCash = 0.00M;
        [DataMember]
        public decimal PettyCash
        {
            get { return _pettyCash; }
            set { _pettyCash = value; }
        }

        private string _createdUser;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private decimal _totalReceived = 0.00M;
        [DataMember]
        public decimal TotalReceived
        {
            get { return _totalReceived; }
            set { _totalReceived = value; }
        }

        private decimal _totalPayments = 0.00M;
        [DataMember]
        public decimal TotalPayments
        {
            get { return _totalPayments; }
            set { _totalPayments = value; }
        }

        private decimal _countedCash = 0.00M;
        [DataMember]
        public decimal CountedCash
        {
            get { return _countedCash; }
            set { _countedCash = value; }
        }

        private decimal _deviation = 0.00M;
        [DataMember]
        public decimal Deviation
        {
            get { return _deviation; }
            set { _deviation = value; }
        }

        private decimal _nextOrder = 0.00M;
        [DataMember]
        public decimal NextOrder
        {
            get { return _nextOrder; }
            set { _nextOrder = value; }
        }

        private decimal _invoice = 0.00M;
        [DataMember]
        public decimal Invoice
        {
            get { return _invoice; }
            set { _invoice = value; }
        }

        private decimal _sMSEmailInvoice = 0.00M;
        [DataMember]
        public decimal SMSEmailInvoice
        {
            get { return _sMSEmailInvoice; }
            set { _sMSEmailInvoice = value; }
        }


        private bool _isForAllSalePoint = false;
        [DataMember]
        public bool IsForAllSalePoint
        {
            get { return _isForAllSalePoint; }
            set { _isForAllSalePoint = value; }
        }
    }
}
