﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.SystemObjects;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using US.GMS.Core.DomainObjects.Admin.ManageClassSettings;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Core.DomainObjects.ScheduleManagement
{
    [DataContract]
    public class ScheduleItemDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _scheduleId = -1;
        [DataMember]
        public int ScheduleId
        {
            get { return _scheduleId; }
            set { _scheduleId = value; }
        }

        private DateTime _startDate;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime _EndDate;
        [DataMember]
        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        private string _day = string.Empty;
        [DataMember]
        public string Day
        {
            get { return _day; }
            set { _day = value; }
        }

        private int _week = -1;
        [DataMember]
        public int Week
        {
            get { return _week; }
            set { _week = value; }
        }

        private string _month = string.Empty;
        [DataMember]
        public string Month
        {
            get { return _month; }
            set { _month = value; }
        }

        private int _year = 0;
        [DataMember]
        public int Year
        {
            get { return _year; }
            set { _year = value; }
        }

        private DateTime _startTime;
        [DataMember]
        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private DateTime _endTime;
        [DataMember]
        public DateTime EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        private int _entityId = -1;
        [DataMember]
        public int EntityId
        {
            get { return _entityId; }
            set { _entityId = value; }
        }

        private int _entityTypeId = -1;
        [DataMember]
        public int EntityTypeId
        {
            get { return _entityTypeId; }
            set { _entityTypeId = value; }
        }

        private ScheduleTypes _occurrence = ScheduleTypes.NONE;
        [DataMember]
        public ScheduleTypes Occurrence
        {
            get { return _occurrence; }
            set { _occurrence = value; }
        }

        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        private DateTime _lastModifiedDate;
        [DataMember]
        public DateTime LastModifiedDate
        {
            get
            {
                return _lastModifiedDate;
            }
            set
            {
                _lastModifiedDate = value;
            }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get
            {
                return _createdUser;
            }
            set
            {
                _createdUser = value;
            }
        }

        private string _lastModifiedUser;
        [DataMember]
        public string LastModifiedUser
        {
            get
            {
                return _lastModifiedUser;
            }
            set
            {
                _lastModifiedUser = value;
            }
        }

        private bool _activeStatus;
        [DataMember]
        public bool ActiveStatus
        {
            get
            {
                return _activeStatus;
            }
            set
            {
                _activeStatus = value;
            }
        }

        private bool _isFixed = false;
        [DataMember]
        public bool IsFixed
        {
            get { return _isFixed; }
            set { _isFixed = value; }
        }

        private DateTime _lastActiveTimeGeneratedDate;
        [DataMember]
        public DateTime LastActiveTimeGeneratedDate
        {
            get { return _lastActiveTimeGeneratedDate; }
            set { _lastActiveTimeGeneratedDate = value; }
        }

        private bool _isParentable = true;
        [DataMember]
        public bool IsParentable
        {
            get { return _isParentable; }
            set { _isParentable = value; }
        }

        private int _parentId = -1;
        [DataMember]
        public int ParentId
        {
            get { return _parentId;  }
            set { _parentId = value; }
        }

        private int _classTypeId = -1;
        [DataMember]
        public int ClassTypeId
        {
            get { return _classTypeId; }
            set { _classTypeId = value; }
        }

        private List<EntityActiveTimeDC> _activeTimes = new List<EntityActiveTimeDC>();
        [DataMember]
        public List<EntityActiveTimeDC> ActiveTimes
        {
            get { return _activeTimes; }
            set { _activeTimes = value; }
        }

        private string _activeTimesString = string.Empty;
        [DataMember]

        public string ActiveTimesString
        {
            get { return _activeTimesString; }
            set { _activeTimesString = value; }
        }

        private int _weekType = 0;
        [DataMember]
        public int WeekType
        {
            get { return _weekType; }
            set { _weekType = value; }
        }

        private int _maxNoOfBooking = 0;
        [DataMember]
        public int MaxNoOfBooking
        {
            get { return _maxNoOfBooking; }
            set { _maxNoOfBooking = value; }
        }

        private string _entityRoleType = string.Empty;
        [DataMember]
        public string EntityRoleType
        {
            get { return _entityRoleType; }
            set { _entityRoleType = value; }
        }

        private List<int> _instructorIdList = new List<int>();
        [DataMember]
        public List<int> InstructorIdList
        {
            get { return _instructorIdList; }
            set { _instructorIdList = value; }
        }

        private Dictionary<int, string> _instructorIdNameList = new Dictionary<int, string>();
        [DataMember]
        public Dictionary<int, string> InstructorIdNameList
        {
            get { return _instructorIdNameList; }
            set { _instructorIdNameList = value; }
        }

        private string _resourcesName = string.Empty;
        [DataMember]
        public string ResourcesName
        {
            get { return _resourcesName; }
            set { _resourcesName = value; }
        }

        private int _resourceId;
        [DataMember]
        public int ResourceId
        {
            get { return _resourceId; }
            set { _resourceId = value; }
        }

        private List<int> _resourceIdList = new List<int>();
        [DataMember]
        public List<int> ResourceIdList
        {
            get { return _resourceIdList; }
            set { _resourceIdList = value; }
        }

        private Dictionary<int, string> _resourceIdNameList = new Dictionary<int, string>();
        [DataMember]
        public Dictionary<int, string> ResourceIdNameList
        {
            get { return _resourceIdNameList; }
            set { _resourceIdNameList = value; }
        }

        private int _classNumber = -1;
        [DataMember]
        public int ClassNumber
        {
            get { return _classNumber; }
            set { _classNumber = value; }
        }

        private ExceClassTypeDC _classType = new ExceClassTypeDC();
        [DataMember]
        public ExceClassTypeDC ClassType
        {
            get { return _classType; }
            set { _classType = value; }
        }

        private bool _isUpdatedAfterClassStarted = false;
        [DataMember]
        public bool IsUpdatedAfterClassStarted
        {
            get { return _isUpdatedAfterClassStarted; }
            set { _isUpdatedAfterClassStarted = value; }
        }

        private bool _isOnlyEndDateUpdated = false;
        [DataMember]
        public bool IsOnlyEndDateUpdated
        {
            get { return _isOnlyEndDateUpdated; }
            set { _isOnlyEndDateUpdated = value; }
        }


        private DateTime? _effectiveDate;
        [DataMember]
        public DateTime? EffectiveDate
        {
            get
            {
                return _effectiveDate;
            }
            set
            {
                _effectiveDate = value;
            }
        }

        private string _empName = string.Empty;
        [DataMember]
        public string EmpName
        {
            get { return _empName; }
            set { _empName = value; }
        }



        private int _empId;
        [DataMember]
        public int EmpId
        {
            get { return _empId; }
            set { _empId = value; }
        }

        private string _roleName = string.Empty;
        [DataMember]
        public string RoleName
        {
            get { return _roleName; }
            set { _roleName = value; }
        }



        private int _roleId;
        [DataMember]
        public int RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }


        private int _updateStatus;
        [DataMember]
        public int UpdateStatus
        {
            get { return _updateStatus; }
            set { _updateStatus = value; }
        }

        private bool _isBooking;
        [DataMember]
        public bool IsBooking
        {
            get { return _isBooking; }
            set { _isBooking = value; }
        }


        private ExcelineJobDC _excelineJob = new ExcelineJobDC();
        [DataMember]
        public ExcelineJobDC ExcelineJob
        {
            get { return _excelineJob; }
            set { _excelineJob = value; }
        }

        private List<EntityActiveTimeDC> _entityActiveTimeLst = new List<EntityActiveTimeDC>();
        [DataMember]
        public List<EntityActiveTimeDC> EntityActiveTimeLst
        {
            get { return _entityActiveTimeLst; }
            set { _entityActiveTimeLst = value; }
        }

        private bool _isRole = false;
        [DataMember]
        public bool IsRole
        {
            get { return _isRole; }
            set { _isRole = value; }
        }

        private bool _isScheduleSelected = false;
        [DataMember]
        public bool IsScheduleSelected
        {
            get { return _isScheduleSelected; }
            set { _isScheduleSelected = value; }
        }


        private List<GymEmployeeDC> _employeeList = new List<GymEmployeeDC>();
        [DataMember]
        public List<GymEmployeeDC> EmployeeList
        {
            get { return _employeeList; }
            set { _employeeList = value; }
        }

        private Dictionary<int, List<DateTime>> _switchDataRange = new Dictionary<int, List<DateTime>>();
        [DataMember]
        public Dictionary<int, List<DateTime>> SwitchDataRange
        {
            get { return _switchDataRange; }
            set { _switchDataRange = value; }
        }

        private List<String> _dayList = new List<string>();
        [DataMember]
        public List<string> DayList
        {
            get { return _dayList; }
            set { _dayList = value; }
        }


        private int _editOptionId;
        [DataMember]
        public int EditOptionId
        {
            get { return _editOptionId; }
            set { _editOptionId = value; }
        }

        private string _daySortingHelper = string.Empty;
        [DataMember]
        public string DaySortingHelper
        {
            get { return _daySortingHelper; }
            set { _daySortingHelper = value; }
        }

        private int _classId = -1;
        [DataMember]
        public int ClassId
        {
            get { return _week; }
            set { _week = value; }
        }
        private int _filterDay = -1;
        [DataMember]
        public int FilterDay
        {
            get { return _filterDay; }
            set { _filterDay = value; }
        }

        private int _activeTimeID;
        [DataMember]
        public int ActiveTimeID
        {
            get { return _activeTimeID; }
            set { _activeTimeID = value; }
        }

        private string _instructorNameListStr;
        [DataMember]
        public string InstructorNameListStr
        {
            get { return string.Join(",", _instructorIdNameList.Values.ToList()); }
            set { _instructorNameListStr = value; }
        }

        private string _locationNameListStr;
        [DataMember]
        public string LocationNameListStr
        {
            get { return string.Join(",", _resourceIdNameList.Values.ToList()); }
            set { _locationNameListStr = value; }
        }

        private int _numberOfParticipants = 0;
        [DataMember]
        public int NumberOfParticipants
        {
            get { return _numberOfParticipants; }
            set { _numberOfParticipants = value; }
        }

        private bool _isIBookingIntegrated = false;
        [DataMember]
        public bool IsIBookingIntegrated
        {
            get { return _isIBookingIntegrated; }
            set { _isIBookingIntegrated = value; }
        }

        private bool _isDeleteRecord;
           [DataMember]
        public bool IsDeleteRecord
        {
            get { return _isDeleteRecord; }
            set { _isDeleteRecord = value; }
        }


        private string _culture;
        [DataMember]
        public string Culture
        {
            get { return _culture; }
            set { _culture = value; }
        }
    }

}
