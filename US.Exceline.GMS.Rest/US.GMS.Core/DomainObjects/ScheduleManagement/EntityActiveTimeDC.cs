﻿using System;
using System.Collections.Generic;
using System.Linq;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.SystemObjects;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Core.DomainObjects.ScheduleManagement
{
    [DataContract]
    public class EntityActiveTimeDC : ActiveTimeDC
    {
        private int _id = 0;
        [DataMember]
        public override int Id
        {
            get { return _id; }
            set
            {
                _id = value;
            }
        }

        private int _sheduleItemId = -1;
        [DataMember]
        public override int SheduleItemId
        {
            get
            {
                return _sheduleItemId;
            }
            set
            {
                _sheduleItemId = value;
            }
        }

        private int _scheduleId = -1;
        [DataMember]
        public int ScheduleId
        {
            get { return _scheduleId; }
            set { _scheduleId = value; }
        }

        private int _scheduleParentId = -1;
        [DataMember]
        public int ScheduleParentId
        {
            get { return _scheduleParentId; }
            set { _scheduleParentId = value; }
        }

        private DateTime _startDateTime;
        [DataMember]
        public override DateTime StartDateTime
        {
            get { return _startDateTime; }
            set
            {
                _startDateTime = value;
            }
        }

        private DateTime _endDateTime;
        [DataMember]
        public override DateTime EndDateTime
        {
            get { return _endDateTime; }
            set
            {
                _endDateTime = value;
            }
        }

        private int _entityId = -1;
        [DataMember]
        public override int EntityId
        {
            get { return _entityId; }
            set
            {
                _entityId = value;
            }
        }

        private string _entityRoleType = string.Empty;
        [DataMember]
        public override string EntityRoleType
        {
            get { return _entityRoleType; }
            set
            {
                _entityRoleType = value;
            }
        }

        private int _entNo = -1;
        [DataMember]
        public override int EntNo
        {
            get { return _entNo; }
            set
            {
                _entNo = value;
            }
        }

        private string _name = string.Empty;
        [DataMember]
        public override string Name
        {
            get { return _name; }
            set
            {
                _name = value;
            }
        }

        private string _entityName = string.Empty;
        [DataMember]
        public string EntityName
        {
            get { return _entityName; }
            set { _entityName = value; }
        }

        private byte[] _profilePicture;
        [DataMember]
        public byte[] ProfilePicture
        {
            get { return _profilePicture; }
            set { _profilePicture = value; }
        }

        private string _imagePath = string.Empty;
        public string ImagePath
        {
            get { return _imagePath; }
            set { _imagePath = value; }
        }

        private bool _isDeleted = false;
        [DataMember]
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }

        private int _numberOfMembers;
        [DataMember]
        public override int NumberOfMembers
        {
            get { return _numberOfMembers; }
            set { _numberOfMembers = value; }
        }

        private int _maxNoOfBookings;
        [DataMember]
        public int MaxNoOfBookings
        {
            get { return _maxNoOfBookings; }
            set { _maxNoOfBookings = value; }
        }

        private int _queueCount;
        [DataMember]
        public int QueueCount
        {
            get { return _queueCount; }
            set { _queueCount = value; }
        }

        private string _instructorName;
        [DataMember]
        public override string InstructorName
        {
            get { return _instructorName; }
            set { _instructorName = value; }
        }

        private int _memberId;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private bool _isScheduleActiveTime;
        [DataMember]
        public bool IsScheduleActiveTime
        {
            get { return _isScheduleActiveTime; }
            set { _isScheduleActiveTime = value; }
        }


        private string _empName = string.Empty;
        [DataMember]
        public string EmpName
        {
            get { return _empName; }
            set { _empName = value; }
        }


        private string _resName = string.Empty;
        [DataMember]
        public string ResName
        {
            get { return _resName; }
            set { _resName = value; }
        }



        private bool _isBooking;
        [DataMember]
        public bool IsBooking
        {
            get { return _isBooking; }
            set { _isBooking = value; }
        }


        private bool _isActivitySchedule;
        [DataMember]
        public bool IsActivitySchedule
        {
            get { return _isActivitySchedule; }
            set { _isActivitySchedule = value; }
        }



        private Dictionary<int, string> _memberLst = new Dictionary<int, string>();
        [DataMember]
        public Dictionary<int, string> MemberLst
        {
            get { return _memberLst; }
            set { _memberLst = value; }
        }

        private List<BookingEntityDc> _bookingMemberList = new List<BookingEntityDc>();
        [DataMember]
        public List<BookingEntityDc> BookingMemberList
        {
            get { return _bookingMemberList; }
            set { _bookingMemberList = value; }
        }

        private List<BookingEntityDc> _bookingResourceList = new List<BookingEntityDc>();
        [DataMember]
        public List<BookingEntityDc> BookingResourceList
        {
            get { return _bookingResourceList; }
            set { _bookingResourceList = value; }
        }

        private List<EntityDC> _resourceLst = new List<EntityDC>();
        [DataMember]
        public List<EntityDC> ResourceLst
        {
            get { return _resourceLst; }
            set { _resourceLst = value; }
        }

        private List<EntityDC> _instructorList = new List<EntityDC>();
        [DataMember]
        public List<EntityDC> InstructorList
        {
            get { return _instructorList; }
            set { _instructorList = value; }
        }

        private Dictionary<int, string> _employeeList = new Dictionary<int, string>();
        [DataMember]
        public Dictionary<int, string> EmployeeList
        {
            get { return _employeeList; }
            set { _employeeList = value; }
        }

        private List<int> _instructorIdList = new List<int>();
        [DataMember]
        public List<int> InstructorIdList
        {
            get { return _instructorIdList; }
            set { _instructorIdList = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private bool _isSmsReminder;
        [DataMember]
        public bool IsSmsReminder
        {
            get { return _isSmsReminder; }
            set { _isSmsReminder = value; }
        }

        private string _activityName = string.Empty;
        [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        private int _activityId;
        [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        private string _articleName = string.Empty;
        [DataMember]
        public string ArticleName
        {
            get { return _articleName; }
            set { _articleName = value; }
        }

        private int _articleId;
        [DataMember]
        public int ArticleId
        {
            get { return _articleId; }
            set { _articleId = value; }
        }

        private int _articleNoOfMinutes;
        [DataMember]
        public int ArticleNoOfMinutes
        {
            get { return _articleNoOfMinutes; }
            set { _articleNoOfMinutes = value; }
        }

        private int _categoryId;
        [DataMember]
        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }

        private string _categoryName = string.Empty;
        [DataMember]
        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }

        private int _bookingCategoryId;
        [DataMember]
        public int BookingCategoryId
        {
            get { return _bookingCategoryId; }
            set { _bookingCategoryId = value; }
        }

        private string _bookingCategoryName = string.Empty;
        [DataMember]
        public string BookingCategoryName
        {
            get { return _bookingCategoryName; }
            set { _bookingCategoryName = value; }
        }

        private string _bookingCategoryColor = string.Empty;
        [DataMember]
        public string BookingCategoryColor
        {
            get { return _bookingCategoryColor; }
            set { _bookingCategoryColor = value; }
        }

        private string _color = string.Empty;
        [DataMember]
        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }

        private bool _isArrived;
        [DataMember]
        public bool IsArrived
        {
            get { return _isArrived; }
            set { _isArrived = value; }
        }

        private DateTime? _arrivedDate;
        [DataMember]
        public DateTime? ArrivedDate
        {
            get { return _arrivedDate; }
            set { _arrivedDate = value; }
        }

        private bool _isPaid;
        [DataMember]
        public bool IsPaid
        {
            get { return _isPaid; }
            set { _isPaid = value; }
        }

        private bool _isPunchCard;
        [DataMember]
        public bool IsPunchCard
        {
            get { return _isPunchCard; }
            set { _isPunchCard = value; }
        }

        private bool _isContractBooking;
        [DataMember]
        public bool IsContractBooking
        {
            get { return _isContractBooking; }
            set { _isContractBooking = value; }
        }

        private bool _isUnAvailableActiveTime;
        [DataMember]
        public bool IsUnAvailableActiveTime
        {
            get { return _isUnAvailableActiveTime; }
            set { _isUnAvailableActiveTime = value; }
        }

        private decimal _totalPaid;
        [DataMember]
        public decimal TotalPaid
        {
            get { return _totalPaid; }
            set { _totalPaid = value; }
        }

        private int _arItemNo;
        [DataMember]
        public int ArItemNo
        {
            get { return _arItemNo; }
            set { _arItemNo = value; }
        }


        private string _roleType = string.Empty;
        [DataMember]
        public string RoleType
        {
            get { return _roleType; }
            set { _roleType = value; }
        }

        private bool _isAdditionalResActiveTime;
        [DataMember]
        public bool IsAdditionalResActiveTime
        {
            get { return _isAdditionalResActiveTime; }
            set { _isAdditionalResActiveTime = value; }
        }

        private int _taskType;
        [DataMember]
        public int TaskType
        {
            get { return _taskType; }
            set { _taskType = value; }
        }

        private string _text = string.Empty;
        [DataMember]
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private int _classTypeId;
        [DataMember]
        public int ClassTypeId
        {
            get { return _classTypeId; }
            set { _classTypeId = value; }
        }

        private string _classType = string.Empty;
        [DataMember]
        public string ClassType
        {
            get { return _classType; }
            set { _classType = value; }
        }

        private bool _isTask;
        [DataMember]
        public bool IsTask
        {
            get { return _isTask; }
            set { _isTask = value; }
        }

        private bool _isBookingCreditback;
        [DataMember]
        public bool IsBookingCreditback
        {
            get { return _isBookingCreditback; }
            set { _isBookingCreditback = value; }
        }


        private bool _isExtraResChange;
        [DataMember]
        public bool IsExtraResChange
        {
            get { return _isExtraResChange; }
            set { _isExtraResChange = value; }
        }


        private int _additionalResId;
        [DataMember]
        public int AdditionalResId
        {
            get { return _additionalResId; }
            set { _additionalResId = value; }
        }

        private bool _isFixed;
        [DataMember]
        public bool IsFixed
        {
            get { return _isFixed; }
            set { _isFixed = value; }
        }


        private int _employeeId;
         [DataMember]
        public int EmployeeId
        {
            get { return _employeeId; }
            set { _employeeId = value; }
        }

   
        private bool isSwitchSchedule;
         [DataMember]
        public bool IsSwitchSchedule
        {
            get { return isSwitchSchedule; }
            set { isSwitchSchedule = value; }
        }

        private int _weekType;
         [DataMember]
        public int WeekType
        {
            get { return _weekType; }
            set { _weekType = value; }
        }

        private int _availableVisit;
         [DataMember]
        public int AvailableVisit
        {
            get { return _availableVisit; }
            set { _availableVisit = value; }
        }

      
        private DateTime _startDate = DateTime.Now;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

      
        private DateTime _endDate = DateTime.Now;
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private List<EntityActiveTimeDC> _activeTimes = new List<EntityActiveTimeDC>();
        [DataMember]
        public List<EntityActiveTimeDC> ActiveTimes
        {
            get { return _activeTimes; }
            set { _activeTimes = value; }
        }

        
        private int _paidMemberId;
        [DataMember]
        public int PaidMemberId
        {
            get { return _paidMemberId; }
            set { _paidMemberId = value; }
        }

        private string _displayDay;
        [DataMember]
        public string DisplayDay
        {
            get { return _displayDay; }
            set { _displayDay = value; }
        }

        private int _day = -1;
        [DataMember]
        public int Day
        {
            get { return _day; }
            set { _day = value; }
        }

        private int _classLevelId;
        [DataMember]
        public int ClassLevelId
        {
            get { return _classLevelId; }
            set { _classLevelId = value; }
        }

        private int _classGroupId;
        [DataMember]
        public int ClassGroupId
        {
            get { return _classGroupId; }
            set { _classGroupId = value; }
        }

        private string _instructorNameListStr;
        [DataMember]
        public string InstructorNameListStr
        {
            get { return _instructorList != null ? string.Join(", ", _instructorList.Select(x => x.DisplayName)) : "" ; }
            set { _instructorNameListStr = value; }
        }

        private string _locationNameListStr;
        [DataMember]
        public string LocationNameListStr
        {
            get { return _resourceLst != null ? string.Join(", ", _resourceLst.Select(x => x.DisplayName)) : "" ; }
            set { _locationNameListStr = value; }
        }

        private bool _isIBookingIntegrated = false;
        [DataMember]
        public bool IsIBookingIntegrated
        {
            get { return _isIBookingIntegrated; }
            set { _isIBookingIntegrated = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

    }
}
