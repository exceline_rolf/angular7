﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ScheduleManagement
{
    [DataContract]
    public class TaskTemplateDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }


        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private TaskTemplateType _templateType;
        [DataMember]
        public TaskTemplateType TemplateType
        {
            get { return _templateType; }
            set { _templateType = value; }
        }

        private bool _isAssignTo = false;
        [DataMember]
        public bool IsAssignTo
        {
            get { return _isAssignTo; }
            set { _isAssignTo = value; }
        }

        private bool _isStartDate = false;
        [DataMember]
        public bool IsStartDate
        {
            get { return _isStartDate; }
            set { _isStartDate = value; }
        }
        private bool _isEndDate = false;
        [DataMember]
        public bool IsEndDate
        {
            get { return _isEndDate; }
            set { _isEndDate = value; }
        }

        private bool _isDueTime = false;
        [DataMember]
        public bool IsDueTime
        {
            get { return _isDueTime; }
            set { _isDueTime = value; }
        }

        private bool _isRepeatable = false;
        [DataMember]
        public bool IsRepeatable
        {
            get { return _isRepeatable; }
            set { _isRepeatable = value; }
        }

        private bool _isStartTime = false;
        [DataMember]
        public bool IsStartTime
        {
            get { return _isStartTime; }
            set { _isStartTime = value; }
        }

        private bool _isEndTime = false;
        [DataMember]
        public bool IsEndTime
        {
            get { return _isEndTime; }
            set { _isEndTime = value; }
        }

        private bool _isFoundDate = false;
        [DataMember]
        public bool IsFoundDate
        {
            get { return _isFoundDate; }
            set { _isFoundDate = value; }
        }

        private bool _isReturnDate = false;
        [DataMember]
        public bool IsReturnDate
        {
            get { return _isReturnDate; }
            set { _isReturnDate = value; }
        }

        private bool _isDueDate = false;
        [DataMember]
        public bool IsDueDate
        {
            get { return _isDueDate; }
            set { _isDueDate = value; }
        }

        private bool _isNoOfDays = false;
        [DataMember]
        public bool IsNoOfDays
        {
            get { return _isNoOfDays; }
            set { _isNoOfDays = value; }
        }

        private bool _isPopUp = false;
        [DataMember]
        public bool IsPopUp
        {
            get { return _isPopUp; }
            set { _isPopUp = value; }
        }

        private bool _isPhoneNo = false;
        [DataMember]
        public bool IsPhoneNo
        {
            get { return _isPhoneNo; }
            set { _isPhoneNo = value; }
        }
        private bool _isSms = false;
        [DataMember]
        public bool IsSms
        {
            get { return _isSms; }
            set { _isSms = value; }
        }

        private bool _isFollowupMember = false;
        [DataMember]
        public bool IsFollowupMember
        {
            get { return _isFollowupMember; }
            set { _isFollowupMember = value; }
        }

        private bool _isBelongsToMember = false;
        [DataMember]
        public bool IsBelongsToMember
        {
            get { return _isBelongsToMember; }
            set { _isBelongsToMember = value; }
        }

        private ObservableCollection<ExtendedTaskTemplateDC> _extendedFieldsList = new ObservableCollection<ExtendedTaskTemplateDC>();
        [DataMember]
        public ObservableCollection<ExtendedTaskTemplateDC> ExtendedFieldsList
        {
            get { return _extendedFieldsList; }
            set { _extendedFieldsList = value; }
        }

        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        private DateTime _lastModifiedDate;
        [DataMember]
        public DateTime LastModifiedDate
        {
            get
            {
                return _lastModifiedDate;
            }
            set
            {
                _lastModifiedDate = value;
            }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get
            {
                return _createdUser;
            }
            set
            {
                _createdUser = value;
            }
        }

        private string _lastModifiedUser = string.Empty;
        [DataMember]
        public string LastModifiedUser
        {
            get
            {
                return _lastModifiedUser;
            }
            set
            {
                _lastModifiedUser = value;
            }
        }

        private bool _activeStatus;
        [DataMember]
        public bool ActiveStatus
        {
            get
            {
                return _activeStatus;
            }
            set
            {
                _activeStatus = value;
            }
        }

        private bool _isDefault = false;
        [DataMember]
        public bool IsDefault
        {
            get { return _isDefault; }
            set { _isDefault = value; }
        }
    }
}
