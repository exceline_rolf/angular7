﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ScheduleManagement
{
    [DataContract]
    public class ExtendedTaskTemplateDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _title = string.Empty;
        [DataMember]
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private CommonUITypes _fieldType;
        [DataMember]
        public CommonUITypes FieldType
        {
            get { return _fieldType; }
            set { _fieldType = value; }
        }

        private int _templateId = -1;
        [DataMember]
        public int TemplateId
        {
            get { return _templateId; }
            set { _templateId = value; }
        }

        private bool _isDeleted = false;
        [DataMember]
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }
 

        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        private DateTime _lastModifiedDate;
        [DataMember]
        public DateTime LastModifiedDate
        {
            get
            {
                return _lastModifiedDate;
            }
            set
            {
                _lastModifiedDate = value;
            }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get
            {
                return _createdUser;
            }
            set
            {
                _createdUser = value;
            }
        }

        private string _lastModifiedUser = string.Empty;
        [DataMember]
        public string LastModifiedUser
        {
            get
            {
                return _lastModifiedUser;
            }
            set
            {
                _lastModifiedUser = value;
            }
        }
    }
}
