﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Collections.ObjectModel;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class ExcelineJobCategoryDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private bool _isDay = false;
        [DataMember]
        public bool IsDay
        {
            get { return _isDay; }
            set { _isDay = value; }
        }

        private bool _isStartDate = false;
        [DataMember]
        public bool IsStartDate
        {
            get { return _isStartDate; }
            set { _isStartDate = value; }
        }
        private bool _isEndDate = false;
        [DataMember]
        public bool IsEndDate
        {
            get { return _isEndDate; }
            set { _isEndDate = value; }
        }

        private bool _isStartTime = false;
        [DataMember]
        public bool IsStartTime
        {
            get { return _isStartTime; }
            set { _isStartTime = value; }
        }

        private bool _isEndTime = false;
        [DataMember]
        public bool IsEndTime
        {
            get { return _isEndTime; }
            set { _isEndTime = value; }
        }

        private bool _isRole = false;
        [DataMember]
        public bool IsRole
        {
            get { return _isRole; }
            set { _isRole = value; }
        }
        [DataMember]
        public string VisibilityRole
        {
            get
            {
                if (_isRole)
                {
                    return "Visible";
                }
                else
                {
                    return "Collapsed";
                }
            }
            set { }
        }
        private bool _isEmployee = false;
        [DataMember]
        public bool IsEmployee
        {
            get { return _isEmployee; }
            set { _isEmployee = value; }
        }
        [DataMember]
        public string VisibilityEmployee
        {
            get
            {
                if (_isEmployee)
                {
                    return "Visible";
                }
                else
                {
                    return "Collapsed";
                }
            }
            set { }
        }
        private bool _isWeekType = false;
        [DataMember]
        public bool IsWeekType
        {
            get { return _isWeekType; }
            set { _isWeekType = value; }
        }

        private List<ExtendedFieldDC> _extendedFieldsList = new List<ExtendedFieldDC>();
        [DataMember]
        public List<ExtendedFieldDC> ExtendedFieldsList
        {
            get { return _extendedFieldsList; }
            set { _extendedFieldsList = value; }
        }

        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        private DateTime _lastModifiedDate;
        [DataMember]
        public DateTime LastModifiedDate
        {
            get
            {
                return _lastModifiedDate;
            }
            set
            {
                _lastModifiedDate = value;
            }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get
            {
                return _createdUser;
            }
            set
            {
                _createdUser = value;
            }
        }

        private string _lastModifiedUser = string.Empty;
        [DataMember]
        public string LastModifiedUser
        {
            get
            {
                return _lastModifiedUser;
            }
            set
            {
                _lastModifiedUser = value;
            }
        }

        private bool _activeStatus;
        [DataMember]
        public bool ActiveStatus
        {
            get
            {
                return _activeStatus;
            }
            set
            {
                _activeStatus = value;
            }
        }
        private bool _isSelectedJobCategory = false;
        [DataMember]
        public bool IsSelectedJobCategory
        {
            get { return _isSelectedJobCategory; }
            set { _isSelectedJobCategory = value; }
        }
    }
}
