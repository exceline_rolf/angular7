﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class FollowUpTemplateDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _categoryId = -1;
        [DataMember]
        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }

        private CategoryDC _category;
        [DataMember]
        public CategoryDC Category
        {
            get { return _category; }
            set { _category = value; }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private DateTime _createdDate = DateTime.Now;
        [DataMember]
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        private bool _isDefault;
        [DataMember]
        public bool IsDefault
        {
            get { return _isDefault; }
            set { _isDefault = value; }
        }

        private List<FollowUpTemplateTaskDC> _followUpTaskList = new List<FollowUpTemplateTaskDC>();
        [DataMember]
        public List<FollowUpTemplateTaskDC> FollowUpTaskList
        {
            get { return _followUpTaskList; }
            set { _followUpTaskList = value; }
        }
    }
}
