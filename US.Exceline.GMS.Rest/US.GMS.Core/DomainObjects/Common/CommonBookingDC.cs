﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class CommonBookingDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }


        private int _activeTimeId;
        [DataMember]
        public int ActiveTimeId
        {
            get { return _activeTimeId; }
            set { _activeTimeId = value; }
        }

        private int _bookingEntityId;
        [DataMember]
        public int BookingEntityId
        {
            get { return _bookingEntityId; }
            set { _bookingEntityId = value; }
        }

        private string _bookingEntityType;
        [DataMember]
        public string BookingEntityType
        {
            get { return _bookingEntityType; }
            set { _bookingEntityType = value; }
        }

        private string _bookingEntityName;
        [DataMember]
        public string BookingEntityName
        {
            get { return _bookingEntityName; }
            set { _bookingEntityName = value; }
        }

        private int _bookingForEntityId;
        [DataMember]
        public int BookingForEntityId
        {
            get { return _bookingForEntityId; }
            set { _bookingForEntityId = value; }
        }

        private string _bookingForEntityType;
        [DataMember]
        public string BookingForEntityType
        {
            get { return _bookingForEntityType; }
            set { _bookingForEntityType = value; }
        }

        private string _bookingForEntityName;
        [DataMember]
        public string BookingForEntityName
        {
            get { return _bookingForEntityName; }
            set { _bookingForEntityName = value; }
        }

        private DateTime _startDateTime;
        [DataMember]
        public DateTime StartDateTime
        {
            get { return _startDateTime; }
            set { _startDateTime = value; }
        }

        private DateTime _endDateTime;
        [DataMember]
        public DateTime EndDateTime
        {
            get { return _endDateTime; }
            set { _endDateTime = value; }
        }

        private int _associatedArticleId;
        [DataMember]
        public int AssociatedArticleId
        {
            get { return _associatedArticleId; }
            set { _associatedArticleId = value; }
        }

        private string _associatedArticleText;
        [DataMember]
        public string AssociatedArticleText
        {
            get { return _associatedArticleText; }
            set { _associatedArticleText = value; }
        }

        private double _bookingUnits;
        [DataMember]
        public double BookingUnits
        {
            get { return _bookingUnits; }
            set { _bookingUnits = value; }
        }

        private string _unitType;
        [DataMember]
        public string UnitType
        {
            get { return _unitType; }
            set { _unitType = value; }
        }

        private decimal _unitPrice;
        [DataMember]
        public decimal UnitPrice
        {
            get { return _unitPrice; }
            set { _unitPrice = value; }
        }

        private decimal _unitPriceTotal;
        [DataMember]
        public decimal UnitPriceTotal
        {
            get { return _unitPriceTotal; }
            set { _unitPriceTotal = value; }
        }

        private decimal _bookingAmount;
        [DataMember]
        public decimal BookingAmount
        {
            get { return _bookingAmount; }
            set { _bookingAmount = value; }
        }

        private decimal _reservedUnits;
        [DataMember]
        public decimal ReservedUnits
        {
            get { return _reservedUnits; }
            set { _reservedUnits = value; }
        }

        private bool _useReserve;
        [DataMember]
        public bool UseReserve
        {
            get { return _useReserve; }
            set { _useReserve = value; }
        }

        private List<BookingArticleDC> _bookingArticles;
        [DataMember]
        public List<BookingArticleDC> BookingArticles
        {
            get { return _bookingArticles; }
            set { _bookingArticles = value; }
        }

        private int _bookingActivityId;
        [DataMember]
        public int BookingActivityId
        {
            get { return _bookingActivityId; }
            set { _bookingActivityId = value; }
        }

    }
}
