﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class ActivityTimeDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _activityId = -1;
        [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        private string _activityName = string.Empty;
        [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        private DateTime? _startdate;
        [DataMember]
        public DateTime? Startdate
        {
            get { return _startdate; }
            set { _startdate = value; }
        }

        private DateTime? _endDate;
        [DataMember]
        public DateTime? EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private DateTime? _startTime;
        [DataMember]
        public DateTime? StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private DateTime? _endTime;
        [DataMember]
        public DateTime? EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private bool _edited = false;
        [DataMember]
        public bool Edited
        {
            get { return _edited; }
            set { _edited = value; }
        }

        private string _reason = string.Empty;
        [DataMember]
        public string Reason
        {
            get { return _reason; }
            set { _reason = value; }
        }
    }
}
