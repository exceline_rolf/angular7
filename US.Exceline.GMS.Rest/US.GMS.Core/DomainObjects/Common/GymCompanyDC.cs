﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class GymCompanyDC
    {
        private string _gymCode = string.Empty;
        [DataMember]
        public string GymCode
        {
            get { return _gymCode; }
            set { _gymCode = value; }
        }

        private string _companyName = string.Empty;
        [DataMember]
        public string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        private int _creditorNo = -1;
        [DataMember]
        public int CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }
    }
}
