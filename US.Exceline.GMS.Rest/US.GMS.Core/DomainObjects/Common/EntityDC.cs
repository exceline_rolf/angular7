﻿
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class EntityDC
    {
        private int _id = -1;
        private string _displayName = string.Empty;
        private string _basedOn = string.Empty;
        
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember]
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        [DataMember]
        public string BasedOn
        {
            get { return _basedOn; }
            set { _basedOn = value; }
        }
    }
}
