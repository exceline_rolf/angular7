﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class ExcelineJobDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private int _jobCategoryId = -1;
        [DataMember]
        public int JobCategoryId
        {
            get { return _jobCategoryId; }
            set { _jobCategoryId = value; }
        }

        private string _jobCategoryName = string.Empty;
        [DataMember]
        public string JobCategoryName
        {
            get { return _jobCategoryName; }
            set { _jobCategoryName = value; }
        }

        private string _title;
        [DataMember]
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private int _assignTo = -1;
        [DataMember]
        public int AssignTo
        {
            get { return _assignTo; }
            set { _assignTo = value; }
        }

        private string _employeeName = string.Empty;
        [DataMember]
        public string EmployeeName
        {
            get { return _employeeName; }
            set { _employeeName = value; }
        }

        private string _assigntoName = string.Empty;
        [DataMember]
        public string AssigntoName
        {
            get { return _assigntoName; }
            set { _assigntoName = value; }
        }

        private string _assignEntityRole = string.Empty;
        [DataMember]
        public string AssignEntityRole
        {
            get { return _assignEntityRole; }
            set { _assignEntityRole = value; }
        }

        private int _belongsToMemberId = -1;
        [DataMember]
        public int BelongsToMemberId
        {
            get { return _belongsToMemberId; }
            set { _belongsToMemberId = value; }
        }

        private string _followupMemName = string.Empty;
        [DataMember]
        public string FollowupMemName
        {
            get { return _followupMemName; }
            set { _followupMemName = value; }
        }

        private string _belongsToMemName = string.Empty;
        [DataMember]
        public string BelongsToMemName
        {
            get { return _belongsToMemName; }
            set { _belongsToMemName = value; }
        }

        private DateTime _dueDate = DateTime.Now;
        [DataMember]
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        private DateTime _dueTime = DateTime.Now;
        [DataMember]
        public DateTime DueTime
        {
            get { return _dueTime; }
            set { _dueTime = value; }
        }
        

        private decimal  _estimatedTime;
        [DataMember]
        public decimal EstimatedTime
        {
            get { return _estimatedTime; }
            set { _estimatedTime = value; }
        }

        private string _description;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private int _numOfDays = 0;
        [DataMember]
        public int NumOfDays
        {
            get { return _numOfDays; }
            set { _numOfDays = value; }
        }

        private bool _isPopup = false;
        [DataMember]
        public bool IsPopup
        {
            get { return _isPopup; }
            set { _isPopup = value; }
        }

        private DateTime _foundDate = DateTime.Now;
        [DataMember]
        public DateTime FoundDate
        {
            get { return _foundDate; }
            set { _foundDate = value; }
        }

        private DateTime _returnDate = DateTime.Now;
        [DataMember]
        public DateTime ReturnDate
        {
            get { return _returnDate; }
            set { _returnDate = value; }
        }

        private string _phoneNo = string.Empty;
        [DataMember]
        public string PhoneNo
        {
            get { return _phoneNo; }
            set { _phoneNo = value; }
        }

        private string _sms = string.Empty;
        [DataMember]
        public string Sms
        {
            get { return _sms; }
            set { _sms = value; }
        }

        private string _entityRoleType;
        [DataMember]
        public string EntityRoleType
        {
            get { return _entityRoleType; }
            set { _entityRoleType = value; }
        }

        private List<TaskExtendedFieldDC> _extendedFieldsList = new List<TaskExtendedFieldDC>();
        [DataMember]
        public List<TaskExtendedFieldDC> ExtendedFieldsList
        {
            get { return _extendedFieldsList; }
            set { _extendedFieldsList = value; }
        }

        private bool _activeStatus = false;
        [DataMember]
        public bool ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private bool _isShowInCalandar = false;
        [DataMember]
        public bool IsShowInCalandar
        {
            get { return _isShowInCalandar; }
            set { _isShowInCalandar = value; }
        }

        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        private DateTime _lastModifiedDate;
        [DataMember]
        public DateTime LastModifiedDate
        {
            get
            {
                return _lastModifiedDate;
            }
            set
            {
                _lastModifiedDate = value;
            }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get
            {
                return _createdUser;
            }
            set
            {
                _createdUser = value;
            }
        }

        private string _lastModifiedUser = string.Empty;
        [DataMember]
        public string LastModifiedUser
        {
            get
            {
                return _lastModifiedUser;
            }
            set
            {
                _lastModifiedUser = value;
            }
        }

        private decimal _totalTimeSpent;
        [DataMember]
        public decimal TotalTimeSpent
        {
            get { return _totalTimeSpent; }
            set { _totalTimeSpent = value; }
        }


        private string _notification;
        [DataMember]
        public string Notification
        {
            get { return _notification; }
            set { _notification = value; }
        }

    }
}
