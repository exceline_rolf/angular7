﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/21/2012 12:31:19
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class BookingPaymentDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _paymentTypeId;
        [DataMember]
        public int PaymentTypeId
        {
            get { return _paymentTypeId; }
            set { _paymentTypeId = value; }
        }

        private string _paymentType;
        [DataMember]
        public string PaymentType
        {
            get { return _paymentType; }
            set { _paymentType = value; }
        }

        private int _memberId;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private int _bookingId;
        [DataMember]
        public int BookingId
        {
            get { return _bookingId; }
            set { _bookingId = value; }
        }

        private decimal _amount;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

    }
}
