﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "4/17/2012 6:22:25 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class CategoryTypeDC
    {
        private int _id = -1;

        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _code = String.Empty;

        [DataMember]
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        private string _description = String.Empty;

        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _name=String.Empty;

        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

    }
}
