﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "4/18/2012 1:10:16 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class CategoryDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int typeId = -1;
        [DataMember]
        public int TypeId
        {
            get { return typeId; }
            set { typeId = value; }
        }

        private string _typeName = string.Empty;
        [DataMember]
        public string TypeName
        {
            get { return _typeName; }
            set { _typeName = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _code = string.Empty;
        [DataMember]
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        private string _categoryTypeCode = string.Empty;
        [DataMember]
        public string CategoryTypeCode
        {
            get { return _categoryTypeCode; }
            set { _categoryTypeCode = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private byte[] _image;
        [DataMember]
        public byte[] CategoryImage
        {

            get { return _image; }
            set { _image = value; }
        }

        private DateTime _createdDate = DateTime.Now;
        [DataMember]
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        private DateTime _lastModifiedDate = DateTime.Now;
        [DataMember]
        public DateTime LastModifiedDate
        {
            get { return _lastModifiedDate; }
            set { _lastModifiedDate = value; }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private string _lastModifiedUser = string.Empty;
        [DataMember]
        public string LastModifiedUser
        {
            get { return _lastModifiedUser; }
            set { _lastModifiedUser = value; }
        }

        private bool _activeStatus = false;
        [DataMember]
        public bool ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private bool _isCheck = false;
        [DataMember]
        public bool IsCheck
        {
            get { return _isCheck; }
            set { _isCheck = value; }
        }

        private string _color = string.Empty;
        [DataMember]
        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }
    }
}
