﻿using System;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class TrainingProgramClassVisitDetailDC
    {
        private int _classNo;
        [DataMember]
        public int ClassNo
        {
            get { return _classNo; }
            set { _classNo = value; }
        }

        private DateTime _startDateTime;
        [DataMember]
        public DateTime StartDateTime
        {
            get { return _startDateTime; }
            set { _startDateTime = value; }
        }

        private int _visitId;
        [DataMember]
        public int VisitId
        {
            get { return _visitId; }
            set { _visitId = value; }
        }


        private int _duration;
        [DataMember]
        public int Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        private DateTime _currentUpdatedDateTime;
        [DataMember]
        public DateTime CurrentUpdatedDateTime
        {
            get { return _currentUpdatedDateTime; }
            set { _currentUpdatedDateTime = value; }
        }

        private MemberIntegrationSettingDC _memberIntegrationSetting;
        [DataMember]
        public MemberIntegrationSettingDC MemberIntegrationSetting
        {
            get { return _memberIntegrationSetting; }
            set { _memberIntegrationSetting = value; }
        }
    }
}
