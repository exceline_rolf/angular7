﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    public class ExcelineContractDC
    {
        private int _id = 0;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _memberContractNo = string.Empty;
        [DataMember]
        public string MemberContractNo
        {
            get { return _memberContractNo; }
            set { _memberContractNo = value; }
        }

        private string _memberCustId = string.Empty;
        [DataMember]
        public string MemberCustId
        {
            get { return _memberCustId; }
            set { _memberCustId = value; }
        }


        private string _memberName = string.Empty;
        [DataMember]
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        private DateTime _contractEndDate = DateTime.Now;
        [DataMember]
        public DateTime ContractEndDate
        {
            get { return _contractEndDate; }
            set { _contractEndDate = value; }
        }

        private decimal _contractPrize = 0;
        [DataMember]
        public decimal ContractPrize
        {
            get { return _contractPrize; }
            set
            {
                _contractPrize = value;
            }
        }

        private string _templateNo = string.Empty;
        [DataMember]
        public string TemplateNo
        {
            get { return _templateNo; }
            set { _templateNo = value; }
        }

        private string _activityName = string.Empty;
        [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        private string _branchName;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }
    }
}
