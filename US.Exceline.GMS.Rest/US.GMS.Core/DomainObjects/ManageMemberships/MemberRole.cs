﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    public enum MemberRole
    {
        COM,
        MEM,
        INS,
        EMP,
        GRP,
        CON,
        DEB,
        CRE,
        TRA,
        NONE,
        OTHER,
        ADMIN,
        SPO,
        ALL
    }
}
