﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "7/10/2012 10:47:12 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class MemberShopAccountDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _memberId;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private string _memberName;
        [DataMember]
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        private decimal _balance;
        [DataMember]
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }
               
        private DateTime? _decativateDate;
        [DataMember]
        public DateTime? DecativateDate
        {
            get { return _decativateDate; }
            set { _decativateDate = value; }
        }

        private string _deactivateReason;
        [DataMember]
        public string DeactivateReason
        {
            get { return _deactivateReason; }
            set { _deactivateReason = value; }
        }

        private bool _activeStatus;
        [DataMember]
        public bool ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private DateTime _createdDateTime;
        [DataMember]
        public DateTime CreatedDateTime
        {
            get { return _createdDateTime; }
            set { _createdDateTime = value; }
        }

        private string _createdUser;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private DateTime? _lastModifiedDateTime;
        [DataMember]
        public DateTime? LastModifiedDateTime
        {
            get { return _lastModifiedDateTime; }
            set { _lastModifiedDateTime = value; }
        }

        private string _lastModifiedUser;
        [DataMember]
        public string LastModifiedUser
        {
            get { return _lastModifiedUser; }
            set { _lastModifiedUser = value; }
        }

        private List<MemberShopAccountItemDC> _creditaccountItemList = new List<MemberShopAccountItemDC>();
        [DataMember]
        public List<MemberShopAccountItemDC> CreditaccountItemList
        {
            get { return _creditaccountItemList; }
            set { _creditaccountItemList = value; }
        }

        private List<MemberShopAccountItemDC> _debitaccountItemList = new List<MemberShopAccountItemDC>();
        [DataMember]
        public List<MemberShopAccountItemDC> DebitaccountItemList
        {
            get { return _debitaccountItemList; }
            set { _debitaccountItemList = value; }
        }

        private decimal _prepaidBalance = 0;
         [DataMember]
        public decimal PrepaidBalance
        {
            get { return _prepaidBalance; }
            set { _prepaidBalance = value; }
        }

         private int _memberBranchID = -1;
         [DataMember]
         public int MemberBranchID
         {
             get { return _memberBranchID; }
             set { _memberBranchID = value; }
         }

        private int _salePointId = -1;
        [DataMember]
        public int SalePointId
        {
            get { return _salePointId; }
            set { _salePointId = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchID
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _mode;
        [DataMember]
        public string Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }
    }
}
