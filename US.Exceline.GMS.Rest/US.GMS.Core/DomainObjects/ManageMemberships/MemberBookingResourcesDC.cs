﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class MemberBookingResourcesDC
    {
        private int _sheduleIdItemId = -1;
        [DataMember]
        public int SheduleItemId
        {
            get { return _sheduleIdItemId; }
            set { _sheduleIdItemId = value; }
        }

        private int _resourceId = -1;
        [DataMember]
        public int ResourceId
        {
            get { return _resourceId; }
            set { _resourceId = value; }
        }

        private string _resourceName = string.Empty;
        [DataMember]
        public string ResourceName
        {
            get { return _resourceName; }
            set { _resourceName = value; }
        }

    }
}
