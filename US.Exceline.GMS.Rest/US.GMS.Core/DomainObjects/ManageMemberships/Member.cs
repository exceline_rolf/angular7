﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    public abstract class Member
    {
        public abstract string FirstName { get; set; }
        public abstract string LastName { get; set; }
        public abstract Gender Gender { get; set; }
        public abstract DateTime BirthDate { get; set; }
        public abstract int Age { get; set; }
        public abstract int EntNo { get; set; }
        public abstract string Address { get; set; }
        public abstract string PostCode { get; set; }
        public abstract string PostPlace { get; set; }
        public abstract string Email { get; set; }
        public abstract string Mobile { get; set; }
        public abstract string WorkTeleNo { get; set; }
        public abstract string PrivateTeleNo { get; set; }
        public abstract string Department { get; set; }
        public abstract string Instructor { get; set; }
        public abstract string AccountNo { get; set; }
        public abstract DateTime ContractStartDate { get; set; }
        public abstract DateTime ContractEndDate { get; set; }
        public abstract string CompanyName { get; set; }
        public abstract string EmployeeNo { get; set; }
        public abstract int BranchId { get; set; }
        //Sponsor and Sponsorship
        public abstract byte[] ProfilePicture { get; set; }
    }
}
