﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class MemberSMSNotificationDC
    {
        private DateTime _sentDateTime;
        [DataMember]
        public DateTime SentDateTime
        {
            get { return _sentDateTime; }
            set { _sentDateTime = value; }
        }

        private DateTime _createdDateTime;
        [DataMember]
        public DateTime CreatedDateTime
        {
            get { return _createdDateTime; }
            set { _createdDateTime = value; }
        }

        private string _category = string.Empty;
        [DataMember]
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private string _content = string.Empty;
        [DataMember]
        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }

        private String _status = string.Empty;
        [DataMember]
        public String Status
        {
            get { return _status; }
            set { _status = value; }
        }
    }
}
