﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class MemberBookingDC
    {
        private string _number = string.Empty;
        [DataMember]
        public string Number
        {
            get { return _number; }
            set { _number = value; }
        }

        private string _cancelNumber = string.Empty;
        [DataMember]
        public string CancelNumber
        {
            get { return _cancelNumber; }
            set { _cancelNumber = value; }
        }

        private int _scheduleItemId;
        [DataMember]
        public int ScheduleItemId
        {
            get { return _scheduleItemId; }
            set { _scheduleItemId = value; }
        }

        private DateTime _date = new DateTime();
        [DataMember]
        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private DateTime _startTime = new DateTime();
        [DataMember]
        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private DateTime _endTime = new DateTime();
        [DataMember]
        public DateTime EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }        


        private string _resourceName = string.Empty;
        [DataMember]
        public string ResourceName
        {
            get { return _resourceName; }
            set { _resourceName = value; }
        }

        private string _otherResource = string.Empty;
        [DataMember]
        public string OtherResource
        {
            get { return _otherResource; }
            set { _otherResource = value; }
        }

        private string _activityName = string.Empty;
        [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        private string _categoryName = string.Empty;
        [DataMember]
        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }
        
        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private string _status = string.Empty;
        [DataMember]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private List<MemberBookingResourcesDC> _memberBookingResources = new List<MemberBookingResourcesDC>();
        [DataMember]
        public List<MemberBookingResourcesDC> MemberBookingResources
        {
            get { return _memberBookingResources; }
            set { _memberBookingResources = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private string _article = string.Empty;
        [DataMember]
        public string Article
        {
            get { return _article; }
            set { _article = value; }
        }

        private bool _isSmsRemindere;
        [DataMember]
        public bool IsSmsRemindered
        {
            get { return _isSmsRemindere; }
            set { _isSmsRemindere = value; }
        }

        private bool _isTask;
        [DataMember]
        public bool IsTask
        {
            get { return _isTask; }
            set { _isTask = value; }
        }

        private string _memberContractNo = string.Empty;
         [DataMember]
        public string MemberContractNo
        {
            get { return _memberContractNo; }
            set { _memberContractNo = value; }
        }


        private int _referenceId;
        [DataMember]
        public int ReferenceId
        {
            get { return _referenceId; }
            set { _referenceId = value; }
        }

        private bool _activeStatus;
        [DataMember]
        public bool ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private int _activeTimeId;
        [DataMember]
        public int ActiveTimeId
        {
            get { return _activeTimeId; }
            set { _activeTimeId = value; }
        }

        private bool _isError;
        [DataMember]
        public bool IsError
        {
            get { return _isError; }
            set { _isError = value; }
        }

        private DateTime? _arrivalDateTime;
        [DataMember]
        public DateTime? ArrivalDateTime
        {
            get { return _arrivalDateTime; }
            set { _arrivalDateTime = value; }
        }

      
    }
}
