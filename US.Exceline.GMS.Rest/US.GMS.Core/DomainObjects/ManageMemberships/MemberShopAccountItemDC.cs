﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "7/10/2012 10:47:07 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class MemberShopAccountItemDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _shopAccountId;
        [DataMember]
        public int ShopAccountId
        {
            get { return _shopAccountId; }
            set { _shopAccountId = value; }
        }

        private decimal? _paymentAmount;
        [DataMember]
        public decimal? PaymentAmount
        {
            get { return _paymentAmount; }
            set { _paymentAmount = value; }
        }

        private int _payModeId = -1;
        [DataMember]
        public int PayModeId
        {
            get { return _payModeId; }
            set { _payModeId = value; }
        }

        private string _payMode;
        [DataMember]
        public string PayMode
        {
            get { return _payMode; }
            set { _payMode = value; }
        }

        private DateTime _createdDateTime;
        [DataMember]
        public DateTime CreatedDateTime
        {
            get { return _createdDateTime; }
            set { _createdDateTime = value; }
        }

        private string _createdUser;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private string _type;
        [DataMember]
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private string _invoiceNo = string.Empty;
        [DataMember]
        public string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }    
    }
}
