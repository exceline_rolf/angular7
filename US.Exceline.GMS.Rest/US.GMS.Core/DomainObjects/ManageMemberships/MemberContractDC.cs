﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "4/02/2012 11:41:40 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class MemberContractDC
    {

        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _memberContractNo = string.Empty;
        [DataMember]
        public string MemberContractNo
        {
            get { return _memberContractNo; }
            set { _memberContractNo = value; }
        }

       
        private DateTime? _renewEffectiveDate;
        [DataMember]
        public DateTime? RenewEffectiveDate
        {
            get { return _renewEffectiveDate; }
            set { _renewEffectiveDate = value; }
        }

        private int _memberId = -1;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private int _sponserId = -1;
        [DataMember]
        public int SponserId
        {
            get { return _sponserId; }
            set { _sponserId = value; }
        }

        private int _employeeCategoryId = -1;
        [DataMember]
        public int EmployeeCategoryId
        {
            get { return _employeeCategoryId; }
            set { _employeeCategoryId = value; }
        }


        private int _sponserContractId = -1;
        [DataMember]
        public int SponserContractId
        {
            get { return _sponserContractId; }
            set { _sponserContractId = value; }
        }

        private string _memberName = string.Empty;
        [DataMember]
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        private string _memberCustId = string.Empty;
        [DataMember]
        public string MemberCustId
        {
            get { return _memberCustId; }
            set { _memberCustId = value; }
        }

        private string _employeeNo = string.Empty;
        [DataMember]
        public string EmployeeNo
        {
            get { return _employeeNo; }
            set { _employeeNo = value; }
        }

        private string _contractName = string.Empty;
        [DataMember]
        public string ContractName
        {
            get { return _contractName; }
            set { _contractName = value; }
        }


        private string _employeeRef = string.Empty;
        [DataMember]
        public string EmployeeReference
        {
            get { return _employeeRef; }
            set { _employeeRef = value; }
        }

        private int _contractId = -1;
        [DataMember]
        public int ContractId
        {
            get { return _contractId; }
            set { _contractId = value; }
        }

        private decimal _contractPrize = 0;
        [DataMember]
        public decimal ContractPrize
        {
            get { return _contractPrize; }
            set
            {
                _contractPrize = value;
            }
        }

        private decimal _enrollmentFee;
        [DataMember]
        public decimal EnrollmentFee
        {
            get { return _enrollmentFee; }
            set { _enrollmentFee = value; }
        }

        private decimal _amountPerInstallment;
        [DataMember]
        public decimal AmountPerInstallment
        {
            get { return _amountPerInstallment; }
            set { _amountPerInstallment = value; }
        }

        private decimal _remainingBalance;
        [DataMember]
        public decimal RemainingBalance
        {
            get { return _remainingBalance; }
            set { _remainingBalance = value; }
        }

        private string _AccountNo = string.Empty;
        [DataMember]
        public string AccountNo
        {
            get { return _AccountNo; }
            set { _AccountNo = value; }
        }

        private int _noOfInstallments = 0;
        [DataMember]
        public int NoOfInstallments
        {
            get { return _noOfInstallments; }
            set
            {
                _noOfInstallments = value;
            }
        }

        private int _numberOfVisits = -1;
        [DataMember]
        public int NumberOfVisits
        {
            get { return _numberOfVisits; }
            set { _numberOfVisits = value; }
        }

        private int _availableVisits = -1;
        [DataMember]
        public int AvailableVisits
        {
            get { return _availableVisits; }
            set { _availableVisits = value; }
        }

        private int _recordedVisits = -1;
        [DataMember]
        public int RecordedVisits
        {
            get { return _recordedVisits; }
            set { _recordedVisits = value; }
        }

        private DateTime _contractStartDate = DateTime.Now;
        [DataMember]
        public DateTime ContractStartDate
        {
            get { return _contractStartDate; }
            set { _contractStartDate = value; }
        }

        private DateTime _contractEndDate = DateTime.Now;
        [DataMember]
        public DateTime ContractEndDate
        {
            get { return _contractEndDate; }
            set { _contractEndDate = value; }
        }

        private int _trainerId = -1;
        [DataMember]
        public int TrainerId
        {
            get { return _trainerId; }
            set { _trainerId = value; }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private int _branchId = 1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        private DateTime _contractValidTime;
        [DataMember]
        public DateTime ContractValidTime
        {
            get { return _contractValidTime; }
            set { _contractValidTime = value; }
        }

        private DateTime _fromTime;
        [DataMember]
        public DateTime FromTime
        {
            get { return _fromTime; }
            set { _fromTime = value; }
        }

        private DateTime _toTime;
        [DataMember]
        public DateTime ToTime
        {
            get { return _toTime; }
            set { _toTime = value; }
        }

        private DateTime _notFromTime;
        [DataMember]
        public DateTime NotFromTime
        {
            get { return _notFromTime; }
            set { _notFromTime = value; }
        }

        private List<InstallmentDC> _installlmentList = new List<InstallmentDC>();
        [DataMember]
        public List<InstallmentDC> InstalllmentList
        {
            get { return _installlmentList; }
            set { _installlmentList = value; }
        }

        private string _reasonToStart = string.Empty;
        [DataMember]
        public string ReasonToStart
        {
            get { return _reasonToStart; }
            set { _reasonToStart = value; }
        }

        private bool _isDeductFromBank = false;
        [DataMember]
        public bool IsDeductFromBank
        {
            get { return _isDeductFromBank; }
            set { _isDeductFromBank = value; }
        }

        private bool _isFreezed = false;
        [DataMember]
        public bool IsFreezed
        {
            get { return _isFreezed; }
            set { _isFreezed = value; }
        }

        private string _freezedBy = string.Empty;
        [DataMember]
        public string FreezedBy
        {
            get { return _freezedBy; }
            set { _freezedBy = value; }
        }

        private decimal _totalSum = 0;
        [DataMember]
        public decimal TotalSum
        {
            get { return _totalSum; }
            set { _totalSum = value; }
        }

        private DateTime? _renewedDate;
        [DataMember]
        public DateTime? RenewedDate
        {
            get { return _renewedDate; }
            set { _renewedDate = value; }
        }

        private int _freezedCategoryId = -1;
        [DataMember]
        public int FreezedCategoryId
        {
            get { return _freezedCategoryId; }
            set { _freezedCategoryId = value; }
        }

        private int _activityId = -1;
        [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        private bool _isATG = false;
        [DataMember]
        public bool IsATG
        {
            get { return _isATG; }
            set { _isATG = value; }
        }

        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _activityName = string.Empty;
        [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        private string _contractDescription = string.Empty;
        [DataMember]
        public string ContractDescription
        {
            get { return _contractDescription; }
            set { _contractDescription = value; }
        }

        private List<ContractItemDC> _startUpItemList = new List<ContractItemDC>();
        [DataMember]
        public List<ContractItemDC> StartUpItemList
        {
            get { return _startUpItemList; }
            set { _startUpItemList = value; }
        }

        private List<ContractItemDC> _everyMonthItemList = new List<ContractItemDC>();
        [DataMember]
        public List<ContractItemDC> EveryMonthItemList
        {
            get { return _everyMonthItemList; }
            set { _everyMonthItemList = value; }
        }

        private int _memberFeeArticleID = -1;
        [DataMember]
        public int MemberFeeArticleID
        {
            get { return _memberFeeArticleID; }
            set { _memberFeeArticleID = value; }
        }

        private string _memberFeeArticleName = string.Empty;
        [DataMember]
        public string MemberFeeArticleName
        {
            get { return _memberFeeArticleName; }
            set { _memberFeeArticleName = value; }
        }

        private int _memberFeeMonth = -1;
        [DataMember]
        public int MemberFeeMonth
        {
            get { return _memberFeeMonth; }
            set { _memberFeeMonth = value; }
        }
        

        private DateTime ? _memFeeLastInvoicedDate = null;
        [DataMember]
        public DateTime ? MemFeeLastInvoicedDate
        {
            get { return _memFeeLastInvoicedDate; }
            set { _memFeeLastInvoicedDate = value; }
        }

        private int _ContractTypeId = -1;
        [DataMember]
        public int ContractTypeId
        {
            get { return _ContractTypeId; }
            set { _ContractTypeId = value; }
        }

        private string _contractTypeName = string.Empty;
        [DataMember]
        public string ContractTypeName
        {
            get { return _contractTypeName; }
            set { _contractTypeName = value; }
        }

        private int _extracActivityId = -1;
        [DataMember]
        public int ExtracActivityId
        {
            get { return _extracActivityId; }
            set { _extracActivityId = value; }
        }

        private string _extraActivityName = string.Empty;
        [DataMember]
        public string ExtraActivityName
        {
            get { return _extraActivityName; }
            set { _extraActivityName = value; }
        }

        private int _settingId = -1;
        [DataMember]
        public int SettingId
        {
            get { return _settingId; }
            set { _settingId = value; }
        }

        private string _status;
        [DataMember]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private int _articleId;
        [DataMember]
        public int ArticleId
        {
            get { return _articleId; }
            set { _articleId = value; }
        }

        private string _articleName;
        [DataMember]
        public string ArticleName
        {
            get { return _articleName; }
            set { _articleName = value; }
        }

        private bool isAssignContractForFamily;
        [DataMember]
        public bool IsAssignContractForFamily
        {
            get { return isAssignContractForFamily; }
            set { isAssignContractForFamily = value; }
        }

        private string _notification;
        [DataMember]
        public string Notification
        {
            get { return _notification; }
            set { _notification = value; }
        }

        private bool _isActive = false;
        [DataMember]
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        private int _lockInPeriod = 0;
        [DataMember]
        public int LockInPeriod
        {
            get { return _lockInPeriod; }
            set { _lockInPeriod = value; }
        }

        private DateTime? _lockInPeriodUntilDate;
        [DataMember]
        public DateTime? LockInPeriodUntilDate
        {
            get { return _lockInPeriodUntilDate; }
            set { _lockInPeriodUntilDate = value; }
        }

        private bool _restPlusMonth;
        [DataMember]
        public bool RestPlusMonth
        {
            get { return _restPlusMonth; }
            set { _restPlusMonth = value; }
        }

        private decimal _restPlusMonthAmount = 0;
        [DataMember]
        public decimal RestPlusMonthAmount
        {
            get { return _restPlusMonthAmount; }
            set { _restPlusMonthAmount = value; }
        }

        private int _guarantyPeriod = 0;
         [DataMember]
        public int GuarantyPeriod
        {
            get { return _guarantyPeriod; }
            set { _guarantyPeriod = value; }
        }

         private bool _isInvoiceDetails = false;
         [DataMember]
         public bool IsInvoiceDetails
         {
             get { return _isInvoiceDetails; }
             set { _isInvoiceDetails = value; }
         }


        private int _noOfMonths;
        [DataMember]
        public int NoOfMonths
        {
            get { return _noOfMonths; }
            set { _noOfMonths = value; }
        }

        private DateTime? _fixDateOfContract;
        [DataMember]
        public DateTime? FixDateOfContract
        {
            get { return _fixDateOfContract; }
            set { _fixDateOfContract = value; }
        }
         private bool _AutoRenew = true;
         [DataMember]
         public bool AutoRenew
         {
             get { return _AutoRenew; }
             set { _AutoRenew = value; }
         }

         private int _accessProfileId = -1;
         [DataMember]
         public int AccessProfileId
         {
             get { return _accessProfileId; }
             set { _accessProfileId = value; }
         }

         private decimal _orderPrice = 0;
         [DataMember]
         public decimal OrderPrice
         {
             get { return _orderPrice; }
             set { _orderPrice = value; }
         }

         private decimal _startUpItemPrice = 0;
         [DataMember]
         public decimal StartUpItemPrice
         {
             get { return _startUpItemPrice; }
             set { _startUpItemPrice = value; }
         }

         private decimal _everyMonthItemPrice = 0;
         [DataMember]
         public decimal EveryMonthItemPrice
         {
             get { return _everyMonthItemPrice; }
             set { _everyMonthItemPrice = value; }
         }

         private string _templateNo = string.Empty;
         [DataMember]
         public string TemplateNo
         {
             get { return _templateNo; }
             set { _templateNo = value; }
         }

         private DateTime? priceGuarantyUntillDate;
         [DataMember]
         public DateTime? PriceGuarantyUntillDate
         {
             get { return priceGuarantyUntillDate; }
             set { priceGuarantyUntillDate = value; }
         }

         private List<ContractBookingDC> _contractBookingList = new List<ContractBookingDC>();
         [DataMember]
         public List<ContractBookingDC> ContractBookingList
         {
             get { return _contractBookingList; }
             set { _contractBookingList = value; }
         }

         private decimal _bookingPrice = 0;
         [DataMember]
         public decimal BookingPrice
         {
             get { return _bookingPrice; }
             set { _bookingPrice = value; }
         }

         private List<int> _contractMemberList = new List<int>();
         [DataMember]
         public List<int> ContractMemberList
         {
             get { return _contractMemberList; }
             set { _contractMemberList = value; }
         }

         private DateTime? _firstDueDate;
         [DataMember]
         public DateTime? FirstDueDate
         {
             get { return _firstDueDate; }
             set { _firstDueDate = value; }
         }

         private string _contractCategoryName;
         [DataMember]
         public string ContractCategoryName
         {
             get { return _contractCategoryName; }
             set { _contractCategoryName = value; }
         }

         private int _categoryId = -1;
         [DataMember]
         public int CategoryId
         {
             get { return _categoryId; }
             set { _categoryId = value; }
         }

         private DateTime? _resignDate;
         [DataMember]
         public DateTime? ResignDate
         {
             get { return _resignDate; }
             set { _resignDate = value; }
         }

         private int _resignCategoryId = -1;
         [DataMember]
         public int ResignCategoryId
         {
             get { return _resignCategoryId; }
             set { _resignCategoryId = value; }
         }

         private string _resignCategory = string.Empty;
         [DataMember]
         public string ResignCategory
         {
             get { return _resignCategory; }
             set { _resignCategory = value; }
         }

         private int _creditoDueDays = 0;
         [DataMember]
         public int CreditoDueDays
         {
             get { return _creditoDueDays; }
             set { _creditoDueDays = value; }
         }

         private DateTime? _secondDueDate;
         [DataMember]
         public DateTime? SecondDueDate
         {
             get { return _secondDueDate; }
             set { _secondDueDate = value; }
         }

         private int _dueDateSetting = 0;
         [DataMember]
         public int DueDateSetting
         {
             get { return _dueDateSetting; }
             set { _dueDateSetting = value; }
         }

         private bool _useTodayAsDueDate = false;
         [DataMember]
         public bool UseTodayAsDueDate
         {
             get { return _useTodayAsDueDate; }
             set { _useTodayAsDueDate = value; }
         }

         private string _templateName = string.Empty;
         [DataMember]
         public string TemplateName
         {
             get { return _templateName; }
             set { _templateName = value; }
         }

         private string _nextTemplateNo = string.Empty;
         [DataMember]
         public string NextTemplateNo
         {
             get { return _nextTemplateNo; }
             set { _nextTemplateNo = value; }
         }

        private string _nextTemplateName = string.Empty;
        [DataMember]
        public string NextTemplateName
        {
            get { return _nextTemplateName; }
            set { _nextTemplateName = value; }
        }

        private int _nextTemplateId = -1;
        [DataMember]
        public int NextTemplateId
        {
            get { return _nextTemplateId; }
            set { _nextTemplateId = value; }
        }

        private int _noofDays = -1;
        [DataMember]
        public int NoofDays
        {
            get { return _noofDays; }
            set { _noofDays = value; }
        }

        private string _contractTypeCode = string.Empty;
        [DataMember]
        public string ContractTypeCode
        {
            get { return _contractTypeCode; }
            set { _contractTypeCode = value; }
        }

        private int _groupContractMemberCount = 0;
        [DataMember]
        public int GroupContractMemberCount
        {
            get { return _groupContractMemberCount; }
            set { _groupContractMemberCount = value; }
        }

        private bool _isMemberContract = false;
        [DataMember]
        public bool IsMemberContract
        {
            get { return _isMemberContract; }
            set { _isMemberContract = value; }
        }

        private bool _isSelected = false;
        [DataMember]
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private Dictionary<int, List<DateTime?>> _contractGroupList =new Dictionary<int, List<DateTime?>>();
         [DataMember]
        public Dictionary<int, List<DateTime?>> ContractGroupList
        {
            get { return _contractGroupList; }
            set { _contractGroupList = value; }
        }
         

        private int _signUpCategoryId = -1;
        [DataMember]
        public int SignUpCategoryId
        {
            get { return _signUpCategoryId; }
            set { _signUpCategoryId = value; }
        }

        private string _contractPDFFillParth = string.Empty;
        [DataMember]
        public string ContractPDFFillParth
        {
            get { return _contractPDFFillParth; }
            set { _contractPDFFillParth = value; }
        }

        private string _aTGPDFFillParth = string.Empty;
        [DataMember]
        public string ATGPDFFillParth
        {
            get { return _aTGPDFFillParth; }
            set { _aTGPDFFillParth = value; }
        }

        private int _nextTemplateIdOfTemplate = -1;
        [DataMember]
        public int NextTemplateIdOfTemplate
        {
            get { return _nextTemplateIdOfTemplate; }
            set { _nextTemplateIdOfTemplate = value; }
        }

        private int _renewedTemplateID = -1;
         [DataMember]
        public int RenewedTemplateID
        {
            get { return _renewedTemplateID; }
            set { _renewedTemplateID = value; }
        }

        private decimal _maxATGAmount = 0;
        [DataMember]
        public decimal MaxATGAmount
        {
            get { return _maxATGAmount; }
            set { _maxATGAmount = value; }
        }

        private DateTime? _closedDate;
        [DataMember]
        public DateTime? ClosedDate
        {
            get { return _closedDate; }
            set { _closedDate = value; }
        }

        private decimal _serviceAmount = 0;
        [DataMember]
        public decimal ServiceAmount
        {
            get { return _serviceAmount; }
            set { _serviceAmount = value; }
        }

        private int _fixedDueDay = -1;
        [DataMember]
        public int FixedDueDay
        {
            get { return _fixedDueDay; }
            set { _fixedDueDay = value; }
        }

        private bool _isBookingActivity = false;
        [DataMember]
        public bool IsBookingActivity
        {
            get { return _isBookingActivity; }
            set { _isBookingActivity = value; }
        }

        private bool _isGroup;
         [DataMember]
        public bool IsGroup
        {
            get { return _isGroup; }
            set { _isGroup = value; }
        }

         private List<ExcelineMemberDC> _groupMembers = new List<ExcelineMemberDC>();
         [DataMember]
         public List<ExcelineMemberDC> GroupMembers
         {
             get { return _groupMembers; }
             set { _groupMembers = value; }
         }

         private int _creditPeriod = 0;
         [DataMember]
         public int CreditPeriod
         {
             get { return _creditPeriod; }
             set { _creditPeriod = value; }
         }

         private int _memberCreditPeriod = 0;
         [DataMember]
         public int MemberCreditPeriod
         {
             get { return _memberCreditPeriod; }
             set { _memberCreditPeriod = value; }
         }

         private bool _postPay = false;
         [DataMember]
         public bool PostPay
         {
             get { return _postPay; }
             set { _postPay = value; }
         }

         private int _contractConditionId = -1;
         [DataMember]
         public int ContractConditionId
         {
             get { return _contractConditionId; }
             set { _contractConditionId = value; }
         }

         private string _contractCondition = string.Empty;
         [DataMember]
         public string ContractCondition
         {
             get { return _contractCondition; }
             set { _contractCondition = value; }
         }

         private int _brisID = -1;
         [DataMember]
         public int BrisID
         {
             get { return _brisID; }
             set { _brisID = value; }
         }

         private int _agressoID = -1;
         [DataMember]
         public int AgressoID
         {
             get { return _agressoID; }
             set { _agressoID = value; }
         }
    }
}
