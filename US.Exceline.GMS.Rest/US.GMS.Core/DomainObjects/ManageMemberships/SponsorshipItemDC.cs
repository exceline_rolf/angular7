﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
     [DataContract]
   public class SponsorshipItemDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _lastName = string.Empty;
        [DataMember]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private string _firstName = string.Empty;
        [DataMember]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
       
        private int _noOfVisit = 0;
        [DataMember]
        public int NoOfVisit
        {
            get { return _noOfVisit; }
            set { _noOfVisit = value; }
        }

        private string _cusId = string.Empty;
        [DataMember]
        public string CusId
        {
            get { return _cusId; }
            set { _cusId = value; }
        }


        private string _memberContractNo = string.Empty;
        [DataMember]
        public string MemberContractNo
        {
            get { return _memberContractNo; }
            set { _memberContractNo = value; }
        }

        private string _installmentText = string.Empty;
        [DataMember]
        public string InstallmentText
        {
            get { return _installmentText; }
            set { _installmentText = value; }
        }

        private decimal _amount = 0;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
    }
}
