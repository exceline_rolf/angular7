﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.Common;
using US.Common.Notification.Core.DomainObjects;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class OrdinaryMemberDC
    {
        private int _id = 0;
        [DataMember]
        public  int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _gATCardNo = string.Empty;
        [DataMember]
        public string GATCardNo
        {
            get { return _gATCardNo; }
            set { _gATCardNo = value; }
        }

        private string _firstName = string.Empty;
        [DataMember]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _employeeCategoryName = string.Empty;
        [DataMember]
        public string EmployeeCategoryName
        {
            get { return _employeeCategoryName; }
            set { _employeeCategoryName = value; }
        }

        private string _lastName = string.Empty;
        [DataMember]
        public  string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private Gender _gender = Gender.NONE;
        [DataMember]
        public Gender Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        private DateTime _birthDate;
        [DataMember]
        public DateTime BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }

        private int _entNo = 0;
        [DataMember]
        public int EntNo
        {
            get { return _entNo; }
            set { _entNo = value; }
        }

        private int _branchId = 0;
        [DataMember]
        public  int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private int _age = 0;
        [DataMember]
        public  int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        private string _address1 = string.Empty;
        [DataMember]
        public  string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }
        private string _address2 = string.Empty;
        [DataMember]
        public  string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }
        private string _address3 = string.Empty;
        [DataMember]
        public  string Address3
        {
            get { return _address3; }
            set { _address3 = value; }
        }

        private string _postCode = string.Empty;
        [DataMember]
        public string PostCode
        {
            get { return _postCode; }
            set { _postCode = value; }
        }

        private string _postPlace = string.Empty;
        [DataMember]
        public string PostPlace
        {
            get { return _postPlace; }
            set { _postPlace = value; }
        }

        private string _email = string.Empty;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _mobile = string.Empty;
        [DataMember]
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        private string _mobilePrefix = string.Empty;
        [DataMember]
        public string MobilePrefix
        {
            get { return _mobilePrefix; }
            set { _mobilePrefix = value; }
        }

        private string _workTeleNo = string.Empty;
        [DataMember]
        public string WorkTeleNo
        {
            get { return _workTeleNo; }
            set { _workTeleNo = value; }
        }

        private string _workTeleNoPrefix = string.Empty;
        [DataMember]
        public string WorkTeleNoPrefix
        {
            get { return _workTeleNoPrefix; }
            set { _workTeleNoPrefix = value; }
        }

        private string _privateTeleNo = string.Empty;
        [DataMember]
        public  string PrivateTeleNo
        {
            get { return _privateTeleNo; }
            set { _privateTeleNo = value; }
        }

        private string _privateTeleNoPrefix = string.Empty;
        [DataMember]
        public string PrivateTeleNoPrefix
        {
            get { return _privateTeleNoPrefix; }
            set { _privateTeleNoPrefix = value; }
        }

        private string _department = string.Empty;
        [DataMember]
        public  string Department
        {
            get { return _department; }
            set { _department = value; }
        }

        private string _instructor = string.Empty;
        [DataMember]
        public  string Instructor
        {
            get { return _instructor; }
            set { _instructor = value; }
        }

        private string _instructorCustId = string.Empty;
        [DataMember]
        public string InstructorCustId
        {
            get { return _instructorCustId; }
            set { _instructorCustId = value; }
        }

        private string _accountNo = string.Empty;
        [DataMember]
        public  string AccountNo
        {
            get { return _accountNo; }
            set { _accountNo = value; }
        }

        private DateTime _contractStartDate = DateTime.Now;
        [DataMember]
        public  DateTime ContractStartDate
        {
            get { return _contractStartDate; }
            set { _contractStartDate = value; }
        }

        private DateTime _contractEndDate = DateTime.Now;
        [DataMember]
        public  DateTime ContractEndDate
        {
            get { return _contractEndDate; }
            set { _contractEndDate = value; }
        }

        private string _companyName = string.Empty;
        [DataMember]
        public  string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        private string _employeeNo = string.Empty;
        [DataMember]
        public  string EmployeeNo
        {
            get { return _employeeNo; }
            set { _employeeNo = value; }
        }


        private byte[] _profilePicture = null;
        [DataMember]
        public  byte[] ProfilePicture
        {
            get { return _profilePicture; }
            set { _profilePicture = value; }
        }
        private string _personNo;
        [DataMember]
        public  string PersonNo
        {
            get { return _personNo; }
            set { _personNo = value; }
        }
        private string _createdUsser;
        [DataMember]
        public  string CreatedUser
        {
            get { return _createdUsser; }
            set { _createdUsser = value; }
        }
        private string _modifiedUser;
        [DataMember]
        public  string ModifiedUser
        {
            get { return _modifiedUser; }
            set { _modifiedUser = value; }
        }
        private bool _activeStatus = true;
        [DataMember]
        public  bool ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }
        private bool _isStartDateEnable = true;
        [DataMember]
        public bool IsStartDateEnable
        {
            get { return _isStartDateEnable; }
            set { _isStartDateEnable = value; }
        }

        private string _name;
        [DataMember]
        public  string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _imagePath = string.Empty;
        [DataMember]
        public  string ImagePath
        {
            get { return _imagePath; }
            set { _imagePath = value; }
        }

        private string _image = string.Empty;
        [DataMember]
        public  string Image
        {
            get { return _image; }
            set { _image = value; }
        }

        private bool _isSelected = false;
        [DataMember]
        public  bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
            }
        }
        private MemberRole _role = MemberRole.MEM;
        [DataMember]
        public MemberRole Role
        {
            get { return _role; }
            set { _role = value; }
        }


        private bool _isSponsor = false;
        [DataMember]
        public bool IsSponsor
        {
            get { return _isSponsor; }
            set { _isSponsor = value; }
        }


        private int _sponsoringId = -1;
        [DataMember]
        public int SponsoringId
        {
            get { return _sponsoringId; }
            set { _sponsoringId = value; }
        }



        private CategoryDC _category = new CategoryDC();

        [DataMember]
        public CategoryDC MemberCategory
        {
            get { return _category; }
            set { _category = value; }
        }

        private string _description = string.Empty;
        [DataMember]
        public  string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        private string _faxNo = string.Empty;
        [DataMember]
        public string FaxNo
        {
            get { return _faxNo; }
            set { _faxNo = value; }
        }
        private string _ref = string.Empty;
        [DataMember]
        public string Ref
        {
            get { return _ref; }
            set { _ref = value; }
        }
        private string _payerName = string.Empty;
        [DataMember]
        public string PayerName
        {
            get { return _payerName; }
            set { _payerName = value; }
        }

        private int _payerId;
        [DataMember]
        public int PayerId
        {
            get { return _payerId; }
            set { _payerId = value; }
        }

        private string _payerCustId = string.Empty;
        [DataMember]
        public string PayerCustId
        {
            get { return _payerCustId; }
            set { _payerCustId = value; }
        }

        private string _groupName = string.Empty;
        [DataMember]
        public string GroupName
        {
            get { return _groupName; }
            set { _groupName = value; }
        }

        private string _groupRoleId = string.Empty;
         [DataMember]
        public string GroupRoleId
        {
            get { return _groupRoleId; }
            set { _groupRoleId = value; }
        }

        private int _groupId = -1;
        [DataMember]
        public int GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }

        private int _introducedById = -1;
        [DataMember]
        public int IntroducedById
        {
            get { return _introducedById; }
            set { _introducedById = value; }
        }

        private string _introducedByName = string.Empty;
        [DataMember]
        public string IntroducedByName
        {
            get { return _introducedByName; }
            set { _introducedByName = value; }
        }

        private string _introducedByCustId = string.Empty;
        [DataMember]
        public string IntroducedByCustId
        {
            get { return _introducedByCustId; }
            set { _introducedByCustId = value; }
        }



        private List<ExcelineMemberDC> _groupMemberList = new List<ExcelineMemberDC>();
        [DataMember]
        public List<ExcelineMemberDC> GroupMemberList
        {
            get { return _groupMemberList; }
            set { _groupMemberList = value; }
        }

        private int _groupMemberLstCount = 0;
        [DataMember]
        public int GroupMemberLstCount
        {
            get { return _groupMemberLstCount; }
            set { _groupMemberLstCount = value; }
        }

        private string _contactPerson = string.Empty;
        [DataMember]
        public string ContactPerson
        {
            get { return _contactPerson; }
            set { _contactPerson = value; }
        }

        private string _memberCardNo = string.Empty;
        [DataMember]
        public string MemberCardNo
        {
            get { return _memberCardNo; }
            set { _memberCardNo = value; }
        }

        //Employee's Id
        private string _empCompanyName = string.Empty;
        [DataMember]
        public string EmpCompanyName
        {
            get { return _empCompanyName; }
            set { _empCompanyName = value; }
        }
        //Sponser's Id
        private int _sponserId = -1;
        [DataMember]
        public int SponserId
        {
            get { return _sponserId; }
            set { _sponserId = value; }
        }

        private string _sponserCustId = string.Empty;
        [DataMember]
        public string SponserCustId
        {
            get { return _sponserCustId; }
            set { _sponserCustId = value; }
        }


        private DateTime? _sponsorStartDate = DateTime.Now;
        [DataMember]
        public DateTime? SponsorStartDate
        {
            get { return _sponsorStartDate; }
            set { _sponsorStartDate = value; }
        }

        private DateTime? _sponsorEndDate = null;
        [DataMember]
        public DateTime? SponsorEndDate
        {
            get { return _sponsorEndDate; }
            set { _sponsorEndDate = value; }
        }

        private string _familyMemberName = string.Empty;
        [DataMember]
        public string FamilyMemberName
        {
            get { return _familyMemberName; }
            set { _familyMemberName = value; }
        }

        private int _familyMemberId;
        [DataMember]
        public int FamilyMemberId
        {
            get { return _familyMemberId; }
            set { _familyMemberId = value; }
        }

        private string _familyMemberCustId = string.Empty;
        [DataMember]
        public string FamilyMemberCustId
        {
            get { return _familyMemberCustId; }
            set { _familyMemberCustId = value; }
        }

        private int _familyMemberBranchId = -1;
        [DataMember]
        public int FamilyMemberBranchId
        {
            get { return _familyMemberBranchId; }
            set { _familyMemberBranchId = value; }
        }

        private MemberRole _familyMemberRoleId = MemberRole.MEM;
        [DataMember]
        public MemberRole FamilyMemberRoleId
        {
            get { return _familyMemberRoleId;  }
            set { _familyMemberRoleId = value; }
        }

        private int _contractIdForFamily;
        [DataMember]
        public int ContractIdForFamily
        {
            get { return _contractIdForFamily; }
            set { _contractIdForFamily = value; }
        }

        private bool _isCheckFamilymember;
        [DataMember]
        public bool IsCheckFamilymember
        {
            get { return _isCheckFamilymember; }
            set { _isCheckFamilymember = value; }
        }

        private string _sponserName = string.Empty;
        [DataMember]
        public string SponserName
        {
            get { return _sponserName; }
            set { _sponserName = value; }
        }
        private bool _hasNotifications = false;
        [DataMember]
        public bool HasNotifications
        {
            get { return _hasNotifications; }
            set { _hasNotifications = value; }
        }

        private bool _hasCalender = false;
        [DataMember]
        public bool HasCalender
        {
            get { return _hasCalender; }
            set { _hasCalender = value; }
        }
        private string _notification = string.Empty;
        [DataMember]
        public string Notification
        {
            get { return _notification; }
            set { _notification = value; }
        }

        private List<USNotificationSummaryInfo> _notificationList = new List<USNotificationSummaryInfo>();
        [DataMember]
        public List<USNotificationSummaryInfo> NotificationList
        {
            get { return _notificationList; }
            set { _notificationList = value; }
        }

        private MemberParentDC _memberParent = new MemberParentDC();
        [DataMember]
        public MemberParentDC MemberParent
        {
            get { return _memberParent; }
            set { _memberParent = value; }
        }

        private bool _isSendAdvertisement = false;
        [DataMember]
        public bool IsSendAdvertisement
        {
            get { return _isSendAdvertisement; }
            set { _isSendAdvertisement = value; }
        }

        private int _memberInroCategoryId = 0;
        [DataMember]
        public int MemberInroCategoryId
        {
            get { return _memberInroCategoryId; }
            set { _memberInroCategoryId = value; }
        }

        private string _commentForInactive = string.Empty;
        [DataMember]
        public string CommentForInactive
        {
            get { return _commentForInactive; }
            set { _commentForInactive = value; }
        }

        private List<CategoryDC> _infoCategoryList = new List<CategoryDC>();
        [DataMember]
        public List<CategoryDC> InfoCategoryList
        {
            get { return _infoCategoryList; }
            set { _infoCategoryList = value; }
        }

        private List<ExcelineMemberDC> _familyMmeberList = new List<ExcelineMemberDC>();
        [DataMember]
        public List<ExcelineMemberDC> FamilyMmeberList
        {
            get { return _familyMmeberList; }
            set { _familyMmeberList = value; }
        }

        private int _familyMmeberLstCount = 0;
        [DataMember]
        public int FamilyMmeberLstCount
        {
            get { return _familyMmeberLstCount; }
            set { _familyMmeberLstCount = value; }
        }


        private DateTime _lastVisitDate;
        [DataMember]
        public DateTime LastVisitDate
        {
            get { return _lastVisitDate; }
            set { _lastVisitDate = value; }
        }

        private DateTime _payerActivatedDate;
        [DataMember]
        public DateTime PayerActivatedDate
        {
            get { return _payerActivatedDate; }
            set { _payerActivatedDate = value; }
        }

        private string _custId = string.Empty;
        [DataMember]
        public string CustId
        {
            get { return _custId; }
            set { _custId = value; }
        }

        private bool _isCreateContracte;
        [DataMember]
        public bool IsCreateContracte
        {
            get { return _isCreateContracte; }
            set { _isCreateContracte = value; }
        }

        private string _note = string.Empty;
        [DataMember]
        public string Note
        {
            get { return _note; }
            set { _note = value; }
        }

        private int _memberStatuse = -1;
        [DataMember]
        public int MemberStatuse
        {
            get { return _memberStatuse; }
            set { _memberStatuse = value; }
        }

        private DateTime _registerDate;
        [DataMember]
        public DateTime RegisterDate
        {
            get { return _registerDate; }
            set { _registerDate = value; }
        }

        private int _instructorId = -1;
        [DataMember]
        public int InstructorId
        {
            get { return _instructorId; }
            set { _instructorId = value; }
        }

        private bool _invoiceChage = true;
        [DataMember]
        public bool InvoiceChage
        {
            get { return _invoiceChage; }
            set { _invoiceChage = value; }
        }

        private bool _smsInvoice;
        [DataMember]
        public bool SmsInvoice
        {
            get { return _smsInvoice; }
            set { _smsInvoice = value; }
        }

        private string _groupNameForMember = string.Empty;
        [DataMember]
        public string GroupNameForMember
        {
            get { return _groupNameForMember; }
            set { _groupNameForMember = value; }
        }

        private string _groupCustId = string.Empty;
        [DataMember]
        public string GroupCustId
        {
            get { return _groupCustId; }
            set { _groupCustId = value; }
        }

        private DateTime? _introduceDate;
        [DataMember]
        public DateTime? IntroduceDate
        {
            get { return _introduceDate; }
            set { _introduceDate = value; }
        }


        private int _statusNo = -1;
        [DataMember]
        public int StatusNo
        {
            get { return _statusNo; }
            set { _statusNo = value; }
        }



        private List<ExcelineMemberDC> _introduceMemberLst = new List<ExcelineMemberDC>();
        [DataMember]
        public List<ExcelineMemberDC> IntroduceMemberLst
        {
            get { return _introduceMemberLst; }
            set { _introduceMemberLst = value; }
        }

        private string _countryId = string.Empty;
        [DataMember]
        public string CountryId
        {
            get { return _countryId; }
            set { _countryId = value; }
        }

        private string _countryName = string.Empty;
        [DataMember]
        public string CountryName
        {
            get { return _countryName; }
            set { _countryName = value; }
        }

        private string _accountingEmail = string.Empty;
        [DataMember]
        public string AccountingEmail
        {
            get { return _accountingEmail; }
            set { _accountingEmail = value; }
        }

        private string _contactPersonName = string.Empty;
        [DataMember]
        public string ContactPersonName
        {
            get { return _contactPersonName; }
            set { _contactPersonName = value; }
        }

        private int _contactPersonId = -1;
        [DataMember]
        public int ContactPersonId
        {
            get { return _contactPersonId; }
            set { _contactPersonId = value; }
        }

        private DateTime? _groupMembershipStartDate;
        [DataMember]
        public DateTime? GroupMembershipStartDate
        {
            get { return _groupMembershipStartDate; }
            set { _groupMembershipStartDate = value; }
        }
        private string _statusName = string.Empty;
        [DataMember]
        public string StatusName
        {
            get { return _statusName; }
            set { _statusName = value; }
        }

        private bool _hasIntroduceDate = false;
        [DataMember]
        public bool HasIntroduceDate
        {
            get { return _hasIntroduceDate; }
            set { _hasIntroduceDate = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private int _guardianBranchId= -1;
        [DataMember]
        public int GuardianBranchId
        {
            get { return _guardianBranchId; }
            set { _guardianBranchId = value; }
        }

        private int _creditPeriod = 0;
        [DataMember]
        public int CreditPeriod
        {
            get { return _creditPeriod; }
            set { _creditPeriod = value; }
        }


        private string _guardianFirstName = string.Empty;
        [DataMember]
        public string GuardianFirstName
        {
            get { return _guardianFirstName; }
            set { _guardianFirstName = value; }
        }

        private string _guardianLastName = string.Empty;
        [DataMember]
        public string GuardianLastName
        {
            get { return _guardianLastName; }
            set { _guardianLastName = value; }
        }


        private string _guardianAddress1 = string.Empty;
        [DataMember]
        public string GuardianAddress1
        {
            get { return _guardianAddress1; }
            set { _guardianAddress1 = value; }
        }


        private string _guardianAddress2 = string.Empty;
        [DataMember]
        public string GuardianAddress2
        {
            get { return _guardianAddress2; }
            set { _guardianAddress2 = value; }
        }


        private string _guardianMobile = string.Empty;
        [DataMember]
        public string GuardianMobile
        {
            get { return _guardianMobile; }
            set { _guardianMobile = value; }
        }

        private string _guardianZipCode = string.Empty;
        [DataMember]
        public string GuardianZipCode
        {
            get { return _guardianZipCode; }
            set { _guardianZipCode = value; }
        }


        private string _guardianZipName = string.Empty;
        [DataMember]
        public string GuardianZipName
        {
            get { return _guardianZipName; }
            set { _guardianZipName = value; }
        }

        private string _guardianEmail = string.Empty;
        [DataMember]
        public string GuardianEmail
        {
            get { return _guardianEmail; }
            set { _guardianEmail = value; }
        }

        private decimal _dueBalance = 0;
        [DataMember]
        public decimal DueBalance
        {
            get { return _dueBalance; }
            set { _dueBalance = value; }
        }

        private decimal _creditBalance = 0;
        [DataMember]
        public decimal CreditBalance
        {
            get { return _creditBalance; }
            set { _creditBalance = value; }
        }

        private int _guardianId = 0;
        [DataMember]
        public int GuardianId
        {
            get { return _guardianId; }
            set { _guardianId = value; }
        }

        private string _guardianCustId = string.Empty;
        [DataMember]
        public string GuardianCustId
        {
            get { return _guardianCustId; }
            set { _guardianCustId = value; }
        }

        private string _guardianName = string.Empty;
        [DataMember]
        public string GuardianName
        {
            get { return _guardianName; }
            set { _guardianName = value; }
        }

        private List<ExcelineMemberDC> _guardianLst = new List<ExcelineMemberDC>();
        [DataMember]
        public List<ExcelineMemberDC> GuardianLst
        {
            get { return _guardianLst; }
            set { _guardianLst = value; }
        }


        private DateTime? _nextDueDate;
        [DataMember]
        public DateTime? NextDueDate
        {
            get { return _nextDueDate; }
            set { _nextDueDate = value; }
        }


        private DateTime? _lastDueDate;
        [DataMember]
        public DateTime? LastDueDate
        {
            get { return _lastDueDate; }
            set { _lastDueDate = value; }
        }

        private decimal _billedToDate;
        [DataMember]
        public decimal BilledToDate
        {
            get { return _billedToDate; }
            set { _billedToDate = value; }
        }

        private int _noOfVisiteLast30 = -1;
        [DataMember]
        public int NoOfVisiteLast30
        {
            get { return _noOfVisiteLast30; }
            set { _noOfVisiteLast30 = value; }
        }

        private int _noOfVisiteLast365 = -1;
        [DataMember]
        public int NoOfVisiteLast365
        {
            get { return _noOfVisiteLast365; }
            set { _noOfVisiteLast365 = value; }
        }

        private string _ctName = string.Empty; // Current Contract Template Name
        [DataMember]
        public string CtName
        {
            get { return _ctName; }
            set { _ctName = value; }
        }

        private string _ctNumber = string.Empty; // Current Contract Template Number
        [DataMember]
        public string CtNumber
        {
            get { return _ctNumber; }
            set { _ctNumber = value; }
        }

        private DateTime? _ctStartDate;
        [DataMember]
        public DateTime? CtStartDate
        {
            get { return _ctStartDate; }
            set { _ctStartDate = value; }
        }


        private DateTime? _ctEndDate;
        [DataMember]
        public DateTime? CtEndDate
        {
            get { return _ctEndDate; }
            set { _ctEndDate = value; }
        }


        private DateTime? _freezeFromDate;
        [DataMember]
        public DateTime? FreezeFromDate
        {
            get { return _freezeFromDate; }
            set { _freezeFromDate = value; }
        }

        private DateTime? _freezeToDate;
        [DataMember]
        public DateTime? FreezeToDate
        {
            get { return _freezeToDate; }
            set { _freezeToDate = value; }
        }


        private bool _isFollowUp = false;
        [DataMember]
        public bool IsFollowUp
        {
            get { return _isFollowUp; }
            set { _isFollowUp = value; }
        }


        private DateTime? _adcSignedDate;
        [DataMember]
        public DateTime? AdcSignedDate
        {
            get { return _adcSignedDate; }
            set { _adcSignedDate = value; }
        }

        private DateTime? _nextFollowUpDate;
        [DataMember]
        public DateTime? NextFollowUpDate
        {
            get { return _nextFollowUpDate; }
            set { _nextFollowUpDate = value; }
        }

        private DateTime? _lastFollowUpDate;
        [DataMember]
        public DateTime? LastFollowUpDate
        {
            get { return _lastFollowUpDate; }
            set { _lastFollowUpDate = value; }
        }



        private string _atgKid = string.Empty;
        [DataMember]
        public string AtgKid
        {
            get { return _atgKid; }
            set { _atgKid = value; }
        }

        private string _oldKid = string.Empty;
        [DataMember]
        public string OldKid
        {
            get { return _oldKid; }
            set { _oldKid = value; }
        }

        private decimal _chargeLimitATG = 0;
        [DataMember]
        public decimal ChargeLimitAtg
        {
            get { return _chargeLimitATG; }
            set { _chargeLimitATG = value; }
        }

        private DateTime? _atgLastUpdated;
        [DataMember]
        public DateTime? AtgLastUpdated
        {
            get { return _atgLastUpdated; }
            set { _atgLastUpdated = value; }
        }


        private List<EmployeeCategoryDC> _employeeCategoryList = new List<EmployeeCategoryDC>();
        [DataMember]
        public List<EmployeeCategoryDC> EmployeeCategoryList
        {
            get { return _employeeCategoryList; }
            set { _employeeCategoryList = value; }
        }



        private EmployeeCategoryDC _employeeCategoryForSponser = new EmployeeCategoryDC();
        [DataMember]
        public EmployeeCategoryDC EmployeeCategoryForSponser
        {
            get { return _employeeCategoryForSponser; }
            set { _employeeCategoryForSponser = value; }
        }



        private string _discountCategoy = string.Empty;
        [DataMember]
        public string DiscountCategoy
        {
            get { return _discountCategoy; }
            set { _discountCategoy = value; }
        }


        private decimal _billedLast365 = 0;
        [DataMember]
        public decimal BilledLast365
        {
            get { return _billedLast365; }
            set { _billedLast365 = value; }
        }

        private decimal _shopPchtoDate;
        [DataMember]
        public decimal ShopPchtoDate
        {
            get { return _shopPchtoDate; }
            set { _shopPchtoDate = value; }
        }



        private decimal _shoppchLast365 = 0;
        [DataMember]
        public decimal ShoppchLast365
        {
            get { return _shoppchLast365; }
            set { _shoppchLast365 = value; }
        }



        private decimal _onAccountBalance = 0;
        [DataMember]
        public decimal OnAccountBalance
        {
            get { return _onAccountBalance; }
            set { _onAccountBalance = value; }
        }


        private string _guestCardNo = string.Empty;
        [DataMember]
        public string GuestCardNo
        {
            get { return _guestCardNo; }
            set { _guestCardNo = value; }
        }

        private int _pinCode = 0;
        [DataMember]
        public int PinCode
        {
            get { return _pinCode; }
            set { _pinCode = value; }
        }

        private String _vatNumber = string.Empty;
        [DataMember]
        public String VatNumber
        {
            get { return _vatNumber; }
            set { _vatNumber = value; }
        }


        private int _memberShipPeriod = 0;
        [DataMember]
        public int MemberShipPeriod
        {
            get { return _memberShipPeriod; }
            set { _memberShipPeriod = value; }
        }


        private string _genderDisplayName = string.Empty;
        [DataMember]
        public string GenderDisplayName
        {
            get { return _genderDisplayName; }
            set { _genderDisplayName = value; }
        }

        private DateTime? _trailDate;
        [DataMember]
        public DateTime? TrailDate
        {
            get { return _trailDate; }
            set { _trailDate = value; }
        }

        private int _noOfFollowUpLast30 = 0;
        [DataMember]
        public int NoOfFollowUpLast30
        {
            get { return _noOfFollowUpLast30; }
            set { _noOfFollowUpLast30 = value; }
        }

        private int _noOfFollowUpLast365 = 0;
        [DataMember]
        public int NoOfFollowUpLast365
        {
            get { return _noOfFollowUpLast365; }
            set { _noOfFollowUpLast365 = value; }
        }

        private int _dueDate = 0;
        [DataMember]
        public int DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        private bool _checkFingerPrint = false;
        [DataMember]
        public bool CheckFingerPrint
        {
            get { return _checkFingerPrint; }
            set { _checkFingerPrint = value; }
        }

        private decimal _prepaidBalance = 0;
        [DataMember]
        public decimal PrepaidBalance
        {
            get { return _prepaidBalance; }
            set { _prepaidBalance = value; }
        }

        private string _sponserRole = string.Empty;
        [DataMember]
        public string SponserRole
        {
            get { return _sponserRole; }
            set { _sponserRole = value; }
        }

        private List<MemberATGStatusInfo> _atgStatusList = new List<MemberATGStatusInfo>();
        [DataMember]
        public List<MemberATGStatusInfo> AtgStatusList
        {
            get { return _atgStatusList; }
            set { _atgStatusList = value; }
        }

        private bool _haveOthergymContracts = false;
         [DataMember]
        public bool HaveOthergymContracts
        {
            get { return _haveOthergymContracts; }
            set { _haveOthergymContracts = value; }
        }

        private string _linkedEmployeeCustId;
        [DataMember]
        public string LinkedEmployeeCustId
        {
            get { return _linkedEmployeeCustId; }
            set { _linkedEmployeeCustId = value; }
        }

        private int _linkedEmployeeId;
         [DataMember]
        public int LinkedEmployeeId
        {
            get { return _linkedEmployeeId; }
            set { _linkedEmployeeId = value; }
        }

        private bool _isAddEmployee;
         [DataMember]
        public bool IsAddEmployee
        {
            get { return _isAddEmployee; }
            set { _isAddEmployee = value; }
        }

        private bool _isRemovedEmployee;
        [DataMember]
        public bool IsRemovedEmployee
        {
            get { return _isRemovedEmployee; }
            set { _isRemovedEmployee = value; }
        }

        private int _integratedId;
        [DataMember]
        public int IntegratedId
        {
            get { return _integratedId; }
            set { _integratedId = value; }
        }

        private MemberIntegrationSettingDC _integrationSetting;
        [DataMember]
        public MemberIntegrationSettingDC IntegrationSetting
        {
            get { return _integrationSetting; }
            set { _integrationSetting = value; }
        }

        private string _infoGymId = string.Empty;
        [DataMember]
        public string InfoGymId
        {
            get { return _infoGymId; }
            set { _infoGymId = value; }
        }

        private string _ssn = string.Empty;
         [DataMember]
        public string Ssn
        {
            get { return _ssn; }
            set { _ssn = value; }
        }

        private int _languageId ;
          [DataMember]
        public int LanguageId
        {
            get { return _languageId; }
            set { _languageId = value; }
        }

        private string _language = string.Empty;
         [DataMember]
        public string Language
        {
            get { return _language; }
            set { _language = value; }
        }

         private int _userId = -1;
         [DataMember]
        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        private string _userName = string.Empty;
        [DataMember]
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        private string _school = string.Empty;
         [DataMember]
        public string School
        {
            get { return _school; }
            set { _school = value; }
        }

         private string _lastPaidSemesterFee = string.Empty;
        [DataMember]
        public string LastPaidSemesterFee
        {
            get { return _lastPaidSemesterFee; }
            set { _lastPaidSemesterFee = value; }
        }

        private int? _agressoId = -1;
          [DataMember]
        public int? AgressoId
        {
            get { return _agressoId; }
            set { _agressoId = value; }
        }


        private int _basicContractId = -1;
        [DataMember]
        public int BasicContractId
        {
            get { return _basicContractId; }
            set { _basicContractId = value; }
        }

        private List<FollowUpDetailDC> _followUpDetailsList = new List<FollowUpDetailDC>();
        [DataMember]
          public List<FollowUpDetailDC> FollowUpDetailsList
          {
              get { return _followUpDetailsList; }
              set { _followUpDetailsList = value; }
          }

        private string _imageURLDomain = string.Empty;
        [DataMember]
        public string ImageURLDomain
        {
            get { return _imageURLDomain; }
            set { _imageURLDomain = value; }
        }

        private bool _isUsingGantner = false;
        [DataMember]
        public bool IsUsingGantner
        {
            get { return _isUsingGantner; }
            set { _isUsingGantner = value; }
        }

        private int _sponsorBranchId = -1;
        [DataMember]
        public int SponsorBranchId
        {
            get { return _sponsorBranchId; }
            set { _sponsorBranchId = value; }
        }

        [DataMember]
        public bool IsBrisIntegrated { get; set; }



    }
}
