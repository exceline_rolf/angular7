﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Login
{

    [DataContract]
    public class ExcePWRecDC
    {
        private int _returnCode;
        [DataMember]
        public int ReturnCode
        {
            get { return _returnCode; }
            set { _returnCode = value; }
        }

        private string _userName;
        [DataMember]
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        private string _userEmail = string.Empty;
        [DataMember]
        public string UserEmail
        {
            get { return _userEmail; }
            set { _userEmail = value; }
        }

        private String _hashBase = "";
        [DataMember]
        public String HashBase
        {
            get { return _hashBase; }
            set { _hashBase = value; }
        }
    }
}
