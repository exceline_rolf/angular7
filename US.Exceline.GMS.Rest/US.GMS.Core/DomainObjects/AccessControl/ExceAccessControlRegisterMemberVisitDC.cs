﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    public class ExceAccessControlRegisterMemberVisitDC
    {
        private string _cardNo;
        private int _terminalId;
        private int _branchId;

        [DataMember(Order = 1)]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        [DataMember(Order = 2)]
        public string CardNo
        {
            get { return _cardNo; }
            set { _cardNo = value; }
        }

        [DataMember(Order = 3)]
        public int TerminalId
        {
            get { return _terminalId; }
            set { _terminalId = value; }
        }

    
     
    }
}
