﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.DomainObjects.AccessControl.ARX
{
    public class person
    {
        public string id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string PinCode { get; set; }
        public int AccessProfileID { get; set; }
        public string CardNo { get; set; }
        public string BranchName {get; set;}
        public string FormatName { get; set; }
        public int BranchID { get; set; }
        public string EndDate { get; set; }
        public string AccessProfileName { get; set; }
        public bool Deleted { get; set; }
        public List<card> CardList { get; set; }

    }
}
