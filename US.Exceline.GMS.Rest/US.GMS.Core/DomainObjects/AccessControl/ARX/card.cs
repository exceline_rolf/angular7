﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.DomainObjects.AccessControl.ARX
{
    public class card
    {
        public string number { get; set; }
        public string person_id { get; set; }
        public string format_name { get; set; }
        public bool IsDeleted { get; set; }
    }
}
