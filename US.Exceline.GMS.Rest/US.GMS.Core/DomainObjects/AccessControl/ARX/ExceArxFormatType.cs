﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.AccessControl.ARX
{
     [DataContract]
   public class ExceArxFormatType
   {
       private int _Id;
        [DataMember]
       public int Id
       {
           get { return _Id; }
           set { _Id = value; }
       }

       private string _name = string.Empty;
        [DataMember]
       public string Name
       {
           get { return _name; }
           set { _name = value; }
       }

       
   }
}
