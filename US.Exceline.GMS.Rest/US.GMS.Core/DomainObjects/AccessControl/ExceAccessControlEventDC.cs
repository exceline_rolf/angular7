﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    public class ExceAccessControlEventDC
    {
        private string _cardNo;
        private int _terminalId;
        private string _terminalType;
        private string _message;
        private string _accessControlType;

        [DataMember(Order = 1)]
        public string CardNo
        {
            get { return _cardNo; }
            set { _cardNo = value; }
        }

        [DataMember(Order = 2)]
        public int TerminalId
        {
            get { return _terminalId; }
            set { _terminalId = value; }
        }

        [DataMember(Order = 3)]
        public string TerminalType
        {
            get { return _terminalType; }
            set { _terminalType = value; }
        }

        [DataMember(Order = 4)]
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        [DataMember(Order = 5)]
        public string AccessControlType
        {
            get { return _accessControlType; }
            set { _accessControlType = value; }
        }
    }
}
