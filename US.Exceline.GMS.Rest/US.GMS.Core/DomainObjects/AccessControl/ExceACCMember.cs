﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    [DataContract]
    public class ExceACCMember
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _memberCardNo = string.Empty;
        [DataMember]
        public string MemberCardNo
        {
            get { return _memberCardNo; }
            set { _memberCardNo = value; }
        }

        private int _memberContractId = -1;
         [DataMember]
        public int MemberContractId
        {
            get { return _memberContractId; }
            set { _memberContractId = value; }
        }

        private int _activityId = -1;
        [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        private string _activityName = string.Empty;
        [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _custId = string.Empty;
        [DataMember]
        public string CustId
        {
            get { return _custId; }
            set { _custId = value; }
        }

        private string _homeGym = string.Empty;
        [DataMember]
        public string HomeGym
        {
            get { return _homeGym; }
            set { _homeGym = value; }
        }

        private string _gender = string.Empty;
         [DataMember]
        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

         private string _lastVisitDate = string.Empty;
         [DataMember]
         public string LastVisitDate
         {
             get { return _lastVisitDate; }
             set { _lastVisitDate = value; }
         }

         private decimal _creditBalance = 0;
        [DataMember]
         public decimal CreditBalance
         {
             get { return _creditBalance; }
             set { _creditBalance = value; }
         }

        private string _guestCardNo = string.Empty;
        [DataMember]
        public string GuestCardNo
        {
            get { return _guestCardNo; }
            set { _guestCardNo = value; }
        }

        private string _memberContractNo = string.Empty;
        [DataMember]
        public string MemberContractNo
        {
            get { return _memberContractNo; }
            set { _memberContractNo = value; }
        }

        private string _contractStartDate;
        [DataMember]
        public string ContractStartDate
        {
            get { return _contractStartDate; }
            set { _contractStartDate = value; }
        }

        private string _contractEndDate;
        [DataMember]
        public string ContractEndDate
        {
            get { return _contractEndDate; }
            set { _contractEndDate = value; }
        }

        private string _templateNo = string.Empty;
        [DataMember]
        public string TemplateNo
        {
            get { return _templateNo; }
            set { _templateNo = value; }
        }

        private string _templateName = string.Empty;
        [DataMember]
        public string TemplateName
        {
            get { return _templateName; }
            set { _templateName = value; }
        }

        private string _contractType = string.Empty;
         [DataMember]
        public string ContractType
        {
            get { return _contractType; }
            set { _contractType = value; }
        }

         private int _availableVisits = 0;
         [DataMember]
         public int AvailableVisits
         {
             get { return _availableVisits; }
             set { _availableVisits = value; }
         }

         private int _accessProfileId = -1;
         [DataMember]
         public int AccessProfileId
         {
             get { return _accessProfileId; }
             set { _accessProfileId = value; }
         }

         private decimal _shopAccBalance = 0;
         [DataMember]
         public decimal ShopAccBalance
         {
             get { return _shopAccBalance; }
             set { _shopAccBalance = value; }
         }

         private string _antiDopingDate = string.Empty;
         [DataMember]
         public string AntiDopingDate
         {
             get { return _antiDopingDate; }
             set { _antiDopingDate = value; }
         }

         private string _isFreezed = string.Empty;
         [DataMember]
         public string IsFreezed
         {
             get { return _isFreezed; }
             set { _isFreezed = value; }
         }

         private string _isContractATG = string.Empty;
         [DataMember]
         public string IsContractATG
         {
             get { return _isContractATG; }
             set { _isContractATG = value; }
         }

         private string _memberATGStatus = string.Empty;
         [DataMember]
         public string MemberATGStatus
         {
             get { return _memberATGStatus; }
             set { _memberATGStatus = value; }
         }

         private string _memberMessages = string.Empty;
         [DataMember]
         public string MemberMessages
         {
             get { return _memberMessages; }
             set { _memberMessages = value; }
         }

         private string _born = string.Empty;
         [DataMember]
         public string Born
         {
             get { return _born; }
             set { _born = value; }
         }

         private bool _isError;
         [DataMember]
         public bool IsError
         {
             get { return _isError; }
             set { _isError = value; }
         }

         private List<string> _messages = new List<string>();
        [DataMember]
        public List<string> Messages
        {
            get { return _messages; }
            set { _messages = value; }
        }

        private string _freezeToDate = string.Empty;
          [DataMember]
        public string FreezeToDate
        {
            get { return _freezeToDate; }
            set { _freezeToDate = value; }
        }

        private string _mobile = string.Empty;
         [DataMember]
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        private Dictionary<string, string> _smsMessages = new Dictionary<string, string>();
        [DataMember]
        public Dictionary<string, string> SmsMessages
        {
            get { return _smsMessages; }
            set { _smsMessages = value; }
        }


        private int _unPaidInvoiceCount;
        [DataMember]
        public int UnPaidInvoiceCount
        {
            get { return _unPaidInvoiceCount; }
            set { _unPaidInvoiceCount = value; }
        }

        private int _systemId = -1;
        [DataMember]
        public int SystemId
        {
            get { return _systemId; }
            set { _systemId = value; }
        }

        private int _minutesBetweenSwipes = -1;
         [DataMember]
        public int MinutesBetweenSwipes
        {
            get { return _minutesBetweenSwipes; }
            set { _minutesBetweenSwipes = value; }
        }

         private int _minimumUnpaidInvoices = -1;
        [DataMember]
         public int MinimumUnpaidInvoices
        {
            get { return _minimumUnpaidInvoices; }
            set { _minimumUnpaidInvoices = value; }
        }

        private decimal _unPaidDueBalance;
        [DataMember]
        public decimal UnPaidDueBalance
        {
            get { return _unPaidDueBalance; }
            set { _unPaidDueBalance = value; }
        }

        private decimal _minimumOnAccountBalance;
         [DataMember]
        public decimal MinimumOnAccountBalance
        {
            get { return _minimumOnAccountBalance; }
            set { _minimumOnAccountBalance = value; }
        }

         private bool _isSendSms;
         [DataMember]
         public bool IsSendSms
        {
            get { return _isSendSms; }
            set { _isSendSms = value; }
        }

         private int _minimumPunches;
         [DataMember]
         public int MinimumPunches
        {
            get { return _minimumPunches; }
            set { _minimumPunches = value; }
        }

         private bool _isDisplayName;
         [DataMember]
         public bool IsDisplayName
         {
             get { return _isDisplayName; }
             set { _isDisplayName = value; }
         }

         private bool _isDisplayCustomerNumber;
         [DataMember]
         public bool IsDisplayCustomerNumber
         {
             get { return _isDisplayCustomerNumber; }
             set { _isDisplayCustomerNumber = value; }
         }

         private bool _isDisplayContractTemplate;
         [DataMember]
         public bool IsDisplayContractTemplate
         {
             get { return _isDisplayContractTemplate; }
             set { _isDisplayContractTemplate = value; }
         }

         private bool _isDisplayHomeGym;
         [DataMember]
         public bool IsDisplayHomeGym
         {
             get { return _isDisplayHomeGym; }
             set { _isDisplayHomeGym = value; }
         }

         private bool _isDisplayContractEndDate;
         [DataMember]
         public bool IsDisplayContractEndDate
         {
             get { return _isDisplayContractEndDate; }
             set { _isDisplayContractEndDate = value; }
         }

         private bool _isDisplayBirthDate;
         [DataMember]
         public bool IsDisplayBirthDate
         {
             get { return _isDisplayBirthDate; }
             set { _isDisplayBirthDate = value; }
         }

         private bool _isDisplayAge;
         [DataMember]
         public bool IsDisplayAge
         {
             get { return _isDisplayAge; }
             set { _isDisplayAge = value; }
         }

         private bool _isDisplayLastVisit;
         [DataMember]
         public bool IsDisplayLastVisit
         {
             get { return _isDisplayLastVisit; }
             set { _isDisplayLastVisit = value; }
         }

         private bool _isDisplayCreditBalance;
         [DataMember]
         public bool IsDisplayCreditBalance
         {
             get { return _isDisplayCreditBalance; }
             set { _isDisplayCreditBalance = value; }
         }

         private bool _isDisplayPicture;
         [DataMember]
         public bool IsDisplayPicture
         {
             get { return _isDisplayPicture; }
             set { _isDisplayPicture = value; }
         }

         private bool _isDisplayLastAccess;
         [DataMember]
         public bool IsDisplayLastAccess
         {
             get { return _isDisplayLastAccess; }
             set { _isDisplayLastAccess = value; }
         }

         private bool _isDisplayNotification;
         [DataMember]
         public bool IsDisplayNotification
         {
             get { return _isDisplayNotification; }
             set { _isDisplayNotification = value; }
         }
    }
}
