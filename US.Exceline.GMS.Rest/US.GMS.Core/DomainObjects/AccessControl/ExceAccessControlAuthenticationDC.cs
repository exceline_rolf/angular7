﻿using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    [DataContract]
    public class ExceAccessControlAuthenticationDC
    {
        [DataMember(Order = 1)]
        public bool? IsValidMember { get; set; }
        [DataMember(Order = 2)]
        public decimal Balance { get; set; }
        [DataMember(Order = 3)]
        public bool? CheckFingerPrint { get; set; }
        [DataMember(Order = 4)]
        public bool? CanPurchase { get; set; }
        //Shouldn't want to pass that property to windows service
        //[DataMember(Order = 3)]
        public int MemberId { get; set; }
        public int BranchId { get; set; }
        public ArticleDC TimeMachineArticle { get; set; }       
    }
}
