﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    public class ExcACCAccessControl
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int BranchId { get; set; }
        public int Status { get; set; }
        public DateTime InstalledDate { get; set; }
        public DateTime LastPingTime { get; set; }
    }
}
