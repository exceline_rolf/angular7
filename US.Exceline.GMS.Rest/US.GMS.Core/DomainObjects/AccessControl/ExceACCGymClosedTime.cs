﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    [DataContract]
    public class ExceACCGymClosedTime
    {
        private string _id = string.Empty;
        [DataMember]
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _activityId = string.Empty;
        [DataMember]
        public string ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        private string _activityName = string.Empty;
        [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        private string _startdate = string.Empty;
        [DataMember]
        public string Startdate
        {
            get { return _startdate; }
            set { _startdate = value; }
        }

        private string _endDate = string.Empty;
        [DataMember]
        public string EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private string _startTime = string.Empty;
        [DataMember]
        public string StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private string _endTime= string.Empty;
        [DataMember]
        public string EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        private string _branchId = string.Empty;
        [DataMember]
        public string BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _edited = string.Empty;
        [DataMember]
        public string Edited
        {
            get { return _edited; }
            set { _edited = value; }
        }

        private string _reason = string.Empty;
        [DataMember]
        public string Reason
        {
            get { return _reason; }
            set { _reason = value; }
        }
    }
}
