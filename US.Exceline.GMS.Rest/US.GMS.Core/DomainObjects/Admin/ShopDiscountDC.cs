﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.Admin;

namespace US.GMS.Core.DomainObjects.Admin
{
    [DataContract]
    public class ShopDiscountDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _artileID = -1;
        [DataMember]
        public int ArticleID
        {
            get { return _artileID; }
            set { _artileID = value; }
        }

        private string _name;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

       
       
        private string _vendorName;
        [DataMember]
        public string VendorName
        {
            get { return _vendorName; }
            set { _vendorName = value; }
        }

        private string _description;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }


        private double _discount = 0;
        [DataMember]
        public double Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }


        private DateTime _startDate;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }


        private DateTime _endDate;
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }


        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }


        private bool _isActive = false;
        [DataMember]
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }


        private List<int> _itemList = new List<int>();
        [DataMember]
        public List<int> ItemList
        {
            get { return _itemList; }
            set { _itemList = value; }
        }


        private List<string> _itemNames = new List<string>();
        [DataMember]
        public List<string> ItemNames
        {
            get { return _itemNames; }
            set { _itemNames = value; }
        }

        private string _discountPercentage = string.Empty;
        [DataMember]
        public string DiscountPercentage
        {
            get { return _discountPercentage; }
            set { _discountPercentage = value; }
        }

        private int _noOfFreeItems = -1;
        [DataMember]
        public int NoOfFreeItems
        {
            get { return _noOfFreeItems; }
            set { _noOfFreeItems = value; }
        }

        private int _minNoOfArticles = 1;
        [DataMember]
        public int MinNoOfArticles
        {
            get { return _minNoOfArticles; }
            set { _minNoOfArticles = value; }
        }


        private string _minNoContent= string.Empty;
        [DataMember]
        public string MinNoContent
        {
            get { return _minNoContent; }
            set { _minNoContent = value; }
        }



       private int _vendorId =-1;
        [DataMember]
        public int VendorId
        {
            get { return _vendorId; }
            set { _vendorId = value; }
        }

        private int _articleCategoryId=-1;
        [DataMember]
        public int ArticleCategoryId
        {
            get { return _articleCategoryId; }
            set { _articleCategoryId = value; }
        }

    }
}
