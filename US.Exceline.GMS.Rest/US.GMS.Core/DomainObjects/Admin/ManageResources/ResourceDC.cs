﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.GMS.Core.DomainObjects.Admin.ManageResources
{
    [DataContract]
    public class ResourceDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private byte[] _profilePicture = null;
        [DataMember]
        public byte[] ProfilePicture
        {
            get { return _profilePicture; }
            set { _profilePicture = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get
            {
                return _branchId;
            }
            set
            {
                _branchId = value;

            }
        }

        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        private DateTime _createdDate;
        [DataMember]
        public  DateTime CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        private DateTime _lastModifiedDate;
        [DataMember]
        public  DateTime ModifiedDate
        {
            get
            {
                return _lastModifiedDate;
            }
            set
            {
                _lastModifiedDate = value;
            }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public  string CreatedUser
        {
            get
            {
                return _createdUser;
            }
            set
            {
                _createdUser = value;
            }
        }

        private string _lastModifiedUser;
        [DataMember]
        public  string LastModifiedUser
        {
            get
            {
                return _lastModifiedUser;
            }
            set
            {
                _lastModifiedUser = value;
            }
        }

        private bool _activeStatus;
        [DataMember]
        public  bool ActiveStatus
        {
            get
            {
                return _activeStatus;
            }
            set
            {
                _activeStatus = value;
            }
        }

        private ScheduleDC _schedule = new ScheduleDC();
        [DataMember]
        public ScheduleDC Schedule
        {
            get { return _schedule; }
            set { _schedule = value; }
        }

        private bool _isSelected = false;
        [DataMember]
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private CategoryDC _category = new CategoryDC();

        [DataMember]
        public CategoryDC ResourceCategory
        {
            get { return _category; }
            set { _category = value; }
        }

        private string _imagePath = string.Empty;
        [DataMember]
        public string ImagePath
        {
            get { return _imagePath; }
            set { _imagePath = value; }
        }


        //private string _resourceNo = string.Empty;
        //[DataMember]
        //public string ResourceNo
        //{
        //    get { return _resourceNo; }
        //    set { _resourceNo = value; }
        //}

        private DateTime _maintenanceDate;
        [DataMember]
        public DateTime MaintenanceDate
        {
            get { return _maintenanceDate; }
            set { _maintenanceDate = value; }
        }

        private DateTime _purchasedDate;
        [DataMember]
        public DateTime PurchasedDate
        {
            get { return _purchasedDate; }
            set { _purchasedDate = value; }
        }

        private bool _isAssign;
        [DataMember]
        public  bool IsAssign
        {
            get
            {
                return _isAssign;
            }
            set
            {
                _isAssign = value;
            }
        }

        private string _resId = string.Empty;
         [DataMember]
        public string ResId
        {
            get { return _resId; }
            set { _resId = value; }
        }

        private Dictionary<int, string> _empList = new Dictionary<int, string>();
        [DataMember]
        public Dictionary<int, string> EmpList
        {
            get { return _empList; }
            set { _empList = value; }
        }

      
        private int _activityId;
         [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

       
        private int _equipmentId = -1;
          [DataMember]
        public int EquipmentId
        {
            get { return _equipmentId; }
            set { _equipmentId = value; }
        }

        private string _activityName = string.Empty;
           [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }


        private int _empCount;
          [DataMember]
        public int EmpCount
        {
            get { return _empCount; }
            set { _empCount = value; }
        }

        private bool _isHasEmp;
           [DataMember]
        public bool IsHasEmp
        {
            get { return _isHasEmp; }
            set { _isHasEmp = value; }
        }


        private bool _isEquipment;
          [DataMember]
        public bool IsEquipment
        {
            get { return _isEquipment; }
            set { _isEquipment = value; }
        }

        

        private string _equipmentText = string.Empty;
        [DataMember]
        public string EquipmentText
        {
            get { return _equipmentText; }
            set { _equipmentText = value; }
        }

       
        private bool _isSwitchRes;
          [DataMember]
        public bool IsSwitchRes
        {
            get { return _isSwitchRes; }
            set { _isSwitchRes = value; }
        }

        private int _switchResId;
         [DataMember]
        public int SwitchResId
        {
            get { return _switchResId; }
            set { _switchResId = value; }
        }

        private bool _cancelBooking;
         [DataMember]
        public bool CancelBooking
        {
            get { return _cancelBooking; }
            set { _cancelBooking = value; }
        }

        private List<ActivityTimeDC> _activityTimeLst = new List<ActivityTimeDC>();
          [DataMember]
        public List<ActivityTimeDC> ActivityTimeLst
        {
            get { return _activityTimeLst; }
            set { _activityTimeLst = value; }
        }

        private bool _activitySmsReminder;
         [DataMember]
        public bool ActivitySmsReminder
        {
            get { return _activitySmsReminder; }
            set { _activitySmsReminder = value; }
        }

        private CategoryDC _timeCategory = new CategoryDC();
         [DataMember]
        public CategoryDC TimeCategory
        {
            get { return _timeCategory; }
            set { _timeCategory = value; }
        }

        private bool _isSalaryPerBooking;
         [DataMember]
        public bool IsSalaryPerBooking
        {
            get { return _isSalaryPerBooking; }
            set { _isSalaryPerBooking = value; }
        }

       
        private int _daysForBookingPerActivity;
         [DataMember]
        public int DaysForBookingPerActivity
        {
            get { return _daysForBookingPerActivity; }
            set { _daysForBookingPerActivity = value; }
        }


         private bool _isReadOnly;
          [DataMember]
         public bool IsReadOnly
        {
            get { return _isReadOnly; }
            set { _isReadOnly = value; }
        }

          private int _articleId;
         [DataMember]
        public int ArticleId
        {
            get { return _articleId; }
            set { _articleId = value; }
        }

         private string _articleName = string.Empty;
          [DataMember]
        public string ArticleName
        {
            get { return _articleName; }
            set { _articleName = value; }
        }

          private int _articleSettingId;
          [DataMember]
        public int ArticleSettingId
        {
            get { return _articleSettingId; }
            set { _articleSettingId = value; }
        }

       




    }
}
