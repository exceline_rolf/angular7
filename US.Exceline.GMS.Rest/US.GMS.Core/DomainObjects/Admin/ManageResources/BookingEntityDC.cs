﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.Admin.ManageResources
{
     [DataContract]
   public class BookingEntityDc
    {
        private int _id ;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
           [DataMember]
         public string Name
         {
             get { return _name; }
             set { _name = value; }
         }

         private string _custId = string.Empty;
           [DataMember]
         public string CustId
         {
             get { return _custId; }
             set { _custId = value; }
         }

         private bool _isArrived;
          [DataMember]
         public bool IsArrived
         {
             get { return _isArrived; }
             set { _isArrived = value; }
         }

          private DateTime? _arrivalDateTime;
          [DataMember]
         public DateTime? ArrivalDateTime
         {
             get { return _arrivalDateTime; }
             set { _arrivalDateTime = value; }
         }

          private bool _isPaid;
            [DataMember]
         public bool IsPaid
         {
             get { return _isPaid; }
             set { _isPaid = value; }
         }

            private int _arItemNo;
            [DataMember]
         public int ArItemNo
         {
             get { return _arItemNo; }
             set { _arItemNo = value; }
         }

            private decimal _amount;
            [DataMember]
         public decimal Amount
         {
             get { return _amount; }
             set { _amount = value; }
         }
          
         private string _memberRole = string.Empty;
          [DataMember]
         public string MemberRole
         {
             get { return _memberRole; }
             set { _memberRole = value; }
         }


         private int _availableVisit;
          [DataMember]
         public int AvailableVisit
         {
             get { return _availableVisit; }
             set { _availableVisit = value; }
         }

          private bool _isBookingCreditback;
             [DataMember]
         public bool IsBookingCreditback
         {
             get { return _isBookingCreditback; }
             set { _isBookingCreditback = value; }
         }

             private bool _isDeleted;
          [DataMember]
         public bool IsDeleted
         {
             get { return _isDeleted; }
             set { _isDeleted = value; }
         }

         private bool _isPaidBtnVisible = true;
         [DataMember]
         public bool IsPaidBtnVisible
         {
             get { return _isPaidBtnVisible; }
             set { _isPaidBtnVisible = value; }
         }

         private int _branchId;
         [DataMember]
         public int BranchId
         {
             get { return _branchId; }
             set { _branchId = value; }
         }

         private List<string> _itemTypeList = new List<string>();
          [DataMember]
         public List<string> ItemTypeList
         {
             get { return _itemTypeList; }
             set { _itemTypeList = value; }
         }

         private string _mobile = string.Empty;
         [DataMember]
         public string Mobile
         {
             get { return _mobile; }
             set { _mobile = value; }
         }

         private int _guardianId;
          [DataMember]
         public int GuardianId
         {
             get { return _guardianId; }
             set { _guardianId = value; }
         }






    }
}
