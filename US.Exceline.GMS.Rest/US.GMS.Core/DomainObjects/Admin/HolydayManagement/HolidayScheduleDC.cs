﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "6/20/2012 10:00:00
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.HolydayManagement
{
    [DataContract]
    public class HolidayScheduleDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _year;
        [DataMember]
        public int Year
        {
            get { return _year; }
            set { _year = value; }
        }

        private int _categoryId;
        [DataMember]
        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }

        private DateTime _startDate;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime _endDate;
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private string _day;
        [DataMember]
        public string Day
        {
            get { return _day; }
            set { _day = value; }
        }

        private string _name = String.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _comment;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _holidayCategoryName;
        [DataMember]
        public string HolidayCategoryName
        {
            get { return _holidayCategoryName; }
            set { _holidayCategoryName = value; }
        }

        private bool _activeStatus;
        [DataMember]
        public bool ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private List<DateTime> _datesList = new List<DateTime>();
        [DataMember]
        public List<DateTime> DatesList
        {
            get { return _datesList; }
            set { _datesList = value; }
        }
    }
}
