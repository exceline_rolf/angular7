﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class GymOpenTimeDC
    {
        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private string _region = string.Empty;
        [DataMember]
        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }

        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private bool _isExpress = false;
        [DataMember]
        public bool IsExpress
        {
            get { return _isExpress; }
            set { _isExpress = value; }
        }

        private DateTime? _startTime = new DateTime();
        [DataMember]
        public DateTime? StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private DateTime? _endTime = new DateTime();
        [DataMember]
        public DateTime? EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        private bool _isMonday = false;
        [DataMember]
        public bool IsMonday
        {
            get { return _isMonday; }
            set { _isMonday = value; }
        }

        private bool _isTuesday = false;
        [DataMember]
        public bool IsTuesday
        {
            get { return _isTuesday; }
            set { _isTuesday = value; }
        }

        private bool _isWednesday = false;
        [DataMember]
        public bool IsWednesday
        {
            get { return _isWednesday; }
            set { _isWednesday = value; }
        }

        private bool _isThursday = false;
        [DataMember]
        public bool IsThursday
        {
            get { return _isThursday; }
            set { _isThursday = value; }
        }

        private bool _isFriday = false;
        [DataMember]
        public bool IsFriday
        {
            get { return _isFriday; }
            set { _isFriday = value; }
        }

        private bool _isSaturday = false;
        [DataMember]
        public bool IsSaturday
        {
            get { return _isSaturday; }
            set { _isSaturday = value; }
        }

        private bool _isSunday = false;
        [DataMember]
        public bool IsSunday
        {
             get { return _isSunday; }
             set { _isSunday = value; }
        }

        private string _days = string.Empty;
        [DataMember]
        public string Days
        {
            get { return _days; }
            set { _days = value; }
        }
    }
}
