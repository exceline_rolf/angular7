﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class ContractCondition
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _condition = string.Empty;
        [DataMember]
        public string Condition
        {
            get { return _condition; }
            set { _condition = value; }
        }

        private bool _isDefault;
        [DataMember]
        public bool IsDefault
        {
            get { return _isDefault; }
            set { _isDefault = value; }
        }

        private bool _isDelete;
        [DataMember]
        public bool IsDelete
        {
            get { return _isDelete; }
            set { _isDelete = value; }
        }

        private string _user = string.Empty;
        [DataMember]
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

    }
}
