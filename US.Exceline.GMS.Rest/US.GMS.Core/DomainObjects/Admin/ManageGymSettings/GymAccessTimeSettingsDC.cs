﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class GymAccessTimeSettingsDC
    {
        private List<ExceAccessProfileDC> _accessProfileList = new List<ExceAccessProfileDC>();
        [DataMember]
        public List<ExceAccessProfileDC> AccessProfileList
        {
            get { return _accessProfileList; }
            set { _accessProfileList = value; }
        }
        private List<GymOpenTimeDC> _gymOpenTimes = new List<GymOpenTimeDC>();
        [DataMember]
        public List<GymOpenTimeDC> GymOpenTimes
        {
            get { return _gymOpenTimes; }
            set { _gymOpenTimes = value; }
        }

        private bool _isExpress = false;
        [DataMember]
        public bool IsExpress
        {
            get { return _isExpress; }
            set { _isExpress = value; }
        }
    }
}
