﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class GymSmsSettingsDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private int _sMSID = -1;
        [DataMember]
        public int SMSID
        {
            get { return _sMSID; }
            set { _sMSID = value; }
        }

        private string _senderName = string.Empty;
        [DataMember]
        public string SenderName
        {
            get { return _senderName; }
            set { _senderName = value; }
        }

        private string _followUp = string.Empty;
        [DataMember]
        public string FollowUp
        {
            get { return _followUp; }
            set { _followUp = value; }
        }

        private int _lastvisitInterval = -1;
        [DataMember]
        public int LastvisitInterval
        {
            get { return _lastvisitInterval; }
            set { _lastvisitInterval = value; }
        }

        private bool _isBirthday = false;
        [DataMember]
        public bool IsBirthday
        {
            get { return _isBirthday; }
            set { _isBirthday = value; }
        }

        private bool _isBookingReminder = false;
        [DataMember]
        public bool IsBookingReminder
        {
            get { return _isBookingReminder; }
            set { _isBookingReminder = value; }
        }

        private int _aTGNotApprovedInterval1 = -1;
        [DataMember]
        public int ATGNotApprovedInterval1
        {
            get { return _aTGNotApprovedInterval1; }
            set { _aTGNotApprovedInterval1 = value; }
        }

        private int _aTGNotApprovedInterval2 = -1;
        [DataMember]
        public int ATGNotApprovedInterval2
        {
            get { return _aTGNotApprovedInterval2; }
            set { _aTGNotApprovedInterval2 = value; }
        }

        private int _aTGNotApprovedInterval3 = -1;
        [DataMember]
        public int ATGNotApprovedInterval3
        {
            get { return _aTGNotApprovedInterval3; }
            set { _aTGNotApprovedInterval3 = value; }
        }

        private int _contractEnding = -1;
        [DataMember]
        public int ContractEnding
        {
            get { return _contractEnding; }
            set { _contractEnding = value; }
        }

        private int _reminderDaysInterval = -1;
        [DataMember]
        public int ReminderDaysInterval
        {
            get { return _reminderDaysInterval; }
            set { _reminderDaysInterval = value; }
        }

        private int _novisitsIntervalMessage1 = -1;
        [DataMember]
        public int NovisitsIntervalMessage1
        {
            get { return _novisitsIntervalMessage1; }
            set { _novisitsIntervalMessage1 = value; }
        }

        private int _novisitsIntervalMessage2 = -1;
        [DataMember]
        public int NovisitsIntervalMessage2
        {
            get { return _novisitsIntervalMessage2; }
            set { _novisitsIntervalMessage2 = value; }
        }

        private int _daysBeforeFreezeEnds = -1;
        [DataMember]
        public int DaysBeforeFreezeEnds
        {
            get { return _daysBeforeFreezeEnds; }
            set { _daysBeforeFreezeEnds = value; }
        }

        private bool _isPriceChange = false;
        [DataMember]
        public bool IsPriceChange
        {
            get { return _isPriceChange; }
            set { _isPriceChange = value; }
        }

        private bool _isReorderingShop = false;
        [DataMember]
        public bool IsReorderingShop
        {
            get { return _isReorderingShop; }
            set { _isReorderingShop = value; }
        }

        private bool _isAccessFreeze = false;
        [DataMember]
        public bool IsAccessFreeze
        {
            get { return _isAccessFreeze; }
            set { _isAccessFreeze = value; }
        }

        private bool _isAccessOutsideProfile = false;
        [DataMember]
        public bool IsAccessOutsideProfile
        {
            get { return _isAccessOutsideProfile; }
            set { _isAccessOutsideProfile = value; }
        }

        private bool _isAccessClosedPeriod = false;
        [DataMember]
        public bool IsAccessClosedPeriod
        {
            get { return _isAccessClosedPeriod; }
            set { _isAccessClosedPeriod = value; }
        }

        private bool _isAccessClosedOutsideOpenHours = false;
        [DataMember]
        public bool IsAccessClosedOutsideOpenHours
        {
            get { return _isAccessClosedOutsideOpenHours; }
            set { _isAccessClosedOutsideOpenHours = value; }
        }

        private bool _isAccessDuebalance = false;
        [DataMember]
        public bool IsAccessDuebalance
        {
            get { return _isAccessDuebalance; }
            set { _isAccessDuebalance = value; }
        }

        private bool _isAccessDueUnpaid = false;
        [DataMember]
        public bool IsAccessDueUnpaid
        {
            get { return _isAccessDueUnpaid; }
            set { _isAccessDueUnpaid = value; }
        }

        private bool _isAccessDueOnAccount = false;
        [DataMember]
        public bool IsAccessDueOnAccount
        {
            get { return _isAccessDueOnAccount; }
            set { _isAccessDueOnAccount = value; }
        }

        private bool _isAccessRecentlyVisit = false;
        [DataMember]
        public bool IsAccessRecentlyVisit
        {
            get { return _isAccessRecentlyVisit; }
            set { _isAccessRecentlyVisit = value; }
        }

        private bool _isAccessInfoAntiDoping = false;
        [DataMember]
        public bool IsAccessInfoAntiDoping
        {
            get { return _isAccessInfoAntiDoping; }
            set { _isAccessInfoAntiDoping = value; }
        }

        private bool _isAccessInfoContractEnds = false;
        [DataMember]
        public bool IsAccessInfoContractEnds
        {
            get { return _isAccessInfoContractEnds; }
            set { _isAccessInfoContractEnds = value; }
        }

        private int _accessInfoPunchesLowestNo = -1;
        [DataMember]
        public int AccessInfoPunchesLowestNo
        {
            get { return _accessInfoPunchesLowestNo; }
            set { _accessInfoPunchesLowestNo = value; }
        }

        private bool _isAccessInfoIsFollowUpPopUp = false;
        [DataMember]
        public bool IsAccessInfoIsFollowUpPopUp
        {
            get { return _isAccessInfoIsFollowUpPopUp; }
            set { _isAccessInfoIsFollowUpPopUp = value; }
        }

        private bool _isAccessInfoInformTrainer = false;
        [DataMember]
        public bool IsAccessInfoInformTrainer
        {
            get { return _isAccessInfoInformTrainer; }
            set { _isAccessInfoInformTrainer = value; }
        }

        private bool _isExpress = false;
        [DataMember]
        public bool IsExpress
        {
            get { return _isExpress; }
            set { _isExpress = value; }
        }

        private string _region = String.Empty;
        [DataMember]
        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }

        private List<ExceSMSProhibitedDayDC> _prohibittedTimes = new List<ExceSMSProhibitedDayDC>();
        [DataMember]
        public List<ExceSMSProhibitedDayDC> ProhibittedTimes
        {
            get { return _prohibittedTimes; }
            set { _prohibittedTimes = value; }
        }

        private List<ExceSMSSendingTimeDC> _sendingTimes = new List<ExceSMSSendingTimeDC>();
        [DataMember]
        public List<ExceSMSSendingTimeDC> SendingTimes
        {
            get { return _sendingTimes; }
            set { _sendingTimes = value; }
        }
    }
}
