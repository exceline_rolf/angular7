﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class ExceAccessProfileDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private bool _isEdited = false;
        [DataMember]
        public bool IsEdited
        {
            get { return _isEdited; }
            set { _isEdited = value; }
        }

        private bool _isDeleted = false;
        [DataMember]
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }

        private string _accessProfileName = string.Empty;
        [DataMember]
        public string AccessProfileName
        {
            get { return _accessProfileName; }
            set { _accessProfileName = value; }
        }

        private List<ExceAccessProfileTimeDC> _accessTimeList = new List<ExceAccessProfileTimeDC>();
        [DataMember]
        public List<ExceAccessProfileTimeDC> AccessTimeList
        {
            get { return _accessTimeList; }
            set { _accessTimeList = value; }
        }

        private bool _isActive = false;
        [DataMember]
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        private bool _allowExpressGym = false;
        [DataMember]
        public bool AllowExpressGym
        {
            get { return _allowExpressGym; }
            set { _allowExpressGym = value; }
        }

        private bool _allowExtraActivity = false;
        [DataMember]
        public bool AllowExtraActivity
        {
            get { return _allowExtraActivity; }
            set { _allowExtraActivity = value; }
        }

        private string _assignedDays = string.Empty;
        [DataMember]
        public string AssignedDays
        {
            get { return _assignedDays; }
            set { _assignedDays = value; }
        }

        private List<int> _branchIdList = new List<int>();
        [DataMember]
        public List<int> BranchIdList
        {
            get { return _branchIdList; }
            set { _branchIdList = value; }
        }

        private List<int> _regionIdList = new List<int>();
        [DataMember]
        public List<int> RegionIdList
        {
            get { return _regionIdList; }
            set { _regionIdList = value; }
        }

        private List<string> _countryIdList = new List<string>();
        [DataMember]
        public List<string> CountryIdList
        {
            get { return _countryIdList; }
            set { _countryIdList = value; }
        }

        private bool _isProfileSelect;
        [DataMember]
        public bool IsProfileSelect
        {
            get { return _isProfileSelect; }
            set { _isProfileSelect = value; }
        }

        private bool _isDrawPuncheSelect;
         [DataMember]
        public bool IsDrawPuncheSelect
        {
            get { return _isDrawPuncheSelect; }
            set { _isDrawPuncheSelect = value; }
        }

        private Gender _gender = Gender.ALL;
        [DataMember]
        public Gender Gender
        {
             get { return _gender; }
             set { _gender = value; }
        }
    }
}
