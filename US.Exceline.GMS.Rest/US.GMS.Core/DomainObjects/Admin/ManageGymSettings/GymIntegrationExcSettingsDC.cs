﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
   public class GymIntegrationExcSettingsDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

       private string _name = string.Empty;
       [DataMember]
       public string Name
       {
           get { return _name; }
           set { _name = value; }
       }

        private int _status;
        [DataMember]
        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private DateTime? _installedDate;
        [DataMember]
        public DateTime? InstalledDate
        {
            get { return _installedDate; }
            set { _installedDate = value; }
        }

        private DateTime? _lastPingTime;
        [DataMember]
        public DateTime? LastPingTime
        {
            get { return _lastPingTime; }
            set { _lastPingTime = value; }
        }

        private int _activityId;
        [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        private string _activityName = string.Empty;
        [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        private List<ExceAccessProfileDC> _accessProfileList = new List<ExceAccessProfileDC>();
        [DataMember]
        public List<ExceAccessProfileDC> AccessProfileList
        {
            get { return _accessProfileList; }
            set { _accessProfileList = value; }
        }

        private string _accessContolTypes = string.Empty;
        [DataMember]
        public string AccessContolTypes
        {
            get { return _accessContolTypes; }
            set { _accessContolTypes = value; }
        }

        private bool _isOpenAccessProfile;
        [DataMember]
        public bool IsOpenAccessProfile
        {
            get { return _isOpenAccessProfile; }
            set { _isOpenAccessProfile = value; }
        }

        
    }
}
