﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class ExceAccessProfileTimeDC
    {

        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }


        private int _accessProfileId = -1;
        [DataMember]
        public int AccessProfileId
        {
            get { return _accessProfileId; }
            set { _accessProfileId = value; }
        }

        private string _assignedDays = string.Empty;
        [DataMember]
        public string AssignedDays
        {
            get { return _assignedDays; }
            set { _assignedDays = value; }
        }

        private string _accessProfileName = string.Empty;
        [DataMember]
        public string AccessProfileName
        {
            get { return _accessProfileName; }
            set { _accessProfileName = value; }
        }

        private DateTime? _fromTime;
        [DataMember]
        public DateTime? FromTime
        {
            get { return _fromTime; }
            set { _fromTime = value; }
        }

        private DateTime? _toTime;
        [DataMember]
        public DateTime? ToTime
        {
            get { return _toTime; }
            set { _toTime = value; }
        }

        private bool _monday = false;
        [DataMember]
        public bool Monday
        {
            get { return _monday; }
            set { _monday = value; }
        }

        private bool _tuesday = false;
        [DataMember]
        public bool Tuesday
        {
            get { return _tuesday; }
            set { _tuesday = value; }
        }

        private bool _wednesday = false;
        [DataMember]
        public bool Wednesday
        {
            get { return _wednesday; }
            set { _wednesday = value; }
        }

        private bool _thursday = false;
        [DataMember]
        public bool Thursday
        {
            get { return _thursday; }
            set { _thursday = value; }
        }

        private bool _friday = false;
        [DataMember]
        public bool Friday
        {
            get { return _friday; }
            set { _friday = value; }
        }

        private bool _saturday = false;
        [DataMember]
        public bool Saturday
        {
            get { return _saturday; }
            set { _saturday = value; }
        }

        private bool _sunday = false;
        [DataMember]
        public bool Sunday
        {
            get { return _sunday; }
            set { _sunday = value; }
        }

        private bool _allowExpressGym = false;
        [DataMember]
        public bool AllowExpressGym
        {
            get { return _allowExpressGym; }
            set { _allowExpressGym = value; }
        }

        private bool _allowExtraActivity = false;
        [DataMember]
        public bool AllowExtraActivity
        {
            get { return _allowExtraActivity; }
            set { _allowExtraActivity = value; }
        } 
    }
}
