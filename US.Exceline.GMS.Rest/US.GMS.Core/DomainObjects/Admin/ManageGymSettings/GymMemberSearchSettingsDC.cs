﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class GymMemberSearchSettingsDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private string _memberSearchText = string.Empty;
        [DataMember]
        public string MemberSearchText
        {
            get { return _memberSearchText; }
            set { _memberSearchText = value; }
        }

        private string _region = string.Empty;
        [DataMember]
        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }

        private bool _isExpress = false;
        [DataMember]
        public bool IsExpress
        {
            get { return _isExpress; }
            set { _isExpress = value; }
        }

    }
}
