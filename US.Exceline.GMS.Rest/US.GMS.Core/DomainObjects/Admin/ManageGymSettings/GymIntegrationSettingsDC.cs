﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class GymIntegrationSettingsDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _port = -1;
        [DataMember]
        public int Port
        {
            get { return _port; }
            set { _port = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _terminalName = string.Empty;
        [DataMember]
        public string TerminalName
        {
            get { return _terminalName; }
            set { _terminalName = value; }
        }

        private string _location = string.Empty;
        [DataMember]
        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        private TerminalType _terminalType = new TerminalType();
        [DataMember]
        public TerminalType TerminalType
        {
            get { return _terminalType; }
            set { _terminalType = value; }
        }

        [DataMember]
        public bool IsOnline { get; set; }

        [DataMember]
        public bool IsSecondIdentification { get; set; }

        private string _accessContolTypes = string.Empty;
        [DataMember]
        public string AccessContolTypes
        {
            get { return _accessContolTypes; }
            set { _accessContolTypes = value; }
        }

        [DataMember]
        public bool IsPaid { get; set; }

        [DataMember]
        public string ArticleNo { get; set; }

        [DataMember]
        public string ArticleName { get; set; }

        private List<ExceAccessProfileDC> _accessProfileList = new List<ExceAccessProfileDC>();
        [DataMember]
        public List<ExceAccessProfileDC> AccessProfileList
        {
            get { return _accessProfileList; }
            set { _accessProfileList = value; }
        }

        private bool _isOpenAccessProfile;
        [DataMember]
        public bool IsOpenAccessProfile
        {
            get { return _isOpenAccessProfile; }
            set { _isOpenAccessProfile = value; }
        }

        private int _activityId;
        [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

    }

    [DataContract]
    public class TerminalType
    {
        [DataMember]
        public string TypeId { get; set; }

        [DataMember]
        public string TypeName { get; set; }

        [DataMember]
        public int DefaultPort { get; set; }
    }

}
