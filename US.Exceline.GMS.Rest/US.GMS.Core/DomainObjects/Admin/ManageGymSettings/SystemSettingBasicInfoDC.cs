﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class SystemSettingBasicInfoDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _gymCompanyID = -1;
        [DataMember]
        public int GymCompanyID
        {
            get { return _gymCompanyID; }
            set { _gymCompanyID = value; }
        }

        private string _gymCompanyName = string.Empty;
        [DataMember]
        public string GymCompanyName
        {
            get { return _gymCompanyName; }
            set { _gymCompanyName = value; }
        }

        private string _address1 = string.Empty;
        [DataMember]
        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        private string _address2 = string.Empty;
        [DataMember]
        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        private string _address3 = string.Empty;
        [DataMember]
        public string Address3
        {
            get { return _address3; }
            set { _address3 = value; }
        }

        private string _postalCode = string.Empty;
        [DataMember]
        public string PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }

        private string _postalPlace = string.Empty;
        [DataMember]
        public string PostalPlace
        {
            get { return _postalPlace; }
            set { _postalPlace = value; }
        }

        private string _contactPerson = string.Empty;
        [DataMember]
        public string ContactPerson
        {
            get { return _contactPerson; }
            set { _contactPerson = value; }
        }

        private string _email = string.Empty;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _mobile = string.Empty;
        [DataMember]
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        private string _accountNo = string.Empty;
        [DataMember]
        public string AccountNo
        {
            get { return _accountNo; }
            set { _accountNo = value; }
        }

        private string _otherAdminMobile = string.Empty;
         [DataMember]
        public string OtherAdminMobile
        {
            get { return _otherAdminMobile; }
            set { _otherAdminMobile = value; }
        }

         private string _otherAdminEmail = string.Empty;
         [DataMember]
        public string OtherAdminEmail
        {
            get { return _otherAdminEmail; }
            set { _otherAdminEmail = value; }
        }

        

    }
}
