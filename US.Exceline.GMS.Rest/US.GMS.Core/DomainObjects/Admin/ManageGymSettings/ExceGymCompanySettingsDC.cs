﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class ExceGymCompanySettingsDC
    {
        private List<VatCodeDC> _vatCodes = new List<VatCodeDC>();
        [DataMember]
        public List<VatCodeDC> VatCodes
        {
            get { return _vatCodes; }
            set { _vatCodes = value; }
        }

        private List<GCSOtherSettingsDC> _otherSettings = new List<GCSOtherSettingsDC>();
        [DataMember]
        public List<GCSOtherSettingsDC> OtherSettings
        {
            get { return _otherSettings; }
            set { _otherSettings = value; }
        }

        private SystemSettingBasicInfoDC _systemSettingBasicInfo = new SystemSettingBasicInfoDC();
        [DataMember]
        public SystemSettingBasicInfoDC SystemSettingBasicInfo
        {
            get { return _systemSettingBasicInfo; }
            set { _systemSettingBasicInfo = value; }
        }

        private SystemSettingEconomySettingDC _systemSettingEconomySetting = new SystemSettingEconomySettingDC();
        [DataMember]
        public SystemSettingEconomySettingDC SystemSettingEconomySetting
        {
            get { return _systemSettingEconomySetting; }
            set { _systemSettingEconomySetting = value; }
        }

        private SystemSettingOtherSettingDC _systemSettingOtherSetting = new SystemSettingOtherSettingDC();
        [DataMember]
        public SystemSettingOtherSettingDC SystemSettingOtherSetting
        {
            get { return _systemSettingOtherSetting; }
            set { _systemSettingOtherSetting = value; }
        }

        private SystemSettingContractConditionDC _systemSettingContractCondition = new SystemSettingContractConditionDC();
        [DataMember]
        public SystemSettingContractConditionDC SystemSettingContractCondition
        {
            get { return _systemSettingContractCondition; }
            set { _systemSettingContractCondition = value; }
        }
    }
}
