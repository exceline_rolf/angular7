﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class ExceMonthlyEnrollmentDC
    {
        private bool _isMonthSelected = false;
        [DataMember]
        public bool IsMonthSelected
        {
            get { return _isMonthSelected; }
            set { _isMonthSelected = value; }
        }

        private CategoryDC _month = new CategoryDC();
        [DataMember]
        public CategoryDC Month
        {
            get { return _month; }
            set { _month = value; }
        }

        private decimal _percentage = 0;
        [DataMember]
        public decimal Percentage
        {
            get { return _percentage; }
            set { _percentage = value; }
        }
    }
}
