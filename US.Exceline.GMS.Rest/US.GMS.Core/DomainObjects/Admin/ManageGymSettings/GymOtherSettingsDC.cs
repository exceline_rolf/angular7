﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class GymOtherSettingsDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }
        
        private int _timeOutAfter = -1;
        [DataMember]
        public int TimeOutAfter
        {
          get { return _timeOutAfter; }
          set { _timeOutAfter = value; }
        }

        private string _phoneCode = string.Empty;
        [DataMember]
        public string PhoneCode
        {
            get { return _phoneCode; }
            set { _phoneCode = value; }
        }

        private bool _isShopAvailable = false;
        [DataMember]
        public bool IsShopAvailable
        {
            get { return _isShopAvailable; }
            set { _isShopAvailable = value; }
        }

        private bool _isAntiDopingRequired = false;
        [DataMember]
        public bool IsAntiDopingRequired
        {
            get { return _isAntiDopingRequired; }
            set { _isAntiDopingRequired = value; }
        }

        private bool _categoryFollowUpNoVisits = false;
        [DataMember]
        public bool CategoryFollowUpNoVisits
        {
            get { return _categoryFollowUpNoVisits; }
            set { _categoryFollowUpNoVisits = value; }
        }

        private bool _followUpNewPerson = false;
        [DataMember]
        public bool FollowUpNewPerson
        {
            get { return _followUpNewPerson; }
            set { _followUpNewPerson = value; }
        }

        private bool _isPrintATGAuthorization = false;
        [DataMember]
        public bool IsPrintATGAuthorization
        {
            get { return _isPrintATGAuthorization; }
            set { _isPrintATGAuthorization = value; }
        }

        private bool _isFreezeConfirmationPopUp = false;
        [DataMember]
        public bool IsFreezeConfirmationPopUp
        {
            get { return _isFreezeConfirmationPopUp; }
            set { _isFreezeConfirmationPopUp = value; }
        }

        private bool _isLoginShop = false;
        [DataMember]
        public bool IsLoginShop
        {
            get { return _isLoginShop; }
            set { _isLoginShop = value; }
        }

        private bool _isExpress = false;
        [DataMember]
        public bool IsExpress
        {
            get { return _isExpress; }
            set { _isExpress = value; }
        }

        private bool _isLogoutFromShopAfterSale = false;
        [DataMember]
        public bool IsLogoutFromShopAfterSale
        {
            get { return _isLogoutFromShopAfterSale; }
            set { _isLogoutFromShopAfterSale = value; }
        }

        private string _region = string.Empty;
        [DataMember]
        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }

     
        private decimal _gmt;
         [DataMember]
        public decimal Gmt
        {
            get { return _gmt; }
            set { _gmt = value; }
        }

        private bool _isPrieviewWhenPrint = false;
        [DataMember]
        public bool IsPrieviewWhenPrint
        {
            get { return _isPrieviewWhenPrint; }
            set { _isPrieviewWhenPrint = value; }
        }

        private int _anonymizingTimePeriod = -1;
        [DataMember]
        public int AnonymizingTimePeriod
        {
            get { return _anonymizingTimePeriod; }
            set { _anonymizingTimePeriod = value; }
        }

        private bool _isBookingBackDate;
         [DataMember]
        public bool IsBookingBackDate
        {
            get { return _isBookingBackDate; }
            set { _isBookingBackDate = value; }
        }         

         private bool _isCalendarPriority;
         [DataMember]
         public bool IsCalendarPriority
         {
             get { return _isCalendarPriority; }
             set { _isCalendarPriority = value; }
         }

         private bool _isIBookingIntegrated = false;
         [DataMember]
         public bool IsIBookingIntegrated
         {
             get { return _isIBookingIntegrated; }
             set { _isIBookingIntegrated = value; }
         }

         private bool _isValidateShopGymId = false;
         [DataMember]
         public bool IsValidateShopGymId
         {
             get { return _isValidateShopGymId; }
             set { _isValidateShopGymId = value; }
         }

         private bool _isCommunicatorAvailable = false;
         [DataMember]
         public bool IsCommunicatorAvailable
         {
             get { return _isCommunicatorAvailable; }
             set { _isCommunicatorAvailable = value; }
         }

        private bool _IsUsingIntroducedBy = false;
        [DataMember]
        public bool IsUsingIntroducedBy
        {
            get { return _IsUsingIntroducedBy; }
            set { _IsUsingIntroducedBy = value; }
        }

        private bool _SortBranchesById = false;
        [DataMember]
        public bool SortBranchesById
        {
            get { return _SortBranchesById; }
            set { _SortBranchesById = value; }
        }

        private bool _IsUsingGantner = false;
        [DataMember]
        public bool IsUsingGantner
        {
            get { return _IsUsingGantner; }
            set { _IsUsingGantner = value; }
        }
    }
}
