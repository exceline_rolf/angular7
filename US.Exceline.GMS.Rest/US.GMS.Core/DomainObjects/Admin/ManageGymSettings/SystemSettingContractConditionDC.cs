﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class SystemSettingContractConditionDC
    {
        
        private int _iD = -1;
        [DataMember]
        public int ID
        {
            get { return _iD; }
            set { _iD = value; }
        }

        private List<string> _conditions = new List<string>();
        [DataMember]
        public List<string> Conditions
        {
            get { return _conditions; }
            set { _conditions = value; }
        }
    }
}
