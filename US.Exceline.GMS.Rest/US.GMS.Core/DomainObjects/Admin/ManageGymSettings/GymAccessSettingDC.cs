﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class GymAccessSettingDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private int _countVisitAfter = -1;
        [DataMember]
        public int CountVisitAfter
        {
            get { return _countVisitAfter; }
            set { _countVisitAfter = value; }
        }

        private int _blockAccessNoOfUnpaidInvoices = -1;
        [DataMember]
        public int BlockAccessNoOfUnpaidInvoices
        {
            get { return _blockAccessNoOfUnpaidInvoices; }
            set { _blockAccessNoOfUnpaidInvoices = value; }
        }

        private decimal _blockAccessDueBalance = -1;
        [DataMember]
        public decimal BlockAccessDueBalance
        {
            get { return _blockAccessDueBalance; }
            set { _blockAccessDueBalance = value; }
        }

        private bool _isMemCardExportAsGuestCard = false;
        [DataMember]
        public bool IsMemCardExportAsGuestCard
        {
            get { return _isMemCardExportAsGuestCard; }
            set { _isMemCardExportAsGuestCard = value; }
        }

        private decimal _blockAccessOnAccountBalance = -1;
        [DataMember]
        public decimal BlockAccessOnAccountBalance
        {
            get { return _blockAccessOnAccountBalance; }
            set { _blockAccessOnAccountBalance = value; }
        }

        private bool _isReceptionAllowCustomerScreen = false;
        [DataMember]
        public bool IsReceptionAllowCustomerScreen
        {
            get { return _isReceptionAllowCustomerScreen; }
            set { _isReceptionAllowCustomerScreen = value; }
        }

        private int _minutesBetweenAccess = -1;
        [DataMember]
        public int MinutesBetweenAccess
        {
            get { return _minutesBetweenAccess; }
            set { _minutesBetweenAccess = value; }
        }

        private string _comPort = string.Empty;
        [DataMember]
        public string ComPort
        {
            get { return _comPort; }
            set { _comPort = value; }
        }

        private int _secondsViewCustomerScreen = -1;
        [DataMember]
        public int SecondsViewCustomerScreen
        {
            get { return _secondsViewCustomerScreen; }
            set { _secondsViewCustomerScreen = value; }
        }

        private string _region = string.Empty;
        [DataMember]
        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }

        private bool _isExpress = false;
        [DataMember]
        public bool IsExpress
        {
            get { return _isExpress; }
            set { _isExpress = value; }
        }

        private bool _isExtraActivity = false;
        [DataMember]
        public bool IsExtraActivity
        {
            get { return _isExtraActivity; }
            set { _isExtraActivity = value; }
        }

        private bool _checkFingerPrint = false;
        [DataMember]
        public bool CheckFingerPrint
        {
            get { return _checkFingerPrint; }
            set { _checkFingerPrint = value; }
        }

        private bool _isSendSms;
        [DataMember]
        public bool IsSendSms
        {
            get { return _isSendSms; }
            set { _isSendSms = value; }
        }

        private bool _accIsDisplayName;
          [DataMember]
        public bool AccIsDisplayName
        {
            get { return _accIsDisplayName; }
            set { _accIsDisplayName = value; }
        }

        private bool _accIsDisplayCustomerNumber;
         [DataMember]
        public bool AccIsDisplayCustomerNumber
        {
            get { return _accIsDisplayCustomerNumber; }
            set { _accIsDisplayCustomerNumber = value; }
        }

        private bool _accIsDisplayContractTemplate;
         [DataMember]
        public bool AccIsDisplayContractTemplate
        {
            get { return _accIsDisplayContractTemplate; }
            set { _accIsDisplayContractTemplate = value; }
        }

        private bool _accIsDisplayHomeGym;
          [DataMember]
        public bool AccIsDisplayHomeGym
        {
            get { return _accIsDisplayHomeGym; }
            set { _accIsDisplayHomeGym = value; }
        }

        private bool _accIsDisplayContractEndDate;
        [DataMember]
        public bool AccIsDisplayContractEndDate
        {
            get { return _accIsDisplayContractEndDate; }
            set { _accIsDisplayContractEndDate = value; }
        }

        private bool _accIsDisplayBirthDate;
         [DataMember]
        public bool AccIsDisplayBirthDate
        {
            get { return _accIsDisplayBirthDate; }
            set { _accIsDisplayBirthDate = value; }
        }

         private bool _accIsDisplayAge;
         [DataMember]
        public bool AccIsDisplayAge
        {
            get { return _accIsDisplayAge; }
            set { _accIsDisplayAge = value; }
        }

         private bool _accIsDisplayLastVisit;
        [DataMember]
        public bool AccIsDisplayLastVisit
        {
            get { return _accIsDisplayLastVisit; }
            set { _accIsDisplayLastVisit = value; }
        }

        private bool _accIsDisplayCreditBalance;
           [DataMember]
        public bool AccIsDisplayCreditBalance
        {
            get { return _accIsDisplayCreditBalance; }
            set { _accIsDisplayCreditBalance = value; }
        }

        private bool _accIsDisplayPicture;
         [DataMember]
        public bool AccIsDisplayPicture
        {
            get { return _accIsDisplayPicture; }
            set { _accIsDisplayPicture = value; }
        }

         private bool _accIsDisplayLastAccess;
           [DataMember]
        public bool AccIsDisplayLastAccess
        {
            get { return _accIsDisplayLastAccess; }
            set { _accIsDisplayLastAccess = value; }
        }

           private bool _accIsDisplayNotification;
          [DataMember]
        public bool AccIsDisplayNotification
        {
            get { return _accIsDisplayNotification; }
            set { _accIsDisplayNotification = value; }
        }


       


    }
}
