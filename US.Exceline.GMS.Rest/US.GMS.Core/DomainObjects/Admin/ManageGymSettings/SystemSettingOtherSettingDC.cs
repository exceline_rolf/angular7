﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class SystemSettingOtherSettingDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _classGeneratingDays = string.Empty;
        [DataMember]
        public string ClassGeneratingDays
        {
            get { return _classGeneratingDays; }
            set { _classGeneratingDays = value; }
        }

        private string _noOfCopiesForInvoice = string.Empty;
        [DataMember]
        public string NoOfCopiesForInvoice
        {
            get { return _noOfCopiesForInvoice; }
            set { _noOfCopiesForInvoice = value; }
        }

        private string _netsSendingNo = string.Empty;
        [DataMember]
        public string NetsSendingNo
        {
            get { return _netsSendingNo; }
            set { _netsSendingNo = value; }
        }

        private string _customerNo = string.Empty;
        [DataMember]
        public string CustomerNo
        {
            get { return _customerNo; }
            set { _customerNo = value; }
        }

        private string _contractNo = string.Empty;
        [DataMember]
        public string ContractNo
        {
            get { return _contractNo; }
            set { _contractNo = value; }
        }

        private string _excludingCategories = string.Empty;
        [DataMember]
        public string ExcludingCategories
        {
            get { return _excludingCategories; }
            set { _excludingCategories = value; }
        }

        private bool _IsBrisIntegrated = false;
        [DataMember]
        public bool IsBrisIntegrated
        {
            get { return _IsBrisIntegrated; }
            set { _IsBrisIntegrated = value; }
        }

    }
}
