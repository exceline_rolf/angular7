﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class GymEconomySettingsDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }


        private string _invoiceAccountNo = String.Empty;
        [DataMember]
        public string InvoiceAccountNo
        {
            get { return _invoiceAccountNo; }
            set { _invoiceAccountNo = value; }
        }

        private string _paymentAccountNo = String.Empty;
        [DataMember]
        public string PaymentAccountNo
        {
            get { return _paymentAccountNo; }
            set { _paymentAccountNo = value; }
        }

        private int _suspendedDays = 0;

        [DataMember]
        public int SuspendedDays
        {
            get { return _suspendedDays; }
            set { _suspendedDays = value; }
        }

        private int _resignPeriod = 0;
        [DataMember]
        public int ResignPeriod
        {
            get { return _resignPeriod; }
            set { _resignPeriod = value; }
        }

        private bool _restPlusMonth;

        [DataMember]
        public bool RestPlusMonth
        {
            get { return _restPlusMonth; }
            set { _restPlusMonth = value; }
        }


        private bool _isDailySettlementForAll;
        [DataMember]
        public bool IsDailySettlementForAll
        {
            get { return _isDailySettlementForAll; }
            set { _isDailySettlementForAll = value; }
        }

        private ExceMonthlyEnrollmentDC _temp = new ExceMonthlyEnrollmentDC();
        [DataMember]
        public ExceMonthlyEnrollmentDC Temp
        {
            get { return _temp; }
            set { _temp = value; }
        }

        private int _duePaymentAfter = 0;
        [DataMember]
        public int DuePaymentAfter
        {
            get { return _duePaymentAfter; }
            set { _duePaymentAfter = value; }
        }

        private decimal _invoiceCharge = 0;
        [DataMember]
        public decimal InvoiceCharge
        {
            get { return _invoiceCharge; }
            set { _invoiceCharge = value; }
        }

        private decimal _smsInvoiceCharge = 0;
        [DataMember]
        public decimal SmsInvoiceCharge
        {
            get { return _smsInvoiceCharge; }
            set { _smsInvoiceCharge = value; }
        }

        private int _invoiceCancellationPeriod = 0;
        [DataMember]
        public int InvoiceCancellationPeriod
        {
            get { return _invoiceCancellationPeriod; }
            set { _invoiceCancellationPeriod = value; }
        }

        private bool _moveExceededPayToInvoice = false;
        [DataMember]
        public bool MoveExceededPayToInvoice
        {
            get { return _moveExceededPayToInvoice; }
            set { _moveExceededPayToInvoice = value; }
        }

        private bool _isMinTotalPayable;
        [DataMember]
        public bool IsMinTotalPayable
        {
            get { return _isMinTotalPayable; }
            set { _isMinTotalPayable = value; }
        }

        private bool _isIgnoreInvoiceFee;
        [DataMember]
        public bool IsIgnoreInvoiceFee
        {
            get { return _isIgnoreInvoiceFee; }
            set { _isIgnoreInvoiceFee = value; }
        }

        private bool _isShopLoginNeeded;
        [DataMember]
        public bool IsShopLoginNeeded
        {
            get { return _isShopLoginNeeded; }
            set { _isShopLoginNeeded = value; }
        }

        private decimal _creditSaleLimit;
        [DataMember]
        public decimal CreditSaleLimit
        {
            get { return _creditSaleLimit; }
            set { _creditSaleLimit = value; }
        }

        private bool _isXtraUsed;
        [DataMember]
        public bool IsXtraUsed
        {
            get { return _isXtraUsed; }
            set { _isXtraUsed = value; }
        }


        private int _creditPeriod = -1;
        [DataMember]
        public int CreditPeriod
        {
            get { return _creditPeriod; }
            set { _creditPeriod = value; }
        }

        private int _creditDueDate = 0;
        [DataMember]
        public int CreditDueDate
        {
            get { return _creditDueDate; }
            set { _creditDueDate = value; }
        }

        private bool _useTodayAsDueDate;
        [DataMember]
        public bool UseTodayAsDueDate
        {
            get { return _useTodayAsDueDate; }
            set { _useTodayAsDueDate = value; }
        }

        private bool _isMemCardAsGuestCard;
        [DataMember]
        public bool IsMemCardAsGuestCard
        {
            get { return _isMemCardAsGuestCard; }
            set { _isMemCardAsGuestCard = value; }
        }

        private decimal _creditNoteMinInvoiceFee;
        [DataMember]
        public decimal CreditNoteMinInvoiceFee
        {
            get { return _creditNoteMinInvoiceFee; }
            set { _creditNoteMinInvoiceFee = value; }
        }

        private int _debtCollectionPeriod;
        [DataMember]
        public int DebtCollectionPeriod
        {
            get { return _debtCollectionPeriod; }
            set { _debtCollectionPeriod = value; }
        }

        private int _remainderPeriod = 0;
        [DataMember]
        public int RemainderPeriod
        {
            get { return _remainderPeriod; }
            set { _remainderPeriod = value; }
        }

        private int _smsReminderPeriod = 0;
        [DataMember]
        public int SmsReminderPeriod
        {
            get { return _smsReminderPeriod; }
            set { _smsReminderPeriod = value; }
        }

        private decimal _remainderFee;
        [DataMember]
        public decimal RemainderFee
        {
            get { return _remainderFee; }
            set { _remainderFee = value; }
        }

        private decimal _smsRemainderFee;
        [DataMember]
        public decimal SmsRemainderFee
        {
            get { return _smsRemainderFee; }
            set { _smsRemainderFee = value; }
        }

        private int _noOfOrdersWhenResign;
        [DataMember]
        public int NoOfOrdersWhenResign
        {
            get { return _noOfOrdersWhenResign; }
            set { _noOfOrdersWhenResign = value; }
        }

        private bool _creditUnpaidInvoiceFee;
        [DataMember]
        public bool CreditUnpaidInvoiceFee
        {
            get { return _creditUnpaidInvoiceFee; }
            set { _creditUnpaidInvoiceFee = value; }
        }

        private bool _creditUnpaidRemainderFee;
        [DataMember]
        public bool CreditUnpaidRemainderFee
        {
            get { return _creditUnpaidRemainderFee; }
            set { _creditUnpaidRemainderFee = value; }
        }

        private decimal _creditPaidDeviationSettableAmount;
        [DataMember]
        public decimal CreditPaidDeviationSettableAmount
        {
            get { return _creditPaidDeviationSettableAmount; }
            set { _creditPaidDeviationSettableAmount = value; }
        }

        private bool _pettyCashAllowedOnUser;
        [DataMember]
        public bool PettyCashAllowedOnUser
        {
            get { return _pettyCashAllowedOnUser; }
            set { _pettyCashAllowedOnUser = value; }
        }

        private bool _shopOnNextOrder;
        [DataMember]
        public bool ShopOnNextOrder
        {
            get { return _shopOnNextOrder; }
            set { _shopOnNextOrder = value; }
        }

        private bool _onAccount;
        [DataMember]
        public bool OnAccount
        {
            get { return _onAccount; }
            set { _onAccount = value; }
        }

        private decimal _onNegativeAccount;
        [DataMember]
        public decimal OnNegativeAccount
        {
            get { return _onNegativeAccount; }
            set { _onNegativeAccount = value; }
        }

        private bool _isDefaultCustomerAvailable;
        [DataMember]
        public bool IsDefaultCustomerAvailable
        {
            get { return _isDefaultCustomerAvailable; }
            set { _isDefaultCustomerAvailable = value; }
        }

        private bool _isPayButtonAvailable;
        [DataMember]
        public bool IsPayButtonAvailable
        {
            get { return _isPayButtonAvailable; }
            set { _isPayButtonAvailable = value; }
        }

        private bool _isPettyCashAllowedOnUser;
        [DataMember]
        public bool IsPettyCashAllowedOnUser
        {
            get { return _isPettyCashAllowedOnUser; }
            set { _isPettyCashAllowedOnUser = value; }
        }

        private bool _isMemberFee;
        [DataMember]
        public bool IsMemberFee
        {
            get { return _isMemberFee; }
            set { _isMemberFee = value; }
        }

        private bool _isCashSalesActived;
        [DataMember]
        public bool IsCashSalesActivated
        {
            get { return _isCashSalesActived; }
            set { _isCashSalesActived = value; }
        }

        private int _memberFeeGenerateDay = 1;
        [DataMember]
        public int MemberFeeGenerateDay
        {
            get { return _memberFeeGenerateDay; }
            set { _memberFeeGenerateDay = value; }
        }

        private int _memberFeeBranchID = -1;
        [DataMember]
        public int MemberFeeBranchID
        {
            get { return _memberFeeBranchID; }
            set { _memberFeeBranchID = value; }
        }

        private bool _isMemberFeeMonth;
        [DataMember]
        public bool IsMemberFeeMonth
        {
            get { return _isMemberFeeMonth; }
            set { _isMemberFeeMonth = value; }
        }

        private bool _isMemberFeeAutoGenerate;
        [DataMember]
        public bool IsMemberFeeAutoGenerate
        {
            get { return _isMemberFeeAutoGenerate; }
            set { _isMemberFeeAutoGenerate = value; }
        }

        private bool _isMemberFeeATG;
        [DataMember]
        public bool IsMemberFeeATG
        {
            get { return _isMemberFeeATG; }
            set { _isMemberFeeATG = value; }
        }

        private string _netsCustomerId;
        [DataMember]
        public string NetsCustomerId
        {
            get { return _netsCustomerId; }
            set { _netsCustomerId = value; }
        }

        private string _netsAssignmentNo;
        [DataMember]
        public string NetsAssignmentNo
        {
            get { return _netsAssignmentNo; }
            set { _netsAssignmentNo = value; }
        }

        private int _netsCancellationPeriod;
        [DataMember]
        public int NetsCancellationPeriod
        {
            get { return _netsCancellationPeriod; }
            set { _netsCancellationPeriod = value; }
        }

        private bool _isBankTerminalIntegrated;
        [DataMember]
        public bool IsBankTerminalIntegrated
        {
            get { return _isBankTerminalIntegrated; }
            set { _isBankTerminalIntegrated = value; }
        }

        private bool _isPrintInvoiceInShop;
        [DataMember]
        public bool IsPrintInvoiceInShop
        {
            get { return _isPrintInvoiceInShop; }
            set { _isPrintInvoiceInShop = value; }
        }

        private bool _isPrintReceiptInShop;
        [DataMember]
        public bool IsPrintReceiptInShop
        {
            get { return _isPrintReceiptInShop; }
            set { _isPrintReceiptInShop = value; }
        }

        private bool _smsInvoice;
        [DataMember]
        public bool SmsInvoice
        {
            get { return _smsInvoice; }
            set { _smsInvoice = value; }
        }

        private bool _isShopOnNextOrder;
        [DataMember]
        public bool IsShopOnNextOrder
        {
            get { return _isShopOnNextOrder; }
            set { _isShopOnNextOrder = value; }
        }

        private bool _enrollmentFeeFirstOrderIfSponsor;
        [DataMember]
        public bool EnrollmentFeeFirstOrderIfSponsor
        {
            get { return _enrollmentFeeFirstOrderIfSponsor; }
            set { _enrollmentFeeFirstOrderIfSponsor = value; }
        }

        private bool _isExpress = false;
        [DataMember]
        public bool IsExpress
        {
            get { return _isExpress; }
            set { _isExpress = value; }
        }

        private string _region = string.Empty;
        [DataMember]
        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }

        private bool _isSmsInvoiceShop = false;
        [DataMember]
        public bool IsSmsInvoiceShop
        {
            get { return _isSmsInvoiceShop; }
            set { _isSmsInvoiceShop = value; }
        }

        private byte _paidAccessMode;
        [DataMember]
        public byte PaidAccessMode
        {
            get { return _paidAccessMode; }
            set { _paidAccessMode = value; }
        }

        private bool _moveToPrepaidBalanace = false;
        [DataMember]
        public bool MoveToPrepaidBalanace
        {
            get { return _moveToPrepaidBalanace; }
            set { _moveToPrepaidBalanace = value; }
        }

        private bool _moveToShopAccount = false;
        [DataMember]
        public bool MoveToShopAccount
        {
            get { return _moveToShopAccount; }
            set { _moveToShopAccount = value; }
        }

        private bool _editRemainingPunches;
        [DataMember]
        public bool EditRemainingPunches
        {
            get { return _editRemainingPunches; }
            set { _editRemainingPunches = value; }
        }

        private bool _CCXEnabled = false;
        [DataMember]
        public bool CCXEnabled
        {
            get { return _CCXEnabled; }
            set { _CCXEnabled = value; }
        }

        private bool _printIpccx;
         [DataMember]
        public bool PrintIpccx
        {
            get { return _printIpccx; }
            set { _printIpccx = value; }
        }

         private bool _printPpccx;
        [DataMember]
        public bool PrintPpccx
        {
            get { return _printPpccx; }
            set { _printPpccx = value; }
        }

        private bool _printSpccx;
         [DataMember]
        public bool PrintSpccx
        {
            get { return _printSpccx; }
            set { _printSpccx = value; }
        }

         private bool _sendSMSforPriceChange;
         [DataMember]
         public bool SendSmSforPriceChange
         {
             get { return _sendSMSforPriceChange; }
             set { _sendSMSforPriceChange = value; }
         }

         private bool _postPay = false;
         [DataMember]
         public bool PostPay
         {
             get { return _postPay; }
             set { _postPay = value; }
         }

        private bool _basisX = false;
        [DataMember]
        public bool BasisX
        {
            get { return _basisX; }
            set { _basisX = value; }
        }

        private bool _notEditableVoucher = false;
        [DataMember]
        public bool NotEditableVoucher
        {
            get { return _notEditableVoucher; }
            set { _notEditableVoucher = value; }
        }

        private bool _shopReturnOnSelectedBranchOnly = false;
        [DataMember]
        public bool ShopReturnOnSelectedBranchOnly
        {
            get { return _shopReturnOnSelectedBranchOnly; }
            set { _shopReturnOnSelectedBranchOnly = value; }
        }
    }
}
