﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.GMS.Core.DomainObjects.Admin.ManageEmployees
{
    [DataContract]
    public class EmployeeBookingDC
    {
        private int _employeeId;
        [DataMember]
        public int EmployeeId
        {
            get { return _employeeId; }
            set { _employeeId = value; }
        }

        private string _day = string.Empty;
        [DataMember]
        public string Day
        {
            get { return _day; }
            set { _day = value; }
        }

        private DateTime _date;
        [DataMember]
        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private DateTime _time = new DateTime();
        [DataMember]
        public DateTime Time
        {
            get { return _time; }
            set { _time = value; }
        }

        private DateTime _startTime = new DateTime();
        [DataMember]
        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private DateTime _endTime = new DateTime();
        [DataMember]
        public DateTime EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        private string _resource = string.Empty;
        [DataMember]
        public string Resource
        {
            get { return _resource; }
            set { _resource = value; }
        }

        private List<ExcelineMemberDC> _memberList = new List<ExcelineMemberDC>();
        [DataMember]
        public List<ExcelineMemberDC> MemberList
        {
            get { return _memberList; }
            set { _memberList = value; }
        }

        private bool _isApproved = false;
        [DataMember]
        public bool IsApproved
        {
            get { return _isApproved; }
            set { _isApproved = value; }
        }
        
        private string _activity = string.Empty;
        [DataMember]
        public string Activity
        {
            get { return _activity; }
            set { _activity = value; }
        }

        private string _category = string.Empty;
        [DataMember]
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        private string _extraResource = string.Empty;
        [DataMember]
        public string ExtraResource
        {
            get { return _extraResource; }
            set { _extraResource = value; }
        }

        private bool _salaryPerBooking;
        [DataMember]
        public bool SalaryPerBooking
        {
            get { return _salaryPerBooking; }
            set { _salaryPerBooking = value; }
        }

        private int _scheduleId = -1;
        [DataMember]
        public int ScheduleId
        {
            get { return _scheduleId; }
            set { _scheduleId = value; }
        }

        private int _scheduleItemId = -1;
        [DataMember]
        public int ScheduleItemId
        {
            get { return _scheduleItemId; }
            set { _scheduleItemId = value; }
        }

        List<ResourceDC> _resourceList = new List<ResourceDC>();
        [DataMember]
        public List<ResourceDC> ResourceList
        {
            get { return _resourceList; }
            set { _resourceList = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName = string.Empty;
         [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        



    }
}
