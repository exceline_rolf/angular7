﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageEmployees
{
    [DataContract]
    public class TimeEntryDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _entityId = -1;
        [DataMember]
        public int EntityId
        {
            get { return _entityId; }
            set { _entityId = value; }
        }

        private string _entityName = string.Empty;
        [DataMember]
        public string EntityName
        {
            get { return _entityName; }
            set { _entityName = value; }
        }


        private string _entityRoleType = string.Empty;
        [DataMember]
        public string EntityRoleType
        {
            get { return _entityRoleType; }
            set { _entityRoleType = value; }
        }

        private int _taskId = -1;
        [DataMember]
        public int TaskId
        {
            get { return _taskId; }
            set { _taskId = value; }
        }

        private string _taskName = string.Empty;
        [DataMember]
        public string TaskName
        {
            get { return _taskName; }
            set { _taskName = value; }
        }

        private DateTime _executedDate;
        [DataMember]
        public DateTime ExecutedDate
        {
            get { return _executedDate; }
            set { _executedDate = value; }
        }

        private decimal _spendTime;
        [DataMember]
        public decimal SpendTime
        {
            get { return _spendTime; }
            set { _spendTime = value; }
        }

        private decimal _timeChange;
        [DataMember]
        public decimal TimeChange
        {
            get { return _timeChange; }
            set { _timeChange = value; }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private string _lastModifiedUser = string.Empty;
        [DataMember]
        public string LastModifiedUser
        {
            get { return _lastModifiedUser; }
            set { _lastModifiedUser = value; }
        }

        private DateTime _createdDateTime = DateTime.Now;
        [DataMember]
        public DateTime CreatedDateTime
        {
            get { return _createdDateTime; }
            set { _createdDateTime = value; }
        }

        private DateTime _lastModifiedDateTime;
        [DataMember]
        public DateTime LastModifiedDateTime
        {
            get { return _lastModifiedDateTime; }
            set { _lastModifiedDateTime = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private int _taskStatusCategoryId = -1;
        [DataMember]
        public int TaskStatusCategoryId
        {
            get { return _taskStatusCategoryId; }
            set { _taskStatusCategoryId = value; }
        }

        private DateTime _dueDate;
        [DataMember]
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        private decimal _totalTimeSpent;
        [DataMember]
        public decimal TotalTimeSpent
        {
            get { return _totalTimeSpent; }
            set { _totalTimeSpent = value; }
        }
    }
}
