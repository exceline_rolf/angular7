﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.Admin.ManageEmployees
{
    [DataContract]
    public class EmployeeTimeEntryDC
    {
        private int _Id = -1;
        [DataMember]
        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private int _employeeId = -1;
        [DataMember]
        public int EmployeeId
        {
            get { return _employeeId; }
            set { _employeeId = value; }
        }

        private DateTime _workDate;
        [DataMember]
        public DateTime WorkDate
        {
            get { return _workDate; }
            set { _workDate = value; }
        }

        private TimeSpan _startTime;
        [DataMember]
        public TimeSpan StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

         

        private TimeSpan _endTime;
        [DataMember]
        public TimeSpan EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

         

        private decimal _timeInMinute;
        [DataMember]
        public decimal TimeInMinutes
        {
            get { return _timeInMinute; }
            set { _timeInMinute = value; }
        }

        private string _timeCategoryName = string.Empty;
        [DataMember]
        public string TimeCategoryName
        {
            get { return _timeCategoryName; }
            set { _timeCategoryName = value; }
        }

        private int _timeCategoryId;
        [DataMember]
        public int TimeCategoryId
        {
            get { return _timeCategoryId; }
            set { _timeCategoryId = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private string _createdDate;
        [DataMember]
        public string CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        private bool _isApproved = false;
        [DataMember]
        public bool IsApproved
        {
            get { return _isApproved; }
            set { _isApproved = value; }
        }

        private int _branchId;
           [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName = string.Empty;
         [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private List<int> _contractList = new List<int>();
        [DataMember]
        public List<int> ContractList
        {
            get { return _contractList; }
            set { _contractList = value; }
        }
    }
}
