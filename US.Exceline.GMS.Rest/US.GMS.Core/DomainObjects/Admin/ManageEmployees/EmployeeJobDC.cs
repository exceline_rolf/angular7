﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.Admin.ManageEmployees
{
    [DataContract]
    public class EmployeeJobDC
    {
        private string _day = string.Empty;
        [DataMember]
        public string Day
        {
            get { return _day; }
            set { _day = value; }
        }

        private DateTime _date = new DateTime();
        [DataMember]
        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private TimeSpan _startTime = new TimeSpan();
        [DataMember]
        public TimeSpan StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private TimeSpan _endTime = new TimeSpan();
        [DataMember]
        public TimeSpan EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        private string _jobCategoryName = string.Empty;
        [DataMember]
        public string JobCategoryName
        {
            get { return _jobCategoryName; }
            set { _jobCategoryName = value; }
        }

        private string _jobName = string.Empty;
        [DataMember]
        public string JobName
        {
            get { return _jobName; }
            set { _jobName = value; }
        }
        
        private string _text = string.Empty;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private string _status = string.Empty;
        [DataMember]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private DateTime _jobStartDate;
        [DataMember]
        public DateTime JobStartDate
        {
            get { return _jobStartDate; }
            set { _jobStartDate = value; }
        }

        private DateTime _jobEndDate;
        [DataMember]
        public DateTime JobEndDate
        {
            get { return _jobEndDate; }
            set { _jobEndDate = value; }
        }

        private int _branchId;
         [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName = string.Empty;
         [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private int _jobCategoryId;
          [DataMember]
        public int JobCategoryId
        {
            get { return _jobCategoryId; }
            set { _jobCategoryId = value; }
        }

     


    }
}
