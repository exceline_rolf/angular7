﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.Admin.ManageEmployees
{
    [DataContract]
    public  class GymEmployeeRoleDurationDC
    {
        private string  _roleName =string.Empty;
        [DataMember]
        public string RoleName
        {
          get { return _roleName; }
          set { _roleName = value; }
        }
        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        private int _roleId = -1;
        [DataMember]
        public int RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }
        private DateTime _startDate = DateTime.MinValue;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }
        private DateTime _endDate = DateTime.MinValue;
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private string _branchName = string.Empty;
         [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

         private int _branchId;
           [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        


    }
}
