﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageOpeningHours
{
    [DataContract]
    public class ExcelineTimeSlotDC
    {
        private string _timeSlot = String.Empty;
        [DataMember]
        public string TimeSlot
        {
            get { return _timeSlot; }
            set { _timeSlot = value; }
        }

        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _startTime = String.Empty;
        [DataMember]
        public string StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private string _endTime = String.Empty;
        [DataMember]
        public string EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }


        private bool _monday = false;
        [DataMember]
        public bool Monday
        {
            get { return _monday; }
            set { _monday = value; }
        }

        private string _mondayString = String.Empty;
        [DataMember]
        public string MondayString
        {
            get { return _mondayString; }
            set { _mondayString = value; }
        }



        private bool _tuesday = false;
        [DataMember]
        public bool Tuesday
        {
            get { return _tuesday; }
            set { _tuesday = value; }
        }

        private string _tuesdayString = String.Empty;
        [DataMember]
        public string TuesdayString
        {
            get { return _tuesdayString; }
            set { _tuesdayString = value; }
        }



        private bool _wednesday = false;
        [DataMember]
        public bool Wednesday
        {
            get { return _wednesday; }
            set { _wednesday = value; }
        }

        private string _wednesdayString = String.Empty;
        [DataMember]
        public string WednesdayString
        {
            get { return _wednesdayString; }
            set { _wednesdayString = value; }
        }



        private bool _thursday = false;
        [DataMember]
        public bool Thursday
        {
            get { return _thursday; }
            set { _thursday = value; }
        }

        private string _thursdayString = String.Empty;
        [DataMember]
        public string ThursdayString
        {
            get { return _thursdayString; }
            set { _thursdayString = value; }
        }



        private bool _friday = false;
        [DataMember]
        public bool Friday
        {
            get { return _friday; }
            set { _friday = value; }
        }

        private string _fridayString = String.Empty;
        [DataMember]
        public string FridayString
        {
            get { return _fridayString; }
            set { _fridayString = value; }
        }



        private bool _saturday = false;
        [DataMember]
        public bool Saturday
        {
            get { return _saturday; }
            set { _saturday = value; }
        }

        private string _saturdayString = String.Empty;
        [DataMember]
        public string SaturdayString
        {
            get { return _saturdayString; }
            set { _saturdayString = value; }
        }


        
        private bool _sunday = false;
        [DataMember]
        public bool Sunday
        {
            get { return _sunday; }
            set { _sunday = value; }
        }

        private string _sundayString = String.Empty;
        [DataMember]
        public string SundayString
        {
            get { return _sundayString; }
            set { _sundayString = value; }
        }



        private List<TimeSlotAvailableStatusDC> _monAvailableStatus = new List<TimeSlotAvailableStatusDC>();
        [DataMember]
        public List<TimeSlotAvailableStatusDC> MonAvailableStatus
        {
            get { return _monAvailableStatus; }
            set { _monAvailableStatus = value; }
        }

        private List<TimeSlotAvailableStatusDC> _tueAvailableStatus = new List<TimeSlotAvailableStatusDC>();
        [DataMember]
        public List<TimeSlotAvailableStatusDC> TueAvailableStatus
        {
            get
            {
                return _tueAvailableStatus;
            }
            set { _monAvailableStatus = value; }
        }

        private List<TimeSlotAvailableStatusDC> _wedAvailableStatus = new List<TimeSlotAvailableStatusDC>();
        [DataMember]
        public List<TimeSlotAvailableStatusDC> WedAvailableStatus
        {
            get
            {
                return _wedAvailableStatus;
            }
            set { _wedAvailableStatus = value; }
        }

        private List<TimeSlotAvailableStatusDC> _thuAvailableStatus = new List<TimeSlotAvailableStatusDC>();
        [DataMember]
        public List<TimeSlotAvailableStatusDC> ThuAvailableStatus
        {
            get
            {
                return _thuAvailableStatus;
            }
            set { _thuAvailableStatus = value; }
        }

        private List<TimeSlotAvailableStatusDC> _friAvailableStatus = new List<TimeSlotAvailableStatusDC>();
        [DataMember]
        public List<TimeSlotAvailableStatusDC> FriAvailableStatus
        {
            get
            {
                return _friAvailableStatus;
            }
            set { _friAvailableStatus = value; }
        }

        private List<TimeSlotAvailableStatusDC> _satAvailableStatus = new List<TimeSlotAvailableStatusDC>();
        [DataMember]
        public List<TimeSlotAvailableStatusDC> SatAvailableStatus
        {
            get
            {
                return _satAvailableStatus;
            }
            set { _satAvailableStatus = value; }
        }

        private List<TimeSlotAvailableStatusDC> _sunAvailableStatus = new List<TimeSlotAvailableStatusDC>();
        [DataMember]
        public List<TimeSlotAvailableStatusDC> SunAvailableStatus
        {
            get
            {
                return _sunAvailableStatus;
            }
            set { _sunAvailableStatus = value; }
        }



        private TimeSlotAvailableStatusDC _monStatus = new TimeSlotAvailableStatusDC();
        public TimeSlotAvailableStatusDC MonStatus
        {
            get { return _monStatus; }
            set { _monStatus = value; }
        }

        private TimeSlotAvailableStatusDC _tueStatus = new TimeSlotAvailableStatusDC();
        public TimeSlotAvailableStatusDC TueStatus
        {
            get { return _tueStatus; }
            set { _tueStatus = value;}
        }

        private TimeSlotAvailableStatusDC _wedStatus = new TimeSlotAvailableStatusDC();
        public TimeSlotAvailableStatusDC WedStatus
        {
            get { return _wedStatus; }
            set { _wedStatus = value;}
        }

        private TimeSlotAvailableStatusDC _thuStatus = new TimeSlotAvailableStatusDC();
        public TimeSlotAvailableStatusDC ThuStatus
        {
            get { return _thuStatus; }
            set { _thuStatus = value;}
        }

        private TimeSlotAvailableStatusDC _friStatus = new TimeSlotAvailableStatusDC();
        public TimeSlotAvailableStatusDC FriStatus
        {
            get { return _friStatus; }
            set { _friStatus = value;}
        }

        private TimeSlotAvailableStatusDC _satStatus = new TimeSlotAvailableStatusDC();
        public TimeSlotAvailableStatusDC SatStatus
        {
            get { return _satStatus; }
            set { _satStatus = value;}
        }

        private TimeSlotAvailableStatusDC _sunStatus = new TimeSlotAvailableStatusDC();
        public TimeSlotAvailableStatusDC SunStatus
        {
            get { return _sunStatus; }
            set { _sunStatus = value;}
        }
    }
}
