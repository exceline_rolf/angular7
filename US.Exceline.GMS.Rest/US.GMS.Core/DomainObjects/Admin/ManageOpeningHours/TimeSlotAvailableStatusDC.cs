﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageOpeningHours
{
    [DataContract]
    public class TimeSlotAvailableStatusDC
    {
        [DataMember]
        public bool status;
        [DataMember]
        public string displayText;
    }
}
