﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageOpeningHours
{
    [DataContract]
    public class DaysOfWeekAvailableDC
    {
        private List<TimeSlotAvailableStatusDC> _monAvailableStatus = new List<TimeSlotAvailableStatusDC>();
        [DataMember]
        public List<TimeSlotAvailableStatusDC> MonAvailableStatus
        {
            get
            {
                return _monAvailableStatus;
            }
            set { _monAvailableStatus = value; }
        }

        private List<TimeSlotAvailableStatusDC> _tueAvailableStatus = new List<TimeSlotAvailableStatusDC>();
        [DataMember]
        public List<TimeSlotAvailableStatusDC> TueAvailableStatus
        {
            get
            {
                return _tueAvailableStatus;
            }
            set { _monAvailableStatus = value; }
        }

        private List<TimeSlotAvailableStatusDC> _wedAvailableStatus = new List<TimeSlotAvailableStatusDC>();
        [DataMember]
        public List<TimeSlotAvailableStatusDC> WedAvailableStatus
        {
            get
            {
                return _wedAvailableStatus;
            }
            set { _wedAvailableStatus = value; }
        }

        private List<TimeSlotAvailableStatusDC> _thuAvailableStatus = new List<TimeSlotAvailableStatusDC>();
        [DataMember]
        public List<TimeSlotAvailableStatusDC> ThuAvailableStatus
        {
            get
            {
                return _thuAvailableStatus;
            }
            set { _thuAvailableStatus = value; }
        }

        private List<TimeSlotAvailableStatusDC> _friAvailableStatus = new List<TimeSlotAvailableStatusDC>();
        [DataMember]
        public List<TimeSlotAvailableStatusDC> FriAvailableStatus
        {
            get
            {
                return _friAvailableStatus;
            }
            set { _friAvailableStatus = value; }
        }

        private List<TimeSlotAvailableStatusDC> _satAvailableStatus = new List<TimeSlotAvailableStatusDC>();
        [DataMember]
        public List<TimeSlotAvailableStatusDC> SatAvailableStatus
        {
            get
            {
                return _satAvailableStatus;
            }
            set { _satAvailableStatus = value; }
        }

        private List<TimeSlotAvailableStatusDC> _sunAvailableStatus = new List<TimeSlotAvailableStatusDC>();
        [DataMember]
        public List<TimeSlotAvailableStatusDC> SunAvailableStatus
        {
            get
            {
                return _sunAvailableStatus;
            }
            set { _sunAvailableStatus = value; }
        }
    }
}
