﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageOpeningHours
{
    
    [DataContract]
    public class ExcelineTimeScheduleDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _activityCategory;
        [DataMember]
        public string ActivityCategory
        {
            get { return _activityCategory; }
            set { _activityCategory = value; }
        }

        private DateTime _startTime;
        [DataMember]
        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private DateTime _endTime;
        [DataMember]
        public DateTime EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        private int _timeInterval = -1;
        [DataMember]
        public int TimeInterval
        {
            get { return _timeInterval; }
            set { _timeInterval = value; }
        }

        private bool _monAvailStatus = false;
        [DataMember]
        public bool MonAvailStatus
        {
            get { return _monAvailStatus; }
            set { _monAvailStatus = value; }
        }

        private bool _tueAvailStatus = false;
        [DataMember]
        public bool TueAvailStatus
        {
            get { return _tueAvailStatus; }
            set { _tueAvailStatus = value; }
        }

        private bool _wedAvailStatus = false;
        [DataMember]
        public bool WedAvailStatus
        {
            get { return _wedAvailStatus; }
            set { _wedAvailStatus = value; }
        }

        private bool _thuAvailStatus = false;
        [DataMember]
        public bool ThuAvailStatus
        {
            get { return _thuAvailStatus; }
            set { _thuAvailStatus = value; }
        }

        private bool _friAvailStatus = false;
        [DataMember]
        public bool FriAvailStatus
        {
            get { return _friAvailStatus; }
            set { _friAvailStatus = value; }
        }

        private bool _satAvailStatus = false;
        [DataMember]
        public bool SatAvailStatus
        {
            get { return _satAvailStatus; }
            set { _satAvailStatus = value; }
        }

        private bool _sunAvailStatus = false;
        [DataMember]
        public bool SunAvailStatus
        {
            get { return _sunAvailStatus; }
            set { _sunAvailStatus = value; }
        }

        private List<ExcelineTimeSlotDC> _timeSlotsList = new List<ExcelineTimeSlotDC>();
        [DataMember]
        public List<ExcelineTimeSlotDC> TimeSlotsList
        {
            get { return _timeSlotsList; }
            set { _timeSlotsList = value; }
        }

        private string _comment = String.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private DateTime _timeSlot;
        [DataMember]
        public DateTime TimeSlot
        {
            get { return _timeSlot; }
            set { _timeSlot = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

    }
}
