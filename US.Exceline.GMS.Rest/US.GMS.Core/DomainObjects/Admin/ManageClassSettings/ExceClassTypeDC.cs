﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Core.DomainObjects.Admin.ManageClassSettings
{
    [DataContract]
    public class ExceClassTypeDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _classGroupId;
        [DataMember]
        public int ClassGroupId
        {
            get { return _classGroupId; }
            set { _classGroupId = value; }
        }

        private string _classGroup;
        [DataMember]
        public string ClassGroup
        {
            get { return _classGroup; }
            set { _classGroup = value; }
        }

        private int _classLevelId;
        [DataMember]
        public int ClassLevelId
        {
            get { return _classLevelId; }
            set { _classLevelId = value; }
        }

        private string _classLevel = string.Empty;
        [DataMember]
        public string ClassLevel
        {
            get { return _classLevel; }
            set { _classLevel = value; }
        }

        private int _timeCategoryId;
        [DataMember]
        public int TimeCategoryId
        {
            get { return _timeCategoryId; }
            set { _timeCategoryId = value; }
        }

        private string _timeCategory;
        [DataMember]
        public string TimeCategory
        {
            get { return _timeCategory; }
            set { _timeCategory = value; }
        }

        private List<CategoryDC> _classCategoryList = new List<CategoryDC>();
        [DataMember]        
        public List<CategoryDC> ClassCategoryList
        {
            get { return _classCategoryList; }
            set { _classCategoryList = value; }
        }

        private List<string> _classCategoryStringList = new List<string>();
        [DataMember]
        public List<string> ClassCategoryStringList
        {
            get { return _classCategoryStringList; }
            set { _classCategoryStringList = value; }
        }

        private string _colour = string.Empty;
        [DataMember]
        public string Colour
        {
            get { return _colour; }
            set { _colour = value; }
        }

        private int _code = -1;
        [DataMember]
        public int Code
        {
            get { return _code; }
            set { _code = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private int _timeDuration = -1;
        [DataMember]
        public int TimeDuration
        {
            get { return _timeDuration; }
            set { _timeDuration = value; }
        }

        private List<CategoryDC> _classKeywordList = new List<CategoryDC>();
        [DataMember]
        public List<CategoryDC> ClassKeywordList
        {
            get { return _classKeywordList; }
            set { _classKeywordList = value; }
        }

        private string _classKeywordString = string.Empty;
        [DataMember]
        public string ClassKeywordString
        {
            get { return _classKeywordString; }
            set { _classKeywordString = value; }
        }

        private string _classCategoryString = string.Empty;
        [DataMember]
        public string ClassCategoryString
        {
            get { return _classCategoryString; }
            set { _classCategoryString = value; }
        }

        private bool _obsoleteLevel = false;
        [DataMember]
        public bool ObsoleteLevel
        {
            get { return _obsoleteLevel; }
            set { _obsoleteLevel = value; }
        }

        private bool _isLocal = false;
        [DataMember]
        public bool IsLocal
        {
            get { return _isLocal; }
            set { _isLocal = value; }
        }

        private List<ExcelineBranchDC> _excelineBranchList = new List<ExcelineBranchDC>();
        [DataMember]
        public List<ExcelineBranchDC> ExcelineBranchList
        {
            get { return _excelineBranchList; }
            set { _excelineBranchList = value; }
        }

        private int _maxNoOfBookings = 0;
        [DataMember]
        public int MaxNoOfBookings
        {
            get { return _maxNoOfBookings; }
            set { _maxNoOfBookings = value; }
        }

        private decimal _hoursToBePaid;
        [DataMember]
        public decimal HoursToBePaid
        {
            get { return _hoursToBePaid; }
            set { _hoursToBePaid = value; }
        }

        private bool _isIBookingIntegrated = false;
        [DataMember]
        public bool IsIBookingIntegrated
        {
            get { return _isIBookingIntegrated; }
            set { _isIBookingIntegrated = value; }
        }

        private string _englishComment = string.Empty;
        [DataMember]
        public string EnglishComment
        {
            get { return _englishComment; }
            set { _englishComment = value; }
        }

    }
}
