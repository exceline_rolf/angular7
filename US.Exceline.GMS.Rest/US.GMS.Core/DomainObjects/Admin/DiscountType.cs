﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.DomainObjects.Admin
{
    public enum DiscountType
    {
        GROUP,
        ACTIVITY,
        SHOP,
        NONE,
        OTHER

    }
}
