﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Core.DomainObjects.ManageClasses
{  

    public interface IExcelineClass
    {   
        int Id { set; get; }      
        string Name { set; get; }       
        CategoryDC ClassCategory { set; get; }        
        string Description { set; get; }     
        int MaxNumberOfMembers { set; get; }      
        ScheduleDC Schedule { set; get; }       
        List<TrainerDC> TrainersList { set; get; }       
        List<InstructorDC> InstructorList { set; get; }       
        List<ResourceDC> ResourceList { set; get; }
        List<ExcelineMemberDC> MemberList { set; get; }        
        DateTime CreatedDate { set; get; }       
        DateTime ModifiedDate { set; get; }       
        string CreatedUser { set; get; }       
        string ModifiedUser { set; get; }      
        bool ActiveStatus { set; get; }
        List<int> TrainerIdList { set; get; }
        List<int> MemberIdList { set; get; }
        List<int> InstructorIdList { set; get; }
        List<int> EmployeeIdList { set; get; }
        List<int> ResourceIdList { set; get; }
        string Mode { set; get; }       
        int AvailableNumberOfMembers { get; set; }
        bool IsPayable { get; set; }
        int ArticleId { get; set; }
    }
}
