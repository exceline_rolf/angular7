﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageClasses
{
    [DataContract]
    public class MemberBookingDetailsDC
    {
        private int _scheduleId = -1;
        [DataMember]
        public int ScheduleId
        {
            get { return _scheduleId; }
            set { _scheduleId = value; }
        }

        private int _scheduleItemId = -1;
        [DataMember]
        public int ScheduleItemId
        {
            get { return _scheduleItemId; }
            set { _scheduleItemId = value; }
        }

        private string _className = String.Empty;
        [DataMember]
        public string ClassName
        {
            get { return _className; }
            set { _className = value; }
        }

        private int _memberId = -1;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private string _memberName;
        [DataMember]
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        private string _memberImagePath = string.Empty;
        [DataMember]
        public string MemberImagePath
        {
            get { return _memberImagePath; }
            set { _memberImagePath = value; }
        }

        private byte[] _profilePicture = null;
        [DataMember]
        public byte[] ProfilePicture
        {
            get { return _profilePicture; }
            set { _profilePicture = value; }
        }

        private int _activeTimeId = -1;
        [DataMember]
        public int ActiveTimeId
        {
            get { return _activeTimeId; }
            set { _activeTimeId = value; }
        }

        private int _numberOfParticipants = -1;
        [DataMember]
        public int NumberOfParticipants
        {
            get { return _numberOfParticipants; }
            set { _numberOfParticipants = value; }
        }

        private bool _paid;
        [DataMember]
        public bool Paid
        {
            get { return _paid; }
            set { _paid = value; }
        }

        private Nullable<DateTime> _startDateTime;
        [DataMember]
        public Nullable<DateTime> StartDateTime
        {
            get { return _startDateTime; }
            set { _startDateTime = value; }
        }

        private Nullable<DateTime> _endDateTime;
        [DataMember]
        public Nullable<DateTime> EndDateTime
        {
            get { return _endDateTime; }
            set { _endDateTime = value; }
        }


    }
}
