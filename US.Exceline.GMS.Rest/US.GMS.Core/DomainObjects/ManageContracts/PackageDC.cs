﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "3/30/2012 1:41:40 PM
// --------------------------------------------------------------------------
// Edit Author       : NHE
// Edit Timestamp    : 4/26/2012 9.30
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    [DataContract]
    public class PackageDC
    {
        private int _packageId = -1;
        [DataMember]
        public int PackageId
        {
            get { return _packageId; }
            set { _packageId = value; }
        }
       
        private int _priority = -1;
        [DataMember]
        public int Priority
        {
            get { return _priority; }
            set { _priority = value; }
        }

        private string _packageName = string.Empty;
        [DataMember]
        public string PackageName
        {
            get { return _packageName; }
            set { _packageName = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private DiscountRateType _discountRate = DiscountRateType.NONE;
        [DataMember]
        public DiscountRateType DiscountRate
        {
            get { return _discountRate; }
            set { _discountRate = value; }
        }

        private int _activityId = -1;
        [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        private string _activityName = string.Empty;
        [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        private ActivityDC _contractActivity = new ActivityDC();
        [DataMember]
        public ActivityDC ContractActivity
        {
            get { return _contractActivity; }
            set { _contractActivity = value; }
        }

        private FreeItemsDC _freeitems = new FreeItemsDC();
        [DataMember]
        public FreeItemsDC Freeitems
        {
            get { return _freeitems; }
            set { _freeitems = value; }
        }

        private DateTime _startDate = DateTime.Now;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime _endDate = DateTime.Now;
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private DateTime? _fixDateOfContract;
        [DataMember]
        public DateTime? FixDateOfContract
        {
            get { return _fixDateOfContract; }
            set { _fixDateOfContract = value; }
        }

        private DateTime? _fixStartDateOfContract;
        [DataMember]
        public DateTime? FixStartDateOfContract
        {
            get { return _fixStartDateOfContract; }
            set { _fixStartDateOfContract = value; }
        }

        private decimal _packagePrice;
        [DataMember]
        public decimal PackagePrice
        {
            get { return _packagePrice; }
            set { _packagePrice = value; }
        }

        private int _numOfInstallments = -1;
        [DataMember]
        public int NumOfInstallments
        {
            get { return _numOfInstallments; }
            set { _numOfInstallments = value; }
        }

        private decimal _ratePerInstallment;
        [DataMember]
        public decimal RatePerInstallment
        {
            get { return _ratePerInstallment; }
            set { _ratePerInstallment = value; }
        }

        private decimal _enrollmentFee;
        [DataMember]
        public decimal EnrollmentFee
        {
            get { return _enrollmentFee; }
            set { _enrollmentFee = value; }
        }

        private int _numOfVisits = -1;
        [DataMember]
        public int NumOfVisits
        {
            get { return _numOfVisits; }
            set { _numOfVisits = value; }
        }

        private int _noOfVisits = -1;
        [DataMember]
        public int NoOfVisits
        {
            get { return _noOfVisits; }
            set { _noOfVisits = value; }
        }

        private bool _activeStatus = true;
        [DataMember]
        public bool ActiveStatus
        {
            get
            {
                return _activeStatus;
            }
            set
            {
                _activeStatus = value;
            }
        }

        private bool _isShownOnNet;
        [DataMember]
        public bool IsShownOnNet
        {
            get
            {
                return _isShownOnNet;
            }
            set
            {
                _isShownOnNet = value;
            }
        }


        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        private DateTime _lastModifiedDate;
        [DataMember]
        public DateTime LastModifiedDate
        {
            get
            {
                return _lastModifiedDate;
            }
            set
            {
                _lastModifiedDate = value;
            }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get
            {
                return _createdUser;
            }
            set
            {
                _createdUser = value;
            }
        }

        private string _lastModifiedUser;
        [DataMember]
        public string LastModifiedUser
        {
            get
            {
                return _lastModifiedUser;
            }
            set
            {
                _lastModifiedUser = value;
            }
        }

        private string _branchName;
        [DataMember]
        public string BranchName
        {
            get
            {
                return _branchName;
            }
            set
            {
                _branchName = value;
            }
        }

        private CategoryDC _category = new CategoryDC();
        [DataMember]
        public CategoryDC PackageCategory
        {
            get { return _category; }
            set { _category = value; }
        }

        private ContractItemDC _memberFeeArticle = new ContractItemDC();
        [DataMember]
        public ContractItemDC MemberFeeArticle
        {
            get { return _memberFeeArticle; }
            set { _memberFeeArticle = value; }
        }

        private List<ContractItemDC> _startUpItemList = new List<ContractItemDC>();
        [DataMember]
        public List<ContractItemDC> StartUpItemList
        {
            get { return _startUpItemList; }
            set { _startUpItemList = value; }
        }

        private List<ContractItemDC> _everyMonthItemList = new List<ContractItemDC>();
        [DataMember]
        public List<ContractItemDC> EveryMonthItemList
        {
            get { return _everyMonthItemList; }
            set { _everyMonthItemList = value; }
        }

        private string _contractDescritpion = string.Empty;
        [DataMember]
        public string ContractDescritpion
        {
            get { return _contractDescritpion; }
            set { _contractDescritpion = value; }
        }

        private string _contractType = string.Empty;
        [DataMember]
        public string ContractType
        {
            get { return _contractType; }
            set { _contractType = value; }
        }

        private ContractTemplateType _contractTemplateType = ContractTemplateType.NONE;
        [DataMember]
        public ContractTemplateType ContractTemplateType
        {
            get { return _contractTemplateType; }
            set { _contractTemplateType = value; }
        }
        // Example : 0-->Member Contract 1-->Sponser Contract
        private int _contractTypeId = -1;
        [DataMember]
        public int ContractTypeId
        {
            get { return _contractTypeId; }
            set { _contractTypeId = value; }
        }
        private int _contractSettingId = -1;
        [DataMember]
        public int ContractSettingId
        {
            get { return _contractSettingId; }
            set { _contractSettingId = value; }
        }

        private CategoryDC _contarctTypeValue;
        [DataMember]
        public CategoryDC ContractTypeValue
        {
            get { return _contarctTypeValue; }
            set { _contarctTypeValue = value; }
        }

        private int _noOfMonths;
        [DataMember]
        public int NoOfMonths
        {
            get { return _noOfMonths; }
            set { _noOfMonths = value; }
        }

        private int _noOfDays;
        [DataMember]
        public int NoOfDays
        {
            get { return _noOfDays; }
            set { _noOfDays = value; }
        }

        private int _lockInPeriod = 0;
        [DataMember]
        public int LockInPeriod
        {
            get { return _lockInPeriod; }
            set { _lockInPeriod = value; }
        }

        private DateTime _lockInPeriodUntilDate;
        [DataMember]
        public DateTime LockInPeriodUntilDate
        {
            get { return _lockInPeriodUntilDate; }
            set { _lockInPeriodUntilDate = value; }
        }

        private bool _restPlusMonth = false;
        [DataMember]
        public bool RestPlusMonth
        {
            get { return _restPlusMonth; }
            set { _restPlusMonth = value; }
        }

        private bool _isInvoiceDetail = false;
        [DataMember]
        public bool IsInvoiceDetail
        {
            get { return _isInvoiceDetail; }
            set { _isInvoiceDetail = value; }
        }

        private bool _autoRenew = false;
        [DataMember]
        public bool AutoRenew
        {
            get { return _autoRenew; }
            set { _autoRenew = value; }
        }

        private string _templateNumber = string.Empty;
        [DataMember]
        public string TemplateNumber
        {
            get { return _templateNumber; }
            set { _templateNumber = value; }
        }

        private int _intTemplateNumber;
        [DataMember]
        public int IntTemplateNumber
        {
            get { return _intTemplateNumber; }
            set { _intTemplateNumber = value; }
        }


        private int _articleId;
        [DataMember]
        public int ArticleId
        {
            get { return _articleId; }
            set { _articleId = value; }
        }

        private int _sortingNo;
        [DataMember]
        public int SortingNo
        {
            get { return _sortingNo; }
            set { _sortingNo = value; }
        }

        private string _mode;
        [DataMember]
        public string Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }

        private int _priceGuaranty = 0;
        [DataMember]
        public int PriceGuaranty
        {
            get { return _priceGuaranty; }
            set { _priceGuaranty = value; }
        }

        private int _articleNo = -1;
        [DataMember]
        public int ArticleNo
        {
            get { return _articleNo; }
            set { _articleNo = value; }
        }

        private string _articleText = string.Empty;
        [DataMember]
        public string ArticleText
        {
            get { return _articleText; }
            set { _articleText = value; }
        }

        private int _accessProfileId = -1;
        [DataMember]
        public int AccessProfileId
        {
            get { return _accessProfileId; }
            set { _accessProfileId = value; }
        }

        private bool _isATG = false;
        [DataMember]
        public bool IsATG
        {
            get { return _isATG; }
            set { _isATG = value; }
        }

        private bool _isInStock = false;
        [DataMember]
        public bool IsInStock
        {
            get { return _isInStock; }
            set { _isInStock = value; }
        }

        private bool _isNoClasses = false;
        [DataMember]
        public bool IsNoClasses
        {
            get { return _isNoClasses; }
            set { _isNoClasses = value; }
        }

        private decimal _amountForATG = 0;
        [DataMember]
        public decimal AmountForATG
        {
            get { return _amountForATG; }
            set { _amountForATG = value; }
        }

        private int _aTGStatus = 0;
        [DataMember]
        public int ATGStatus
        {
            get { return _aTGStatus; }
            set { _aTGStatus = value; }
        }

        private int _creditDueDays = 0;
        [DataMember]
        public int CreditDueDays
        {
            get { return _creditDueDays; }
            set { _creditDueDays = value; }
        }

        private DateTime? _seconDueDate;
        [DataMember]
        public DateTime? SecondDueDate
        {
            get { return _seconDueDate; }
            set { _seconDueDate = value; }
        }

        private int _nextTemplateId = -1;
        [DataMember]
        public int NextTemplateId
        {
            get { return _nextTemplateId; }
            set { _nextTemplateId = value; }
        }

        private string _nextTemplateNo = string.Empty;
        [DataMember]
        public string NextTemplateNo
        {
            get { return _nextTemplateNo; }
            set { _nextTemplateNo = value; }
        }

        private string _nextContractTemplateName = string.Empty;
        [DataMember]
        public string NextContractTemplateName
        {
            get { return _nextContractTemplateName; }
            set { _nextContractTemplateName = value; }
        }

        private decimal _orderPrice = 0;
        [DataMember]
        public decimal OrderPrice
        {
            get { return _orderPrice; }
            set { _orderPrice = value; }
        }

        private decimal _startUpItemPrice = 0;
        [DataMember]
        public decimal StartUpItemPrice
        {
            get { return _startUpItemPrice; }
            set { _startUpItemPrice = value; }
        }

        private decimal _everyMonthItemsPrice = 0;
        [DataMember]
        public decimal EveryMonthItemsPrice
        {
            get { return _everyMonthItemsPrice; }
            set { _everyMonthItemsPrice = value; }
        }

        private int _maxAge = 0;
        [DataMember]
        public int MaxAge
        {
            get { return _maxAge; }
            set { _maxAge = value; }
        }

        private DateTime? _firstDueDate;
        [DataMember]
        public DateTime? FirstDueDate
        {
            get { return _firstDueDate; }
            set { _firstDueDate = value; }
        }

        private int _inStock = 0;
        [DataMember]
        public int InStock
        {
            get { return _inStock; }
            set { _inStock = value; }
        }

        private int _creditDueDateSetting = -1;
        [DataMember]
        public int CreditDueDateSetting
        {
            get { return _creditDueDateSetting; }
            set { _creditDueDateSetting = value; }
        }

        private bool _useTodayAsDueDate = false;
        [DataMember]
        public bool UseTodayAsDueDate
        {
            get { return _useTodayAsDueDate; }
            set { _useTodayAsDueDate = value; }
        }

        private List<int> _branchIdList = new List<int>();
        [DataMember]
        public List<int> BranchIdList
        {
            get { return _branchIdList; }
            set { _branchIdList = value; }
        }

        private List<int> _regionIdList = new List<int>();
        [DataMember]
        public List<int> RegionIdList
        {
            get { return _regionIdList; }
            set { _regionIdList = value; }
        }

        private List<string> _countryIdList = new List<string>();
        [DataMember]
        public List<string> CountryIdList
        {
            get { return _countryIdList; }
            set { _countryIdList = value; }
        }

        private bool _expressAvailable = false;
        [DataMember]
        public bool ExpressAvailable
        {
            get { return _expressAvailable; }
            set { _expressAvailable = value; }
        }

        private string _color = string.Empty;
         [DataMember]
        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }

         private decimal _servicePrice;
         [DataMember]
         public decimal ServicePrice
         {
             get { return _servicePrice; }
             set { _servicePrice = value; }
         }

         private bool _isBookingActivity = false;
         [DataMember]
         public bool IsBookingActivity
         {
             get { return _isBookingActivity; }
             set { _isBookingActivity = value; }
         }

         private bool _isBasicActivity = false;
         [DataMember]
         public bool IsBasicActivity
         {
             get { return _isBasicActivity; }
             set { _isBasicActivity = value; }
         }

        private int _creditPeriod = 0;
        [DataMember]
        public int CreditPeriod
        {
            get { return _creditPeriod; }
            set { _creditPeriod = value; }
        }

        private int _memberCreditPeriod = 0;
        [DataMember]
        public int MemberCreditPeriod
        {
            get { return _memberCreditPeriod; }
            set { _memberCreditPeriod = value; }
        }

        private bool _postPay = false;
        [DataMember]
        public bool PostPay
        {
            get { return _postPay; }
            set { _postPay = value; }
        }

        private int _contractConditionId = -1;
        [DataMember]
        public int ContractConditionId
        {
            get { return _contractConditionId; }
            set { _contractConditionId = value; }
        }

        private string _contractCondition = string.Empty;
        [DataMember]
        public string ContractCondition
        {
            get { return _contractCondition; }
            set { _contractCondition = value; }
        }
    }
}
