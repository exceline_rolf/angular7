﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    public enum ContractTemplateType
    {
        NONE,
        SPONSOR,
        MEMBER
    }
}
