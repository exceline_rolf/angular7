﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : "4/26/2012 16:22:14
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    [DataContract]
    public class ContractItemDC
    {
        public ContractItemDC ShallowCopy()
        {
            return (ContractItemDC)this.MemberwiseClone();
        }

        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _itemName = string.Empty;
         [DataMember]
        public string ItemName
        {
            get { return _itemName; }
            set { _itemName = value; }
        }


         private decimal _unitPrice = 0;
         [DataMember]
         public decimal UnitPrice
         {
             get { return _unitPrice; }
             set { _unitPrice = value; }
         }
        
         private decimal _employeePrice = 0;
        [DataMember]
        public decimal EmployeePrice
        {
            get { return _employeePrice; }
            set { _employeePrice = value; }
        }

        private int _itemId = -1;
        [DataMember]
        public int ItemId
        {
            get { return _itemId; }
            set { _itemId = value; }
        }

        private int _categoryId = -1;
        [DataMember]
        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }

        private bool _isSelected = false;
        [DataMember]
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private bool _isStartUpItem = false;
        [DataMember]
        public bool IsStartUpItem
        {
            get { return _isStartUpItem; }
            set { _isStartUpItem = value; }
        
        }
       
        private int _quantity = 1;
        [DataMember]
        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        private int _noOfVisits = 1;
        [DataMember]
        public int NoOfVisits
        {
            get { return _noOfVisits; }
            set { _noOfVisits = value; }
        }

        private int _articleId;
        [DataMember]
        public int ArticleId
        {
            get { return _articleId; }
            set { _articleId = value; }
        }

        private int _stockLevel = -1;
        [DataMember]
        public int StockLevel
        {
            get { return _stockLevel; }
            set { _stockLevel = value; }
        }

        private int _numberOfOrders = 0;
        [DataMember]
        public int NumberOfOrders
        {
            get { return _numberOfOrders; }
            set { _numberOfOrders = value; }
        }

        private decimal _price = 0;
        [DataMember]
        public decimal Price
        {
            get { return _price; }
            set { _price = value; }
        }

        private decimal _discount = 0;
        [DataMember]
        public decimal Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }

        private bool _isActivityArticle = false;
        [DataMember]
        public bool IsActivityArticle
        {
            get { return _isActivityArticle; }
            set { _isActivityArticle = value; }
        }

        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private int _priority = -1;
        [DataMember]
        public int Priority
        {
            get { return _priority; }
            set { _priority = value; }
        }

        private bool _isBookingArticle = false;
        [DataMember]
        public bool IsBookingArticle
        {
            get { return _isBookingArticle; }
            set { _isBookingArticle = value; }
        }

        private bool _isReadOnly = false;
        [DataMember]
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
            set { _isReadOnly = value; }
        }

        private string _returnComment = string.Empty;
        [DataMember]
        public string ReturnComment
        {
            get { return _returnComment; }
            set { _returnComment = value; }
        }

        private string _InvoiceNo = string.Empty;
        [DataMember]
        public string InvoiceNo
        {
            get { return _InvoiceNo; }
            set { _InvoiceNo = value; }
        }


        private bool _isSponsored = false;
        [DataMember]
        public bool IsSponsored
        {
            get { return _isSponsored; }
            set { _isSponsored = value; }
        }


        private string _sponsoringText = string.Empty;
        [DataMember]
        public string SponsoringText
        {
            get { return _sponsoringText; }
            set { _sponsoringText = value; }
        }

        private string _articleNo = string.Empty;
         [DataMember]
        public string ArticleNo
        {
            get { return _articleNo; }
            set { _articleNo = value; }
        }

         private bool _stockStatus;
         [DataMember]
        public bool StockStatus
        {
            get { return _stockStatus; }
            set { _stockStatus = value; }
        }

         private int _startOrder = 0;
         [DataMember]
         public int StartOrder
         {
             get { return _startOrder; }
             set { _startOrder = value; }
         }

         private int _endOrder = 0;
         [DataMember]
         public int EndOrder
         {
             get { return _endOrder; }
             set { _endOrder = value; }
         }

         private string _actityCode = string.Empty;
         [DataMember]
         public string ActityCode
         {
             get { return _actityCode; }
             set { _actityCode = value; }
         }

         private int _activityID = -1;
         [DataMember]
         public int ActivityID
         {
             get { return _activityID; }
             set { _activityID = value; }
         }

         private bool _canDeleted = true;
         [DataMember]
         public bool CanDeleted
         {
             get { return _canDeleted; }
             set { _canDeleted = value; }
         }

         private bool _isAvailableForGym = true;
          [DataMember]
         public bool IsAvailableForGym
         {
             get { return _isAvailableForGym; }
             set { _isAvailableForGym = value; }
         }

          private bool _isMemberFee = false;
         [DataMember]
          public bool IsMemberFee
          {
              get { return _isMemberFee; }
              set { _isMemberFee = value; }
          }

         private string _categoryCode = string.Empty;
         [DataMember]
         public string CategoryCode
         {
             get { return _categoryCode; }
             set { _categoryCode = value; }
         }

         private string _voucherNo = string.Empty;
         [DataMember]
         public string VoucherNo
         {
             get { return _voucherNo; }
             set { _voucherNo = value; }
         }

         private DateTime? _expiryDate;
         [DataMember]
         public DateTime? ExpiryDate
         {
             get { return _expiryDate; }
             set { _expiryDate = value; }
         }

        private bool _isBtnDeleteVisible = true;
        [DataMember]
        public bool IsBtnDeleteVisible
        {
            get { return _isBtnDeleteVisible; }
            set { _isBtnDeleteVisible = value; }
        }

        private int _contractConditionId = -1;
        [DataMember]
        public int ContractConditionId
        {
            get { return _contractConditionId; }
            set { _contractConditionId = value; }
        }

        private string _contractCondition = string.Empty;
        [DataMember]
        public string ContractCondition
        {
            get { return _contractCondition; }
            set { _contractCondition = value; }
        }

        private bool _addedFromShop = false;
        [DataMember]
        public bool IsAddedFromShop
        {
            get { return _addedFromShop; }
            set { _addedFromShop = value; }
        }
    }
}
