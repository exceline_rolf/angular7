﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/7/2012 3:07:40 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    [DataContract]
    public class SponsorSettingDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _lastSponsoredMembersCount = 0;
        [DataMember]
        public int LastSponsoredMembersCount
        {
            get { return _lastSponsoredMembersCount; }
            set { _lastSponsoredMembersCount = value; }
        }

        private int _currentMembersCount = 0;
        [DataMember]
        public int CurrentMembersCount
        {
            get { return _currentMembersCount; }
            set { _currentMembersCount = value; }
        }

        private bool _isSponsor = false;
        [DataMember]
        public bool IsSponsor
        {
            get { return _isSponsor; }
            set { _isSponsor = value; }
        }

        private DateTime _startDate = DateTime.Now;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime _endDate = DateTime.Now.AddYears(100);
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private int _sponsorDueDay = 0;
        [DataMember]
        public int SponsorDueDay
        {
            get { return _sponsorDueDay; }
            set { _sponsorDueDay = value; }
        }

        private decimal _minAmountAfterDiscount = 0.00M;
        [DataMember]
        public decimal MinAmountAfterDiscount
        {
            get { return _minAmountAfterDiscount; }
            set { _minAmountAfterDiscount = value; }
        }

        private int _minVisits = 0;
        [DataMember]
        public int MinVisits
        {
            get { return _minVisits; }
            set { _minVisits = value; }
        }

        private bool _isDisplayVisitNo = false;
        [DataMember]
        public bool IsDisplayVisitNo
        {
            get { return _isDisplayVisitNo; }
            set { _isDisplayVisitNo = value; }
        }

        private bool _isDisplayRemain = false;
        [DataMember]
        public bool IsDisplayRemain
        {
            get { return _isDisplayRemain; }
            set { _isDisplayRemain = value; }
        }

        private decimal _minPayment = 0.00M;
        [DataMember]
        public decimal MinPayment
        {
            get { return _minPayment; }
            set { _minPayment = value; }
        }

        private string _invoiceText = string.Empty;
        [DataMember]
        public string InvoiceText
        {
            get { return _invoiceText; }
            set { _invoiceText = value; }
        }

        private List<DiscountDC> _discountCategoryList = new List<DiscountDC>();
        [DataMember]
        public List<DiscountDC> DiscountCategoryList
        {
            get { return _discountCategoryList; }
            set { _discountCategoryList = value; }
        }

        private List<EmployeeCategoryDC> _employeeCategoryList = new List<EmployeeCategoryDC>();
        [DataMember]
        public List<EmployeeCategoryDC> EmployeeCategoryList
        {
            get { return _employeeCategoryList; }
            set { _employeeCategoryList = value; }
        }

        private int _invoicingPeriod = 1;
        [DataMember]
        public int InvoicingPeriod
        {
            get { return _invoicingPeriod; }
            set { _invoicingPeriod = value; }
        }

        private string _pinCode = string.Empty;
        [DataMember]
        public string PinCode
        {
            get { return _pinCode; }
            set { _pinCode = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private bool _isSponsorWithoutVist = false;
        [DataMember]
        public bool IsSponsorWithoutVist
        {
            get { return _isSponsorWithoutVist; }
            set { _isSponsorWithoutVist = value; }
        }


        private bool _isEmailInvoice = false;
        [DataMember]
        public bool IsEmailInvoice
        {
            get { return _isEmailInvoice; }
            set { _isEmailInvoice = value; }
        }



        private int _discountTypeId = -1;
        [DataMember]
        public int DiscountTypeId
        {
            get { return _discountTypeId; }
            set { _discountTypeId = value; }
        }

        private CategoryDC _discoutType = new CategoryDC();
        [DataMember]
        public CategoryDC DiscoutType
        {
            get { return _discoutType; }
            set { _discoutType = value; }
        }


        private int _packageId = -1;
        [DataMember]
        public int PackageId
        {
            get { return _packageId; }
            set { _packageId = value; }
        }

        private int _activityId = -1;
        [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        private bool _isActive = false;
        [DataMember]
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        private int _sponsorId = -1;
        [DataMember]
        public int SponsorId
        {
            get { return _sponsorId; }
            set { _sponsorId = value; }
        }

        private string _discountTypeName = string.Empty;
        [DataMember]
        public string DiscountTypeName
        {
            get { return _discountTypeName; }
            set { _discountTypeName = value; }
        }


        private EnrollmentFeePayerType _enrollmentFeePayer = EnrollmentFeePayerType.NONE;
        [DataMember]
        public EnrollmentFeePayerType EnrollmentFeePayer
        {
            get { return _enrollmentFeePayer; }
            set { _enrollmentFeePayer = value; }
        }

        private int _percentageModeId = -1;
        [DataMember]
        public int PercentageModeId
        {
            get { return _percentageModeId; }
            set { _percentageModeId = value; }
        }

        private int _amountModeId = - 1;
        [DataMember]
        public int AmountModeId
        {
            get { return _amountModeId; }
            set { _amountModeId = value; }
        }

        private List<SponsoredMemberDC> _sponsoredMembers = new List<SponsoredMemberDC>();
        [DataMember]
        public List<SponsoredMemberDC> SponsoredMembers
        {
            get { return _sponsoredMembers; }
            set { _sponsoredMembers = value; }
        }

    }
}
