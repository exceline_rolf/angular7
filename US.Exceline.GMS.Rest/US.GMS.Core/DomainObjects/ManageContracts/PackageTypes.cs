﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    public enum PackageTypes
    {
        NONE,
        PUNCHCARD ,
        FREE,
        PREMIUM ,
        NORMAL,
        TIMELIMITED,
        ADDITIONALCONTRACT,
        ADDICONTRTOTAL
    }
}
