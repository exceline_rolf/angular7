﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Core
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "7/05/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    [DataContract]
    public class PriceItemTypeDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _code = string.Empty;
        [DataMember]
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }


    }
}
