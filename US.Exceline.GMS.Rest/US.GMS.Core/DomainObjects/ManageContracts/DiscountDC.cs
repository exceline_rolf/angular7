﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/7/2012 3:13:54 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.Admin;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    [DataContract]
    public class DiscountDC
    {

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        private string _minNoContent = string.Empty;
        [DataMember]
        public string MinNoContent
        {
            get { return _minNoContent; }
            set { _minNoContent = value; }
        }


        private string _categoryName = string.Empty;
        [DataMember]
        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }

        private string _campaignText = string.Empty;
        [DataMember]
        public string CampaignText
        {
            get { return _campaignText; }
            set { _campaignText = value; }
        }



        private string _categoryCode = string.Empty;
        [DataMember]
        public string CategoryCode
        {
            get { return _categoryCode; }
            set { _categoryCode = value; }
        }



        private string _vendorName = string.Empty;
        [DataMember]
        public string VendorName
        {
            get { return _vendorName; }
            set { _vendorName = value; }
        }

        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private int _vendorId = -1;
        [DataMember]
        public int VendorId
        {
            get { return _vendorId; }
            set { _vendorId = value; }
        }

        private int _articleId = -1;
        [DataMember]
        public int ArticleId
        {
            get { return _articleId; }
            set { _articleId = value; }
        }
        private int _articleCategoryId = -1;
        [DataMember]
        public int ArticleCategoryId
        {
            get { return _articleCategoryId; }
            set { _articleCategoryId = value; }
        }

        private List<ArticleDC> _articleList = new List<ArticleDC>();
        [DataMember]
        public List<ArticleDC> ArticleList
        {
            get { return _articleList; }
            set { _articleList = value; }
        }

        private int _modeId = -1;
        [DataMember]
        public int ModeId
        {
            get { return _modeId; }
            set { _modeId = value; }
        }

        private int _minNumberOfArticles = 1;
        [DataMember]
        public int MinNumberOfArticles
        {
            get { return _minNumberOfArticles; }
            set { _minNumberOfArticles = value; }
        }

        private int _numberOfFreeItems = 0;
        [DataMember]
        public int NumberOfFreeItems
        {
            get { return _numberOfFreeItems; }
            set { _numberOfFreeItems = value; }
        }

        private DateTime _startDate = DateTime.Now;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime _endDate = DateTime.Now;
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private DiscountType _discountType = DiscountType.NONE;
        [DataMember]
        public DiscountType DiscountType
        {
            get { return _discountType; }
            set { _discountType = value; }
        }



        private int _sponsorId = -1;
        [DataMember]
        public int SponsorId
        {
            get { return _sponsorId; }
            set { _sponsorId = value; }
        }


        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _minVisitNo = 0;
        [DataMember]
        public int MinVisitsNo
        {
            get { return _minVisitNo; }
            set { _minVisitNo = value; }
        }

        private int _typeId = -1;
        [DataMember]
        public int TypeId
        {
            get { return _typeId; }
            set { _typeId = value; }
        }



        private string _typeName = string.Empty;
        [DataMember]
        public string TypeName
        {
            get { return _typeName; }
            set { _typeName = value; }
        }



        private int _percentageModeId = -1;
        [DataMember]
        public int PercentageModeId
        {
            get { return _percentageModeId; }
            set { _percentageModeId = value; }
        }

        private int _amountModeId = -1;
        [DataMember]
        public int AmountModeId
        {
            get { return _amountModeId; }
            set { _amountModeId = value; }
        }

        private string _groupDiscountName = string.Empty;
        [DataMember]
        public string GroupDiscountName
        {
            get { return _groupDiscountName; }
            set { _groupDiscountName = value; }
        }

        private string _activityDiscountName = string.Empty;
        [DataMember]
        public string ActivityDiscountName
        {
            get { return _activityDiscountName; }
            set { _activityDiscountName = value; }
        }

        private string _activityName = string.Empty;
        [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }


        //Minimum memebers count
        private int _minMemberNo = 0;
        [DataMember]
        public int MinMemberNo
        {
            get { return _minMemberNo; }
            set { _minMemberNo = value; }
        }

        private decimal _discountAmount = 0.00M;
        [DataMember]
        public decimal DiscountAmount
        {
            get { return _discountAmount; }
            set { _discountAmount = value; }
        }

        private decimal _discountPercentage = 0.00M;
        [DataMember]
        public decimal DiscountPercentage
        {
            get { return _discountPercentage; }
            set { _discountPercentage = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private DateTime _trailTime = DateTime.Now;
        [DataMember]
        public DateTime TrailTime
        {
            get { return _trailTime; }
            set { _trailTime = value; }
        }

        private bool _isActive = false;
        [DataMember]
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        private int _activityId = -1;
        [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }
        private int _categoryId = -1;
        [DataMember]
        public int DiscountCategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }

        private string _groupHeader = string.Empty;
        [DataMember]
        public string GroupHeader
        {
            get { return _groupHeader; }
            set { _groupHeader = value; }
        }

        private decimal _minPayment = 0.00M;
        [DataMember]
        public decimal MinPayment
        {
            get { return _minPayment; }
            set { _minPayment = value; }
        }

        #region Shop Discount

        private double _discount = 0;
        [DataMember]
        public double Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }

        private List<int> _itemList = new List<int>();
        [DataMember]
        public List<int> ItemList
        {
            get { return _itemList; }
            set { _itemList = value; }
        }

        private List<string> _itemNames = new List<string>();
        [DataMember]
        public List<string> ItemNames
        {
            get { return _itemNames; }
            set { _itemNames = value; }
        }

        private string _discountString = string.Empty;
        [DataMember]
        public string DiscountString
        {
            get { return _discountString; }
            set { _discountString = value; }
        }
        #endregion

        private decimal _highAmount = 0.00M;
        [DataMember]
        public decimal HighAmount
        {
            get { return _highAmount; }
            set { _highAmount = value; }
        }
        private decimal _highPercentage = 0.00M;
        [DataMember]
        public decimal HighPercentage
        {
            get { return _highPercentage; }
            set { _highPercentage = value; }
        }

        private decimal _lowAmount = 0.00M;
        [DataMember]
        public decimal LowAmount
        {
            get { return _lowAmount; }
            set { _lowAmount = value; }
        }
        private decimal _lowPercentage = 0.00M;
        [DataMember]
        public decimal LowPercentage
        {
            get { return _lowPercentage; }
            set { _lowPercentage = value; }
        }

        private bool _isHighPercentageEnable = false;
        [DataMember]
        public bool IsHighPercentageEnable
        {
            get { return _isHighPercentageEnable; }
            set { _isHighPercentageEnable = value; }
        }
        private bool _isLowPercentageEnable = false;
        [DataMember]
        public bool IsLowPercentageEnable
        {
            get { return _isLowPercentageEnable; }
            set { _isLowPercentageEnable = value; }
        }

        private bool _isHighAmoutEnable = false;
        [DataMember]
        public bool IsHighAmoutEnable
        {
            get { return _isHighAmoutEnable; }
            set { _isHighAmoutEnable = value; }
        }

        private bool _isLowAmoutEnable = false;
        [DataMember]
        public bool IsLowAmoutEnable
        {
            get { return _isLowAmoutEnable; }
            set { _isLowAmoutEnable = value; }
        }

    }
}
