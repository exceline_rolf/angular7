﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    [DataContract]
    public class ContractBookingDC
    {
        private int _bookingId = -1;
        [DataMember]
        public int BookingId
        {
          get { return _bookingId; }
          set { _bookingId = value; }
        }

        private string _resourceName = string.Empty;
        [DataMember]
        public string ResourceName
        {
            get { return _resourceName; }
            set { _resourceName = value; }
        }

        private int _resourceId = -1;
        [DataMember]
        public int ResourceId
        {
            get { return _resourceId; }
            set { _resourceId = value; }
        }

        private string _day = string.Empty;
        [DataMember]
        public string Day
        {
            get { return _day; }
            set { _day = value; }
        }

        private DateTime? _bookingStartTime;
        [DataMember]
        public DateTime? BookingStartTime
        {
            get { return _bookingStartTime; }
            set { _bookingStartTime = value; }
        }

        private DateTime? _bookingEndTime;
        [DataMember]
        public DateTime? BookingEndTime
        {
            get { return _bookingEndTime; }
            set { _bookingEndTime = value; }
        }

        private decimal _bookingAmount = 0;
        [DataMember]
        public decimal BookingAmount
        {
            get { return _bookingAmount; }
            set { _bookingAmount = value; }
        }
    }
}
