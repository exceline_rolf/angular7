﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    [DataContract]
    public class SponsoredMemberDC
    {


        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _sponsorId = -1;
        [DataMember]
        public int SponsorId
        {
            get { return _sponsorId; }
            set { _sponsorId = value; }
        }

        private int _memberId = -1;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private string _memberNo = string.Empty;
        [DataMember]
        public string MemberNo
        {
            get { return _memberNo; }
            set { _memberNo = value; }
        }

        private string _memberName = string.Empty;
        [DataMember]
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        private int _sponsorNo = -1;
        [DataMember]
        public int SponsorNo
        {
            get { return _sponsorNo; }
            set { _sponsorNo = value; }
        }

        private string _sponsorName = string.Empty;
        [DataMember]
        public string SponsorName
        {
            get { return _sponsorName; }
            set { _sponsorName = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        EmployeeCategoryDC _sponsoredLevel = new EmployeeCategoryDC();
        [DataMember]
        public EmployeeCategoryDC SponsoredLevel
        {
            get { return _sponsoredLevel; }
            set { _sponsoredLevel = value; }
        }

        int _sponsoredLevelId = -1;
        [DataMember]
        public int SponsoredLevelId
        {
            get { return _sponsoredLevelId; }
            set { _sponsoredLevelId = value; }
        }

        string _sponsoredLevelName = string.Empty;
        [DataMember]
        public string SponsoredLevelName
        {
            get { return _sponsoredLevelName; }
            set { _sponsoredLevelName = value; }
        }

        DateTime _startDate = DateTime.Now;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        DateTime  _endDate = DateTime.Now.AddYears(100); // Default endYear is in the distant future
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        List<EmployeeCategoryDC> _employeeCategoryList = new List<EmployeeCategoryDC>();
        [DataMember]
        public List<EmployeeCategoryDC> EmployeeCategoryList
        {
            get { return _employeeCategoryList; }
            set { _employeeCategoryList = value; }
        }

       
        private string _role = string.Empty;
        [DataMember]
        public string Role
        {
            get { return _role; }
            set { _role = value; }
        }

        private int _sponsorBranchId = -1;
        [DataMember]
        public int SponsorBranchId
        {
            get { return _sponsorBranchId; }
            set { _sponsorBranchId = value; }
        }
    }
}
