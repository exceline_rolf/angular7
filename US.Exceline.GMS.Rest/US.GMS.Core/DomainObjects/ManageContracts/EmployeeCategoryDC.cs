﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/7/2012 3:19:17 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    [DataContract]
    public class EmployeeCategoryDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private decimal _monthlyAmount = 0.00M;
        [DataMember]
        public decimal MonthlyAmount
        {
            get { return _monthlyAmount; }
            set { _monthlyAmount = value; }
        }

        private decimal _amountPerVisit = 0.00M;
        [DataMember]
        public decimal AmountPerVisit
        {
            get { return _amountPerVisit; }
            set { _amountPerVisit = value; }
        }

        private string _Name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private string _levelName = string.Empty;
        [DataMember]
        public string LevelName
        {
            get { return _levelName; }
            set { _levelName = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private int _sponsorId = -1;
        [DataMember]
        public int SponsorId
        {
            get { return _sponsorId; }
            set { _sponsorId = value; }
        }

        private int _minVisits = 0;
        [DataMember]
        public int MinVisits
        {
            get { return _minVisits; }
            set { _minVisits = value; }
        }

        private int _levelId = 0;
        [DataMember]
        public int LevelId
        {
            get { return _levelId; }
            set { _levelId = value; }
        }


        private decimal _sponsoringPercentage = 0.00M;
        [DataMember]
        public decimal SponsoringPercentage
        {
            get { return _sponsoringPercentage; }
            set { _sponsoringPercentage = value; }
        }

        private bool _isAmountEnable = true;
        [DataMember]
        public bool IsAmountEnable
        {
            get { return _isAmountEnable; }
            set { _isAmountEnable = value; }
        }

        private bool _isPercentageEnable = true;
        [DataMember]
        public bool IsPercentageEnable
        {
            get { return _isPercentageEnable; }
            set { _isPercentageEnable = value; }
        }

        private bool _isDefaultCategory = false;
        [DataMember]
        public bool IsDefaultCategory
        {
            get { return _isDefaultCategory; }
            set { _isDefaultCategory = value; }
        }

    }
}
