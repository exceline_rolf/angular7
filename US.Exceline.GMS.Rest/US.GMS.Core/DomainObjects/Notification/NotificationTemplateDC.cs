﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 09/08/2012 2:31:53 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Core.DomainObjects.Notification
{
    public class NotificationTemplateDC
    {
        private int _id = -1;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _branchId = -1;

        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _text = string.Empty;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private List<CategoryTypeDC> _textCategoryTypes = new List<CategoryTypeDC>();

        public List<CategoryTypeDC> TextCategoryTypes
        {
            get { return _textCategoryTypes; }
            set { _textCategoryTypes = value; }
        }

        private DateTime _createdDate = DateTime.Now;

        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }


        private string _createdUser = string.Empty;

        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private NotifyMethodType _notifyMethod = new NotifyMethodType();

        public NotifyMethodType NotifyMethod
        {
            get { return _notifyMethod; }
            set { _notifyMethod = value; }
        }


        private int _typeId = -1;

        public int TypeId
        {
            get { return _typeId; }
            set { _typeId = value; }
        }

        private string _categoryName = string.Empty;

        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }

    }
}
