﻿
namespace US.GMS.Core.DomainObjects.Notification
{
    public enum NotifyMethodType
    {

        EMAIL,
        SMS,
        SMSEMAIL,
        POPUP,
        POST,
        EVENTLOG,
        ERRORLOG,
        NONE,
        OTHER
    }
}
