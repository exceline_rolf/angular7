﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Payments
{
    [DataContract]
    public class ExcePaymentInfoDC
    {
        private int _paymentArItemNo = -1;
        [DataMember]
        public int PaymentArItemNo
        {
            get { return _paymentArItemNo; }
            set { _paymentArItemNo = value; }
        }

        private string _invoiceNo = string.Empty;
        [DataMember]
        public string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }

        private DateTime _paymentDateTime;
        [DataMember]
        public DateTime PaymentDateTime
        {
            get { return _paymentDateTime; }
            set { _paymentDateTime = value; }
        }

        private DateTime _paymentRegDateTime;
        [DataMember]
        public DateTime PaymentRegDateTime
        {
            get { return _paymentRegDateTime; }
            set { _paymentRegDateTime = value; }
        }

        private int _paymentTypeId = -1;
        [DataMember]
        public int PaymentTypeId
        {
            get { return _paymentTypeId; }
            set { _paymentTypeId = value; }
        }

        private string _paymentType = string.Empty;
        [DataMember]
        public string PaymentType
        {
            get { return _paymentType; }
            set { _paymentType = value; }
        }

        private string _contractNo = string.Empty;
        [DataMember]
        public string ContractNo
        {
            get { return _contractNo; }
            set { _contractNo = value; }
        }

        private string _orderNo = string.Empty;
        [DataMember]
        public string OrderNo
        {
            get { return _orderNo; }
            set { _orderNo = value; }
        }

        private string _contractTemplateNo = string.Empty;
        [DataMember]
        public string ContractTemplateNo
        {
            get { return _contractTemplateNo; }
            set { _contractTemplateNo = value; }
        }

        private decimal _paymentAmount = 0;
        [DataMember]
        public decimal PaymentAmount
        {
            get { return _paymentAmount; }
            set { _paymentAmount = value; }
        }

        private string _creditorName = string.Empty;
        [DataMember]
        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }
        }

        private string _registeredEmpName = string.Empty;
        [DataMember]
        public string RegisteredEmpName
        {
            get { return _registeredEmpName; }
            set { _registeredEmpName = value; }
        }

        private string _gymName = string.Empty;
        [DataMember]
        public string GymName
        {
            get { return _gymName; }
            set { _gymName = value; }
        }


    }
}
