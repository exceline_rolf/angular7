﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Payments
{
    [DataContract]
    public  class AccountDC
    {
        private int _id = -1;
        [DataMember]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _accountNo = string.Empty;
         [DataMember]
        public string AccountNo
        {
            get { return _accountNo; }
            set { _accountNo = value; }
        }
        private string _name = string.Empty;
         [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}
