﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Payments
{
    [DataContract]
    public class VatCodeDC
    {

        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }


        private int _vatCode = -1;
        [DataMember]
        public int VatCode
        {
            get { return _vatCode; }
            set { _vatCode = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        private decimal _rate;
        [DataMember]
        public decimal Rate
        {
            get { return _rate; }
            set { _rate = value; }
        }

        private AccountDC _vatAccount = new AccountDC();
        [DataMember]
        public AccountDC VatAccount
        {
            get { return _vatAccount; }
            set { _vatAccount = value; }
        }

        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private int _versionId = -1;
        [DataMember]
        public int VersionId
        {
            get { return _versionId; }
            set { _versionId = value; }
        }

        private DateTime _startDate = DateTime.Now;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime _endDate = DateTime.Now;
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private string _registerUser = string.Empty;
        [DataMember]
        public string RegisteredUser
        {
            get { return _registerUser; }
            set { _registerUser = value; }
        }

        private DateTime _registeredDate;
        [DataMember]
        public DateTime RegisteredDate
        {
            get { return _registeredDate; }
            set { _registeredDate = value; }
        }

        private int _status = -1;
        [DataMember]
        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private string _formattedName = string.Empty;
        [DataMember]
        public string FormattedName
        {
            get { return _formattedName; }
            set { _formattedName = value; }
        }

        private bool _isCurrentVersion;
        [DataMember]
        public bool IsCurrentVersion
        {
            get { return _isCurrentVersion; }
            set { _isCurrentVersion = value; }
        }

        private bool _isActive;
        [DataMember]
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
    }
}
