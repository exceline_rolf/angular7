﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingClassCalendar
    {
        private int _classId = -1;
        private string _classDate;
        private string _classStarttime;
        private string _classEndtime;
        private int _maxParticipants = -1;
        private int _classtypeId = -1;
        private List<int> _instructorIds = new List<int>();
        private string _classRoom = string.Empty;
        private int _visitCount = -1;
        private decimal _visitPercentage = -1;

        [DataMember(Order = 1)]
        public int ClassId
        {
            get { return _classId; }
            set { _classId = value; }
        }

        [DataMember(Order = 2)]
        public string ClassDate
        {
            get { return _classDate; }
            set { _classDate = value; }
        }

        [DataMember(Order = 3)]
        public string ClassStarttime
        {
            get { return _classStarttime; }
            set { _classStarttime = value; }
        }

        [DataMember(Order = 4)]
        public string ClassEndtime
        {
            get { return _classEndtime; }
            set { _classEndtime = value; }
        }

        [DataMember(Order = 5)]
        public int MaxParticipants
        {
            get { return _maxParticipants; }
            set { _maxParticipants = value; }
        }

        [DataMember(Order = 6)]
        public int ClasstypeId
        {
            get { return _classtypeId; }
            set { _classtypeId = value; }
        }

        [DataMember(Order = 7)]
        public List<int> InstructorIds
        {
            get { return _instructorIds; }
            set { _instructorIds = value; }
        }

        [DataMember(Order = 8)]
        public string ClassRoom
        {
            get { return _classRoom; }
            set { _classRoom = value; }
        }

        [DataMember(Order = 9)]
        public int VisitCount
        {
            get { return _visitCount; }
            set { _visitCount = value; }
        }

        [DataMember(Order = 10)]
        public decimal VisitPercentage
        {
            get { return _visitPercentage; }
            set { _visitPercentage = value; }
        }
    }
}
