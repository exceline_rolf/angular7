﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingInterest
    {
            private int _interestId = -1;
            private string _interestName = string.Empty;

            [DataMember(Order = 1)]
            public int InterestId
            {
                get { return _interestId; }
                set { _interestId = value; }
            }

            [DataMember(Order = 2)]
            public string InterestName
            {
                get { return _interestName; }
                set { _interestName = value; }
            }
        }
    }

