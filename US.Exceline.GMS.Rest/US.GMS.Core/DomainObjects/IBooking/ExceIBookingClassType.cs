﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingClassType
    {
        private int _classtypeId = -1;
        private string _classtypeName = string.Empty;
        private int _maxParticipants = -1;
        private int _classGroup = -1;
        private List<int> _classCategoryIds = new List<int>();
        private List<int> _classKeywordIds = new List<int>();
        private List<string> _classKeywords = new List<string>();
        private int _classLevelId = -1;
        private string _colorCode = string.Empty;
        private string _classDescription = string.Empty;
        private string _englishComment = string.Empty;

        [DataMember(Order = 1)]
        public int ClasstypeId
        {
            get { return _classtypeId; }
            set { _classtypeId = value; }
        }

        [DataMember(Order = 2)]
        public string ClasstypeName
        {
            get { return _classtypeName; }
            set { _classtypeName = value; }
        }

        [DataMember(Order = 3)]
        public int ClassGroup
        {
            get { return _classGroup; }
            set { _classGroup = value; }
        }

        [DataMember(Order = 4)]
        public List<int> ClassCategoryIds
        {
            get { return _classCategoryIds; }
            set { _classCategoryIds = value; }
        }

        [DataMember(Order = 5)]
        public List<int> ClassKeywordIds
        {
            get { return _classKeywordIds; }
            set { _classKeywordIds = value; }
        }

        [DataMember(Order = 6)]
        public int ClassLevelId
        {
            get { return _classLevelId; }
            set { _classLevelId = value; }
        }

        [DataMember(Order = 7)]
        public string ColorCode
        {
            get { return _colorCode; }
            set { _colorCode = value; }
        }

        [DataMember(Order = 8)]
        public string ClassDescription
        {
            get { return _classDescription; }
            set { _classDescription = value; }
        }

        [DataMember(Order = 9)]
        public string EnglishComment
        {
            get { return _englishComment; }
            set { _englishComment = value; }
        }
        

    }
}
