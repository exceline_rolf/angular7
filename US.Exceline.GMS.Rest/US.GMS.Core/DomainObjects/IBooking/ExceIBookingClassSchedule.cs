﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingClassSchedule
    {
        [DataMember(Order = 1)]
        public int ClassId { get; set; }
        [DataMember(Order = 2)]
        public int BranchId { get; set; }
        [DataMember(Order = 3)]
        public int ClassActivityType { get; set; }
        [DataMember(Order = 4)]
        public DateTime ClassDate { get; set; }
        [DataMember(Order = 5)]
        public DateTime StartTime { get; set; }
        [DataMember(Order = 6)]
        public DateTime EndTime { get; set; }
        [DataMember(Order = 7)]
        public int MaxNumOfParticipants { get; set; }
        [DataMember(Order = 8)]
        public int ClassTypeId { get; set; }
        [DataMember(Order = 9)]
        public int InstructorId { get; set; }
        [DataMember(Order = 10)]
        public string ClassRoom { get; set; }
        [DataMember(Order = 11)]
        public int VisitCount { get; set; }
        [DataMember(Order = 12)]
        public int VisitPercentage { get; set; }
        [DataMember(Order = 13)]
        public int NewInstructorId { get; set; }
    }
}
