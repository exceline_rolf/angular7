﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingFreezeResignCategories
    {
        private int _id = -1;
        private int _categoryTypeId = -1;
        private string _categoryName = string.Empty;
        private string _code = string.Empty;
        private int _activeStatus = -1;

        [DataMember(Order = 1)]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember(Order = 2)]
        public int CategoryTypeId
        {
            get { return _categoryTypeId; }
            set { _categoryTypeId = value; }
        }

        [DataMember(Order = 3)]
        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }

        [DataMember(Order = 4)]
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        [DataMember(Order = 4)]
        public int ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }
    }
}
