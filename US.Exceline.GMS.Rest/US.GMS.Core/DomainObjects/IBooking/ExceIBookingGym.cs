﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingGym
    {
        private int _gymId = -1;
        private string _branchName = string.Empty;
        private string _address1 = string.Empty;
        private string _address2 = string.Empty;
        private string _address3 = string.Empty;
        private string _postCode = string.Empty;
        private string _postPlace = string.Empty;
        private string _gymAccountNo = string.Empty;

        [DataMember(Order = 1)]
        public int GymId
        {
            get { return _gymId; }
            set { _gymId = value; }
        }

        [DataMember(Order = 2)]
        public string GymName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        [DataMember(Order = 3)]
        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        [DataMember(Order = 4)]
        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        [DataMember(Order = 5)]
        public string Address3
        {
            get { return _address3; }
            set { _address3 = value; }
        }

        [DataMember(Order = 6)]
        public string PostCode
        {
            get { return _postCode; }
            set { _postCode = value; }
        }

        [DataMember(Order = 7)]
        public string PostPlace
        {
            get { return _postPlace; }
            set { _postPlace = value; }
        }

        [DataMember(Order = 8)]
        public string GymAccountNo
        {
            get { return _gymAccountNo; }
            set { _gymAccountNo = value; }
        }
    }
}
