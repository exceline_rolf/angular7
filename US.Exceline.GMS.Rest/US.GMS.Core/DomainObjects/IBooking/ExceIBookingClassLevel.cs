﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingClassLevel
    {
        private int _classlevelId = -1;
        private string _classlevelName = string.Empty;

        [DataMember(Order = 1)]
        public int ClasslevelId
        {
            get { return _classlevelId; }
            set { _classlevelId = value; }
        }

        [DataMember(Order = 2)]
        public string ClasslevelName
        {
            get { return _classlevelName; }
            set { _classlevelName = value; }
        }
    }
}
