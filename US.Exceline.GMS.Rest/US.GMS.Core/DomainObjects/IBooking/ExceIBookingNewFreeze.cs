﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.Notification;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingNewFreeze
    {
        private string _systemId = string.Empty;
        private string _gymId = string.Empty;                          
        private int _memberContractid = -1;
        private int _categoryId = -1;        
        private string _fromDate = string.Empty;
        private string _note = string.Empty;
        private int _freezeMonths = -1;
        private bool _isExtended = false;

        [DataMember(Order = 1)]
        public string SystemId
        {
            get { return _systemId; }
            set { _systemId = value; }
        }

        [DataMember(Order = 2)]
        public string GymId
        {
            get { return _gymId; }
            set { _gymId = value; }
        }        
       
        [DataMember(Order = 3)]
        public int MemberContractid
        {
            get { return _memberContractid; }
            set { _memberContractid = value; }
        }

        
        [DataMember(Order = 4)]
        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }
        
        
        [DataMember(Order = 5)]
        public string FromDate
        {
            get { return _fromDate; }
            set { _fromDate = value; }
        }
   
        [DataMember(Order = 6)]
        public string Note
        {
            get { return _note; }
            set { _note = value; }
        }

        [DataMember(Order = 7)]
        public int FreezeMonths
        {
            get { return _freezeMonths; }
            set { _freezeMonths = value; }
        }

        [DataMember(Order = 8)]
        public bool IsExtended
        {
            get { return _isExtended; }
            set { _isExtended = value; }
        }
        
    }
}