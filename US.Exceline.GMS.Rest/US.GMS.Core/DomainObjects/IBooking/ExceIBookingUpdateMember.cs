﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingUpdateMember
    {
        private string _systemId = string.Empty;
        private string _gymId = string.Empty;
        private string _customerNo = string.Empty;
        private string _newFirstname = string.Empty;
        private string _newLastName = string.Empty;
        private string _newAddress = string.Empty;
        private string _newPostcode = string.Empty;
        private string _newPostplace = string.Empty;
        private string _newPhonePrivate = string.Empty;
        private string _newPhoneWork = string.Empty;
        private string _newEmail = string.Empty;
        private string _newAccountnumber = string.Empty;
        private string _pinCode = string.Empty;
        private string _newPhoneMobile = string.Empty;
        private string _newPhoneMobileCountryCode = string.Empty;
        private bool _sendEmail = false;
        private bool _sendSMS = false;
        private bool _customerClub = false;
        private string _memcardno = string.Empty;

        [DataMember(Order = 1)]
        public string SystemId
        {
            get { return _systemId; }
            set { _systemId = value; }
        }

        [DataMember(Order = 2)]
        public string GymId
        {
            get { return _gymId; }
            set { _gymId = value; }
        }

        [DataMember(Order = 3)]
        public string CustomerNo
        {
            get { return _customerNo; }
            set { _customerNo = value; }
        }

        [DataMember(Order = 4)]
        public string NewFirstname
        {
            get { return _newFirstname; }
            set { _newFirstname = value; }
        }

        [DataMember(Order = 5)]
        public string NewLastName
        {
            get { return _newLastName; }
            set { _newLastName = value; }
        }

        [DataMember(Order = 6)]
        public string NewAddress
        {
            get { return _newAddress; }
            set { _newAddress = value; }
        }

        [DataMember(Order = 7)]
        public string NewPostcode
        {
            get { return _newPostcode; }
            set { _newPostcode = value; }
        }

        [DataMember(Order = 8)]
        public string NewPostplace
        {
            get { return _newPostplace; }
            set { _newPostplace = value; }
        }

        [DataMember(Order = 9)]
        public string NewPhonePrivate
        {
            get { return _newPhonePrivate; }
            set { _newPhonePrivate = value; }
        }

        [DataMember(Order = 10)]
        public string NewPhoneWork
        {
            get { return _newPhoneWork; }
            set { _newPhoneWork = value; }
        }

        [DataMember(Order = 11)]
        public string NewEmail
        {
            get { return _newEmail; }
            set { _newEmail = value; }
        }

        [DataMember(Order = 12)]
        public string NewAccountnumber
        {
            get { return _newAccountnumber; }
            set { _newAccountnumber = value; }
        }

        [DataMember(Order = 13)]
        public string PinCode
        {
            get { return _pinCode; }
            set { _pinCode = value; }
        }

        [DataMember(Order = 14)]
        public string NewPhoneMobile
        {
            get { return _newPhoneMobile; }
            set { _newPhoneMobile = value; }
        }

        [DataMember(Order = 15)]
        public string NewPhoneMobileCountryCode
        {
            get { return _newPhoneMobileCountryCode; }
            set { _newPhoneMobileCountryCode = value; }
        }


        [DataMember(Order = 16)]
        public bool SendEmail
        {
            get { return _sendEmail; }
            set { _sendEmail = value; }
        }

        [DataMember(Order = 17)]
        public bool SendSMS
        {
            get { return _sendSMS; }
            set { _sendSMS = value; }
        }

        [DataMember(Order = 18)]
        public bool CustomerClub
        {
            get { return _customerClub; }
            set { _customerClub = value; }
        }
        [DataMember(Order = 19)]
        public string MemCardNo
        {
            get { return _memcardno; }
            set { _memcardno = value; }
        }
    }
}
