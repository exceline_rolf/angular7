﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingResource
    {
        private int _id = -1;
        [DataMember(Order = 1)]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
        [DataMember(Order = 2)]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _categoryId = -1;
        [DataMember(Order = 3)]
        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }

        private int _articlId = -1;
        [DataMember(Order = 4)]
        public int ArticlId
        {
            get { return _articlId; }
            set { _articlId = value; }
        }

        private decimal _articlePricePerTimeUnit;
        [DataMember(Order = 5)]
        public decimal ArticlePricePerTimeUnit
        {
            get { return _articlePricePerTimeUnit; }
            set { _articlePricePerTimeUnit = value; }
        }

        private int _articleTimeUnit;
        [DataMember(Order = 5)]
        public int ArticleTimeUnit
        {
            get { return _articleTimeUnit; }
            set { _articleTimeUnit = value; }
        }

    }
}
