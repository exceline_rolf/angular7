﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.IBooking
{
    public class ExceIBookingInvoice
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        public string InvoiceNo { get; set; }

        [DataMember(Order = 3)]
        public string InvoiceDate { get; set; }

        [DataMember(Order = 4)]
        public string DueDate { get; set; }

        [DataMember(Order = 5)]
        public string ContractNumber { get; set; }

        [DataMember(Order = 6)]
        public decimal Amount { get; set; }

        [DataMember(Order = 7)]
        public decimal Balance { get; set; }

        [DataMember(Order = 8)]
        public string CustomerId { get; set; }

        [DataMember(Order = 9)]
        public string OriginalCustomerId { get; set; }

        [DataMember(Order = 10)]
        public string URL { get; set; }

    }
}
