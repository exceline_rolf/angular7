﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.DomainObjects.IBooking
{
    public class ExceIBookingNotificationDetails
    {
        private int _branchId = -1;
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _gymName = string.Empty;
        public string GymName
        {
            get { return _gymName; }
            set { _gymName = value; }
        }

        private string _templateName = string.Empty;
        public string TemplateName
        {
            get { return _templateName; }
            set { _templateName = value; }
        }

        private decimal _ATGAmount = 0;
        public decimal ATGAmount
        {
            get { return _ATGAmount; }
            set { _ATGAmount = value; }
        }
    }
}
