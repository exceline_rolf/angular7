﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingShowUp
    {
        private int _customerId = -1;
        private string _showupDate;
        private string _showupTime;
        private int _classActivityType = -1;

        [DataMember(Order = 1)]
        public int CustomerNo
        {
            get { return _customerId; }
            set { _customerId = value; }
        }

        [DataMember(Order = 2)]
        public string ShowupDate
        {
            get { return _showupDate; }
            set { _showupDate = value; }
        }

        [DataMember(Order = 3)]
        public string ShowupTime
        {
            get { return _showupTime; }
            set { _showupTime = value; }
        }

        [DataMember(Order = 4)]
        public int ClassActivityType
        {
            get { return _classActivityType; }
            set { _classActivityType = value; }
        }
    }
}
