﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingClass
    {
        [DataMember(Order = 1)]
        public int ClassId { get; set; }
        [DataMember(Order = 2)]
        public string ClassName { get; set; }
        [DataMember(Order = 3)]
        public string Description { get; set; }
        [DataMember(Order = 4)]
        public string Category { get; set; }
        [DataMember(Order = 5)]
        public string Payable { get; set; }
        [DataMember(Order = 6)]
        public decimal DefaultPrice { get; set; }
        [DataMember(Order = 7)]
        public int MaxNumOfParticipants { get; set; }
    }
}
