﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingNewResign
    {
        private string _systemId = string.Empty;
        private int _memberContractId = -1;
        private string _memberContractNo = string.Empty;
        private int _pendingOrders = -1;
        private int _memberId = -1;
        private decimal _amount = 0.00M;
        private string _resignedDate;
        private string _contractEndDate;
        private int _lockInPeriod = 0;
        private string _lockUntillDate;
        private int _resignCategoryId = -1;
        private List<int> _deletedOrders = new List<int>();
        private List<int> _mergedOrders = new List<int>();
        private int _memberFeeArticleID = -1;


        [DataMember(Order = 1)]
        public string SystemId
        {
            get { return _systemId; }
            set { _systemId = value; }
        }

        [DataMember(Order = 2)]
        public int MemberContractId
        {
            get { return _memberContractId; }
            set { _memberContractId = value; }
        }


        [DataMember(Order = 3)]
        public string MemberContractNo
        {
            get { return _memberContractNo; }
            set { _memberContractNo = value; }
        }


        [DataMember(Order = 4)]
        public int PendingOrders
        {
            get { return _pendingOrders; }
            set { _pendingOrders = value; }
        }


        [DataMember(Order = 5)]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }


        [DataMember(Order = 6)]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }



        [DataMember(Order = 7)]
        public string ResignedDate
        {
            get { return _resignedDate; }
            set { _resignedDate = value; }
        }


        [DataMember(Order = 8)]
        public string ContractEndDate
        {
            get { return _contractEndDate; }
            set { _contractEndDate = value; }
        }


        [DataMember(Order = 9)]
        public int LockInPeriod
        {
            get { return _lockInPeriod; }
            set { _lockInPeriod = value; }
        }


        [DataMember(Order = 10)]
        public string LockUntillDate
        {
            get { return _lockUntillDate; }
            set { _lockUntillDate = value; }
        }


        [DataMember(Order = 11)]
        public int ResignCategoryId
        {
            get { return _resignCategoryId; }
            set { _resignCategoryId = value; }
        }


        [DataMember(Order = 12)]
        public List<int> DeletedOrders
        {
            get { return _deletedOrders; }
            set { _deletedOrders = value; }
        }


        [DataMember(Order = 13)]
        public List<int> MergedOrders
        {
            get { return _mergedOrders; }
            set { _mergedOrders = value; }
        }


        [DataMember(Order = 14)]
        public int MemberFeeArticleID
        {
            get { return _memberFeeArticleID; }
            set { _memberFeeArticleID = value; }
        }

    }
}
