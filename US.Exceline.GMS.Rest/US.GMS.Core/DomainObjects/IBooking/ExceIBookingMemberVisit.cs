﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingMemberVisit
    {
        private string _systemId = string.Empty;
        private string _gymId = string.Empty;
        private string _customerNo = string.Empty;
        private string _visitDateTime = string.Empty;

        [DataMember(Order = 1)]
        public string SystemId
        {
            get { return _systemId; }
            set { _systemId = value; }
        }

        [DataMember(Order = 2)]
        public string GymId
        {
            get { return _gymId; }
            set { _gymId = value; }
        }

        [DataMember(Order = 3)]
        public string CustomerNo
        {
            get { return _customerNo; }
            set { _customerNo = value; }
        }

        [DataMember(Order = 4)]
        public string VisitDateTime
        {
            get { return _visitDateTime; }
            set { _visitDateTime = value; }
        }
    }
}
