﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingSystem
    {
        private string _systemName = string.Empty;
        private string _address1 = string.Empty;
        private string _address2 = string.Empty;
        private string _address3 = string.Empty;
        private string _postCode = string.Empty;
        private string _postPlace = string.Empty;
        private string _contactPerson = string.Empty;
        private string _email = string.Empty;
        private string _mobile = string.Empty;
        private string _accountNumber = string.Empty;
        private string _contractConditions = string.Empty;

        [DataMember(Order = 1)]
        public string SystemName
        {
            get { return _systemName; }
            set { _systemName = value; }
        }

        [DataMember(Order = 2)]
        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        [DataMember(Order = 3)]
        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        [DataMember(Order = 4)]
        public string Address3
        {
            get { return _address3; }
            set { _address3 = value; }
        }

        [DataMember(Order = 5)]
        public string PostCode
        {
            get { return _postCode; }
            set { _postCode = value; }
        }

        [DataMember(Order = 6)]
        public string PostPlace
        {
            get { return _postPlace; }
            set { _postPlace = value; }
        }

        [DataMember(Order = 7)]
        public string ContactPerson
        {
            get { return _contactPerson; }
            set { _contactPerson = value; }
        }

        [DataMember(Order = 8)]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        [DataMember(Order = 9)]
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        [DataMember(Order = 10)]
        public string AccountNumber
        {
            get { return _accountNumber; }
            set { _accountNumber = value; }
        }

        [DataMember(Order = 11)]
        public string ContractConditions
        {
            get { return _contractConditions; }
            set { _contractConditions = value; }
        }
    }
}
