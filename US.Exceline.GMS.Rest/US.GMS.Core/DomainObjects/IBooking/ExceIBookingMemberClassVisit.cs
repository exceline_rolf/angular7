﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingMemberClassVisit
    {
        private string _systemId = string.Empty;
        private string _gymId = string.Empty;
        private string _customerId = string.Empty;
        private string _classId = string.Empty;
        private string _visitStatus = "True";

        [DataMember(Order = 1)]
        public string SystemId
        {
            get { return _systemId; }
            set { _systemId = value; }
        }

        [DataMember(Order = 2)]
        public string GymId
        {
            get { return _gymId; }
            set { _gymId = value; }
        }

        [DataMember(Order = 3)]
        public string CustomerId
        {
            get { return _customerId; }
            set { _customerId = value; }
        }

        [DataMember(Order = 4)]
        public string ClassId
        {
            get { return _classId; }
            set { _classId = value; }
        }

        [DataMember(Order = 5)]
        public string VisitStatus
        {
            get { return _visitStatus; }
            set { _visitStatus = value; }
        }
    }
}
