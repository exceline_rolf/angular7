﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.IBooking
{
    public class ExceIBookingModifiedMember
    {
        private string _name = string.Empty;
        private int _customerID = -1;
        private int _parentcustomerID = -1;
        private int _ContractNo = -1;
        private int _contractTempNo = -1;
        private string _contractTempName = string.Empty;
        private string _conractStartDate = string.Empty;
        private string _contractEndDate = string.Empty;
        private string _activeFreezeStartDate = string.Empty;
        private string _activeFreezeEndDate = string.Empty;
        private int _activeStatus = -1;
        private string _lastModifiedDateTime = string.Empty;
        private string _createdDateTime = string.Empty;
        private string _notifyMethod = string.Empty;

        [DataMember(Order = 1)]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [DataMember(Order = 2)]
        public int CustomerID
        {
            get { return _customerID; }
            set { _customerID = value; }
        }

        [DataMember(Order = 3)]
        public int ParentCustomerID
        {
            get { return _parentcustomerID; }
            set { _parentcustomerID = value; }
        }


        [DataMember(Order = 4)]
        public int ContractNo
        {
            get { return _ContractNo; }
            set { _ContractNo = value; }
        }

        [DataMember(Order = 5)]
        public int ContractTempNo
        {
            get { return _contractTempNo; }
            set { _contractTempNo = value; }
        }

        [DataMember(Order = 6)]
        public string ContractTempName
        {
            get { return _contractTempName; }
            set { _contractTempName = value; }
        }

        [DataMember(Order = 7)]
        public string ContractStartDate
        {
            get { return _conractStartDate; }
            set { _conractStartDate = value; }
        }

        [DataMember(Order = 8)]
        public string ContractEndDate
        {
            get { return _contractEndDate; }
            set { _contractEndDate = value; }
        }

        [DataMember(Order = 9)]
        public string ActiveFreezeStartDate
        {
            get { return _activeFreezeStartDate; }
            set { _activeFreezeStartDate = value; }
        }

        [DataMember(Order = 10)]
        public string ActiveFreezeEndDate
        {
            get { return _activeFreezeEndDate; }
            set { _activeFreezeEndDate = value; }
        }

        [DataMember(Order = 11)]
        public int ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        [DataMember(Order = 12)]
        public string LastModifiedDateTime
        {
            get { return _lastModifiedDateTime; }
            set { _lastModifiedDateTime = value; }
        }

        [DataMember(Order = 13)]
        public string CreatedDateTime
        {
            get { return _createdDateTime; }
            set { _createdDateTime = value; }
        }

        [DataMember(Order = 14)]
        public string NotifyMethod
        {
            get { return _notifyMethod; }
            set { _notifyMethod = value; }
        }
    }
}
