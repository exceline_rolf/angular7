﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingClassCategory
    {
        private int _classcategoryId = -1;
        private string _classcategoryName = string.Empty;

        [DataMember(Order = 1)]
        public int ClasscategoryId
        {
            get { return _classcategoryId; }
            set { _classcategoryId = value; }
        }

        [DataMember(Order = 2)]
        public string ClasscategoryName
        {
            get { return _classcategoryName; }
            set { _classcategoryName = value; }
        }
    }
}
