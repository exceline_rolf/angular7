﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.IBooking
{
   [DataContract]
   public class ExceIBookingResourceAvailableTime
   {
       private int _id = -1;
       [DataMember(Order = 1)]
       public int Id
       {
           get { return _id; }
           set { _id = value; }
       }

       private string _day = string.Empty;
       [DataMember(Order = 2)]
       public string Day
       {
           get { return _day; }
           set { _day = value; }
       }

       private List<ExceIBookingResourceScheduleTime> _availableTimes;
       [DataMember(Order = 3,Name = "AvailableTime")]
       public List<ExceIBookingResourceScheduleTime> AvailableTimes
       {
           get { return _availableTimes; }
           set { _availableTimes = value; }
       }

       private string _fromTime = string.Empty;
       public string FromTime
       {
           get { return _fromTime; }
           set { _fromTime = value; }
       }

       private String _toTime = string.Empty;
       public string ToTime
       {
           get { return _toTime; }
           set { _toTime = value; }
       }

      

      

       //[DataMember(Order = 3, Name = "Array")]
       //public List<ExceIBookingResourceScheduleTime> ScheduleTime
       //{
       //    get { return _scheduleTime; }
       //    set { _scheduleTime = value; }
       //}

   }
}
