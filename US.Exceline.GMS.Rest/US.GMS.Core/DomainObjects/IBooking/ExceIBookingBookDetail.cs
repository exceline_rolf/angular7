﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingBookDetail
    {
        [DataMember]
        public string CustomerId { get; set; }
        [DataMember]
        public int ClassId { get; set; }
        [DataMember]
        public int BranchId { get; set; }

        [DataMember]
        public List<ExceIBookingBookSchedule> Times { get; set; }
    }
}
