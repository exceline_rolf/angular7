﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.Notification;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingListFreeze
    {
        private int _id = -1;
        private string _systemId = string.Empty;
        private string _gymId = string.Empty;
        private int _memberContractid = -1;
        private int _categoryId = -1;
        private string _fromDate = string.Empty;
        private string _toDate = string.Empty;
        private string _note = string.Empty;
        private int _freezeMonths = -1;
        private bool _isExtended = false;
        private int _freezeDays = -1;
        private int _shiftCount = -1;
        private int _shiftStartInstallmentId = -1;
        private int _activeStatus = -1;
        private string _contractEndDate = string.Empty;

        [DataMember(Order = 1)]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember(Order = 2)]
        public string SystemId
        {
            get { return _systemId; }
            set { _systemId = value; }
        }

        [DataMember(Order = 3)]
        public string GymId
        {
            get { return _gymId; }
            set { _gymId = value; }
        }

        [DataMember(Order = 4)]
        public int MemberContractid
        {
            get { return _memberContractid; }
            set { _memberContractid = value; }
        }


        [DataMember(Order = 5)]
        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }


        [DataMember(Order = 6)]
        public string FromDate
        {
            get { return _fromDate; }
            set { _fromDate = value; }
        }


        [DataMember(Order = 7)]
        public string ToDate
        {
            get { return _toDate; }
            set { _toDate = value; }
        }

        [DataMember(Order = 8)]
        public string Note
        {
            get { return _note; }
            set { _note = value; }
        }

        [DataMember(Order = 9)]
        public int FreezeMonths
        {
            get { return _freezeMonths; }
            set { _freezeMonths = value; }
        }

        [DataMember(Order = 10)]
        public bool IsExtended
        {
            get { return _isExtended; }
            set { _isExtended = value; }
        }

        [DataMember(Order = 11)]
        public int FreezeDays
        {
            get { return _freezeDays; }
            set { _freezeDays = value; }
        }

        [DataMember(Order = 12)]
        public int ShiftCount
        {
            get { return _shiftCount; }
            set { _shiftCount = value; }
        }

        [DataMember(Order = 13)]
        public int ShiftStartInstallmentId
        {
            get { return _shiftStartInstallmentId; }
            set { _shiftStartInstallmentId = value; }
        }

        [DataMember(Order = 14)]
        public int ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        [DataMember(Order = 15)]
        public string ContractEndDate
        {
            get { return _contractEndDate; }
            set { _contractEndDate = value; }
        }

    }
}