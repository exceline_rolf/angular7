﻿using System;

namespace US.GMS.Core.Utils
{
    public static class Common
    {
        public static string GetDate()
        {
            string today = string.Empty;
            string day = Convert.ToString(DateTime.Now.Day);
            string month = Convert.ToString(DateTime.Now.Month);
            string year = Convert.ToString(DateTime.Now.Year);
            if (day.Length == 1)
            {
                day = "0" + day;
            }
            if (month.Length == 1)
            {
                month = "0" + month;
            }
            today = year + month + day;
            return today;
        }
        public static string GetCurrentTime()
        {
            string currentTime = string.Empty;
            string Hour = Convert.ToString(DateTime.Now.Hour);
            string Minute = Convert.ToString(DateTime.Now.Minute);
            string Second = Convert.ToString(DateTime.Now.Second);
            if (Second.Length == 1)
            {
                Second = "0" + Second;
            }
            if (Minute.Length == 1)
            {
                Minute = "0" + Minute;
            }
            if (Hour.Length == 1)
            {
                Hour = "0" + Hour;
            }
            currentTime = Hour + Minute + Second;
            return currentTime;
        }
    }
}
