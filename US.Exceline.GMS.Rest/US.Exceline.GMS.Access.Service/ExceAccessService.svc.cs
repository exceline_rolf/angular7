﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading;
using US.Common.Logging.API;
using US.Common.Notification.API;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Communication.API;
using US.Communication.Core.SystemCore.DomainObjects;
using US.Exceline.GMS.Access.Service.Responses;
using US.Exceline.GMS.IBooking.API;
using US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership;
using US.Exceline.GMS.Modules.Operations.API.Operations;
using US.GMS.API;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.Utils;
using US.Payment.Core.Enums;
using US.Payment.Core.ResultNotifications;
using System.Linq;

namespace US.Exceline.GMS.Access.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ExceAccessService : IExceAccessService
    {
        public CommandResponse GetCommands(string systemId, string gymId, string userName, string password)
        {
            CommandResponse commandResponses;
            try
            {
                if (AuthenticationManager.Authenticate(userName, password))
                {
                    string gymCode = string.Empty;
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;

                    OperationResult<List<ExcACCCommand>> result = GMSOperations.GetCommands(Convert.ToInt32(gymId), gymCode);
                    if (result.ErrorOccured)
                    {
                        foreach (NotificationMessage message in result.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", result.Notifications.FirstOrDefault().Message);
                        commandResponses = new CommandResponse(null, status);
                        return commandResponses;
                    }
                    else
                    {
                        if (result.OperationReturnValue.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            commandResponses = new CommandResponse(result.OperationReturnValue, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Members Found");
                            commandResponses = new CommandResponse(null, status);
                        }
                        return commandResponses;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    commandResponses = new CommandResponse(null, accessstatus);
                    return commandResponses;
                }

            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                commandResponses = new CommandResponse(null, status);
                return commandResponses;
            }
        }

        public SettingResponse GetSettings(string systemId, string gymId, string userName, string password)
        {
            SettingResponse settingResponse;
            try
            {
                if (AuthenticationManager.Authenticate(userName, password))
                {
                    string gymCode = string.Empty;
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;

                    OperationResult<ExceACCSettings> result = GMSOperations.GetSettings(Convert.ToInt32(gymId), gymCode);
                    if (result.ErrorOccured)
                    {
                        foreach (NotificationMessage message in result.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", result.Notifications.FirstOrDefault().Message);
                        settingResponse = new SettingResponse(null, status);
                        return settingResponse;
                    }
                    else
                    {
                        var status = new ResponseStatus("1", "Successfull");
                        settingResponse = new SettingResponse(result.OperationReturnValue, status);
                        return settingResponse;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    settingResponse = new SettingResponse(null, accessstatus);
                    return settingResponse;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                settingResponse = new SettingResponse(null, status);
                return settingResponse;
            }
        }

        public AccessTimesResponse GetAccessTimes(string systemId, string gymId, string userName, string password)
        {
            AccessTimesResponse accessResponse;
            try
            {
                if (AuthenticationManager.Authenticate(userName, password))
                {
                    string gymCode = string.Empty;
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                   
                    OperationResult<List<ExceACCAccessProfileTime>> result = GMSOperations.GetAccessTimes(Convert.ToInt32(gymId), gymCode);
                    if (result.ErrorOccured)
                    {
                        foreach (NotificationMessage message in result.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", result.Notifications.FirstOrDefault().Message);
                        accessResponse = new AccessTimesResponse(null, status);
                        return accessResponse;
                    }
                    else
                    {
                        if (result.OperationReturnValue.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            accessResponse = new AccessTimesResponse(result.OperationReturnValue, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No times Found");
                            accessResponse = new AccessTimesResponse(null, status);
                        }
                        return accessResponse;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    accessResponse = new AccessTimesResponse(null, accessstatus);
                    return accessResponse;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                accessResponse = new AccessTimesResponse(null, status);
                return accessResponse;
            }
        }


       
        public GymOpenTimeResponse GetOpenTimes(string systemId, string gymId, string userName, string password)
        {
            GymOpenTimeResponse accessResponse;
            try
            {
                if (AuthenticationManager.Authenticate(userName, password))
                {

                    string gymCode = string.Empty;
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                   
                    OperationResult<List<GymACCOpenTime>> result = GMSOperations.GetOpenTimes(Convert.ToInt32(gymId), gymCode);
                    if (result.ErrorOccured)
                    {
                        foreach (NotificationMessage message in result.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "System");
                        }
                        var status = new ResponseStatus("3", result.Notifications.FirstOrDefault().Message);
                        accessResponse = new GymOpenTimeResponse(null, status);
                        return accessResponse;
                    }
                    else
                    {
                        if (result.OperationReturnValue.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            accessResponse = new GymOpenTimeResponse(result.OperationReturnValue, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No times Found");
                            accessResponse = new GymOpenTimeResponse(null, status);
                        }
                        return accessResponse;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    accessResponse = new GymOpenTimeResponse(null, accessstatus);
                    return accessResponse;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                accessResponse = new GymOpenTimeResponse(null, status);
                return accessResponse;
            }
        }

        public GymClosedTimesResponse GetClosedTimes(string systemId, string gymId, string userName, string password)
        {
            GymClosedTimesResponse closeResponse;
            try
            {
                if (AuthenticationManager.Authenticate(userName, password))
                {
                    string gymCode = string.Empty;
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;

                    OperationResult<List<ExceACCGymClosedTime>> result = GMSOperations.GetClosedTimes(Convert.ToInt32(gymId), gymCode);
                    if (result.ErrorOccured)
                    {
                        foreach (NotificationMessage message in result.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "System");
                        }
                        var status = new ResponseStatus("3", result.Notifications.FirstOrDefault().Message);
                        closeResponse = new GymClosedTimesResponse(null, status);
                        return closeResponse;
                    }
                    else
                    {
                        if (result.OperationReturnValue.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            closeResponse = new GymClosedTimesResponse(result.OperationReturnValue, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No times Found");
                            closeResponse = new GymClosedTimesResponse(null, status);
                        }
                        return closeResponse;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    closeResponse = new GymClosedTimesResponse(null, accessstatus);
                    return closeResponse;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                closeResponse = new GymClosedTimesResponse(null, status);
                return closeResponse;
            }
        }

        public ResponseMemberList GetMembers(string systemId, string gymId, string userName, string password)
        {
            ResponseMemberList members;
            try
            {
                if (AuthenticationManager.Authenticate(userName, password))
                {

                    string gymCode = string.Empty;
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;

                    OperationResult<List<ExceACCMember>> result = GMSOperations.GetMembers(Convert.ToInt32(gymId), gymCode);
                  

                    if (result.ErrorOccured)
                    {
                        foreach (NotificationMessage message in result.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "System");
                        }
                        var status = new ResponseStatus("3", result.Notifications.FirstOrDefault().Message);
                        members = new ResponseMemberList(null, status);
                        return members;
                    }
                    else
                    {
                        if (result.OperationReturnValue.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            members = new ResponseMemberList(result.OperationReturnValue, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Members Found");
                            members = new ResponseMemberList(null, status);
                        }
                        return members;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    members = new ResponseMemberList(null, accessstatus);
                    return members;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                members = new ResponseMemberList(null, status);
                return members;
            }
        }

        public void SaveTaskStatus(string taskName, string description, int branchId, int status, string gymCode, string userName)
        {
            OperationResult<bool> result = GMSSystem.SaveTaskStatus(taskName, description, branchId, status, gymCode);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), userName);
                }
            }
            
        }

       

        public ResponseMember GetMember(string systemId, string branchId, string cardNo, string accessType,string terminalId, string userName, string password)
        {
            ResponseMember member;
            string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
            try
            {
                if (AuthenticationManager.Authenticate(userName, password))
                {
                    OperationResult<ExceACCMember> result = GMSManageMembership.ValidateMemberWithCardForExceline(cardNo, accessType, Convert.ToInt32(terminalId), gymCode, Convert.ToInt32(branchId), userName);

                    if (result.ErrorOccured)
                    {
                        foreach (NotificationMessage message in result.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "System");
                            SaveTaskStatus("AccessController", message.Message, Convert.ToInt32(branchId), 0, gymCode, userName);
                           // AddEvent(-1, cardNo, message.Message, terminalId, branchId, systemId, userName, password);
                        }
                        var status = new ResponseStatus("3", result.Notifications.FirstOrDefault().Message);
                        member = new ResponseMember(null, status);
                        return member;
                    }
                    else
                    {
                       
                        if (result.OperationReturnValue != null)
                        {
                            if (!result.OperationReturnValue.IsError)
                            {
                                var status = new ResponseStatus("1", "Successfull");
                                member = new ResponseMember(result.OperationReturnValue, status);
                            }
                            else
                            {
                                var status = new ResponseStatus("2", "Not a valid member");
                                member = new ResponseMember(result.OperationReturnValue, status);
                            }

                            if (result.OperationReturnValue.SmsMessages != null && result.OperationReturnValue.SmsMessages.Any() && !string.IsNullOrEmpty(result.OperationReturnValue.Mobile))
                            {
                                var mesList = result.OperationReturnValue.SmsMessages.Select(me => me.Value).ToList();
                                SendSmsOnline(result.OperationReturnValue.Id, branchId, userName, mesList, result.OperationReturnValue.Mobile, gymCode);
                            }

                           // AddEvent(result.OperationReturnValue.Id, cardNo, result.OperationReturnValue.MemberMessages, terminalId, branchId, systemId, userName, password);
                           
                        }
                        else
                        {
                            SaveTaskStatus("AccessController", "No Member Found", Convert.ToInt32(branchId), 0, gymCode, userName);
                            var status = new ResponseStatus("4", "No Member Found");

                         //  AddEvent(-1, cardNo, "Card no cannot be identified", terminalId, branchId, systemId, userName, password);
                            member = new ResponseMember(null, status);
                        }
                        return member;
                    }
                }
                else
                {
                    SaveTaskStatus("AccessController", "Access denied", Convert.ToInt32(branchId), 0, gymCode, userName);
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    member = new ResponseMember(null, accessstatus);

                    //AddEvent(-1, cardNo, "Card no cannot be identified", terminalId, branchId, systemId, userName, password);
                    return member;
                }
            }
            catch (Exception ex)
            {
                SaveTaskStatus("AccessController", ex.Message, Convert.ToInt32(branchId), 0, gymCode, userName);
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                member = new ResponseMember(null, status);
               // AddEvent(-1, cardNo, "Error Executing Command", terminalId, branchId, systemId, userName, password);
                return member;
            }
        }

        public ImageResponse GetImage(string CustomerNo, string systemId, string gymId, string userName, string password)
        {
            ImageResponse image;
            try
            {
                if (AuthenticationManager.Authenticate(userName, password))
                {
                    string gymCode = string.Empty;
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;

                    OperationResult<string> result = GMSOperations.GetImage(Convert.ToInt32(gymId), gymCode, CustomerNo);
                    if (result.ErrorOccured)
                    {
                        foreach (NotificationMessage message in result.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "System");
                        }
                        var status = new ResponseStatus("3", result.Notifications.FirstOrDefault().Message);
                        image = new ImageResponse(null, status);
                        return image;
                    }
                    else
                    {
                        var status = new ResponseStatus("1", "Successfull");
                        image = new ImageResponse(result.OperationReturnValue, status);
                        return image;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    image = new ImageResponse(null, accessstatus);
                    return image;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                image = new ImageResponse(null, status);
                return image;
            }
        }

        public AddVisitResponse AddVisit(EntityVisitDetails visit, string systemId, string gymId, string controllerNo, string userName, string password)
        {
            AddVisitResponse visitResponse;
            try
            {
                if (visit != null)
                {
                    EntityVisitDC entityVist = new EntityVisitDC();
                    entityVist.ActivityId = Convert.ToInt32(visit.ActivityId);
                    entityVist.ArticleID = Convert.ToInt32(visit.ArticleID);
                    entityVist.BranchId = Convert.ToInt32(visit.BranchId);
                    entityVist.ContractNo = visit.ContractNo;
                    entityVist.CountVist = Convert.ToBoolean(visit.CountVist);
                    entityVist.CutomerNo = visit.CutomerNo;
                    entityVist.EntityId = Convert.ToInt32(visit.EntityId);
                    entityVist.EntityName = visit.EntityName;
                    entityVist.GymName = visit.GymName;
                    entityVist.Id = Convert.ToInt32(visit.Id);
                    entityVist.InTime = Convert.ToDateTime(visit.InTime);
                    entityVist.InvoiceRerefence = Convert.ToInt32(visit.InvoiceRerefence);
                    entityVist.ItemName = visit.ItemName;
                    entityVist.MemberContractId = Convert.ToInt32(visit.MemberContractId);
                    entityVist.Mobile = visit.Mobile;
                    entityVist.VisitDate = Convert.ToDateTime(visit.VisitDate);
                    entityVist.VisitType = visit.VisitType;
                        entityVist.Messages = visit.Messages;
                      //  entityVist.CardNo = Convert.ToInt32(visit.CardNo);
                        entityVist.IsVisit = visit.IsVisit;
                        entityVist.IsExcAccess = true;
                        entityVist.TerminalId = Convert.ToInt32(controllerNo);

                    if (AuthenticationManager.Authenticate(userName, password))
                    {
                         string gymCode = string.Empty;
                         gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                         OperationResult<int> result = GMSManageMembership.SaveMemberVisit(entityVist, gymCode, userName);
                        if (result.ErrorOccured)
                        {
                            foreach (NotificationMessage message in result.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), "System");
                            }
                            var status = new ResponseStatus("3", result.Notifications.FirstOrDefault().Message);
                            visitResponse = new AddVisitResponse(false, status);
                            return visitResponse;
                        }
                        else
                        {
                            //if (result.OperationReturnValue == -2)
                            //{
                            //    bool isSendSms = false;
                            //    var settings = new List<string> { "AccIsSendSms" };
                            //    OperationResult<Dictionary<string, object>> gymSettingDictionary = GMSManageGymSetting.GetSelectedGymSettings(settings, true, Convert.ToInt32(gymId), gymCode);

                            //    if (gymSettingDictionary != null && gymSettingDictionary.OperationReturnValue != null)
                            //        isSendSms = Convert.ToBoolean(gymSettingDictionary.OperationReturnValue.FirstOrDefault(x => x.Key.Equals("AccIsSendSms")).Value);

                            OperationResult<int> avalableVisit = GMSOperations.GetAvailableVisitByContractId(entityVist.MemberContractId, gymCode);
                            if (visit.IsSendSms && !string.IsNullOrEmpty(visit.Mobile) && avalableVisit != null && visit.MinimumPunches != 0 && avalableVisit.OperationReturnValue < visit.MinimumPunches)
                            {
                                string message = "You have only " + avalableVisit.OperationReturnValue + " Visits Remain";
                                if (Thread.CurrentThread.CurrentCulture.Name == "nb-NO" || Thread.CurrentThread.CurrentCulture.Name == "nn-NO")
                                    message = "BDu har " + avalableVisit.OperationReturnValue + " Besøk igjen";

                                SendSMSFromMemberCard(Convert.ToInt32(visit.EntityId), entityVist.BranchId, userName, message, entityVist.Mobile, gymCode);
                            }
                            //    var status = new ResponseStatus("2", "Recently Visit");
                            //    visitResponse = new AddVisitResponse(false, status);
                            //    return visitResponse;
                            //}
                            //else
                            //{
                                var status = new ResponseStatus("1", "Successfull");
                                visitResponse = new AddVisitResponse(true, status);
                                return visitResponse;
                           // }
                        }
                    }
                    else
                    {
                        var accessstatus = new ResponseStatus("6", "Access denied ");
                        visitResponse = new AddVisitResponse(false, accessstatus);
                        return visitResponse;
                    }
                }
                else
                {
                    var status = new ResponseStatus("3", "No Visit found");
                    visitResponse = new AddVisitResponse(false, status);
                    return visitResponse;

                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                visitResponse = new AddVisitResponse(false, status);
                return visitResponse;
            }
        }


        public ARXMemberResponse GetARXMembers(string systemId, string gymId, string isallmember, string userName, string password)
        {
            ARXMemberResponse image;
            try
            {
                if (AuthenticationManager.Authenticate(userName, password))
                {
                    string gymCode = string.Empty;
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;

                    OperationResult<string> result = GMSOperations.GetARXMembers(Convert.ToInt32(gymId), gymCode, isallmember == "1");
                    if (result.ErrorOccured)
                    {
                        foreach (NotificationMessage message in result.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "System");
                        }
                        var status = new ResponseStatus("3", result.Notifications.FirstOrDefault().Message);
                        image = new ARXMemberResponse(null, status);
                        return image;
                    }
                    else
                    {
                        var status = new ResponseStatus("1", "Successfull");
                        image = new ARXMemberResponse(result.OperationReturnValue, status);
                        return image;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    image = new ARXMemberResponse(null, accessstatus);
                    return image;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                image = new ARXMemberResponse(null, status);
                return image;
            }
        }

        public SystemIdResponse GetSystemID(string key, string userName, string password)
        {
            SystemIdResponse response;
            try
            {
                if (AuthenticationManager.Authenticate(userName, password))
                {
                    string gymCode = GetGymCodefromKey(key);

                    OperationResult<int> result = GMSOperations.GetSystemId(gymCode);
                    var status = new ResponseStatus("1", "Successfull");
                    response = new SystemIdResponse(result.OperationReturnValue, status);
                    return response;
                }
                else
                {
                    var status = new ResponseStatus("6", "Access denied ");
                    response = new SystemIdResponse(-1, status);
                    return response;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                response = new SystemIdResponse(-1, status);
                return response;
            }
        }

        private string GetGymCodefromKey(string key)
        {
            List<char> characters = key.ToList<char>();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (char character in characters)
            {
                if (character != '@')
                {
                    stringBuilder.Append(character);
                }
            }

            return stringBuilder.ToString();
        }

        public RegisterControlResponse RegisterControl(ExcACCAccessControl accessControl, string systemId, string userName, string password)
        {
            RegisterControlResponse response;
            try
            {
                if (AuthenticationManager.Authenticate(userName, password))
                {
                    string gymCode = string.Empty;
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;

                    OperationResult<int> result = GMSOperations.RegisterControl(accessControl, gymCode);
                    var status = new ResponseStatus("1", "Successfull");
                    response = new RegisterControlResponse(result.OperationReturnValue, status);
                    return response;
                }
                else
                {
                    var status = new ResponseStatus("6", "Access denied ");
                    response = new RegisterControlResponse(-1, status);
                    return response;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                response = new RegisterControlResponse(-1, status);
                return response;
            }
        }


        public PingServerResponse PingServer(string terminalId, string systemId, string userName, string password)
        {
            PingServerResponse response;
            try
            {
                if (AuthenticationManager.Authenticate(userName, password))
                {
                    string gymCode = string.Empty;
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<int> result = GMSOperations.PingServer(Convert.ToInt32(terminalId), gymCode);

                    var status = new ResponseStatus("1", "Successfull");
                    response = new PingServerResponse(result.OperationReturnValue, status);
                    return response;
                }
                else
                {
                    var status = new ResponseStatus("6", "Access denied ");
                    response = new PingServerResponse(-1, status);
                    return response;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                response = new PingServerResponse(-1, status);
                return response;
            }
        }

        private void AddEvent(int memberId,string cardNo,string message,string terminalId, string branchId, string systemId, string userName, string password)
        {
            var accessEvent = new ExcAccessEvent();
            accessEvent.id = -1;
            accessEvent.TerminalId = Convert.ToInt32(terminalId);
            accessEvent.BranchId = Convert.ToInt32(branchId);
            accessEvent.MemberId = memberId;
            accessEvent.CardNo = cardNo;
            accessEvent.Messages = message;
            AddEventLogs(accessEvent, systemId, userName, password);
        }

        public void AddEventLogs(ExcAccessEvent accessEvent, string systemId, string userName, string password)
        {
            if (AuthenticationManager.Authenticate(userName, password))
            {
                string gymCode = string.Empty;
                gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;

                OperationResult<int> result = GMSOperations.AddEventLogs(accessEvent, gymCode);
                if (result.ErrorOccured)
                {
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), userName);
                    }
                }

                //NotifiedEventChange("QA", "6", "1");
                NotifiedEventChange(gymCode.ToUpper(), accessEvent.BranchId.ToString(), accessEvent.TerminalId.ToString());
            }
        }

        private void NotifiedEventChange(string gymCode, string branchId, string terminalId)
        {
            try
            {
                var baseUrl = ConfigurationManager.AppSettings["AccessControllLiveCopyWebAPIHost"];
                var uri = baseUrl + "/api/values/" + gymCode + "," + branchId + "," + terminalId;

                using (var wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/from-data";
                    wc.UploadString(uri, "POST", "");
                }
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), "ACLS");
            }
        }

        public SendSmsResponse SendSms(List<string> mesList,string memberId,string mobileNo,string branchId, string systemId, string userName, string password)
        {

            SendSmsResponse response;
            try
            {
                if (AuthenticationManager.Authenticate(userName, password))
                {
                    string gymCode = string.Empty;
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;

                    foreach (var message in mesList)
                    {
                        SendSMSFromMemberCard(Convert.ToInt32(memberId), Convert.ToInt32(branchId), userName, message, mobileNo, gymCode);
                    }
                   
                    var status = new ResponseStatus("1", "Successfull");
                    response = new SendSmsResponse(true, status);
                    return response;
                }
                else
                {
                    var status = new ResponseStatus("6", "Access denied ");
                    response = new SendSmsResponse(false, status);
                    return response;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                response = new SendSmsResponse(false, status);
                return response;
            }


            //SendSmsResponse response;
            //var status = new ResponseStatus("1", "Successfull");
            //response = new SendSmsResponse(true, status);
            //return response;
            //if (AuthenticationManager.Authenticate(userName, password))
            //{
            //    string gymCode = string.Empty;
            //    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;

            //    OperationResult<int> result = GMSOperations.AddEventLogs(accessEvent, gymCode);
            //    if (result.ErrorOccured)
            //    {
            //        foreach (NotificationMessage message in result.Notifications)
            //        {
            //            USLogError.WriteToFile(message.Message, new Exception(), userName);
            //        }
            //    }
            //}
        }

        public void SendSmsOnline(int memberId, string branchId, string userName, List<string> messageList, string mobileNo,string gymCode)
        {
            foreach (var message in messageList)
            {
                SendSMSFromMemberCard(memberId, Convert.ToInt32(branchId), userName, message, mobileNo, gymCode);
            }
           
        }

        public bool SendSMSFromMemberCard(int memberID, int branchId, string user, string message, string mobileNo,string gymCode)
        {
            // create common notification
            USCommonNotificationDC commonNotification = new USCommonNotificationDC();
            commonNotification.IsTemplateNotification = false;
            commonNotification.Description = message;
            commonNotification.BranchID = branchId;
            string createUser = gymCode + "/" + user;
            commonNotification.CreatedUser = createUser;
            commonNotification.Role = "MEM";
            commonNotification.NotificationType = NotificationTypeEnum.Messages;
            commonNotification.Severity = NotificationSeverityEnum.Minor;
            commonNotification.MemberID = memberID;
            commonNotification.Status = NotificationStatusEnum.New;
            commonNotification.Method = NotificationMethodType.SMS;
            commonNotification.Title = "ACCESS CONTROL SMS";
            commonNotification.SenderDescription = mobileNo;
            

            USImportResult<int> result = NotificationAPI.AddNotification(commonNotification);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage mgs in result.NotificationMessages)
                {
                    if (mgs.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(mgs.Message, new Exception(), user);
                    }
                }
                return false;
            }
            else
            {
                string application = ConfigurationManager.AppSettings["USC_Notification_SMS"].ToString();
                string outputFormat = "SMS";

                try
                {
                    // send SMS through the USC API
                    IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, gymCode, result.MethodReturnValue.ToString());

                    NotificationStatusEnum status = NotificationStatusEnum.Attended;

                    if (smsStatus != null)
                        status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                    var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                    // update status in notification table
                    USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, createUser, status, statusMessage);

                    if (updateResult.ErrorOccured)
                    {
                        foreach (USNotificationMessage mgs in updateResult.NotificationMessages)
                        {
                            if (mgs.ErrorLevel == ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(mgs.Message, new Exception(), user);
                            }
                        }
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                catch (Exception e)
                {
                    USLogError.WriteToFile(e.Message, new Exception(), user);
                    return false;
                }
            }
        }


        //public AccessTerminalResponse GetTerminalDetails(string systemId, int terminalID, string userName, string password)
        //{
        //    AccessTerminalResponse terminalResponse;
        //    try
        //    {

        //        if (AuthenticationManager.Authenticate(userName, password))
        //        {
        //            string gymCode = string.Empty;
        //            gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;

        //            OperationResult<ExceACCTerminal> result = GMSOperations.GetTerminalDetails(terminalID, gymCode);
        //            if (result.ErrorOccured)
        //            {
        //                foreach (NotificationMessage message in result.Notifications)
        //                {
        //                    USLogError.WriteToFile(message.Message, new Exception(), userName);
        //                }

        //                var status = new ResponseStatus("3", result.Notifications.FirstOrDefault().Message);
        //                terminalResponse = new AccessTerminalResponse(null, status);
        //                return terminalResponse;
        //            }
        //            else
        //            {
        //                var status = new ResponseStatus("1", "Successfull");
        //                terminalResponse = new AccessTerminalResponse(result.OperationReturnValue, status);
        //                return terminalResponse;
        //            }
        //        }
        //        else
        //        {
        //            var status = new ResponseStatus("6", "Access denied ");
        //            terminalResponse = new AccessTerminalResponse(null, status);
        //            return terminalResponse;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
        //        terminalResponse = new AccessTerminalResponse(null, status);
        //        return terminalResponse;
        //    }
        //}

        public AccessTerminalResponse GetTerminalDetails(string systemId, string terminalId, string userName, string password)
        {
            AccessTerminalResponse accessResponse;
            try
            {
                if (AuthenticationManager.Authenticate(userName, password))
                {
                    string gymCode = string.Empty;
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;

                    OperationResult<ExceACCTerminal> result = GMSOperations.GetTerminalDetails(Convert.ToInt32(terminalId), gymCode);
                    if (result.ErrorOccured)
                    {
                        foreach (NotificationMessage message in result.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", result.Notifications.FirstOrDefault().Message);
                        accessResponse = new AccessTerminalResponse(null, status);
                        return accessResponse;
                    }
                    else
                    {
                        if (result.OperationReturnValue != null)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            accessResponse = new AccessTerminalResponse(result.OperationReturnValue, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No times Found");
                            accessResponse = new AccessTerminalResponse(null, status);
                        }
                        return accessResponse;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    accessResponse = new AccessTerminalResponse(null, accessstatus);
                    return accessResponse;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                accessResponse = new AccessTerminalResponse(null, status);
                return accessResponse;
            }
        }

    }
}
