﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace US.Exceline.GMS.Access.Service.Responses
{
    [DataContract]
    public class RegisterControlResponse
    {
         public RegisterControlResponse() { }
         public RegisterControlResponse(int controlId, ResponseStatus status)
        {
            ControlId = controlId;
            Status = status;
        }

        [DataMember(Order=1)]
        public int ControlId { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}