﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl;

namespace US.Exceline.GMS.Access.Service
{
    [DataContract]
    public class CommandResponse
    {
        public CommandResponse() { }
        public CommandResponse(List<ExcACCCommand> commands, ResponseStatus status)
        {
            Commands = commands;
            Status = status;
        }

        [DataMember(Order=1)]
        public List<ExcACCCommand> Commands { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}
