﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using US.GMS.Core.DomainObjects.AccessControl;

namespace US.Exceline.GMS.Access.Service.Responses
{
     [DataContract]
    public class ResponseMember
    {
        public ResponseMember() { }
        public ResponseMember(ExceACCMember member, ResponseStatus status)
        {
            Member = member;
            Status = status;
        }

        [DataMember(Order = 1)]
        public ExceACCMember Member { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}