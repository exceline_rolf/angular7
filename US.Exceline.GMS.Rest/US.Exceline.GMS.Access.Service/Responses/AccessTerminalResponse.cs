﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using US.GMS.Core.DomainObjects.AccessControl;

namespace US.Exceline.GMS.Access.Service.Responses
{
    public class AccessTerminalResponse
    {
        public AccessTerminalResponse() { }
        public AccessTerminalResponse(ExceACCTerminal accessTerminal, ResponseStatus status)
        {
            AccessTerminal = accessTerminal;
            Status = status;
        }

        [DataMember(Order=1)]
        public ExceACCTerminal AccessTerminal { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}