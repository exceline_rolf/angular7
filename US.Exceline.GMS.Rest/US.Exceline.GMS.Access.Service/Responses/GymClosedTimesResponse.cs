﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl;

namespace US.Exceline.GMS.Access.Service
{
    [DataContract]
    public class GymClosedTimesResponse
    {
        public GymClosedTimesResponse() { }
        public GymClosedTimesResponse(List<ExceACCGymClosedTime> closedTimes, ResponseStatus status)
        {
            ClosedTimes = closedTimes;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceACCGymClosedTime> ClosedTimes { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}
