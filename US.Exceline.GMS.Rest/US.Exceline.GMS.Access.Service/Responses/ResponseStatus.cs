﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Exceline.GMS.Access.Service
{
    [DataContract]
    public class ResponseStatus
    {
        public ResponseStatus() { }

        public ResponseStatus(string statusCode, string statusMessage)
        {
            StatusCode = statusCode;
            StatusMessage = statusMessage;
        }

        [DataMember(Order = 1)]
        public string StatusCode { get; set; }
        [DataMember(Order = 1)]
        public string StatusMessage { get; set; }
    }
}
