﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl;

namespace US.Exceline.GMS.Access.Service
{
    [DataContract]
    public class GymOpenTimeResponse
    {
        public GymOpenTimeResponse() { }
        public GymOpenTimeResponse(List<GymACCOpenTime> openTimes, ResponseStatus status)
        {
            OpenTimes = openTimes;
            Status = status;
        }

        [DataMember(Order=1)]
        public List<GymACCOpenTime> OpenTimes { get; set; }
        [DataMember(Order=2)]
        public ResponseStatus Status { get; set; }
    }
}
