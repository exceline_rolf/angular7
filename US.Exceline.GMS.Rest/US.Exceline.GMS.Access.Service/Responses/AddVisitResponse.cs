﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace US.Exceline.GMS.Access.Service.Responses
{
    [DataContract]
    public class AddVisitResponse
    {
        public AddVisitResponse() { }
        public AddVisitResponse(bool result, ResponseStatus status)
        {
            Result = result;
            Status = status;
        }

        [DataMember(Order = 1)]
        public bool Result { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}