﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace US.Exceline.GMS.Access.Service.Responses
{
    [DataContract]
    public class ARXMemberResponse
    {
        public ARXMemberResponse() { }
        public ARXMemberResponse(string members, ResponseStatus status)
        {
            Members = members;
            Status = status;
        }

        [DataMember(Order = 1)]
        public string Members { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}