﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using US.Exceline.GMS.Access.Service.Responses;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.Exceline.GMS.Access.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IExceAccessService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Commands/{systemId}/{gymId}/{userName}/{password}")]
        CommandResponse GetCommands(string systemId, string gymId, string userName, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Settings/{systemId}/{gymId}/{userName}/{password}")]
        SettingResponse GetSettings(string systemId, string gymId, string userName, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Terminal/{systemId}/{terminalId}/{userName}/{password}")]
        AccessTerminalResponse GetTerminalDetails(string systemId, string terminalId, string userName, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "AccessTimes/{systemId}/{gymId}/{userName}/{password}")]
        AccessTimesResponse GetAccessTimes(string systemId, string gymId, string userName, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "OpenTimes/{systemId}/{gymId}/{userName}/{password}")]
        GymOpenTimeResponse GetOpenTimes(string systemId, string gymId, string userName, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "ClosedTimes/{systemId}/{gymId}/{userName}/{password}")]
        GymClosedTimesResponse GetClosedTimes(string systemId, string gymId, string userName, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Members/{systemId}/{gymId}/{userName}/{password}")]
        ResponseMemberList GetMembers(string systemId, string gymId, string userName, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Member/{systemId}/{branchId}/{cardNo}/{accessType}/{terminalId}/{userName}/{password}")]
        ResponseMember GetMember(string systemId, string branchId, string cardNo, string accessType, string terminalId, string userName, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Members/ARX/{systemId}/{gymId}/{isallmember}/{userName}/{password}")]
        ARXMemberResponse GetARXMembers(string systemId, string gymId, string isallmember, string userName, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Image/{CustomerNo}/{systemId}/{gymId}/{userName}/{password}")]
        ImageResponse GetImage(string CustomerNo, string systemId, string gymId, string userName, string password);

        //[OperationContract]
        //[WebInvoke(Method = "POST", RequestFormat= WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "AddVisit/{systemId}/{gymId}/{userName}/{password}")]
        //AddVisitResponse AddVisit(EntityVisitDetails vist, string systemId, string gymId, string userName, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "SystemId/{key}/{userName}/{password}")]
        SystemIdResponse GetSystemID(string key,string userName, string password);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "RegisterControl/{systemId}/{userName}/{password}")]
        RegisterControlResponse RegisterControl(ExcACCAccessControl accessControl, string systemId, string userName, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "PingServer/{terminalId}/{systemId}/{userName}/{password}")]
        PingServerResponse PingServer(string terminalId, string systemId, string userName, string password);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "AddEvent/{systemId}/{userName}/{password}")]
        void AddEventLogs(ExcAccessEvent accessEvent, string systemId, string userName, string password);

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "AddVisit/{systemId}/{gymId}/{controllerNo}/{userName}/{password}")]
        AddVisitResponse AddVisit(EntityVisitDetails vist, string systemId, string gymId, string controllerNo, string userName, string password);

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SendSms/{memberId}/{mobileNo}/{branchId}/{systemId}/{userName}/{password}")]
        SendSmsResponse SendSms(List<string> mesList, string memberId, string mobileNo, string branchId, string systemId, string userName, string password);

    }
}
