﻿namespace US_DataAccess
{
    public enum EnumDatabase
    {
        USP,
        LOGDB,
        CPM,
        Report,
        Search,
        Workflow,
        Exceline,
        WorkStation,
        Default,
        Procasso
    }
}
