﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US_DataAccess.Utill
{
    public class ConnectionStringProvider
    {
        public static string GetConStringFromDb(string companyCode)
        {
            GetConStringFromDbAction action = new GetConStringFromDbAction(companyCode);
            return action.Execute(EnumDatabase.WorkStation);
        }
    }
}
