// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.SendEmailOrSMSAction.#_emailOrSMS")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.SendEmailOrSMSAction.#_body")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.SendEmailOrSMSAction.#_subject")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.CheckReportIsExistInDBAction.#_coreSessionUserName")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "isExist", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.CheckReportIsExistInDBAction.#Body(System.Data.Common.DbConnection)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "aa", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.SaveReportAction.#Body(System.Data.Common.DbConnection)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "reportID", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.SaveReportAction.#Body(System.Data.Common.DbConnection)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.SaveReportScheduleAction.#_userName")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.SaveReportScheduleAction.#_reportQuery")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.GetScheduleListByReportIdAction.#_reportId")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "scheduleDetail", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.GetScheduleListByReportIdAction.#Body(System.Data.Common.DbConnection)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "latestPropertyList", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.ViewAllReportsAction.#UpdateEntityProperty(US.Payment.Reporting.Core.DomainObjects.ReportEntity,System.Collections.Generic.List`1<US.Payment.Reporting.Core.DomainObjects.ReportEntity>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "List12", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.ViewAllReportsAction.#UpdateEntityProperty(US.Payment.Reporting.Core.DomainObjects.ReportEntity,System.Collections.Generic.List`1<US.Payment.Reporting.Core.DomainObjects.ReportEntity>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "l1", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.ViewAllReportsAction.#UpdateEntityProperty(US.Payment.Reporting.Core.DomainObjects.ReportEntity,System.Collections.Generic.List`1<US.Payment.Reporting.Core.DomainObjects.ReportEntity>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "newlyAddedColumns", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.ViewAllReportsAction.#UpdateEntityProperty(US.Payment.Reporting.Core.DomainObjects.ReportEntity,System.Collections.Generic.List`1<US.Payment.Reporting.Core.DomainObjects.ReportEntity>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "deletedColumns", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.ViewAllReportsAction.#UpdateEntityProperty(US.Payment.Reporting.Core.DomainObjects.ReportEntity,System.Collections.Generic.List`1<US.Payment.Reporting.Core.DomainObjects.ReportEntity>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Payment.Reporting.Data.Commands.SaveChangePropertyDisplayNameAction.#_userName")]
