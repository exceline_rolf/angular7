﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Logging.API;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    class ExecuteReportSchedule_MoveToHistoryTaskAction : USDBActionBase<bool>
    {
        private string _gymCode = string.Empty;
        public ExecuteReportSchedule_MoveToHistoryTaskAction(string gymCode)
        {
            _gymCode = gymCode;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USRPT.MoveScheduleDataToHistory";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report :" + _gymCode + " : Exception Occour while ExecuteReportSchedule_MoveToHistoryTaskAction " + ex.Message, ex, "");
                throw;
            }
        }
    }
}
