﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Logging.API;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    class GetReportSettingsAction : USDBActionBase<List<ReportSetting>>
    {
        List<ReportSetting> _settingList = new List<ReportSetting>();
        protected override List<ReportSetting> Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USRPT.GetReportSettings";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ReportSetting setting = new ReportSetting();
                    setting.Key = Convert.ToString(reader["Key"]);
                    setting.KeyValue = Convert.ToString(reader["Value"]);
                    _settingList.Add(setting);
                }
            }
            catch
            {
                throw;
            }
            return _settingList;
        }
    }
}
