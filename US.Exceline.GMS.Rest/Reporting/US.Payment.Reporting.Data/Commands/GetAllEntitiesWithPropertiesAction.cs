﻿using System;
using System.Collections.Generic;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.Common.Logging.API;
using System.Configuration;

namespace US.Payment.Reporting.Data.Commands
{
    public class GetAllEntitiesWithPropertiesAction : USDBActionBase<List<ReportEntity>>
    {
        List<ReportEntity> entityList = new List<ReportEntity>();
        private string _coreSessionUserName = string.Empty;

        public GetAllEntitiesWithPropertiesAction(string CoreSessionUserName)
        {
            _coreSessionUserName = CoreSessionUserName;
        }

        protected override List<ReportEntity> Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USRPT.GetAllEntities";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ReportEntity reportEntity = new ReportEntity();
                    reportEntity.Id = Convert.ToString(reader["ID"]);
                    reportEntity.DisplayName = Convert.ToString(reader["DisplayName"]);
                    reportEntity.TableOrViewName = Convert.ToString(reader["DataTableOrView"]);
                    reportEntity.PrimaryColumn = Convert.ToString(reader["PrimaryColumn"]);
                    reportEntity.BasedOn = Convert.ToString(reader["BasedOn"]);
                    List<ReportingEntityProperty> entityPropertyList = new List<ReportingEntityProperty>();
                    entityPropertyList = ReportingFacade.GetEntityPropertyInfo(Convert.ToInt32(reader["ID"]), reportEntity,_coreSessionUserName);
                    reportEntity.Properties = entityPropertyList;

                    entityList.Add(reportEntity);
                }

            }
            catch (Exception e)
            {
                USLogError.WriteToFile("Report : Exception Occour while GetAllEntitiesWithPropertiesAction " + e.Message, e, _coreSessionUserName);
                throw;
            }
            return entityList;
        }
    }
}
