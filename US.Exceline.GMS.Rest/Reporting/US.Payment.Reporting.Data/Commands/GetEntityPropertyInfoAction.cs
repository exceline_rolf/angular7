﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.Payment.Reporting.Core.DomainObjects;
using US.Common.Logging.API;
using System.Configuration;

namespace US.Payment.Reporting.Data.Commands
{
    class GetEntityPropertyInfoAction : USDBActionBase<List<ReportingEntityProperty>>
    {
        string _userName = ConfigurationManager.AppSettings["userName"];
        /// <summary>
        /// get all properties related to a given Entity
        /// </summary>
        private List<ReportingEntityProperty> _resultAllReportEntitiesProperties = new List<ReportingEntityProperty>();
        private int _filterEntityId = -1;
        //private ReportEntity _rEntity = new ReportEntity();

        public GetEntityPropertyInfoAction(int filterEntityId, ReportEntity reportEntity)
        {
            _filterEntityId = filterEntityId;
            //_rEntity = reportEntity;
        
        }
        protected override List<ReportingEntityProperty> Body(DbConnection connection)
        {
            try
            {

                string StoredProcedureName = "USRPT.GetAllReportPropertiesByEntityID";
                DbCommand Cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                Cmd.Parameters.Add(DataAcessUtils.CreateParam("@id", DbType.Int32, _filterEntityId));
                DbDataReader Reader = Cmd.ExecuteReader();
                int counter = 0;
                while (Reader.Read())
                {
                    ReportingEntityProperty _reportEntityPropertyInfo = new ReportingEntityProperty();
                    _reportEntityPropertyInfo.Id = Convert.ToString(Reader["ID"]);
                    _reportEntityPropertyInfo.DisplayName = Convert.ToString(Reader["DisplayName"]);
                    _reportEntityPropertyInfo.DataColumnName = Convert.ToString(Reader["DataColumnName"]);
                    _reportEntityPropertyInfo.DataColumnReturnName = Convert.ToString(Reader["DataColumnReturnName"]);
                    _reportEntityPropertyInfo.DataType = Convert.ToString(Reader["DataType"]);
                    if (_reportEntityPropertyInfo.DataType == "Int" || _reportEntityPropertyInfo.DataType == "Decimal" || _reportEntityPropertyInfo.DataType == "numeric")
                    {
                        _reportEntityPropertyInfo.EnableAggregate = true;
                    }
                    else
                    {
                        _reportEntityPropertyInfo.EnableAggregate = false;
                    }
                    _reportEntityPropertyInfo.ColumnWidth = Convert.ToString(Reader["ColumnWidth"]);
                    //_reportEntityPropertyInfo.REntity = _rEntity;
                    _reportEntityPropertyInfo.OrderNo = counter;
                    _resultAllReportEntitiesProperties.Add(_reportEntityPropertyInfo);
                    counter++;
                }

            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while GetEntityPropertyInfoAction " + ex.Message, ex, _userName);
                throw;
            }
            return _resultAllReportEntitiesProperties;
        }
       
    }
}
