﻿using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.Payment.Reporting.Core.DomainObjects;
using US.Common.Logging.API;
using System.Configuration;

namespace US.Payment.Reporting.Data.Commands
{
     public  class GetOldReportItemAction : USDBActionBase<USReport >
    {
         string _userName = string.Empty;
        private int _oldId = -1;

        public GetOldReportItemAction(int oldId, string CoreSessionUserName)
        {
            _oldId = oldId;
            _userName = CoreSessionUserName;
        }

        protected override USReport Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USRPT.GetOldReportItem";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OldId", DbType.Int32, _oldId));

                DbDataReader reader = cmd.ExecuteReader();
                USReport oldReport = new USReport();
                while (reader.Read())
                {
                    oldReport.Category = Convert.ToString(reader["Category"]);
                    oldReport.Name = Convert.ToString(reader["Name"]);
                }
                return oldReport;
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while GetOldReportItemAction " + ex.Message, ex, _userName);
                throw;
            }
        }
    }
}
