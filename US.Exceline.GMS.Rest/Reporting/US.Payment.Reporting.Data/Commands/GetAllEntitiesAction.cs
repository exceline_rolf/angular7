﻿using System;
using System.Collections.Generic;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;
using System.Data;
using US.Common.Logging.API;

namespace US.Payment.Reporting.Data.Commands
{
    class GetAllEntitiesAction : USDBActionBase<List<ReportEntity>>
    {
        readonly string _userName = string.Empty;
        readonly List<ReportEntity> _entityList = new List<ReportEntity>();

        public GetAllEntitiesAction(string coreSessionUserName)
        {
            _userName = coreSessionUserName;
        }

        protected override List<ReportEntity> Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                const string storedProcedureName = "USRPT.GetAllEntities";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var reportEntity = new ReportEntity
                    {
                        Id = Convert.ToString(reader["ID"]),
                        DisplayName = Convert.ToString(reader["DisplayName"]),
                        TableOrViewName = Convert.ToString(reader["DataTableOrView"]),
                        PrimaryColumn = Convert.ToString(reader["PrimaryColumn"]),
                        BasedOn = Convert.ToString(reader["BasedOn"])
                    };
                    _entityList.Add(reportEntity);
                }

            }
            catch (Exception e)
            {
                USLogError.WriteToFile("Report : Exception Occour while GetAllEntitiesAction " + e.Message, e, _userName);
                throw;
            }
            return _entityList;
        }
    }
}
