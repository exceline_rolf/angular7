﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Logging.API;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    class GetAllRolesAction : USDBActionBase<List<UserRole>>
    {
        private string _userName = string.Empty;
        private List<UserRole> _roleList = new List<UserRole>();

        public GetAllRolesAction(string CoreSessionUserName)
        {
            _userName = CoreSessionUserName;
        }
        protected override List<UserRole> Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USRPT.GetAllUserRoles";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    UserRole role = new UserRole();
                    role.Id = Convert.ToInt32(reader["ID"]);
                    role.RoleName = Convert.ToString(reader["RoleName"]);
                    role.Description = Convert.ToString(reader["Description"]);
                    _roleList.Add(role);
                }
            }
            catch(Exception e)
            {
                USLogError.WriteToFile("Report : Exception Occour while GetAllRolesAction " + e.Message, e, _userName);
                throw;
            }
            return _roleList;
        }
    }
}
