﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Logging.API;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    public class GetPreFunctionAction : USDBActionBase<List<PreFunction>>
    {
        private string _userName = string.Empty;
        private List<PreFunction> _preFunctionList = new List<PreFunction>();

        public GetPreFunctionAction(string CoreSessionUserName)
        {
            _userName = CoreSessionUserName;
        }
        protected override List<PreFunction> Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USRPT.GetAllPreFunction";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    PreFunction function = new PreFunction();
                    function.FunctionName = Convert.ToString(reader["FunctionName"]);
                    function.FunctionDisplayName = Convert.ToString(reader["FunctionDisplayName"]);
                    function.ActingFunction = Convert.ToString(reader["ActingFunction"]);
                    function.FunctionToolTip = Convert.ToString(reader["FunctionToolTip"]);

                    _preFunctionList.Add(function);
                }
            }
            catch (Exception e)
            {
                USLogError.WriteToFile("Report : Exception Occour while GetPreFunctionAction " + e.Message, e, _userName);
                throw;
            }
            return new List<PreFunction>();
        }
    }
}
