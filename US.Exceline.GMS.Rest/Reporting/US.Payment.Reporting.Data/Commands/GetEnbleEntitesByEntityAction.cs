﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Logging.API;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    class GetEnbleEntitesByEntityAction : USDBActionBase<Dictionary<string, bool>>
    {
        private string _userName = string.Empty;
        private string _entityName = string.Empty;

        public GetEnbleEntitesByEntityAction(string entityName, string userName)
        {
            _entityName = entityName;
            _userName = userName;
        }

        protected override Dictionary<string, bool> Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                Dictionary<string, bool> entityEnableStateList = new Dictionary<string, bool>();

                string storedProcedure = "USRPT.GetEnbleEntitesByEntity";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityName", DbType.String, _entityName));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    bool isEnable =false;

                    if (Convert.ToInt32(reader["EnableState"])==-1 || Convert.ToInt32(reader["EnableState"])>0)
                    {
                        isEnable = true;
                    }
                    else
                    {
                        isEnable = false;
                    }

                    entityEnableStateList.Add(Convert.ToString(reader["DisplayName"]), isEnable);
                }
                return entityEnableStateList;
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while GetEnbleEntitesByEntityAction " + ex.Message, ex, _userName);
                throw;
            }
        }
    }
}
