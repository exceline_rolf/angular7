﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    public class GetGymUsageReportSettingAction : USDBActionBase<List<GymUsage>>
    {
        private readonly int _branchId = -1;

        public GetGymUsageReportSettingAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<GymUsage> Body(DbConnection connection)
        {
            var gymUsageSettingList = new List<GymUsage>();
            try
            {
                const string storedProcedureName = "USRPT.GetGymUsageReportSetting";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var gymUsage = new GymUsage
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            FromTime = Convert.ToDateTime(reader["FromTime"]),
                            ToTime = Convert.ToDateTime(reader["ToTime"]),
                            Sunday = Convert.ToInt32(reader["Sunday"]),
                            Monday = Convert.ToInt32(reader["Monday"]),
                            Tuesday = Convert.ToInt32(reader["Tuesday"]),
                            Wednesday = Convert.ToInt32(reader["Wednesday"]),
                            Thursday = Convert.ToInt32(reader["Thursday"]),
                            Friday = Convert.ToInt32(reader["Friday"]),
                            Saturday = Convert.ToInt32(reader["Saturday"])
                        };
                    gymUsageSettingList.Add(gymUsage);
                }
                return gymUsageSettingList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
