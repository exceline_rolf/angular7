﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    public class UpdateGymUsageReportSettingAction : USDBActionBase<bool>
    {
        private List<GymUsage> _settingList;
        private string _userName;
        private int _branchId;

        public UpdateGymUsageReportSettingAction(List<GymUsage> settingList, int branchId, string userName)
        {
            _settingList = settingList;
            _userName = userName;
            _branchId = branchId;
        }

        protected override bool Body(DbConnection connection)
        {
            try
            {
                const string storedProcedureName = "USRPT.UpdateGymUsageReportSetting";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gymUsageSettingList", SqlDbType.Structured, GetDataTable(_settingList)));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@userName", DbType.String, _userName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.ExecuteNonQuery();
                return true;
            }
            catch
            {
                throw;
            }
        }

        private DataTable GetDataTable(List<GymUsage> settingList)
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("FromTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("ToTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("Sunday", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Monday", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Tuesday", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Wednesday", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Thursday", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Friday", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Saturday", typeof(int)));
            dataTable.Columns.Add(new DataColumn("IsDelete", typeof(bool)));

            foreach (var gymUsage in settingList)
            {
                DataRow dataTableRow = dataTable.NewRow();
                dataTableRow["ID"] = gymUsage.Id;
                dataTableRow["FromTime"] = gymUsage.FromTime;
                dataTableRow["ToTime"] = gymUsage.ToTime;
                dataTableRow["Sunday"] = gymUsage.Sunday;
                dataTableRow["Monday"] = gymUsage.Monday;
                dataTableRow["Tuesday"] = gymUsage.Tuesday;
                dataTableRow["Wednesday"] = gymUsage.Wednesday;
                dataTableRow["Thursday"] = gymUsage.Thursday;
                dataTableRow["Friday"] = gymUsage.Friday;
                dataTableRow["Saturday"] = gymUsage.Saturday;
                dataTableRow["IsDelete"] = gymUsage.IsDelete;
                dataTable.Rows.Add(dataTableRow);
            }
            return dataTable;
        }
    }
}
