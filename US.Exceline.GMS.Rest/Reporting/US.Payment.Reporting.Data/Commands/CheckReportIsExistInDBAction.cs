﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    class CheckReportIsExistInDBAction : USDBActionBase<bool>
    {
        private string _reportName = string.Empty;
        private string _path = string.Empty;
        private string _coreSessionUserName = string.Empty;

        public CheckReportIsExistInDBAction(string reportName, string path, string coreSessionUserName)
        {
            _reportName = reportName;
            _path=path;
            _coreSessionUserName = coreSessionUserName;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string storedProcedureName = "USRPT.CheckReportIsExistInDB";
            DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReportName", DbType.String, _reportName));
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@Path", DbType.String, _path));
            DbParameter _isReportExist = new SqlParameter();
            _isReportExist.DbType = DbType.Boolean;
            _isReportExist.Direction = ParameterDirection.Output;
            _isReportExist.ParameterName = "@IsReportExist";
            cmd.Parameters.Add(_isReportExist);

            cmd.ExecuteNonQuery();

            return Convert.ToBoolean(_isReportExist.Value);
        }
    }
}
