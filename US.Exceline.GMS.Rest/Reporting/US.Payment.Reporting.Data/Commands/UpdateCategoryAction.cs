﻿using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Configuration;
using US.Common.Logging.API;

namespace US.Payment.Reporting.Data.Commands
{
    class UpdateCategoryAction: USDBActionBase<bool>
    {
        string _userName = string.Empty;
        private string _oldCategoryName = string.Empty;
        private string _newCategoryName = string.Empty;

        public UpdateCategoryAction(string oldCategoryName, string newCategoryName, string CoreSessionUserName)
        {
            _oldCategoryName = oldCategoryName;
            _newCategoryName = newCategoryName;
            _userName = CoreSessionUserName;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USRPT.UpdateReportCategory";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CurruntCategoryName", DbType.String, _oldCategoryName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NewCategoryName", DbType.String, _newCategoryName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _userName));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while UpdateCategoryAction " + ex.Message, ex, _userName);
                throw;
            }
            return true;
        }
    }
}