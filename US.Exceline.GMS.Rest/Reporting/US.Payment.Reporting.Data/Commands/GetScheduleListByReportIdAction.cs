﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    public class GetScheduleListByReportIdAction : USDBActionBase<List<ReportSchedule>>
    {
        private int _reportId = 0;
        private string _userName = string.Empty;

        private List<ReportSchedule> reportScheduleList = new List<ReportSchedule>();
        private List<KeyValueObject> _recurrenceList = new List<KeyValueObject>();
        private List<KeyValueObject> _recurrenceMonthList = new List<KeyValueObject>();
        private List<KeyValueObject> _recurrenceWeekList = new List<KeyValueObject>();
        private List<KeyValueObject> _recurrenceDayList = new List<KeyValueObject>();
        private List<KeyValueObject> _recurrenceDateList = new List<KeyValueObject>();
        private List<KeyValueObject> _categoryList = new List<KeyValueObject>();
        private List<UserRole> _roleList = new List<UserRole>();

        public GetScheduleListByReportIdAction(int reportId,string userName)
        {
            _reportId = reportId;
            _userName = userName;

            _recurrenceList.Clear();
            _recurrenceList.Add(new KeyValueObject() { Id = 1, DisplayName = "Yearly" });
            _recurrenceList.Add(new KeyValueObject() { Id = 2, DisplayName = "Monthly" });
            _recurrenceList.Add(new KeyValueObject() { Id = 3, DisplayName = "Weekly" });
            _recurrenceList.Add(new KeyValueObject() { Id = 4, DisplayName = "Daily" });

            _recurrenceMonthList.Clear();
            _recurrenceMonthList.Add(new KeyValueObject() { Id = 1, DisplayName = "January" });
            _recurrenceMonthList.Add(new KeyValueObject() { Id = 2, DisplayName = "February" });
            _recurrenceMonthList.Add(new KeyValueObject() { Id = 3, DisplayName = "March" });
            _recurrenceMonthList.Add(new KeyValueObject() { Id = 4, DisplayName = "April" });
            _recurrenceMonthList.Add(new KeyValueObject() { Id = 5, DisplayName = "May" });
            _recurrenceMonthList.Add(new KeyValueObject() { Id = 6, DisplayName = "June" });
            _recurrenceMonthList.Add(new KeyValueObject() { Id = 7, DisplayName = "July" });
            _recurrenceMonthList.Add(new KeyValueObject() { Id = 8, DisplayName = "August" });
            _recurrenceMonthList.Add(new KeyValueObject() { Id = 9, DisplayName = "September" });
            _recurrenceMonthList.Add(new KeyValueObject() { Id = 10, DisplayName = "October" });
            _recurrenceMonthList.Add(new KeyValueObject() { Id = 11, DisplayName = "November" });
            _recurrenceMonthList.Add(new KeyValueObject() { Id = 12, DisplayName = "December" });

            _recurrenceWeekList.Clear();
            _recurrenceWeekList.Add(new KeyValueObject() { Id = 1, DisplayName = "1" });
            _recurrenceWeekList.Add(new KeyValueObject() { Id = 2, DisplayName = "2" });
            _recurrenceWeekList.Add(new KeyValueObject() { Id = 3, DisplayName = "3" });
            _recurrenceWeekList.Add(new KeyValueObject() { Id = 4, DisplayName = "4" });
            _recurrenceWeekList.Add(new KeyValueObject() { Id = 5, DisplayName = "5" });

            _recurrenceDayList.Clear();
            _recurrenceDayList.Add(new KeyValueObject() { Id = 2, DisplayName = "Monday" });
            _recurrenceDayList.Add(new KeyValueObject() { Id = 3, DisplayName = "Tuesday" });
            _recurrenceDayList.Add(new KeyValueObject() { Id = 4, DisplayName = "Wednesday" });
            _recurrenceDayList.Add(new KeyValueObject() { Id = 5, DisplayName = "Thursday" });
            _recurrenceDayList.Add(new KeyValueObject() { Id = 6, DisplayName = "Friday" });
            _recurrenceDayList.Add(new KeyValueObject() { Id = 7, DisplayName = "Saturday" });
            _recurrenceDayList.Add(new KeyValueObject() { Id = 1, DisplayName = "Sunday" });

            _recurrenceDateList.Clear();
            for (var i = 1; i < 32; i++)
            {
                _recurrenceDateList.Add(new KeyValueObject() { Id = i, DisplayName = i.ToString() });
            }

            _categoryList.Clear();
            _categoryList.Add(new KeyValueObject() { Id = 1, DisplayName = "Follow-up" });
            _categoryList.Add(new KeyValueObject() { Id = 2, DisplayName = "SMS" });
            _categoryList.Add(new KeyValueObject() { Id = 3, DisplayName = "Email" });
            _categoryList.Add(new KeyValueObject() { Id = 4, DisplayName = "Print" });

            _roleList = ReportingFacade.GetAllRoles(_userName);
        }
        protected override List<ReportSchedule> Body(System.Data.Common.DbConnection connection)
        {
            string storedProcedure = "USRPT.GetScheduleByReportId";
            DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReportId", DbType.Int32, _reportId));

            DbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ReportSchedule schedule = new ReportSchedule();
                schedule.ScheduleId = Convert.ToInt32(reader["ScheduleId"]);
                schedule.ReportPrimaryEntity = Convert.ToString(reader["ReportPrimaryEntity"]);
                schedule.ScheduleStartDate = Convert.ToDateTime(reader["ScheduleStartDate"]);
                schedule.ScheduleEnddate = Convert.ToDateTime(reader["ScheduleEndDate"]);
                schedule.IsUntillFurtherNotice = Convert.ToBoolean(reader["IsUntillNoticed"]);
                schedule.ScheduleItemCategory = _categoryList.SingleOrDefault(x => (x.DisplayName.ToUpper() == Convert.ToString(reader["TemplateCategory"]).ToUpper()));
                schedule.ScheduleItemTemplate.TemplateId = Convert.ToInt32(reader["TemplateId"]);
                schedule.ScheduleItemTemplate.TemplateName = Convert.ToString(reader["TemplateName"]);
                schedule.ScheduleItemRecipient = Convert.ToString(reader["Recipient"]);
                schedule.ScheduleItemRecipientRole = _roleList.SingleOrDefault(x => x.Id == Convert.ToInt32(reader["RecipientRoleID"]));
                schedule.ScheduleRecurrence = _recurrenceList.SingleOrDefault(x => x.Id == Convert.ToInt32(reader["Recurrence"]));
                schedule.ScheduleMonth = _recurrenceMonthList.SingleOrDefault(x => x.Id == Convert.ToInt32(reader["ScheduleMonth"]));
                schedule.ScheduleWeek = _recurrenceWeekList.SingleOrDefault(x => x.Id == Convert.ToInt32(reader["ScheduleWeek"]));
                schedule.ScheduleDay = _recurrenceDayList.SingleOrDefault(x => x.Id == Convert.ToInt32(reader["ScheduleDay"]));
                schedule.ScheduleDate = _recurrenceDateList.SingleOrDefault(x => x.Id == Convert.ToInt32(reader["ScheduleDate"]));
                schedule.ScheduleTime = Convert.ToDateTime(reader["ScheduleTime"]);

                if (Convert.ToString(reader["TemplateCategory"]) == "Follow-up")
                {
                    schedule.ReportScheduleFollowUPItem.ScheduleItemFollowupNameText = Convert.ToString(reader["FollowupNameText"]);
                    schedule.ReportScheduleFollowUPItem.FollowupStartTime = Convert.ToString(reader["StartTime"]);
                    schedule.ReportScheduleFollowUPItem.FollowupEndTime = Convert.ToString(reader["EndTime"]);
                    schedule.ReportScheduleFollowUPItem.FollowupDescription = Convert.ToString(reader["FollowUPDescription"]);
                }

                reportScheduleList.Add(schedule);
            }
            return reportScheduleList;
        }
    }
}
