﻿using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.Common.Logging.API;
using System.Configuration;

namespace US.Payment.Reporting.Data.Commands
{
    class DeleteReportAction : USDBActionBase<bool>
    {
        private int _reportIdToBeDeleted;
        private string _userName = string.Empty;

        public DeleteReportAction(int reportIdToBeDeleted, string CoreSessionUserName)
        {
            _reportIdToBeDeleted = reportIdToBeDeleted;
            _userName = CoreSessionUserName;
        }
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USRPT.DeleteReport";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _reportIdToBeDeleted));
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while DeleteReportAction " + ex.Message, ex, _userName);
                throw;
            }
        }

    }
}
