﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Logging.API;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    public class GetScheduleTemplateListAction : USDBActionBase<List<ReportScheduleTemplate>>
    {
        private List<ReportScheduleTemplate> returnedTemplateList = new List<ReportScheduleTemplate>();
        private KeyValueObject _category = new KeyValueObject();
        private string _entity = string.Empty;
        private string _userName = string.Empty;

        public GetScheduleTemplateListAction(KeyValueObject category, string entity, string userName)
        {
            _category = category;
            _entity = entity;
            _userName = userName;
        }
        protected override List<ReportScheduleTemplate> Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedure = "USRPT.GetScheduleTemplateList";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateCategory", DbType.String, GetCategoryCode(_category.Id)));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateEntity", DbType.String, _entity));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ReportScheduleTemplate template = new ReportScheduleTemplate();
                    template.TemplateId = Convert.ToInt32(reader["TemplateId"]);
                    template.TemplateName = Convert.ToString(reader["TemplateName"]);
                    template.TemplateText = Convert.ToString(reader["TemplateText"]);
                    returnedTemplateList.Add(template);
                }
                return returnedTemplateList;
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while GetScheduleTemplateListAction " + ex.Message, ex, _userName);
                throw;
            }
        }

        private string GetCategoryCode(int key)
        {
            if (key == 1)
                return "Follow-up";
            if (key == 2)
                return "SMS";
            if (key == 3)
                return "EMAIL";
            if (key == 4)
                return "PRINT";
            return "";
        }
    }
}
