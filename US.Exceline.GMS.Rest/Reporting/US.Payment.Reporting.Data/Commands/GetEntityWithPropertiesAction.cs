﻿using System;
using System.Collections.Generic;
using System.Data;
using US.Common.Logging.API;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    public class GetEntityWithPropertiesAction : USDBActionBase<List<EntityPropertyForGrid>>
    {
        private readonly string _userName = string.Empty;
        private readonly int _entityId = -1;
        private readonly List<EntityPropertyForGrid> _entityPropertyList = new List<EntityPropertyForGrid>();

        public GetEntityWithPropertiesAction(int entityId, string userName)
        {
            _userName = userName;
            _entityId = entityId;
        }

        protected override List<EntityPropertyForGrid> Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                const string storedProcedureName = "USRPT.GetEntityWithProperties";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _userName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityId", DbType.Int32, _entityId));

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var entityProperty = new EntityPropertyForGrid();
                    try
                    {
                        entityProperty.EntityId = Convert.ToInt32(reader["EntityId"]);
                        entityProperty.EntityName = Convert.ToString(reader["EntityName"]);
                        entityProperty.PropertyId = Convert.ToInt32(reader["PropertyId"]);
                        entityProperty.PropertyName = Convert.ToString(reader["PropertyName"]);
                        entityProperty.PropertyReturnName = Convert.ToString(reader["DataColumnReturnName"]);

                        _entityPropertyList.Add(entityProperty);
                    }
                    catch
                    {
                        continue;
                    }
                }
            }
            catch(Exception e)
            {
                USLogError.WriteToFile("Report : Exception Occour while GetEntityWithPropertiesAction " + e.Message, e, _userName);
                throw;
            }
            return _entityPropertyList;
        }
    }
}
