﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Configuration;
using US.Common.Logging.API;

namespace US.Payment.Reporting.Data.Commands
{
    class GetAllReportCategoriesAction : USDBActionBase<List<string>>
    {
        string _userName = string.Empty;
        List<string> entityList = new List<string>();

        public GetAllReportCategoriesAction(string CoreSessionUserName)
        {
            _userName = CoreSessionUserName;
        }
        protected override List<string> Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USRPT.GetAllReportCategories";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string reportEntityInfo = string.Empty;
                    reportEntityInfo = Convert.ToString(reader["Name"]);
                    entityList.Add(reportEntityInfo);
                }

            }
            catch (Exception e)
            {
                USLogError.WriteToFile("Report : Exception Occour while GetAllReportCategoriesAction " + e.Message, e, _userName);
                throw;
            }
            return entityList;
        }
    }
}
