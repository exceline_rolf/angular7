﻿using System.Collections.Generic;
using System.Linq;
using US.Payment.Reporting.Core.DomainObjects;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using US.Payment.Core;
using US.Payment.Reporting.Data;

namespace US.Payment.Reporting.BL
{
    public class BLManager
    {
        private static string pattern = "GETDATE" + Regex.Escape("(") + Regex.Escape(")") + "|" +
                                        "DATEADD" + Regex.Escape("(") + "|" +
                                        "DATEPART" + Regex.Escape("(");

        public static string GeneateSQLQuery(List<JoinQuery> joinQueryList, USReport report, string recordCount, string CoreSessionUserName)
        {
          
            string query = "SELECT DISTINCT ";
            if (!string.IsNullOrEmpty(recordCount)) query = "SELECT DISTINCT TOP " + recordCount.ToUpper() + " ";
            if (report.EntityList.Count == 1)
            {
                foreach (ReportingEntityProperty ep in report.EntityList[0].Properties)
                {
                    if (ep.Grouped || ep.Selected)
                    {
                        query = query + ep.DataColumnName + " AS "+ep.DataColumnReturnName + ",";
                    }
                }
                query = query.Trim(",".ToCharArray());
                query = query + " FROM " +report.EntityList[0].TableOrViewName;
                if (report.WhereConditionList.Count > 0)
                {
                    query = query + " " + GetWhereCondition(report.WhereConditionList, CoreSessionUserName);
                }
              //  query = query + AddBranchIdWithWhereCondition(report.EntityList);
                return query;
            }
            else
            {
                string joinQuery = string.Empty;
                int loopCount = 0;
                foreach (ReportEntity re in report.EntityList)
                {
                    
                    
                    if (loopCount !=0)
                    {
                        ReportEntity nextReportEntity = GetPreviousReportEntity(report.EntityList, loopCount);
                        if (nextReportEntity != null)
                        {
                            string entityJoinQuery = GetJoinQuery(joinQueryList, re, nextReportEntity);
                            joinQuery = joinQuery + " " + entityJoinQuery;
                        }
                    }
                    else
                    {
                        //Nothing to do..
                    }
                    foreach (ReportingEntityProperty ep in re.Properties)
                    {
                        if (ep.Grouped || ep.Selected)
                        {
                            query = query + ep.DataColumnName + " AS "+ep.DataColumnReturnName + ",";
                        }
                    }
                    loopCount++;
                }
                query = query.Trim(",".ToCharArray());
                query = query + " FROM " +report.EntityList[0].TableOrViewName;
                query = query + joinQuery;
                if (report.WhereConditionList!= null && report.WhereConditionList.Count > 0)
                {
                    query = query + " " + GetWhereCondition(report.WhereConditionList, CoreSessionUserName);
                }
               // query = query + AddBranchIdWithWhereCondition(report.EntityList);
            }
            
            return query;
        }

        private static string GetWhereCondition(List<ConditionExpressionItem> whereConditionList, string CoreSessionUserName)
        {
            string whereCondition = "WHERE ";
            string rightOperand = string.Empty;
            //bool addBranchId = true;
            //if (whereConditionList[0].Conjunction)
            //{

            //}
            whereConditionList[0].Conjunction = "";
            foreach (ConditionExpressionItem condition in whereConditionList)
            {

                //if (condition.EntityPropertyForLeftOperand.DataColumnName=="BranchId")
                //{
                //    addBranchId = false;
                //}
                string leftOperand = GetOperand(condition.EntityPropertyForLeftOperand, condition.EntityPropertyForRightOperand, CoreSessionUserName);

                if (condition.AskedFromUser)
                {
                    // rightOperand = "@" + condition.EntityPropertyForRightOperand.UserParameterPromptNane;//condition.EntityPropertyForLeftOperand.DataColumnReturnName;
                    rightOperand = condition.EntityPropertyForRightOperand.UserParameterPromptNane;
                    if (condition.Operator == "Start With")
                    {
                        condition.DisplayingOperator = "Start With";
                        condition.Operator = " LIKE ";
                        //new code
                        // condition.EntityPropertyForRightOperand.ConditionFixValue=condition.EntityPropertyForRightOperand.ConditionFixValue + "%";
                    }
                    if (condition.Operator == "End With")
                    {
                        condition.DisplayingOperator = "End With";
                        condition.Operator = " LIKE ";
                        // condition.EntityPropertyForRightOperand.ConditionFixValue = condition.EntityPropertyForRightOperand.ConditionFixValue + "%";
                    }
                }
                else if (condition.Operator == "IN")
                {
                    rightOperand = GetOperandForInOperator(condition.EntityPropertyForRightOperand, condition.EntityPropertyForLeftOperand);
                }
                else if (condition.Operator == "LIKE")
                {
                    //condition.EntityPropertyForRightOperand.ConditionFixValue = condition.EntityPropertyForRightOperand.ConditionFixValue + "%";
                    rightOperand = "'" + condition.EntityPropertyForRightOperand.ConditionFixValue + "%" + "'";//GetOperandForLikeOperator(condition.EntityPropertyForRightOperand, condition.EntityPropertyForLeftOperand);
                }
                else if (condition.Operator == "Start With")
                {
                    //condition.EntityPropertyForRightOperand.ConditionFixValue = condition.EntityPropertyForRightOperand.ConditionFixValue + "%";
                    rightOperand = "'" + condition.EntityPropertyForRightOperand.ConditionFixValue + "%" + "'";
                    condition.DisplayingOperator = "Start With";
                    condition.Operator = " LIKE ";

                }
                else if (condition.Operator == "End With")
                {
                    // condition.EntityPropertyForRightOperand.ConditionFixValue = condition.EntityPropertyForRightOperand.ConditionFixValue + "%";
                    rightOperand = "'" + condition.EntityPropertyForRightOperand.ConditionFixValue + "%" + "'";
                    condition.DisplayingOperator = "End With";
                    condition.Operator = "LIKE";

                }
                else if (condition.Operator == "IS NULL OR EMPTY")
                {
                    rightOperand = "ISNULL(" + leftOperand.Trim() + ",'') = ''";
                    condition.DisplayingOperator = "IS NULL OR EMPTY";
                }
                else if (condition.Operator == "(Length) =")
                {
                    rightOperand = "LEN(" + leftOperand.Trim() + ") = " + condition.EntityPropertyForRightOperand.ConditionFixValue;
                    condition.DisplayingOperator = "(Length) =";
                }
                else if (condition.Operator == "(YEAR) =")
                {
                    rightOperand = "DATEPART(year," + leftOperand.Trim() + ") = " + condition.EntityPropertyForRightOperand.ConditionFixValue;
                    condition.DisplayingOperator = "(YEAR) =";
                }
                else if (condition.Operator == "(MONTH) =")
                {
                    rightOperand = "DATEPART(month," + leftOperand.Trim() + ") = " + condition.EntityPropertyForRightOperand.ConditionFixValue;
                    condition.DisplayingOperator = "(MONTH) =";
                }
                else if (condition.Operator == "(DAY) =")
                {
                    rightOperand = "DATEPART(day," + leftOperand.Trim() + ") = " + condition.EntityPropertyForRightOperand.ConditionFixValue;
                    condition.DisplayingOperator = "(DAY) =";
                }
                else
                {
                    rightOperand = GetOperand(condition.EntityPropertyForRightOperand, condition.EntityPropertyForLeftOperand, CoreSessionUserName);
                }

                /////////////////////////////////
                if (condition.Operator == "IS NULL OR EMPTY" || condition.Operator == "(Length) =" || condition.Operator == "(YEAR) =" || condition.Operator == "(MONTH) =" || condition.Operator == "(DAY) =")
                {
                    whereCondition = whereCondition +" "+ condition.Conjunction +" "+ rightOperand;
                }
                else
                {
                    whereCondition = whereCondition + " " + condition.Conjunction + " " + condition.LeftBrackets + leftOperand + " " + condition.Operator + " " + rightOperand + condition.RightBrackets + " ";
                }
            }

            return whereCondition;
        }

        private static string GetOperand(ReportingEntityProperty valueEntity, ReportingEntityProperty otherEntity, string CoreSessionUserName)
        {
            ReportingEntityProperty tempProperty = new ReportingEntityProperty();
            bool isHit = false;

            if (!valueEntity.InConditionFixValue)
            {
                return valueEntity.DataColumnName;
            }
            else
            {
                List<ReportingEntityProperty> propertyList = ReportingFacade.GetEntityPropertyInfo(0, null, CoreSessionUserName);

                foreach (var item in propertyList)
                {
                    if (item.DataColumnReturnName == valueEntity.ConditionFixValue)
                    {
                        tempProperty = item;
                        isHit = true;
                        break;
                    }
                }
            }
            if (isHit)
            {
                return tempProperty.DataColumnName;
            }
            else
            {
                switch (otherEntity.DataType)
                {
                    case "String":
                        {
                            return "'" + valueEntity.ConditionFixValue + "'";
                        }
                    case "Int":
                        {
                            return valueEntity.ConditionFixValue;
                        }
                    case "Decimal":
                        {
                            return valueEntity.ConditionFixValue;
                        }
                    case "DateTime":
                        {
                            string input = valueEntity.ConditionFixValue.ToUpper();
                            Match matches = Regex.Match(input, pattern);
                            if (matches.Success)
                            {
                                return "CAST(" + input.Trim() + " AS DATE)";
                            }
                            return "'" + valueEntity.ConditionFixValue + "'";
                        }
                }
                return valueEntity.ConditionFixValue;
            }
        }

        private static string GetOperandForInOperator(ReportingEntityProperty valueEntity, ReportingEntityProperty otherEntity)
        {
            switch (otherEntity.DataType)
            {
                case "String":
                    {
                        string operand = "(";
                        foreach (string item in valueEntity.ConditionFixValue.Split(','))
                        {

                            operand = operand+"'" + item + "',";
                           // return "'" + valueEntity.ConditionFixValue + "'";
                        }
                        return operand.Trim(',')+")";
                    }
                case "Int":
                    {
                        return "(" + valueEntity.ConditionFixValue + ")";
                    }
                case "Decimal":
                    {
                        return "(" + valueEntity.ConditionFixValue + ")";
                    }
                case "DateTime":
                    {
                        string operand = "(";
                        foreach (string item in valueEntity.ConditionFixValue.Split(','))
                        {
                            string input = item.ToUpper();
                            Match matches = Regex.Match(input, pattern);
                            if (matches.Success)
                            {
                                operand = operand.Trim() + " " + item.Trim() + ",";
                            }
                            else
                            {
                                operand = operand + "'" + item + "',";
                            }
                            // return "'" + valueEntity.ConditionFixValue + "'";
                        }
                        return operand.Trim(',') + ")";
                    }
            }
            return "";
        }
        private static string GetJoinQuery(List<JoinQuery> joinQueryList, ReportEntity currentEntity, ReportEntity previousEntity)
        {
            foreach (JoinQuery query in joinQueryList)
            {
                if (previousEntity.DisplayName.Trim() == query.FirstEntity.Trim() && currentEntity.DisplayName.Trim() == query.SencondEntity.Trim())
                {
                    return query.JoinQueryText;
                }
                //else if (nextReportEntity.DisplayName.Trim() == query.FirstEntity.Trim() && firstReportEntity.DisplayName.Trim() == query.SencondEntity.Trim())
                //{
                //    return query.JoinQueryText;
                //}
            }
            return string.Empty;
        }

        private static ReportEntity GetPreviousReportEntity(List<ReportEntity> list, int loopCount)
        {
            if (list.Count > loopCount - 1)
            {
                return list[loopCount - 1];
            }
            else
            {
                return null;
            }
        }

        // The http://schemas.microsoft.com/sqlserver/reporting/2008/01/reportdefinition namespace is forced to be the default namespace.
        static XNamespace df;
        static XNamespace rd;
        static XElement tagReport;

        //query should include the Grouping Columns.....
        public static string GenerateRdl(USReport report, string query, string connectionString)
        {
            // The http://schemas.microsoft.com/sqlserver/reporting/2008/01/reportdefinition namespace is forced to be the default namespace.
            df = "http://schemas.microsoft.com/sqlserver/reporting/2008/01/reportdefinition";
            rd = "http://schemas.microsoft.com/SQLServer/reporting/reportdesigner";
            tagReport = new XElement(df + "Report",
                new XAttribute("xmlns", "http://schemas.microsoft.com/sqlserver/reporting/2008/01/reportdefinition"),
                new XAttribute(XNamespace.Xmlns + "rd", "http://schemas.microsoft.com/SQLServer/reporting/reportdesigner")
            );

            XElement tagBody;
            XElement tagPage;

            TagReportHeader(report, query, out tagBody, out tagPage);


            XElement tagTablix;
            XElement tagTablixBody;
            XElement tagTablixMembersCol;
            XElement tagTablixMembersRow;
            TagReportBody(report, tagBody, out tagTablix, out tagTablixBody, out tagTablixMembersCol, out tagTablixMembersRow);

            //ading Grouping Entity Properties...
            List<string> groupItemList = new List<string>();
            List<string> displayHeaderGroupItemList = new List<string>();
            foreach (ReportEntity reportEntity in report.EntityList)
            {
                if (reportEntity.IsGroupAll)
                {
                    int selectedFieldCount = 0;
                    foreach (ReportingEntityProperty entityProperty in reportEntity.Properties)
                    {
                        if (reportEntity.IsGroupAll && entityProperty.Selected)
                        {
                            selectedFieldCount++;
                        }
                    }
                    if (selectedFieldCount > 1)
                    {
                        ReportingEntityProperty firstSelectedProperty = reportEntity.Properties.Where(a => a.Selected == true).ToList()[0];
                        groupItemList.Add(firstSelectedProperty.DataColumnReturnName);
                        displayHeaderGroupItemList.Add(firstSelectedProperty.DataColumnReturnName);
                    }
                    else
                    {
                        foreach (ReportingEntityProperty entityProperty in reportEntity.Properties)
                        {
                            if (reportEntity.IsGroupAll && entityProperty.Selected)
                            {
                                groupItemList.Add(entityProperty.DataColumnReturnName);
                                displayHeaderGroupItemList.Add(entityProperty.DataColumnReturnName);
                            }
                        }
                    }
                }
            }

            XElement temptagTablixMembers2;
            int textBoxNumber;

            TagReportGroup(tagTablixMembersRow, groupItemList, displayHeaderGroupItemList, out temptagTablixMembers2, out textBoxNumber);



            XElement tagFinalTablixMember = AddElement(temptagTablixMembers2, df, "TablixMember", null);
            XElement tagGroupDetails = new XElement(df + "Group", new XAttribute("Name", "Details"));
            tagFinalTablixMember.Add(tagGroupDetails);
            if (groupItemList.Count > 0)
            {
                XElement tagVisibilityHeader22 = AddElement(tagFinalTablixMember, df, "Visibility", null);
                AddElement(tagVisibilityHeader22, df, "Hidden", "true");
                AddElement(tagVisibilityHeader22, df, "ToggleItem", groupItemList.ElementAt(groupItemList.Count-1));
            }

            AddElement(tagTablix, df, "DataSetName", "DataSet3");
            AddElement(tagTablix, df, "Top", "0.36in");
            AddElement(tagTablix, df, "Height", "0.43in");
            AddElement(tagTablix, df, "Width", "6.63542in");
            AddElement(tagTablix, df, "Style", null);

            XElement tagTablixColumns = AddElement(tagTablixBody, df, "TablixColumns", null);
            XElement tagTablixRows = AddElement(tagTablixBody, df, "TablixRows", null);

            XElement tagTablixRow1 = AddElement(tagTablixRows, df, "TablixRow", null);

            AddElement(tagTablixRow1, df, "Height", "0.22in"); // Header Row
            XElement tagTablixCellsR1 = AddElement(tagTablixRow1, df, "TablixCells", null);

            int noOfSelectedColumns = 0;
            int intLoop = 2;


            TagReportColumnList(report, tagTablixMembersCol, tagTablixColumns, tagTablixCellsR1, ref noOfSelectedColumns, ref intLoop);

            int insideIntLoop = -1;
            for (int i = 0; i < groupItemList.Count; i++)
            {
                XElement tagTablixRowNewlyAdded = AddElement(tagTablixRows, df, "TablixRow", null);
                AddElement(tagTablixRowNewlyAdded, df, "Height", "0.25in"); // Header Row
                XElement tagTablixCellsRNewlyAdded = AddElement(tagTablixRowNewlyAdded, df, "TablixCells", null);
                bool isAddedFirstcell = false;
                for (int j = 0; j < noOfSelectedColumns; j++)
                {
                    if (isAddedFirstcell)
                    {
                        AddElement(tagTablixCellsRNewlyAdded, df, "TablixCell", null);
                        //XElement tagTablixEmptyCellRNewlyAdded = AddElement(tagTablixCellsRNewlyAdded, df, "TablixCell", null);
                        continue;
                    }
                    insideIntLoop = textBoxNumber++;
                    //NewlyAdded


                    XElement tagTablixCellRNewlyAdded = AddElement(tagTablixCellsRNewlyAdded, df, "TablixCell", null);
                    XElement tagCellContentsRNewlyAdded = AddElement(tagTablixCellRNewlyAdded, df, "CellContents", null);

                    XElement tagTextboxRNewlyAdded = new XElement(df + "Textbox", new XAttribute("Name", "textbox" + insideIntLoop));
                    tagCellContentsRNewlyAdded.Add(tagTextboxRNewlyAdded);
                    AddElement(tagTextboxRNewlyAdded, df, "CanGrow", "true");
                    AddElement(tagTextboxRNewlyAdded, df, "KeepTogether", "true");

                    XElement tagParagraphsRNewlyAdded = AddElement(tagTextboxRNewlyAdded, df, "Paragraphs", null);
                    AddElement(tagTextboxRNewlyAdded, rd, "DefaultName", "textbox" + insideIntLoop);



                    XElement tagStyleR1TBNewlyAdded = AddElement(tagTextboxRNewlyAdded, df, "Style", null);
                    AddElement(tagCellContentsRNewlyAdded, df, "ColSpan", noOfSelectedColumns.ToString());
                    //XElement tagColSpanR1TBNewlyAdded = AddElement(tagCellContentsRNewlyAdded, df, "ColSpan", noOfSelectedColumns.ToString());
                    XElement tagBorderR1TBNewlyAdded = AddElement(tagStyleR1TBNewlyAdded, df, "Border", null);
                    AddElement(tagBorderR1TBNewlyAdded, df, "Color", "LightGrey");
                    AddElement(tagBorderR1TBNewlyAdded, df, "Style", "None");
                    AddElement(tagStyleR1TBNewlyAdded, df, "BackgroundColor", "#cbd4df");
                    AddElement(tagStyleR1TBNewlyAdded, df, "PaddingLeft", "2pt");
                    AddElement(tagStyleR1TBNewlyAdded, df, "PaddingRight", "2pt");
                    AddElement(tagStyleR1TBNewlyAdded, df, "PaddingTop", "2pt");
                    AddElement(tagStyleR1TBNewlyAdded, df, "PaddingBottom", "2pt");


                    XElement tagParagraphRNewlyAdded = AddElement(tagParagraphsRNewlyAdded, df, "Paragraph", null);
                    XElement tagTextRunsRNewlyAdded = AddElement(tagParagraphRNewlyAdded, df, "TextRuns", null);

                    XElement tagTextRunRNewlyAdded3 = AddElement(tagTextRunsRNewlyAdded, df, "TextRun", null);
                    AddElement(tagTextRunRNewlyAdded3, df, "Value", GetCountCellValues(report.EntityList, groupItemList.Count, i));
                    XElement tagStyleNewlyAdded3 = AddElement(tagTextRunRNewlyAdded3, df, "Style", null);
                    AddElement(tagStyleNewlyAdded3, df, "FontWeight", "ExtraBold");

                    XElement tagTextRunRNewlyAdded = AddElement(tagTextRunsRNewlyAdded, df, "TextRun", null);
                    AddElement(tagTextRunRNewlyAdded, df, "Value", GetGroupMergedCellValues(report.EntityList, groupItemList.Count, i));
                    AddElement(tagTextRunRNewlyAdded, df, "Style", null);

                    XElement tagTextRunRNewlyAdded2 = AddElement(tagTextRunsRNewlyAdded, df, "TextRun", null);
                    AddElement(tagTextRunRNewlyAdded2, df, "Value", GetGroupMergedSumCellValues(report.EntityList, groupItemList.Count, i));
                    XElement tagStyleNewlyAdded2 = AddElement(tagTextRunRNewlyAdded2, df, "Style", null);
                    AddElement(tagStyleNewlyAdded2, df, "FontWeight", "Bold");

                    AddElement(tagParagraphRNewlyAdded, df, "Style", null);
                    isAddedFirstcell = true;
                }
            }
            ///----------------------------------------------
            ///
            //Add Row list Loop

            XElement tagTablixRow2 = AddElement(tagTablixRows, df, "TablixRow", null);
            AddElement(tagTablixRow2, df, "Height", "0.21in"); // Details Row
            XElement tagTablixCellsR2 = AddElement(tagTablixRow2, df, "TablixCells", null);

            foreach (var entity in report.EntityList)
            {
                foreach (var item in entity.Properties)
                {
                    if (item.Selected && !entity.IsGroupAll )
                    {
                        //Row 2

                        XElement tagTablixCellR2 = AddElement(tagTablixCellsR2, df, "TablixCell", null);
                        XElement tagCellContentsR2 = AddElement(tagTablixCellR2, df, "CellContents", null);
                        XElement tagTextboxR2 = new XElement(df + "Textbox", new XAttribute("Name", item.DataColumnReturnName));
                        tagCellContentsR2.Add(tagTextboxR2);


                        AddElement(tagTextboxR2, df, "CanGrow", "true");
                        AddElement(tagTextboxR2, df, "KeepTogether", "true");

                        XElement tagParagraphsR2 = AddElement(tagTextboxR2, df, "Paragraphs", null);
                        AddElement(tagTextboxR2, rd, "DefaultName", item.DataColumnReturnName);


                        XElement tagParagraphR2 = AddElement(tagParagraphsR2, df, "Paragraph", null);
                        XElement tagTextRunsR2 = AddElement(tagParagraphR2, df, "TextRuns", null);
                        AddElement(tagParagraphR2, df, "Style", null);

                        XElement tagTextRunR2 = AddElement(tagTextRunsR2, df, "TextRun", null);
                        if (item.DataType != "DateTime")
                        {
                            AddElement(tagTextRunR2, df, "Value", "=Fields!" + item.DataColumnReturnName + ".Value");
                        }
                        else
                        {
                            AddElement(tagTextRunR2, df, "Value", "=IIF(IsNothing(Fields!" + item.DataColumnReturnName + ".Value),\"-\",Format(Fields!" + item.DataColumnReturnName + ".Value,”dd/MM/yyyy”))");
                        }

                        XElement tagStyleR2 = AddElement(tagTextRunR2, df, "Style", null);
                        AddElement(tagStyleR2, df, "FontFamily", "Segoe UI");


                        XElement tagStyleR2TB = AddElement(tagTextboxR2, df, "Style", null);
                        XElement tagBorderR2TB = AddElement(tagStyleR2TB, df, "Border", null);
                        AddElement(tagBorderR2TB, df, "Color", "LightGrey");
                        AddElement(tagBorderR2TB, df, "Style", "Solid");

                        AddElement(tagStyleR2TB, df, "PaddingLeft", "2pt");
                        AddElement(tagStyleR2TB, df, "PaddingRight", "2pt");
                        AddElement(tagStyleR2TB, df, "PaddingTop", "2pt");
                        AddElement(tagStyleR2TB, df, "PaddingBottom", "2pt");


                       // noOfSelectedColumns++;
                       // intLoop++;
                    }
                }
            }
            ///-----------------------------------------------------
            AddElement(tagPage, df, "LeftMargin", "1in");
            AddElement(tagPage, df, "RightMargin", "1in");
            AddElement(tagPage, df, "TopMargin", "1in");
            AddElement(tagPage, df, "BottomMargin", "1in");
            AddElement(tagPage, df, "Style", null);
            return tagReport.ToString();
        }

        private static void TagReportColumnList(USReport report, XElement tagTablixMembersCol, XElement tagTablixColumns, XElement tagTablixCellsR1, ref int noOfSelectedColumns, ref int intLoop)
        {
            //Add Column list Loop 
            foreach (ReportEntity reportEntity in report.EntityList)
            {
                if (!reportEntity.IsGroupAll)
                {
                    foreach (ReportingEntityProperty item in reportEntity.Properties)
                    {
                        if (item.Selected)
                        {

                            XElement tagTablixColumn = AddElement(tagTablixColumns, df, "TablixColumn", null);
                            AddElement(tagTablixColumn, df, "Width", item.ColumnWidth.ToString() + "in");

                            AddElement(tagTablixMembersCol, df, "TablixMember", null);

                            // Row 1
                            XElement tagTablixCellR1 = AddElement(tagTablixCellsR1, df, "TablixCell", null);
                            XElement tagCellContentsR1 = AddElement(tagTablixCellR1, df, "CellContents", null);

                            XElement tagTextboxR1 = new XElement(df + "Textbox", new XAttribute("Name", "textbox" + intLoop));
                            tagCellContentsR1.Add(tagTextboxR1);
                            AddElement(tagTextboxR1, df, "CanGrow", "true");
                            AddElement(tagTextboxR1, df, "KeepTogether", "true");

                            XElement tagParagraphsR1 = AddElement(tagTextboxR1, df, "Paragraphs", null);
                            AddElement(tagTextboxR1, rd, "DefaultName", "textbox" + intLoop);

                            XElement tagStyleR1TB = AddElement(tagTextboxR1, df, "Style", null);
                            XElement tagBorderR1TB = AddElement(tagStyleR1TB, df, "Border", null);
                            AddElement(tagBorderR1TB, df, "Color", "LightGrey");
                            AddElement(tagBorderR1TB, df, "Style", "None");
                            AddElement(tagStyleR1TB, df, "BackgroundColor", "#3f5267");
                            AddElement(tagStyleR1TB, df, "PaddingLeft", "2pt");
                            AddElement(tagStyleR1TB, df, "PaddingRight", "2pt");
                            AddElement(tagStyleR1TB, df, "PaddingTop", "2pt");
                            AddElement(tagStyleR1TB, df, "PaddingBottom", "2pt");

                            XElement tagParagraphR1 = AddElement(tagParagraphsR1, df, "Paragraph", null);
                            XElement tagTextRunsR1 = AddElement(tagParagraphR1, df, "TextRuns", null);
                            XElement tagTextRunR1 = AddElement(tagTextRunsR1, df, "TextRun", null);
                            AddElement(tagTextRunR1, df, "Value", item.DataColumnReturnName);

                            XElement tagStyleR1 = AddElement(tagTextRunR1, df, "Style", null);
                            AddElement(tagStyleR1, df, "FontFamily", "Segoe UI Semibold");
                            AddElement(tagStyleR1, df, "FontSize", "11pt");
                            AddElement(tagStyleR1, df, "FontWeight", "Normal");
                            AddElement(tagStyleR1, df, "Color", "White");

                            AddElement(tagParagraphR1, df, "Style", null);

                            noOfSelectedColumns++;
                            intLoop++;
                        }
                    }
                }
            }
        }

        private static void TagReportGroup(XElement tagTablixMembersRow, List<string> groupItemList, List<string> displayHeaderGroupItemList, out XElement temptagTablixMembers2, out int textBoxNumber)
        {
            temptagTablixMembers2 = tagTablixMembersRow;
            XElement temptagTablixMember2 = null;
            textBoxNumber = 100;
            bool firstTimeLoop = true;

            if (groupItemList.Count >= 0)
            {
                foreach (string groupItem in groupItemList)
                {
                    //Row 1
                    //For Stepped Table....
                    XElement temptagTablixMembersForGroup = temptagTablixMembers2;
                    // XElement temptagTablixMemberForGroup = null;
                    int newTextBoxNumber = -1;
                    for (int i = 0; i < (groupItemList.Count - groupItemList.IndexOf(groupItem)); i++)
                    {
                        newTextBoxNumber = textBoxNumber++;
                        //add TablixMember...
                        XElement tagTablixMemberForGroup = AddElement(temptagTablixMembersForGroup, df, "TablixMember", null);
                        //add TablixHeader...
                        XElement tagTablixHeaderForGroup = AddElement(tagTablixMemberForGroup, df, "TablixHeader", null);
                        // XElement tagTablixMembersRowForNextLineForGroup = AddElement(tagTablixMemberForGroup, df, "TablixMembers", null);
                        //inside the header call......
                        AddElement(tagTablixHeaderForGroup, df, "Size", "2in");
                        XElement tagCellContentsForGroup = AddElement(tagTablixHeaderForGroup, df, "CellContents", null);
                        XElement tagTextboxInHeaderForGroup = new XElement(df + "Textbox", new XAttribute("Name", "Textbox" + newTextBoxNumber));
                        tagCellContentsForGroup.Add(tagTextboxInHeaderForGroup);
                        AddElement(tagTextboxInHeaderForGroup, df, "CanGrow", "true");
                        AddElement(tagTextboxInHeaderForGroup, df, "KeepTogether", "true");
                        XElement tagParagraphsInHeaderForGroup = AddElement(tagTextboxInHeaderForGroup, df, "Paragraphs", null);
                        XElement tagParagraphInHeaderForGroup = AddElement(tagParagraphsInHeaderForGroup, df, "Paragraph", null);
                        XElement tagTextRunsInHeaderForGroup = AddElement(tagParagraphInHeaderForGroup, df, "TextRuns", null);
                        XElement tagTextRunInHeaderForGroup = AddElement(tagTextRunsInHeaderForGroup, df, "TextRun", null);
                        if (groupItemList.IndexOf(groupItem) == 0)
                        {
                            AddElement(tagTextRunInHeaderForGroup, df, "Value", displayHeaderGroupItemList.ElementAt(i));
                        }
                        else
                        {
                            AddElement(tagTextRunInHeaderForGroup, df, "Value", null);
                        }
                        XElement tagStyleFontInHeaderForGroup = AddElement(tagTextRunInHeaderForGroup, df, "Style", null);
                        AddElement(tagStyleFontInHeaderForGroup, df, "FontFamily", "Segoe UI Semibold");
                        AddElement(tagStyleFontInHeaderForGroup, df, "FontSize", "11pt");
                        AddElement(tagStyleFontInHeaderForGroup, df, "FontWeight", "Normal");
                        AddElement(tagStyleFontInHeaderForGroup, df, "Color", "White");

                        AddElement(tagTextboxInHeaderForGroup, rd, "DefaultName", "Textbox" + newTextBoxNumber);
                        XElement tagStyleInHeaderForGroup = AddElement(tagTextboxInHeaderForGroup, df, "Style", null); //added the <Style />
                        XElement tagBorderInHeaderForGroup = AddElement(tagStyleInHeaderForGroup, df, "Border", null);
                        AddElement(tagBorderInHeaderForGroup, df, "Color", "LightGrey");
                        AddElement(tagBorderInHeaderForGroup, df, "Style", "Solid");
                        if (firstTimeLoop)
                        {
                            AddElement(tagStyleInHeaderForGroup, df, "BackgroundColor", "#3f5267");
                        }
                        else
                        {
                            AddElement(tagStyleInHeaderForGroup, df, "BackgroundColor", "#cbd4df");
                        }
                        AddElement(tagStyleInHeaderForGroup, df, "PaddingLeft", "2pt");
                        AddElement(tagStyleInHeaderForGroup, df, "PaddingRight", "2pt");
                        AddElement(tagStyleInHeaderForGroup, df, "PaddingTop", "2pt");
                        AddElement(tagStyleInHeaderForGroup, df, "PaddingBottom", "2pt");

                        // temptagTablixMemberForGroup = tagTablixMemberForGroup;
                        if (i != (groupItemList.Count - groupItemList.IndexOf(groupItem)) - 1)
                        {
                            XElement tagTablixMembersRowForNextLineForGroup = AddElement(tagTablixMemberForGroup, df, "TablixMembers", null);
                            temptagTablixMembersForGroup = tagTablixMembersRowForNextLineForGroup;
                        }
                        textBoxNumber++;

                    }
                    firstTimeLoop = false;
                    //Row 2
                    XElement tagTablixMember2 = AddElement(temptagTablixMembers2, df, "TablixMember", null);
                    //adding Group...
                    XElement tagGroup2 = new XElement(df + "Group", new XAttribute("Name", groupItem));
                    tagTablixMember2.Add(tagGroup2);

                    int curruntItemIndex = groupItemList.IndexOf(groupItem);
                    //if (curruntItemIndex == 0) ;
                    if (curruntItemIndex != 0)
                    {
                        XElement tagVisibilityHeader2 = AddElement(tagTablixMember2, df, "Visibility", null);
                        AddElement(tagVisibilityHeader2, df, "Hidden", "true");
                        AddElement(tagVisibilityHeader2, df, "ToggleItem", groupItemList.ElementAt(curruntItemIndex - 1));
                    }

                    //if (groupItemList.Count > 1)
                    //{
                    //    //AddElement(tagVisibilityHeader2, df, "Hidden", "true");
                    //    //AddElement(tagVisibilityHeader2, df, "ToggleItem", groupItem);
                    //}

                    XElement tagGroupExpressions2 = AddElement(tagGroup2, df, "GroupExpressions", null);
                    AddElement(tagGroupExpressions2, df, "GroupExpression", "=Fields!" + groupItem + ".Value");

                    //adding SortExpression...
                    XElement tagSortExpressions2 = AddElement(tagTablixMember2, df, "SortExpressions", null);
                    XElement tagSortExpression2 = AddElement(tagSortExpressions2, df, "SortExpression", null);
                    AddElement(tagSortExpression2, df, "Value", "=Fields!" + groupItem + ".Value");

                    XElement tagTablixHeader2 = AddElement(tagTablixMember2, df, "TablixHeader", null);
                    //inside the header call for Row 2......
                    AddElement(tagTablixHeader2, df, "Size", "2in");
                    XElement tagCellContents2 = AddElement(tagTablixHeader2, df, "CellContents", null);
                    XElement tagTextboxInHeader2 = new XElement(df + "Textbox", new XAttribute("Name", groupItem));
                    tagCellContents2.Add(tagTextboxInHeader2);
                    AddElement(tagTextboxInHeader2, df, "CanGrow", "true");
                    AddElement(tagTextboxInHeader2, df, "KeepTogether", "true");
                    XElement tagParagraphsInHeader2 = AddElement(tagTextboxInHeader2, df, "Paragraphs", null);
                    XElement tagParagraphInHeader2 = AddElement(tagParagraphsInHeader2, df, "Paragraph", null);
                    XElement tagTextRunsInHeader2 = AddElement(tagParagraphInHeader2, df, "TextRuns", null);
                    XElement tagTextRunInHeader2 = AddElement(tagTextRunsInHeader2, df, "TextRun", null);
                    AddElement(tagTextRunInHeader2, df, "Value", "=Fields!" + groupItem + ".Value");
                    XElement tagStyleFontInHeader2 = AddElement(tagTextRunInHeader2, df, "Style", null);
                    AddElement(tagStyleFontInHeader2, df, "FontFamily", "Segoe UI");

                    AddElement(tagTextboxInHeader2, rd, "DefaultName", groupItem);
                    XElement tagStyleInHeader2 = AddElement(tagTextboxInHeader2, df, "Style", null); //added the <Style />
                    XElement tagBorderInHeader2 = AddElement(tagStyleInHeader2, df, "Border", null);
                    AddElement(tagBorderInHeader2, df, "Color", "LightGrey");
                    AddElement(tagBorderInHeader2, df, "Style", "Solid");

                    temptagTablixMember2 = tagTablixMember2;
                    XElement tagTablixMembersRowForNextLine2 = AddElement(temptagTablixMember2, df, "TablixMembers", null);
                    temptagTablixMembers2 = tagTablixMembersRowForNextLine2;

                    textBoxNumber++;
                }
            }

            AddElement(temptagTablixMembers2, df, "TablixMember", null);

        }

        private static void TagReportBody(USReport report, XElement tagBody, out XElement tagTablix, out XElement tagTablixBody, out XElement tagTablixMembersCol, out XElement tagTablixMembersRow)
        {
            //Tag Body
            XElement tagReportItems = AddElement(tagBody, df, "ReportItems", null);
            AddElement(tagBody, df, "Height", "0.79in");
            AddElement(tagBody, df, "Style", null);
            tagTablix = new XElement(df + "Tablix", new XAttribute("Name", "table1"));
            tagReportItems.Add(tagTablix);


            XElement tagTextbox = new XElement(df + "Textbox", new XAttribute("Name", "textbox1"));
            tagReportItems.Add(tagTextbox);

            AddElement(tagTextbox, df, "CanGrow", "true");
            AddElement(tagTextbox, df, "KeepTogether", "true");

            XElement tagParagraphs = AddElement(tagTextbox, df, "Paragraphs", null);

            AddElement(tagTextbox, rd, "DefaultName", "textbox1");
            AddElement(tagTextbox, df, "Height", "0.36in");
            AddElement(tagTextbox, df, "Width", "5in");
            AddElement(tagTextbox, df, "ZIndex", "1");


            XElement tagTextboxStyle = AddElement(tagTextbox, df, "Style", null);
            AddElement(tagTextboxStyle, df, "PaddingLeft", "2pt");
            AddElement(tagTextboxStyle, df, "PaddingRight", "2pt");
            AddElement(tagTextboxStyle, df, "PaddingTop", "2pt");
            AddElement(tagTextboxStyle, df, "PaddingBottom", "2pt");

            XElement tagParagraph = AddElement(tagParagraphs, df, "Paragraph", null);
            XElement tagTextRuns = AddElement(tagParagraph, df, "TextRuns", null);
            XElement tagTextRun = AddElement(tagTextRuns, df, "TextRun", null);
            AddElement(tagTextRun, df, "Value", report.Name);
            XElement tagStyle = AddElement(tagTextRun, df, "Style", null);
            AddElement(tagStyle, df, "FontFamily", "Segoe UI Semibold");
            AddElement(tagStyle, df, "FontSize", "18pt");
            AddElement(tagStyle, df, "FontWeight", "Normal");
            AddElement(tagStyle, df, "Color", "Black");
            AddElement(tagParagraph, df, "Style", null);



            tagTablixBody = AddElement(tagTablix, df, "TablixBody", null);
            XElement tagTablixColumnHierarchy = AddElement(tagTablix, df, "TablixColumnHierarchy", null);
            tagTablixMembersCol = AddElement(tagTablixColumnHierarchy, df, "TablixMembers", null);



            XElement tagTablixRowHierarchy = AddElement(tagTablix, df, "TablixRowHierarchy", null);
            tagTablixMembersRow = AddElement(tagTablixRowHierarchy, df, "TablixMembers", null);

            ///////////-----------------------------------------------------------------------------------
        }

        private static void TagReportHeader(USReport report, string query, out XElement tagBody, out XElement tagPage)
        {

            //Report element

            XElement tagDataSources = AddElement(tagReport, df, "DataSources", null);
            XElement tagDataSets = AddElement(tagReport, df, "DataSets", null);
            tagBody = AddElement(tagReport, df, "Body", null);
            AddElement(tagReport, df, "Width", "6.63542in");
            tagPage = AddElement(tagReport, df, "Page", null);
            AddElement(tagReport, df, "Language", "en-US");
            AddElement(tagReport, df, "ConsumeContainerWhitespace", "true");
            AddElement(tagReport, rd, "ReportID", "123231313");
            AddElement(tagReport, rd, "ReportUnitType", "Inch");


            //Tag DataSources

            XElement tagDataSource = new XElement(df + "DataSource", new XAttribute("Name", "DataSource1"));
            tagDataSources.Add(tagDataSource);

            AddElement(tagDataSource, df, "DataSourceReference", "DataSource1");
            AddElement(tagDataSource, rd, "DataSourceID", "DataSource1");
            AddElement(tagDataSource, rd, "SecurityType", "None");


            //Tag DataSet
            XElement tagDataSet = new XElement(df + "DataSet", new XAttribute("Name", "DataSet3"));
            tagDataSets.Add(tagDataSet);

            XElement tagFields = AddElement(tagDataSet, df, "Fields", null);
            XElement tagQuery = AddElement(tagDataSet, df, "Query", null);
            AddElement(tagQuery, df, "DataSourceName", "DataSource1");
            AddElement(tagQuery, df, "CommandText", query);

            //Report Parameters adding...
            bool firstTime = true;
            XElement tagReportParameters = null;
            XElement tagQueryParameters = null;
            foreach (ConditionExpressionItem conditionItem in report.WhereConditionList)
            {
                if (conditionItem.AskedFromUser == true)
                {
                    if (firstTime == true)
                    {
                        tagReportParameters = AddElement(tagReport, df, "ReportParameters", null);
                        tagQueryParameters = AddElement(tagQuery, df, "QueryParameters", null);
                        firstTime = false;
                    }
                    XElement tagReportParameter = new XElement(df + "ReportParameter", new XAttribute("Name", conditionItem.EntityPropertyForRightOperand.UserParameterPromptNane.Substring(1)));
                    tagReportParameters.Add(tagReportParameter);
                    if (conditionItem.EntityPropertyForLeftOperand.DataType.ToUpper() == "INT")
                    {
                        AddElement(tagReportParameter, df, "DataType", "Integer");
                    }
                    else if (conditionItem.EntityPropertyForLeftOperand.DataType.ToUpper() == "DECIMAL")
                    {
                        AddElement(tagReportParameter, df, "DataType", "Float");
                    }
                    else if (conditionItem.EntityPropertyForLeftOperand.DataType.ToUpper() == "BIT")
                    {
                        AddElement(tagReportParameter, df, "DataType", "Boolean");
                    }
                    else if (conditionItem.EntityPropertyForLeftOperand.DataType.ToUpper() == "NUMERIC")
                    {
                        AddElement(tagReportParameter, df, "DataType", "Float");
                    }
                    else
                    {
                        AddElement(tagReportParameter, df, "DataType", conditionItem.EntityPropertyForLeftOperand.DataType);
                    }
                    AddElement(tagReportParameter, df, "Nullable", "true");
                    AddElement(tagReportParameter, df, "Prompt", conditionItem.EntityPropertyForRightOperand.UserParameterPromptNane.Substring(1));//.DataColumnReturnName);

                   // XElement tagQueryParameter = new XElement(df + "QueryParameter", new XAttribute("Name", "@" + conditionItem.EntityPropertyForRightOperand.UserParameterPromptNane));
                    XElement tagQueryParameter = new XElement(df + "QueryParameter", new XAttribute("Name", conditionItem.EntityPropertyForRightOperand.UserParameterPromptNane));
                    tagQueryParameters.Add(tagQueryParameter);
                    //AddElement(tagQueryParameter, df, "Value", "=Parameters!" + conditionItem.EntityPropertyForRightOperand.UserParameterPromptNane + ".Value");
                   // string value = conditionItem.EntityPropertyForRightOperand.UserParameterPromptNane.Substring(1);
                    AddElement(tagQueryParameter, df, "Value", "=Parameters!" + conditionItem.EntityPropertyForRightOperand.UserParameterPromptNane.Substring(1) + ".Value");
                }
            }

            //Add Field list Loop 
            foreach (ReportEntity entity in report.EntityList)
            {
                foreach (ReportingEntityProperty selectedField in entity.Properties)
                {
                    //Grouped colums cannot be included to the field list.....
                    if (selectedField.Selected)
                    {
                        XElement tagField = new XElement(df + "Field", new XAttribute("Name", selectedField.DataColumnReturnName));
                        tagFields.Add(tagField);
                        AddElement(tagField, df, "DataField", selectedField.DataColumnReturnName);
                        AddElement(tagField, rd, "TypeName", selectedField.DataType);
                    }
                    else if (report.WhereConditionList.Count > 0)
                    {
                        List<string> alreadyAddedProperties = new List<string>();
                        bool alreadyAdded = false;

                        foreach (ConditionExpressionItem wc in report.WhereConditionList)
                        {
                            if (wc.EntityPropertyForLeftOperand.DataColumnReturnName == selectedField.DataColumnReturnName)
                            {
                                for (int i = 0; i < alreadyAddedProperties.Count; i++)
                                {
                                    if (wc.EntityPropertyForLeftOperand.DataColumnReturnName == alreadyAddedProperties[i])
                                    {
                                        alreadyAdded = true;
                                        break;
                                    }
                                }
                                if (!alreadyAdded)
                                {
                                    alreadyAddedProperties.Add(wc.EntityPropertyForLeftOperand.DataColumnReturnName);
                                    XElement tagField = new XElement(df + "Field", new XAttribute("Name", selectedField.DataColumnReturnName));
                                    tagFields.Add(tagField);
                                    AddElement(tagField, df, "DataField", selectedField.DataColumnReturnName);
                                    AddElement(tagField, rd, "TypeName", selectedField.DataType);
                                }
                            }
                        }
                    }
                }
            }



            //End Field list loop
        }

        private static string GetCountCellValues(List<ReportEntity> list, int groupListCount, int i)
        {
            string value = string.Empty;
            if (list[i].IsCount)
            {
                value = "=\" Count = \"+CStr(CountRows())+\" : \"";
            }
            return value;
        }

        private static string GetGroupMergedCellValues(List<ReportEntity> list,int groupListCount, int i)
        {
            bool firstTimeInLoop = true;
            string value ="="+ string.Empty;
            //if (list[i].IsCount)
            //{
            //     value = "=\" Count = \"+CStr(CountRows())+\" : \"" + string.Empty + "+";
            //}
            //else
            //{
            //     value ="="+ string.Empty;
            //}
            int counter = 0;
            foreach (ReportingEntityProperty propertySelected in list[i].Properties)
            {
                if (propertySelected.Selected)
                {
                    counter++;
                    if (!firstTimeInLoop)
                    {
                        value = value + "\" " + propertySelected.DisplayName + " - \"+" + "CStr(Fields!" + propertySelected.DataColumnReturnName + ".Value)" + "+\",\"+";
                    }
                    firstTimeInLoop = false;
                }
            }
            if (counter > 1)
            {
                return value.Trim("+\",\"+".ToCharArray());
            }
            else
            {
                return "";
            }
        }
        private static string GetGroupMergedSumCellValues(List<ReportEntity> list, int groupListCount, int i)
        {
            string value ="="+string.Empty;
            for (int j = groupListCount; j < list.Count; j++)
            {
                foreach (ReportingEntityProperty propertySelected in list[j].Properties)
                {
                    if (propertySelected.Aggregated)
                    {
                        value = value + "\" " + propertySelected.DisplayName + " Sum = \"+" + "CStr(SUM(Fields!" + propertySelected.DataColumnReturnName + ".Value))" + "+\",\"+";
                    }
                }
            }
            if (value.Trim()=="=")
            {
                return "";
            }
            return value.Trim("+\",\"+".ToCharArray());
        }

        public static XElement AddElement(XElement parent, XNamespace ns, string name, string value)
        {
            XElement newelement = new XElement(ns + name, value);

            parent.Add(newelement);
            return newelement;
        }
    }
}
