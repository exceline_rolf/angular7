﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;
using US.Common.Notification.API;
using US.Common.Notification.Core.DomainObjects;
using US.GMS.Core.Utils;
using US.Payment.Core.Enums;
using US.Payment.Reporting.Core.DomainObjects;
using US.Payment.Reporting.BL;
using System.Net;
using System.Web.Services.Protocols;
using US.Common.Logging.API;
using US.Payment.Core.BusinessDomainObjects.Reporting;
using XMLUtils = US.Payment.Core.Utils.XMLUtils;

namespace US.Payment.Reporting.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class USPReporting : IUSPReporting, IPolicyRetriever
    {
        //Temp variables for debugging purpose...
        //string CoreSessionUserName = ConfigurationManager.AppSettings["CoreSessionUserName"];
        //string BranchId = ConfigurationManager.AppSettings["BranchId"];

        public List<ReportInfo> GetReportInfoList()
        {
            return new List<ReportInfo>();
        }

        public string GetReportServerRootPath()
        {
            try
            {
                US.Common.Logging.API.USLogEvent.WriteToFile("Report : GetReportServerRootPath initiated.", "TestUser");
                string _rootFilepath = string.Empty;
                _rootFilepath = GetReportFilePath();
                return _rootFilepath;
            }
            catch (Exception ex)
            {
                US.Common.Logging.API.USLogError.WriteToFile("Report : Error in GetReportInfoList: " + ex.Message, new Exception(), "TestUser");
                //File.WriteAllText(@"D:\usplog.txt", ex.Message);
            }
            return string.Empty;
        }
        private string _broserRemitFilePath = string.Empty;
        public string GetReportFilePath()
        {
            if (string.IsNullOrEmpty(_broserRemitFilePath))
            {
                _broserRemitFilePath = ConfigurationManager.AppSettings["ReportsFilePath"];
            }
            return _broserRemitFilePath;
        }

        #region IPolicyRetriever Members

        public Stream ProvidePolicyFile()
        {
            string result = @"<?xml version=""1.0"" encoding=""utf-8""?>
<access-policy>
    <cross-domain-access>
        <policy>
            <allow-from http-request-headers=""*"">
                <domain uri=""*""/>
            </allow-from>
            <grant-to>
                <resource path=""/"" include-subpaths=""true""/>
            </grant-to>
        </policy>
    </cross-domain-access>
</access-policy>";

            if (WebOperationContext.Current != null)
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";
            return new MemoryStream(Encoding.UTF8.GetBytes(result));
        }


        #endregion

        //getting the parameters from app config file...
        //string CoreSessionUserName = ConfigurationManager.AppSettings["userName"];

        public List<ReportEntity> GetFilteredReportEntities(List<string> reportEntityList, string CoreSessionUserName)
        {
            USLogEvent.WriteToFile("Report : GetFilteredReportEntities initiated.", CoreSessionUserName);
            try
            {
                return API.Reporting.GetFilteredReportEntities(reportEntityList, CoreSessionUserName);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : GetFilteredReportEntities Error : " + ex.Message, ex, CoreSessionUserName);
                return new List<ReportEntity>();
            }
        }
        public string ViewAllReports(string CoreSessionUserName)
        {
            USLogEvent.WriteToFile("Report : ViewAllReports initiated.", CoreSessionUserName);
            List<USReport> reportList = API.Reporting.ViewAllReports(CoreSessionUserName);
            return XMLUtils.SerializeDataContractObjectToXML(reportList);
        }

        public string GetReportById(int reportId, string CoreSessionUserName)
        {
            USLogEvent.WriteToFile("Report : GetReportById initiated.", CoreSessionUserName);
            USReport report = API.Reporting.GetReportById(reportId, CoreSessionUserName);
            return XMLUtils.SerializeDataContractObjectToXML(report);

        }

        public List<ReportSchedule> GetScheduleListByReportId(int reportId, string CoreSessionUserName)
        {
            USLogEvent.WriteToFile("Report : GetScheduleListByReportId intiated.", CoreSessionUserName);
            List<ReportSchedule> scheduleList = API.Reporting.GetScheduleListByReportId(reportId, CoreSessionUserName);
            //return XMLUtils.SerializeDataContractObjectToXML(scheduleList);
            return scheduleList;
        }


        public List<ReportEntity> GetAllEntityList(string CoreSessionUserName)
        {
            USLogEvent.WriteToFile("Report : GetAllEntityList initiated.", CoreSessionUserName);
            return API.Reporting.GetAllEntityList(CoreSessionUserName);
        }

        public bool DeleteReport(int reportIdToBeDeleted, string item, string CoreSessionUserName)
        {
            return API.Reporting.DeleteReport(reportIdToBeDeleted, item, CoreSessionUserName);
        }

        public List<string> GetAllReportCategories(string CoreSessionUserName)
        {
            USLogEvent.WriteToFile("Report : GetAllReportCategories initiated.", CoreSessionUserName);
            return API.Reporting.GetAllReportCategories(CoreSessionUserName);
        }
        public List<UserRole> GetAllRoles(string CoreSessionUserName)
        {
            USLogEvent.WriteToFile("Report : GetAllRoles initiated.", CoreSessionUserName);
            return API.Reporting.GetAllRoles(CoreSessionUserName);
        }
        public bool SaveCategory(string name, string CoreSessionUserName)
        {
            try
            {
                USLogEvent.WriteToFile("Report : SaveCategory initiated.", CoreSessionUserName);
                return API.Reporting.SaveCategory(name, CoreSessionUserName);
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateCategory(string oldName, string newName, string CoreSessionUserName)
        {
            USLogEvent.WriteToFile("Report : UpdateCategory initiated.", CoreSessionUserName);
            return API.Reporting.UpdateCategory(oldName, newName, CoreSessionUserName);
        }
        //public WhereCondition getWhereCondition()
        //{
        //    return new WhereCondition();
        //}
        public USReport getUSReport()
        {
            return new USReport();
        }

        public string SaveReportConfiguration(string report, string fileName, string CoreSessionUserName)
        {
            return API.Reporting.SaveReportConfiguration(report, fileName, CoreSessionUserName);
        }

        public string SaveEditedReportConfiguration(string report, string fileName, string CoreSessionUserName)
        {
            return API.Reporting.SaveEditedReportConfiguration(report, fileName, CoreSessionUserName);
        }

        public bool SaveEditedReportQuery(string report, string CoreSessionUserName)
        {
            return API.Reporting.SaveEditedReportQuery(report, CoreSessionUserName);
        }

        public List<ReportEntity> GetAllEntitiesWithProperties(string CoreSessionUserName)
        {
            USLogEvent.WriteToFile("Report : GetAllEntitiesWithProperties initiated.", CoreSessionUserName);
            return API.Reporting.GetAllEntitiesWithProperties(CoreSessionUserName);
        }
        public string TestQuery(string query, string CoreSessionUserName)
        {
            USLogEvent.WriteToFile("Report : TestQuery initiated.", CoreSessionUserName);
            List<string> dt = API.Reporting.TestQuery(query, CoreSessionUserName);
            return XMLUtils.SerializeDataContractObjectToXML(dt);
        }
        public string FollowUpQuery(string query, string CoreSessionUserName)
        {
            USLogEvent.WriteToFile("Report : FollowUpQuery initiated.", CoreSessionUserName);
            List<string> dt = API.Reporting.FollowUpQuery(query, CoreSessionUserName);
            return XMLUtils.SerializeDataContractObjectToXML(dt);
        }
        public string IsQueryVisible()
        {
            return API.Reporting.IsQueryVisible();
        }

        public List<ReportingEntityProperty> GetEntityPropertyInfo(int filterEntityId, ReportEntity reportEntity, string CoreSessionUserName)
        {
            return API.Reporting.GetEntityPropertyInfo(filterEntityId, reportEntity, CoreSessionUserName);
        }

        public string GetExcelineReportServerPath()
        {
            return ConfigurationManager.AppSettings["ExcelineServerPathForReportView"].Trim();
        }

        public int SendReportOutPut(string userName, string subject, string body, bool isSelected, int outputType, List<string> recipientList, string primaryColumn, string basedOn, bool reminder, bool addreminderFee, int branchID, DateTime? ReminderDueDate)
        {
            //outputType : 0-Email,1-SMS, 2- Print
            return API.Reporting.SendReportOutPut(userName, subject, body, isSelected, outputType, recipientList, primaryColumn, basedOn, reminder, addreminderFee, branchID, ReminderDueDate);
        }


        public string SaveAsReportConfiguration(string report, string fileName, string CoreSessionUserName)
        {
            return API.Reporting.SaveReportConfiguration(report, fileName, CoreSessionUserName);
        }

        //Admin pannel implementation
        public string SaveReportSetting(string userName, ReportConfigureSetting settingItem)
        {
            return API.Reporting.SaveReportSetting(userName, settingItem);
        }

        public ReportConfigureSetting GetReportSetting(string userName)
        {
            return API.Reporting.GetReportSetting(userName);
        }

        public List<ReportScheduleTemplate> GetScheduleTemplateList(KeyValueObject category, string entity, string userName)
        {
            //should be implemented.....
            return API.Reporting.GetScheduleTemplateList(category, entity, userName);

            //List<ReportScheduleTemplate> aa = new List<ReportScheduleTemplate>();

            //ReportScheduleTemplate temp = new ReportScheduleTemplate();
            //temp.TemplateId = 1;
            //temp.TemplateName = "temp1";
            //ReportScheduleTemplate temp2 = new ReportScheduleTemplate();
            //temp2.TemplateId = 2;
            //temp2.TemplateName = "temp2";

            //aa.Add(temp);
            //aa.Add(temp2);
            //return aa;
        }


        public Dictionary<string, bool> GetEnbleEntitesByEntity(string EntityName, string userName)
        {
            return API.Reporting.GetEnbleEntitesByEntity(EntityName, userName);
        }

        public List<EntityPropertyForGrid> GetEntityWithProperties(int entityId, string userName)
        {
            return API.Reporting.GetEntityWithProperties(entityId, userName);
        }

        public List<GymUsage> GetGymUsageReportSetting(int branchId, string userName)
        {
            return API.Reporting.GetGymUsageReportSetting(branchId, userName);
        }

        public bool UpdateGymUsageReportSetting(List<GymUsage> settingList, int branchId, string userName)
        {
            return API.Reporting.UpdateGymUsageReportSetting(settingList, branchId, userName);
        }


        public int SaveChangePropertyDisplayName(List<EntityPropertyForGrid> propertyList, string userName)
        {
            return API.Reporting.SaveChangePropertyDisplayName(propertyList, userName);
        }

        public List<ExceNotificationTepmlateDC> GetSMSNotificationTemplateByEntity(string entity, string user)
        {
            var result = NotificationAPI.GetSMSNotificationTemplateByEntity(entity, ExceConnectionManager.GetGymCode(user));

            if (result.ErrorOccured)
            {
                foreach (var mgs in result.NotificationMessages.Where(mgs => mgs.ErrorLevel == ErrorLevels.Error))
                {
                    USLogError.WriteToFile(mgs.Message, new Exception(), user);
                }
                return new List<ExceNotificationTepmlateDC>();
            }
            return result.MethodReturnValue;
        }



        public List<UserRole> GetRoleListByReportId(int reportId, string user)
        {
            try
            {
                return API.Reporting.GetRoleListByReportId(reportId, user);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Error on GetRoleListByReportId", ex, user);
            }
            return new List<UserRole>();
        }


        public int GetReminderDueDays(string user)
        {
            try
            {
                return API.Reporting.GetReminderDueDays(user);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Error on GetRoleListByReportId", ex, user);
            }
            return 0;
        }
    }
}
