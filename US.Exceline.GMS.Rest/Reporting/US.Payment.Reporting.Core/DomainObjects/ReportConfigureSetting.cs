﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Reporting.Core.DomainObjects
{
    public class ReportConfigureSetting
    {
        private List<ReportSetting> _reportSettingList = new List<ReportSetting>();

        public List<ReportSetting> ReportSettingList
        {
            get { return _reportSettingList; }
            set { _reportSettingList = value; }
        }
    }
}
