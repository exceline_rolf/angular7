﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Reporting.Core.DomainObjects
{
    public class JoinQuery
    {
        private string _firstEntity = string.Empty;

        public string FirstEntity
        {
            get { return _firstEntity; }
            set { _firstEntity = value; }
        }
        private string _sencondEntity = string.Empty;

        public string SencondEntity
        {
            get { return _sencondEntity; }
            set { _sencondEntity = value; }
        }
        private string _joinQueryText = string.Empty;

        public string JoinQueryText
        {
            get { return _joinQueryText; }
            set { _joinQueryText = value; }
        }
    }
}
