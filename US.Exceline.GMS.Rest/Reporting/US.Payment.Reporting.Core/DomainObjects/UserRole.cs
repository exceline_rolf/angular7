﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Reporting.Core.DomainObjects
{
    public class UserRole
    {
        private int _id = -1;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _roleName = string.Empty;

        public string RoleName
        {
            get { return _roleName; }
            set { _roleName = value; }
        }
        private string _description = string.Empty;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private bool _isEditable;
        public bool IsEditable
        {
            get { return _isEditable; }
            set { 
                _isEditable = value;
                if(value)
                    _isView = true;
            }
        }

        private bool _isView;
        public bool IsView
        {
            get { return _isView; }
            set { _isView = value; }
        }

    }
}
