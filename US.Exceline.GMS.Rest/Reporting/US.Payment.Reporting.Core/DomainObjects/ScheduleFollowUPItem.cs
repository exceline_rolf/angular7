﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Reporting.Core.DomainObjects
{
    public class ScheduleFollowUPItem
    {
        private string _scheduleItemFollowupNameText = string.Empty;

        public string ScheduleItemFollowupNameText
        {
            get { return _scheduleItemFollowupNameText; }
            set { _scheduleItemFollowupNameText = value; }
        }
        private string _followupStartTime = string.Empty;

        public string FollowupStartTime
        {
            get { return _followupStartTime; }
            set { _followupStartTime = value; }
        }
        private string _followupEndTime = string.Empty;

        public string FollowupEndTime
        {
            get { return _followupEndTime; }
            set { _followupEndTime = value; }
        }
        private string _followupDescription = string.Empty;

        public string FollowupDescription
        {
            get { return _followupDescription; }
            set { _followupDescription = value; }
        }
    }
}
