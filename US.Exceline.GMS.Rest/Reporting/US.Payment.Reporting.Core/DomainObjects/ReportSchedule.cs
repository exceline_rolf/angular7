﻿using System;

namespace US.Payment.Reporting.Core.DomainObjects
{
    public class ReportSchedule
    {
        private int _scheduleId = 0;

        public int ScheduleId
        {
            get { return _scheduleId; }
            set { _scheduleId = value; }
        }

        private DateTime _scheduleStartDate;

        public DateTime ScheduleStartDate
        {
            get { return _scheduleStartDate; }
            set { _scheduleStartDate = value; }
        }
        private DateTime _scheduleEnddate;

        public DateTime ScheduleEnddate
        {
            get { return _scheduleEnddate; }
            set { _scheduleEnddate = value; }
        }

        private bool _isUntillFurtherNotice = false;

        public bool IsUntillFurtherNotice
        {
            get { return _isUntillFurtherNotice; }
            set { _isUntillFurtherNotice = value; }
        }

        private KeyValueObject _scheduleRecurrence = new KeyValueObject();

        public KeyValueObject ScheduleRecurrence
        {
            get { return _scheduleRecurrence; }
            set { _scheduleRecurrence = value; }
        }

        private KeyValueObject _scheduleMonth = new KeyValueObject();

        public KeyValueObject ScheduleMonth
        {
            get { return _scheduleMonth; }
            set { _scheduleMonth = value; }
        }

        private KeyValueObject _scheduleWeek = new KeyValueObject();

        public KeyValueObject ScheduleWeek
        {
            get { return _scheduleWeek; }
            set { _scheduleWeek = value; }
        }

        private KeyValueObject _scheduleDay = new KeyValueObject();

        public KeyValueObject ScheduleDay
        {
            get { return _scheduleDay; }
            set { _scheduleDay = value; }
        }

        private KeyValueObject _scheduleDate = new KeyValueObject();
        public KeyValueObject ScheduleDate
        {
            get { return _scheduleDate; }
            set { _scheduleDate = value; }
        }

        private DateTime _scheduleTime;
        public DateTime ScheduleTime
        {
            get { return _scheduleTime; }
            set { _scheduleTime = value; }
        }
        private string _reportPrimaryEntity = string.Empty;

        public string ReportPrimaryEntity
        {
            get { return _reportPrimaryEntity; }
            set { _reportPrimaryEntity = value; }
        }
        private KeyValueObject _scheduleItemCategory = new KeyValueObject();

        public KeyValueObject ScheduleItemCategory
        {
            get { return _scheduleItemCategory; }
            set { _scheduleItemCategory = value; }
        }

        private ReportScheduleTemplate _scheduleItemTemplate = new ReportScheduleTemplate();

        public ReportScheduleTemplate ScheduleItemTemplate
        {
            get { return _scheduleItemTemplate; }
            set { _scheduleItemTemplate = value; }
        }

        private string _scheduleItemRecipient = string.Empty;

        public string ScheduleItemRecipient
        {
            get { return _scheduleItemRecipient; }
            set { _scheduleItemRecipient = value; }
        }
        private UserRole _scheduleItemRecipientRole = new UserRole();

        public UserRole ScheduleItemRecipientRole
        {
            get { return _scheduleItemRecipientRole; }
            set { _scheduleItemRecipientRole = value; }
        }
        private int _scheduleItemHours = 0;

        public int ScheduleItemHours
        {
            get { return _scheduleItemHours; }
            set { _scheduleItemHours = value; }
        }
        private bool _isDeleted = false;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }
        private ScheduleFollowUPItem _reportScheduleFollowUPItem = new ScheduleFollowUPItem();

        public ScheduleFollowUPItem ReportScheduleFollowUPItem
        {
            get { return _reportScheduleFollowUPItem; }
            set { _reportScheduleFollowUPItem = value; }
        }

    }
}
