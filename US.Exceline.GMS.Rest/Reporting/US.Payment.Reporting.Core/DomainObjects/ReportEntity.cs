﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Reporting.Core.DomainObjects
{
    public class ReportEntity
    {
        private string _id = string.Empty;

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _displayName = string.Empty;

        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }
        private  List<ReportingEntityProperty> _properties = new List<ReportingEntityProperty>();

        public List<ReportingEntityProperty> Properties
        {
            get { return _properties; }
            set { _properties = value; }
        }
        private string _tableOrViewName = string.Empty;

        public string TableOrViewName
        {
            get { return _tableOrViewName; }
            set { _tableOrViewName = value; }
        }

        private bool _isGroupAll = false;

        public bool IsGroupAll
        {
            get { return _isGroupAll; }
            set { _isGroupAll = value; }
        }
        private bool _isCount = false;

        public bool IsCount
        {
            get { return _isCount; }
            set { _isCount = value; }
        }
        private bool _enableCount = true;

        public bool EnableCount
        {
            get { return _enableCount; }
            set { _enableCount = value; }
        }
        private bool _enableGrouping = true;

        public bool EnableGrouping
        {
            get { return _enableGrouping; }
            set { _enableGrouping = value; }
        }

        private string _primaryColumn = string.Empty;
        public string PrimaryColumn
        {
            get { return _primaryColumn; }
            set { _primaryColumn = value; }
        }
        private string _basedOn = string.Empty;

        public string BasedOn
        {
            get { return _basedOn; }
            set { _basedOn = value; }
        }
        private bool _isEnableEntity = false;

        public bool IsEnableEntity
        {
            get { return _isEnableEntity; }
            set { _isEnableEntity = value; }
        }

        private bool _isSelect = false;

        public bool isSelect
        {
            get { return _isSelect; }
            set { _isSelect = value; }
        }

    }
}
