﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Reporting.Core.DomainObjects
{
    public class ConditionExpressionItem
    {
        private ReportEntity _reportEntityForLeft = new ReportEntity();

        public ReportEntity ReportSingleEntityForLeft
        {
            get { return _reportEntityForLeft; }
            set
            {
                _reportEntityForLeft = value;
            }
        }
        private ReportEntity _reportEntityForRight = new ReportEntity();

        public ReportEntity ReportSingleEntityForRight
        {
            get { return _reportEntityForRight; }
            set
            {
                _reportEntityForRight = value;
            }
        }

        private ReportingEntityProperty _leftOperand = new ReportingEntityProperty();

        public ReportingEntityProperty EntityPropertyForLeftOperand
        {
            get { return _leftOperand; }
            set
            {
                _leftOperand = value;
            }
        }
        private ReportingEntityProperty _rightOperand = new ReportingEntityProperty();
        public ReportingEntityProperty EntityPropertyForRightOperand
        {
            get { return _rightOperand; }
            set
            {
                _rightOperand = value;
            }
        }
        private string _operator = "=";

        public string Operator
        {
            get { return _operator; }
            set { _operator = value; }
        }
        private string _displayingOperator = string.Empty;

        public string DisplayingOperator
        {
            get { return _displayingOperator; }
            set { _displayingOperator = value; }
        }
        private string _conjunction = "AND";

        public string Conjunction
        {
            get { return _conjunction; }
            set { _conjunction = value; }
        }
        private bool _askedFromUser = false;

        public bool AskedFromUser
        {
            get { return _askedFromUser; }
            set { _askedFromUser = value; }
        }
        private string _selectedDate = string.Empty;

        public string SelectedDate
        {
            get { return _selectedDate; }
            set { _selectedDate = value; }
        }

        private string _value = string.Empty;

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        private string _conjunctionVisible = "Visible";

        public string ConjunctionVisible
        {
            get { return _conjunctionVisible; }
            set { _conjunctionVisible = value; }
        }

        private string waterMarkText = "";

        public string WaterMarkText
        {
            get { return waterMarkText; }
            set { waterMarkText = value;  }
        }

        private bool _isTextBoxEnabled = true;

        public bool IsTextBoxEnabled
        {
            get { return _isTextBoxEnabled; }
            set { _isTextBoxEnabled = value; }
        }

        private string _leftBrackets = "";

        public string LeftBrackets
        {
            get { return _leftBrackets; }
            set { _leftBrackets = value; }
        }
        private string _rightBrackets = "";

        public string RightBrackets
        {
            get { return _rightBrackets; }
            set { _rightBrackets = value; }
        }
        private bool _isChecked = false;

        public bool IsChecked
        {
            get { return _isChecked; }
            set { _isChecked = value; }
        }

        private List<ViewProperty> _viewColor = new List<ViewProperty>();

        public List<ViewProperty> ViewColor
        {
            get { return _viewColor; }
            set { _viewColor = value; }
        }
        private bool _isOpen = false;

        public bool IsOpen
        {
            get { return _isOpen; }
            set { _isOpen = value; }
        }
        private string _helpBtnVisibility = "Collapsed";

        public string HelpBtnVisibility
        {
            get { return _helpBtnVisibility; }
            set { _helpBtnVisibility = value; }
        }

        //public string HelpBtnVisibility
        //{
        //    get 
        //    {
        //        if (EntityPropertyForLeftOperand.DataType == "DateTime")
        //            return "Visible";
        //        else
        //            return "Collapsed";
        //    }
        //    set { _helpBtnVisibility = value; }
        //}
        //private string _toolTipText = string.Empty;

        //public string ToolTipText
        //{
        //    get { return _toolTipText; }
        //    set { _toolTipText = value; }
        //}
        //private PreFunction _preFunction = new PreFunction();

        //public PreFunction PreFunction
        //{
        //    get { return _preFunction; }
        //    set { _preFunction = value; }
        //}
    }
}
