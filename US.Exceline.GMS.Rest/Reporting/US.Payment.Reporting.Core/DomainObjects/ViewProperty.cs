﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Reporting.Core.DomainObjects
{
   public class ViewProperty
    {
        private int _ID = -1;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        private string _IsView = "Collapsed";

        public string IsView
        {
            get { return _IsView; }
            set { _IsView = value; }
        }

        private string _IsColor = string.Empty;

        public string IsColor
        {
            get { return _IsColor; }
            set { _IsColor = value; }
        }
    }
}
