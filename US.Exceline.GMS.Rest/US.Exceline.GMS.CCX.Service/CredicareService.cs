﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.ServiceModel;
using US.Common.Logging.API;
using US.GMS.API;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.CCX.Service
{
    public class CredicareService : ICredicareService
    {
        public string GetAllInvoices(int creditorNo,int chunkSize,int batchSequence)
        {
            try
            {
                string orderlineInvoices = ConfigurationManager.AppSettings["OrderlineINVTypes"].ToString();
                List<string> invTypes = new List<string>();
                if (!string.IsNullOrEmpty(orderlineInvoices))
                {
                    string[] invoiceTypes = orderlineInvoices.Split(',');
                    invTypes = new List<string>(invoiceTypes);
                }

                OperationResult<string> result = GMSCcx.GetAllInvoices(chunkSize, creditorNo, batchSequence, invTypes);
                if (result.ErrorOccured)
                {
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "System");
                    }
                    return result.OperationReturnValue;
                }
                else
                {
                    return result.OperationReturnValue;
                }
            }
            catch (FaultException )
            {               
                return "Error in Service";
            }
            catch (Exception )
            {
                return "Error in Service";
            }
        }
        public string GetDebtWarningInvoices(int creditorNo, int chunkSize, int batchSequence)
        {
            try
            {
                OperationResult<string> result = GMSCcx.GetDebtWarningInvoices(chunkSize, creditorNo, batchSequence);
                if (result.ErrorOccured)
                {
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "System");
                    }
                    return result.OperationReturnValue;
                }
                else
                {
                    return result.OperationReturnValue;
                }
            }
            catch (FaultException )
            {
                return "Error in Service";
            }
            catch (Exception )
            {
                return "Error in Service";
            }           
        }

        public string GetInvoices(DateTime startDate, DateTime endDate, int chunkSize)
        {
            return string.Empty;
        }


        public string GetDirectPayments(int creditorNo, int chunkSize,int batchSequenceNo)
        {
            try
            {
                OperationResult<string> result = GMSCcx.GetDirectPayments(chunkSize, creditorNo, batchSequenceNo);
                if (result.ErrorOccured)
                {
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "System");
                    }
                    return result.OperationReturnValue;
                }
                else
                {
                    return result.OperationReturnValue;
                }
            }
            catch (FaultException )
            {                
                return "";
            }
            catch (Exception )
            {               
                return "";
            }
            
        }

        public int AddPaymentStatus(string paymentStatusData, int creditorNo)
        {
            try
            {
                OperationResult<int> result = GMSCcx.AddPaymentStatus(paymentStatusData, creditorNo);
                if (result.ErrorOccured)
                {
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "System");
                    }
                    result.OperationReturnValue = -1;
                    return result.OperationReturnValue;
                }
                else
                {
                    return result.OperationReturnValue;
                }
            }
            catch (FaultException )
            {              
                return -1;
            }
            catch (Exception )
            {                
                return -1;
            }
            
        }

        public int AddNewCancelledStatus(string newCancelledStatus, int creditorNo)
        {
            try
            {
                OperationResult<int> result = GMSCcx.AddNewCancelledStatus(newCancelledStatus, creditorNo);
                if (result.ErrorOccured)
                {
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "System");
                    }
                    result.OperationReturnValue = -1;
                    return result.OperationReturnValue;
                }
                else
                {
                    return result.OperationReturnValue;
                }
            }
            catch (FaultException )
            {               
                return -1;
            }
            catch (Exception )
            {                
                return -1;
            }
           
        }

        public int GetDataImportReceipt(string dataImportReceipt, int creditorNo)
        {
            try
            {
                OperationResult<int> result = GMSCcx.AddDataImportReceipt(dataImportReceipt, creditorNo);
                if (result.ErrorOccured)
                {
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "System");
                    }
                    result.OperationReturnValue = -1;
                    return result.OperationReturnValue;
                }
                else
                {
                    return result.OperationReturnValue;
                }
            }
            catch (FaultException )
            {                
                return -1;
            }
            catch (Exception )
            {              
                return -1;
            }           
        }

        public string ServiceTestMethod(string testString)
        {
            return testString;        
        }

        public void GetRemitFile(byte[] byteStream, int creditorNo)
        {
            try
            {
                
                string folderPath = System.Configuration.ConfigurationSettings.AppSettings["OCRPath"];
                string crediorPath = folderPath + "\\" + creditorNo + "\\";
                string fileName = "OCR." + US.GMS.Core.Utils.Common.GetDate() + ".txt";

                createDirectory(crediorPath);
                if (!File.Exists(crediorPath + "\\" + fileName))
                {
                    System.IO.FileStream oFileStream = null;
                    oFileStream = new System.IO.FileStream(crediorPath + "\\" + fileName, System.IO.FileMode.Create);
                    oFileStream.Write(byteStream, 0, byteStream.Length);
                    oFileStream.Close();
                }
                {
                    string failPath = folderPath + "\\" + creditorNo + "\\AlreadyExists";
                    createDirectory(failPath);
                    fileName = "OCR." + US.GMS.Core.Utils.Common.GetDate() + "_"+Guid.NewGuid()+".txt";
                    System.IO.FileStream oFileStream = null;
                    oFileStream = new System.IO.FileStream(failPath + "\\" + fileName, System.IO.FileMode.Create);
                    oFileStream.Write(byteStream, 0, byteStream.Length);
                    oFileStream.Close();                    
                }
            }
            catch (FaultException ex)
            {
                USLogError.WriteToFile("Error In GetRemitFile(service)" + ex.Message,ex, Convert.ToString(creditorNo));   
            } 
        }
        public static void createDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }


        public int UpdateAddress(string addressData, int creditorNo)
        {
            try
            {
                OperationResult<int> result = GMSCcx.UpdateAddress(addressData, creditorNo);
                return result.OperationReturnValue;
            }
            catch (FaultException)
            {
                return -1;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}
