﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using US.Common.Web.UI.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Admin;

namespace US.Exceline.GMS.Modules.Shop.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IShopService
    {
        #region Get Sales Point List
        //done
        [OperationContract]
        List<SalePointDC> GetSalesPointList(int branchID, string user);
        #endregion

        #region Manage Shop
        //done
        [OperationContract]
        List<CategoryDC> GetCategories(string type, string user, int branchId);
        //done
        [OperationContract]
        List<CategoryTypeDC> GetCategoryTypes(string name, string user, int branchId);
        //done
        [OperationContract]
        int SaveCategory(CategoryDC category, string user, int branchId);
        #endregion
        //done
        [OperationContract]
        SalePointDC GetSalesPointByMachineName(int branchID, string machineName, string user);
        //done
        [OperationContract]
        ShopSalesPaymentDC GetSalesPaymentList();
        //done
        [OperationContract]
        string GetShopBillSummary(DateTime fromDate, DateTime toDate, int branchId, string user);
        //done
        [OperationContract]
        Dictionary<string, decimal> GetMemberEconomyBalances(string user, int memberId);
        //done
        [OperationContract]
        ShopBillDetailDC GetShopSaleBillDetail(int saleId, int branchId, string user);

        //done
        [OperationContract]
        bool ManageMemberShopAccount(MemberShopAccountDC memberShopAccount, string mode, string user, int branchId, int salePointId);
        //done
        [OperationContract]
        List<ShopSalesItemDC> GetDailySales(string user, DateTime salesDate, int branchId);
        //done
        [OperationContract]
        bool AddShopAccount(MemberShopAccountDC memberShopAccount, string mode, string user, int branchId, int salePoinId);
        //done
        [OperationContract]
        List<WithdrawalDC> GetWithdrawalByMemberId(DateTime startDate, DateTime endDate, WithdrawalTypes type, int memberId, string user);

        #region GetGymSettings
        //done
        [OperationContract]
        string GetGymSettings(int branchId, string user, GymSettingType gymSettingType);
        #endregion

        #region GetSelectedGymSettings
        //done
        [OperationContract]
        Dictionary<string, object> GetSelectedGymSettings(List<string> gymSetColNames, bool isGymSettings, int branchId, string user);
        #endregion

        #region DummyMethodForPassDomainObject
        [OperationContract]
        void DummyMethodForPassDomainObject(ExceGymSettingDC gymSetting);
        #endregion

        #region Payments
        //done
        [OperationContract]
        SaleResultDC AddShopSales(InstallmentDC installment, ShopSalesDC shopSaleDetails, PaymentDetailDC paymentDetails, int memberBranchID, int loggedBranchID, string user, bool printInvoice, string custRoleType);
        #endregion

        #region shop account
        //done
        [OperationContract]
        MemberShopAccountDC GetMemberShopAccounts(int memberId, string user);
        //done
        [OperationContract]
        MemberShopAccountDC GetMemberShopAccountsForEntityType(int memberId, string entityType, string user);
        //done
        [OperationContract]
        PDFPrintResultDC ViewDailySettlementReport(int reconciliationId, decimal countedCashDraft, int dailySettlementId, int salePointId, int branchId, string user, DailySettlementPrintType mode, DateTime startDate, DateTime endDate);
        //done
        [OperationContract]
        DailySettlementDC GetDailySettlements(int branchId, int salePointId, string user);
        //done
        [OperationContract]
        int AddDailySettlements(DailySettlementDC dailySettlement, string user);
        //done
        [OperationContract]
        int SaveWithdrawal(int branchId, WithdrawalDC withDrawal, string user);

        #endregion

        #region Get Entities
        //done
        [OperationContract]
        List<GymEmployeeDC> GetGymEmployees(int branchId, string searchText, bool isActive, string user);

        //done
        [OperationContract]
        List<InstructorDC> GetInstructors(int branchId, string searchText, bool isActive, string user);

        //done
        [OperationContract]
        List<TrainerDC> GetTrainers(int branchId, string searchText, bool isActive, string user);

        //done
        [OperationContract]
        List<ExcelineMemberDC> GetMembers(int branchId, string searchText, int status, MemberSearchType searchType, MemberRole memberRole, string user, int hit);
        #endregion

        #region Manage Vouchers
        //done
        [OperationContract]
        List<VoucherDC> GetVouchersList(int branchid, string user);
        //done
        [OperationContract]
        bool AddVoucher(int branchId, VoucherDC voucher, string user);
        //done
        [OperationContract]
        int EditVoucher(int branchId, VoucherDC voucher, string user);

        #endregion

        #region Get Return Item List
        //done
        [OperationContract]
        List<ReturnItemDC> GetReturnItemList(string user, int branchId);
        #endregion

        #region Add Returned Item
        //done
        [OperationContract]
        int AddReturnedItem(string user, int branchId, ReturnItemDC returnedItem);
        #endregion

        #region Edit Returned Item
        //done
        [OperationContract]
        int EditReturnedItem(string user, int branchId, ReturnItemDC editedReturnItem);
        #endregion

        #region Get Articale
        //done
        [OperationContract]
        List<ArticleDC> GetArticles(int branchId, string user, ArticleTypes categpryType, string keyword, CategoryDC category, bool isActive, bool filterByGym);
        #endregion

        #region Check Member as EMP
        //done
        [OperationContract]
        int CheckEmployeeByEntNo(int entityNO, string user);
        #endregion

        [OperationContract]
        ExcelineMemberDC GetMem();
        //done
        [OperationContract]
        USPUser ValidateUserWithCard(string cardNumber, string user);
        //done
        [OperationContract]
        bool AddEditUserHardwareProfile(int branchId, int hardwareId, string user);
        //done
        [OperationContract]
        int GetSelectedHardwareProfile(int branchId, string user);
        //done
        [OperationContract]
        int SavePointOfSale(int branchId, SalePointDC pointOfSale, string user);
        //done
        [OperationContract]
        bool ValidateUser(string userName, string password);
        //done
        [OperationContract]
        List<DiscountDC> GetDiscountList(int branchId, DiscountType discountType, string user);
        //done
        [OperationContract]
        List<CountryDC> GetCountryDetails(string user);
        //done
        [OperationContract]
        string SaveMember(OrdinaryMemberDC member, string user);
        //done
        [OperationContract]
        string GetSessionValue(string sessionKey, string user);


        //done
        [OperationContract]
        string GetConfigSettings(string type);
        //done
        [OperationContract]
        int AddSessionValue(string sessionKey, string value, string user);
        //done
        [OperationContract]
        int GetReconciliationSession(string sessionKey, int dailySettlementID, string user);

        //done
        [OperationContract]
        List<ExcelineMemberDC> GetMembersForShop(int branchId, string searchText, int status, MemberRole memberRole,
                                                 int hit, string user);
        //done
        [OperationContract]
        int SaveArticle(ArticleDC article,string categoryCode, string user, int branchId);
        //done
        [OperationContract]
        int GetNextGiftVoucherNumber(string seqId, string subSeqId, string user);
        //done
        [OperationContract]
        List<ShopSalesPaymentDC> GetGiftVoucherSaleSummary(DateTime fromDate, DateTime toDate, int branchId, string user);
        //done
        [OperationContract]
        int ValidateGiftVoucherNumber(string voucherNumber,decimal payment, string user);
        //done
        [OperationContract]
        Dictionary<string, string> GetGiftVoucherDetail(string user);

        [OperationContract]
        string GetCityForPostalCode(string postalCode, string user);
        //done
        [OperationContract]
        int SavePostalArea(string user, string postalCode, string postalArea, long population, long houseHold);

        //done
        [OperationContract]
        bool UpdateSettingForUserRoutine(string key, string value, string user);
        //done
        [OperationContract]
        string GetMemberSearchCategory(int branchId, string user);
        //done
        [OperationContract]
        List<ExcelineBranchDC> GetBranches(string user, int branchId);

        //done
        [OperationContract]
        CustomerBasicInfoDC GetMemberBasicInfo(int memberId, string user);
    }
}