﻿using System;
using System.Collections.Generic;
using US.Common.USSAdmin.API;
using US.Common.Web.UI.Core.SystemObjects;
using US.Exceline.GMS.Modules.Admin.API.ManageSystemSettings;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.API;
using System.Linq;
using US.GMS.Core.ResultNotifications;
using US.Common.Logging.API;
using US.Exceline.GMS.Modules.Shop.API.InventoryManagement;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.Exceline.GMS.Modules.Admin.API.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.SystemObjects;
using US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership;
using US.Exceline.GMS.Modules.Shop.API.ManageShop;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.Utils;
using US.Payment.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Admin;
using System.Configuration;
using US.Communication.Core.SystemCore.DomainObjects;
using US.Communication.API;
using US.Common.Notification.API;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using SessionManager;
using US.Payment.Core.Enums;

namespace US.Exceline.GMS.Modules.Shop.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class ShopService : IShopService
    {

        public List<SalePointDC> GetSalesPointList(int branchID, string user)
        {
            OperationResult<List<SalePointDC>> result = GMSShopManager.GetSalesPointList(branchID, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<SalePointDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #region Get Categories
        public List<CategoryDC> GetCategories(string type, string user, int branchId)
        {
            OperationResult<List<CategoryDC>> result = GMSCategory.GetCategoriesByType(type, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        #region Get Category Types
        public List<CategoryTypeDC> GetCategoryTypes(string name, string user, int branchId)
        {
            OperationResult<List<CategoryTypeDC>> result = GMSCategory.GetCategoryTypes(name, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryTypeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        #region Save Category
        public int SaveCategory(CategoryDC category, string user, int branchId)
        {
            OperationResult<int> result = GMSCategory.SaveCategory(category, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        //OperationResult

        public SalePointDC GetSalesPointByMachineName(int branchID, string machineName, string user)
        {
            OperationResult<SalePointDC> result = GMSShopManager.GetSalesPointByMachineName(branchID, machineName, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new SalePointDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public ShopSalesPaymentDC GetSalesPaymentList()
        {
            return new ShopSalesPaymentDC();
        }

        public string GetShopBillSummary(DateTime fromDate, DateTime toDate, int branchId, string user)
        {
            OperationResult<List<ShopSalesPaymentDC>> result = GMSShopManager.GetShopBillSummary(fromDate, toDate, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return XMLUtils.SerializeDataContractObjectToXML(new List<ShopSalesPaymentDC>());
            }
            else
            {
                return XMLUtils.SerializeDataContractObjectToXML(result.OperationReturnValue);
            }
        }

        public bool ManageMemberShopAccount(MemberShopAccountDC memberShopAccount, string mode, string user, int branchId, int salePointId)
        {
            var result = GMSMember.ManageMemberShopAccount(memberShopAccount, mode, user, salePointId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public ShopBillDetailDC GetShopSaleBillDetail(int saleId, int branchId, string user)
        {
            OperationResult<ShopBillDetailDC> result = GMSShopManager.GetShopSaleBillDetail(saleId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new ShopBillDetailDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool AddShopAccount(MemberShopAccountDC memberShopAccount, string mode, string user, int branchId, int salePoinId)
        {
            var result = GMSMember.ManageMemberShopAccount(memberShopAccount, mode, user, branchId, salePoinId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<WithdrawalDC> GetWithdrawalByMemberId(DateTime startDate, DateTime endDate, WithdrawalTypes type, int memberId, string user)
        {
            OperationResult<List<WithdrawalDC>> result = GMSShopManager.GetWithdrawalByMemberId(startDate, endDate, type, memberId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<WithdrawalDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ShopSalesItemDC> GetDailySales(string user, DateTime salesDate, int branchId)
        {
            OperationResult<List<ShopSalesItemDC>> result = GMSShopManager.GetDailySales(salesDate, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ShopSalesItemDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #region Payments

        public SaleResultDC AddShopSales(InstallmentDC installment, ShopSalesDC shopSaleDetails, PaymentDetailDC paymentDetails, int memberBranchID, int loggedBranchId, string user, bool printInvoice,string custRoleType)
        {
            PDFPrintResultDC pdfResult = new PDFPrintResultDC();
            OperationResult<SaleResultDC> result = GMSShop.AddShopSales(installment, shopSaleDetails, paymentDetails, memberBranchID, loggedBranchId, user, ExceConnectionManager.GetGymCode(user), false, custRoleType);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                pdfResult.ErrorStateId = result.OperationReturnValue.AritemNo;
                result.OperationReturnValue.PrintResult = pdfResult;
                return result.OperationReturnValue;
            }
            try
            {
                if (paymentDetails != null && paymentDetails.PayModes != null && result.OperationReturnValue.SaleStatus  == SaleErrors.SUCCESS)
                {
                    if (paymentDetails.PayModes.FirstOrDefault(x => x.PaymentTypeCode == "EMAILSMSINVOICE") != null)
                    {
                        try
                        {
                            var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"].ToString();
                            var dataDictionary = new Dictionary<string, string>() { { "itemID", result.OperationReturnValue.AritemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                            pdfResult = GetFilePathForPdf(appUrl, "PDF", user, memberBranchID, dataDictionary);
                            SetPDFFileParth(result.OperationReturnValue.AritemNo, pdfResult.FileParth, "INV", memberBranchID, user);

                            var InvoiceDetails = GMSInvoice.GetInvoiceDetails(result.OperationReturnValue.AritemNo, memberBranchID, ExceConnectionManager.GetGymCode(user));

                            if (InvoiceDetails.ErrorOccured)
                            {
                                foreach (NotificationMessage message in InvoiceDetails.Notifications)
                                {
                                    USLogError.WriteToFile(message.Message, new Exception(), user);
                                }
                                pdfResult.ErrorStateId = -4;
                                result.OperationReturnValue.PrintResult = pdfResult;
                                return result.OperationReturnValue;
                            }
                            ExcelineInvoiceDetailDC invoice = InvoiceDetails.OperationReturnValue;

                            // create parameter string for notification
                            Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                            paraList.Add(TemplateFieldEnum.INVOICENO, invoice.InvoiceNo.ToString());
                            paraList.Add(TemplateFieldEnum.DUEDATE, invoice.InvoiceDueDate.ToString());
                            paraList.Add(TemplateFieldEnum.AMOUNT, invoice.InvoiceAmount.ToString());
                            paraList.Add(TemplateFieldEnum.KID, invoice.BasicDetails.KID.ToString());
                            paraList.Add(TemplateFieldEnum.GYMACCOUNT, invoice.OtherDetails.BranchAccountNo);
                            paraList.Add(TemplateFieldEnum.GYMNAME, invoice.OtherDetails.BranchName.ToString());

                            // create common notification
                            USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                            commonNotification.IsTemplateNotification = true;
                            commonNotification.ParameterString = paraList;
                            commonNotification.BranchID = memberBranchID;
                            commonNotification.CreatedUser = user;
                            commonNotification.Role = "MEM";
                            commonNotification.NotificationType = NotificationTypeEnum.Messages;
                            commonNotification.Severity = NotificationSeverityEnum.Minor;
                            commonNotification.MemberID = result.OperationReturnValue.MemberId;
                            commonNotification.Status = NotificationStatusEnum.New;

                            // FOR SMS **************************************************************
                            commonNotification.Type = TextTemplateEnum.SMSINVOICE;
                            commonNotification.Method = NotificationMethodType.SMS;
                            commonNotification.Title = "INVOICE SMS";

                            //add SMS notification into notification tables
                            USImportResult<int> SMSresult = NotificationAPI.AddNotification(commonNotification);

                            if (result.ErrorOccured)
                            {
                                foreach (USNotificationMessage message in SMSresult.NotificationMessages)
                                {
                                    if (message.ErrorLevel == ErrorLevels.Error)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                    }
                                }
                                pdfResult.ErrorStateId = -4;
                            }
                            else
                            {
                                string application = ConfigurationManager.AppSettings["USC_Notification_SMS"].ToString();
                                string outputFormat = "SMS";

                                try
                                {
                                    // send SMS through the USC API
                                    IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), SMSresult.MethodReturnValue.ToString());

                                    NotificationStatusEnum status = NotificationStatusEnum.Attended;
                                    if (smsStatus != null)
                                        status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                    var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                                    // update status in notification table
                                    USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(SMSresult.MethodReturnValue, user, status, statusMessage);

                                    if (updateResult.ErrorOccured)
                                    {
                                        foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                        {
                                            if (message.ErrorLevel == ErrorLevels.Error)
                                            {
                                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                            }
                                        }
                                        pdfResult.ErrorStateId = -4;
                                    }
                                }
                                catch (Exception e)
                                {
                                    USLogError.WriteToFile(e.Message, new Exception(), user);
                                    pdfResult.ErrorStateId = -4;
                                }
                            }

                            // FOR EMAIL **************************************************************
                            commonNotification.Type = TextTemplateEnum.EMAILINVOICE;
                            commonNotification.Method = NotificationMethodType.EMAIL;
                            commonNotification.AttachmentFileParth = pdfResult.FileParth;
                            commonNotification.Title = "INVOICE EMAIL";

                            //add EMAIL notification into notification tables
                            USImportResult<int> EMAILresult = NotificationAPI.AddNotification(commonNotification);

                            if (result.ErrorOccured)
                            {
                                foreach (USNotificationMessage message in EMAILresult.NotificationMessages)
                                {
                                    if (message.ErrorLevel == ErrorLevels.Error)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                    }
                                }
                                pdfResult.ErrorStateId = -4;
                            }
                            else
                            {
                                string application = ConfigurationManager.AppSettings["USC_Notification_Email"].ToString();
                                string outputFormat = "EMAIL";

                                try
                                {
                                    // send EMAIL through the USC API
                                    IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), EMAILresult.MethodReturnValue.ToString());

                                    NotificationStatusEnum status = NotificationStatusEnum.Attended;

                                    if (emailStatus != null)
                                        status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                    var statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                                    // update status in notification table
                                    USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(EMAILresult.MethodReturnValue, user, status, statusMessage);

                                    if (updateResult.ErrorOccured)
                                    {
                                        foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                        {
                                            if (message.ErrorLevel == ErrorLevels.Error)
                                            {
                                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                            }
                                        }
                                        pdfResult.ErrorStateId = -4;
                                    }
                                }
                                catch (Exception e)
                                {
                                    USLogError.WriteToFile(e.Message, new Exception(), user);
                                    pdfResult.ErrorStateId = -4;
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            USLogError.WriteToFile(e.Message, new Exception(), user);
                            pdfResult.ErrorStateId = -4;
                        }
                    }

                    if (paymentDetails.PayModes.Where(x => x.PaymentTypeCode == "INVOICE").FirstOrDefault() != null && printInvoice)
                    {
                        var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"].ToString();
                        var dataDictionary = new Dictionary<string, string>() { { "itemID", result.OperationReturnValue.AritemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                        pdfResult = GetFilePathForPdf(appUrl, "PDF", user, memberBranchID, dataDictionary);
                        SetPDFFileParth(result.OperationReturnValue.AritemNo, pdfResult.FileParth, "INV", memberBranchID, user);
                        return result.OperationReturnValue;
                    }
                }
            }
            catch (Exception e)
            {
                USLogError.WriteToFile(e.Message, new Exception(), user);
                pdfResult.ErrorStateId = -4;
            }
            return result.OperationReturnValue;
        }

        #endregion

        #region Shop Account

        public MemberShopAccountDC GetMemberShopAccounts(int memberId, string user)
        {
            OperationResult<MemberShopAccountDC> result = GMSMember.GetMemberShopAccounts(memberId, 1, true, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public MemberShopAccountDC GetMemberShopAccountsForEntityType(int memberId, string entityType, string user)
        {
            OperationResult<MemberShopAccountDC> result = GMSMember.GetMemberShopAccounts(memberId, entityType, 1, true, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public DailySettlementDC GetDailySettlements(int branchId, int salePointId, string user)
        {
            OperationResult<DailySettlementDC> result = GMSShop.GetDailySettlements(salePointId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }

        }

        public int AddDailySettlements(DailySettlementDC dailySettlement, string user)
        {
            OperationResult<int> result = GMSShop.AddDailySettlements(dailySettlement, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            return result.OperationReturnValue;
        }


        #endregion

        #region Get Entities


        public List<GymEmployeeDC> GetGymEmployees(int branchId, string searchText, bool isActive, string user)
        {
            OperationResult<List<GymEmployeeDC>> result = GMSGymEmployee.GetGymEmployees(branchId, searchText, isActive, -1,ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<GymEmployeeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<InstructorDC> GetInstructors(int branchId, string searchText, bool isActive, string user)
        {
            OperationResult<List<InstructorDC>> result = GMSInstructor.GetInstructors(branchId, searchText, isActive, -1, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InstructorDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<TrainerDC> GetTrainers(int branchId, string searchText, bool isActive, string user)
        {
            OperationResult<List<TrainerDC>> result = GMSTrainer.GetTrainers(branchId, searchText, isActive, -1, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<TrainerDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineMemberDC> GetMembers(int branchId, string searchText, int status, MemberSearchType searchType, MemberRole memberRole, string user, int hit)
        {
            //int status = 1;  //active member status=1;
            OperationResult<List<ExcelineMemberDC>> result = GMSManageMembership.GetMembers(branchId, searchText, status, searchType, memberRole, user, ExceConnectionManager.GetGymCode(user), hit, false, false, string.Empty);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<ExcelineMemberDC> GetMembersForShop(int branchId, string searchText, int status, MemberRole memberRole, int hit, string user)
        {
            OperationResult<List<ExcelineMemberDC>> result = GMSMember.GetMembersForShop(branchId, searchText, status, memberRole, hit, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
               return  new List<ExcelineMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveArticle(ArticleDC article, string categoryCode, string user, int branchId)
        {
            var gymCode = ExceConnectionManager.GetGymCode(user);

            var giftVoucherCategory = GMSSystemSettings.GetCategoryByCode(categoryCode, gymCode).OperationReturnValue;
            article.CategoryId = giftVoucherCategory.Id;
            var result = GMSShopManager.SaveGiftVoucherArticle(article, gymCode);
                                                            
            if (!result.ErrorOccured)
            {
                return result.OperationReturnValue;
            }
            else
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
        }

        #endregion

        #region Get Vouchers
        public List<VoucherDC> GetVouchersList(int branchid, string user)
        {
            OperationResult<List<VoucherDC>> result = GMSShopManager.GetVouchersList(branchid, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<VoucherDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        #region Add Voucher
        public bool AddVoucher(int branchId, VoucherDC voucher, string user)
        {
            OperationResult<bool> result = GMSShopManager.AddVoucher(branchId, voucher, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion


        public List<DiscountDC> GetDiscountList(int branchId, DiscountType discountType, string user)
        {
            OperationResult<List<DiscountDC>> result = GMSManageMembership.GetDiscountList(branchId, discountType, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<DiscountDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public Dictionary<string, decimal> GetMemberEconomyBalances(string user, int memberId)
        {
            var result = GMSShopManager.GetMemberEconomyBalances(ExceConnectionManager.GetGymCode(user), memberId);

            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new Dictionary<string, decimal>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #region Edit Voucher
        public int EditVoucher(int branchId, VoucherDC voucher, string user)
        {
            OperationResult<int> result = GMSShopManager.EditVoucher(branchId, voucher, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        #region Get Return Item List
        public List<ReturnItemDC> GetReturnItemList(string user, int branchId)
        {
            OperationResult<List<ReturnItemDC>> result = GMSShopManager.GetReturnItemList(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ReturnItemDC>();
            }
            else
                return result.OperationReturnValue;
        }
        #endregion

        #region Add Returned Item
        public int AddReturnedItem(string user, int branchId, ReturnItemDC returnedItem)
        {
            OperationResult<int> result = GMSShopManager.AddReturnedItem(branchId, returnedItem, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return 0;
            }
            else
                return result.OperationReturnValue;
        }
        #endregion

        #region EditReturnedItem
        public int EditReturnedItem(string user, int branchId, ReturnItemDC editedReturnItem)
        {
            OperationResult<int> result = GMSShopManager.EditReturnedItem(branchId, editedReturnItem, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return 0;
            }
            else
                return result.OperationReturnValue;
        }
        #endregion

        #region Get Articles
        public List<ArticleDC> GetArticles(int branchId, string user, ArticleTypes categpryType, string keyword, CategoryDC category, bool isActive, bool isFilerByGym)
        {
            OperationResult<List<ArticleDC>> result = GMSArticle.GetArticles(branchId, user, categpryType, keyword, category, -1, false, isActive, ExceConnectionManager.GetGymCode(user), isFilerByGym);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ArticleDC>();
            }
           return result.OperationReturnValue;
        }
        #endregion

        #region Check Member as EMP
        public int CheckEmployeeByEntNo(int entityNO, string user)
        {
            OperationResult<int> result = GMSShopManager.CheckEmployeeByEntNo(entityNO, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        public ExcelineMemberDC GetMem()
        {
            return null;
        }

        public USPUser ValidateUserWithCard(string cardNumber, string user)
        {
            OperationResultValue<USPUser> result = AdminUserSettings.ValidateUserWithCard(cardNumber, user);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), cardNumber);
                }
                return new USPUser();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public bool ValidateUser(string userName, string password)
        {
            OperationResultValue<bool> result = AdminUserSettings.ValidateUser(userName, password);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), userName);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SavePointOfSale(int branchId, SalePointDC pointOfSale, string user)
        {
            OperationResult<int> result = GMSShopManager.SavePointOfSale(branchId, pointOfSale, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }




        #region GetGymSettings
        public string GetGymSettings(int branchId, string user, GymSettingType gymSettingType)
        {
            OperationResult<string> result = GMSManageGymSetting.GetGymSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        #region GetSelectedGymSettings
        public Dictionary<string, object> GetSelectedGymSettings(List<string> gymSetColNames, bool isGymSettings, int branchId, string user)
        {
            OperationResult<Dictionary<string, object>> result = GMSManageGymSetting.GetSelectedGymSettings(gymSetColNames, isGymSettings, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        #region DummyMethodForPassDomainObject
        public void DummyMethodForPassDomainObject(ExceGymSettingDC gymSetting)
        { }
        #endregion

        public bool AddEditUserHardwareProfile(int branchId, int hardwareId, string user)
        {
            OperationResult<bool> result = GMSShopManager.AddEditUserHardwareProfile(branchId, hardwareId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int GetSelectedHardwareProfile(int branchId, string user)
        {
            OperationResult<int> result = GMSShopManager.GetSelectedHardwareProfile(branchId, user, ExceConnectionManager.GetGymCode(user));

            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CountryDC> GetCountryDetails(string user)
        {
            var result = GMSMember.GetCountryDetails(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CountryDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string SaveMember(OrdinaryMemberDC member, string user)
        {
            OperationResult<string> result = GMSManageMembership.SaveMember(member, String.Empty, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return "-1";
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        //----------------------------------------------------------Start Of USC Integration---------------------------
        public PDFPrintResultDC ViewDailySettlementReport(int reconciliationId, decimal countedCashDraft, int dailySettlementId, int salePointId, int branchId, string user, DailySettlementPrintType mode, DateTime startDate, DateTime endDate)
        {
            PDFPrintResultDC PDFResult = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_DailySettlement_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "dailySettlementId", dailySettlementId.ToString() }, { "reconciliationId", reconciliationId.ToString() }, { "salePointId", salePointId.ToString() }, { "branchId", branchId.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } ,
                 { "mode", mode.ToString() }, { "startDate", startDate.ToString("yyyy-MM-dd HH:mm:ss") } , { "endDate", endDate.ToString("yyyy-MM-dd HH:mm:ss") } , { "countedcashdraft", countedCashDraft.ToString() } 
 
                };
                PDFResult = GetFilePathForPdf(appUrl, "PDF", user, branchId, dataDictionary);
                return PDFResult;
            }
            catch (Exception)
            {
                return PDFResult;
            }
        }

        private void SetPDFFileParth(int id, string fileParth, string flag, int branchId, string user)
        {
            GMSManageMembership.SetPDFFileParth(id, fileParth, flag, branchId, ExceConnectionManager.GetGymCode(user), user);
        }

        private PDFPrintResultDC GetFilePathForPdf(string application, string OutputPlugging, string user, int branchId, Dictionary<string, string> dataDictionary)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var list = ExcecuteWithUSC(application, OutputPlugging, dataDictionary, ExceConnectionManager.GetGymCode(user), user);
                foreach (var item in list)
                {
                    var Message = item.Messages.Where(mssge => mssge.Header.ToLower().Equals("filepath")).First();
                    if (Message != null)
                    {
                        result.FileParth = Message.Message;
                        break;
                    }
                }

                // for get visibility on browser.
                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
            }
            return result;
        }

        private List<IProcessingStatus> ExcecuteWithUSC(string application, string OutputPlugging, Dictionary<string, string> dataDictionary, string gymCode, string user)
        {
            List<IProcessingStatus> lists = null;
            try
            {
                lists = TemplateManager.ExecuteTemplate(dataDictionary, application, OutputPlugging, gymCode);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
                throw ex;
            }
            return lists;
        }
        //----------------------------------------------------------End Of USC Integration---------------------------

        public int SaveWithdrawal(int branchId, WithdrawalDC withDrawal, string user)
        {
            OperationResult<int> result = GMSShopManager.SaveWithdrawal(branchId, withDrawal, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string GetSessionValue(string sessionKey, string user)
        {
            return Session.GetSession(sessionKey, ExceConnectionManager.GetGymCode(user));
        }

        public string GetConfigSettings(string type)
        {
            switch (type)
            {
                case "RPT":
                    return ConfigurationManager.AppSettings["ReportViewerURL"].ToString();
            }
            return string.Empty;
        }

        public int AddSessionValue(string sessionKey, string value, string user)
        {
            return Session.AddSessionValue(sessionKey, value, ExceConnectionManager.GetGymCode(user));
        }

        public int GetReconciliationSession(string sessionKey, int dailySettlementID, string user)
        {
            return Session.GetReconciliationSession(sessionKey, dailySettlementID, user, ExceConnectionManager.GetGymCode(user));
        }


        public int GetNextGiftVoucherNumber(string seqId, string subSeqId, string user)
        {
            var result = GMSShopManager.GetNextGiftVoucherNumber(seqId, subSeqId, ExceConnectionManager.GetGymCode(user));

            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<ShopSalesPaymentDC> GetGiftVoucherSaleSummary(DateTime fromDate, DateTime toDate, int branchId, string user)
        {
            var result = GMSShopManager.GetGiftVoucherSaleSummary(fromDate, toDate, branchId,
                                                                  ExceConnectionManager.GetGymCode(user));

            if (!result.ErrorOccured)
            {
                return result.OperationReturnValue;
            }
            else
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
        }

        public int ValidateGiftVoucherNumber(string voucherNumber,decimal payment, string user)
        {
            var result = GMSShopManager.ValidateGiftVoucherNumber(voucherNumber,payment, ExceConnectionManager.GetGymCode(user));

            if (!result.ErrorOccured)
            {
                return result.OperationReturnValue;
            }
            else
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -3;
            }
        }

        public Dictionary<string, string> GetGiftVoucherDetail(string user)
        {
            var result = GMSShopManager.GetGiftVoucherDetail(ExceConnectionManager.GetGymCode(user));

            if (!result.ErrorOccured)
            {
                return result.OperationReturnValue;
            }
            else
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
        }

        public string GetCityForPostalCode(string postalCode, string user)
        {
            var result = GMSUtility.GetCityByPostalCode(postalCode, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return string.Empty;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SavePostalArea(string user, string postalCode, string postalArea, long population, long houseHold)
        {
            var result = GMSMember.SavePostalArea(user, postalCode, postalArea, population, houseHold, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateSettingForUserRoutine(string key, string value, string user)
        {
            var result = GMSUser.UpdateSettingForUserRoutine(key, value, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string GetMemberSearchCategory(int branchId, string user)
        {
            OperationResult<string> result = GMSManageMembership.GetMemberSearchCategory(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return string.Empty;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<ExcelineBranchDC> GetBranches(string user, int branchId)
        {
            OperationResult<List<ExcelineBranchDC>> result = GMSSystemSettings.GetBranches(user, ExceConnectionManager.GetGymCode(user), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineBranchDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public CustomerBasicInfoDC GetMemberBasicInfo(int memberId, string user)
        {
            OperationResult<CustomerBasicInfoDC> result = GMSManageMembership.GetMemberBasicInfo(memberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
    }
}
