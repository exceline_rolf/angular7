﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.Common.Web.UI.Core.SystemObjects.ScheduleManagement;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class GetUserSelectedBranchAction : USDBActionBase<List<UserBranchSelected>>
    {
        private string _userName = string.Empty;
        private string _loggedUser = string.Empty;

        public GetUserSelectedBranchAction(string userName,string loggedUser)
        {
            this._userName = userName;
            this._loggedUser = loggedUser;
            OverwriteUser(_loggedUser);
        }

        protected override List<UserBranchSelected> Body(DbConnection connection)
        {
            List<UserBranchSelected> selectedBranchList = new List<UserBranchSelected>();

            try
            {
                string storedProcedure = "USP_AUT_GetSelectedBranches";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _userName));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    UserBranchSelected branch = new UserBranchSelected();
                    if (!string.IsNullOrEmpty(reader["Id"].ToString()))
                    {
                        branch.Id = Convert.ToInt32(reader["Id"]);
                    }

                    branch.BranchId = Convert.ToInt32(reader["BranchId"]);
                    branch.UserId = Convert.ToInt32(reader["UserId"]);
                    branch.BranchName = reader["BranchName"].ToString();
                    branch.IsDeleted = false;

                    selectedBranchList.Add(branch);
                }
            }
            catch
            {
                throw;
            }
            return selectedBranchList;
        }
    }
}
