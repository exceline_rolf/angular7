﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Web.UI.Core.SystemObjects;
using US_DataAccess;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class GetHardwareProfilesAction : USDBActionBase<List<ExceHardwareProfile>>
    {
        private int _branchId;
         
        
        public GetHardwareProfilesAction(int branchId, string user)
        {
            _branchId = branchId;
            OverwriteUser(user);
        }

        protected override List<ExceHardwareProfile> Body(System.Data.Common.DbConnection connection)
        {
            List<ExceHardwareProfile> hardwareProfileList = new List<ExceHardwareProfile>();            
            string storedProcedureName = "USExceGMSAdminGetHardwareProfiles";

             try
             {
                 DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                 cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32 , _branchId));
                 DbDataReader reader = cmd.ExecuteReader();

                 while (reader.Read())
                 {
                     ExceHardwareProfile hardwareProfile = new ExceHardwareProfile();
                     hardwareProfile.Id = Convert.ToInt32(reader["ID"]);
                     hardwareProfile.Name = Convert.ToString(reader["Name"]);
                     hardwareProfileList.Add(hardwareProfile);
                 }
                 reader.Close();
             }
             catch
             {
                 throw;
             }
             return hardwareProfileList;
         }
    }
}
