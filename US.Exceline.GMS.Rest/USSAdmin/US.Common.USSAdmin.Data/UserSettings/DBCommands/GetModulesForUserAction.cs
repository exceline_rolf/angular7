﻿using System;
using System.Collections.Generic;
using US.Common.Web.UI.Core.SystemObjects;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    class GetModulesForUserAction : USDBActionBase<List<USPModule>>
    {
        private string _userName = string.Empty;

        public GetModulesForUserAction(string userName)
        {
            _userName = userName;
            OverwriteUser(_userName);
        }

        protected override List<USPModule> Body(System.Data.Common.DbConnection connection)
        {
            List<USPModule> moduleList = new List<USPModule>();

            try
            {
                string spName = "USP_AUT_GetModulesForUser";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@userName", DbType.String, _userName));

                DbDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    USPModule module = new USPModule();
                    module.DisplayName = Convert.ToString(reader["DisplayName"]);
                    module.ID = Convert.ToInt32(reader["ID"]);
                    module.Name = Convert.ToString(reader["Name"]);
                    module.ModuleHome = Convert.ToString(reader["Namespace"]);
                    module.ModuleHomeURL = Convert.ToString(reader["ModuleHomeURL"]);
                    module.ThumbnailImage = Convert.ToString(reader["ThumbnailImage"]);
                    if (reader["COLOR"] != null)
                        module.ModuleColor = Convert.ToString(reader["COLOR"]);
                    module.ModulePackage = Convert.ToString(reader["ModulePackage"]);
                    moduleList.Add(module);
                }
            }
            catch
            {
                throw;
            }
            return moduleList;
        }



    }
}
