﻿#region File Header
// --------------------------------------------------------------------------
// Copyright(c) <2010> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USAR
// Project Name      : US.Payment.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : 29/12/2011 HH:MM  PM
// --------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using US_DataAccess;
using US.Common.Web.UI.Core.SystemObjects;
using System.Data.Common;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class GetModulesAction : USDBActionBase<List<USPModule>>
    {
        private string _loggedUser = string.Empty;
        public GetModulesAction(string loggedUser)
        {
            _loggedUser = loggedUser;
            OverwriteUser(_loggedUser);
        }

        protected override List<USPModule> Body(System.Data.Common.DbConnection connection)
        {
            List<USPModule> moduleList = new List<USPModule>();

            try
            {
                string spName = "USP_AUT_GetModules ";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);                

                DbDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    USPModule module = new USPModule();
                    module.DisplayName = Convert.ToString(reader["DisplayName"]);
                    module.ID = Convert.ToInt32(reader["ID"]);
                    module.Name = Convert.ToString(reader["Name"]);
                    module.ModuleHome = Convert.ToString(reader["Namespace"]);
                    module.ThumbnailImage = Convert.ToString(reader["ThumbnailImage"]);
                    moduleList.Add(module);
                }
            }
            catch
            {
                throw;
            }
            return moduleList;
        }

    }
}
