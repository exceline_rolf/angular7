﻿using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    class EditUSPUserBasicInfoAction : USDBActionBase<bool>
    {
        private USPUser _uspUser;
        private bool _isPasswdChange;
        private string _loggedUser = string.Empty;
        public EditUSPUserBasicInfoAction(bool isPasswdChange, USPUser uspUser, string loggedUser)
        {
            this._uspUser = uspUser;
            this._isPasswdChange = isPasswdChange;
            _loggedUser = loggedUser;
            OverwriteUser(_loggedUser);
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool reader = false; 

            try
            {
                string spName = "USP_AUT_EditUserBasicInfo";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _uspUser.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DisplayName", DbType.String, _uspUser.DisplayName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Email", DbType.String, _uspUser.email));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPasswdChange", DbType.Boolean, _isPasswdChange));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Passwd", DbType.String, _uspUser.passowrd));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsLoginFirstTime", DbType.Boolean, _uspUser.IsLoginFirstTime));
                
                reader =Convert.ToBoolean( cmd.ExecuteScalar());

                
            }
            catch
            {
                throw;
            }
            return reader;
        }

       

    }
}
