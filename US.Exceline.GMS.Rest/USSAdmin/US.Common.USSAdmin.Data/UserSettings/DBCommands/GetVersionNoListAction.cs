﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Web.UI.Core.SystemObjects.ScheduleManagement;
using US_DataAccess;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class GetVersionNoListAction :USDBActionBase<List<string>>
    {
        private string _gymCode = string.Empty;

        public GetVersionNoListAction(string gymCode)
        {
            _gymCode = gymCode;
        }

        protected override List<string> Body(DbConnection connection)
        {
            List<string> versionNoList = new List<string>();
            try
            {
                string storedProcedure = "ExceWorkStationGetVersionNoList";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GymCode", DbType.String, _gymCode));
                DbDataReader reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    versionNoList.Add(Convert.ToString(reader["AppVersionNo"]));
                    versionNoList.Add(Convert.ToString(reader["DBVersionNo"]));
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return versionNoList;
        }
    }
}
