﻿using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class ValidateUserWithCardAction : USDBActionBase<USPUser>
    {
        private string _cardNumber = string.Empty;
        public ValidateUserWithCardAction(string cardNumber,string user)
        {
            this._cardNumber = cardNumber;
            OverwriteUser(user);
        }

        protected override USPUser Body(DbConnection connection)
        {
            USPUser user = new USPUser();

            try
            {
                string spName = "USP_AUT_ValidateUserWithCard";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@cardNumber", DbType.String, _cardNumber));

                DbDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["ID"] != null)
                        user.Id = Convert.ToInt32(reader["ID"]);
                    if (reader["UserName"] != null)
                        user.UserName = Convert.ToString(reader["UserName"]).Trim();
                    if (reader["DisplayName"] != null)
                        user.DisplayName = Convert.ToString(reader["DisplayName"]).Trim();

                }

            }
            catch
            {
                throw;
            }
            return user;
        }
    }
}
