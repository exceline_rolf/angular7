﻿using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using US.Common.Web.UI.Core.SystemObjects;
using US.Common.USSAdmin.Data.UserSettings.DBCommands;
using US_DataAccess;
using US.Common.Web.UI.Core.SystemObjects.ScheduleManagement;
using System;
using US.GMS.Core.SystemObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands;

namespace US.Common.USSAdmin.Data.UserSettings
{
    public class UserSettingsManager
    {

        #region UserLogin
        public static List<USPUserProfile> GetUserProfileInfo(string userName)
        {
            GetUserProfileAction command = new GetUserProfileAction(userName);
            return command.Execute(EnumDatabase.USP);
        }

        public static string ValidateUser(string username, string password)
        {
            ValidateUserAction command = new ValidateUserAction(username, password);
            return command.Execute(EnumDatabase.USP);
        }

        public static List<string> GetVersionNoList(string gymCode)
        {
            GetVersionNoListAction command = new GetVersionNoListAction(gymCode);
            return command.Execute(EnumDatabase.WorkStation);
        }

        public static USPUser ValidateUserWithCard(string cardNumber,string user)
        {
            ValidateUserWithCardAction command = new ValidateUserWithCardAction(cardNumber, user);
            return command.Execute(EnumDatabase.USP);
        }

        public static USPUser GetUserInfo(string user)
        {
            GetUserInfoAction getUserInfoAction = new GetUserInfoAction(user);
            return getUserInfoAction.Execute(EnumDatabase.USP);
        }

        public static List<USPModule> GetModulesListForUser(string user)
        {
            GetModulesForUserAction getModuleListAction = new GetModulesForUserAction(user);
            return getModuleListAction.Execute(EnumDatabase.USP);
        }

        public static List<USPFeature> GetFeaturesForUser(string user)
        {
            GetFeaturesForUserAction getFeaturesAction = new GetFeaturesForUserAction(user);
            return getFeaturesAction.Execute(EnumDatabase.USP);
        }

        public static List<USPOperation> GetOperationsForUser(string user)
        {
            GetOperationsForUserAction getOperaionsAction = new GetOperationsForUserAction(user);
            return getOperaionsAction.Execute(EnumDatabase.USP);
        }

        public static bool EditUSPUserBasicInfo(bool isPasswdChange, USPUser uspUser, string loggedUser)
        {
            EditUSPUserBasicInfoAction action = new EditUSPUserBasicInfoAction(isPasswdChange, uspUser, loggedUser);
            return action.Execute(EnumDatabase.USP);
        }

        #endregion

        /// <summary>
        /// Execute Data action class to get USPUsers for Admin purposes
        /// </summary>
        /// <returns></returns>
        public static List<USPUserTemp> GetUSPUsers(string searchText, bool isActive, string loggedUser)
        {
            try
           {
                USPUsersSearchAction command = new USPUsersSearchAction(searchText, isActive, loggedUser);
                return command.Execute(EnumDatabase.USP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Execute Add, Edit USPUsers action class
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static int AddEditUSPUsers(UspUsersExtended user, List<UserBranchSelected> branchesList, string loggedUser)
        {
            try
            {
                AddEditUSPUserAction command = new AddEditUSPUserAction(user, branchesList, loggedUser);
                return command.Execute(EnumDatabase.USP);
            }
            catch
            {
                throw;
            }

        }

      
        /// <summary>
        /// Execute Data action class to get USPRoles for Admin purposes
        /// </summary>
        /// <returns></returns>
        public static List<USPRoleTemp> GetUSPRoles(string searchText, bool isActive, string loggedUser)
        {

            try
            {
                USPRolesSearchAction command = new USPRolesSearchAction(searchText, isActive, loggedUser);
                return command.Execute(EnumDatabase.USP);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Execute Data Action class which Add,Edit USPRole
        /// </summary>
        /// <param name="role"></param>
        /// <param name="moduleList"></param>
        /// <param name="featureList"></param>
        /// <param name="operationList"></param>
        /// <returns></returns>
        public static int AddEditUSPRole(USPRole role, string loggedUser)
        {
            try
            {
                AddEditUSPRoleAction action = new AddEditUSPRoleAction(role, loggedUser);
                return action.Execute(EnumDatabase.USP);

            }
            catch
            {
                throw;
            }

        }


        public static int SaveSubOperations(List<USPSubOperation> subOperations, int roleId, string loggedUser)
        {
            var result = -1;
            try
            {
                foreach (var uspSubOperation in subOperations)
                {
                    SaveSubOperations action = new SaveSubOperations(roleId, uspSubOperation, loggedUser);
                   result=  action.Execute(EnumDatabase.USP);
                }
            }
            catch
            {
                throw;
            }
            return result;
        }

        //public static int GetModulesFeaturesOperations()
        //{
        //    try
        //    {
        //        GetModulesFeaturesOperationsAction action = new GetModulesFeaturesOperationsAction();
        //        return action.Execute(EnumDatabase.USP);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        /// <summary>
        /// Action class to Get all the modules in the system
        /// </summary>
        /// <returns></returns>
        public static List<USPModule> GetModules(string loggedUser)
        {

            try
            {
                GetModulesAction action = new GetModulesAction(loggedUser);
                return action.Execute(EnumDatabase.USP);

            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Action class to Get all the Features in the System
        /// </summary>
        /// <returns></returns>
        public static List<USPFeature> GetFeatures(string loggedUser)
        {
            try
            {
                GetFeaturesAction action = new GetFeaturesAction(loggedUser);
                return action.Execute(EnumDatabase.USP);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Action class to get all the Operations in the Sytem
        /// </summary>
        /// <returns></returns>
        public static List<USPOperation> GetOperations(string loggedUser)
        {
            try
            {
                GetOperationsAction action = new GetOperationsAction(loggedUser);
                return action.Execute(EnumDatabase.USP);
            }
            catch
            {
                throw;
            }
        }

        public static List<UserBranchSelected> GetUserSelectedBranches(string userName, string loggedUser)
        {
            
            try
            {
                GetUserSelectedBranchAction action = new GetUserSelectedBranchAction(userName, loggedUser );
                return action.Execute(EnumDatabase.USP);
            }
            catch
            {
                throw;
            }
        }

        public static bool CheckUserNameAvailability(string userName)
        {
            try
            {
                CheckUserNameAvailabilityAction action = new CheckUserNameAvailabilityAction(userName);
                return action.Execute(EnumDatabase.USP);
            }
            catch
            {
                throw;
            }
        }

        public static bool CheckEmailAvailability(string email, string loggedUser)
        {
            try
            {
                CheckEmailAvailabilityAction action = new CheckEmailAvailabilityAction(email, loggedUser);
                return action.Execute(EnumDatabase.USP);
            }
            catch
            {
                throw;
            }
        }

        public static List<USPSubOperation> GetSubOperationsFoleRole(int roleId, string loggedUser)
        {
            GetSubOperationListForRoleAction command = new GetSubOperationListForRoleAction(roleId,loggedUser);
            return command.Execute(EnumDatabase.USP);
        }

        public static List<USPSubOperation> GetSubOperationsForUser(string userName)
        {
            GetSubOperationListForUserAction command = new GetSubOperationListForUserAction( userName);
            return command.Execute(EnumDatabase.USP);
        }

        public static USPRole GetDefaultUspRole(string loggedUser)
        {
            USPRole uspRole = new USPRole();
            uspRole.ModuleList = GetModules(loggedUser).Select(x => x.ID).ToList();
            uspRole.FeatureList = GetFeatures(loggedUser).Select(x => x.ID).ToList();
            uspRole.OperationList = GetOperations(loggedUser);
            return uspRole;
        }

        public static List<USPEmployee> SearchUSPEmployees(string searchText, bool isActive, string loggedUser)
        {
            SearchUSPEmployeeAction command = new SearchUSPEmployeeAction(searchText, isActive, loggedUser);
            return command.Execute(EnumDatabase.USP);
        }

        public static List<UserBranch> GetBranches(string loggedUser)
        {
            GetBranchesAction command = new GetBranchesAction(loggedUser);
            return command.Execute(EnumDatabase.USP);
        }

        public static bool CheckCardNumberAvilability(string cardNumber, string loggedUser)
        {
            CheckCardNumberAvilabilityAction command = new CheckCardNumberAvilabilityAction(cardNumber, loggedUser);
            return command.Execute(EnumDatabase.USP);
        }

       
        public static List<ExceCategory> GetCategories(string type, string loggedUser)
        {
            GetCategoriesOfTypeAction command = new GetCategoriesOfTypeAction(type,loggedUser);
            return command.Execute(EnumDatabase.USP);            
        }

        public static List<ExceHardwareProfile> GetHardwareProfiles(int branchId, string user)
        {
            GetHardwareProfilesAction command = new GetHardwareProfilesAction(branchId, user);
            return command.Execute(EnumDatabase.USP);
        }


        public static bool SaveUserCulture(string culture, string user)
        {
            try
            {
                var action = new SaveUserCultureAction(culture, user);
                return action.Execute(EnumDatabase.USP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static int GetLoginGymByUser(string user, string gymCode)
        {
            try
            {
                var action = new GetLoginGymByUserAction(user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string IsUserAdmin(string user, string gymcode)
        {
            try
            {
                IsUserAdminAction command = new IsUserAdminAction(user);
                return command.Execute(EnumDatabase.Exceline, gymcode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
