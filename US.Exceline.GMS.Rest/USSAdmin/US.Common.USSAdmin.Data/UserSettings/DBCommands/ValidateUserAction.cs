﻿using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class ValidateUserAction : USDBActionBase<string>
    {
        private string _userName = string.Empty;
        private string _password = string.Empty;
        public ValidateUserAction(string userName, string password)
        {
            _userName = userName;
            _password = password;
            OverwriteUser(userName);
        }

        protected override string Body(DbConnection connection)
        {
            string reader = "0";

            try
            {
                string spName = "USP_AUT_ValidateUser";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@userName", DbType.String, _userName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@password", DbType.String, _password));

                reader = Convert.ToString(cmd.ExecuteScalar());
            }
            catch
            {
                throw;
            }
            return reader;
        }

    }
}
