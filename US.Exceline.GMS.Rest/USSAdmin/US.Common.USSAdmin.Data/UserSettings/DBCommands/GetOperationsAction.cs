﻿#region File Header
// --------------------------------------------------------------------------
// Copyright(c) <2010> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USAR
// Project Name      : US.Payment.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : 29/12/2011 HH:MM  PM
// --------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using US_DataAccess;
using US.Common.Web.UI.Core.SystemObjects;
using System.Data.Common;
using US.Payment.Core.Utils;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    class GetOperationsAction : USDBActionBase<List<USPOperation>>
    {
        private string _loggedUser = string.Empty;
        public GetOperationsAction(string loggedUser)
        {
            _loggedUser = loggedUser;
            OverwriteUser(_loggedUser);
        }

        protected override List<USPOperation> Body(System.Data.Common.DbConnection connection)
        {
            List<USPOperation> operationList = new List<USPOperation>();

            try
            {
                string spName = "USP_AUT_GetOperations";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
               

                DbDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    USPOperation operation = new USPOperation();
                    if (reader["DisplayName"] != DBNull.Value)
                    {
                        operation.DisplayName = Convert.ToString(reader["DisplayName"]);
                    }
                    if (reader["ID"] != DBNull.Value)
                    {
                        operation.ID = Convert.ToInt32(reader["ID"]);
                    }
                    if (reader["Name"] != DBNull.Value)
                    {
                        operation.Name = Convert.ToString(reader["Name"]);
                    }
                    if (reader["ThumbnailImage"] != DBNull.Value)
                    {
                        operation.ThumbnailImage = Convert.ToString(reader["ThumbnailImage"]);
                    }
                    if (reader["ModuleId"] != DBNull.Value)
                    {
                        operation.ModuleId = Convert.ToInt32(reader["ModuleId"]);
                    }
                    if (reader["FeatureId"] != DBNull.Value)
                    {
                        operation.FeatureId = Convert.ToInt32(reader["FeatureId"]);
                    }
                    if (reader["Namespace"] != DBNull.Value)
                    {
                        operation.OperationNameSpace = Convert.ToString(reader["Namespace"]);
                    }
                    if (reader["FieldList"] != DBNull.Value && !String.IsNullOrEmpty(reader["FieldList"].ToString()))
                    {
                        string feildListXML = Convert.ToString(reader["FieldList"]);
                        operation.OperationExInfo = (OperationExtendedInfo)XMLUtils.DesrializeXMLToObject(feildListXML, typeof(OperationExtendedInfo));
                    }
                    operationList.Add(operation);
                }
            }
            catch
            {
                throw;
            }
            return operationList;
        }
    }
}
