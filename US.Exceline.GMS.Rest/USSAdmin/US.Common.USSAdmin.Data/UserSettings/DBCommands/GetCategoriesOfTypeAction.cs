﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Web.UI.Core.SystemObjects;
using US_DataAccess;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class GetCategoriesOfTypeAction : USDBActionBase<List<ExceCategory>>
    {
        private string _type;
        private string _loggedUser;

        public GetCategoriesOfTypeAction(string type,string loggedUser)
        {
            _type = type;
            _loggedUser = loggedUser;
            OverwriteUser(_loggedUser);

        }

        protected override List<ExceCategory> Body(System.Data.Common.DbConnection connection)
        {
            List<ExceCategory> categoryList = new List<ExceCategory>();
            string storedProcedureName = "USExceGMSGetCategoriesByType";
            
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, _type));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExceCategory category = new ExceCategory();
                    category.Id = Convert.ToInt32(reader["ID"]);
                    category.TypeId = Convert.ToInt32(reader["CategoryTypeId"]);
                    category.BranchId = Convert.ToInt32(reader["BranchId"]);
                    category.Code = reader["Code"].ToString();
                    category.Name = reader["Description"].ToString();
                    //category.CategoryImage = reader["Image"];//TODO:
                    category.CreatedUser = reader["CreatedUser"].ToString();
                    category.LastModifiedUser = reader["LastModifiedUser"].ToString();

                    if (!string.IsNullOrEmpty(reader["CreatedDateTime"].ToString()))
                    {
                        category.CreatedDate = Convert.ToDateTime(reader["CreatedDateTime"]);
                    }
                    if (!string.IsNullOrEmpty(reader["LastModifiedDateTime"].ToString()))
                    {
                        category.CreatedDate = Convert.ToDateTime(reader["LastModifiedDateTime"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ActiveStatus"].ToString()))
                    {
                        category.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    }
                    categoryList.Add(category);
                }
            }
            catch
            {
                throw;
            }

            return categoryList;
        }
    }
}
