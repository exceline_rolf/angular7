﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.Common.Web.UI.Core.SystemObjects;
using System.Data.Common;
using System.Data;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    class SearchUSPEmployeeAction: USDBActionBase<List<USPEmployee>>
    {
        private string _searchText;
        private bool _isActive;
        private string _loggedUser = string.Empty;

        public SearchUSPEmployeeAction(string searchText, bool isActive,string loggedUser)
        {
            this._isActive = isActive;
            this._searchText = searchText;
            _loggedUser = loggedUser;
            OverwriteUser(_loggedUser);
        }

        protected override List<USPEmployee> Body(System.Data.Common.DbConnection connection)
        {
            List<USPEmployee> uspEmployeeList = new List<USPEmployee>();

            try
            {
                string storedProcedure = "USP_AUT_USPEmployeeSearch";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SearchText", DbType.String, _searchText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActive", DbType.Boolean, _isActive));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    USPEmployee uspemployee = new USPEmployee();
                     
                    if (!string.IsNullOrEmpty(reader["EmployeeId"].ToString()))
                    {
                        uspemployee.Id = Convert.ToInt32(reader["EmployeeId"]);
                    }
                    //if (!string.IsNullOrEmpty(reader["FirstName"].ToString()))
                    //{
                        uspemployee.Name = Convert.ToString(reader["FirstName"]) + " " + Convert.ToString(reader["LastName"]);
                    //}
                    if(!string.IsNullOrEmpty(reader["Email"].ToString()))
                    {
                        uspemployee.Email = Convert.ToString(reader["Email"]);
                    }
                    if (!string.IsNullOrEmpty(reader["CardNumber"].ToString()))
                    {
                        uspemployee.EmployeeCard = Convert.ToString(reader["CardNumber"]);
                    }
                    
                    uspEmployeeList.Add(uspemployee);
                }
            }
            catch
            {
                throw;
            }
            return uspEmployeeList;
        }
    }
}


