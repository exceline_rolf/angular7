﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.Common.Web.UI.Core.SystemObjects;
using System.Data.Common;
using System.Data;


namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    class GetFeaturesForUserAction : USDBActionBase<List<USPFeature>>
    {
        private string _userName = string.Empty;

        public GetFeaturesForUserAction(string userName)
        {
            _userName = userName;
            OverwriteUser(_userName);
        }

        protected override List<USPFeature> Body(System.Data.Common.DbConnection connection)
        {
            List<USPFeature> featureList = new List<USPFeature>();

            try
            {
                string spName = "USP_AUT_GetFeaturesForUser";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@userName", DbType.String, _userName));

                DbDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    USPFeature feature = new USPFeature();
                    feature.ID = Convert.ToInt32(reader["ID"]);
                    feature.DisplayName = Convert.ToString(reader["DisplayName"]);
                    feature.Name = Convert.ToString(reader["Name"]);
                    feature.ThumbnailImage = Convert.ToString(reader["ThumbnailImage"]);
                    if (reader["COLOR"] != null)
                        feature.FeaturColor = Convert.ToString(reader["COLOR"]);
                    feature.HomeUIControl = Convert.ToString(reader["Namespace"]);
                    feature.ModuleId = Convert.ToInt32(reader["ModuleId"]);
                    feature.FeatureHomeURL = Convert.ToString(reader["FeatureHomeURL"]);
                    featureList.Add(feature);
                }
            }
            catch
            {
                throw;
            }
            return featureList;

        }


    }
}
