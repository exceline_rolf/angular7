﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class CheckEmailAvailabilityAction :USDBActionBase<bool>
    {
        private readonly string _email = string.Empty;

        public CheckEmailAvailabilityAction(string email, string loggedUser)
        {
            _email = email;
            OverwriteUser(loggedUser);
        }

        protected override bool Body(DbConnection connection)
        {
            const string spName = "USP_AUT_EmailAvailability";
            var cmd = CreateCommand(CommandType.StoredProcedure, spName);
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@email", DbType.String, _email));

            var reader = Convert.ToBoolean(cmd.ExecuteScalar());
            return reader;
        }
    }
}
