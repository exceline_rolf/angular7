﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.Common.Web.UI.Core.SystemObjects;
using System.Data.Common;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class GetBranchesAction : USDBActionBase<List<UserBranch>>
    {
        private string _loggedUser = string.Empty;

        public GetBranchesAction(string loggedUser)
        {
            _loggedUser = loggedUser;
            OverwriteUser(_loggedUser);
        }

        protected override List<UserBranch> Body(DbConnection connection)
        {
            List<UserBranch> branchList = new List<UserBranch>();
            string storedProcedure = "USExceGMSAdminGetBranches";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    UserBranch item = new UserBranch();
                    item.Id = Convert.ToInt32(reader["BranchId"]);
                    item.EntNo = Convert.ToInt32(reader["EntNo"]);
                    item.BranchName = reader["BranchName"].ToString();
                    item.GroupId = Convert.ToInt32(reader["GroupId"]);
                    item.GroupName = reader["GroupName"].ToString();
                    item.RegisteredDate = Convert.ToDateTime(reader["RegisteredDate"]);
                    item.Addr1 = reader["Addr1"].ToString();
                    item.Addr2 = reader["Addr2"].ToString();
                    item.Addr3 = reader["Addr3"].ToString();
                    item.ZipCode = reader["ZipCode"].ToString();
                    item.ZipName = reader["ZipName"].ToString();
                    item.CreditorCollectionId = reader["CreditorInkassoID"].ToString();
                    item.CountryId = reader["CountryId"].ToString();
                    item.TelWork = reader["TelWork"].ToString().Trim();
                    item.Email = reader["Email"].ToString();
                    item.Fax = reader["Fax"].ToString().Trim();
                    item.AccountNo = reader["AccountNo"].ToString();
                    item.RegisteredNo = reader["RegisteredNo"].ToString();

                    branchList.Add(item);
                }

            }
            catch
            {
                throw;
            }
            return branchList;
        }
    }
}
