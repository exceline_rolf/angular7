﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class GetLoginGymByUserAction : USDBActionBase<int>
    {
        private readonly string _user = string.Empty;

        public GetLoginGymByUserAction(string user)
        {
            _user = user;
        }
        protected override int Body(DbConnection connection)
        {
            try
            {
                const string spName = "USP_GetLoginGymByUser";
                var cmd = CreateCommand(CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@userName", DbType.String, _user));
                var dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    return Convert.ToInt32(dataReader["BranchId"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 1;
        }
    }
}
