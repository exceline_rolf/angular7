﻿using System;
using System.Collections.Generic;
using US.Common.Web.UI.Core.SystemObjects;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.Payment.Core.Utils;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class GetSubOperationListForRoleAction : USDBActionBase<List<USPSubOperation>>
    {
        private int _roleId = -1;
        private string _loggedUser = string.Empty;
        public GetSubOperationListForRoleAction(int roleId,string loggedUser)
        {
            _roleId = roleId;
            _loggedUser = loggedUser;
            OverwriteUser(_loggedUser);
        }

        protected override List<USPSubOperation> Body(System.Data.Common.DbConnection connection)
        {
            List<USPSubOperation> operationList = new List<USPSubOperation>();

            try
            {
                string spName = "USP_AUT_GetSubOperationListForRole";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RoleId", DbType.Int16, _roleId ));
                DbDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    USPSubOperation operation = new USPSubOperation();
                    if (!(string.IsNullOrEmpty(reader["Name"].ToString())))
                    {
                        operation.Name = Convert.ToString(reader["Name"]);
                    }
                    if (!(string.IsNullOrEmpty(reader["DisplayName"].ToString())))
                    {
                        operation.DisplayName = Convert.ToString(reader["DisplayName"]);
                    }
                    if (!(string.IsNullOrEmpty(reader["ID"].ToString())))
                    {
                        operation.Id = Convert.ToInt32(reader["ID"]);
                    }

                    if (reader["ThumbnailImage"] != null)
                    {
                        operation.ThumbnailImage = Convert.ToString(reader["ThumbnailImage"]);
                    }
                    if (reader["COLOR"] != null)
                    {
                        operation.OperationColor = Convert.ToString(reader["COLOR"]);
                    }

                    if (!string.IsNullOrEmpty(reader["FieldList"].ToString()))
                    {
                        string extendedInfo = Convert.ToString(reader["FieldList"]).ToString();
                        extendedInfo = extendedInfo.Replace("US.Payment.Core.DomainObjects.DomainObjects.Authentication", "US.Common.Web.UI.Core.SystemObjects");
                        operation.ExtendedInfo = (OperationExtendedInfo)XMLUtils.DesrializeXMLToObject(extendedInfo, typeof(OperationExtendedInfo));
                    }
                    operationList.Add(operation);
                }
            }
            catch
            {
                throw;
            }
            return operationList;
        }
    }
}
