﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class CheckForAdminAccess : USDBActionBase<int>
    {
        private  string _userName = string.Empty;

        public CheckForAdminAccess(String userName)
        {
            _userName = userName;
          
        }

        protected override int Body(DbConnection connection)
        {
            var reader = -1;
            try
            {
                const string spName = "CheckForAdminAccess";
                var cmd = CreateCommand(CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _userName));

                 reader = Convert.ToInt32(cmd.ExecuteScalar());
           
            }
            catch
            {
                throw;
            }
            return reader;
        }
    }
}
