﻿using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class CheckCardNumberAvilabilityAction : USDBActionBase<bool>
    {
        private string _cardNumber = string.Empty;
        private string _loggedUser = string.Empty;
        public CheckCardNumberAvilabilityAction(string cardNumber, string loggedUser)
        {
            this._cardNumber = cardNumber;
            _loggedUser = loggedUser;
            OverwriteUser(_loggedUser);
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool reader = false;

            try
            {
                string spName = "USP_AUT_CardNumberAvailability";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@cardNumber", DbType.String, _cardNumber));

                reader = Convert.ToBoolean(cmd.ExecuteScalar());


            }
            catch
            {
                throw;
            }
            return reader;
        }
    }
}
