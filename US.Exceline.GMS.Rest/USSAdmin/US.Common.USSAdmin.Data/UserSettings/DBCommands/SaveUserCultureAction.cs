﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class SaveUserCultureAction : USDBActionBase<bool>
    {
        private readonly string _culture = string.Empty;
        private readonly string _userName = string.Empty;
        public SaveUserCultureAction(string culture, string userName)
        {
            _culture = culture;
            _userName = userName;
            OverwriteUser(userName);

        }
        protected override bool Body(DbConnection connection)
        {
            try
            {
                const string storedProcedure = "USExceGMSSaveUserCulture";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Culture", DbType.String, _culture));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _userName));
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
