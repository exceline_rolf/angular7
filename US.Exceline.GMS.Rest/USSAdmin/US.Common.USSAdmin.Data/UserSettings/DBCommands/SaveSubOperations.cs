﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Web.UI.Core.SystemObjects;
using US.Payment.Core.Utils;
using US_DataAccess;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class SaveSubOperations : USDBActionBase<int>
    {
        private int _roleId;
        private USPSubOperation _subOperation;
        public SaveSubOperations(int roleId, USPSubOperation subOperation, string loggedUser)
        {
            _roleId = roleId;
            _subOperation = subOperation;
            OverwriteUser(loggedUser);
        }
        protected override int Body(DbConnection connection)
        {
            int _result = -1;
            try
            {
                string storedProcedureName = "USP_AUT_AddEditUSPSubOperationForRole";
                DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);

                cmd2.Parameters.Add(DataAcessUtils.CreateParam("@RoleNo", DbType.Int16, _roleId));
                cmd2.Parameters.Add(DataAcessUtils.CreateParam("@SubOperation", DbType.Int32, _subOperation.Id));
                cmd2.Parameters.Add(DataAcessUtils.CreateParam("@ExtendedInfo", DbType.Xml,XMLUtils.SerializeDataContractObjectToXML(_subOperation.ExtendedInfo)));
                cmd2.ExecuteNonQuery();
                _result = 1;

            }catch
            {
                
            }
            return _result;

        }
    }
}
