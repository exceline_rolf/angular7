﻿using System;
using US_DataAccess;
using US.Common.Web.UI.Core.SystemObjects;
using System.Data.Common;
using System.Data;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    class GetUserInfoAction : USDBActionBase<USPUser>
    {
        private string _userName = string.Empty;

        public GetUserInfoAction(string userName)
        {
            _userName = userName;
            OverwriteUser(_userName);
        }

        protected override USPUser Body(System.Data.Common.DbConnection connection)
        {
            USPUser user = new USPUser();

            try
            {
                string spName = "USP_AUT_GetUserInfo";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _userName));

                DbDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    user.Id = Convert.ToInt32(reader["ID"]);
                    if (reader["Home"] != null)
                    {
                        user.HomePage = Convert.ToString(reader["Home"]);
                    }
                    if (reader["ExternalUserName"] != null)
                        user.ExternalUserName = Convert.ToString(reader["ExternalUserName"]).Trim();
                    if (reader["Email"] != null)
                        user.email = Convert.ToString(reader["Email"]).Trim();
                    if (reader["DisplayName"] != null)
                        user.DisplayName = Convert.ToString(reader["DisplayName"]).Trim();
                    if (reader["IsLoginFirstTime"] != null)
                        user.IsLoginFirstTime = Convert.ToBoolean(reader["IsLoginFirstTime"]);
                    if (reader["ActiveStatus"] != null)
                        user.IsActive = Convert.ToBoolean(reader["ActiveStatus"]); 
                }

            }
            catch
            {
                throw;
            }
            return user;
        }
    }
}
