﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Common.USSAdmin.RestApi.Models
{
    public class USPParameterDC
    {
        public string ID { get; set; }
        public string DisplayName { get; set; }
        public string ParameterType { get; set; }
        public object Value { get; set; }
    }
}