﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.Common.USSAdmin.RestApi.Models
{
    public class BasicInfo
    {

        public bool isPasswdChange { get; set; }
        public USPUser uspUser { get; set; }
        public string Password { get; set; }
     
    }
}
