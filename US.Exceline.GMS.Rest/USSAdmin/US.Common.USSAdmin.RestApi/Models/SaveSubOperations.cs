﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.Common.USSAdmin.RestApi.Models
{
    public class SaveSubOperations
    {
        public List<USPSubOperation> subOperations { get; set; }
        public int roleId { get; set; }
    }
}