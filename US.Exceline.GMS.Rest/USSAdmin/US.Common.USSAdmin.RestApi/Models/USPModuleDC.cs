﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Common.USSAdmin.RestApi.Models
{
    public class USPModuleDC
    {
        private List<USPFeatureDC> _features = new List<USPFeatureDC>();
        public string ID { get; set; }
        public int ModuleId { get; set; }
        public string DisplayName { get; set; }
        public USPFeatureDC DeafaultFeature { get; set; }
        public string ThumbnailImage { get; set; }
        public string ModuleHome { get; set; }
        public string ModulePackage { get; set; }
        public string ModuleColor { get; set; }
        public List<USPFeatureDC> Features
        {
            get { return _features; }
            set { _features = value; }
        }
        private List<OperationDC> _operations = new List<OperationDC>();
        public List<OperationDC> Operations
        {
            get { return _operations; }
            set { _operations = value; }
        }
    }
}