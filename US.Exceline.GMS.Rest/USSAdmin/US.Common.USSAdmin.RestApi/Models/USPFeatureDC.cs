﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Common.USSAdmin.RestApi.Models
{
    public class USPFeatureDC
    {
        public string ID { get; set; }
        public int FeatureId { get; set; }
        public string DisplayName { get; set; }
        public string HomeUIControl { get; set; }
        public string ThumbnailImage { get; set; }
        public int Priority { get; set; }
        private List<OperationDC> _operations = new List<OperationDC>();
        public List<OperationDC> Operations
        {
            get { return _operations; }
            set { _operations = value; }
        }
        public string FeaturePath { get; set; }
    }
}