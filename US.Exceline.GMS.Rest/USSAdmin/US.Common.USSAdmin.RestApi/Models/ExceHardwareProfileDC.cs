﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Common.USSAdmin.RestApi.Models
{
    public class ExceHardwareProfileDC
    {

        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _branchId = -1;
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _categoryListString = string.Empty;
        public string CategoryListString
        {
            get { return _categoryListString; }
            set { _categoryListString = value; }
        }
    }
}