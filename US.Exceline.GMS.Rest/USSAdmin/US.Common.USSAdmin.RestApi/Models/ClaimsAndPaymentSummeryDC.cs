﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Common.USSAdmin.RestApi.Models
{
    public class ClaimsAndPaymentSummeryDC
    {
        private int _numberOfClaims = 0;
        public int NumberOfClaims
        {
            get { return _numberOfClaims; }
            set { _numberOfClaims = value; }
        }
    }
}