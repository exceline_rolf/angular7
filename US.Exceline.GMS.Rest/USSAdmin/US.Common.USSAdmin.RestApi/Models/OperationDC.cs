﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Common.USSAdmin.RestApi.Models
{
    public class OperationDC
    {
        public string OperationHome { get; set; }
        public string ID { get; set; }
        public int OperationId { get; set; }
        public string DisplayName { get; set; }
        public string ThumbnailImage { get; set; }
        public string Mode { get; set; }
        public int IsAddedToWorkStation { get; set; }
        public int RoleId { get; set; }
        public string OperationNameSpace { get; set; }
        public string OperationPath { get; set; }
        public List<OperationFieldDC> FieldList { set; get; }
    }
}