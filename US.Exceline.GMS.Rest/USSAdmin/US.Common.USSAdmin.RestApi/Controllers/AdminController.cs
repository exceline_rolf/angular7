﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.GMS.Core.Utils;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Login;
using US.GMS.Core.SystemObjects;
using US.Common.USSAdmin.API;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.Exceline.GMS.Modules.Admin.API.ManageHolidays;
using US.Common.Logging.API;
using US.Payment.Core.ResultNotifications;
using US.Common.Web.UI.Core.SystemObjects;
using US.Exceline.GMS.Modules.Admin.API.ManageEmployees;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.Common.Web.UI.Core.SystemObjects.ScheduleManagement;
using US.Common.USSAdmin.RestApi.Models;
using System.Web.Script.Serialization;
using System.ServiceModel;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using bfDemoService;
using US.Exceline.GMS.Modules.Admin.API.ManageSubscriptions;

namespace US.Common.USSAdmin.RestApi.Controllers
{
    [RoutePrefix("api/Admin")]
    public class AdminController : ApiController
    {
        [HttpGet]
        [Route("getdata")]
        [Authorize]
        public IHttpActionResult GetString()
        {
            return Ok("string");
        }

        [HttpGet]
        [Route("GetUspUsers")]
        [Authorize]
        public HttpResponseMessage GetUSPUsers(string searchText, bool isActive)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = AdminUserSettings.GetUSPUsers(searchText, isActive, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<USPUser>(), ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<UspUsersExtended>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("SearchUSPEmployees")]
        [Authorize]
        public HttpResponseMessage SearchUSPEmployees(string searchText, bool isActive)
        {
            try
            {

                string user = Request.Headers.GetValues("UserName").First();

                var result = GMSGymEmployee.GetGymEmployees(-2, searchText, isActive, -1, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetHardwareProfiles")]
        [Authorize]
        public HttpResponseMessage GetHardwareProfiles(int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResultValue<List<ExceHardwareProfile>> result = AdminUserSettings.GetHardwareProfiles(branchId, user);
                List<Models.ExceHardwareProfileDC> exceCategoryDCList = new List<Models.ExceHardwareProfileDC>();

                foreach (ExceHardwareProfile hardwareProfile in result.OperationReturnValue)
                {
                    Models.ExceHardwareProfileDC hardwareProfileDC = new Models.ExceHardwareProfileDC();
                    hardwareProfileDC.Id = hardwareProfile.Id;
                    hardwareProfileDC.Name = hardwareProfile.Name;
                    exceCategoryDCList.Add(hardwareProfileDC);
                }
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<Models.ExceHardwareProfileDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<Models.ExceHardwareProfileDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetBranches")]
        [Authorize]
        public HttpResponseMessage GetBranches()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = AdminUserSettings.GetBranches(user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<UserBranch>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<UserBranch>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("IsUserAdmin")]
        [Authorize]
        public HttpResponseMessage IsUserAdmin()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = AdminUserSettings.IsUserAdmin(user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<UserBranch>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<UserBranch>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }



        [HttpGet]
        [Route("GetUserSelectedBranches")]
        [Authorize]
        public HttpResponseMessage GetUserSelectedBranches(string userName)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = AdminUserSettings.GetUserSelectedBranches(userName, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<UserBranchSelected>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<UserBranchSelected>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetUSPRoles")]
        [Authorize]
        public HttpResponseMessage GetUSPRoles(string searchText, bool isActive)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = AdminUserSettings.GetUSPRoles(searchText, isActive, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("CheckCardNumberAvilability")]
        [Authorize]
        public HttpResponseMessage CheckCardNumberAvilability(string cardNumber)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = AdminUserSettings.CheckCardNumberAvilability(cardNumber, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetModulesFeaturesOperations")]
        [Authorize]
        public HttpResponseMessage GetModulesFeaturesOperations(string loggedUser)
        {
            try
            {
                List<USPModule> moduleList = AdminUserSettings.GetModulesFeaturesOperations(loggedUser).MethodReturnValue;
                List<USPModuleDC> uspModuleDCList = new List<USPModuleDC>();


                foreach (var item in moduleList)
                {
                    USPModuleDC mod = new USPModuleDC();
                    mod.DeafaultFeature = GetServiceFeature(item.DeafaultFeature);
                    mod.DisplayName = item.DisplayName;
                    mod.ModuleId = item.ID;
                    mod.Features = GetServiceFeatures(item.Features);
                    mod.ID = item.Name;  //wrong usage
                    mod.ModuleHome = item.ModuleHome;
                    mod.Operations = GetServiceOparations(item.Operations);
                    mod.ThumbnailImage = item.ThumbnailImage;
                    uspModuleDCList.Add(mod);
                }


                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(uspModuleDCList, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
            return null;
        }

        private OperationDC GetServiceOparation(USPOperation item)
        {
            if (item == null)
            {
                return null;
            }
            OperationDC operation = new OperationDC();
            operation.DisplayName = item.DisplayName;
            operation.ID = item.Name;
            operation.OperationId = item.ID;
            operation.OperationHome = item.OperationHome;
            operation.OperationNameSpace = item.OperationNameSpace;
            operation.ThumbnailImage = item.ThumbnailImage;
            operation.Mode = item.Mode;
            operation.RoleId = item.RoleId;
            operation.IsAddedToWorkStation = item.IsAddedToWorkStation;
            operation.FieldList = GetOperationFieldList(item.OperationExInfo);
            return operation;
        }

        private List<OperationFieldDC> GetOperationFieldList(OperationExtendedInfo operationExtendedInfo)
        {
            List<OperationFieldDC> ofdcList = new List<OperationFieldDC>();
            foreach (OperationField of in operationExtendedInfo.OperationFieldList)
            {
                OperationFieldDC fdc = new OperationFieldDC();
                fdc.DisplayName = of.DisplayName;
                fdc.Status = of.FieldStatus.ToString();
                ofdcList.Add(fdc);
            }
            return ofdcList;
        }

        private List<USPFeatureDC> GetServiceFeatures(List<USPFeature> list)
        {
            if (list == null)
            {
                return null;
            }
            List<USPFeatureDC> features = new List<USPFeatureDC>();

            foreach (var item in list)
            {
                features.Add(GetServiceFeature(item));
            }
            return features;
        }

        private USPFeatureDC GetServiceFeature(USPFeature uSPFeature)
        {
            if (uSPFeature == null)
            {
                return null;
            }
            USPFeatureDC feature = new USPFeatureDC();
            if (uSPFeature != null)
            {
                feature.DisplayName = uSPFeature.DisplayName;
                feature.HomeUIControl = uSPFeature.HomeUIControl;
                feature.ID = uSPFeature.Name;
                feature.FeatureId = uSPFeature.ID;
                feature.Operations = GetServiceOparations(uSPFeature.Operations);
                feature.Priority = uSPFeature.Priority;
                feature.ThumbnailImage = uSPFeature.ThumbnailImage;
            }
            return feature;
        }


        private List<OperationDC> GetServiceOparations(List<USPOperation> list)
        {
            if (list == null)
            {
                return null;
            }
            List<OperationDC> oparations = new List<OperationDC>();
            foreach (var item in list)
            {
                oparations.Add(GetServiceOparation(item));
            }

            return oparations;
        }

        [HttpGet]
        [Route("GetModulesFeaturesOperationsConstrained")]
        [Authorize]
        public HttpResponseMessage GetModulesFeaturesOperationsConstrained(string loggedUser)
        {
            try
            {
                List<USPModule> moduleList = AdminUserSettings.GetModulesFeaturesOperations(loggedUser).MethodReturnValue;
                List<USPModuleDC> uspModuleDCList = new List<USPModuleDC>();
                SubsriptionHandler sub = new SubsriptionHandler();
                List<SubscriptionInfo> sublist = new List<SubscriptionInfo>();
              
                sublist = sub.SubscriptionsForAccess(ExceConnectionManager.GetGymCode(loggedUser));




                foreach (var item in moduleList){ 
                    USPModuleDC mod = new USPModuleDC();        
                    mod.DeafaultFeature = GetServiceFeature(item.DeafaultFeature);
                    mod.DisplayName = item.DisplayName;
                    mod.ModuleId = item.ID;
                    mod.Features = GetServiceFeatures(item.Features);
                    mod.ID = item.Name;  //wrong usage
                    mod.ModuleHome = item.ModuleHome;
                    mod.Operations = GetServiceOparations(item.Operations);
                    mod.ThumbnailImage = item.ThumbnailImage;
                    uspModuleDCList.Add(mod);
                }

                foreach (var element in sublist)
                {

                    foreach (var item in uspModuleDCList.ToList())
                    {
                        foreach (var item2 in item.Features.ToList())
                        {
                            foreach (var item3 in item2.Operations.ToList())
                            {
                               //Check Operation
                                if (element.OperationId == item3.OperationId &&  !element.IsActive)
                                {
                                    item2.Operations.Remove(item3);
                                }
                               
                            }

                            //Check Feature
                            if (element.OperationId == 0 && element.FeatureId == item2.FeatureId && element.ModuleId == item.ModuleId && !element.IsActive)
                            {
                                item.Features.Remove(item2);
                            }
                        }
                        //Check Module
                        if (element.OperationId == 0 && element.FeatureId == 0 && element.ModuleId == item.ModuleId && !element.IsActive)
                        {
                            uspModuleDCList.Remove(item);
                        }

                        foreach (var item4 in item.Operations.ToList())
                        {

                            //Check Operation (No Feature)
                            if (element.OperationId == item4.OperationId  && element.ModuleId == item.ModuleId && !element.IsActive)
                            {
                                item.Operations.Remove(item4);
                            }
                        }

                    }

                }






 

                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(uspModuleDCList, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
            
        }


  

        [HttpGet]
        [Route("GetSubOperationsForRole")]
        [Authorize]
        public HttpResponseMessage GetSubOperationsForRole(int roleId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = AdminUserSettings.GetSubOperationsForRole(roleId, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<USPSubOperation>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<USPSubOperation>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }



        [HttpGet]
        [Route("GetSubOperationsForUser")]
        [Authorize]
        public HttpResponseMessage GetSubOperationsForUser(string userName)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = AdminUserSettings.GetSubOperationsForUser(userName);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<USPSubOperation>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<USPSubOperation>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetDefaultUspRole")]
        [Authorize]
        public HttpResponseMessage GetDefaultUspRole(String loggedUser)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = AdminUserSettings.GetDefaultUspRole(loggedUser);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new USPRole(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new USPRole(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        
    

        [HttpPost]
        [Route("SaveSubOperations")]
        [Authorize]
        public HttpResponseMessage SaveSubOperations(SaveSubOperations saveSubOperations)
        {
            try
            {
                string loggedUser = Request.Headers.GetValues("UserName").First();
     

                var result = AdminUserSettings.SaveSubOperations(saveSubOperations.subOperations, saveSubOperations.roleId, loggedUser); ;
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), loggedUser);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }



        [HttpPost]
        [Route("AddEditUSPUsers")]
        [Authorize]
        public HttpResponseMessage AddEditUSPUsers(AddUspUser addUspUser)
        {
            try
            {
                string loggedUser = Request.Headers.GetValues("UserName").First();
                Dictionary<int, int> _uniqueOperationIdList = addUspUser.uspUser.UniqueOperationIdList;

                //foreach (var key in _uniqueOperationIdList.Keys) {

                //}
             
                var result = AdminUserSettings.AddEditUSPUsers(addUspUser.uspUser, addUspUser.branchesList, loggedUser); ;
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), loggedUser);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpPost]
        [Route("AddEditUSPRole")]
        [Authorize]
        public HttpResponseMessage AddEditUSPRole(USPRole uspRole)
        {
            try
            {
                string loggedUser = Request.Headers.GetValues("UserName").First();
     
                var result = AdminUserSettings.AddEditUSPRole(uspRole, loggedUser);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), loggedUser);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }



        


        [HttpPost]
        [Route("ValidateUser")]
        [Authorize]
        public HttpResponseMessage ValidateUser(ValidatePw pword)
        {
         
            try
            {

                string decrypted=EncryptDecrypt.DecryptStringAES(pword.Password);
 
                string loggedUser = Request.Headers.GetValues("UserName").First();
               
                var result = AdminUserSettings.ValidateUser(loggedUser, decrypted);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), loggedUser);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(0, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(0, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("EditUSPUserBasicInfo")]
        [Authorize]
        public HttpResponseMessage EditUSPUserBasicInfo(BasicInfo basicInfo)
        {
            
            try
            {
                string loggedUser = Request.Headers.GetValues("UserName").First();
                USPUser user = new USPUser();

                user.Id = basicInfo.uspUser.Id;
                user.DisplayName = basicInfo.uspUser.DisplayName;
                user.email = basicInfo.uspUser.email;
                user.ExternalUserName = basicInfo.uspUser.ExternalUserName;
                user.passowrd = EncryptDecrypt.DecryptStringAES(basicInfo.Password);
                user.IsLoginFirstTime = basicInfo.uspUser.IsLoginFirstTime;

                var result = AdminUserSettings.EditUSPUserBasicInfo(basicInfo.isPasswdChange,user, loggedUser);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), loggedUser);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
        }


    }
}
