﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Common.USSAdmin.BusinessLogic;
using US.Common.Web.UI.Core.SystemObjects;
using US.Common.Web.UI.Core.SystemObjects.ScheduleManagement;
using US.GMS.Core.SystemObjects;
using US.Payment.Core.ResultNotifications;

namespace US.Common.USSAdmin.API
{
    public class AdminUserSettings
    {
        #region User Loging
        public static USImportResult<USPUserProfileInfo> GetUserProfileInfo(string userName)
        {
            return ManageAdminUsers.GetUserProfileInfo(userName);
        }
        /// <summary>
        /// Return whether provided user is valid or not
        /// Unit testing done
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static OperationResultValue<string> ValidateUser(string username, string password)
        {
            return ManageAdminUsers.ValidateUser(username, password);
        }

        public static OperationResultValue<USPUser> ValidateUserWithCard(string cardNumber,string user)
        {
            return ManageAdminUsers.ValidateUserWithCard(cardNumber, user);
        }

        public static OperationResultValue<List<string>> GetVersionNoList(string gymCode)
        {
            return ManageAdminUsers.GetVersionNoList(gymCode);
        }
        #endregion

        #region UserSettings
        /// <summary>
        /// Return list of users matached with the provided search text
        /// Unit testing done
        /// </summary>
        /// <param name="searchText"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public static USImportResult<List<UspUsersExtended>> GetUSPUsers(string searchText, bool isActive, string loggedUser)
        {
            return ManageAdminUsers.GetUSPUsers(searchText, isActive, loggedUser);
        }
        /// <summary>
        /// Register/Update user in the is the system and return corresponding ID of the user created.
        /// Unit testing done
        /// </summary>
        /// <param name="user"></param>
        /// <param name="branchesList"></param>
        /// <returns></returns>
        public static USImportResult<int> AddEditUSPUsers(UspUsersExtended user, List<UserBranchSelected> branchesList, string loggedUser)
        {
            return ManageAdminUsers.AddEditUSPUsers(user, branchesList, loggedUser);
        }
        /// <summary>
        /// Return list of User roles match with the provided search text
        /// Unit testing done
        /// </summary>
        /// <param name="searchText"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public static USImportResult<List<USPRole>> GetUSPRoles(string searchText, bool isActive, string loggedUser)
        {
            return ManageAdminUsers.GetUSPRoles(searchText, isActive, loggedUser);
        }
        /// <summary>
        /// Update User Basic info. 
        /// Unit testing done for update password scenario
        /// </summary>
        /// <param name="isPasswdChange"></param>
        /// <param name="uspUser"></param>
        /// <returns></returns>
        public static OperationResultValue<bool> EditUSPUserBasicInfo(bool isPasswdChange, USPUser uspUser, string loggedUser)
        {
            return ManageAdminUsers.EditUSPUserBasicInfo(isPasswdChange, uspUser, loggedUser);
        }
        /// <summary>
        /// Add / Update user role provided
        /// Unit testing done
        /// </summary>
        /// <param name="uspRole"></param>
        /// <returns></returns>
        public static USImportResult<int> AddEditUSPRole(USPRole uspRole, string loggedUser)
        {
            return ManageAdminUsers.AddEditUSPRole(uspRole, loggedUser);
        }

        public static USImportResult<int> SaveSubOperations(List<USPSubOperation> subOperations, int roleId, string loggedUser)
        {
            return ManageAdminUsers.SaveSubOperations(subOperations, roleId, loggedUser);
        }
        /// <summary>
        /// Return default user role
        /// Unit Testing done
        /// </summary>
        /// <returns></returns>
        public static OperationResultValue<USPRole> GetDefaultUspRole(string loggedUser)
        {
            return ManageAdminUsers.GetDefaultUspRole(loggedUser);
        }
        /// <summary>
        /// Returns the availability of the user name
        /// Unit testing done
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static OperationResultValue<bool> CheckUserNameAvailability(string userName)
        {
            return ManageAdminUsers.CheckUserNameAvailability(userName);
        }

        public static OperationResultValue<bool> CheckEmailAvailability(string email, string loggedUser)
        {
            return ManageAdminUsers.CheckEmailAvailability(email, loggedUser);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static OperationResultValue<List<UserBranchSelected>> GetUserSelectedBranches(string userName,string loggedUser)
        {
            return ManageAdminUsers.GetUserSelectedBranches(userName,loggedUser);
        }

        /// <summary>
        /// Get All the Modules,Features, Operations in the System
        /// </summary>
        /// <returns></returns>
        public static USImportResult<List<USPModule>> GetModulesFeaturesOperations(string loggedUser)
        {
            return ManageAdminUsers.GetModulesFeaturesOperations(loggedUser);
        }
        /// <summary>
        /// Returns all the USP operation premission granted for provided role id
        /// Since Module, Feature and Operation registration are not implemented, Unit testing is done only just to make sure code is not broken
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public static OperationResultValue<List<USPSubOperation>> GetSubOperationsForRole(int roleId, string loggedUser)
        {
            return ManageAdminUsers.GetSubOperationsForRole(roleId,loggedUser);
        }
        /// <summary>
        /// Returns all the USP operation premission granted for provided user name
        /// Since Module, Feature and Operation registration are not implemented, Unit testing is done only just to make sure code is not broken
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public static OperationResultValue<List<USPSubOperation>> GetSubOperationsForUser(string userName)
        {
            return ManageAdminUsers.GetSubOperationsForUser( userName);
        }

        public static OperationResultValue<List<USPEmployee>> SearchUSPEmployees(string searchText, bool isActive, string loggedUser)
        {
            return ManageAdminUsers.SearchUSPEmployees(searchText, isActive, loggedUser);
        }

        public static OperationResultValue<List<UserBranch>> GetBranches(string user)
        {
            return ManageAdminUsers.GetBranches(user);
        }

        public static OperationResultValue<bool> CheckCardNumberAvilability(string cardNumber, string loggedUser)
        {
            return ManageAdminUsers.CheckCardNumberAvilability(cardNumber, loggedUser);
        }

        public static OperationResultValue<List<ExceCategory>> GetCategories(string type, string loggedUser)
        {
            return ManageAdminUsers.GetCategories(type, loggedUser);
        }

        public static OperationResultValue<List<ExceHardwareProfile>> GetHardwareProfiles(int branchId, string user)
        {
            return ManageAdminUsers.GetHardwareProfiles(branchId, user);
        }

        public static OperationResultValue<bool> SaveUserCulture(string culture, string user)
        {
            return ManageAdminUsers.SaveUserCulture(culture, user);
        }

        public static OperationResultValue<string> IsUserAdmin(string user, string gymcode)
        {
            return ManageAdminUsers.IsUserAdmin(user, gymcode);
        }

        #endregion

        public static int GetLoginGymByUser(string user, string gymCode)
        {
            try
            {
                return ManageAdminUsers.GetLoginGymByUser(user, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
