﻿#region File Header
// --------------------------------------------------------------------------
// Copyright(c) <2010> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USAR
// Project Name      : US.Payment.Service
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : 01/12/2011 HH:MM  PM
// --------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using US.Common.USSAdmin.Service.DataContracts;
using US.Common.Web.UI.Core.SystemObjects;
using US.Common.Web.UI.Core.SystemObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.SystemObjects;

namespace US.Common.USSAdmin.Service.UserSettings
{
    [ServiceContract(Name = "UserSettings", Namespace = "US.Payment.Service")]
    interface IUserSettingsService
    {
        [OperationContract]
        List<UspUsersExtended> GetUSPUsers(string searchText,bool isActive,string loggedUser);

        [OperationContract]
        int AddEditUSPUsers(UspUsersExtended user, List<UserBranchSelected> branchesList, string loggedUser);

        [OperationContract]
        List<USPRole> GetUSPRoles(string searchText, bool isActive, string loggedUser);

        [OperationContract]
        int AddEditUSPRole(USPRole uspRole, string loggedUser);

          [OperationContract]
        int SaveSubOperations(List<USPSubOperation> subOperations, int roleId, string loggedUser);

        [OperationContract]
        List<USPModuleDC> GetModulesFeaturesOperations(string loggedUser);

        [OperationContract]
        List<UserBranch> GetBranches(string user);

        [OperationContract]
        List<UserBranchSelected> GetUserSelectedBranches(string userName,string loggedUser);

        [OperationContract]
        bool CheckUserNameAvailability(string userName,string loggedUser);

        [OperationContract]
        bool CheckEmailAvailability(string email, string loggedUser);

        [OperationContract]
        bool EditUSPUserBasicInfo(bool isPasswdChange, USPUserDC uspUser,string loggedUser);
        
        [OperationContract]
        List<USPSubOperation> GetSubOperationsForRole(int roleId, string loggedUser);
        
        [OperationContract]
        List<USPSubOperation> GetSubOperationsForUser(string userName,string loggedUser);

        [OperationContract]
        USPRole GetDefaultUspRole(string loggedUser);

        [OperationContract]
        List<GymEmployeeDC> SearchUSPEmployees(string searchText, bool isActive, string loggedUser);

        [OperationContract]
        USPUserDC GetUSPUserInfo(string userName);

        [OperationContract]
        bool CheckCardNumberAvilability(string cardNumber, string loggedUser);

        [OperationContract]
        OperationInfo GetOperationInfo(string loggedUser);

        [OperationContract]
        string ValidateUser(string useName, string password);

        [OperationContract]
        List<ExceCategoryDC> GetCategories(string type, string loggedUser);

        [OperationContract]
        List<ExceHardwareProfileDC> GetHardwareProfiles(int branchId, string user);

        
        [OperationContract]
        bool test(bool test);
       
    }
}
