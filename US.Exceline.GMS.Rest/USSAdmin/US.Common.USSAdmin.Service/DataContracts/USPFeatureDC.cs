﻿// --------------------------------------------------------------------------
// Copyright(c) 2008 UnicornSolutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.Services.DataContracts
// Coding Standard   : US Coding Standards
// Author            : DAM
// Created Timestamp : 27/06/2011 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.CompilerServices;

namespace US.Common.USSAdmin.Service.DataContracts
{
    [DataContract]
    public class USPFeatureDC
    {
        [DataMember]
        public  string ID { get; set; }
        [DataMember]
        public int FeatureId { get; set; }
        [DataMember]
        public  string DisplayName { get; set; }
        [DataMember]
        public  string HomeUIControl { get; set; }
        [DataMember]
        public  string ThumbnailImage { get; set; }
        [DataMember]
        public  int Priority { get; set; }
        private List<OperationDC> _operations = new List<OperationDC>();
        [DataMember]
        public List<OperationDC> Operations
        {
            get { return _operations; }
            set { _operations = value; }
        }
        [DataMember]
        public string FeaturePath { get; set; }
    }
}
