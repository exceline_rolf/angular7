﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Common.USSAdmin.Service.DataContracts
{
    [DataContract]
    public class OperationDC
    {
        [DataMember]
        public string OperationHome { get; set; }
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public int OperationId { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public string ThumbnailImage { get; set; }
        [DataMember]
        public string Mode { get; set; }
        [DataMember]
        public int IsAddedToWorkStation { get; set; }
        [DataMember]
        public int RoleId { get; set; }
        [DataMember]
        public string OperationNameSpace { get; set; }
        [DataMember]
        public string OperationPath { get; set; }
        [DataMember]
        public List<OperationFieldDC> FieldList { set; get; }

    }
}
