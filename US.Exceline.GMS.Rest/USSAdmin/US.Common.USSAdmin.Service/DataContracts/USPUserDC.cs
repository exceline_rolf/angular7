﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.Common.USSAdmin.Service.DataContracts
{
    [DataContract]
    public class USPUserDC
    {
        
        private List<USPModuleDC> _moduleList = new List<USPModuleDC>();

        [DataMember]
        public List<USPModuleDC> ModuleList
        {
            get { return _moduleList; }
            set { _moduleList = value; }
        }

        private int _userID = -1;

        [DataMember]
        public int UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }


        private string _userName = string.Empty;

        [DataMember]
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        private string _userExternalName = string.Empty;
         [DataMember]
        public string UserExternalName
        {
            get { return _userExternalName; }
            set { _userExternalName = value; }
        }

        private string _displayName = string.Empty;

        [DataMember]
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        private string _roleId = string.Empty;

        [DataMember]
        public string RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }

        private string _homePage = string.Empty;

        [DataMember]
        public string HomePage
        {
            get { return _homePage; }
            set { _homePage = value; }
        }

        private string _email = string.Empty;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _password = string.Empty;
        [DataMember]
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private bool _isLoginFirstTime = false;
        [DataMember]
        public bool IsLoginFirstTime
        {
            get { return _isLoginFirstTime; }
            set { _isLoginFirstTime = value; }
        }

        private int _employeeId = -1;
        [DataMember]
        public int EmployeeId
        {
            get { return _employeeId; }
            set { _employeeId = value; }
        }

        private string _employeeRole = string.Empty;
        [DataMember]
        public string EmployeeRole
        {
            get { return _employeeRole; }
            set { _employeeRole = value; }
        }

        private List<int> _branches;
        [DataMember]
        public List<int> Branches
        {
            get { return _branches; }
            set { _branches = value; }
        }

        private List<USPSubOperation> _subOperationList = new List<USPSubOperation>();
        [DataMember]
        public List<USPSubOperation> SubOperationList
        {
            get { return _subOperationList; }
            set { _subOperationList = value; }
        }

        private bool _isActive = false;
        [DataMember]
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
    }
}
