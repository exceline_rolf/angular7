﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using US.Exceline.GMS.Modules.ManageMembership.BusinessLogic.ManageMembership;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Core.DomainObjects.Payments;
using US.Common.Notification.Core.DomainObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership
{
    public class GMSManageMembership
    {
        #region Manage Member

        public static OperationResult<string> SaveMember(OrdinaryMemberDC member, string imagefolderPath, string gymCode, string user)
        {
            return MembershipManager.SaveMember(member, imagefolderPath, gymCode, user);
        }

        public static OperationResult<bool> ValidateMemberBrisId(int brisId, string gymCode)
        {
            return MembershipManager.ValidateMemberBrisId(brisId, gymCode);
        }

        public static OperationResult<bool> UpdateBRISMemberData(OrdinaryMemberDC member, string gymCode)
        {
            return MembershipManager.UpdateBRISMemberData(member, gymCode);
        }

        public static OperationResult<string> UpdateMember(OrdinaryMemberDC member, string imagefolderPath, string gymCode, string user)
        {
            return MembershipManager.UpdateMember(member, imagefolderPath, gymCode, user);
        }
        public static OperationResult<List<OrdinaryMemberDC>> GetGroupMemebersByGroup(int branchId, string searchText, bool IsActive, int groupId, string gymCode)
        {
            return MembershipManager.GetGroupMemebersByGroup(branchId, searchText, IsActive, groupId, gymCode);
        }

        public static OperationResult<bool> DisableMember(int memberId, bool activeState, string comment, string gymCode, string user)
        {
            return MembershipManager.DisableMember(memberId, activeState, comment, gymCode, user);
        }


        public static OperationResult<bool> RemoveSponsorshipOfMember(int sponsoredRecordId, string gymCode, string user)
        {
            return MembershipManager.RemoveSponsorshipOfMember(sponsoredRecordId, gymCode, user);
        }


        public static OperationResult<PackageDC> GetContractTemplateDetails(string type, int templateID, string gymCode)
        {
            return MembershipManager.GetContractTemplateDetails(type, templateID, gymCode);
        }

        public static OperationResult<List<ExcelineMemberDC>> GetExceMember()
        {
            OperationResult<List<ExcelineMemberDC>> result = new OperationResult<List<ExcelineMemberDC>>();

            List<ExcelineMemberDC> memberList = new List<ExcelineMemberDC>();

            for (int i = 1; i < 30; i++)
            {
                ExcelineMemberDC item = new ExcelineMemberDC() { Name = "Nuwan" + i, FirstName = "Nuwan" + i, LastName = "Sanjeewa" + i };
                memberList.Add(item);
            }

            result.OperationReturnValue = memberList;
            return result;
        }

        public static OperationResult<bool> MembercardVisit(int memberId, string user, string gymCode)
        {
            return MembershipManager.MembercardVisit(memberId, user, gymCode);
        }

        public static OperationResult<List<MemberForMemberlist>> GetMemberList(FilterMemberList parameters, string user, string gymCode)
        {
            return MembershipManager.GetMemberList(parameters, user, gymCode);
        }

        public static OperationResult<List<int>> GetMemberCount(string gymCode, int branchId)
        {
            return MembershipManager.GetMemberCount(gymCode, branchId);
        }

        public static OperationResult<List<ExcelineMemberDC>> GetMembers(int branchId, string searchText, int statuse, MemberSearchType searchType, MemberRole memberRole, string user, string gymCode, int hit, bool isHeaderClick, bool isAscending, string sortName)
        {
            return MembershipManager.GetMembers(branchId, searchText, statuse, searchType, memberRole, user, gymCode, hit, isHeaderClick, isAscending, sortName);
        }

        public static OperationResult<List<OrdinaryMemberDC>> GetMembersByStatus(int statusId, string systemName, string user, string gymCode)
        {
            return MembershipManager.GetMembersByStatus(statusId, systemName, user, gymCode);
        }

        public static OperationResult<List<PackageDC>> GetContracts(int branchId, string user, int contractType, string gymCode)
        {
            return MembershipManager.GetContracts(branchId, contractType, gymCode);

        }

        public static OperationResult<bool> SetContractSequenceID(List<PackageDC> contractList, int branchID, string gymCode)
        {
            return MembershipManager.SetContractSequenceID(contractList, branchID, gymCode);
        }

        public static OperationResult<bool> SaveRenewContractDetails(RenewSummaryDC renewSummary, int branchId, string gymCode, string user)
        {
            return MembershipManager.SaveRenewContractDetails(renewSummary, branchId, gymCode, user);

        }

        public static OperationResult<InstallmentDC> GetMemberContractLastInstallment(int memberContractId, string gymCode)
        {
            return MembershipManager.GetMemberContractLastInstallment(memberContractId, gymCode);
        }

        public static OperationResult<ContractSaveResultDC> SaveMemberContract(MemberContractDC memberContract, string gymCode, string user)
        {
            return MembershipManager.SaveMemberContract(memberContract, gymCode, user);
        }

        public static OperationResult<CreditNoteDC> GetCreditNoteDetails(int arItemNo, string gymCode)
        {
            return MembershipManager.GetCreditNoteDetails(arItemNo, gymCode);
        }

        public static OperationResult<MemberContractDC> GetMemberContractDetails(int contractId, int branchId, string gymCode)
        {
            return MembershipManager.GetMemberContractDetails(contractId, branchId, gymCode);
        }

        public static OperationResult<List<ContractItemDC>> GetContractTemplateItems(int contractId, int branchId, string gymCode)
        {
            return MembershipManager.GetContractTemplateItems(contractId, branchId, gymCode);
        }

        public static OperationResult<List<InstallmentDC>> GetInstallments(int memberContractId, string gymCode)
        {
            return MembershipManager.GetInstallments(memberContractId, gymCode);
        }

        public static OperationResult<bool> RenewMemberContract(MemberContractDC renewedContract, List<InstallmentDC> installmentList, string gymCode, int branchId, bool isAutoRenew, string user)
        {
            return MembershipManager.RenewMemberContract(renewedContract, installmentList, gymCode, branchId, isAutoRenew, user);
        }

        public static OperationResult<bool> UpdateMemberInstallment(InstallmentDC installment, string gymCode)
        {
            return MembershipManager.UpdateMemberInstallment(installment, gymCode);
        }

        public static OperationResult<bool> UpdateMemberAddonInstallments(List<InstallmentDC> installments, string gymCode)
        {
            return MembershipManager.UpdateMemberAddonInstallments(installments, gymCode);
        }

        public static OperationResult<int> SwitchPayer(int memberId, int payerId, DateTime activatedDate, bool isSave, string gymCode)
        {
            return MembershipManager.SwitchPayer(memberId, payerId, activatedDate, isSave, gymCode);
        }

        public static OperationResult<EntityVisitDetailsDC> GetMemberVisits(int memberId, DateTime selectedDate, int branchId, string gymCode, string user)
        {
            return MembershipManager.GetMemberVisits(memberId, selectedDate, branchId, gymCode, user);
        }

        public static OperationResult<int> SaveMemberVisit(EntityVisitDC memberVisit, string gymCode, string user)
        {
            return MembershipManager.SaveMemberVisit(memberVisit, gymCode, user);
        }

        public static OperationResult<bool> DeleteMemberVisit(int memberVisitId, int memberContractId, string gymCode)
        {
            return MembershipManager.DeleteMemberVisit(memberVisitId, memberContractId, gymCode);
        }
        public static OperationResult<bool> IntroducePayer(int memberId, int introduceId, string gymCode)
        {
            return MembershipManager.IntroducePayer(memberId, introduceId, gymCode);
        }

        public static OperationResult<bool> DeleteCreditNote(int creditNoteId, string gymCode)
        {
            return MembershipManager.DeleteCreditNote(creditNoteId, gymCode);
        }



        public static OperationResult<bool> CancelMemberContract(int memberContractId, string canceledBy, string comment, List<int> installmentIds, int minNumber, int tergetInstallmentId, string gymCode)
        {
            return MembershipManager.CancelMemberContract(memberContractId, canceledBy, comment, installmentIds, minNumber, tergetInstallmentId, gymCode);
        }

        public static OperationResult<List<MemberContractDC>> GetMemberContractsForActivity(int memberID, int activityId, int branchId, string user, string gymCode)
        {
            return MembershipManager.GetMemberContractsForActivity(memberID, activityId, branchId, user, gymCode);
        }

        public static OperationResult<List<ClassDetailDC>> GetClassByMemberId(int branchId, int memberId, DateTime? classDate, string user, string gymCode)
        {
            return MembershipManager.GetClassByMemberId(branchId, memberId, classDate, user, gymCode);
        }

        public static OperationResult<bool> AddFamilyMember(int memberId, int familyMemberId, int memberContractId, string gymCode)
        {
            return MembershipManager.AddFamilyMember(memberId, familyMemberId, memberContractId, gymCode);
        }

        public static OperationResult<List<InstallmentDC>> UpdateInstallmentWithMemberContract(decimal installmentAmount, int updatingInstallmentNo, MemberContractDC memberContract, string gymCode)
        {
            return MembershipManager.UpdateInstallmentWithMemberContract(installmentAmount, updatingInstallmentNo, memberContract, gymCode);
        }

        public static OperationResult<bool> SaveSponsor(int memberId, int sponsorId, int branchId, string user, string gymCode)
        {
            return MembershipManager.SaveSponsor(memberId, sponsorId, branchId, user, gymCode);
        }

        public static OperationResult<string> SaveMemberParent(OrdinaryMemberDC memberParent, int branchId, string user, string gymCode)
        {
            return MembershipManager.SaveMemberParent(memberParent, branchId, user, gymCode);
        }

        public static OperationResult<OrdinaryMemberDC> GetMemberDetailsByMemberId(int branchId, string user, int memberId, string memberRole, string gymCode)
        {
            return MembershipManager.GetMemberDetailsByMemberId(branchId, user, memberId, memberRole, gymCode);
        }

        public static OperationResult<List<CategoryDC>> GetInterestCategoryByMember(int memberId, int branchId, string gymCode)
        {
            return MembershipManager.GetInterestCategoryByMember(memberId, branchId, gymCode);
        }

        public static OperationResult<bool> SaveInterestCategoryByMember(int memberId, int branchId, List<CategoryDC> interestCategoryList, string gymCode)
        {
            return MembershipManager.SaveInterestCategoryByMember(memberId, branchId, interestCategoryList, gymCode);
        }

        public static OperationResult<List<OrdinaryMemberDC>> GetSwitchPayers(int branchId, int memberId, string gymCode)
        {
            return MembershipManager.GetSwitchPayers(branchId, memberId, gymCode);
        }

        public static OperationResult<List<ExcelineMemberDC>> GetIntroducedMembers(int memberId, int branchId, string gymCode)
        {
            return MembershipManager.GetIntroducedMembers(memberId, branchId, gymCode);
        }

        public static OperationResult<bool> UpdateIntroducedMembers(int memberId, DateTime? creditedDate, string creditedText, bool isDelete, string gymCode)
        {
            return MembershipManager.UpdateIntroducedMembers(memberId, creditedDate, creditedText, isDelete, gymCode);
        }

        public static OperationResult<Dictionary<int, string>> GetMemberStatus(string gymCode, bool getAllStatuses)
        {
            return MembershipManager.GetMemberStatus(gymCode, getAllStatuses);
        }
        #endregion

        #region Sponser Contract
        public static OperationResult<List<DiscountDC>> GetGroupDiscountByType(int branchId, string user, int discountTypeId, string gymCode, int sponsorId)
        {
            return MembershipManager.GetGroupDiscountByType(branchId, user, discountTypeId, gymCode, sponsorId);
        }


        public static OperationResult<SponsorSettingDC> GetSponsorSetting(int branchId, string user, int sponsorId, string gymCode)
        {
            return MembershipManager.GetSponsorSetting(branchId, user, sponsorId, gymCode);
        }

        public static OperationResult<List<int>> GetSponsoredMemberListByTimePeriod(int sponsorId, Dictionary<int, List<DateTime>> sponsorMembers, string gymcode)
        {
            return MembershipManager.GetSponsoredMemberListByTimePeriod(sponsorId, sponsorMembers, gymcode);
        }

        public static OperationResult<List<ExcelineMemberDC>> GetMembersBySponsorId(int sponsorId, int branchId, string gymcode)
        {
            return MembershipManager.GetMembersBySponsorId(sponsorId, branchId, gymcode);
        }

        public static OperationResult<bool> SaveSponsorSetting(SponsorSettingDC contractSetting, int branchId, string user, string gymCode)
        {
            return MembershipManager.SaveSponsorSetting(contractSetting, branchId, user, gymCode);
        }
        public static OperationResult<bool> AddSponserShipForMember(int memContractId, int sponserContractId, int employeeCategotyID, int actionType, string employeeRef, string gymCode)
        {
            return MembershipManager.AddSponserShipForMember(memContractId, sponserContractId, employeeCategotyID, actionType, employeeRef, gymCode);
        }

        public static OperationResult<bool> DeleteDiscount(List<int> discountIDList, string gymCode)
        {
            return MembershipManager.DeleteDiscount(discountIDList, gymCode);
        }

        public static OperationResult<bool> DeleteEmployeeCategory(int categoryID, string gymCode)
        {
            return MembershipManager.DeleteEmployeeCategory(categoryID, gymCode);
        }

        public static OperationResult<int> SaveSponserContract(SponsorSettingDC sponserContract, int branchId, string user, string gymCode)
        {
            return MembershipManager.SaveSponserContract(sponserContract, branchId, user, gymCode);
        }

        public static OperationResult<List<OrdinaryMemberDC>> GetMembersByRoleType(int branchId, string user, int status, MemberRole roleType, string searchText, string gymCode)
        {
            return MembershipManager.GetMembersByRoleType(branchId, user, status, roleType, searchText, gymCode);
        }

        public static OperationResult<List<MemberForMemberlist>> GetVisitedMembers(string user, string gymCode)
        {
            return MembershipManager.GetVisitedMembers(user, gymCode);
        }

        public static OperationResult<List<EmployeeCategoryDC>> GetSponsorEmployeeCategoryList(int branchId, string gymcode, int sponsorId)
        {
            return MembershipManager.GetSponsorEmployeeCategoryList(branchId, gymcode, sponsorId);
        }

        public static OperationResult<OrdinaryMemberDC> GetEmployeeCategoryBySponsorId(int branchId, string gymcode, int sponsorId)
        {
            return MembershipManager.GetEmployeeCategoryBySponsorId(branchId, gymcode, sponsorId);
        }

        public static OperationResult<List<MemberContractDC>> GetSponserContractsByActivity(int sponserId, int activityId, int branchId, string gymCode)
        {
            return MembershipManager.GetSponserContractsByActivity(sponserId, activityId, branchId, gymCode);
        }

        public static OperationResult<bool> CancelSponsorContract(int contractId, string user, string comment, string gymCode)
        {
            return MembershipManager.CancelSponsorContract(contractId, user, comment, gymCode);
        }
        #endregion

        #region Manage Discounts
        public static OperationResult<bool> AddMemberToGroup(List<int> memberIdList, int groupId, string user, string gymCode)
        {
            return MembershipManager.AddMemberToGroup(memberIdList, groupId, user, gymCode);
        }

        public static OperationResult<bool> RemoveMemberFromGroup(List<int> memberList, int groupId, string user, string gymCode)
        {
            return MembershipManager.RemoveMemberFromGroup(memberList, groupId, user, gymCode);
        }

        public static OperationResult<List<DiscountDC>> GetDiscountList(int branchId, DiscountType discountType, string gymCode)
        {
            return MembershipManager.GetDiscountList(branchId, discountType, gymCode);
        }

        public static OperationResult<int> SaveDiscount(List<DiscountDC> discountList, string user, int branchId, int contractSettingID, string gymCode)
        {
            return MembershipManager.SaveDiscount(discountList, user, branchId, contractSettingID, gymCode);
        }

        public static OperationResult<List<DiscountDC>> GetDiscountsForActivity(int branchId, int activityId, string gymCode)
        {
            return MembershipManager.GetDiscountsForActivity(branchId, activityId, gymCode);
        }

        public static OperationResult<ShopSalesDC> GetMemberPerchaseHistory(int memberId, DateTime fromDate, DateTime toDate, string gymCode, int branchId, string user)
        {
            return MembershipManager.GetMemberPerchaseHistory(memberId, fromDate, toDate, gymCode, branchId, user);
        }

        #endregion

        #region Freeze Contract

        public static OperationResult<int> SaveContractFreezeItem(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> freezeInstallments, string user, string gymCode)
        {
            return MembershipManager.SaveContractFreezeItem(freezeItem, freezeInstallments, user, gymCode);
        }

        public static OperationResult<List<ContractFreezeItemDC>> GetContractFreezeItems(int memberContractId, string user, string gymCode)
        {
            return MembershipManager.GetContractFreezeItems(memberContractId, user, gymCode);
        }

        public static OperationResult<List<ContractFreezeInstallmentDC>> GetContractFreezeInstallments(int freezeItemId, string user, string gymCode)
        {
            return MembershipManager.GetContractFreezeInstallments(freezeItemId, user, gymCode);
        }

        public static OperationResult<List<InstallmentDC>> GetInstallmentsToFreeze(int memberContractId, string user, string gymCode)
        {
            return MembershipManager.GetInstallmentsToFreeze(memberContractId, user, gymCode);
        }

        public static OperationResult<int> ExtendContractFreezeItem(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> freezeInstallments, string user, string gymCode)
        {
            return MembershipManager.ExtendContractFreezeItem(freezeItem, freezeInstallments, user, gymCode);
        }

        public static OperationResult<int> UnfreezeContract(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> unfreezeInstallments, string user, string gymCode)
        {
            return MembershipManager.UnfreezeContract(freezeItem, unfreezeInstallments, user, gymCode);
        }

        public static OperationResult<int> IsFreezeAllowed(int memberContractId, int memberId, string gymCode, int branchId)
        {
            return MembershipManager.IsFreezeAllowed(memberContractId, memberId, gymCode, branchId);
        }

        #endregion

        #region Payments History
        public static OperationResult<List<ExcePaymentInfoDC>> GetMemberPaymentsHistory(int memberId, string paymentType, string gymCode, int hit, string user)
        {
            return MembershipManager.GetMemberPaymentsHistory(memberId, paymentType, gymCode, hit, user);
        }

        #endregion

        public static OperationResult<bool> SaveMemberInstallments(List<InstallmentDC> installmentList, bool initialGenInstmnts, DateTime startDate, DateTime endDate, int memberContractId, int membercontractNo, int branchId, string gymCode)
        {
            return MembershipManager.SaveMemberInstallments(installmentList, initialGenInstmnts, startDate, endDate, memberContractId, membercontractNo, branchId, gymCode);
        }

        public static OperationResult<int> SaveNote(int branchId, string use, int memberId, string note, string gymCode)
        {
            return MembershipManager.SaveNote(branchId, use, memberId, note, gymCode);
        }

        public static OperationResult<OrdinaryMemberDC> GetNote(int branchId, string user, int memberId, string gymCode)
        {
            return MembershipManager.GetNote(branchId, user, memberId, gymCode);
        }

        public static OperationResult<string> GetInvoicePathByARitemNo(int arItemNo, string gymCode)
        {
            return MembershipManager.GetInvoicePathByARitemNo(arItemNo, gymCode);
        }


        public static OperationResult<List<ExcelineMemberDC>> GetGroupMembers(int groupId, string gymCode)
        {
            return MembershipManager.GetGroupMembers(groupId, gymCode);
        }

        public static OperationResult<List<ContractBookingDC>> GetContractBookings(int membercontractId, string gymCode)
        {
            return MembershipManager.GetContractBookings(membercontractId, gymCode);
        }

        public static OperationResult<List<ExcelineMemberDC>> GetFamilyMembers(int memberId, string gymCode)
        {
            return MembershipManager.GetFamilyMembers(memberId, gymCode);
        }

        public static OperationResult<List<FollowUpDC>> GetFollowUps(int memberId, int followUpId, string gymCode, string user)
        {
            return MembershipManager.GetFollowUps(memberId, followUpId, gymCode, user);
        }


        public static OperationResult<bool> SaveFollowUpPopup(int followUpId, string gymCode)
        {
            return MembershipManager.SaveFollowUpPopup(followUpId, gymCode);
        }

        public static OperationResult<List<FollowUpTemplateTaskDC>> GetFollowUpTask(string gymCode)
        {
            return MembershipManager.GetFollowUpTask(gymCode);
        }

        public static OperationResult<int> SaveFollowUp(List<FollowUpDC> followUpList, string gymCode, string user)
        {
            return MembershipManager.SaveFollowUp(followUpList, gymCode, user);
        }

        public static OperationResult<List<RenewContractDC>> GetContractsForAutoRenew(int branchId, string gymCode)
        {
            return MembershipManager.GetContractsForAutoRenew(branchId, gymCode);
        }

        public static OperationResult<List<ExcelineMemberDC>> GetListOfGuardian(int guardianId, string gymCode)
        {
            return MembershipManager.GetListOfGuardian(guardianId, gymCode);
        }

        public static OperationResult<double> ChangePrePaidAccount(string incrase, float amount, int memberId, int branchId, string gymCode, string user)
        {
            return MembershipManager.ChangePrePaidAccount(incrase, amount, memberId, branchId, gymCode, user);
        }

        public static OperationResult<bool> SaveEconomyDetails(OrdinaryMemberDC member, string gymCode, string user)
        {
            return MembershipManager.SaveEconomyDetails(member, gymCode, user);
        }

        public static OperationResult<OrdinaryMemberDC> GetEconomyDetails(int memberId, string gymCode)
        {
            return MembershipManager.GetEconomyDetails(memberId, gymCode);
        }

        public static OperationResult<string> SaveMemberInfoWithNotes(OrdinaryMemberDC member, string gymCode)
        {
            return MembershipManager.SaveMemberInfoWithNotes(member, gymCode);
        }

        public static OperationResult<OrdinaryMemberDC> GetMemberInfoWithNotes(int branchId, int memberId, string gymCode)
        {
            return MembershipManager.GetMemberInfoWithNotes(branchId, memberId, gymCode);
        }

        public static OperationResult<int> UpdateMemberContract(MemberContractDC memberContract, string gymCode, string user)
        {
            return MembershipManager.UpdateMemberContract(memberContract, gymCode, user);
        }

        public static OperationResult<bool> DeleteInstallment(List<int> installmentIdList, int memberContractId, string gymCode, string user)
        {
            return MembershipManager.DeleteInstallment(installmentIdList, memberContractId, gymCode, user);
        }

        public static OperationResult<bool> ResignContract(ContractResignDetailsDC resignDetails, string gymCode, string user)
        {
            return MembershipManager.ResignContract(resignDetails, gymCode, user);
        }

        public static OperationResult<List<InstallmentDC>> GetMemberOrders(int memberIdc, string type, string gymCode, string user)
        {
            return MembershipManager.GetMemberOrders(memberIdc, type, gymCode, user);
        }

        public static OperationResult<string> GetContractMembers(int memberContractId, string gymCode)
        {
            return MembershipManager.GetContractMembers(memberContractId, gymCode);
        }

        public static OperationResult<bool> UpdateContractOrder(List<InstallmentDC> installmentList, int branchId, string gymCode, string user)
        {
            return MembershipManager.UpdateContractOrder(installmentList, gymCode, user);
        }

        public static OperationResult<int> SaveContractGroupMembers(int groupId, List<ExcelineMemberDC> groupMemberList, int contractId, string gymCode, string user)
        {
            return MembershipManager.SaveContractGroupMembers(groupId, groupMemberList, contractId, gymCode, user);
        }

        public static OperationResult<bool> CheckBookingAvailability(int resourceID, string day, DateTime startTime, DateTime endTime, string gymCode)
        {
            return MembershipManager.CheckBookingAvailability(resourceID, day, startTime, endTime, gymCode);
        }

        public static OperationResult<int> AddInstallment(InstallmentDC installment, string gymCode, string user)
        {
            return MembershipManager.AddInstallment(installment, gymCode, user);
        }

        public static OperationResult<int> AddBlancInstallment(int MemberId, int BranchId, string gymCode, string user)
        {
            return MembershipManager.AddBlancInstallment(MemberId, BranchId, gymCode, user);
        }

        public static OperationResult<string> GetMemberSearchCategory(int branchId, string gymCode)
        {
            return MembershipManager.GetMemberSearchCategory(branchId, gymCode);
        }

        public static OperationResult<bool> SaveRenewMemberContractItem(List<ContractItemDC> itemList, int memberContractId, int branchId, string gymCode)
        {
            return MembershipManager.SaveRenewMemberContractItem(itemList, memberContractId, branchId, gymCode);
        }

        public static OperationResult<bool> SaveDocumentData(DocumentDC document, string username, int branchId)
        {
            return MembershipManager.SaveDocumentData(document, username, branchId);
        }

        public static OperationResult<List<DocumentDC>> GetDocumentData(bool isActive, string custId, string searchText, int documentType, string gymCode, int branchId)
        {
            return MembershipManager.GetDocumentData(isActive, custId, searchText, documentType, gymCode, branchId);
        }

        public static OperationResult<bool> DeleteDocumentData(int docId, string username, int branchId)
        {
            return MembershipManager.DeleteDocumentData(docId, username, branchId);
        }

        public static object SetPDFFileParth(int id, string fileParth, string flag, int branchId, string GymCode, string user)
        {
            return MembershipManager.SetPDFFileParth(id, fileParth, flag, branchId, GymCode, user);
        }

        public static OperationResult<CustomerBasicInfoDC> GetMemberBasicInfo(int memberId, string gymcode)
        {
            return MembershipManager.GetMemberBasicInfo(memberId, gymcode);
        }

        public static OperationResult<List<ContractSummaryDC>> GetContractSummaries(int memberId, int branchId, string gymCode, bool isFreezDetailNeeded, string user, bool isFreezdView)
        {
            return MembershipManager.GetContractSummaries(memberId, branchId, gymCode, isFreezDetailNeeded, user, isFreezdView);
        }

        public static OperationResult<int> GetVisitCount(int memberId, DateTime startDate, DateTime endDate, string gymCode)
        {
            return MembershipManager.GetVisitCount(memberId, startDate, endDate, gymCode);
        }

        public static OperationResult<List<InstallmentDC>> MergeOrders(List<InstallmentDC> updatedOrders, List<int> removedOrder, int memberContractId, string gymCode, string user)
        {
            return MembershipManager.MergeOrders(updatedOrders, removedOrder, memberContractId, gymCode, user);
        }

        public static OperationResult<List<FollowUpDetailDC>> GetFollowUpDetailByEmpId(int empId, string gymCode)
        {
            return MembershipManager.GetFollowUpDetailByEmpId(empId, gymCode);
        }

        public static OperationResult<List<MemberBookingDC>> GetMemberBookings(int memberId, string gymCode, string user)
        {
            return MembershipManager.GetMemberBookings(memberId, gymCode, user);
        }

        public static OperationResult<List<ExcelineMemberDC>> GetOtherMembersForBooking(int schdeduleId, string gymCode)
        {
            return MembershipManager.GetOtherMembersForBookging(schdeduleId, gymCode);
        }

        public static OperationResult<bool> ValidateMemberWithCard(string cardNo, string accessType, string gymCode, int branchId, string user)
        {
            return MembershipManager.ValidateMemberWithCard(cardNo, accessType, gymCode, branchId, user);
        }

        public static OperationResult<ExceACCMember> ValidateMemberWithCardForExceline(string cardNo, string accessType, int terminalId, string gymCode, int branchId, string user)
        {
            return MembershipManager.ValidateMemberWithCardForExcAccessControl(cardNo, accessType, terminalId, gymCode, branchId, user);
        }

        public static OperationResult<List<InstallmentDC>> GetSponsorOrders(List<int> sponsors, string gymCode)
        {
            return MembershipManager.GetSponsorOrders(sponsors, gymCode);
        }

        public static OperationResult<int> AddMachineSale(int articleNo, int memberId, string articleName, decimal amount, string user, string gymCode, int brachId, int salepointId)
        {
            return MembershipManager.AddMachineSale(articleNo, memberId, articleName, amount, user, gymCode, brachId, salepointId);
        }

        public static OperationResult<SaleResultDC> AddSunBedShopSale(SunBedBusinessHelperObject businessHelperObj)
        {
            return MembershipManager.AddSunBedShopSale(businessHelperObj);
        }

        public static OperationResult<List<MemberIntegrationSettingDC>> GetMemberIntegrationSettings(int branchId, int memberId, string gymCode)
        {
            return MembershipManager.GetMemberIntegrationSettings(branchId, memberId, gymCode);
        }

        public static OperationResult<bool> WellnessIntegrationOperation(OrdinaryMemberDC member, MemberIntegrationSettingDC wellnessIntegrationSetting, WellnessOperationType wellnessOperationType, string gymCode, int branchId, string user, EntityVisitDC memberVisit = null, TrainingProgramClassVisitDetailDC classVisitDetail = null)
        {
            return MembershipManager.WellnessIntegrationOperation(member, wellnessIntegrationSetting, wellnessOperationType, gymCode, branchId, user, memberVisit, classVisitDetail);
        }

        public static OperationResult<int> ValidateChangeGymInMember(int currentBranchID, int newBranchID, int branchID, int memberID, string user, string gymCode)
        {
            return MembershipManager.ValidateChangeGymInMember(currentBranchID, newBranchID, branchID, memberID, user, gymCode);
        }

        public static OperationResult<int> CheckContractGymChange(int memberID, int memberContractID, int oldBranchID, int newBranchID, string gymCode)
        {
            return MembershipManager.CheckContractGymChange(memberID, memberContractID, oldBranchID, newBranchID, gymCode);
        }

        public static OperationResult<int> ValidateMemberWithStatus(int memberId, int statusId, string user, string gymCode)
        {
            return MembershipManager.ValidateMemberWithStatus(memberId, statusId, user, gymCode);
        }

        public static OperationResult<ArticleDC> GetTimeMachineArticl(string gymCode)
        {
            return MembershipManager.GetTimeMachineArticle(gymCode);
        }

        public static OperationResult<bool> UpdateShopItemAndGatPurchase(GatpurchaseShopItem gatPurchaseShopItem)
        {
            return MembershipManager.UpdateShopItemAndGatPurchase(gatPurchaseShopItem);
        }

        public static OperationResult<int> GetPriceChangeContractCount(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode, bool changeToNextTemplate)
        {
            return MembershipManager.GetPriceChangeContractCount(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, gymCode, changeToNextTemplate);
        }


        //public void AccessToken(string code)
        //{
        //    string url = @"https://auth.infogym.no/token";
        //    WebClient client = new WebClient();
        //    string ApiKey = "538321a55f486706a909d81d";
        //    string ApiSecret = "sQ8Fs3RYxptQ6tuzaLrNZQWLzFeyfxqtXpcQ9fvLcmUVixj8AxH6vZIX45Td0H9z";
        //    string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(ApiKey + ":" + ApiSecret));
        //    client.Headers[HttpRequestHeader.Authorization] = "Basic " + credentials;
        //    client.Headers[HttpRequestHeader.Accept] = "application/json";
        //    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
        //    client.Headers[HttpRequestHeader.Date] = " Mon, 25 Feb 2013 12:00:00 GMT";
        //    client.Headers[HttpRequestHeader.Host] = "auth.infogym.no";
        //    client.Headers[HttpRequestHeader.Connection] = "close";
        //    client.Headers["grant_type"] = "authorization_code";

        //    string data = string.Format(
        //        "grant_type=urn:infogym:oauth:grant_type:organization&org=538322585f486706a909d81e&scope=members:read",
        //        code);
        //    var result = client.UploadString(url, data);
        //}


        public static OperationResult<bool> UpdatePrice(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode, bool changeToNextTemplate, string user)
        {
            return MembershipManager.UpdatePrice(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, gymCode, changeToNextTemplate, user);
        }

        public static OperationResult<List<ExcelineContractDC>> GetPriceUpdateContracts(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode, bool changeToNextTemplate, int hit)
        {
            return MembershipManager.GetPriceUpdateContracts(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, gymCode, changeToNextTemplate, hit);
        }

        public static OperationResult<List<PackageDC>> GetTemplatesForPriceUpdate(string gymCode)
        {
            return MembershipManager.GetTemplatesForPriceUpdate(gymCode);
        }

        public static string InfoGymGetAccessToken(string clientId, string clientSecret, string host, string data)
        {
            return MembershipManager.InfoGymGetAccessToken(clientId, clientSecret, host, data);
        }

        public static string InfoGymRegisterMember(InfoGymMember member, string url, string host, string accessToken)
        {
            return MembershipManager.InfoGymRegisterUpdateMember(member, url, host, accessToken, HttpMethod.Post);
        }

        public static string InfoGymUpdateMember(InfoGymMember member, string url, string host, string accessToken)
        {
            return MembershipManager.InfoGymRegisterUpdateMember(member, url, host, accessToken, HttpMethod.Put);
        }

        public static bool InfoGymUpdateExceMember(string infoGymId, int memberId, string user)
        {
            return MembershipManager.InfoGymUpdateExceMember(infoGymId, memberId, user);
        }

        public static OperationResult<List<ContractSummaryDC>> GetContractSummariesByEmployee(int employeeID, int branchId, DateTime createdDate, string gymCode)
        {
            return MembershipManager.GetContractSummariesByEmployee(employeeID, branchId, createdDate, gymCode);
        }

        public static OperationResult<DateTime?> ValidateSponsoringGeneration(string gymCode)
        {
            return MembershipManager.ValidateSponsoringGeneration(gymCode);
        }

        public static OperationResult<string> GetCountryCode(string landCode, string gymCode)
        {
            return MembershipManager.GetCountryCode(landCode, gymCode);
        }

        public static OperationResult<bool> UpdateAgressoId(int memberID, int aggressoID, string gymCode)
        {
            return MembershipManager.UpdateAgressoId(memberID, aggressoID, gymCode);
        }

        public static OperationResult<bool> UpdateBRISStatus(int brisID, string message, string response, string status, string gymCode)
        {
            return MembershipManager.UpdateBRISStatus(brisID, message, response, status, gymCode);
        }

        public static OperationResult<bool> RemoveContractResign(int memberContractId, string user, string gymCode)
        {
            return MembershipManager.RemoveContractResign(memberContractId, user, gymCode);
        }

        public static OperationResult<bool> UpdateInvoicePdfPath(int arItemNo, string path, string gymCode)
        {
            return MembershipManager.UpdateInvoicePdfPath(arItemNo, path, gymCode);
        }

        public static OperationResult<bool> ImageChangedAddEvent(USNotificationTree notification, string gymcode, string user)
        {
            return MembershipManager.ImageChangedAddEvent(notification, gymcode, user);
        }

        public static OperationResult<bool> RegisterChangeOfInvoiceDueDate(AlterationInDueDateData changeData, String user, String gymCode)
        {
            return MembershipManager.RegisterChangeOfInvoiceDueDate(changeData, user, gymCode);
        } 
    }
}
