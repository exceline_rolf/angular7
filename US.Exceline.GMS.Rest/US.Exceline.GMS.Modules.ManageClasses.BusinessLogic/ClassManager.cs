﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;


namespace US.Exceline.GMS.Modules.ManageClasses.BusinessLogic
{
    public class ClassManager
    {
        public static  OperationResult<List<ExcelineClassDC>> SearchClass(string category, string searchText, string gymCode)
        {
            OperationResult<List<ExcelineClassDC>> result = new OperationResult<List<ExcelineClassDC>>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.SearchClass(category, searchText, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Searching Classes" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<List<ExcelineClassDC>> GetClasses(string className, int branchId, string gymCode)
        {
            OperationResult<List<ExcelineClassDC>> result = new OperationResult<List<ExcelineClassDC>>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.GetClasses(className, branchId, gymCode);
            }

            catch(Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Classes" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<List<ExcelineClassDC>> SearchClass(string category, string searchText, DateTime startDate, string gymCode)
        {
            OperationResult<List<ExcelineClassDC>> result = new OperationResult<List<ExcelineClassDC>>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.SearchClass(category, searchText, startDate, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Searching Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<List<ExcelineClassDC>> SearchClass(string category, string searchText, DateTime startDate, DateTime endDate, string gymCode)
        {
            OperationResult<List<ExcelineClassDC>> result = new OperationResult<List<ExcelineClassDC>>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.SearchClass(category, searchText, startDate, endDate, gymCode);

            }catch(Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Searching Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<int> SaveClass(ExcelineClassDC excelineClass, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.SaveClass(excelineClass, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Saving Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<bool> UpdateClass(ExcelineClassDC excelineClass, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.UpdateClass(excelineClass, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Updateing Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<bool> DeleteClass(int classId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.DeleteClass(classId, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Deleting Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }       

        public static  OperationResult<int> GetNextClassId(string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.GetNextClassId(gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Next ClassId" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineClassActiveTimeDC>> GetExcelineClassActiveTimes(int branchId, DateTime startTime, DateTime endTime, int entNO, string gymCode)
        {
            OperationResult<List<ExcelineClassActiveTimeDC>> result = new OperationResult<List<ExcelineClassActiveTimeDC>>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.GetExcelineClassActiveTimes(branchId, startTime, endTime, entNO, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Getting classes Active Times " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineMemberDC>> GetMembersForClass(int classId, string gymCode)
        {
            OperationResult<List<ExcelineMemberDC>> result = new OperationResult<List<ExcelineMemberDC>>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.GetMembersForClass(classId, gymCode);
            }

            catch(Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting MembersForClass" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<List<TrainerDC>> GetTrainersForClass(int classId, string gymCode)
        {
            OperationResult<List<TrainerDC>> result = new OperationResult<List<TrainerDC>>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.GetTrainersForClass(classId, gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Trainers For Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<List<InstructorDC>> GetInstructorsForClass(int classId, string gymCode)
        {
            OperationResult<List<InstructorDC>> result = new OperationResult<List<InstructorDC>>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.GetInstructorsForClass(classId, gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Instructors For Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<List<ResourceDC>> GetResourcesForClass(int classId, string gymCode)
        {
            OperationResult<List<ResourceDC>> result = new OperationResult<List<ResourceDC>>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.GetresourcesForClass(classId, gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Resources For Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<ScheduleDC> GetClassSchedule(int classId, string gymCode)
        {
            OperationResult<ScheduleDC> result = new OperationResult<ScheduleDC>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.GetClassSchedule(classId, gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Class Schedule" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<List<ExcelineClassActiveTimeDC>> GetClassHistory(int classId, List<ScheduleItemDC> scheduleItemList, string gymCode)
        {
            OperationResult<List<ExcelineClassActiveTimeDC>> result = new OperationResult<List<ExcelineClassActiveTimeDC>>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.GetClassHistory(classId, scheduleItemList, gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Class History" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<List<OrdinaryMemberDC>> GetMembersForClassBooking(int classId, int branchId, string searchText, string user, string gymCode)
        {
            OperationResult<List<OrdinaryMemberDC>> result = new OperationResult<List<OrdinaryMemberDC>>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.GetMembersForClassBooking(classId, branchId, searchText, gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Members For Class Booking" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ClassBookingActiveTimeDC>> GetClassActiveTimesForBooking(int branchId, DateTime startTime, DateTime endTime, int entNO, string gymCode)
        {
            OperationResult<List<ClassBookingActiveTimeDC>> result = new OperationResult<List<ClassBookingActiveTimeDC>>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.GetClassActiveTimesForBooking(branchId, startTime, endTime, entNO, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Getting class booking Active Times " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<MemberBookingDetailsDC>> GetMemberBookingDetails(int classId, int branchId, string user, string gymCode)
        {
            OperationResult<List<MemberBookingDetailsDC>> result = new OperationResult<List<MemberBookingDetailsDC>>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.GetMemberBookingDetails(classId, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Getting booking member details " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveMemberBookingDetails(int classId, OrdinaryMemberDC ordinaryMemberDC, int memberId, List<ClassBookingDC> bookingList, decimal totalBookingAmount, decimal totalAvailableAmount, string user, int branchId, string scheduleCategoryType, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.SaveMemberBookingDetails(classId, ordinaryMemberDC, memberId, bookingList, totalBookingAmount, totalAvailableAmount, scheduleCategoryType, user, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Saving booking details " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<bool> ApplyClassScheduleForNextYearAction(int classId, DateTime startdate, DateTime enddate, string name, int branchId, string createdUser, List<ScheduleItemDC> scheduleItemList, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.ApplyClassScheduleForNextYearAction(classId, startdate, enddate, name, branchId, createdUser, scheduleItemList, gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Applying Class Schedule For Next Year Action" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<bool> GetVerificationOfAppliedClassScheduleAction(int classScheduleId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.GetVerificationOfAppliedClassScheduleAction(classScheduleId, gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Verification Of Applied Class Schedule Action" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<List<ArticleDC>> GetArticlesForClass(string gymCode)
        {
            OperationResult<List<ArticleDC>> result = new OperationResult<List<ArticleDC>>();
            try
            {
                result.OperationReturnValue= ManageClassFacade.GetArticlesForClass(gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Articles For Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveClassBookingPayment(int memberId, int classId, BookingArticleDC bookingArticle, List<ClassBookingDC> PaidBookings, List<BookingPaymentDC> bookingPayments, decimal totalAmount, decimal paidAmount, decimal defaultPrice, string user, int branchId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.SaveClassBookingPayment(memberId, classId, bookingArticle, PaidBookings, bookingPayments, totalAmount, paidAmount, defaultPrice, user, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Saving booking Payments " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> GetExcelineClassIdByName(string className, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageClassFacade.GetExcelineClassIdByName(className, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Getting Exceline ClassId By Name" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
       
    }
}
 