﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US.Common.Notification.API;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Exceline.GMS.Modules.Admin.API.ManageEmployees;
using US.Exceline.GMS.Modules.Login.API;
using US.Exceline.GMS.Modules.Shop.API.InventoryManagement;
using US.Exceline.GMS.Modules.Shop.API.ManageShop;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.Exceline.GMS.Modules.Admin.API.ManageResources;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Core.ResultNotifications;
using US.Common.Logging.API;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.Exceline.GMS.Modules.Admin.API.ManageContract;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.API;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.SystemObjects;
using System.Configuration;
using US.Exceline.GMS.Modules.Admin.API.ManageSystemSettings;
using US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership;
using US.Exceline.GMS.Modules.Admin.API.ManageMembers;
using US.GMS.Core.DomainObjects.Admin.HolydayManagement;
using US.Exceline.GMS.Modules.Admin.API.ManageHolidays;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.DomainObjects.ManageShop;
using US.Exceline.GMS.Modules.Admin.API.ManageInventoryDiscounts;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.Utils;
using US.GMS.Core.DomainObjects.Admin.ManageClassSettings;
using US.Exceline.GMS.Modules.Admin.API.ManageJobs;
using US.Payment.Core.Enums;
using US.Payment.Core.ResultNotifications;
using US.Common.Web.UI.Core.SystemObjects;
using US.Common.USSAdmin.API;
using US.Communication.Core.SystemCore.DomainObjects;
using US.Communication.API;
using System.Linq;
using SessionManager;
using System.Threading;
using System.IO;
using US.Common.Web.UI.Core.Util;
using US.GMS.Core.DomainObjects.Admin.ManageAnonymizing;
using US.Exceline.GMS.Modules.Admin.API.ManageAnonymizing;


namespace US.Exceline.GMS.Modules.Admin.Service
{
    public class AdminService : IAdminService
    {

        public List<InstructorDC> GetInstructors(int branchId, string searchText, string user, bool isActive, int instructorId)
        {
            OperationResult<List<InstructorDC>> result = GMSInstructor.GetInstructors(branchId, searchText, isActive, instructorId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InstructorDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #region Employee
        public string SaveGymEmployee(GymEmployeeDC employee, string user)
        {
            OperationResult<string> result = GMSGymEmployee.SaveGymEmployee(employee, GetConfigurations(user), ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return "-1";
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        private string GetConfigurations(string user)
        {
            string imageSavepath = string.Empty;
            try
            {
                imageSavepath = ConfigurationSettings.AppSettings["ImageFolderPath"].ToString();
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Error in getting Image folder path", ex, user);
            }
            return imageSavepath;
        }

        public int UpdateGymEmployee(GymEmployeeDC employee, int branchId, string user)
        {
            OperationResult<int> result = GMSGymEmployee.UpdateGymEmployee(employee, branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeActivateGymEmployee(int employeeId, int branchId, string user)
        {
            OperationResult<bool> result = GMSGymEmployee.DeActivateGymEmployee(employeeId, branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<GymEmployeeDC> GetGymEmployees(int branchId, string searchText, string user, bool isActive, int roleId)
        {
            OperationResult<List<GymEmployeeDC>> result = GMSGymEmployee.GetGymEmployees(branchId, searchText, isActive, roleId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<GymEmployeeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public GymEmployeeDC GetGymEmployeeByEmployeeId(int branchId, string user, int employeeId)
        {
            OperationResult<GymEmployeeDC> result = GMSGymEmployee.GetGymEmployeeByEmployeeId(branchId, user, employeeId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new GymEmployeeDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public bool ValidateEmployeeFollowUp(int employeeId, DateTime endDate, string user)
        {
            OperationResult<bool> result = GMSGymEmployee.ValidateEmployeeFollowUp(employeeId, endDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<GymEmployeeRoleDurationDC> GetGymEmployeeRoleDuration(int employeeId, string user)
        {
            OperationResult<List<GymEmployeeRoleDurationDC>> result = GMSGymEmployee.GetGymEmployeeRoleDuration(employeeId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<GymEmployeeRoleDurationDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<GymEmployeeWorkPlanDC> GetGymEmployeeWorkPlan(int employeeId, string user)
        {
            OperationResult<List<GymEmployeeWorkPlanDC>> result = GMSGymEmployee.GetGymEmployeeWorkPlan(employeeId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<GymEmployeeWorkPlanDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<GymEmployeeApprovalDC> GetGymEmployeeApprovals(int employeeId, string approvalType, string user)
        {
            OperationResult<List<GymEmployeeApprovalDC>> result = GMSGymEmployee.GetGymEmployeeApprovals(employeeId, approvalType, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<GymEmployeeApprovalDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public bool AddEmployeeTimeEntry(EmployeeTimeEntryDC timeEntry, string user)
        {
            OperationResult<bool> result = GMSGymEmployee.AddEmployeeTimeEntry(timeEntry, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<EmployeeTimeEntryDC> GetEmployeeTimeEntries(int employeeId, string user)
        {
            OperationResult<List<EmployeeTimeEntryDC>> result = GMSGymEmployee.GetEmployeeTimeEntries(employeeId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<EmployeeTimeEntryDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }



        }

        public bool ApproveEmployeeTimes(int employeeId, bool isAllApproved, string timeIdString, string user)
        {
            OperationResult<bool> result = GMSGymEmployee.ApproveAllEmployeeTimes(employeeId, isAllApproved, timeIdString, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteGymEmployeeActiveTime(int activeTimeId, string user)
        {
            OperationResult<bool> result = GMSGymEmployee.DeleteGymEmployeeActiveTime(activeTimeId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        public List<EmployeeEventDC> GetEmployeeEvents(int employeeId, string user)
        {
            OperationResult<List<EmployeeEventDC>> result = GMSGymEmployee.GetEmployeeEvents(employeeId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<EmployeeEventDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<EmployeeJobDC> GetEmployeeJobs(int employeeId, string user)
        {
            OperationResult<List<EmployeeJobDC>> result = GMSGymEmployee.GetEmployeeJobs(employeeId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<EmployeeJobDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }

        }
        public List<EmployeeClass> GetEmployeeClasses(int empId, string user)
        {
            OperationResult<List<EmployeeClass>> result = GMSEmployee.GetEmployeeClasses(empId, user.Split('/')[0].Trim());
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<EmployeeClass>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int UpdateApproveStatus(bool IsApproved, int EntityActiveTimeID, string user)
        {
            OperationResult<int> result = GMSEmployee.UpdateApproveStatus(IsApproved, EntityActiveTimeID, user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineRoleDc> GetGymEmployeeRolesById(int employeeId, int branchId, string user)
        {
            OperationResult<List<ExcelineRoleDc>> result = GMSEmployee.GetGymEmployeeRolesById(employeeId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineRoleDc>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<EmployeeBookingDC> GetEmployeeBookings(int employeeId, string user)
        {
            OperationResult<List<EmployeeBookingDC>> result = GMSGymEmployee.GetEmployeeBookings(employeeId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<EmployeeBookingDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }



        #endregion

        #region TimeEntry

        public List<TimeEntryDC> GetTimeEntryForAdminView(DateTime fromDate, DateTime endDate, string user)
        {
            OperationResult<List<TimeEntryDC>> result = GMSTimeEntry.GetTimeEntriesForAdminView(fromDate, endDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<TimeEntryDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Resource
        public int SaveResources(ResourceDC resource, string user)
        {
            OperationResult<int> result = GMSResources.SaveResources(resource, user, GetConfigurations(user), ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateResource(ResourceDC resource, string user)
        {
            OperationResult<bool> result = GMSResources.UpdateResource(resource, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteResource(int branchId, string user)
        {
            OperationResult<bool> result = GMSResources.DeleteResource(branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeActivateResource(int resourceId, int branchId, string user)
        {
            OperationResult<bool> result = GMSResources.DeActivateResource(resourceId, branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SwitchResource(int resourceTd1, int resourceId2, DateTime startDate, DateTime endDate, string comment, int branchId, string user)
        {
            OperationResult<bool> result = GMSResources.SwitchResource(resourceTd1, resourceId2, startDate, endDate, comment, branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SwitchResourceUndo(int resId, DateTime startDate, DateTime endDate, string user, int branchId)
        {
            OperationResult<bool> result = GMSResources.SwitchResourceUndo(resId, startDate, endDate, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool ValidateScheduleWithResourceId(int resourceId, DateTime startDate, DateTime endDate, int branchId, string user)
        {
            OperationResult<bool> result = GMSResources.ValidateScheduleWithResourceId(resourceId, startDate, endDate, branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool ValidateScheduleWithSwitchResource(int resourceId1, int resourceId2, DateTime startDate, DateTime endDate, int branchId, string user)
        {
            OperationResult<bool> result = GMSResources.ValidateScheduleWithSwitchResource(resourceId1, resourceId2, startDate, endDate, branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ResourceDC> GetResources(int branchId, string searchText, int categoryId, int activityId, int equipmentid, bool isActive, string user)
        {
            OperationResult<List<ResourceDC>> result = GMSResources.GetResources(branchId, searchText, categoryId, activityId, equipmentid, isActive, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ResourceDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public ResourceDC GetResourceDetailsById(int resourceId, string user)
        {
            OperationResult<ResourceDC> result = GMSResources.GetResourceDetailsById(resourceId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new ResourceDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ResourceDC> GetResourceCalender(int branchId, string user, int hit)
        {
            OperationResult<List<ResourceDC>> result = GMSResources.GetResourceCalender(branchId, ExceConnectionManager.GetGymCode(user), hit);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ResourceDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveScheduleItem(ScheduleItemDC scheduleItem, int resId, int branchId, string user)
        {
            OperationResult<int> result = GMSResources.SaveScheduleItem(scheduleItem, resId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<AvailableResourcesDC> GetAvailableResources(int branchId, int resId, string user)
        {
            OperationResult<List<AvailableResourcesDC>> result = GMSResources.GetAvailableResources(branchId, resId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<AvailableResourcesDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<AvailableResourcesDC> GetResourceAvailableTimeById(int branchId, int resId, string user)
        {
            OperationResult<List<AvailableResourcesDC>> result = GMSResources.GetResourceAvailableTimeById(branchId, resId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<AvailableResourcesDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public ScheduleDC GetResourceScheduleItems(int resId, string roleTpye, string user)
        {
            OperationResult<ScheduleDC> result = GMSResources.GetResourceScheduleItems(resId, roleTpye, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new ScheduleDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ScheduleDC> GetResourceCalenderScheduleItems(List<int> resIdList, string roleTpye, string user)
        {
            OperationResult<List<ScheduleDC>> result = GMSResources.GetResourceCalenderScheduleItems(resIdList, roleTpye, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ScheduleDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<string> ValidateResourcesWithSchedule(string roleType, List<AvailableResourcesDC> resourceList, int branchId, string user)
        {
            OperationResult<List<string>> result = GMSResources.ValidateResourcesWithSchedule(roleType, resourceList, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<string>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool AddOrUpdateAvailableResources(List<AvailableResourcesDC> resourceList, int branchId, string user)
        {
            OperationResult<bool> result = GMSResources.AddOrUpdateAvailableResources(resourceList, branchId, user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        public List<EntityActiveTimeDC> GetMemberBookingDetails(int branchId, int scheduleItemId, string user)
        {
            OperationResult<List<EntityActiveTimeDC>> result = GMSResources.GetMemberBookingDetails(branchId, scheduleItemId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<EntityActiveTimeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool ValidateScheduleWithPaidBooking(int scheduleItemId, int resourceId, string user)
        {
            OperationResult<bool> result = GMSResources.ValidateScheduleWithPaidBooking(scheduleItemId, resourceId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int DeleteResourcesScheduleItem(int scheduleItemId, string user)
        {
            OperationResult<int> result = GMSResources.DeleteResourcesScheduleItem(scheduleItemId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public int CheckDuplicateResource(int sourceId, int destinationId, DateTime startDate, DateTime endDate, string user)
        {
            var result = GMSResources.CheckDuplicateResource(sourceId, destinationId, startDate, endDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public int SaveResourceActiveTime(EntityActiveTimeDC activeTimeItem, string user, int branchId)
        {
            var result = GMSResources.SaveResourceActiveTime(activeTimeItem, ExceConnectionManager.GetGymCode(user), branchId, user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public int DeleteResourceActiveTime(int activeTimeId, string articleName, DateTime visitDateTime, string roleType, bool isArrived, bool isPaid, List<int> memberList, string user)
        {
            var result = GMSResources.DeleteResourceActiveTime(activeTimeId, articleName, visitDateTime, roleType, isArrived, isPaid, memberList, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ArticleDC> GetArticleForResouceBooking(int activityId, int branchId, string user)
        {
            var result = GMSResources.GetArticleForResouceBooking(activityId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ArticleDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ArticleDC> GetArticleForResouce(int activityId, int branchId, string user)
        {
            var result = GMSResources.GetArticleForResouce(activityId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ArticleDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int AdminApproveEmployeeWork(GymEmployeeWorkDC work, bool isApproved, string user)
        {
            var result = GMSGymEmployee.AdminApproveEmployeeWork(work, isApproved, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveGymEmployeeWorkScheduleItem(ScheduleItemDC scheduleItem, int resourceId, int branchId, string user)
        {
            var result = GMSGymEmployee.SaveGymEmployeeWorkScheduleItem(scheduleItem, resourceId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveWorkActiveTime(GymEmployeeWorkPlanDC work, int branchId, string user)
        {
            var result = GMSGymEmployee.SaveWorkActiveTime(work, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveWorkItem(GymEmployeeWorkDC work, int branchId, string user)
        {
            var result = GMSGymEmployee.SaveWorkItem(work, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public Dictionary<List<int>, List<int>> CheckUnusedCancelBooking(int activityId, int branchId, string accountNo, List<int> memberIdList, DateTime bookingDate, string user)
        {
            var result = GMSResources.CheckUnusedCancelBooking(activityId, branchId, accountNo, memberIdList, bookingDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new Dictionary<List<int>, List<int>>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int UnavailableResourceBooking(int scheduleItemId, int activeTimeId, DateTime startDate, DateTime endDate, string user)
        {
            var result = GMSResources.UnavailableResourceBooking(scheduleItemId, activeTimeId, startDate, endDate, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public Dictionary<List<int>, List<int>> ValidatePunchcardContractMember(int activityId, int branchId, string accountNo, List<int> memberIdList, DateTime startDate, string user)
        {
            var result = GMSResources.ValidatePunchcardContractMember(activityId, branchId, accountNo, memberIdList, startDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new Dictionary<List<int>, List<int>>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SavePaymentBookingMember(int activetimeId, int articleId, int memberId, bool paid, int aritemNo, decimal amount, string paymentType, string user)
        {
            var result = GMSResources.SavePaymentBookingMember(activetimeId, articleId, memberId, paid, aritemNo, amount, paymentType, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<int> GetEmpolyeeForResources(List<int> resIdList, string user)
        {
            var result = GMSResources.GetEmpolyeeForResources(resIdList, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<int>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<int> ValidateResWithSwitchDataRange(List<int> resourceId, DateTime startDate, DateTime endDate, string user)
        {
            var result = GMSResources.ValidateResWithSwitchDataRange(resourceId, startDate, endDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<int>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public bool ValidateArticleByActivityId(int activityId, int articleId, string user)
        {
            var result = GMSResources.ValidateArticleByActivityId(activityId, articleId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Contract

        public int SaveContract(PackageDC package, string user)
        {
            OperationResult<int> result = GMSContract.SaveContract(package, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                var _memberContractNotification = new NotificationDC();
                _memberContractNotification.Source = "dbo.ExceContractTemplate";
                _memberContractNotification.SourceID = result.OperationReturnValue;
                _memberContractNotification.ReceiverEntityID = package.PackageId;
                _memberContractNotification.Role = MemberRole.MEM;
                var notificationMethodList = new List<NotificationMethodDC>();
                var notificationMethod = new NotificationMethodDC();
                notificationMethod.DueDate = DateTime.Now.AddDays(30);
                notificationMethod.Method = NotifyMethodType.SMS;
                notificationMethodList.Add(notificationMethod);
                _memberContractNotification.MethodList = notificationMethodList;
            }
            return result.OperationReturnValue;
        }

        public int UpdateContractPriority(Dictionary<int, int> packages, string user)
        {
            OperationResult<int> result = GMSContract.UpdateContractPriority(packages, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ArticleDC> GetArticlesForCategory(int categoryID, string user, int branchID)
        {
            OperationResult<List<ArticleDC>> result = US.Exceline.GMS.API.GMSContract.GetArticlesForCommonUse(categoryID, ExceConnectionManager.GetGymCode(user), branchID);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ArticleDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<PackageDC> GetContracts(int branchId, string searchText, int searchType, string searchCriteria, string user)
        {
            OperationResult<List<PackageDC>> result = GMSContract.GetContracts(branchId, searchText, searchType, searchCriteria, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<PackageDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string GetNextContractId(string user, int branchID)
        {
            OperationResult<string> result = GMSContract.GetNextContractId(branchID, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return string.Empty;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int DeleteContract(string user, int contractId)
        {
            OperationResult<int> result = GMSContract.DeleteContract(contractId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        // Added to RestAPI @SKE    
        public List<ActivityDC> GetActivities(int branchId, string user)
        {
            OperationResult<List<ActivityDC>> result = US.Exceline.GMS.API.GMSContract.GetActivities(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ActivityDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<PriceItemTypeDC> GetPriceItemTypes(int branchId, string user)
        {
            OperationResult<List<PriceItemTypeDC>> result = GMSContract.GetPriceItemTypes(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<PriceItemTypeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Category

        public List<CategoryDC> GetCategories(string type, string user)
        {
            OperationResult<List<CategoryDC>> result = GMSCategory.GetCategoriesByType(type, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryDC>(); ;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CategoryTypeDC> GetCategoryTypes(string name, string user, int branchId)
        {
            OperationResult<List<CategoryTypeDC>> result = GMSCategory.GetCategoryTypes(name, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryTypeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveRegion(RegionDC region, string user, int branchId)
        {
            OperationResult<int> result = GMSCategory.SaveRegion(region, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveCategory(CategoryDC category, string user, int branchId)
        {
            OperationResult<int> result = GMSCategory.SaveCategory(category, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateCategory(CategoryDC category, string user, int branchId)
        {
            OperationResult<bool> result = GMSCategory.UpdateCategory(category, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int DeleteCategory(int categoryId, string user, int branchId)
        {
            OperationResult<int> result = GMSCategory.DeleteCategory(categoryId, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return 0;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CategoryDC> SearchCategories(string searchText, string searchType, string user, int branchId)
        {
            OperationResult<List<CategoryDC>> result = GMSCategory.SearchCategories(searchText, searchType, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryDC>(); ;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        // Added to RestAPI @SKE
        public List<ActivityDC> GetActivitiesForBranch(int branchId, string user)
        {
            OperationResult<List<ActivityDC>> result = US.Exceline.GMS.API.GMSContract.GetActivities(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ActivityDC>(); ;
            }

            else
            {
                return result.OperationReturnValue;
            }
        }

        // Added to RestAPI @SKE
        public int AddActivitySettings(int branchId, string user, ActivitySettingDC activitySetting)
        {
            OperationResult<int> result = GMSSystemSettings.AddActivitySettings(activitySetting, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }

            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Cotract Item

        public bool RemoveContractItem(int contractItemId, string user, int branchId)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result = GMSContract.RemoveContractItem(contractItemId, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool AddContractItem(ContractItemDC contractItem, string user, int branchId)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result = GMSContract.AddContractItem(contractItem, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ContractItemDC> GetContractItems(int branchId, string user)
        {
            OperationResult<List<ContractItemDC>> result = GMSContract.GetContractItems(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ContractItemDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Task Template

        public bool SaveTaskTemplate(TaskTemplateDC smsTemplate, bool isEdit, string user)
        {
            OperationResult<bool> result = GMSManageTask.SaveTaskTemplate(smsTemplate, isEdit, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateTaskTemplate(TaskTemplateDC smsTemplate, string user)
        {
            OperationResult<bool> result = GMSManageTask.UpdateTaskTemplate(smsTemplate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteTaskTemplate(int smsTemplateId, string user)
        {
            OperationResult<bool> result = GMSManageTask.DeleteTaskTemplate(smsTemplateId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeActivateTaskTemplate(int smsTemplateId, string user)
        {
            OperationResult<bool> result = GMSManageTask.DeActivateTaskTemplate(smsTemplateId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<TaskTemplateDC> GetTaskTemplates(TaskTemplateType templateType, int branchId, int templateId, string user)
        {
            OperationResult<List<TaskTemplateDC>> result = GMSManageTask.GetTaskTemplates(templateType, branchId, templateId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<TaskTemplateDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExtendedTaskTemplateDC> GetTaskTemplateExtFields(int extFieldId, string user)
        {
            OperationResult<List<ExtendedTaskTemplateDC>> result = GMSManageTask.GetTaskTemplateExtFields(extFieldId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExtendedTaskTemplateDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExtendedFieldDC> GetExtFieldsByCategory(int categoryId, string categoryType, string user)
        {
            OperationResult<List<ExtendedFieldDC>> result = GMSManageTask.GetExtFieldsByCategory(categoryId, categoryType, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExtendedFieldDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Schedule

        public bool UpdateActiveTime(EntityActiveTimeDC activeTime, string user, int branchId, bool isDelete)
        {
            OperationResult<bool> result = GMSSchedule.UpdateActiveTime(activeTime, isDelete, branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateSheduleItems(ScheduleDC schedule, string user)
        {
            OperationResult<bool> result = GMSSchedule.UpdateSheduleItems(schedule, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<EntityActiveTimeDC> GetEntityActiveTimes(int branchid, DateTime startDate, DateTime endDate, List<int> entityList, string entityRoleType, string user)
        {
            OperationResult<List<EntityActiveTimeDC>> result = GMSSchedule.GetEntityActiveTimes(branchid, startDate, endDate, entityList, entityRoleType, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<EntityActiveTimeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public ScheduleDC GetEntitySchedule(int entityId, string entityRoleType, string user)
        {
            OperationResult<ScheduleDC> result = GMSSchedule.GetEntitySchedule(entityId, entityRoleType, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new ScheduleDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteScheduleItem(int scheduleItemId, bool activeStatus, string user)
        {
            OperationResult<bool> result = GMSSchedule.DeleteScheduleItem(scheduleItemId, activeStatus, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SaveShedule(int id, string name, string roleId, int branchId, ScheduleDC sheduleDc, string user)
        {
            OperationResult<bool> result = GMSSchedule.SaveShedule(id, name, roleId, branchId, sheduleDc, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ScheduleItemDC> GetScheduleItemsByScheduleId(int scheduleId, string user)
        {
            OperationResult<List<ScheduleItemDC>> result = GMSSchedule.GetScheduleItemsByScheduleId(scheduleId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ScheduleItemDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Member

        public List<OrdinaryMemberDC> GetMembersForCommonBooking(int branchId, string searchText, string user)
        {
            OperationResult<List<OrdinaryMemberDC>> result = GMSMember.GetMembersForCommonBooking(branchId, searchText, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<OrdinaryMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        // Added to RestAPI @SKE
        public List<ActivityDC> GetActivitiesForCommonBooking(int entityId, string entityType, int memberId, string user)
        {
            OperationResult<List<ActivityDC>> result = GMSMember.GetActivitiesForCommonBooking(entityId, entityType, memberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ActivityDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<EntityVisitDC> GetMembersVisits(DateTime startDate, DateTime endDate, int branchId, string user)
        {
            OperationResult<List<EntityVisitDC>> result = GMSMembers.GetMemberVisits(startDate, endDate, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<EntityVisitDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public OrdinaryMemberDC GetMembersById(int branchId, int memberId, string user)
        {
            OperationResult<OrdinaryMemberDC> result = GMSMember.GetMembersById(branchId, memberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new OrdinaryMemberDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Booking

        public bool CheckCommonBookingAvailability(int activeTimeId, int entityId, int branchId, string entityType, DateTime startDateTime, DateTime endDateTime, string user)
        {
            OperationResult<bool> result = GMSCommonBooking.CheckCommonBookingAvailability(activeTimeId, entityId, branchId, entityType, startDateTime, endDateTime, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SaveCommonBookingDetails(CommonBookingDC commonBookingDC, ScheduleDC scheduleDC, int branchId, string user, string scheduleCategoryType)
        {
            OperationResult<bool> result = GMSCommonBooking.SaveCommonBookingDetails(commonBookingDC, scheduleDC, branchId, user, scheduleCategoryType, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Common

        public string UpdateEmployees(EmployeeDC employeeDc, string user)
        {
            OperationResult<string> result = GMSEmployee.UpdateEmployees(employeeDc, GetConfigurations(user), user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return string.Empty;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string UpdateMemeberActiveness(int trainerId, string rolId, int branchId, string user, bool isActive)
        {
            OperationResult<string> result = GMSEmployee.UpdateMemeberActiveness(trainerId, rolId, branchId, user, isActive, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return string.Empty;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineTaskDC> GetEntityTasks(int entityId, int branchId, string entityRoleType, string user)
        {
            OperationResult<List<ExcelineTaskDC>> result = GMSEmployee.GetEntityTasks(entityId, branchId, entityRoleType, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineTaskDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SaveEntityTimeEntry(TimeEntryDC timeEntry, string user)
        {
            OperationResult<bool> result = GMSEmployee.SaveEntittTimeEntry(timeEntry, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<TimeEntryDC> GetEntityTimeEntries(DateTime startDate, DateTime endDate, int entityId, string entityRoleType, string user)
        {
            OperationResult<List<TimeEntryDC>> result = GMSEmployee.GetEntityTimeEntries(startDate, endDate, entityId, entityRoleType, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<TimeEntryDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteEntityTimeEntry(int entityTimeEntryId, int taskId, decimal timeSpent, string user)
        {
            OperationResult<bool> result = GMSEmployee.DeleteEntityTimeEntry(entityTimeEntryId, taskId, timeSpent, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CategoryDC> GetTaskStatusCategories(string user, int branchId)
        {
            OperationResult<List<CategoryDC>> result = GMSEmployee.GetTaskStatusCategories(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SaveEmployeesRoles(EmployeeDC employeeDc, string user)
        {
            OperationResult<bool> result = GMSEmployee.SaveEmployeesRoles(employeeDc, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        // Added to RestAPI @SKE
        public List<RoleActivityDC> GetActivitiesForRoles(int memberId, string user)
        {
            OperationResult<List<RoleActivityDC>> result = GMSEmployee.GetActivitiesForRoles(memberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<RoleActivityDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteGymEmployee(int memberId, int assignEmpId, string user)
        {
            OperationResult<bool> result = GMSEmployee.DeleteGymEmployee(memberId, assignEmpId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Exceline Task

        public bool SaveExcelineTask(ExcelineTaskDC excelineTask, string user)
        {
            OperationResult<int> result = GMSManageTask.SaveExcelineTask(excelineTask, ExceConnectionManager.GetGymCode(user));
            //------------------------Add Notification---------------------------------------------------------
            if (excelineTask != null)
            {
                if (excelineTask.TemplateType == TaskTemplateType.LOSTANDFOUND && excelineTask.Id <= 0)
                {
                    try
                    {
                        NotificationDC _discountNotification = new NotificationDC();
                        _discountNotification.Source = "dbo.ExceTask";
                        _discountNotification.SourceID = result.OperationReturnValue;
                        List<NotificationMethodDC> notificationMethodList = new List<NotificationMethodDC>();
                        NotificationMethodDC notificationMethod = new NotificationMethodDC();
                        notificationMethod.Notification = "An item is lost";
                        notificationMethod.DueDate = DateTime.Now.AddDays(30);
                        notificationMethod.Method = NotifyMethodType.POPUP;
                        notificationMethodList.Add(notificationMethod);
                        _discountNotification.MethodList = notificationMethodList;
                    }
                    catch
                    {
                    }
                }
            }


            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                if (result.OperationReturnValue > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<ExcelineTaskDC> GetExcelineTasks(int branchId, bool isFxied, int templateId, int assignTo, string roleType, int followupMemberId, string user)
        {
            OperationResult<List<ExcelineTaskDC>> result = GMSManageTask.GetExcelineTasks(branchId, isFxied, templateId, assignTo, roleType, followupMemberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineTaskDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateExcelineTask(ExcelineTaskDC excelineTask, string user)
        {
            OperationResult<int> result = GMSManageTask.EditExcelineTask(excelineTask, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                if (result.OperationReturnValue > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<ExcelineTaskDC> SearchExcelineTask(string searchText, TaskTemplateType type, int followUpMemId, int branchId, bool isFxied, string user)
        {
            OperationResult<List<ExcelineTaskDC>> result = GMSManageTask.SearchExcelineTask(searchText, type, followUpMemId, branchId, isFxied, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineTaskDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SaveTaskWithOutSchedule(ExcelineTaskDC excelineTask, string user)
        {
            OperationResult<bool> result = GMSManageTask.SaveTaskWithOutSchedule(excelineTask, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Articale

        public int SaveArticle(ArticleDC articleDc, string user, int branchID, int activityCategoryId)
        {
            OperationResult<int> result = GMSArticle.SaveArticle(articleDc, user, branchID, activityCategoryId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool ImportArticleList(string articleList, string user, int branchID)
        {

            List<ArticleDC> articles = XMLUtilities.DesrializeXMLToObject(articleList, typeof(List<ArticleDC>)) as List<ArticleDC>;
            OperationResult<bool> result = GMSArticle.ImportArticleList(articles, user, branchID, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }




        public List<ArticleDC> GetArticles(int branchId, string user, ArticleTypes categpryType, string keyword, CategoryDC category, int activityId, bool? isObsalate, bool isActive, bool filterByGym)
        {
            OperationResult<List<ArticleDC>> result = GMSArticle.GetArticles(branchId, user, categpryType, keyword, category, activityId, isObsalate, isActive, ExceConnectionManager.GetGymCode(user), filterByGym);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ArticleDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Branch

        public int SaveBranchDetails(ExcelineBranchDC branch, string user)
        {
            OperationResult<int> result = GMSSystemSettings.SaveBranchDetails(branch, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool ValidateCreditorNo(int branchId, string user, string creditorNo)
        {

            OperationResult<bool> result = GMSSystemSettings.ValidateCreditorNo(branchId, ExceConnectionManager.GetGymCode(user), creditorNo);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineCreditorGroupDC> GetBranchGroups(string user)
        {
            OperationResult<List<ExcelineCreditorGroupDC>> result = GMSSystemSettings.GetBranchGroups(user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineCreditorGroupDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineBranchDC> GetBranches(string user, int branchId)
        {
            OperationResult<List<ExcelineBranchDC>> result = GMSSystemSettings.GetBranches(user, ExceConnectionManager.GetGymCode(user), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineBranchDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int UpdateBranchDetails(ExcelineBranchDC branch, string user)
        {
            File.WriteAllText(@"D:\WriteFile.txt", "methord is ok");

            OperationResult<int> result = GMSSystemSettings.UpdateBranchDetails(branch, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Manage Discount

        public List<OrdinaryMemberDC> GetMembersByRoleType(int branchId, string user, bool activeState, MemberRole roleType, string searchText)
        {
            int status = 1;
            OperationResult<List<OrdinaryMemberDC>> result = GMSManageMembership.GetMembersByRoleType(branchId, user, status, roleType, searchText, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<OrdinaryMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveDiscount(List<DiscountDC> discountList, string user, int branchId, int contractSettingID)
        {
            OperationResult<int> result = GMSManageMembership.SaveDiscount(discountList, user, branchId, contractSettingID, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return 0;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteDiscount(List<int> discountIDList, string user)
        {
            OperationResult<bool> result = GMSManageMembership.DeleteDiscount(discountIDList, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteShopSponsorDiscount(int discountId, string type, string user)
        {
            OperationResult<bool> result = GMSInventoryDiscounts.DeleteDiscount(discountId, type, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<DiscountDC> GetDiscountList(int branchId, DiscountType discountType, string user)
        {
            OperationResult<List<DiscountDC>> result = GMSManageMembership.GetDiscountList(branchId, discountType, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<DiscountDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<ExcelineMemberDC> GetVenderList(int branchId, string searchText, string user)
        {
            OperationResult<List<ExcelineMemberDC>> result = GMSManageMembership.GetMembers(branchId, searchText, 1, MemberSearchType.SEARCH, MemberRole.COM, user, ExceConnectionManager.GetGymCode(user), 1, false, false, string.Empty);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Invoice

        public int GenerateInvoice(List<ExcelineInvoiceDetailDC> invoiceDetails, bool ismemberRecords, int branchID, string user, bool isGenereteNow)
        {
            return 0;
        }

        public List<ExcelineInvoiceDetailDC> GetInvoiceDetails(DateTime invoiceDate, bool isMemberRecords, string user, int branchID)
        {

            return new List<ExcelineInvoiceDetailDC>();
        }

        public string SearchInvoices(string searchValue, int invoiceType, string filedType, object constValue, string user, int branchId)
        {
            return string.Empty;
        }

        public InvoiceBasicInfoDC GetInvoiceBasicInfo(int subCaseNo, string user)
        {

            return null;
        }

        public bool CancelInvoice(int arItemNo, string invoiceNo, string user)
        {
            return true;
        }

        #endregion

        #region Holidays

        public List<HolidayScheduleDC> GetHolidays(int branchId, string holidayType, DateTime startDate, DateTime endDate, string user)
        {
            OperationResult<List<HolidayScheduleDC>> result = GMSHolidays.GetHolidays(branchId, holidayType, startDate, endDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<HolidayScheduleDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool AddNewHoliday(HolidayScheduleDC newHoliday, string user)
        {
            OperationResult<bool> result = GMSHolidays.AddNewHoliday(newHoliday, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int GetHolidayCategoryId(string categoryName, string user)
        {
            OperationResult<int> result = GMSHolidays.GetHolidayCategoryId(categoryName, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateHolidayStatus(HolidayScheduleDC updateHoliday, string user)
        {
            OperationResult<bool> result = GMSHolidays.UpdateHolidayStatus(updateHoliday, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Utility

        public string GetCityForPostalCode(string postalCode, string user)
        {
            OperationResult<string> result = GMSUtility.GetCityByPostalCode(postalCode, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return string.Empty;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CalendarHoliday> GetGymHolidays(DateTime calendarDate, string user, int branchId)
        {
            OperationResult<List<CalendarHoliday>> result = GMSUtility.GetHolidays(calendarDate, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CalendarHoliday>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        public GymIntegrationExcSettingsDC grt()
        {
            GymIntegrationExcSettingsDC h = new GymIntegrationExcSettingsDC();
            return h;
        }

        public bool AttachFile(string fileName, string custId, string companyCode, string username, int branchId)
        {
            string fullFileName = ConfigurationManager.AppSettings["UploadScanDocRoot"];
            int maxRetryTime = Convert.ToInt32(ConfigurationManager.AppSettings["DocumentUploadRetryTime"]);
            fullFileName = fullFileName + @"\" + companyCode + @"\" + branchId + @"\" + custId + @"\" + fileName;
            int i = 0;
            bool isUploaded = false;
            while (i < (maxRetryTime / 5))
            {
                Thread.Sleep(5000);
                if (File.Exists(fullFileName))
                {
                    isUploaded = true;
                    break;
                }
                i++;
            }
            return isUploaded;
        }

        public List<RegionDC> GetRegions(int branchId, string user, string countryId)
        {
            OperationResult<List<RegionDC>> result = GMSCategory.GetRegions(branchId, ExceConnectionManager.GetGymCode(user), countryId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<RegionDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SaveDocumentData(DocumentDC document, string username, int branchId)
        {
            document.FilePath = ConfigurationManager.AppSettings["UploadScanDocRoot"] + document.FilePath;
            OperationResult<bool> result = GMSManageMembership.SaveDocumentData(document, ExceConnectionManager.GetGymCode(username), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), username);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public SalePointDC GetSalesPointByMachineName(int branchID, string machineName, string user)
        {
            OperationResult<SalePointDC> result = GMSShopManager.GetSalesPointByMachineName(branchID, machineName, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new SalePointDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #region Gym Gym Settings
        // Added  to Rest  API  
        //public string GetGymSettings(int branchId, string user, GymSettingType gymSettingType, string systemName)
        //{
        //    OperationResult<string> result = GMSManageGymSetting.GetGymSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
        //    if (result.ErrorOccured)
        //    {
        //        foreach (NotificationMessage message in result.Notifications)
        //        {
        //            USLogError.WriteToFile(message.Message, new Exception(), user);
        //        }
        //        return null;
        //    }
        //    else
        //    {
        //        return result.OperationReturnValue;
        //    }
        //}

        //Added  to Rest  API
        //public string GetTerminalTypes(string user)
        //{
        //    OperationResult<string> result = GMSManageGymSetting.GetTerminalTypes(ExceConnectionManager.GetGymCode(user));
        //    if (result.ErrorOccured)
        //    {
        //        foreach (NotificationMessage message in result.Notifications)
        //        {
        //            USLogError.WriteToFile(message.Message, new Exception(), user);
        //        }
        //        return null;
        //    }
        //    else
        //    {
        //        return result.OperationReturnValue;
        //    }
        //}

        //Added to Rest API
        public bool DeleteIntegrationSettingById(int id, string user)
        {
            OperationResult<bool> result = GMSManageGymSetting.DeleteIntegrationSettingById(id, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        // Added  to  Rest API
        public bool SaveGymSettings(int branchId, string user, string accessTimeDetails, GymSettingType gymSettingType)
        {
            OperationResult<bool> result = GMSManageGymSetting.SaveGymSettings(branchId, accessTimeDetails, ExceConnectionManager.GetGymCode(user), gymSettingType);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        //Added to  Rest API
        public Dictionary<string, object> GetSelectedGymSettings(List<string> gymSetColNames, bool isGymSettings, int branchId, string user)
        {
            OperationResult<Dictionary<string, object>> result = GMSManageGymSetting.GetSelectedGymSettings(gymSetColNames, isGymSettings, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        #region Gym Gym Company Settings
        // Added  to  Rest  API
        //public string GetGymCompanySettings(string user, GymCompanySettingType gymCompanySettingType)
        //{
        //    OperationResult<string> result = GMSSystemSettings.GetGymCompanySettings(ExceConnectionManager.GetGymCode(user), gymCompanySettingType);
        //    if (result.ErrorOccured)
        //    {
        //        foreach (NotificationMessage message in result.Notifications)
        //        {
        //            USLogError.WriteToFile(message.Message, new Exception(), user);
        //        }
        //        return null;
        //    }
        //    else
        //    {
        //        return result.OperationReturnValue;
        //    }
        //}

        //Added to Rest  API 
        public int AddUpdateGymCompanySettings(string settingsDetails, string user, GymCompanySettingType gymCompanySettingType)
        {
            OperationResult<int> result = GMSSystemSettings.AddUpdateGymCompanySettings(settingsDetails, user, ExceConnectionManager.GetGymCode(user), gymCompanySettingType);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        // Added  to Rest  API
        public List<ExceAccessProfileDC> GetAccessProfiles(string user)
        {
            OperationResult<List<ExceAccessProfileDC>> result = GMSSystem.GetAccessProfiles(ExceConnectionManager.GetGymCode(user), Gender.ALL);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        //Added  to  Rest  API
        public int UpdateAccessProfiles(List<ExceAccessProfileDC> AccessProfileList, string user)
        {
            OperationResult<int> result = GMSSystem.UpdateAccessProfiles(AccessProfileList, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        //should go  to  api/Admin/Class
        public List<String> GetClassKeywords(string user)
        {
            OperationResult<List<string>> result = GMSSystemSettings.GetClassKeyword(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<string>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool AddEditClassKeyWords(List<CategoryDC> keyWords, string user)
        {
            return true;
        }

        #endregion

        #region DummyMethodForPassDomainObject


        public void DummyMethodForPassDomainObject(ExceGymSettingDC gymSetting, ExceGymCompanySettingsDC gymCompanySetting)
        {
        }

        public void DummyMethodForGym(GymAccessSettingDC gymSetting)
        {
        }

        #endregion

        #region Notification

        public int SaveNotificationTemplate(NotificationTemplateDC notificationTemplate, string user, int branchId)
        {
            OperationResult<int> result = new OperationResult<int>();//GMSNotification.SaveNotificationTemplate(notificationTemplate, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<NotificationTemplateDC> GetNotificationTemplates(int branchId, string user)
        {
            OperationResult<List<NotificationTemplateDC>> result = new OperationResult<List<NotificationTemplateDC>>();
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<NotificationTemplateDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region USC

        public void TestUSC()
        {
            US.Communication.API.TemplateManager.GetAllTempaltes("tets");
        }
        #endregion

        public List<ShopSalesItemDC> GetDailySales(string user, DateTime salesDate, int branchId)
        {
            OperationResult<List<ShopSalesItemDC>> result = GMSInventoryDiscounts.GetDailySales(salesDate, branchId, ExceConnectionManager.GetGymCode(user));
            try
            {
                if (result.ErrorOccured)
                {
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                }
            }
            catch
            {
                return new List<ShopSalesItemDC>();
            }
            return result.OperationReturnValue;
        }

        public List<ExcelineMemberDC> GetMembers(int branchId, string user, string searchText, int activeState, MemberSearchType searchType, MemberRole memberRole, int hit, string sortName, bool isAscending)
        {
            //if (searchType == MemberSearchType.ALLMEMANDCOM)
            //{
            //    activeState = 10; //all mem and com
            //    OperationResult<string> result = GMSMember.GetAllMemberAndCompany(branchId, searchText, activeState, hit, ExceConnectionManager.GetGymCode(user));
            //    if (result.ErrorOccured)
            //    {
            //        foreach (NotificationMessage message in result.Notifications)
            //        {
            //            USLogError.WriteToFile(message.Message, new Exception(), user);
            //        }
            //        return XMLUtils.SerializeDataContractObjectToXML(new List<ExcelineMemberDC>());
            //    }
            //    else
            //    {
            //        return result.OperationReturnValue;
            //    }
            //}
            //else
            //{
            OperationResult<List<ExcelineMemberDC>> result = GMSManageMembership.GetMembers(branchId, searchText, activeState, searchType, memberRole, user, ExceConnectionManager.GetGymCode(user), hit, !string.IsNullOrEmpty(sortName), isAscending, sortName);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
            //}

        }

        public OrdinaryMemberDC GetMemberDetailsByMemberId(int branchId, string user, int memberId, string memberRole)
        {
            OperationResult<OrdinaryMemberDC> result = GMSManageMembership.GetMemberDetailsByMemberId(branchId, user, memberId, memberRole, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #region Revenue Accounts
        public List<AccountDC> GetRevenueAccounts(string user)
        {
            OperationResult<List<AccountDC>> result = GMSSystemSettings.GetRevenueAccounts(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int AddUpdateRevenueAccounts(AccountDC revenueAccountDetail, string user)
        {
            OperationResult<int> result = GMSSystemSettings.AddUpdateRevenueAccounts(revenueAccountDetail, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int DeleteRevenueAccounts(AccountDC revenueAccountDetail, string user)
        {
            OperationResult<int> result = GMSSystemSettings.DeleteRevenueAccounts(revenueAccountDetail, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        public string SaveMember(OrdinaryMemberDC member, string user)
        {
            OperationResult<string> result = GMSManageMembership.SaveMember(member, String.Empty, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return "-1";
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SavePostalArea(string user, string postalCode, string postalArea, long population, long houseHold)
        {
            var result = GMSMember.SavePostalArea(user, postalCode, postalArea, population, houseHold, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CountryDC> GetCountryDetails(string user)
        {
            var result = GMSMember.GetCountryDetails(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CountryDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string GetMem()
        {
            ExcePaymentInfoDC k = new ExcePaymentInfoDC();
            return k.PaymentType;
        }

        #region ClassTypeSettings

        public List<ExceClassTypeDC> GetClassTypes(string user)
        {
            var result = ManageClassTypeSetting.GetClassTypes(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExceClassTypeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool AddEditClassType(ExceClassTypeDC classType, string user)
        {
            var result = ManageClassTypeSetting.AddEditClassType(classType, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int DeleteClassType(int classtypeId, string user)
        {
            var result = ManageClassTypeSetting.DeleteClassType(classtypeId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        public decimal SaveStock(ArticleDC article, int branchId, string user)
        {
            var result = GMSSystemSettings.SaveStock(article, branchId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            return result.OperationReturnValue;
        }

        public int DeleteArticle(int articleId, int branchId, string user, bool isAdminUser)
        {
            var result = GMSSystemSettings.DeleteArticle(articleId, branchId, ExceConnectionManager.GetGymCode(user), user, isAdminUser);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        // Added to RestAPI @SKE
        public List<ActivityTimeDC> GetActivityTimes(int branchId, string user, bool isUnavailableTimes)
        {
            OperationResult<List<ActivityTimeDC>> result = GMSSystemSettings.GetActivityTimes(branchId, isUnavailableTimes, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ActivityTimeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        // @SKE
        public bool AddActivityTimes(List<ActivityTimeDC> activityTimes, string user, bool isUnavailableTimes)
        {
            OperationResult<bool> result = GMSSystemSettings.AddActivityTimes(activityTimes, ExceConnectionManager.GetGymCode(user), isUnavailableTimes);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveInventory(InventoryItemDC inventoryItem, string user)
        {
            var result = GMSSystemSettings.SaveInventory(inventoryItem, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<InventoryItemDC> GetInventory(int branchId, string user)
        {
            var result = GMSSystemSettings.GetInventory(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InventoryItemDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int DeleteInventory(int inventoryId, string user)
        {
            var result = GMSSystemSettings.DeleteInventory(inventoryId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ArticleDC> GetInventoryDetail(int branchId, int inventoryId, string user)
        {
            var result = GMSSystemSettings.GetInventoryDetail(branchId, inventoryId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ArticleDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public decimal SaveInventoryDetail(List<ArticleDC> articleList, int inventoryId, string user)
        {
            var result = GMSSystemSettings.SaveInventoryDetail(articleList, inventoryId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public Dictionary<decimal, decimal> GetNetValueForArticle(int inventoryId, int articleId, int counted, int branchId, string user)
        {
            var result = GMSSystemSettings.GetNetValueForArticle(inventoryId, articleId, counted, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new Dictionary<decimal, decimal>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #region Task Category

        // Added to Rest API
        public bool SaveTaskCategory(ExcelineTaskCategoryDC taskCategory, bool isEdit, string user)
        {
            OperationResult<bool> result = GMSManageTask.SaveExcelineTaskCategory(taskCategory, isEdit, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public bool DeleteTaskCategory(int taskCategoryId, string user)
        {
            OperationResult<bool> result = GMSManageTask.DeleteTaskCategory(taskCategoryId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineTaskCategoryDC> GetTaskCategories(int branchId, string user)
        {
            OperationResult<List<ExcelineTaskCategoryDC>> result = GMSManageTask.GetTaskCategories(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineTaskCategoryDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        #region followUp


        public bool AddFollowUpTemplate(FollowUpTemplateDC template, string user, int branchId)
        {
            OperationResult<bool> result = GMSSystemSettings.AddFollowUpTemplate(template, ExceConnectionManager.GetGymCode(user), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<FollowUpTemplateDC> GetFollowUpTemplates(string user, int branchId)
        {
            OperationResult<List<FollowUpTemplateDC>> result = GMSSystemSettings.GetFollowupTemplates(ExceConnectionManager.GetGymCode(user), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<FollowUpTemplateDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteFollowUpTemplate(int followUpTemplateId, string user)
        {
            OperationResult<bool> result = GMSSystemSettings.DeleteFollowUpTemplate(followUpTemplateId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<FollowUpTemplateTaskDC> GetFollowUpTaskByTemplateId(int followUpTemplateId, string user)
        {
            OperationResult<List<FollowUpTemplateTaskDC>> result = GMSSystemSettings.GetFollowUpTaskByTemplateId(followUpTemplateId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<FollowUpTemplateTaskDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }

        }
        public List<FollowUpDetailDC> GetFollowUpDetailByEmpId(int empId, string user)
        {
            OperationResult<List<FollowUpDetailDC>> result = GMSManageMembership.GetFollowUpDetailByEmpId(empId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<FollowUpDetailDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Job Category

        public bool SaveJobCategory(ExcelineJobCategoryDC jobCategory, bool isEdit, string user)
        {
            OperationResult<bool> result = GMSManageTask.SaveExcelineJobCategory(jobCategory, isEdit, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public bool DeleteJobCategory(int jobCategoryId, string user)
        {
            OperationResult<bool> result = GMSManageTask.DeleteJobCategory(jobCategoryId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineJobCategoryDC> GetJobCategories(int branchId, string user)
        {
            OperationResult<List<ExcelineJobCategoryDC>> result = GMSManageTask.GetJobCategories(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineJobCategoryDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Job

        public int SaveExcelineJob(ScheduleItemDC scheduleItem, int branchId, string user)
        {
            OperationResult<int> result = GMSJobs.SaveExcelineJob(scheduleItem, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<ScheduleItemDC> GetJobScheduleItems(int branchId, string user)
        {
            OperationResult<List<ScheduleItemDC>> result = GMSJobs.GetJobScheduleItems(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ScheduleItemDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int DeleteJobScheduleItem(List<int> scheduleItemIdList, string user)
        {
            OperationResult<int> result = GMSJobs.DeleteScheduleItem(scheduleItemIdList, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineCommonTaskDC> GetAllTasksByEmployeeId(int branchId, int employeeId, string user)
        {
            OperationResult<List<ExcelineCommonTaskDC>> result = GMSJobs.GetAllTasksByEmployeeId(branchId, employeeId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineCommonTaskDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineCommonTaskDC> GetFollowUpByEmployeeId(int branchId, int employeeId, string user, int hit)
        {
            OperationResult<List<ExcelineCommonTaskDC>> result = GMSJobs.GetFollowUpByEmployeeId(branchId, employeeId, ExceConnectionManager.GetGymCode(user), user, hit);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineCommonTaskDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineCommonTaskDC> GetJobsByEmployeeId(int branchId, int employeeId, string user, int hit)
        {
            OperationResult<List<ExcelineCommonTaskDC>> result = GMSJobs.GetJobsByEmployeeId(branchId, employeeId, ExceConnectionManager.GetGymCode(user), user, hit);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineCommonTaskDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int AssignTaskToEmployee(ExcelineCommonTaskDC commonTask, int employeeId, string user)
        {
            OperationResult<int> result = GMSJobs.AssignTaskToEmployee(commonTask, employeeId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        public List<USPRole> GetEmpRoles(string user)
        {
            var result = AdminUserSettings.GetUSPRoles("", true, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }

                return new List<USPRole>();
            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public List<ExcelineRoleDc> GetExcelineEmpRoles(string user)
        {
            var result = AdminUserSettings.GetUSPRoles("", true, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }

                return new List<ExcelineRoleDc>();
            }
            else
            {
                return result.MethodReturnValue.Select(uspRole => new ExcelineRoleDc
                {
                    Id = uspRole.Id,
                    RoleName = uspRole.RoleName
                }).ToList();
            }
        }

        public ExtendedFieldDC Test()
        {
            return new ExtendedFieldDC();
        }

        #endregion

        public ArticleDC GetInvoiceFeeArticle(int branchId, string user)
        {
            var result = GMSArticle.GetInvoiceFee(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<SalePointDC> GetSalesPointList(int branchID, string user)
        {
            OperationResult<List<SalePointDC>> result = GMSShopManager.GetSalesPointList(branchID, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<SalePointDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SavePointOfSale(int branchId, SalePointDC pointOfSale, string user)
        {
            OperationResult<int> result = GMSShopManager.SavePointOfSale(branchId, pointOfSale, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int GetSelectedHardwareProfile(int branchId, string user)
        {
            OperationResult<int> result = GMSShopManager.GetSelectedHardwareProfile(branchId, user, ExceConnectionManager.GetGymCode(user));

            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public MemberShopAccountDC GetMemberShopAccounts(int memberId, int hit, bool itemsNeeded, string user)
        {
            var result = GMSMember.GetMemberShopAccounts(memberId, hit, itemsNeeded, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new MemberShopAccountDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public SaleResultDC AddShopSales(InstallmentDC installment, ShopSalesDC shopSaleDetails, PaymentDetailDC paymentDetails, int memberBranchId, int branchID, string user)
        {
            //this brancId can be changed when resource booking is avalible for all gyms
            OperationResult<SaleResultDC> result = GMSShop.AddShopSales(installment, shopSaleDetails, paymentDetails, memberBranchId, branchID, user, ExceConnectionManager.GetGymCode(user), true, "");
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new SaleResultDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public OccuranceType getData()
        {
            return OccuranceType.ONCE;
        }

        public bool SendSmsForBooking(Dictionary<int, string> memberIdList, int branchId, string user, string message, string mobileNo)
        {
            // create common notification
            USCommonNotificationDC commonNotification = new USCommonNotificationDC();
            commonNotification.IsTemplateNotification = false;
            commonNotification.Description = message;
            commonNotification.BranchID = branchId;
            commonNotification.CreatedUser = user;
            commonNotification.Role = "MEM";
            commonNotification.NotificationType = NotificationTypeEnum.Messages;
            commonNotification.Severity = NotificationSeverityEnum.Minor;
            commonNotification.MemberIdList = memberIdList;
            commonNotification.Status = NotificationStatusEnum.New;
            commonNotification.Method = NotificationMethodType.SMS;
            commonNotification.Title = "BOOKING TEXT SMS";
            commonNotification.SenderDescription = mobileNo;

            USImportResult<int> result = NotificationAPI.AddNotification(commonNotification);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage mgs in result.NotificationMessages)
                {
                    if (mgs.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(mgs.Message, new Exception(), user);
                    }
                }
                return false;
            }
            else
            {
                return true;
            }


        }

        public int AddMemberNotification(USNotificationTree notification, string text)
        {
            USImportResult<int> result = NotificationAPI.AddNotification(notification);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "TestUser");
                    }
                }
                return -1;

            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public int AddFreeDefineNotification(Dictionary<int, string> memberIdList, string memName, DateTime startTime, DateTime endTime, string resName, DateTime bookingDate, int branchId, string user)
        {

            try
            {

                Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                paraList.Add(TemplateFieldEnum.MEMBERNAME, memName);
                paraList.Add(TemplateFieldEnum.RESOURCENAME, resName);
                paraList.Add(TemplateFieldEnum.FROMTIME, startTime.ToShortTimeString());
                paraList.Add(TemplateFieldEnum.TOTIME, endTime.ToShortTimeString());
                paraList.Add(TemplateFieldEnum.DATE, bookingDate.ToShortDateString());

                USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                commonNotification.IsTemplateNotification = true;
                commonNotification.ParameterString = paraList;
                commonNotification.BranchID = branchId;
                commonNotification.CreatedUser = user;
                commonNotification.Role = "MEM";
                commonNotification.NotificationType = NotificationTypeEnum.Messages;
                commonNotification.Severity = NotificationSeverityEnum.Minor;
                commonNotification.MemberIdList = memberIdList;
                commonNotification.Status = NotificationStatusEnum.New;
                // if wants to send sms
                commonNotification.Type = TextTemplateEnum.BOOKINGREMINDER;
                commonNotification.Method = NotificationMethodType.SMS;
                commonNotification.Title = "BOOKING SMS";
                //add SMS notification into notification tables
                USImportResult<int> result = NotificationAPI.AddNotification(commonNotification);

                if (result.ErrorOccured)
                {
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        if (message.ErrorLevel == ErrorLevels.Error)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                        }
                    }
                    return -1;
                }
                else
                {
                    return 1;
                }

            }
            catch (Exception e)
            {
                USLogError.WriteToFile(e.Message, new Exception(), user);
                return -1;
            }
        }

        public int BookingPaymentCancel(int bookingId, CreditNoteDC creditNote, int branchId, bool isReturn, string gymCode)
        {
            return 1;
        }

        public int AddCreditNoteForBooking(CreditNoteDC creditNote, List<PayModeDC> payModeDcs, int branchId, bool isReturn, string user)
        {
            var result = GMSInvoice.AddCreditNoteForBooking(creditNote, payModeDcs, branchId, false, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public USPUser ValidateUserWithCard(string cardNumber, string user)
        {
            OperationResultValue<USPUser> result = AdminUserSettings.ValidateUserWithCard(cardNumber, user);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), cardNumber);
                }
                return new USPUser();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public DailySettlementDC GetDailySettlements(int branchId, int salePointId, string user)
        {
            OperationResult<DailySettlementDC> result = GMSShop.GetDailySettlements(salePointId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public PDFPrintResultDC ViewContractDetail(int CCId, int branchId, string user)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_MemberContract_Print_App_Path"].ToString();
                var dataDictionary = new Dictionary<string, string>() { { "itemID", CCId.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
            }
            catch (Exception)
            {
            }
            return result;
        }

        private string GetFilePathForPdf(string application, string OutputPlugging, string user, Dictionary<string, string> dataDictionary)
        {
            var filePath = string.Empty;
            try
            {
                var list = ExcecuteWithUSC(application, OutputPlugging, dataDictionary, ExceConnectionManager.GetGymCode(user), user);
                foreach (var item in list)
                {
                    if (item.Messages != null)
                    {
                        var Message = item.Messages.Where(mssge => mssge.Header.ToLower().Equals("filepath")).First();
                        if (Message != null)
                        {
                            filePath = Message.Message;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
            }
            return filePath;
        }

        private List<IProcessingStatus> ExcecuteWithUSC(string application, string OutputPlugging, Dictionary<string, string> dataDictionary, string gymCode, string user)
        {
            List<IProcessingStatus> lists = null;
            try
            {
                lists = TemplateManager.ExecuteTemplate(dataDictionary, application, OutputPlugging, gymCode);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
                throw ex;
            }
            return lists;
        }

        public string GetConfigSettings(string type)
        {
            switch (type)
            {
                case "RPT":
                    return ConfigurationManager.AppSettings["ReportViewerURL"].ToString();
            }
            return string.Empty;
        }

        //public bool ValidateUser(string userName, string password)
        //{
        //    OperationResultValue<bool> result = AdminUserSettings.ValidateUser(userName, password);
        //    if (result.ErrorOccured)
        //    {
        //        foreach (USNotificationMessage message in result.Notifications)
        //        {
        //            USLogError.WriteToFile(message.Message, new Exception(), userName);
        //        }
        //        return false;
        //    }
        //    else
        //    {
        //        return result.OperationReturnValue;
        //    }
        //}

        public string GetSponsorsforSponsoring(int branchId, DateTime startDate, DateTime endDate, string user)
        {
            OperationResult<string> result = GMSMembers.GetSponsorsforSponsoring(branchId, startDate, endDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return US.GMS.Core.Utils.XMLUtils.SerializeDataContractObjectToXML(new List<ExcelineMemberDC>());
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool GenerateSponsorOrders(SponsorShipGenerationDetailsDC sponsorDetails, string user)
        {
            OperationResult<bool> result = GMSMembers.GenerateSponsorOrders(sponsorDetails, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool GetTest()
        {
            EntityActiveTimeDC active = new EntityActiveTimeDC();
            return active.IsSwitchSchedule;
        }

        public string GetSessionValue(string sessionKey, string user)
        {
            return Session.GetSession(sessionKey, ExceConnectionManager.GetGymCode(user));
        }

        public bool AddCountryDetails(CountryDC country, string user)
        {
            var result = GMSMember.AddCountryDetails(country, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return true;
            }
            return result.OperationReturnValue;
        }

        public bool AddRegionDetails(RegionDC region, string user)
        {
            var result = GMSMember.AddRegionDetails(region, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return true;
            }
            return result.OperationReturnValue;
        }

        public int AddSessionValue(string sessionKey, string value, string user)
        {
            return Session.AddSessionValue(sessionKey, value, ExceConnectionManager.GetGymCode(user));
        }

        public PDFPrintResultDC PrintInventoryList(int branchId, string user, int inventoryId)
        {
            var result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_InventoryList_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "InventoryId", inventoryId.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) }, { "BranchId", branchId.ToString() } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return result;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
            }
            return result;
        }

        public PDFPrintResultDC PrintResourceBookingCalender(int branchId, string user, DateTime fromDate, DateTime endDate)
        {
            var result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_ResourceBooking_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "FromDate", fromDate.ToString("yyyy-MM-dd") }, { "endDate", endDate.ToString("yyyy-MM-dd") }, { "gymCode", ExceConnectionManager.GetGymCode(user) }, { "BranchId", branchId.ToString() } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return result;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
            }
            return result;
        }

        public List<ExceUserBranchDC> GetUserSelectedBranches(string userName, string user)
        {
            OperationResultValue<List<ExceUserBranchDC>> result = ManageExcelineLogin.GetUserSelectedBranches(userName, user);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExceUserBranchDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #region Manage Anonymizing
        public int DeleteAnonymizingData(ExceAnonymizingDC anonymizing, int branchId, string user)
        {
            var result = GMSAnonymizing.DeleteAnonymizingData(anonymizing, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return 0;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExceAnonymizingDC> GetAnonymizingData(string user)
        {
            var result = GMSAnonymizing.GetAnonymizingData(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExceAnonymizingDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        public List<ExcelineRoleDc> GetUserRole(string user)
        {
            OperationResult<List<ExcelineRoleDc>> result = GMSGymDetail.GetUserRole(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineRoleDc>();
            }
            return result.OperationReturnValue;
        }

        public bool UploadArticle(List<ArticleDC> articleList, string user, int branchId)
        {
            OperationResult<bool> result = GMSArticle.UploadArticle(articleList, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int ValidateGiftVoucherNumber(string voucherNumber, decimal payment, string user)
        {
            var result = GMSShopManager.ValidateGiftVoucherNumber(voucherNumber, payment, ExceConnectionManager.GetGymCode(user));

            if (!result.ErrorOccured)
            {
                return result.OperationReturnValue;
            }
            foreach (var message in result.Notifications)
            {
                USLogError.WriteToFile(message.Message, new Exception(), user);
            }
            return -3;
        }

        public List<ContractSummaryDC> GetContractSummariesByEmployee(int employeeID, int branchId, DateTime createdDate, string user)
        {
            var result = GMSManageMembership.GetContractSummariesByEmployee(employeeID, branchId, createdDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ContractSummaryDC>();
            }
            return result.OperationReturnValue;
        }

        public List<ExcACCAccessControl> GetExcAccessControlList(int branchId, string user)
        {
            try
            {
                return GMSSystemSettings.GetExcAccessControlList(branchId, ExceConnectionManager.GetGymCode(user));
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("GetExcAccessControlList", ex, user);
                return new List<ExcACCAccessControl>();
            }
        }

        public string GetExcelineHostPath()
        {
            return ConfigurationManager.AppSettings["AccessControllLiveCopyWebHost"].Trim();
        }

        public bool UpdateSettingForUserRoutine(string key, string value, string user)
        {
            var result = GMSUser.UpdateSettingForUserRoutine(key, value, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            return result.OperationReturnValue;
        }

        public string GetResourceBookingViewMode(string user)
        {
            try
            {
                return GMSResources.GetResourceBookingViewMode(user, ExceConnectionManager.GetGymCode(user));
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("ERROR: GetResourceBookingViewMode" + ex.Message, ex, user);
                return "";
            }
        }

        public List<ContractTemplateDC> ValidateContractConditionWithTemplate(int contractConditionId, string user)
        {
            try
            {
                return GMSSystemSettings.ValidateContractConditionWithTemplate(contractConditionId, ExceConnectionManager.GetGymCode(user));
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("ERROR: ValidateContractConditionWithTemplate " + ex.Message, ex, user);
                return new List<ContractTemplateDC>();
            }
        }

        public int ValidateContractConditionWithContracts(int contractConditionId, string user)
        {
            try
            {
                return GMSSystemSettings.ValidateContractConditionWithContracts(contractConditionId, ExceConnectionManager.GetGymCode(user));
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("ERROR: ValidateContractConditionWithContracts " + ex.Message, ex, user);
                return -1;
            }
        }

        public ContractCondition TempContractCondition()
        {
            throw new NotImplementedException();
        }

        public string GetMemberSearchCategory(int branchId, string user)
        {
            OperationResult<string> result = GMSManageMembership.GetMemberSearchCategory(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return string.Empty;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public CustomerBasicInfoDC GetMemberBasicInfo(int memberId, string user)
        {
            OperationResult<CustomerBasicInfoDC> result = GMSManageMembership.GetMemberBasicInfo(memberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExceArxFormatDetail> GetArxSettingDetail(int branchId, string user)
        {
            OperationResult<List<ExceArxFormatDetail>> result = GMSSystemSettings.GetArxSettingDetail(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<ExceArxFormatType> GetArxFormatType(string user)
        {
            OperationResult<List<ExceArxFormatType>> result = GMSSystemSettings.GetArxFormatType(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public int SaveArxSettingDetail(ExceArxFormatDetail arxFormatDetail, int tempCodeDeleteDate, int branchId, string user)
        {
            OperationResult<int> result = GMSSystemSettings.SaveArxSettingDetail(arxFormatDetail, tempCodeDeleteDate, branchId, user, ExceConnectionManager.GetGymCode(user), "");
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

    }
}
