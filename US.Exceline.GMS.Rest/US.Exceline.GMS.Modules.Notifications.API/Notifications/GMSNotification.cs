﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : US.Exceline.GMS
// Project Name      : US.Exceline.GMS.Modules.Notifications.API
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 19/6/2012 4:27:11 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Notification;
using US.Exceline.GMS.Modules.Notifications.BusinessLogic.Notifications;

namespace US.Exceline.GMS.Modules.Notifications.API.Notifications
{
    public class GMSNotification
    {
        public static OperationResult<int> SaveNotification(NotificationDC notification, string createdUser, int branchId, string gymCode)
        {
            try
            {
                return NotificationManager.SaveNotification(notification, createdUser, branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static OperationResult<int> SaveNotificationTemplate(NotificationTemplateDC notificationTemplate, string createdUser, int branchId,string gymCode)
        {
            try
            {
                return NotificationManager.SaveNotificationTemplate(notificationTemplate, createdUser, branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OperationResult<List<NotificationMethodDC>> GetNotifications(int branchId,string gymCode)
        {
            try
            {
                return NotificationManager.GetNotifications(branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public static OperationResult<List<NotificationTemplateDC>> GetNotificationTemplates(int branchId, string gymCode)
        {
            try
            {
                return NotificationManager.GetNotificationTemplates(branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OperationResult<List<NotificationMethodDC>> GetNotificationsByMember(int branchId, int memberId, string gymCode)
        {
            try
            {
                return NotificationManager.GetNotificationsByMember(branchId, memberId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OperationResult<int> UpdateIsNotified(int notificationMethodId, string createdUser, int branchId, string gymCode)
        {
            try
            {
                return NotificationManager.UpdateIsNotified(notificationMethodId, createdUser, branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OperationResult<bool> DeleteNotificationMethod(int notificationMethodId, string createdUser, int branchId,string gymCode)
        {
            try
            {
                return NotificationManager.DeleteNotificationMethod(notificationMethodId, createdUser, branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
