﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands;
using US.Exceline.GMS.Modules.ManageClasses.Data.SystemObjects;
using US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters
{
    public class ManageClassFacade
    {
        private static IManageClassesDataAdapter GetDataAdapter()
        {
            return new SQLServerManageClassDataAdapter();
        }
        public static List<ExcelineClassDC> SearchClass(string category, string searchText, string gymCode)
        {
            return GetDataAdapter().SearchClass(category, searchText, gymCode);
        }

        public static List<ExcelineClassDC> GetClasses(string className, int branchId, string gymCode)
        {
            return GetDataAdapter().GetClasses(className, branchId, gymCode);
        }

        public static List<ExcelineClassDC> SearchClass(string category, string searchText, DateTime startDate, string gymCode)
        {
            return GetDataAdapter().SearchClass(category, searchText, startDate, gymCode);
        }

        public static List<ExcelineClassDC> SearchClass(string category, string searchText, DateTime startDate, DateTime endDate, string gymCode)
        {
            return GetDataAdapter().SearchClass(category, searchText, startDate, endDate, gymCode);
        }

        public static int SaveClass(ExcelineClassDC excelineClass, string gymCode)
        {
            return GetDataAdapter().SaveClass(excelineClass, gymCode);
        }

        public static bool UpdateClass(ExcelineClassDC excelineClass, string gymCode)
        {
            return GetDataAdapter().UpdateClass(excelineClass, gymCode);
        }

        public static bool DeleteClass(int classId, string gymCode)
        {
            return GetDataAdapter().DeleteClass(classId, gymCode);
        }

        public static List<ExcelineClassActiveTimeDC> GetExcelineClassActiveTimes(int branchId, DateTime startDate, DateTime endDate, int entNo, string gymCode)
        {
            return GetDataAdapter().GetClassActiveTimes(branchId, startDate, endDate, entNo, gymCode);
        }

        public static int GetNextClassId(string gymCode)
        {
            return GetDataAdapter().GetNextClassId(gymCode);
        }

        public static List<ExcelineMemberDC> GetMembersForClass(int classId, string gymCode)
        {
            return GetDataAdapter().GetMembersForClass(classId, gymCode);
        }

        public static List<InstructorDC> GetInstructorsForClass(int classId, string gymCode)
        {
            return GetDataAdapter().GetInstructorsForClass(classId, gymCode);
        }

        public static List<TrainerDC> GetTrainersForClass(int classId, string gymCode)
        {
            return GetDataAdapter().GetTrainersForClass(classId, gymCode);
        }

        public static List<ResourceDC> GetresourcesForClass(int classId, string gymCode)
        {
            return GetDataAdapter().GetResourcesForClass(classId, gymCode);
        }

        public static ScheduleDC GetClassSchedule(int classId, string gymCode)
        {
            return GetDataAdapter().GetClassSchedule(classId, gymCode);
        }

        public static List<ExcelineClassActiveTimeDC> GetClassHistory(int classId, List<ScheduleItemDC> scheduleItemList, string gymCode)
        {
            return GetDataAdapter().GetClassHistory(classId, scheduleItemList, gymCode);
        }

        public static List<OrdinaryMemberDC> GetMembersForClassBooking(int classId, int branchId, string searchText, string gymCode)
        {
            return GetDataAdapter().GetMembersForClassBooking(classId, branchId, searchText, gymCode);
        }

        public static List<ClassBookingActiveTimeDC> GetClassActiveTimesForBooking(int branchId, DateTime startDate, DateTime endDate, int entNo, string gymCode)
        {
            return GetDataAdapter().GetClassActiveTimesForBooking(branchId, startDate, endDate, entNo, gymCode);
        }

        public static List<MemberBookingDetailsDC> GetMemberBookingDetails(int classId, int branchId, string gymCode)
        {
            return GetDataAdapter().GetMemberBookingDetails(classId, branchId, gymCode);
        }

        public static int SaveMemberBookingDetails(int classId, OrdinaryMemberDC ordinaryMemberDC, int memberId, List<ClassBookingDC> bookingList, decimal totalBookingAmount, decimal totalAvailableAmount, string scheduleCategoryType, string user, int branchId, string gymCode)
        {
            return GetDataAdapter().SaveMemberBookingDetails(classId, ordinaryMemberDC, memberId, bookingList, totalBookingAmount, totalAvailableAmount, scheduleCategoryType, user, branchId, gymCode);
        }

        public static bool ApplyClassScheduleForNextYearAction(int classId, DateTime startdate, DateTime enddate, string name, int branchId, string createdUser, List<ScheduleItemDC> scheduleItemList, string gymCode)
        {
            return GetDataAdapter().ApplyClassScheduleForNextYear(classId, startdate, enddate, name, branchId, createdUser, scheduleItemList, gymCode);
        }

        public static bool GetVerificationOfAppliedClassScheduleAction(int classScheduleId, string gymCode)
        {
            return GetDataAdapter().GetVerificationOfAppliedClassScheduleAction(classScheduleId, gymCode);
        }

        public static List<ArticleDC> GetArticlesForClass(string gymCode)
        {
            return GetDataAdapter().GetArticlesForClass(gymCode);
        }

        public static int SaveClassBookingPayment(int memberId, int classId, BookingArticleDC bookingArticle, List<ClassBookingDC> PaidBookings, List<BookingPaymentDC> bookingPayments, decimal totalAmount, decimal paidAmount, decimal defaultPrice, string user, int branchId, string gymCode)
        {
            return GetDataAdapter().SaveClassBookingPayment(memberId, classId, bookingArticle, PaidBookings, bookingPayments, totalAmount, paidAmount, defaultPrice, user, branchId, gymCode);
        }

        public static int GetExcelineClassIdByName(string className, string gymCode)
        {
            return GetDataAdapter().GetExcelineClassIdByName(className, gymCode);
        }
    }
}
