﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class GetArticlesForClassAction : USDBActionBase<List<ArticleDC>>
    {
        public GetArticlesForClassAction()
        {
        }

        protected override List<ArticleDC> Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSManageClassesGetClassTypeArticles";
            List<ArticleDC> _articleList = new List<ArticleDC>();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ArticleDC article = new ArticleDC();
                    article.Id = Convert.ToInt32(reader["ArticleId"]);                   
                    article.Description = reader["ArticleName"].ToString();
                    _articleList.Add(article);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _articleList;
        }
    }
}
