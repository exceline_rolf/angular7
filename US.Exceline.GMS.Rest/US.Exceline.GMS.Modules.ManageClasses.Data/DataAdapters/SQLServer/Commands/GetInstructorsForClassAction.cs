﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class GetInstructorsForClassAction:USDBActionBase<List<InstructorDC>>
    {
        private int _classId = -1;

        public GetInstructorsForClassAction(int classId)
        {
            _classId = classId;
        }

        protected override List<InstructorDC> Body(DbConnection connection)
        {
            List<InstructorDC> _instructorLst = new List<InstructorDC>();
            string StoredProcedureName = "USExceGMSManageClassesGetInstructorsForClass";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    InstructorDC classInstructor = new InstructorDC();
                    classInstructor.Id = Convert.ToInt32(reader["InstructorId"]);
                    classInstructor.FirstName = reader["FirstName"].ToString();
                    classInstructor.LastName = reader["LastName"].ToString();
                    _instructorLst.Add(classInstructor);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _instructorLst;
        }
    }
}
