﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Collections.ObjectModel;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.ManageResources;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateEntityScheduleAction : USDBActionBase<int>
    {
        private int _parentId;
        private int _entityId;        
        private string _entityType;
        private int _categoryId;
        private string _name;      
        private int _branchId;

        public UpdateEntityScheduleAction(int parentId,string entityType, int categoryId, string name, int entityId, int branchId)
        {
            _parentId = parentId;          
            _entityType = entityType;
            _entityId = entityId;
            _categoryId = categoryId;
            _name = name;           
            _branchId = branchId;
        }  

 
        protected override int Body(DbConnection connection)
        {
            int _entityScheduleId = -1;
            string StoredProcedureName = "USExceGMSManageClassesUpdateEntitySchedule";
            //DbTransaction transaction = null;
            try
            {
               // transaction = connection.BeginTransaction();
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
               // cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@parentId", DbType.Int32, _parentId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityType", DbType.String, _entityType));                
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.String, _branchId));                              
                object obj = cmd.ExecuteScalar();
                _entityScheduleId = Convert.ToInt32(obj);
            }


            catch (Exception ex)
            {
                throw ex;
            }

            return _entityScheduleId;
        }

        public int RunOnTransaction(DbTransaction Transaction)
        {
             int _entityScheduleId = -1;
            string StoredProcedureName = "USExceGMSManageClassesUpdateEntitySchedule";
          
            try
            {               
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Connection = Transaction.Connection;
                cmd.Transaction = Transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@parentId", DbType.Int32, _parentId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityType", DbType.String, _entityType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.String, _branchId));
                object obj = cmd.ExecuteScalar();
                _entityScheduleId = Convert.ToInt32(obj);
            }


            catch (Exception ex)
            {
                throw ex;
            }

            return _entityScheduleId;
        }
    }
}

