﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "6/22/2012 20:36:23
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data;
using US.GMS.Core.SystemObjects;
using System.Data.SqlClient;
using US.GMS.Data.DataAdapters.SQLServer.Commands;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class SaveClassBookingPaymentAction : USDBActionBase<int>
    {
        private int _memberId;
        private int _classId;
        private BookingArticleDC _bookingArticle;
        private List<ClassBookingDC> _paidBookings;
        private List<BookingPaymentDC> _bookingPayments;
        private string _user;
        private int _branchId;
        private string _scheduleCategoryType;
        private decimal _defaultPrice;
        private decimal _totalAmount = 0;
        private decimal _paidAmount = 0;

        public SaveClassBookingPaymentAction(int memberId, int classId, BookingArticleDC bookingArticle, List<ClassBookingDC> paidBookings, List<BookingPaymentDC> bookingPayments, decimal totalAmount,decimal paidAmount, decimal defaultPrice, string user, int branchId)
        {
            this._memberId = memberId;
            this._classId = classId;
            this._bookingArticle = bookingArticle;
            this._paidBookings = paidBookings;
            this._bookingPayments = bookingPayments;
            this._user = user;
            this._branchId = branchId;
            _scheduleCategoryType = ScheduleCategoryTypes.CLASSBOOKING.ToString();
            _defaultPrice = defaultPrice;
            _totalAmount = totalAmount;
            _paidAmount = paidAmount;
        }

        protected override int Body(DbConnection connection)
        {

            return SaveClassBookingPaymentDetails(connection, "");
        }

        private int SaveClassBookingPaymentDetails(DbConnection connection, string invoiceNo)
        {
            string StoredProcedureName = "USExceGMSManageClassesSaveClassBookingPayment";
            DbTransaction dbtran = null;
            try
            {
                dbtran = connection.BeginTransaction();

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Connection = dbtran.Connection;
                cmd.Transaction = dbtran;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@bookingType", DbType.String, _scheduleCategoryType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@defaultPrice", DbType.Decimal, _defaultPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@invoiceNo", DbType.String, invoiceNo));

                DataTable paidActiveTimes = new DataTable();
                DataColumn col = null;
                col = new DataColumn("Id", typeof(Int32));
                paidActiveTimes.Columns.Add(col);

                foreach (var booking in _paidBookings)
                {
                    paidActiveTimes.Rows.Add(booking.ActiveTimeId);
                }

                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@paidClassActiveTimeList";
                parameter.SqlDbType = System.Data.SqlDbType.Structured;
                parameter.Value = paidActiveTimes;
                cmd.Parameters.Add(parameter);

                DataTable paymentTypes = new DataTable();
                DataColumn col2 = null;
                col2 = new DataColumn("PayModeId", typeof(Int32));
                paymentTypes.Columns.Add(col2);
                col2 = new DataColumn("Amount", typeof(Int32));
                paymentTypes.Columns.Add(col2);

                foreach (var payment in _bookingPayments)
                {
                    paymentTypes.Rows.Add(payment.PaymentTypeId, payment.Amount);
                }

                SqlParameter parameter2 = new SqlParameter();
                parameter2.ParameterName = "@paymentTypeLists";
                parameter2.SqlDbType = System.Data.SqlDbType.Structured;
                parameter2.Value = paymentTypes;
                cmd.Parameters.Add(parameter2);

                cmd.ExecuteScalar();
                dbtran.Commit();
                return 1;
            }

            catch (Exception ex)
            {
                dbtran.Rollback();
                return -1;
            }
        }
    }
}
