﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.IO;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class GetMembersForClassAction : USDBActionBase<List<ExcelineMemberDC>>
    {
        private int _classId = -1;

        public GetMembersForClassAction(int classId)
        {
            _classId = classId;
        }

        protected override List<ExcelineMemberDC> Body(DbConnection connection)
        {
            List<ExcelineMemberDC> _memberLst = new List<ExcelineMemberDC>();
            string StoredProcedureName = "USExceGMSManageClassesGetMembersForClass";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExcelineMemberDC classmember = new ExcelineMemberDC();
                    classmember.Id = Convert.ToInt32(reader["MemberId"]);
                    classmember.FirstName = reader["FirstName"].ToString();
                    classmember.LastName = reader["LastName"].ToString();
                    //classmember.ImagePath = Convert.ToString(reader["ImagePath"]);
                    //if (!string.IsNullOrEmpty(classmember.ImagePath))
                    //{
                    //    classmember.ProfilePicture = GetMemberProfilePicture(classmember.ImagePath);
                    //}
                    _memberLst.Add(classmember);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _memberLst;
        }

        private byte[] GetMemberProfilePicture(string p)
        {
            if (File.Exists(p))
            {
                byte[] byteArray = System.IO.File.ReadAllBytes(p);
                return byteArray;
            }
            return new byte[0];
        }
    }
}
