﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class ApplyClassScheduleForNextYearAction : USDBActionBase<bool>
    {
        private int _classId;
        private DateTime _startDate;
        private DateTime _endDate;
        private string _name;
        private int _branchId;
        private string _createdUser;
        private DataTable _dataTable;
        private List<ScheduleItemDC> _scheduleItemList; 

        public ApplyClassScheduleForNextYearAction(int classId,DateTime startdate, DateTime enddate, string name,int branchId, string createdUser,List<ScheduleItemDC> scheduleItemList)
        {
            this._classId = classId;
            this._startDate = startdate.AddYears(1);
            this._endDate = enddate.AddYears(1);
            this._name = name;
            this._branchId = branchId;
            this._createdUser = createdUser;
            this._scheduleItemList = scheduleItemList;
            this._dataTable = GetScheduleItemList(_scheduleItemList);
        }

        private DataTable GetScheduleItemList(List<ScheduleItemDC> scheduleItemList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ScheduleId", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("Occurrence", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("Day", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("Week", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("Month", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("Year", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("StartTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("EndTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("StartDate", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("EndDate", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("CreatedDateTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("LastModifiedDateTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("CreatedUser", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("LastModifiedUser", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("ActiveStatus", typeof(bool)));
            _dataTable.Columns.Add(new DataColumn("IsFixed", typeof(bool)));

            foreach (ScheduleItemDC scheduleitem in scheduleItemList)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["ScheduleId"] = 0;
                _dataTableRow["Occurrence"] = scheduleitem.Occurrence;
                _dataTableRow["Day"] = scheduleitem.Day;
                _dataTableRow["Week"] = scheduleitem.Week;
                _dataTableRow["Month"] = scheduleitem.Month;
                _dataTableRow["Year"] = scheduleitem.Year;
                _dataTableRow["StartTime"] = scheduleitem.StartTime.AddYears(1);
                _dataTableRow["EndTime"] = scheduleitem.EndTime.AddYears(1);
                _dataTableRow["StartDate"] = scheduleitem.StartDate.AddYears(1);
                _dataTableRow["EndDate"] = scheduleitem.EndDate.AddYears(1);
                _dataTableRow["CreatedDateTime"] = DateTime.Now;//scheduleitem.CreatedDate;
                _dataTableRow["LastModifiedDateTime"] = DBNull.Value;
                _dataTableRow["CreatedUser"] = scheduleitem.CreatedUser;
                _dataTableRow["LastModifiedUser"] = scheduleitem.LastModifiedUser;
                _dataTableRow["ActiveStatus"] = true;
                _dataTableRow["IsFixed"] = scheduleitem.IsFixed; 
                _dataTable.Rows.Add(_dataTableRow);
            }

            return _dataTable;
        }

        protected override bool Body(DbConnection connection)
        {
            string _storedProcedureName = "USExceGMSManageClassesApplyClassScheduleNextYear";
            bool _isAdded = false;

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, _storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate",DbType.Date,_startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate",DbType.Date,_endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassId",DbType.Int32,_classId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name",DbType.String,_name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId",DbType.Int32,_branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser",DbType.String,_createdUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemList",SqlDbType.Structured,_dataTable));
                cmd.ExecuteNonQuery();
                _isAdded = true;
            }

            catch (Exception ex)
            {
                throw ex; 
            }

            return _isAdded;
        }

    }
}
