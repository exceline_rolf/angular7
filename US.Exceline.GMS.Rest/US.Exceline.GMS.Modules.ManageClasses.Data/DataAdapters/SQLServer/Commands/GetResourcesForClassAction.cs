﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Admin.ManageResources;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class GetResourcesForClassAction : USDBActionBase<List<ResourceDC>>
    {
        private int _classId = -1;

        public GetResourcesForClassAction(int classId)
        {
            _classId = classId;
        }

        protected override List<ResourceDC> Body(DbConnection connection)
        {
            List<ResourceDC> _resourceLst = new List<ResourceDC>();
            string StoredProcedureName = "USExceGMSManageClassesGetResourcesForClass";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ResourceDC classResource = new ResourceDC();
                    classResource.Id = Convert.ToInt32(reader["ResourceId"]);
                    classResource.Name = reader["ResourceName"].ToString();
                    _resourceLst.Add(classResource);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _resourceLst;
        }
    }
}
