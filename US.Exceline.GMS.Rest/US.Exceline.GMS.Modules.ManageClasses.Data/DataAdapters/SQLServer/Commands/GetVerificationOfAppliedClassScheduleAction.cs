﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
   public class GetVerificationOfAppliedClassScheduleAction:US_DataAccess.USDBActionBase<bool>
    {
       private bool _isApplied = false;
       private int _classScheduleId;

       public GetVerificationOfAppliedClassScheduleAction(int classScheduleId)
       {
           this._classScheduleId = classScheduleId;
       }

       protected override bool Body(System.Data.Common.DbConnection connection)
       {
           string StoredProcedureName = "USExceGMSManageClassesGetVerificationOfScheduleApplied";
           int _year = -1;
           try
           {
               DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
               cmd.Parameters.Add(US_DataAccess.DataAcessUtils.CreateParam("@classScheduleId", DbType.Int32, _classScheduleId));
               object obj = cmd.ExecuteScalar();
               _year = Convert.ToInt32(obj);
               if (_year > 0)
                   _isApplied = true;
               else
                   _isApplied = false;
           }

           catch (Exception ex)
           {
               throw ex;
           }
           return _isApplied;
           
       }

    }
}
