﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CCXTestApplication.CCXService;
using US.USDF.Core.DomainObjects;
using US.USDF.Data;

namespace CCXTestApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

            int invoicesCount = Convert.ToInt32(txtInvoicesCount.Text.Trim());
            CCXService.CredicareServiceClient client = new CCXService.CredicareServiceClient();
            string invoices = client.GetAllInvoices(0,invoicesCount);

            USDFInvoiceInfo invoiceInfo = (USDFInvoiceInfo)US.GMS.Core.Utils.XMLUtils.DesrializeXMLToObject(invoices, typeof(USDFInvoiceInfo));

            //DBSetting dbSetting = new DBSetting();
            //dbSetting.ConnectionString = "Data Source="+txtServer.Text.Trim()+";Initial Catalog="+txtDatabase.Text.Trim()+";User ID="+txtUserName.Text.Trim()+"; Password="+txtpassword.Text.Trim()+";Asynchronous Processing=true";

            //USDFInvoiceInfo resu = USDFManager.UploadClaimsInfoWithResultBack(invoiceInfo, dbSetting);
            //USDFManager.TrigerSynchronouslyCaseWrite(resu.Token, dbSetting);

        }
    }
}
