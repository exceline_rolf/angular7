﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using US.Common.Logging.API;
using US.Common.Web.UI.Core.SystemObjects;
using US.Exceline.GMS.Modules.Operations.API.Operations;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.Utils;
using US.Payment.Core;

namespace US.Exceline.GMS.Modules.Operations.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class OperationsService : IOperationsService
    {
        public string GetMemberLoginDataPerDay(DateTime selectedDate, string username, int branchId)
        {
            //OperationResult<string> result = GMSOperations.GetMemberLoginDataPerDay(selectedDate, ExceConnectionManager.GetGymCode(username), branchId);
            //if (result.ErrorOccured)
            //{
            //    foreach (NotificationMessage message in result.Notifications)
            //    {
            //        USLogError.WriteToFile(message.Message, new Exception(), username);
            //    }
            //    return XMLUtils.SerializeDataContractObjectToXML(new List<MemberLogin>());
            //}
            //else
            //{
            //    return result.OperationReturnValue;
            //}
            return "";
        }

        public int SaveMemberVisit(EntityVisitDC memberVisit, string username, int branchId)
        {
            //OperationResult<int> result = GMSOperations.SaveMemberVisit(memberVisit, ExceConnectionManager.GetGymCode(username), branchId);
            //if (result.ErrorOccured)
            //{
            //    foreach (NotificationMessage message in result.Notifications)
            //    {
            //        USLogError.WriteToFile(message.Message, new Exception(), username);
            //    }
            //    return 0;
            //}
            //else
            //{
            //    return result.OperationReturnValue;
            //}
            return new int();
        }

        public int SaveMemberVisitList(List<EntityVisitDC> memberVisit, string username, int branchId)
        {
            //OperationResult<int> result = GMSOperations.SaveMemberVisitList(memberVisit, ExceConnectionManager.GetGymCode(username), branchId);
            //if (result.ErrorOccured)
            //{
            //    foreach (NotificationMessage message in result.Notifications)
            //    {
            //        USLogError.WriteToFile(message.Message, new Exception(), username);
            //    }
            //    return 0;
            //}
            //else
            //{
            //    return result.OperationReturnValue;
            //}
            return new int();
        }

        public bool AttachFile(string fileName, string username, int branchId)
        {
            DateTime currentTime = DateTime.Now;
            string companyCode = string.Empty;
            string branch = string.Empty;
            string newFileName = string.Empty;

            if (!string.IsNullOrEmpty(fileName))
            {
                string[] userParts = fileName.Split(new char[] { '-' });
                if (userParts.Length > 0)
                {
                    companyCode = userParts[0];
                    branch = userParts[1];
                    newFileName = userParts[2] + "-" + userParts[3] + "-"
                                    + userParts[4];
                }
            }
            string fullFileName = ConfigurationManager.AppSettings["UploadVisitXMLRoot"];
            fullFileName = fullFileName + @"\" + companyCode + @"\" + branch + @"\" + newFileName;

            while (true)
            {
                Thread.Sleep(5000);
                if ((DateTime.Now - currentTime).Seconds > 50)
                {
                    return false;
                }
                if (File.Exists(fullFileName))
                {
                    List<EntityVisitDC> memberVisitList = new List<EntityVisitDC>();
                    string text = File.ReadAllText(fullFileName, Encoding.GetEncoding("UTF-8"));
                    //string text = reader.ReadLine();
                    XElement xele = XElement.Parse(text);//xml format, one root
                    var visits = from member in xele.Elements()
                                 select new
                                {
                                    VisiterName = member.Element("VisiterName").Value,
                                    VisiterId = member.Element("VisiterId").Value,
                                    BranchId = member.Element("BranchId").Value,
                                    InTime = member.Element("Date").Value
                                };

                    foreach (var visit in visits)
                    {
                        EntityVisitDC memVisit = new EntityVisitDC();
                        memVisit.BranchId = Convert.ToInt32(visit.BranchId);
                        memVisit.InTime = Convert.ToDateTime(visit.InTime);
                        memVisit.EntityId = Convert.ToInt32(visit.VisiterId);
                        memVisit.EntityName = visit.VisiterName;

                        memberVisitList.Add(memVisit);
                    }
                    SaveMemberVisitList(memberVisitList, username, branchId)
                    ;
                    break;

                }
            }
            return true;
        }
    }
}
