﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.Exceline.GMS.Modules.Operations.Service
{
   [ServiceContract]
    public interface IOperationsService
    {
        [OperationContract]
        string GetMemberLoginDataPerDay(DateTime selectedDate, string username, int branchId);

        [OperationContract]
        int SaveMemberVisit(EntityVisitDC memberVisit, string username, int branchId);

        [OperationContract]
        int SaveMemberVisitList(List<EntityVisitDC> memberVisit, string username, int branchId);

        [OperationContract]
        bool AttachFile(string fileName, string username, int branchId);
    }
}
