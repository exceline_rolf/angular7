﻿using System;
using System.Linq;
using System.Collections.Generic;
using US.Common.Logging.API;
using US.Common.Notification.BusinessLogic;
using US.Common.Notification.Core;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.GMS.Core.DomainObjects.Common;
using US.Payment.Core.Enums;
using US.Payment.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.ResultNotifications;

namespace US.Common.Notification.API
{
    public class NotificationAPI
    {
        public static OperationResult<string> AddAutomatedSMSNotification(string gymCode, int branchID)
        {
            return NotificationManager.AddAutomatedSMSNotification(gymCode, branchID);
        }

        public static USImportResult<int> AddNotification(USNotificationTree notification)
        {
            return NotificationManager.AddNotification(notification);
        }

        public static USImportResult<int> AddNotification(USCommonNotificationDC commonNotification)
        {
            return NotificationManager.AddNotification(commonNotification);
        }

        public static int AddNotificationFromLogging(string message, List<string> categories, string severity, string description)
        {
            //TODO:
            USNotificationTree tree = new USNotificationTree();
            USNotification notification = new USNotification();
            List<USNotificationChannel> methods = new List<USNotificationChannel>();
            notification.CreatedDate = DateTime.Now;
            notification.CreatedUser = "System";
            notification.Module = categories[0];
            notification.Title = message;
            notification.Description = description;
            notification.CreatedUserRoleId = "MEM";
            notification.TimeStamp = TimeSpan.MaxValue;
            notification.TypeId = (int)NotificationTypeEnum.Messages;
            notification.SeverityId = (int)NotificationSeverityEnum.Critical;
            notification.StatusId = 1;
            notification.LastAttendDate = DateTime.Now;

            USNotificationAction action = new USNotificationAction();
            action.AssgnId = NotificationRunTimeSettings.GetNotificationReceiver(categories[0]);
            action.Comment = "";
            action.ReAssignId = "";
            action.TimeSpent = 0.0f;

            tree.Action = action;

            switch (severity)
            {
                case "Error":
                    {
                        USNotificationChannel email = new USNotificationChannel();
                        email.ChannelStatus = true;
                        email.ChannelAssign = NotificationRunTimeSettings.GetNotificationReceiver(categories[0]);
                        email.ChannelDueDate = DateTime.Now.AddDays(7);
                        email.ChannelMessage = message;
                        email.ChannelNextDueDate = DateTime.Now.AddDays(14);
                        email.ChannelType = 2;
                        email.Occurance = 1;
                        email.TemplateName = NotificationRunTimeSettings.GetNotificationTemplateName(categories[0]);
                        email.TemplateCategory = NotificationRunTimeSettings.GetNotificationTemplateCategory(categories[0]);
                        methods.Add(email);
                        notification.TypeId = (int)NotificationTypeEnum.Errors;
                        notification.SeverityId = (int)NotificationSeverityEnum.Critical;

                        break;

                    }
                case "Critical":
                    {
                        USNotificationChannel email = new USNotificationChannel();
                        email.ChannelStatus = true;
                        email.ChannelAssign = NotificationRunTimeSettings.GetNotificationReceiver(categories[0]);
                        email.ChannelDueDate = DateTime.Now.AddDays(7);
                        email.ChannelMessage = message;
                        email.ChannelNextDueDate = DateTime.Now.AddDays(14);
                        email.ChannelType = 2;
                        email.Occurance = 1;
                        email.TemplateName = NotificationRunTimeSettings.GetNotificationTemplateName(categories[0]);
                        email.TemplateCategory = NotificationRunTimeSettings.GetNotificationTemplateCategory(categories[0]);
                        methods.Add(email);

                        USNotificationChannel sms = new USNotificationChannel();
                        sms.ChannelStatus = true;
                        sms.ChannelAssign = NotificationRunTimeSettings.GetNotificationReceiver(categories[0]);
                        sms.ChannelDueDate = DateTime.Now.AddDays(7);
                        sms.ChannelMessage = message;
                        sms.ChannelNextDueDate = DateTime.Now.AddDays(14);
                        sms.ChannelType = 2;
                        sms.Occurance = 1;
                        sms.TemplateName = NotificationRunTimeSettings.GetNotificationTemplateName(categories[0]);
                        sms.TemplateCategory = NotificationRunTimeSettings.GetNotificationTemplateCategory(categories[0]);
                        methods.Add(sms);

                        notification.TypeId = (int)NotificationTypeEnum.Errors;
                        notification.SeverityId = (int)NotificationSeverityEnum.Critical;

                        break;

                    }
                case "Warning":
                    {
                        notification.TypeId = (int)NotificationTypeEnum.Warning;
                        notification.SeverityId = (int)NotificationSeverityEnum.Minor;
                        break;
                    }
                case "Information":
                    {
                        notification.TypeId = (int)NotificationTypeEnum.Messages;
                        notification.SeverityId = (int)NotificationSeverityEnum.Moderate;
                        break;
                    }
            }

            tree.Notification = notification;
            tree.NotificationMethods = methods;

            NotificationManager.AddNotification(tree);

            return 0;
        }

        public static USImportResult<bool> UpdateNotification(USNotification notification)
        {
            return NotificationManager.UpdateNotification(notification);
        }

        public static USImportResult<List<NotificationTemplateText>> GetNotificationTemplateText(string methodCode, string typeCode, string gymCode)
        {
            return NotificationManager.GetNotificationTemplateText(methodCode, typeCode, gymCode);
        }

        public static USImportResult<List<ExceNotificationTepmlateDC>> GetNotificationTemplateListByMethod(NotificationMethodType method, TextTemplateEnum temp, string user, int branchId)
        {
            return NotificationManager.GetNotificationTemplateListByMethod(method, temp, user, branchId);
        }

        public static USImportResult<bool> UpdateNotificationStatus(List<int> selectedIds, int status, string user)
        {
            return NotificationManager.UpdateNotificationStatus(selectedIds, status, user);
        }

        public static USImportResult<bool> UpdateNotificationStatus(int notificationID, string user, NotificationStatusEnum status, string statusMessage)
        {
            return NotificationManager.UpdateNotificationStatus(notificationID, user, status, statusMessage);
        }

        public static bool UpdateNotificationStatusWithDeliveryReport(int notificationId, string mobileNo, int statusId, string statusMessage, string user)
        {
            try
            {
                return NotificationManager.UpdateNotificationStatusWithDeliveryReport(notificationId, mobileNo, statusId, statusMessage, user);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
                return false;
            }
        }

        public static USImportResult<List<USNotificationSummaryInfo>> GetNotificationSummaryList(int branchId, NotificationStatusEnum status, string user)
        {
            return NotificationManager.GetNotificationSummaryList( branchId,status, user);
        }

        public static USImportResult<List<USNotificationSummaryInfo>> GetMemberNotificationSummaryList(int memberId, NotificationStatusEnum status, string user)
        {
            return NotificationManager.GetMemberNotificationSummaryList(memberId, status, user);
        }

        public static USImportResult<USNotificationSummaryInfo> GetNotificationByIdAction(int notificationId, string user)
        {
            return NotificationManager.GetNotificationByIdAction(user, notificationId);
        }

        public static USImportResult<List<USNotificationSummaryInfo>> GetNotificationsByMemberId(int memberId, string user)
        {
            return NotificationManager.GetNotificationsByMemberId(user, memberId);
        }

        public static USImportResult<int> SaveNotificationTextTemplate(int entityId,NotificationTemplateText templatetText, int branchId, string loggedUser)
        {
            return NotificationManager.SaveNotificationTextTemplate(entityId, templatetText, branchId, loggedUser);
        }

        public static USImportResult<NotificationTemplateText> GetNotificationTextByType(NotifyMethodType method, TextTemplateEnum type, string user, int branchId)
        {
            return NotificationManager.GetNotificationTextByType(method, type, user, branchId);
        }

        public static USImportResult<List<USNotificationSummaryInfo>> GetNotificationByNotifiyMethod(int memberId, string user, int branchId, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod)
        {
            return NotificationManager.GetNotificationByNotifiyMethod(memberId, user, branchId, fromDate, toDate, notifyMethod);
        }

        public static USImportResult<List<USNotificationSummaryInfo>> GetMemberNotificationSearchResult(int memberId, string user, string title, string assignTo, DateTime? receiveDateFrom, DateTime? receiveDateTo, DateTime? dueDateFrom, DateTime? dueDateTo, List<int> severityIdList, List<int> typeIdList, int statusId)
        {
            return NotificationManager.GetMemberNotificationSearchResult(memberId, user, title, assignTo, receiveDateFrom, receiveDateTo, dueDateFrom, dueDateTo, severityIdList, typeIdList, statusId);
        }

        public static USImportResult<List<USNotificationSummaryInfo>> GetNotificationSearchResult(int branchId, string user, string title, string assignTo, DateTime? receiveDateFrom, DateTime? receiveDateTo, DateTime? dueDateFrom, DateTime? dueDateTo, List<int> severityIdList, List<int> typeIdList, int statusId)
        {
            return NotificationManager.GetNotificationSearchResult( branchId,user, title, assignTo, receiveDateFrom, receiveDateTo, dueDateFrom, dueDateTo, severityIdList, typeIdList, statusId);
        }
       
        public static string ConvertTemplateToText(string text, List<TemplateField> templateFieldList)
        {
            string _text = string.Empty;
            string _editedText = string.Empty;
            _text = text;
            _editedText = text;
            List<TemplateFieldEnum> templateField = new List<TemplateFieldEnum>();
            int count = 0;
            int startIndex = 0;
            int endIndex = 0;
            if (!string.IsNullOrEmpty(_text))
            {
                for (int i = 0; i < _text.Length; i++)
                {
                    if (_text[i] == '[' && _text.Length > i && _text[count + 1] == '[')
                    {
                        startIndex = count + 2;
                        for (int j = count; j < _text.Length; j++)
                        {
                            if (_text[j] == ']' && _text.Length > j && _text[j + 1] == ']')
                            {
                                endIndex = j;
                                string word = (_text.Substring(startIndex, (endIndex - startIndex)));
                                try
                                {
                                    TemplateFieldEnum MyStatus = (TemplateFieldEnum)Enum.Parse(typeof(TemplateFieldEnum), word, true);
                                    templateField.Add(MyStatus);
                                    string oldValue = "[[" + MyStatus.ToString() + "]]";
                                    TemplateField field = templateFieldList.Where(s => s.Key == MyStatus).FirstOrDefault();
                                    string newValue = field.Value;
                                    if (_editedText.Contains(oldValue))
                                    {
                                        _editedText = _editedText.Replace(oldValue, newValue);
                                    }
                                }
                                catch
                                {
                                }
                                break;
                            }
                            else
                            {
                            }
                        }

                    }
                    count++;
                }
            }
            if (_editedText == null)
            {
                _editedText = string.Empty;
            }
            return _editedText;
        }

        public static USImportResult<List<USNotificationSummaryInfo>> GetChangeLogNotificationList(string user, int branchId, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod)
        {
            return NotificationManager.GetChangeLogNotificationList(user, branchId, fromDate, toDate, notifyMethod);
            //return new USImportResult<List<USNotificationSummaryInfo>>();
        }

        /// Automated Notification SMS 
        /// <param name="notificationID"></param>
        /// <param name="gymCode"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static OperationResult<List<NotificationSMS>> GetNotificationSMSToSend(int notificationID, string gymCode, string user)
        {
            return NotificationManager.GetNotificationSMSToSend(notificationID, gymCode, user);
        }
        public static OperationResult<bool> UpdateSMSNotificationList(List<NotificationSMS> smsList, string gymCode, string user)
        {
            return NotificationManager.UpdateSMSNotificationList(smsList, gymCode, user);
        }

        /// Automated Notification Email
        /// <param name="notificationID"></param>
        /// <param name="gymCode"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static OperationResult<List<NotificationEmail>> GetNotificationEmailToSend(int notificationID, string gymCode, string user)
        {
            return NotificationManager.GetNotificationEmailToSend(notificationID, gymCode, user);
        }
        public static OperationResult<bool> UpdateEmailNotificationList(List<NotificationEmail> emailList, string gymCode, string user)
        {
            return NotificationManager.UpdateEmailNotificationList(emailList, gymCode, user);
        }

        public static OperationResult<bool> AddNotificationWithReportScheduler(string gymCode, string user)
        {
            return NotificationManager.AddNotificationWithReportScheduler(gymCode, user);
        }

        public static bool UpdateNotificationWithReceiveSMS(string gymCompanyRef, string notificationCode, string message, string sender, string user)
        {
            try
            {
                return NotificationManager.UpdateNotificationWithReceiveSMS(gymCompanyRef, notificationCode, message, sender, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ExceNotificationScheduleItem> AutomatedPrintPDFGetItemList(string gymCode, string user)
        {
            try
            {
                return NotificationManager.AutomatedPrintPDFGetItemList(gymCode, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool AutomatedPrintPDFUpdateItemList(List<ExceNotificationScheduleItem> itemList, string gymCode, string user)
        {
            try
            {
                return NotificationManager.AutomatedPrintPDFUpdateItemList(itemList, gymCode, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OperationResult<List<EntityDC>> GetEntityList(string gymCode)
        {
            try
            {
                return NotificationManager.GetEntityList(gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OperationResult<List<CategoryDC>> GetTemplateTextKeyByEntity(string type, int entity, string gymCode)
        {
            try
            {
                return NotificationManager.GetTemplateTextKeyByEntity(type, entity, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static USImportResult<List<ExceNotificationTepmlateDC>> GetSMSNotificationTemplateByEntity(string entity, string gymCode)
        {
            return NotificationManager.GetSMSNotificationTemplateByEntity(entity, gymCode);
        }

        public static USImportResult<int> AddSMSNotification(string subject, string body, string mobileNo, string entity, int entityId, int memberId, int branchId, string user, string gymCode)
        {
            return NotificationManager.AddSMSNotification(subject, body, mobileNo, entity, entityId, memberId, branchId, user, gymCode);
        }


        public static USImportResult<List<AccessDeniedLog>> GetAccessDeniedList(DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            try
            {
                return NotificationManager.GetAccessDeniedList(fromDate, toDate, branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
