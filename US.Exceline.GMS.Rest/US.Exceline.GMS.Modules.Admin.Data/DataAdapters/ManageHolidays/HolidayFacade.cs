﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Admin.HolydayManagement;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageHolidays
{
    public class HolidayFacade
    {

        private static IAdminDataAdapter GetDataAdapter()
        {
            return new SQLServerAdminDataAdapter();
        }

        public static List<HolidayScheduleDC> GetHolidays(int branchId, string holidayType, DateTime startDate, DateTime endDate, string gymCode)
        {
            return GetDataAdapter().GetHolidays(branchId, holidayType, startDate, endDate, gymCode);
        }

        public static bool AddNewHoliday(HolidayScheduleDC newHoliday, string gymCode)
        {
            return GetDataAdapter().AddNewHoliday(newHoliday, gymCode);
        }

        public static int GetHolidayCategoryId(string categoryName, string gymCode)
        {
            return GetDataAdapter().GetHolidayCategoryId(categoryName, gymCode);
        }

        public static bool UpdateHolidayStatus(HolidayScheduleDC updateHoliday, string gymCode)
        {
            return GetDataAdapter().UpdateHolidayStatus(updateHoliday, gymCode);
        }
    }
}
