﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/29/2012 15:56:17
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageSystemSettings
{
    public class SystemSettingsFacade
    {
        private static IAdminDataAdapter GetDataAdapter()
        {
            return new SQLServerAdminDataAdapter();
        }

        public static int SaveBranchDetails(ExcelineBranchDC branch, string gymCode)
        {
            return GetDataAdapter().SaveBranchDetails(branch, gymCode);
        }

        public static bool ValidateCreditorNo(int branchId, string gymCode, string creditorNo)
        {
            return GetDataAdapter().ValidateCreditorNo(branchId, gymCode, creditorNo);
        }

        public static List<ExcelineCreditorGroupDC> GetBranchGroups(string gymCode)
        {
            return GetDataAdapter().GetBranchGroups(gymCode);
        }

        public static List<ExcelineBranchDC> GetBranches(string user, string gymCode, int branchId)
        {
            return GetDataAdapter().GetBranches(user, gymCode, branchId);
        }

        public static int UpdateBranchDetails(ExcelineBranchDC branch, string gymCode)
        {
            return GetDataAdapter().UpdateBranchDetails(branch, gymCode);
        }

        public static int AddActivitySettings(ActivitySettingDC activitySetting, string gymCode, string user)
        {
            return GetDataAdapter().AddActivitySettings(activitySetting, gymCode, user);
        }

        public static List<ArticleDC> GetInventoryList(int branchId, string gymCode)
        {
            return GetDataAdapter().GetInventoryList(branchId, gymCode);
        }


        #region System Settings
        public static List<VatCodeDC> GetVatCode(string gymCode)
        {
            return GetDataAdapter().GetVatCode(gymCode);
        }

        public static int AddUpdateVatCodes(VatCodeDC vatCodeDetail, string user, string gymCode)
        {
            return GetDataAdapter().AddUpdateVatCodes(vatCodeDetail, user, gymCode);
        }


        public static SystemSettingBasicInfoDC GetSystemSettingBasicInfo(string gymCode)
        {
            return GetDataAdapter().GetSystemSettingBasicInfo(gymCode);
        }

        public static SystemSettingEconomySettingDC GetSystemSettingEconomySetting(string gymCode)
        {
            return GetDataAdapter().GetSystemSettingEconomySetting(gymCode);
        }

        public static SystemSettingOtherSettingDC GetOtherSettings(string gymCode)
        {
            return GetDataAdapter().GetOtherSettings(gymCode);
        }

        public static List<ContractCondition> GetSystemSettingContractConditions(string gymCode)
        {
            return GetDataAdapter().GetSystemSettingContractConditions(gymCode);
        }
        
        public static int AddUpdateSystemSettingBasicInfo(SystemSettingBasicInfoDC SystemSettingBasicInfo, string user, string gymCode)
        {
            return GetDataAdapter().AddUpdateSystemSettingBasicInfo(SystemSettingBasicInfo, user, gymCode);
        }

        public static int AddUpdateSystemSettingEconomySetting(SystemSettingEconomySettingDC SystemSettingEconomySetting, string user, string gymCode)
        {
            return GetDataAdapter().AddUpdateSystemSettingEconomySetting(SystemSettingEconomySetting, user, gymCode);
        }

        public static int AddUpdateOtherSettings(SystemSettingOtherSettingDC otherSettingsDetail, string user, string gymCode)
        {
            return GetDataAdapter().AddUpdateOtherSettings(otherSettingsDetail, user, gymCode);
        }
        
        public static int AddUpdateSystemSettingContractCondition(ContractCondition SystemSettingContractCondition, string user, string gymCode)
        {
            return GetDataAdapter().AddUpdateSystemSettingContractCondition(SystemSettingContractCondition, user, gymCode);
        }


        public static List<string> GetClassKeyword(string gymCode)
        {
            return GetDataAdapter().GetClassKeyword(gymCode);
        }

        #endregion


        public static List<AccountDC> GetRevenueAccounts(string gymCode)
        {
            return GetDataAdapter().GetRevenueAccounts(gymCode);
        }

        public static int AddUpdateRevenueAccounts(AccountDC revenueAccountDetail, string user, string gymCode)
        {
            return GetDataAdapter().AddUpdateRevenueAccounts(revenueAccountDetail, user, gymCode);
        }

        public static int DeleteRevenueAccounts(AccountDC revenueAccountDetail, string user, string gymCode)
        {
            return GetDataAdapter().DeleteRevenueAccounts(revenueAccountDetail, user, gymCode);
        }

        public static decimal SaveStock(ArticleDC article, int branchId,string gymCode, string user)
        {
            return GetDataAdapter().SaveStock(article,branchId, gymCode, user);
        }

        public static int DeleteArticle(int articleId, int branchId, string gymCode, string user, bool isAdminUser)
        {
            return GetDataAdapter().DeleteArticle(articleId, branchId, gymCode, user, isAdminUser);
        }

        public static List<ActivityTimeDC> GetActivityUnavailableTimes(int branchId, string gymCode)
        {
            return GetDataAdapter().GetActivityUnavailableTimes(branchId, gymCode);
        }

        public static bool AddActivityUnavailableTimes(List<ActivityTimeDC> activityTimes, string gymCode)
        {
            return GetDataAdapter().AddActivityUnavailableTimes(activityTimes, gymCode);
        }

        public static int SaveInventory(InventoryItemDC inventoryItem, string gymCode, string user)
        {
            return GetDataAdapter().SaveInventory(inventoryItem, gymCode, user);
        }

        public static List<InventoryItemDC> GetInventory(int branchId, string gymCode)
        {
            return GetDataAdapter().GetInventory(branchId, gymCode);
        }

        public static int DeleteInventory(int inventoryId, string gymCode)
        {
            return GetDataAdapter().DeleteInventory(inventoryId, gymCode);
        }

        public static List<ArticleDC> GetInventoryDetail(int branchId, int inventoryId, string gymCode)
        {
            return GetDataAdapter().GetInventoryDetail(branchId, inventoryId, gymCode);
        }


        public static decimal SaveInventoryDetail(List<ArticleDC> articleList, int inventoryId, string gymCode)
        {
            return GetDataAdapter().SaveInventoryDetail(articleList, inventoryId, gymCode);
        }


        public static Dictionary<decimal, decimal> GetNetValueForArticle(int inventoryId, int articleId, int counted, int branchId, string gymCode)
        {
            return GetDataAdapter().GetNetValueForArticle(inventoryId, articleId, counted,branchId, gymCode);
        }

        public static CategoryDC GetCategoryByCode(string categoryCode, string gymCode)
        {
            return GetDataAdapter().GetCategoryByCode(categoryCode, gymCode);
        }

        #region followUp

        public static bool AddFollowUpTemplate(FollowUpTemplateDC template, string gymCode, int branchId)
        {
            return GetDataAdapter().AddFollowUpTemplate(template, gymCode, branchId);
        }

        public static List<FollowUpTemplateDC> GetFollowUpTemplateList(string gymCode, int branchId)
        {
            return GetDataAdapter().GetFollowUpTemplates(gymCode, branchId);

        }

        public static bool DeleteFollowUpTemplate(int followUpTemplate, string gymCode)
        {
            return GetDataAdapter().DeleteFollowUpTemplate(followUpTemplate, gymCode);

        }

        public static List<FollowUpTemplateTaskDC> GetTaskByFollowUpTemplateId(int followUpTemplateId, string gymCode)
        {
            return GetDataAdapter().GetTaskCategoryByFollowUpTemplateId(followUpTemplateId, gymCode);
        }


        #endregion

        public static List<ExcACCAccessControl> GetExcAccessControlList(int branchId, string gymCode)
        {
            return GetDataAdapter().GetExcAccessControlList(branchId, gymCode);
        }

        public static ExceACCMember GetLastAccMember(string gymCode, int branchId, int accTerminalId)
        {
            return GetDataAdapter().GetLastAccMember(gymCode, branchId, accTerminalId);
        }

        public static List<ExceArxFormatDetail> GetArxSettingDetail(int branchId, string gymCode)
        {
            return GetDataAdapter().GetArxSettingDetail(branchId, gymCode);
        }

        public static List<ExceArxFormatType> GetArxFormatType(string gymCode)
        {
            return GetDataAdapter().GetArxFormatType( gymCode);
        }


        public static int SaveArxSettingDetail(ExceArxFormatDetail arxFormatDetail, int tempCodeDeleteDate, int branchId, string user, string gymCode, string accessContolTypes)
        {
            return GetDataAdapter().SaveArxSettingDetail(arxFormatDetail, tempCodeDeleteDate, branchId, user, gymCode, accessContolTypes);
        }

        public static List<ContractTemplateDC> ValidateContractConditionWithTemplate(int contractConditionId, string gymCode)
        {
            return GetDataAdapter().ValidateContractConditionWithTemplate(contractConditionId, gymCode);
        }

        public static int ValidateContractConditionWithContracts(int contractConditionId, string gymCode)
        {
            return GetDataAdapter().ValidateContractConditionWithContracts(contractConditionId, gymCode);
        }

       
    }
}
