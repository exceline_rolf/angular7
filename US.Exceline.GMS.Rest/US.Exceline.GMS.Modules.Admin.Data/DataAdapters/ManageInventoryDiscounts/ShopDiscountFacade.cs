﻿using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.GMS.Core.DomainObjects.Admin;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageInventoryDiscounts
{
    public class DiscountFacade
    {
        private static IAdminDataAdapter GetDataAdapter()
        {
            return new SQLServerAdminDataAdapter();
        }

        public static bool AddDiscount(ShopDiscountDC newDiscount, string gymCode)
        {
            return GetDataAdapter().AddDiscount(newDiscount, gymCode);
        }

        public static List<ShopDiscountDC> GetDiscountsList(int branchId, bool isActive, string gymCode)
        {
            return GetDataAdapter().GetDiscountsList(branchId, isActive, gymCode);
        }

        public static bool UpdateDiscount(ShopDiscountDC updateDiscount, string gymCode)
        {
            return GetDataAdapter().UpdateDiscount(updateDiscount, gymCode);
        }

        public static List<InventoryItemDC> GetInventoryList(int branchId)
        {
            return new List<InventoryItemDC>();
        }

        public static bool DeleteDiscount(int discountId,string type,  string gymCode)
        {
            return GetDataAdapter().DeleteDiscount(discountId, type, gymCode);
        }
    }
}
