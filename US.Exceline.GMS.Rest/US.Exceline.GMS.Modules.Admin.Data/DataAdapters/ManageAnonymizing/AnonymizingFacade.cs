﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/29/2012 15:56:17
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Admin.ManageAnonymizing;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageAnonymizing
{
    public class AnonymizingFacade
    {
        private static IAdminDataAdapter GetDataAdapter()
        {
            return new SQLServerAdminDataAdapter();
        }

        public static int DeleteAnonymizingData(ExceAnonymizingDC anonymizing, string user, int branchId, string gymCode)
        {
            return GetDataAdapter().DeleteAnonymizingData(anonymizing,user, branchId, gymCode);
        }

        public static List<ExceAnonymizingDC> GetAnonymizingData(string gymCode)
        {
            return GetDataAdapter().GetAnonymizingData(gymCode);
        }
    }
}
