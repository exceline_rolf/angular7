﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "3/29/2012 8:25:25 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using System.Diagnostics;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageResources
{
    public class ResourcesFacade
    {
        private static IAdminDataAdapter GetDataAdapter()
        {
            return new SQLServerAdminDataAdapter();
        }

        public static int UpdateContractVisits(int selectedContractId, int punchedContractId, int bookingId, string gymCode, string user)
        {
            return GetDataAdapter().UpdateContractVisits(selectedContractId, punchedContractId, bookingId, gymCode, user);
        }

            public static List<ResourceBookingElemnt> GetResourceBooking(int scheduleId, string _startDateTime, string _endDateTime, string gymCode, string user)
        {
            return GetDataAdapter().GetResourceBooking(scheduleId, _startDateTime, _endDateTime, gymCode, user);
        }

        public static List<ContractInfo> GetPunchcardContractsOnMember(int memberId, string activity, string gymCode, string user)
            {
                return GetDataAdapter().GetPunchcardContractsOnMember(memberId, activity, gymCode, user);
            }


        public static int IsArticlePunchcard(int articleId, string gymCode, string user)
        {
            return GetDataAdapter().IsArticlePunchcard(articleId, gymCode, user);
        }

        public static int SaveResources(ResourceDC resource, string user, string gymCode)
        {
            return GetDataAdapter().SaveResources(resource, user, gymCode);
        }

        public static bool UpdateResource(ResourceDC resource, string user, string gymCode)
        {
            return GetDataAdapter().UpdateResource(resource, user, gymCode);

        }

        public static bool DeleteResource(int branchId, string user, string gymCode)
        {
            return GetDataAdapter().DeleteResource(branchId, user, gymCode);
        }

        public static bool DeActivateResource(int resourceId, int branchId, string user, string gymCode)
        {
            return GetDataAdapter().DeActivateResource(resourceId, branchId, user, gymCode);
        }

        public static bool SwitchResource(int resourceTd1, int resourceId2, DateTime startDate, DateTime endDate,string comment, int branchId, string user, string gymCode)
        {
            return GetDataAdapter().SwitchResource(resourceTd1, resourceId2, startDate, endDate, comment,branchId, user, gymCode);
        }

        public static bool SwitchResourceUndo(int resId, DateTime startDate, DateTime endDate, string user, int branchId,string gymCode)
        {
            return GetDataAdapter().SwitchResourceUndo(resId, startDate, endDate, user, branchId, gymCode);
        }

        public static bool ValidateScheduleWithResourceId(int resourceId, DateTime startDate, DateTime endDate, int branchId, string user, string gymCode)
        {
            return GetDataAdapter().ValidateScheduleWithResourceId(resourceId, startDate, endDate, branchId, user, gymCode);
        }

        public static bool ValidateScheduleWithSwitchResource(int resourceId1, int resourceId2, DateTime startDate, DateTime endDate, int branchId, string user, string gymCode)
        {
            return GetDataAdapter().ValidateScheduleWithSwitchResource(resourceId1, resourceId2, startDate, endDate, branchId, user, gymCode);
        }

        public static List<ResourceDC> GetResources(int branchId, string searchText, int categoryId, int activityId, int equipmentid, bool isActive, string gymCode)
        {
            return GetDataAdapter().GetResources(branchId, searchText, categoryId, activityId, equipmentid, isActive, gymCode);
        }

        public static List<ScheduleItemDC> GetBookingsOnDeletedScheduleItem(int resourceId, string gymCode)
        {
            return GetDataAdapter().GetBookingsOnDeletedScheduleItem(resourceId, gymCode);
        }


        public static ResourceDC GetResourceDetailsById(int resourceId, string gymCode)
        {
            return GetDataAdapter().GetResourceDetailsById(resourceId, gymCode);
        }

        public static List<ResourceDC> GetResourceCalender(int branchId, string gymCode, int hit)
        {
            return GetDataAdapter().GetResourceCalender(branchId, gymCode,hit);
        }

        public static List<ResourceActiveTimeDC> GetResourceActiveTimes(int branchId, DateTime startTime, DateTime endTime, int entNO, string gymCode)
        {
            return GetDataAdapter().GetResourceActiveTimes(branchId, startTime, endTime, entNO, gymCode);
        }

        public static int SaveScheduleItem(ScheduleItemDC scheduleItem, int resId, int branchId, string gymCode)
        {
            return GetDataAdapter().SaveScheduleItem(scheduleItem, resId, branchId,gymCode);
        }

        public static int ChangeResourceOnBooking(int scheduleItemId, int resourceId, string comment, string gymCode, string user, int branchId)
        {
            return GetDataAdapter().ChangeResourceOnBooking(scheduleItemId, resourceId, comment, gymCode, user, branchId);
        }

        public static List<AvailableResourcesDC> GetAvailableResources(int branchId,int resId, string gymCode)
        {
            return GetDataAdapter().GetAvailableResources(branchId, resId,gymCode);
        }

        public static List<AvailableResourcesDC> GetResourceAvailableTimeById(int branchId, int resId, string gymCode)
        {
            return GetDataAdapter().GetResourceAvailableTimeById(branchId, resId, gymCode);
        }

        public static ScheduleDC GetResourceScheduleItems(int resId, string roleTpye, string gymCode)
        {
            return GetDataAdapter().GetResourceScheduleItems(resId, roleTpye, gymCode);
        }

        public static List<ScheduleDC> GetResourceCalenderScheduleItems(List<int> resIdList, string roleTpye, string gymCode)
        {
            return GetDataAdapter().GetResourceCalenderScheduleItems(resIdList, roleTpye, gymCode);
        }

        public static List<string> ValidateResourcesWithSchedule(string roleType, List<AvailableResourcesDC> resourceList, int branchId, string gymCode)
        {
            return GetDataAdapter().ValidateResourcesWithSchedule(roleType, resourceList, branchId, gymCode);
        }

        public static bool AddOrUpdateAvailableResources(List<AvailableResourcesDC> resourceList, int branchId, string user)
        {
            return GetDataAdapter().AddOrUpdateAvailableResources(resourceList, branchId, user);
        }

        public static List<EntityActiveTimeDC> GetMemberBookingDetails(int branchId,int scheduleItemId, string gymCode)
        {
            return GetDataAdapter().GetMemberBookingDetails(branchId, scheduleItemId, gymCode);
        }

        public static bool ValidateScheduleWithPaidBooking(int scheduleItemId,int resourceId, string gymCode)
        {
            return GetDataAdapter().ValidateScheduleWithPaidBooking(scheduleItemId,resourceId, gymCode);
        }

        public static int DeleteResourcesScheduleItem(int scheduleItemId, string gymCode)
        {
            return GetDataAdapter().DeleteResourcesScheduleItem(scheduleItemId, gymCode);
        }

        public static int CheckDuplicateResource(int sourceId, int destinationId, DateTime startDate, DateTime endDate, string gymCode)
        {
            return GetDataAdapter().CheckDuplicateResource(sourceId,destinationId,startDate,endDate, gymCode);
        }

        public static string CheckActiveTimeOverlap(EntityActiveTimeDC activeTimeItem, string gymCode)
        {
            return GetDataAdapter().CheckActiveTimeOverlap(activeTimeItem, gymCode);
        }

        public static int SaveResourceActiveTimePunchcard(EntityActiveTimeDC activeTimeItem, int contractId, string gymCode, int branchId, string user)
        {
            Debug.WriteLine("facade");
            return GetDataAdapter().SaveResourceActiveTimePunchard(activeTimeItem, contractId, gymCode, branchId, user);
        }

        public static int SaveResourceActiveTimeRepeatablePunchcard(List<EntityActiveTimeDC> activeTimeItemList, int contractId, string gymCode, string user)
        {
            return GetDataAdapter().SaveResourceActiveTimeRepeatablePunchcard(activeTimeItemList, contractId, gymCode, user);
        }

        public static int SaveResourceActiveTimeRepeatable(List<EntityActiveTimeDC> activeTimeItemList, string gymCode, string user)
        {
            return GetDataAdapter().SaveResourceActiveTimeRepeatable(activeTimeItemList, gymCode, user);
        }

        public static int SaveResourceActiveTime(EntityActiveTimeDC activeTimeItem, string gymCode, int branchId, string user)
        {
            return GetDataAdapter().SaveResourceActiveTime(activeTimeItem, gymCode, branchId, user);
        }

        public static int DeleteResourceActiveTime(int activeTimeId,string articleName,DateTime visitDateTime, string roleType, bool isArrived, bool isPaid,List<int> memberList, string gymCode)
        {
            return GetDataAdapter().DeleteResourceActiveTime(activeTimeId, articleName, visitDateTime, roleType, isArrived, isPaid,memberList, gymCode);
        }

        public static List<ArticleDC> GetArticleForResouceBooking(int activityId, int branchId,string gymCode)
        {
            return GetDataAdapter().GetArticleForResouceBooking(activityId,branchId, gymCode);
        }

        public static List<ArticleDC> GetArticleForResouce(int activityId, int branchId, string gymCode)
        {
            return GetDataAdapter().GetArticleForResouce(activityId, branchId, gymCode);
        }

        public static Dictionary<string, List<int>> CheckUnusedCancelBooking(int activityId,int branchId, string accountNo, List<int> memberIdList, DateTime bookingDate, string gymCode)
        {
            return GetDataAdapter().CheckUnusedCancelBooking(activityId,branchId, accountNo,memberIdList, bookingDate, gymCode);
        }

        public static int UnavailableResourceBooking(int scheduleItemId, int activeTimeId, DateTime startDate, DateTime endDate, string gymCode, string user)
        {
            return GetDataAdapter().UnavailableResourceBooking(scheduleItemId, activeTimeId, startDate, endDate, gymCode, user);
        }

        public static Dictionary<string, List<int>> ValidatePunchcardContractMember(int activityId,int branchId,string accountNo, List<int> memberIdList, DateTime startDate, string gymCode)
        {
            return GetDataAdapter().ValidatePunchcardContractMember(activityId, branchId, accountNo, memberIdList, startDate, gymCode);
        }

        public static int SavePaymentBookingMember(int activetimeId, int articleId, int memberId, bool paid, int aritemNo, decimal amount,string paymentType, string gymCode)
        {
            return GetDataAdapter().SavePaymentBookingMember(activetimeId,articleId, memberId, paid, aritemNo, amount,paymentType, gymCode);
        }
        
        public static List<int> GetEmpolyeeForResources(List<int> resIdList, string gymCode)
         {
             return GetDataAdapter().GetEmpolyeeForResources(resIdList, gymCode);
         }

        public static List<int> ValidateResWithSwitchDataRange(List<int> resourceId, DateTime startDate, DateTime endDate, string gymCode)
        {
            return GetDataAdapter().ValidateResWithSwitchDataRange(resourceId, startDate, endDate, gymCode);
        }

        public static string GetResourceBookingViewMode(string user, string gymCode)
        {
            return GetDataAdapter().GetResourceBookingViewMode(user, gymCode);
        }

        public static bool ValidateArticleByActivityId(int activityId, int articleId, string gymCode)
        {
            return GetDataAdapter().ValidateArticleByActivityId(activityId,articleId, gymCode);
        }
    }
}
