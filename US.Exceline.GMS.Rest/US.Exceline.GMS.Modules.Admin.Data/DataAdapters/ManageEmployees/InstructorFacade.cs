﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageEmployees
{
    public class InstructorFacade
    {
        private static IAdminDataAdapter GetDataAdapter()
        {
            return new SQLServerAdminDataAdapter();
        }

        public static List<InstructorDC> GetInstructors(int branchId, string searchText, bool isActive, int instructorId, string gymCode)
        {
            return GetDataAdapter().GetInstructors(branchId, searchText, isActive, instructorId, gymCode);
        }

       
    }   
         
}
