﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageEmployees
{
    public class GymEmployeeFacade
    {
        private static IAdminDataAdapter GetDataAdapter()
        {
            return new SQLServerAdminDataAdapter();
        }

        public static string SaveGymEmployee(GymEmployeeDC gymEmployee, string gymCode)
        {
            return GetDataAdapter().SaveGymEmployee(gymEmployee, gymCode);
        }

        public static int UpdateGymEmployee(GymEmployeeDC gymEmployee, int branchId, string user, string gymCode)
        {
            return GetDataAdapter().UpdateGymEmployee(gymEmployee, branchId, user, gymCode);
        }

        //public static bool DeleteGymEmployee(int gymEmployeeId, int assignEmpId, int branchId, string user, string gymCode)
        //{
        //    return GetDataAdapter().DeleteGymEmployee(gymEmployeeId,assignEmpId, branchId, user, gymCode);
        //}

        public static bool DeActivateGymEmployee(int gymEmployeeId, int branchId, string user, string gymCode)
        {
            return GetDataAdapter().DeActivateGymEmployee(gymEmployeeId, branchId, user, gymCode);
        }

        public static List<GymEmployeeDC> GetGymEmployees(int branchId, string user, bool isActive,int roleId, string gymCode)
        {
            return GetDataAdapter().GetGymEmployees(branchId, user, isActive, roleId, gymCode);
        }
        
        public static GymEmployeeDC GetGymEmployeeById(int branchId, int employeeId, string gymCode)
        {
            return GetDataAdapter().GetGymEmployeeById(branchId, employeeId, gymCode);
        }

        public static GymEmployeeDC GetGymEmployeeByEmployeeId(int branchId, string user, int employeeId, string gymCode)
        {
            return GetDataAdapter().GetGymEmployeeByEmployeeId(branchId, user, employeeId, gymCode);
        }

        public static bool ValidateEmployeeFollowUp(int employeeId, DateTime endDate, string gymCode)
        {
            return GetDataAdapter().ValidateEmployeeFollowUp(employeeId, endDate, gymCode);
        }

        public static List<GymEmployeeRoleDurationDC> GetGymEmployeeRoleDuration(int employeeId, string gymCode)
        {
            return GetDataAdapter().GetGymEmployeeRoleDuration(employeeId, gymCode);
        }

        public static List<GymEmployeeWorkPlanDC> GetGymEmployeeWorkPlan(int employeeId,  string gymCode)
        {
            return GetDataAdapter().GetGymEmployeeWorkPlan(employeeId,  gymCode);
        }

        public static List<GymEmployeeApprovalDC> GetGymEmployeeApprovals(int employeeId, string approvalType, string gymCode)
        {
            return GetDataAdapter().GetGymEmployeeApprovals(employeeId,approvalType, gymCode);
        }

        public static bool AddEmployeeTimeEntry(EmployeeTimeEntryDC time,string gymCode)
        {
            return GetDataAdapter().AddEmployeeTimeEntry(time, gymCode);
        }

        public static List<EmployeeTimeEntryDC> GetEmployeeTimeEntries(int employeeId, string gymCode)
        {
            return GetDataAdapter().GetEmployeeTimeEntries(employeeId, gymCode);
        }

        public static bool ApproveEmployeeTimes(int employeeId, bool isAllApproved, string timeIdList, string gymCode)
        {
            return GetDataAdapter().ApproveEmployeeTimes(employeeId, isAllApproved, timeIdList,  gymCode);
        }

        public static int AdminApproveEmployeeWork(GymEmployeeWorkDC work, bool isApproved, string user, string gymCode)
        {
            return GetDataAdapter().AdminApproveEmployeeWork(work, isApproved, user, gymCode);
        }

        public static int SaveGymEmployeeWork(ScheduleItemDC scheduleItem,int resourceId, int branchId, string gymCode)
        {
            return GetDataAdapter().SaveGymEmployeeWork( scheduleItem, resourceId, branchId,  gymCode);
        }

        public static int SaveWorkActiveTime(GymEmployeeWorkPlanDC work, int branchId, string gymCode)
        {
            return GetDataAdapter().SaveWorkActiveTime(work, branchId, gymCode);
        }

        public static int SaveWorkItem(GymEmployeeWorkDC work, int branchId, string gymCode)
        {
            return GetDataAdapter().SaveWorkItem(work, branchId, gymCode);
        }


        public static bool DeleteGymEmployeeActiveTime(int activeTimeId, string gymCode)
        {
            return GetDataAdapter().DeleteGymEmployeeActiveTime(activeTimeId,  gymCode);
        }

        public static List<EmployeeEventDC> GetEmployeeEvents(int employeeId, string gymCode)
        {
            return GetDataAdapter().GetEmployeeEvents(employeeId, gymCode);
        }

        public static List<EmployeeJobDC> GetEmployeeJobs(int employeeId, string gymCode)
        {
            return GetDataAdapter().GetEmployeeJobs(employeeId, gymCode);
        }

        public static List<EmployeeBookingDC> GetEmployeeBookings(int employeeId, string gymCode)
        {
            return GetDataAdapter().GetEmployeeBookings(employeeId, gymCode);
        }

         

    }
}
