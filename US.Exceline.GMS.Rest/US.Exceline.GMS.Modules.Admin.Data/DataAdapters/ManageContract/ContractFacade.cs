﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "3/30/2012 2:32:52 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageContract
{
    public class ContractFacade
    {
        private static IAdminDataAdapter GetDataAdapter()
        {
            return new SQLServerAdminDataAdapter();
        }

        public static int SaveContract(PackageDC resource, string user, string gymCode)
        {
            return GetDataAdapter().SaveContract(resource, user, gymCode);
        }
        public static int UpdateContractPriority(Dictionary<int, int> packages, string user, string gymCode)
        {
            return GetDataAdapter().UpdateContractPriority(packages, user, gymCode);
        }
        public static bool UpdateContract(PackageDC resource, string user)
        {
            return GetDataAdapter().UpdateContract(resource, user);
        }

        public static bool DeActivateContract(int resourceId, int branchId, string user)
        {
            return GetDataAdapter().DeActivateContract(resourceId, branchId, user);
        }
        public static List<PackageDC> GetContracts(int branchId, string searchText, int searchType, string searchCriteria, string gymCode)
        {
            return GetDataAdapter().GetContracts(branchId, searchText, searchType, searchCriteria, gymCode);
        }
        public static string GetNextContractId(int branchId, string gymCode)
        {
            return GetDataAdapter().GetNextContractId(branchId, gymCode);
        }

        public static int DeleteContract(int contractId, string gymCode)
        {
            return GetDataAdapter().DeleteContract(contractId, gymCode);
        }

        public static List<ContractItemDC> GetContractItems(int branchId, string gymCode)
        {
            return GetDataAdapter().GetContractItems(branchId, gymCode);
        }

        public static List<PriceItemTypeDC> GetPriceItemTypes(int branchId, string gymCode)
        {
            return GetDataAdapter().GetPriceItemTypes(branchId, gymCode);
        }

        #region Contract Item
        public static bool AddContractItem(ContractItemDC contractItem, string user, int branchId, string gymCode)
        {
            return GetDataAdapter().AddContractItem(contractItem, user, branchId, gymCode);
        }

        public static bool RemoveContractItem(int contractItemID, string user, int branchId, string gymCode)
        {
            return GetDataAdapter().RemoveContractItem(contractItemID, user, branchId, gymCode);
        }
        #endregion
    }
}
