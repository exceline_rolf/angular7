﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class ChangeResourceOnBookingAction : USDBActionBase<int>
    {
        private int _scheduleItemId;
        private int _resourceId;
        private string _comment;
        private string _user;
        private int _returnVal;
        private int _branchId;

        public ChangeResourceOnBookingAction(int scheduleItemId, int resourceId, string comment, string user, int branchId)
        {
            this._scheduleItemId = scheduleItemId;
            this._resourceId = resourceId;
            this._comment = comment;
            this._user = user;
            this._returnVal = 0;
            this._branchId = branchId;
        }

        protected override int Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSChangeResouceOnBooking";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleItemId", DbType.Int32, _scheduleItemId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@resourceId", DbType.Int32, _resourceId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, _comment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@output";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
                this._returnVal = Convert.ToInt32(param.Value);
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return this._returnVal;
        }
    }
}
