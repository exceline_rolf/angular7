﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetEmployeeBookingAction : USDBActionBase<List<EmployeeBookingDC>>
    {
        private int _employeeId;

        public GetEmployeeBookingAction(int employeeId)
        {
            _employeeId = employeeId;
        }

        protected override List<EmployeeBookingDC> Body(System.Data.Common.DbConnection connection)
        {
            List<EmployeeBookingDC> employeeBookingList = new List<EmployeeBookingDC>();
            
            string storedProcedure1 = "USGMSAdminGetEmployeeBooking";
            string storedProcedure2 = "USGMSAdminGetMemberDetailsWithScheduleId";
            string storedProcedure3 = "USGMSAdminGetExtraResourceDetailsWithScheduleId";

            try
            {
                DbCommand cmd1 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure1);
                cmd1.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeId", DbType.Int32, _employeeId));
                DbDataReader reader1 = cmd1.ExecuteReader();

                while (reader1.Read())
                {
                    EmployeeBookingDC employeeBooking = new EmployeeBookingDC();

                    employeeBooking.EmployeeId = _employeeId;
                    employeeBooking.ScheduleId = Convert.ToInt32(reader1["ID"]);
                    employeeBooking.SalaryPerBooking = (reader1["SalaryPerBooking"] != DBNull.Value)? Convert.ToBoolean(reader1["SalaryPerBooking"]) : false;

                    employeeBooking.Day = Convert.ToString(reader1["Day"]);
                    employeeBooking.Date = Convert.ToDateTime(reader1["Date"]);
                    employeeBooking.StartTime = Convert.ToDateTime(reader1["StartTime"]);
                    employeeBooking.EndTime = Convert.ToDateTime(reader1["EndTime"]);
                    employeeBooking.Resource = Convert.ToString(reader1["ResourceName"]);
                    employeeBooking.IsApproved = (employeeBooking.SalaryPerBooking && (reader1["isPayed"] != DBNull.Value) && Convert.ToBoolean(reader1["isPayed"]));
                    employeeBooking.Activity = Convert.ToString(reader1["ActivityName"]);
                    employeeBooking.Category = Convert.ToString(reader1["CategoryName"]);
                    employeeBooking.BranchId = Convert.ToInt32(reader1["BranchId"]);
                    employeeBooking.BranchName = reader1["BranchName"].ToString();

                    employeeBookingList.Add(employeeBooking);
                }
                reader1.Close();

                foreach (var item in employeeBookingList)
                {
                    // Member List
                    List<ExcelineMemberDC> memberList = new List<ExcelineMemberDC>();
                    DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure2);
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", DbType.Int32, item.ScheduleId));
                    DbDataReader reader2 = cmd2.ExecuteReader();

                    while (reader2.Read())
                    {
                        ExcelineMemberDC _member = new ExcelineMemberDC();

                        _member.Id = ((reader2["MemberID"]) != DBNull.Value) ? Convert.ToInt32(reader2["MemberID"]) : 0;
                        _member.Name = ((reader2["MemberName"]) != DBNull.Value) ? Convert.ToString(reader2["MemberName"]) : "";

                        memberList.Add(_member);
                    }
                    item.MemberList = memberList;
                    reader2.Close();

                    // Get Extra Resource List
                    List<ResourceDC> resourceList = new List<ResourceDC>();
                    DbCommand cmd3 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure3);
                    cmd3.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", DbType.Int32, item.ScheduleId));
                    DbDataReader reader3 = cmd3.ExecuteReader();

                    while (reader3.Read())
                    {
                        ResourceDC _resource = new ResourceDC();

                        _resource.Id = ((reader3["ResourceID"]) != DBNull.Value) ? Convert.ToInt32(reader3["ResourceID"]) : 0;
                        _resource.Name = ((reader3["ResourceName"]) != DBNull.Value) ? Convert.ToString(reader3["ResourceName"]) : "";

                        resourceList.Add(_resource);
                    }
                    item.ResourceList = resourceList;
                    reader3.Close();
                }
            }
            catch
            {
                throw;
            }

            return employeeBookingList;
        }

    }
}
