﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
   public class UnavailableResourceBookingAction  : USDBActionBase<int>
   {
       private int _scheduleItemId;
       private int _activeTimeId;
       private DateTime _startDate;
       private DateTime _endDate;
       private string _user = string.Empty;
       public UnavailableResourceBookingAction(int scheduleItemId, int activeTimeId, DateTime startDate, DateTime endDate,string user)
       {
           _scheduleItemId = scheduleItemId;
           _activeTimeId = activeTimeId;
           _startDate = startDate;
           _endDate = endDate;
           _user = user;
       }
        protected override int Body(DbConnection connection)
        {
            int result = 0;
            const string storedProcedureName = "USExceGMSAdminUnAvailableResourceBooking";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _scheduleItemId));
                if (_activeTimeId > 0)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimeId",DbType.Int32, _activeTimeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDateTime",DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDateTime",DbType.DateTime, _endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                DbParameter para3 = new SqlParameter();
                para3.DbType = DbType.Int32;
                para3.ParameterName = "@outId";
                para3.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para3);
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(para3.Value);

            }
            catch (Exception)
            {
            }
            return result; 
        }
    }
}
