﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.SystemObjects;
using US_DataAccess;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveGymEmployeeAction : USDBActionBase<string>
    {
        private readonly GymEmployeeDC _gymEmployee;
        public SaveGymEmployeeAction(GymEmployeeDC gymEmployee)
        {
            _gymEmployee = gymEmployee;
        }

        protected override string Body(DbConnection connection)
        {
            int employeeId;
            string custId = string.Empty;
            const string storedProcedureName = "USExceGMSAdminSaveEmployee";
            DbTransaction transaction = null;
            try
            {
                transaction = connection.BeginTransaction();
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entFromMEM", DbType.Int32, _gymEmployee.EntNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@custId", DbType.String, _gymEmployee.CustId = string.IsNullOrEmpty(_gymEmployee.CustId) ? null : _gymEmployee.CustId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gymEmployeeId", DbType.Int32, _gymEmployee.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Boolean, _gymEmployee.ActiveStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@firstname", DbType.String, _gymEmployee.FirstName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastname", DbType.String, _gymEmployee.LastName));
                if (!_gymEmployee.BirthDay.Equals(DateTime.MinValue) && _gymEmployee.BirthDay.Date != new DateTime(1900, 1, 1))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@birthdate", DbType.DateTime, _gymEmployee.BirthDay));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@email", DbType.String, _gymEmployee.Email));
                if (!string.IsNullOrEmpty(_gymEmployee.Mobile))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@mobile", DbType.String, _gymEmployee.Mobile));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@mobilePrefix", DbType.String, _gymEmployee.MobilePrefix));
                }

                if (!string.IsNullOrEmpty(_gymEmployee.Work))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@telework", DbType.String, _gymEmployee.Work));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@teleworkPrefix", DbType.String, _gymEmployee.WorkTeleNoPrefix));
                }

                if (!string.IsNullOrEmpty(_gymEmployee.Private))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@telehome", DbType.String, _gymEmployee.Private));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@telehomePrefix", DbType.String, _gymEmployee.PrivateTeleNoPrefix));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address1", DbType.String, _gymEmployee.Address1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address2", DbType.String, _gymEmployee.Address2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address3", DbType.String, _gymEmployee.Address3));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@zip", DbType.String, _gymEmployee.PostCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@postplace ", DbType.String, _gymEmployee.PostPlace));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@role", DbType.String, _gymEmployee.RoleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gender", DbType.String, _gymEmployee.Gender));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@description", DbType.String, _gymEmployee.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _gymEmployee.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@empCardNo", DbType.String, !string.IsNullOrEmpty(_gymEmployee.EmployeeCardNo) ? _gymEmployee.EmployeeCardNo : null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@assignedEntity", DbType.Int32, _gymEmployee.AssignedEntityId));
                if (_gymEmployee.StartDate != DateTime.MinValue && _gymEmployee.StartDate!=null)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@startdate", DbType.DateTime, _gymEmployee.StartDate));

                if (_gymEmployee.StartDate == null)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@startdate", DbType.DateTime, DateTime.Now));

                cmd.Parameters.Add(_gymEmployee.EndDate != DateTime.MinValue
                                       ? DataAcessUtils.CreateParam("@enddate", DbType.DateTime, _gymEmployee.EndDate)
                                       : DataAcessUtils.CreateParam("@enddate", DbType.DateTime, DateTime.MaxValue.Date));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ImagePath", DbType.String, _gymEmployee.ImagePath));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReplaceStartDate", DbType.DateTime, _gymEmployee.TaskReplaceStartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@payrollNo", DbType.String, _gymEmployee.PayrollNumber));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReplaceEndDate", DbType.DateTime, _gymEmployee.TaskReplaceEndDate));
                if (_gymEmployee.HomeBranchId > 0)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@HomeBranchId", DbType.Int32, _gymEmployee.HomeBranchId));


                DbParameter outputEmp = new SqlParameter();
                outputEmp.DbType = DbType.Int32;
                outputEmp.ParameterName = "@OutEmployeeId";
                outputEmp.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputEmp);

                DbParameter outputCustId = new SqlParameter();
                outputCustId.DbType = DbType.String;
                outputCustId.ParameterName = "@OutCustId";
                outputCustId.Size = 50;
                outputCustId.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputCustId);

                cmd.ExecuteNonQuery();

                employeeId = Convert.ToInt32(outputEmp.Value);
                custId = Convert.ToString(outputCustId.Value);

                if (employeeId > 0)
                {
                    SaveEmployeeRole(_gymEmployee.ExceEmpRoleList, employeeId, transaction, _gymEmployee.CreatedUser);
                    transaction.Commit();
                }
                else
                {
                    transaction.Rollback();
                }
            }

            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
            return employeeId.ToString() +":" +custId;
        }


        private void SaveEmployeeRole(IEnumerable<ExcelineRoleDc> excelineRole, int empId, DbTransaction transaction,string user)
        {
            try
            {
                if (excelineRole != null)
                {
                    foreach (ExcelineRoleDc role in excelineRole)
                    {
                        var action = new SaveEmployeeRoleAction(role, empId, user);
                        action.SaveEmployeeRole(transaction);
                    }
                }
              
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
