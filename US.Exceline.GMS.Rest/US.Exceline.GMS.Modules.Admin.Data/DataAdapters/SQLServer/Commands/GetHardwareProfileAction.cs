﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetHardwareProfileAction : USDBActionBase<List<ExceHardwareProfileDC>>
    {
        private int _branchId;

        public GetHardwareProfileAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<ExceHardwareProfileDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ExceHardwareProfileDC> hardwareProfileList = new List<ExceHardwareProfileDC>();
            string storedProcedureName = "USExceGMSAdminGetHardwareProfiles";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExceHardwareProfileDC hardwareProfile = new ExceHardwareProfileDC();
                    hardwareProfile.Id = Convert.ToInt32(reader["ID"]);
                    hardwareProfile.Name = Convert.ToString(reader["Name"]);
                    hardwareProfileList.Add(hardwareProfile);
                }
                reader.Close();
            }
            catch
            {
                throw;
            }
            List<ExceHardwareProfileItemDC> hardwareProfileItemList = new List<ExceHardwareProfileItemDC>();
            string storedProcedureName2 = "USExceGMSAdminGetHardwareProfileItems";

            try
            {
                DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName2);
                cmd2.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader2 = cmd2.ExecuteReader();

                while (reader2.Read())
                {
                    ExceHardwareProfileItemDC hardwareProfileItem = new ExceHardwareProfileItemDC();
                    hardwareProfileItem.Id = Convert.ToInt32(reader2["ID"]);
                    hardwareProfileItem.HardwareProfileId = Convert.ToInt32(reader2["HardwareProfileId"]);
                    hardwareProfileItem.CategoryId = Convert.ToInt32(reader2["CategoryId"]);
                    hardwareProfileItem.Code = Convert.ToString(reader2["Code"]);
                    hardwareProfileItem.CategoryName = Convert.ToString(reader2["Name"]);
                    hardwareProfileItemList.Add(hardwareProfileItem);
                }
            }
            catch
            {
                throw;
            }
            foreach (ExceHardwareProfileDC hardwareProfile in hardwareProfileList)
            {
                foreach (ExceHardwareProfileItemDC hardwareProfileItem in hardwareProfileItemList)
                {
                    if (hardwareProfile.Id == hardwareProfileItem.HardwareProfileId)
                    {
                        hardwareProfile.HardwareProfileItemList.Add(hardwareProfileItem);
                        hardwareProfile.CategoryListString += hardwareProfileItem.CategoryName + ",";
                    }
                }
            }
            return hardwareProfileList;
        }
    }
}
