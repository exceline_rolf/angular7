﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Web.UI.Core.SystemObjects;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymEmployeeRolesAction : USDBActionBase<List<ExcelineRoleDc>>
    {
        private readonly int _employeeId;
        private readonly int _branchId;

        public GetGymEmployeeRolesAction(int employeeId,int branchId)
        {
            _employeeId = employeeId;
            _branchId = branchId;
        }

        protected override List<ExcelineRoleDc> Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSAdminGetEmployeeRoles";
            var exceEmpRoleList = new List<ExcelineRoleDc>();
            try
            {

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeeId", DbType.Int32, _employeeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                
                
                DbDataReader reader = cmd.ExecuteReader();
               
                while (reader.Read())
                {
                    var excelineRole = new ExcelineRoleDc
                        {
                            RoleId = Convert.ToInt32(reader["ID"]),
                            RoleName = Convert.ToString(reader["RoleName"]),
                            IsActive = true
                        };
                    exceEmpRoleList.Add(excelineRole);
                    
                }                     
                

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return exceEmpRoleList;
        }


    }
}
