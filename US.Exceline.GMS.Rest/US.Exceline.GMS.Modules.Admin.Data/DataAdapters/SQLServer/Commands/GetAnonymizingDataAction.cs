﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageAnonymizing;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetAnonymizingDataAction : USDBActionBase<List<ExceAnonymizingDC>>
    {
        protected override List<ExceAnonymizingDC> Body(DbConnection connection)
        {
            var anonymizingDcList = new List<ExceAnonymizingDC>();
            const string storedProcedureName = "USExceGMSAdminGetAnonymizingData";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
               
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var anonymizingDc = new ExceAnonymizingDC
                                                          {
                                                              Id = Convert.ToInt32(reader["Id"]),
                                                              Code = reader["Code"].ToString(),
                                                              Name = reader["Name"].ToString()
                                                          };
                    anonymizingDcList.Add(anonymizingDc);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return anonymizingDcList;
        }
    }
}
