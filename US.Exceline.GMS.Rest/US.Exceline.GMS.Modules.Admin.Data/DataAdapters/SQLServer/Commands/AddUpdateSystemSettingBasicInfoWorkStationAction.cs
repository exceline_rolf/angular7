﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer
{
    class AddUpdateSystemSettingBasicInfoWorkStationAction : USDBActionBase<int>
    {
        private SystemSettingBasicInfoDC _systemSettingBasicInfo = new SystemSettingBasicInfoDC();
        private string _user = string.Empty;
        private string _gymCode = string.Empty;

        public AddUpdateSystemSettingBasicInfoWorkStationAction(SystemSettingBasicInfoDC SystemSettingBasicInfo, string user, string gymCode)
        {
            _systemSettingBasicInfo = SystemSettingBasicInfo;
            _user = user;
            _gymCode = gymCode;
        }

        protected override int Body(DbConnection connection)
        {
            string spName = "ExceWorkStationAddUpdateSystemSettingBasicInfoWorkStation";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@GymCode", DbType.String, _gymCode));
                command.Parameters.Add(DataAcessUtils.CreateParam("@GymID", DbType.Int32, _systemSettingBasicInfo.GymCompanyID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@GymName", DbType.String, _systemSettingBasicInfo.GymCompanyName));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ModifiedBy", DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PostalCode", DbType.String, _systemSettingBasicInfo.PostalCode));
                
                command.ExecuteNonQuery();
                return 1;
            }
            catch
            {
                throw;
            }
        }
    }
}
