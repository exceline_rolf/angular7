﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/29/2012 14:57:41
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveCreditorGroupAction:USDBActionBase<int>
    {
        private ExcelineCreditorGroupDC _creditorGroup;

        public SaveCreditorGroupAction(ExcelineCreditorGroupDC creditorGroup)
        {
            this._creditorGroup = creditorGroup;
        }
        
        protected override int Body(DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USP_Add_BMD_CreditorGroup";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ID", DbType.Int32, _creditorGroup.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GroupName", DbType.String, _creditorGroup.GroupName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DisplayName", DbType.String, _creditorGroup.DisplayName));

                SqlParameter outputParam = new SqlParameter();
                outputParam.ParameterName = "@outPutID";
                outputParam.SqlDbType = SqlDbType.Int;
                outputParam.Direction = ParameterDirection.Output;
                
                cmd.Parameters.Add(outputParam);
                cmd.ExecuteScalar();

                int groupId = Convert.ToInt32(outputParam.Value);
                return groupId;
            }
            catch
            {
                return -1;
            }
        }

        /// <summary>
        /// Save Creditor Group Run On Transaction
        /// </summary>
        /// <param name="transaction">parent transaction object</param>
        /// <returns></returns>
        public int SaveCreditorGroup(DbTransaction transaction)
        {
            try
            {
                string storedProcedureName = "USP_Add_BMD_CreditorGroup";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ID", DbType.Int32, _creditorGroup.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GroupName", DbType.String, _creditorGroup.GroupName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DisplayName", DbType.String, _creditorGroup.DisplayName));

                SqlParameter outputParam = new SqlParameter();
                outputParam.ParameterName = "@outPutID";
                outputParam.SqlDbType = SqlDbType.Int;
                outputParam.Direction = ParameterDirection.Output;
                
                cmd.Parameters.Add(outputParam);
                cmd.ExecuteScalar();

                int groupId = Convert.ToInt32(outputParam.Value);
                return groupId;
            }
            catch 
            {
                return -1;
            }
        }
    }
}
