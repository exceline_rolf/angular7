﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetArticleForResouceAction : USDBActionBase<List<ArticleDC>>
    {
        private readonly int _activityId;
        private readonly int _branchId;
        public GetArticleForResouceAction(int activityId, int branchId)
        {
            _activityId = activityId;
            _branchId = branchId;
        }
        protected override List<ArticleDC> Body(DbConnection connection)
        {
            var articleList = new List<ArticleDC>();
            const string storedProcedureName = "USExceGMSAdminGetArticleForResource";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", DbType.Int32, _activityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var article = new ArticleDC
                        {
                            Id = Convert.ToInt32(reader["ID"]),
                            ArticleNo = reader["ArticleNo"].ToString(),
                            Description = reader["Name"].ToString(),
                            ArticleSettingId = Convert.ToInt32(reader["ArticleSettingId"])
                        };

                    articleList.Add(article);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return articleList;
        }
    }
}
