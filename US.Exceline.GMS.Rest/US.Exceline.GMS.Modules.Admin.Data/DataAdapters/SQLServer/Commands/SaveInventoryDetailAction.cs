﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveInventoryDetailAction : USDBActionBase<decimal>
    {
        private DataTable _dataTabel = null;
        private int _inventoryId;
        public SaveInventoryDetailAction(List<ArticleDC> articleList, int inventoryId)
        {
            _inventoryId = inventoryId;
            if (articleList != null)
            {
                _dataTabel = GetInventoryDetailLst(articleList);

            }
             
        }

        private DataTable GetInventoryDetailLst(List<ArticleDC> articleLst)
        {
            _dataTabel = new DataTable();
            _dataTabel.Columns.Add(new DataColumn("ID", Type.GetType("System.Int32")));
            _dataTabel.Columns.Add(new DataColumn("InventoryId", Type.GetType("System.Int32")));
            _dataTabel.Columns.Add(new DataColumn("ArticleId", Type.GetType("System.Int32")));
            _dataTabel.Columns.Add(new DataColumn("Counted", Type.GetType("System.Int32")));


            foreach (ArticleDC article in articleLst)
            {
                if (article.Counted > -1)
                {
                    DataRow dataTableRow = _dataTabel.NewRow();
                    dataTableRow["ID"] = article.InventoryDetailId;
                    dataTableRow["InventoryId"] = _inventoryId;
                    dataTableRow["ArticleId"] = article.Id;
                    dataTableRow["Counted"] = article.Counted;

                    _dataTabel.Rows.Add(dataTableRow);
                    
                }
              
            }

            return _dataTabel;
        }


        protected override decimal Body(DbConnection connection)
        {

            decimal result;
            const string storedProcedureName = "USExceGMSAdminSaveInventoryDetail";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InventoryData", SqlDbType.Structured, _dataTabel));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InventoryId", DbType.Int32, _inventoryId));


                SqlParameter output = new SqlParameter();
                output.DbType = DbType.Decimal;
                output.Precision = 18;
                output.Scale = 3;
                output.ParameterName = "@outId";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);

                cmd.ExecuteNonQuery();
                result = Convert.ToDecimal(output.Value);
               // result = 1;

            }
            catch (Exception ex)
            {
                
                throw ex;

            }
            return result;
       
        }
    }
}
