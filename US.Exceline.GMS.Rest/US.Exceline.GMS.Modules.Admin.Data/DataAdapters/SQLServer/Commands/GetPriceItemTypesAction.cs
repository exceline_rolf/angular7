﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "5/07/2012 3:05:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetPriceItemTypesAction : USDBActionBase<List<PriceItemTypeDC>>
    {
        private int _branchId;

        public GetPriceItemTypesAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<PriceItemTypeDC> Body(System.Data.Common.DbConnection connection)
        {
            List<PriceItemTypeDC> priceItemTypeList = new List<PriceItemTypeDC>();
            string SPGetItems = "USExceGMSAdminGetPriceItemTypes";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, SPGetItems);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    PriceItemTypeDC item = new PriceItemTypeDC();
                    item.Id = Convert.ToInt32(reader["ID"]);
                    item.Name = Convert.ToString(reader["Name"]);
                    item.Code = Convert.ToString(reader["Code"]);
                    item.BranchId = Convert.ToInt32(reader["BranchId"]);
                    priceItemTypeList.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return priceItemTypeList;
        }
    }
}
