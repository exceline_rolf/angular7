﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.Utils;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
   public class AssignTaskToEmployeeAction : USDBActionBase<int>
    {
       private ExcelineCommonTaskDC _commonTask = new ExcelineCommonTaskDC();
       private int _employeeId;
       private string _user = string.Empty;

       public AssignTaskToEmployeeAction(ExcelineCommonTaskDC commonTask, int employeeId,string user)
       {
           _commonTask = commonTask;
           _employeeId = employeeId;
           _user = user;
       }

      
        protected override int Body(DbConnection connection)
        {
            int updatedStatus = -1;
            const string storedProcedureName = "USExceGMSAssignTaskToEmployee";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@commonId", DbType.Int32, _commonTask.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeeId", DbType.Int32, _employeeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isFollowUp", DbType.Int32, _commonTask.IsFollowUp));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                SqlParameter output = new SqlParameter("@outputId", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                int outputId = Convert.ToInt32(output.Value);
                if (outputId > 0)
                {
                    updatedStatus = 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return updatedStatus;
        }
    }
}
