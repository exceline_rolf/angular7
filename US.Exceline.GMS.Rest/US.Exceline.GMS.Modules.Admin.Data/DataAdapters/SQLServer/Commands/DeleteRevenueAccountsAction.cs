﻿// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : Unicorngma
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US.GMS.Core.DomainObjects.Payments;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteRevenueAccountsAction : USDBActionBase<int>
    {
        private readonly AccountDC _revenueAccountDetail;
        private readonly string _user;
        public DeleteRevenueAccountsAction(AccountDC revenueAccountDetail, String user)
        {
            _revenueAccountDetail = revenueAccountDetail;
            _user = user;
        }

        protected override int Body(System.Data.Common.DbConnection dbConnection)
        {
            var output = 0;
            const string storedProcedureName = "USExceGMSAdminDeleteRevenueAccounts";
            try
            {
                var cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccountId", System.Data.DbType.Int32, _revenueAccountDetail.ID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                var obj = cmd.ExecuteScalar();
                output = Convert.ToInt32(obj);
             
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return output;
        }
    }
}
