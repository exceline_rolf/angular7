﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetResourceAction : USDBActionBase<List<ResourceDC>>
    {
        private int _branchId;
        private string _name;
        private bool _isActive = true;
        private int _categoryId;
        private string _gymCode = string.Empty;
        private List<ResourceDC> _resourceList = new List<ResourceDC>();
        public GetResourceAction(int branchId, string name, int categoryId, bool isActive, string gymCode)
        {
            _branchId = branchId;
            _name = name;
            _isActive = isActive;
            _categoryId = categoryId;
            _gymCode = gymCode;
        }

        protected override List<ResourceDC> Body(DbConnection connection)
        {
            //try
            //{
            //    DbTransaction dbtran = connection.BeginTransaction();
            //    GetResourceDetailsAction getAction = new GetResourceDetailsAction(_branchId, _name, _categoryId, _isActive);
            //    List<ResourceDC> resourceList = getAction.Execute(EnumDatabase.Exceline, _gymCode);

            //    foreach (ResourceDC emp in resourceList)
            //    {
            //        getAction.RunOnTransation(dbtran, emp);
            //    }
            //    _resourceList = resourceList;
            //    if (resourceList == null)
            //    {
            //        dbtran.Rollback();
            //        return new List<ResourceDC>();
            //    }
            //    dbtran.Commit();
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            return _resourceList;
        }
    }
}
