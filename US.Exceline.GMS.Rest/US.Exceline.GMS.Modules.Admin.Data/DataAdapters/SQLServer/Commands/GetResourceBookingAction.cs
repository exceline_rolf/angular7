﻿// --------------------------------------------------------------------------
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : Martin Tømmerås
// Created Timestamp : 23.08.2018 - 08:45
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using System.Collections.Generic;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetResourceBookingAction : USDBActionBase<List<ResourceBookingElemnt>>
    {
        private int _scheduleId;
        private string _startDateTime;
        private string _endDateTime;
        private List<ResourceBookingElemnt> _resourceBooking = new List<ResourceBookingElemnt>();

        public GetResourceBookingAction(int scheduleId, string startDateTime, string endDateTime)
        {
            this._scheduleId = scheduleId;
            this._startDateTime = startDateTime;
            this._endDateTime = endDateTime;
            
        }

        protected override List<ResourceBookingElemnt> Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSGetRescourceBooking";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleId", DbType.Int32, _scheduleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDateTime", DbType.String, _startDateTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndStartTime", DbType.String, _endDateTime));

                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    ResourceBookingElemnt resBookEl = new ResourceBookingElemnt();

                    resBookEl.ID = Convert.ToString(reader["ID"]);
                    resBookEl.ActiveTimeId = Convert.ToString(reader["ActiveTimeId"]);
                    resBookEl.EntityId = Convert.ToString(reader["EntityId"]);
                    resBookEl.ContractId = Convert.ToString(reader["ContractId"]);

                    _resourceBooking.Add(resBookEl);
                };
            }

            catch (Exception ex)
            {
                throw ex;
            }

            Debug.WriteLine(_resourceBooking);
            return _resourceBooking;
        }
    }
}
