﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteResourceActiveTimeAction : USDBActionBase<int>
    {
        private int _activeTimeId;
        private string _roleType;
        private bool _isArrived;
        private bool _isPaid;
        private string _articleName;
        private DateTime _visitDateTime;
        private DataTable _dataTable = null;
        private List<int> _memberList = new List<int>(); 

        public DeleteResourceActiveTimeAction(int activeTimeId, string articleName, DateTime visitDateTime, string roleType, bool isArrived, bool isPaid,List<int> memberlist )
        {
            _activeTimeId = activeTimeId;
            _roleType = roleType;
            _isArrived = isArrived;
            _isPaid = isPaid;
            _articleName = articleName;
            _visitDateTime = visitDateTime;
            _memberList = memberlist;
            _dataTable = GetMemList(memberlist);
        }

        private DataTable GetMemList(List<int> memList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ID", Type.GetType("System.Int32")));
            foreach (var item in memList)
            {
                DataRow dataTableRow = _dataTable.NewRow();
                dataTableRow["ID"] = item;
                _dataTable.Rows.Add(dataTableRow);
            }
            return _dataTable;
        }

        protected override int Body(DbConnection connection)
        {
            int result = -1;
            string spName = "USExceGMSUpdateResourceScheduleItemActiveTime";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimeId", DbType.Int32, _activeTimeId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@RoleType", DbType.String, _roleType));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArticleName", DbType.String, _articleName));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VisitDateTime", DbType.DateTime, _visitDateTime));
                command.Parameters.Add(_memberList != null
                                      ? DataAcessUtils.CreateParam("@MemberList", SqlDbType.Structured,
                                                                   _dataTable)
                                      : DataAcessUtils.CreateParam("@MemberList", SqlDbType.Structured, null));

                command.ExecuteNonQuery();
                result = 1;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
