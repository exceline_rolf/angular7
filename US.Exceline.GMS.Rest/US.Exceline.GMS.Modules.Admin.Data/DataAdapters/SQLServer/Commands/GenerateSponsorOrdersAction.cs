﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GenerateSponsorOrdersAction : USDBActionBase<bool>
    {
        private SponsorShipGenerationDetailsDC _sponsingDetails;
        private string _user = string.Empty;



        public GenerateSponsorOrdersAction(SponsorShipGenerationDetailsDC sponsingDetails, string createdUser)
        {
            _sponsingDetails = sponsingDetails;
            _user = createdUser;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSGenerateSpOrderManually";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@DueStartDate", System.Data.DbType.Date, _sponsingDetails.StartDueDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@DueEndDate", System.Data.DbType.Date, _sponsingDetails.EndDueDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SponsoringGymId", System.Data.DbType.Int32, _sponsingDetails.SposoringBranchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@UserBranchId", System.Data.DbType.Int32, _sponsingDetails.UserBranchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SponsoringDueDate", System.Data.DbType.Date, _sponsingDetails.SponsoringDueDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VisitStartDate", System.Data.DbType.Date, _sponsingDetails.VisitStartDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VisitEndDate", System.Data.DbType.Date, _sponsingDetails.VisitEndDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PaymentDueDate", System.Data.DbType.Date, _sponsingDetails.PaymentDueDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SponsoringGuid", System.Data.DbType.String, _sponsingDetails.SposoringGuiId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsOrderlinesAdded", System.Data.DbType.Boolean, _sponsingDetails.ShowOrderlines));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser ", System.Data.DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Sponsors", SqlDbType.Structured, GetSponsorIds(_sponsingDetails.SelectedSponsors)));
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        private DataTable GetSponsorIds(List<int> ids)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));

            for (int i = 0; i < ids.Count; i++)
            {
                DataRow dataTableRow = dataTable.NewRow();
                dataTableRow["ID"] = ids[i];
                dataTable.Rows.Add(dataTableRow);
            }
            return dataTable;
        }
    }
}
