﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetResourceBookingViewModeAction : USDBActionBase<string>
    {
        private readonly string _user = string.Empty;

        public GetResourceBookingViewModeAction(string user)
        {
            _user = user;
        }
        protected override string Body(DbConnection connection)
        {
            try
            {
                const string spName = "USExceGMSAdminGetResourceBookingViewMode";
                var cmd = CreateCommand(CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@userName", DbType.String, _user));
                var dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    return Convert.ToString(dataReader["item"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return "ToDay";
        }
    }
}
