﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class SwitchResourceAction : USDBActionBase<bool>
    {
        private int _resourceTd1 = -1;
        private int _resourceId2 = -1;
        private int _branchId = -1;
        private string _user = string.Empty;
        private DateTime _startDate = DateTime.Today;
        private DateTime _endDate = DateTime.MaxValue;
        private string _comment = string.Empty;

        public SwitchResourceAction(int resourceTd1, int resourceId2, DateTime startDate, DateTime endDate,string comment, int branchId, string user)
        {
            _resourceTd1 = resourceTd1;
            _resourceId2 = resourceId2;
            _startDate = startDate;
            _endDate = endDate;
            _branchId = branchId;
            _user = user;
            _comment = comment;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSAdminSwitchResource";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@resourceId", DbType.Int32, _resourceTd1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _resourceId2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, _comment));

                DbParameter para = new SqlParameter();
                para.DbType = DbType.Boolean;
                para.ParameterName = "@outId";
                para.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para);
                cmd.ExecuteNonQuery();
                bool result = Convert.ToBoolean(para.Value);
                return result;
            }

            catch (Exception)
            {
                return false;
            }
        }
    }
}
