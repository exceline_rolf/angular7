﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetEmployeeBookingsByScheduleAction : USDBActionBase<List<EmployeeBookingDC>>
    {
        private int _employeeId;
        private int _scheduleItemId;

        public GetEmployeeBookingsByScheduleAction(int employeeId, int scheduleItemId)
        {
            _employeeId = employeeId;
            _scheduleItemId = scheduleItemId;
        }

        protected override List<EmployeeBookingDC> Body(System.Data.Common.DbConnection connection)
        {
            List<EmployeeBookingDC> employeeBookingList = new List<EmployeeBookingDC>();

            string storedProcedure = "USGMSAdminGetEmployeeBookingsBySchedule";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeId", DbType.Int32, _employeeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _scheduleItemId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    EmployeeBookingDC employeeBooking = new EmployeeBookingDC();
                    employeeBooking.EmployeeId = _employeeId;
                    employeeBooking.ScheduleItemId = _scheduleItemId;
                    employeeBooking.Day = Convert.ToString(reader["Day"]);
                    employeeBooking.Date = Convert.ToDateTime(reader["Date"]);
                    employeeBooking.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    employeeBooking.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    employeeBooking.BranchId = Convert.ToInt32(reader["BranchId"]);
                    employeeBooking.BranchName = reader["BranchName"].ToString();
                    employeeBookingList.Add(employeeBooking);
                }
                reader.Close();
            }
            catch
            {
                throw;
            }

            return employeeBookingList;
        }

    }
}
