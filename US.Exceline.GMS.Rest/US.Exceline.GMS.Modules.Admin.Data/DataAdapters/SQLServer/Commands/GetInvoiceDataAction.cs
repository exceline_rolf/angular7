﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.SystemObjects;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetInvoiceDataAction : USDBActionBase<List<ExcelineInvoiceDetailDC>>
    {
        private DateTime _invoiceDate;
        private bool _isMemberRecords = true;
        private int _branchId = -1;
        private string _coRelationID = string.Empty;
        private Dictionary<int, Guid> _sponsorReleationIDs = new Dictionary<int, Guid>();
        private List<int> _memberIDList = new List<int>();
        public GetInvoiceDataAction(DateTime invoiceDate, bool isMemberRecords, int branchId)
        {
            _invoiceDate = invoiceDate;
            _isMemberRecords = isMemberRecords;
            _branchId = branchId;
            if (!_isMemberRecords)
                _coRelationID = Guid.NewGuid().ToString();
        }

        protected override List<ExcelineInvoiceDetailDC> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSGetInvoiceDetails";
            List<ExcelineInvoiceDetailDC> invoiceDetailsList = new List<ExcelineInvoiceDetailDC>();
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceDate", System.Data.DbType.DateTime, _invoiceDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsMemberRecords", System.Data.DbType.Boolean, _isMemberRecords));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _branchId));

                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExcelineInvoiceDetailDC invoiceDetail = new ExcelineInvoiceDetailDC();
                    invoiceDetail.InstallmentId = Convert.ToInt32(reader["ID"]);
                    invoiceDetail.MemberId = Convert.ToInt32(reader["MemberId"]);
                    invoiceDetail.MemberContractId = Convert.ToInt32(reader["MemberContractId"]);
                    invoiceDetail.Text = Convert.ToString(reader["Text"]);
                    invoiceDetail.InstallmentDate = Convert.ToDateTime(reader["InstallmentDate"]);
                    invoiceDetail.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    invoiceDetail.Amount = Convert.ToDecimal(reader["Amount"]);
                    invoiceDetail.BillingDate = Convert.ToDateTime(reader["BillingDate"]);
                    invoiceDetail.BillingFee = Convert.ToDecimal(reader["BillingFee"]);
                    invoiceDetail.AdonPrice = (reader["AddOnPrice"]!= DBNull.Value)? Convert.ToDecimal(reader["AddOnPrice"]):0;
                    invoiceDetail.Discount = (reader["Discount"] != DBNull.Value)? Convert.ToDecimal(reader["Discount"]):0;
                    invoiceDetail.SponsoredAmount =(reader["SponsoredAmount"] != DBNull.Value)? Convert.ToDecimal(reader["SponsoredAmount"]):0;
                    invoiceDetail.NumberOfReminders = Convert.ToInt32(reader["NumberOfReminders"]);
                    invoiceDetail.InstallmentNo = Convert.ToInt32(reader["InstallmentNo"]);
                    invoiceDetail.KID = Convert.ToString(reader["KID"]);
                    invoiceDetail.UnitPrice = Convert.ToDecimal(reader["UnitPrice"]);
                    invoiceDetail.ArticleNo = Convert.ToString(reader["ArticleNo"]);
                    invoiceDetail.IsATG = (Convert.ToBoolean(reader["IsATG"]) == true) ? 1 : 0;
                    invoiceDetail.BranchID = Convert.ToInt32(reader["BranchId"]);
                    invoiceDetail.ContractEndDate = Convert.ToDateTime(reader["ContractEndDate"]);
                    invoiceDetail.ContractKID = (reader["ContractKID"] != DBNull.Value) ? Convert.ToString(reader["ContractKID"]) : string.Empty; ;
                    invoiceDetail.CreditorNo = Convert.ToString(reader["CreditorNo"]);
                    invoiceDetail.MemberEntNo = Convert.ToInt32(reader["MemberEntNo"]);
                    invoiceDetail.GroupID = Convert.ToInt32(reader["GroupId"]);
                    invoiceDetail.TransType = Convert.ToInt32(reader["TransType"]);
                    invoiceDetail.OrderType = Convert.ToString(reader["OrderType"]);
                    invoiceDetail.NameOnContract = Convert.ToString(reader["NameOncontract"]);
                    invoiceDetail.AccountNo = Convert.ToString(reader["AccountNumber"]);
                    invoiceDetail.InvoiceNo = Convert.ToString(reader["InvoiceNo"]);
                    invoiceDetail.NoOfVisits = (reader["NoOfVisits"] != DBNull.Value) ? Convert.ToInt32(reader["NoOfVisits"]) : 0;
                    invoiceDetail.Amount = invoiceDetail.Amount - (invoiceDetail.Discount + invoiceDetail.SponsoredAmount);
                    invoiceDetail.InvoiceAmount = invoiceDetail.Amount;
                    invoiceDetailsList.Add(invoiceDetail);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return invoiceDetailsList;
        }
    }
}
