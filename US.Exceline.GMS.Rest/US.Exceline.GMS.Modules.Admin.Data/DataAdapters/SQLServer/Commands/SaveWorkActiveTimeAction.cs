﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.Utils;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
   public  class SaveWorkActiveTimeAction: USDBActionBase<int>
    {
       private GymEmployeeWorkPlanDC _work = new GymEmployeeWorkPlanDC();
       private int _branchId;
       private DateTime _startDateTime = DateTime.Now;
       private DateTime _endDateTime = DateTime.Now;
       public SaveWorkActiveTimeAction(GymEmployeeWorkPlanDC work, int branchId)
       {
           _branchId = branchId;
           _work = work;
           _startDateTime = new DateTime(_work.Date.Year, _work.Date.Month,_work.Date.Day, _work.StartTime.Hour, _work.StartTime.Minute, 0, 0);
           _endDateTime = new DateTime(_work.Date.Year, _work.Date.Month, _work.Date.Day, _work.EndTime.Hour, _work.EndTime.Minute, 0, 0);
       }

        protected override int Body(DbConnection connection)
        {
            int workId = -1;
            const string storedProcedureName = "USExceGMSAdminUpdateWorkActiveTime";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@workId", DbType.Int32, _work.Id));
                if (_work.StartTime != DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDateTime", DbType.DateTime, _startDateTime));
                if (_work.EndTime != DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDateTime", DbType.DateTime, _endDateTime));
                SqlParameter output = new SqlParameter("@outputId", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                int outputId = Convert.ToInt32(output.Value);
                if (outputId > 0)
                {
                    workId = 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return workId;
        }
    }
}
