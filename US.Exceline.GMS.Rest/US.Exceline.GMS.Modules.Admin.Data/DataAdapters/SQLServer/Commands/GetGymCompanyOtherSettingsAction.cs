﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US.GMS.Core.SystemObjects;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymCompanyOtherSettingsAction : USDBActionBase<SystemSettingOtherSettingDC>
    {
        protected override SystemSettingOtherSettingDC Body(DbConnection connection)
        {
            var otherSettings = new SystemSettingOtherSettingDC();
            try
            {
                const string storedProcedure = "USExceGMSAdminGetGymCompanyOtherSettings";
                var cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    otherSettings.Id                    = Convert.ToInt32(reader["ID"]);
                    otherSettings.ClassGeneratingDays   = Convert.ToString(reader["ClassGeneratingDays"]);
                    otherSettings.NoOfCopiesForInvoice  = Convert.ToString(reader["NoOfCopiesForInvoice"]);
                    otherSettings.NetsSendingNo         = Convert.ToString(reader["NetsSendingNo"]);
                    otherSettings.CustomerNo            = Convert.ToString(reader["CustomerNo"]);
                    otherSettings.ContractNo            = Convert.ToString(reader["ContractNo"]);
                    otherSettings.ExcludingCategories   = Convert.ToString(reader["ExcludingConnCategories"]);
                    otherSettings.IsBrisIntegrated      = Convert.ToBoolean(reader["IsBrisIntegrated"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return otherSettings;
        }
    }
}