﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveInstructorAction : USDBActionBase<string>
    {
        private InstructorDC _instructor;
        private DataTable _dataTable;
        public SaveInstructorAction(InstructorDC instructor)
        {
            _instructor = instructor;
            if (_instructor.ActivityList != null)
                _dataTable = GetActivityItemLst(_instructor.ActivityList);
        }

        private DataTable GetActivityItemLst(List<ActivityDC> activityList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ActivityId", Type.GetType("System.Int32")));
            foreach (ActivityDC activityItem in activityList)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["ActivityId"] = activityItem.Id;
                _dataTable.Rows.Add(_dataTableRow);
            }
            return _dataTable;
        }

        protected override string Body(System.Data.Common.DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminAddInstructor";
            string instructorId = "-1";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@instructorId", DbType.Int32, _instructor.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Boolean, _instructor.ActiveStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@firstname", DbType.String, _instructor.FirstName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastname", DbType.String, _instructor.LastName));
                if (!_instructor.BirthDay.Equals(DateTime.MinValue))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@birthdate", DbType.DateTime, _instructor.BirthDay));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@email", DbType.String, _instructor.Email));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@mobile", DbType.String, _instructor.Mobile));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@telework", DbType.String, _instructor.Work));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@telehome", DbType.String, _instructor.Private));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address1", DbType.String, _instructor.Address1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address2", DbType.String, _instructor.Address2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address3", DbType.String, _instructor.Address3));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@zip", DbType.String, _instructor.PostCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@postplace ", DbType.String, _instructor.PostPlace));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@role", DbType.String, _instructor.RoleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gender", DbType.String, _instructor.Gender));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@description", DbType.String, _instructor.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _instructor.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryId", DbType.Int32, _instructor.Category.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@assignedEntity", DbType.Int32, _instructor.AssignedEntityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, _instructor.CommentForInactive));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ImagePath", DbType.String, _instructor.ImagePath));
                if (_instructor.ActivityList != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityItemList", SqlDbType.Structured, _dataTable));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityItemList", SqlDbType.Structured, null));
                }
                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 50;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                instructorId = Convert.ToString(output.Value);
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return instructorId;
        }
    }
}
