﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/29/2012 14:57:47
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveCreditorAction : USDBActionBase<int>
    {
        private ExcelineCreditorDC _creditor;

        public SaveCreditorAction(ExcelineCreditorDC creditor)
        {
            this._creditor = creditor;
        }

        protected override int Body(DbConnection connection)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Save Creditor Run On Transaction
        /// </summary>
        /// <param name="transaction">parent transaction object</param>
        /// <returns></returns>
        public int SaveCreditor(DbTransaction transaction)
        {
            try
            {
                string storedProcedureName = "USP_AddCreditorExeline";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FirstName", DbType.String, _creditor.FirstName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastName", DbType.String, String.Empty));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PersonNo", DbType.String, String.Empty));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Born", DbType.Date, _creditor.BornDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditorEntNo", DbType.Int32, _creditor.CreditorEntNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditorInkassoID", DbType.String, _creditor.CreditorInkassoId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Reference1", DbType.String, String.Empty));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Addr1", DbType.String, _creditor.Addr1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Addr2", DbType.String, _creditor.Addr2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Addr3", DbType.String, _creditor.Addr3));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ZipCode", DbType.String, _creditor.ZipCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ZipName", DbType.String, _creditor.ZipName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CountryId", DbType.String, _creditor.CountryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@County", DbType.String, _creditor.Region));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Addrsource", DbType.String, String.Empty));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TelMobile", DbType.String, String.Empty));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TelWork", DbType.String, _creditor.TelWork));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TelHome", DbType.String, String.Empty));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Email", DbType.String, _creditor.Email));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Fax", DbType.String, _creditor.Fax));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MSN", DbType.String, String.Empty));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Skype", DbType.String, String.Empty));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SMS", DbType.String, String.Empty));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccountNo", DbType.String, _creditor.AccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@KidSwapAccountNo", DbType.String, _creditor.KidSwapAccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CollectionAccountNO", DbType.String, _creditor.CollectionAccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NotifyBank", DbType.Int32, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Startdate", DbType.Date, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Productno", DbType.Int32, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SendBBSOkDate", DbType.Date, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BBSOkDate", DbType.Date, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SendExlineOkDate", DbType.Date, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ExlineOkDate", DbType.Date, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SendKundeOkDate", DbType.Date, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ATGDate", DbType.Date, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CompanyID", DbType.String, _creditor.CompanyId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NETSID", DbType.String, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@COSTCATA", DbType.Int32, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@COSTCATB", DbType.Int32, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@COSTCATC", DbType.Int32, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@COSTCATD", DbType.Int32, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AllowPrint", DbType.Boolean, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AllowPrintDate", DbType.Date, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PrintDisableDate", DbType.Date, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PrintAllowComment", DbType.String, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@creditorGroupId", DbType.Int32, _creditor.CreditorGroupId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", DbType.Int32, _creditor.BranchID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Area", DbType.Int32, _creditor.Area));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BureauAccountNO", DbType.String, _creditor.BureauAccountNO));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@trace", DbType.Int32, 0));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReorderSmsReceiver", DbType.String, _creditor.ReorderSmsReceiver));

                SqlParameter outputParam = new SqlParameter();
                outputParam.ParameterName = "@outPutID";
                outputParam.SqlDbType = SqlDbType.Int;
                outputParam.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(outputParam);
                cmd.ExecuteScalar();

                if (!(outputParam.Value is DBNull))
                {
                    int entNo = Convert.ToInt32(outputParam.Value);
                    return entNo;
                }
                else
                {
                    return -1;
                }
            }
            catch
            {

                return -1;
            }
        }
    }
}
