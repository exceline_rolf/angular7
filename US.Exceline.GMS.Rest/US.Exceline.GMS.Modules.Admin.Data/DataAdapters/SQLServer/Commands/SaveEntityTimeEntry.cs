﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "5/19/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveEntityTimeEntry : USDBActionBase<bool>
    {
        private TimeEntryDC _timeEntry;

        public SaveEntityTimeEntry(TimeEntryDC timeEntry)
        {
            _timeEntry = timeEntry;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            try
            {
                string storedProcedureName = "USExceGMSAdminSaveEditEntityTimeEntry";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _timeEntry.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _timeEntry.EntityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRoleType", DbType.String, _timeEntry.EntityRoleType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@taskId", DbType.Int32, _timeEntry.TaskId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@executedDate", DbType.DateTime, _timeEntry.ExecutedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdDateTime", DbType.DateTime, _timeEntry.CreatedDateTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastModifiedDateTime", DbType.DateTime, _timeEntry.LastModifiedDateTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _timeEntry.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastModifiedUser", DbType.String, _timeEntry.LastModifiedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@spendTime", DbType.Decimal, _timeEntry.SpendTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@timeChange", DbType.Decimal, _timeEntry.TimeChange));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId ", DbType.Int32, _timeEntry.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, _timeEntry.Comment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TaskStatusCategoryId", DbType.Int32, _timeEntry.TaskStatusCategoryId));
                cmd.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result; ;
        }
    }
}
