﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetSMSSendingTimesAction : USDBActionBase<List<ExceSMSSendingTimeDC>>
    {
        private readonly int _branchId = -1;
        public GetSMSSendingTimesAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<ExceSMSSendingTimeDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ExceSMSSendingTimeDC> sendingTimes = new List<ExceSMSSendingTimeDC>();
            string spname = "USExceGMSAdminGetSMSSendingTimes";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spname);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", System.Data.DbType.Int32, _branchId));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExceSMSSendingTimeDC sendingTime = new ExceSMSSendingTimeDC();
                    sendingTime.Id = Convert.ToInt32(reader["ID"]);
                    sendingTime.BranchId = Convert.ToInt32(reader["BranchId"]);
                    sendingTime.SendAfterTime = Convert.ToDateTime(reader["FromTime"]);
                    sendingTime.IsMonday = Convert.ToBoolean(reader["Monday"]);
                    sendingTime.IsTuesday = Convert.ToBoolean(reader["Tuesday"]);
                    sendingTime.IsWednesday = Convert.ToBoolean(reader["Wednesday"]);
                    sendingTime.IsThursday = Convert.ToBoolean(reader["Thursday"]);
                    sendingTime.IsFriday = Convert.ToBoolean(reader["Friday"]);
                    sendingTime.IsSaturday = Convert.ToBoolean(reader["Saturday"]);
                    sendingTime.IsSunday = Convert.ToBoolean(reader["Sunday"]);

                    sendingTimes.Add(sendingTime);
                }

            }
            catch (Exception)
            {
                throw;
            }

            return sendingTimes;
        }
    }
}
