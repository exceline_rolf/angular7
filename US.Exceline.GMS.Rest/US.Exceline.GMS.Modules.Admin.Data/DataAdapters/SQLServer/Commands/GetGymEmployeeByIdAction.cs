﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "4/24/2012 2:50:24 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class GetGymEmployeeByIdAction : USDBActionBase<GymEmployeeDC>
    {
        private int _branchId = 1;
        private int _employeeId = -1;

        public GetGymEmployeeByIdAction(int branchId, int employeeId)
        {
            this._branchId = branchId;
            this._employeeId = employeeId;   
        }
        protected override GymEmployeeDC Body(System.Data.Common.DbConnection connection)
        {
            GymEmployeeDC gymEmployee = new GymEmployeeDC();
            string StoredProcedureName = "USExceGMSAdminGetGymEmployeeById";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeeId", DbType.Int32, _employeeId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    gymEmployee.Id = Convert.ToInt32(reader["EmployeeId"]);
                    gymEmployee.EntNo = Convert.ToInt32(reader["EntNo"]);
                    if (!string.IsNullOrEmpty(reader["Name"].ToString()))
                    {
                        gymEmployee.Name = Convert.ToString(reader["Name"]);
                    }
                    string[] name = gymEmployee.Name.Split(' ');
                    gymEmployee.FirstName = name[0];
                    gymEmployee.LastName = name[1];
                    
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return gymEmployee;
        }
    }
}
