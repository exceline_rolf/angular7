﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateArticleByActivityIdAction : USDBActionBase<bool>
    {
        private readonly int _activityId = 10;
        private readonly int _articleId = 50;
        public ValidateArticleByActivityIdAction(int activityId ,int articleId)
        {
            _activityId = activityId;
            _articleId = articleId;
        }
        protected override bool Body(DbConnection connection)
        {
            bool result;
            string spName = "USExceGMSAdminValidateArticleByActivity";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", DbType.Int32, _activityId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArticleId", DbType.Int32, _articleId));



                DbParameter output = new SqlParameter();
                output.DbType = DbType.Boolean;
                output.ParameterName = "@OutPut";
                output.Direction = ParameterDirection.Output;
                command.Parameters.Add(output);
                command.ExecuteNonQuery();
                result = Convert.ToBoolean(output.Value);
                //result  = (bool) command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
