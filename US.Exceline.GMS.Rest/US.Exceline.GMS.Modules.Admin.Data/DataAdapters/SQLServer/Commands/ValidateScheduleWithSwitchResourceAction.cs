﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class ValidateScheduleWithSwitchResourceAction : USDBActionBase<bool>
    {
        private int _resourceId1     = -1;
        private int _resourceId2     = -1;
        private DateTime _startDate = DateTime.Today;
        private DateTime _endDate   = DateTime.MaxValue;
        private int _branchId       = -1;
        private string _user        = string.Empty;

        public ValidateScheduleWithSwitchResourceAction(int resourceId1, int resourceId2, DateTime startDate, DateTime endDate, int branchId, string user)
        {
            _resourceId1 = resourceId1;
            _resourceId2 = resourceId2;
            _startDate = startDate;
            _endDate = endDate;
            _branchId = branchId;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            string storedProcedureName = "USExceGMSAdminValidateScheduleWithSwitchResource";
            string scheduleId = string.Empty;

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@resourceId1", DbType.Int32, _resourceId1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@resourceId2", DbType.Int32, _resourceId2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _endDate));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    scheduleId = Convert.ToString(reader["ScheduleId"]); break;
                }
            }
            catch (Exception)
            {
            }

            if (string.IsNullOrEmpty(scheduleId))
                return false;
            else
                return true;
        }
    }
}
