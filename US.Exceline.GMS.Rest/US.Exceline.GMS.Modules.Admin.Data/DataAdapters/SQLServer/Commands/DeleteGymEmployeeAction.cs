﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteGymEmployeeAction : USDBActionBase<bool>
    {
        private int _employeeId;
        private string _user = string.Empty;
        private int _assignEmpId;
        public DeleteGymEmployeeAction(int employeeId ,int assignEmpId,  string user)
        {
            _employeeId = employeeId;
            _user = user;
            _assignEmpId = assignEmpId;

        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSAdminDeleteGymEmployee";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeeId", System.Data.DbType.Int32, _employeeId));
                if (_assignEmpId > 0)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@assignEmpId", System.Data.DbType.Int32, _assignEmpId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", System.Data.DbType.String, _user));
                
                cmd.ExecuteScalar();
                result = true;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException("", ex);
            }
            return result;
        }
    }
}
