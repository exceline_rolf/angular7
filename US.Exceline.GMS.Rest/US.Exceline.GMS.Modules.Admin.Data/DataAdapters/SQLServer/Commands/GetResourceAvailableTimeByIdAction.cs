﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
  public class GetResourceAvailableTimeByIdAction : USDBActionBase<List<AvailableResourcesDC>>
    {
        private readonly int _branchId = -1;
        private readonly int _resourceId = -1;

        public GetResourceAvailableTimeByIdAction(int branchId, int resId)
        {
            _branchId = branchId;
            _resourceId = resId;
        }

        protected override List<AvailableResourcesDC> Body(DbConnection connection)
        {
            var availableTimeResList = new List<AvailableResourcesDC>();
            const string storedProcedureName = "USExceGMSAdminGetResourceAvailableTimeById";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Branchid", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Resourceid", DbType.Int32, _resourceId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var resAvailableTime = new AvailableResourcesDC();
                    resAvailableTime.ResourID = Convert.ToInt32(reader["ResourceId"]);
                    resAvailableTime.ResourName = Convert.ToString(reader["ResourceName"]);
                    resAvailableTime.Day = Convert.ToString(reader["Day"]);
                    if (reader["FromTime"] != DBNull.Value)
                        resAvailableTime.FromTime = Convert.ToDateTime(reader["FromTime"]);
                    if (reader["ToTime"] != DBNull.Value)
                        resAvailableTime.ToTime = Convert.ToDateTime(reader["ToTime"]);

                        availableTimeResList.Add(resAvailableTime);
                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
            return availableTimeResList;
        }
    }
}
