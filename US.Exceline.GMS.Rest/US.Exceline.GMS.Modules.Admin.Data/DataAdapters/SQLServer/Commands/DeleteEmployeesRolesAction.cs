﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteEmployeesRolesAction : USDBActionBase<bool>
    {
        private EmployeeDC _employeeDc;
        public DeleteEmployeesRolesAction(EmployeeDC employeeDc)
        {
            _employeeDc = employeeDc;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSAdminDeleteRoles";
            DbTransaction transaction = null;
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _employeeDc.Id));
                cmd.ExecuteNonQuery();
                result = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public bool RunOnTransation(DbTransaction dbTransaction)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSAdminDeleteRoles";  
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Connection = dbTransaction.Connection;
                cmd.Transaction = dbTransaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _employeeDc.Id));
                cmd.ExecuteNonQuery();
                result = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
