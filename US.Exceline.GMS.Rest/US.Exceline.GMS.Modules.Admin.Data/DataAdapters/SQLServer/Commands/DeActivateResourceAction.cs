﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class DeActivateResourceAction : USDBActionBase<bool>
    {
        private int _resourceId;
        private string _user;

        public DeActivateResourceAction(int resourceId, string user)
        {
            _resourceId = resourceId;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            bool outputId = false;
            const string storedProcedureName = "USExceGMSAdminDeleteSchedulWithResourceDeActivation";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@resourceId", DbType.Int32, _resourceId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                DbParameter para1 = new SqlParameter();
                para1.DbType = DbType.Boolean;
                para1.ParameterName = "@outPutID";
                para1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para1);
                cmd.ExecuteNonQuery();
                outputId = Convert.ToBoolean(para1.Value);
            }
            catch
            {
                throw;
            }
            return outputId;
        }
    }
}
