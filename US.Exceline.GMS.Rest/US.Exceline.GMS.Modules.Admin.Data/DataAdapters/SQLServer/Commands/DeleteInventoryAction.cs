﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteInventoryAction : USDBActionBase<int>
   {
       private int _inventoryId;
       public DeleteInventoryAction(int inventoryId)
       {
           _inventoryId = inventoryId;
       }

       protected override int Body(DbConnection connection)
       {
           int result = -1;
           const string storedProcedureName = "USExceGMSAdminDeleteInventory";
           try
           {
               DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@InventoryId", DbType.Int32, _inventoryId));

               cmd.ExecuteNonQuery();
               result = 1;
           }
           catch (Exception ex)
           {
               throw ;
           }
           return result;
          
       }
   }
}
