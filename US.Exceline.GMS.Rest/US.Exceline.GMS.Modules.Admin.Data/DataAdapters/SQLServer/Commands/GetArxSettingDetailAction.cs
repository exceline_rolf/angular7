﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetArxSettingDetailAction : USDBActionBase<List<ExceArxFormatDetail>>
    {
        private readonly int _branchId;
        public GetArxSettingDetailAction(int branchId)
        {
            _branchId = branchId;
        }
      
        protected override List<ExceArxFormatDetail> Body(DbConnection connection)
        {
            var arxFormatDetails = new List<ExceArxFormatDetail>();
            const string storedProcedureName = "USExceGMSGetArxSettingDetail";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var arxFormat = new ExceArxFormatDetail();

                    arxFormat.Id = Convert.ToInt32(reader["Id"]);
                    arxFormat.FormatTypeId = Convert.ToInt32(reader["FormatTypeId"]);
                    if (reader["MinLength"] != DBNull.Value)
                    arxFormat.MinLength = Convert.ToInt32(reader["MinLength"]);
                    if (reader["MaxLength"] != DBNull.Value)
                    arxFormat.MaxLength = Convert.ToInt32(reader["MaxLength"]);
                   

                    arxFormatDetails.Add(arxFormat);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return arxFormatDetails;
        }
    }
}
