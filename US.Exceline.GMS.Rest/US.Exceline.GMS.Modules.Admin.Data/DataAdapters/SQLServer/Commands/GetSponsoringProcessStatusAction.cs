﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 20/10/2014
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetSponsoringProcessStatusAction : USDBActionBase<bool>
    {
        private string _guiId = string.Empty;
        private string _user = string.Empty;

        public GetSponsoringProcessStatusAction(string guID, string user)
        {
            _guiId = guID;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            bool outPut = false;
            string spName = "USExceGMSAdminGetSponsoringStatus";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@guiId", DbType.String, _guiId));

                DbParameter para1 = new SqlParameter();
                para1.DbType = DbType.Int32;
                para1.ParameterName = "@outPutID";
                para1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para1);
                cmd.ExecuteNonQuery();
                if ((Convert.ToInt32(para1.Value)) > 0)
                {
                    outPut = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return outPut;
        }
    }
}
