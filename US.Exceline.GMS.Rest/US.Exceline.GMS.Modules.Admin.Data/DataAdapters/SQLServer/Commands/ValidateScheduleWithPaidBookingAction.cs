﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
   public class ValidateScheduleWithPaidBookingAction : USDBActionBase<bool>
   {
       private readonly int _scheduleItemId;
       private readonly int _resourceId;
       public ValidateScheduleWithPaidBookingAction(int scheduleItemId,int resourceId)
       {
           _scheduleItemId = scheduleItemId;
           _resourceId = resourceId;
       }

        protected override bool Body(DbConnection connection)
        {
            bool result;
            string spName = "USExceGMSAdminValidateScheduleWithPaidBooking";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                if (_scheduleItemId > 0)
                command.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _scheduleItemId));
                if (_resourceId > 0)
                command.Parameters.Add(DataAcessUtils.CreateParam("@ResourceId", DbType.Int32, _resourceId));
              
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Boolean;
                output.ParameterName = "@OutPut";
                output.Direction = ParameterDirection.Output;
                command.Parameters.Add(output);
                command.ExecuteNonQuery();
                result = Convert.ToBoolean(output.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
