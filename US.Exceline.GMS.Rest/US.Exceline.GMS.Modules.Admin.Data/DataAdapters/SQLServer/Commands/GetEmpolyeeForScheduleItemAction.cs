﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetEmpolyeeForScheduleItemAction : USDBActionBase<List<GymEmployeeDC>>
    {
        private int _scheduleItemId;

         public GetEmpolyeeForScheduleItemAction(int scheduleItemId)
        {
            _scheduleItemId = scheduleItemId;
        }

         protected override List<GymEmployeeDC> Body(DbConnection connection)
         {
            var empList = new List<GymEmployeeDC>();
            const string storedProcedureName = "USExceGMSAdminGetEmployeesForScheduleItem";
            DbDataReader reader = null;
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleItemId", DbType.Int32, _scheduleItemId));
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    GymEmployeeDC employee = new GymEmployeeDC();
                    employee.Id = Convert.ToInt32(reader["EmployeeId"]);
                    employee.Name = reader["Name"].ToString();
                    employee.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    employee.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    employee.IsDefaultSchedule = Convert.ToBoolean(reader["IsDefault"]);

                    empList.Add(employee);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return empList;
        }
         
    }
}
