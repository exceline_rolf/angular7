﻿
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : Unicorngma
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Payments;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class AddUpdateVatCodesAction : USDBActionBase<int>
    {
        private readonly VatCodeDC _vatCodeDetail;
        private readonly string _user;

        public AddUpdateVatCodesAction(VatCodeDC vatCodeDetail, String user)
        {
            _vatCodeDetail = vatCodeDetail;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            int outputId = -1;
            const string storedProcedureName = "USExceGMSAdminAddEditVatAccounts";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _vatCodeDetail.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VatCode", DbType.Int32, _vatCodeDetail.VatCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VersionId ", DbType.String, _vatCodeDetail.VersionId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _vatCodeDetail.Name));
                if (_vatCodeDetail.VatAccount != null && _vatCodeDetail.VatAccount.ID != 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccountId ", DbType.String, _vatCodeDetail.VatAccount.ID));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _vatCodeDetail.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _vatCodeDetail.StartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _vatCodeDetail.EndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Rate", DbType.Decimal, _vatCodeDetail.Rate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActive", DbType.Boolean, _vatCodeDetail.IsActive));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                DbParameter para1 = new SqlParameter();
                para1.DbType = DbType.Int32;
                para1.ParameterName = "@outPutID";
                para1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para1);
                cmd.ExecuteNonQuery();
                outputId = Convert.ToInt32(para1.Value);
            }
            catch
            {
                throw;
            }
            return outputId;
        }
    }
}
