﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetEmpolyeeForResourceAction : USDBActionBase<Dictionary<int,string>>
    {
        private int _resourceId;

        public GetEmpolyeeForResourceAction(int resourceId)
        {
            _resourceId = resourceId;
        }

        protected override Dictionary<int, string> Body(DbConnection connection)
        {
            var empList = new Dictionary<int, string>();
            const string storedProcedureName = "USExceGMSAdminGetEmployeesForResource";
            DbDataReader reader = null;
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ResourceId", DbType.Int32, _resourceId));
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int id = Convert.ToInt32(reader["EmployeeId"]);
                    string name = reader["EmpName"].ToString();

                    empList.Add(id, name);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return empList;
        }
    }
}
