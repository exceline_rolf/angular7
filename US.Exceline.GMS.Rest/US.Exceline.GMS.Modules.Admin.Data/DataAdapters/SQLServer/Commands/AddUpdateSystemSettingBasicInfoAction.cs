﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer
{
    class AddUpdateSystemSettingBasicInfoAction : USDBActionBase<int>
    {
        private SystemSettingBasicInfoDC _systemSettingBasicInfo;
        private string _user;

        public AddUpdateSystemSettingBasicInfoAction(SystemSettingBasicInfoDC SystemSettingBasicInfo, string user)
        {
            _systemSettingBasicInfo = SystemSettingBasicInfo;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            int outputId = -1;
            string StoredProcedureName = "USExceGMSAdminAddUpdateSystemSettingBasicInfo";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ID",                DbType.Int32, _systemSettingBasicInfo.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GymCompanyID",      DbType.Int32, _systemSettingBasicInfo.GymCompanyID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GymCompanyName",    DbType.String, _systemSettingBasicInfo.GymCompanyName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Address1",          DbType.String, _systemSettingBasicInfo.Address1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Address2",          DbType.String, _systemSettingBasicInfo.Address2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Address3",          DbType.String, _systemSettingBasicInfo.Address3));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PostalCode",        DbType.String, _systemSettingBasicInfo.PostalCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PostalPlace",       DbType.String, _systemSettingBasicInfo.PostalPlace));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContactPerson",     DbType.String, _systemSettingBasicInfo.ContactPerson));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Email",             DbType.String, _systemSettingBasicInfo.Email));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Mobile",            DbType.String, _systemSettingBasicInfo.Mobile));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccountNo",         DbType.String, _systemSettingBasicInfo.AccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherAdminMobile", DbType.String, _systemSettingBasicInfo.OtherAdminMobile));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherAdminEmail", DbType.String, _systemSettingBasicInfo.OtherAdminEmail));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User",              DbType.String, _user));
               
                DbParameter para1 = new SqlParameter();
                para1.DbType = DbType.Int32;
                para1.ParameterName = "@outPutID";
                para1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para1);
                cmd.ExecuteNonQuery();
                outputId = Convert.ToInt32(para1.Value);
            }
            catch
            {
                throw;
            }
            return outputId;
        }
    }
}
