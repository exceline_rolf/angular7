﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer
{
    class GetSystemSettingBasicInfoAction : USDBActionBase<SystemSettingBasicInfoDC>
    {
        public GetSystemSettingBasicInfoAction() { }
        
        protected override SystemSettingBasicInfoDC Body(DbConnection connection)
        {
            SystemSettingBasicInfoDC SystemSettingBasicInfo = new SystemSettingBasicInfoDC();
            try
            {
                const string storedProcedure = "USExceGMSAdminGetSystemSettingBasicInfo";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {

                    SystemSettingBasicInfo.Id = Convert.ToInt32(reader["ID"]);
                    SystemSettingBasicInfo.GymCompanyID = Convert.ToInt32(reader["GymCompanyID"]);
                    SystemSettingBasicInfo.GymCompanyName = Convert.ToString(reader["GymCompanyName"]);
                    SystemSettingBasicInfo.Address1 = Convert.ToString(reader["Address1"]);
                    SystemSettingBasicInfo.Address2 = Convert.ToString(reader["Address2"]);
                    SystemSettingBasicInfo.Address3 = Convert.ToString(reader["Address3"]);
                    SystemSettingBasicInfo.PostalCode = Convert.ToString(reader["PostalCode"]);
                    SystemSettingBasicInfo.PostalPlace = Convert.ToString(reader["PostalPlace"]);
                    SystemSettingBasicInfo.ContactPerson = Convert.ToString(reader["ContactPerson"]);
                    SystemSettingBasicInfo.Email = Convert.ToString(reader["Email"]);
                    SystemSettingBasicInfo.Mobile = Convert.ToString(reader["Mobile"]);
                    SystemSettingBasicInfo.AccountNo = Convert.ToString(reader["AccountNo"]);
                    SystemSettingBasicInfo.OtherAdminMobile = Convert.ToString(reader["OtherAdminMobile"]);
                    SystemSettingBasicInfo.OtherAdminEmail = Convert.ToString(reader["OtherAdminEmail"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return SystemSettingBasicInfo;
        }
    }
}
