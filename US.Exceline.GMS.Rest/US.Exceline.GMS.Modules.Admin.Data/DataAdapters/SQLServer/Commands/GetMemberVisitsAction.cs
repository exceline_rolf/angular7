﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "06/06/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberVisitsAction : USDBActionBase<List<EntityVisitDC>>
    {
        private DateTime _startDate;
        private DateTime _endDate;
        private int _branchId;

        public GetMemberVisitsAction(DateTime startDate, DateTime endDate, int branchId)
        {
            _startDate = startDate;
            _endDate = endDate;
            _branchId = branchId;
        }

        protected override List<EntityVisitDC> Body(DbConnection connection)
        {
            List<EntityVisitDC> timeEntryList = new List<EntityVisitDC>();
            string StoredProcedureName = "USExceGMSAdminGetMemberVisitsForAdminView";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    EntityVisitDC entityVisit = new EntityVisitDC();
                    entityVisit.Id = Convert.ToInt32(reader["ID"]);
                    if (reader["FirstName"] != DBNull.Value)
                    {
                        entityVisit.EntityName = reader["FirstName"].ToString(); ;
                    }
                    if (reader["LastName"] != DBNull.Value)
                    {
                        entityVisit.EntityName += " " + reader["LastName"].ToString();
                    }
                    entityVisit.InTime = Convert.ToDateTime(reader["InTime"]);
                    entityVisit.VisitDate = Convert.ToDateTime(reader["VisitDate"]);
                    entityVisit.CutomerNo = Convert.ToString(reader["CustId"]);
                    entityVisit.BranchId = Convert.ToInt32(reader["BranchId"]);
                    // entityVisit.Role = Convert.ToString(reader["RoleId"]);
                    entityVisit.EntityId = Convert.ToInt32(reader["EntityId"]);
                    entityVisit.AccessControlType = Convert.ToString(reader["AccessControlType"]);
                    timeEntryList.Add(entityVisit);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return timeEntryList;

        }
    }
}
