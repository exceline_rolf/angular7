﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetEmployeeJobsAction : USDBActionBase<List<EmployeeJobDC>>     
    {
        private int _employeeId;

        public GetEmployeeJobsAction(int employeeId)
        {
            _employeeId = employeeId;
        }

        protected override List<EmployeeJobDC> Body(DbConnection connection)
        {
            List<EmployeeJobDC> employeeJobList = new List<EmployeeJobDC>();
            string StoredProcedureName = "USExceGMSAdminGetEmployeeJobs";
            try 
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeeId", DbType.Int32, _employeeId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    EmployeeJobDC employeeJob = new EmployeeJobDC();
                    employeeJob.Day  = Convert.ToString(reader["Day"]);
                    employeeJob.JobStartDate = Convert.ToDateTime(reader["JobStartDate"]);
                    employeeJob.JobEndDate = Convert.ToDateTime(reader["JobEndDate"]);
                    
                    employeeJob.Date  = Convert.ToDateTime(reader["StartDateTime"]);
                    employeeJob.StartTime = Convert.ToDateTime (reader["StartTime"]).TimeOfDay;
                    employeeJob.EndTime = Convert.ToDateTime(reader["EndTime"]).TimeOfDay;
                    employeeJob.JobName = Convert.ToString(reader["JobName"]);
                    employeeJob.JobCategoryName = Convert.ToString(reader["Name"]);
                    employeeJob.BranchId = Convert.ToInt32(reader["BranchId"]);
                    employeeJob.BranchName = Convert.ToString(reader["BranchName"]);
                    employeeJob.JobCategoryId = Convert.ToInt32(reader["JobCategoryId"]);  

                    employeeJobList.Add(employeeJob);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return employeeJobList;      
        }


    }
}
