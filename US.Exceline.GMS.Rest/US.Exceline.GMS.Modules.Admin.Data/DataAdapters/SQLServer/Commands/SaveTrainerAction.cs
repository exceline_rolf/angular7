﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveTrainerAction : USDBActionBase<string>
    {
        private TrainerDC _trainer;
        private DataTable _dataTable = null;
        public SaveTrainerAction(TrainerDC trainer)
        {
            _trainer = trainer;
            if (_trainer.ActivityList != null)
                _dataTable = GetActivityItemLst(_trainer.ActivityList);
        }

        private DataTable GetActivityItemLst(List<ActivityDC> activityList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ActivityId", Type.GetType("System.Int32")));
            foreach (ActivityDC activityItem in activityList)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["ActivityId"] = activityItem.Id;
                _dataTable.Rows.Add(_dataTableRow);
            }

            return _dataTable;
        }


        protected override string Body(DbConnection connection)
        {
            string trainerId = "-1";
            string StoredProcedureName = "USExceGMSAdminAddTrainer";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.String, _trainer.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@firstname", DbType.String, _trainer.FirstName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastname", DbType.String, _trainer.LastName));
                if (!_trainer.BirthDay.Equals(DateTime.MinValue))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@birthdate", DbType.DateTime, _trainer.BirthDay));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@personNo", DbType.String, _trainer.PersonNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@email", DbType.String, _trainer.Email));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@mobile", DbType.String, _trainer.Mobile));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@telework", DbType.String, _trainer.Work));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@telehome", DbType.String, _trainer.Private));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address1", DbType.String, _trainer.Address1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address2", DbType.String, _trainer.Address2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address3", DbType.String, _trainer.Address3));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@zip", DbType.String, _trainer.PostCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@postplace ", DbType.String, _trainer.PostPlace));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@role", DbType.String, _trainer.RoleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gender", DbType.String, _trainer.Gender));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@description", DbType.String, _trainer.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _trainer.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _trainer.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastModifiedUser", DbType.String, _trainer.LastModifiedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Boolean, _trainer.ActiveStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryId", DbType.Int32, _trainer.Category.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@assignedEntity", DbType.Int32, _trainer.AssignedEntityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, _trainer.CommentForInactive));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@imagepath", DbType.String, _trainer.ImagePath));
                if (_trainer.ActivityList != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityItemList", SqlDbType.Structured, _dataTable));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityItemList", SqlDbType.Structured, null));
                }
                DbParameter para3 = new SqlParameter();
                para3.DbType = DbType.String;
                para3.ParameterName = "@outId";
                para3.Size = 50;
                para3.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para3);
                cmd.ExecuteNonQuery();
                trainerId = Convert.ToString(para3.Value);
            }

            catch (Exception ex)
            {
                trainerId = "-1";
                throw ex;
            }
            return trainerId;
        }
    }
}
