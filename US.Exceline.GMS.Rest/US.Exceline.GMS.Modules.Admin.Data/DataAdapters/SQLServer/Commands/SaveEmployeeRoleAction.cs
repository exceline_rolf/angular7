﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveEmployeeRoleAction : USDBActionBase<int>
    {
        private readonly ExcelineRoleDc _excelineRoleDc;
        private readonly int _empId;
        private readonly string _user = string.Empty;
        public  SaveEmployeeRoleAction(ExcelineRoleDc excelineRole,int empId,string user)
        {
            _excelineRoleDc = excelineRole;
            _empId = empId;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            return 1;
        }

        public int SaveEmployeeRole(DbTransaction transaction)
        {
            const string spName = "USExceGMSAdminSaveEmployeeRole";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeId", DbType.Int32, _empId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@RoleId", DbType.Int32, _excelineRoleDc.RoleId));
                //command.Parameters.Add(DataAcessUtils.CreateParam("@RoleId", DbType.Boolean, _excelineRoleDc.RoleId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsActive", DbType.Boolean, _excelineRoleDc.IsActive));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                if (_excelineRoleDc.BranchList != null)
                {
                    command.Parameters.Add(DataAcessUtils.CreateParam("@BranchList", SqlDbType.Structured, GetDataTable(_excelineRoleDc.BranchList)));
                }
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 1;
        }

        private DataTable GetDataTable(IEnumerable<int> branchList)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));

            foreach (int i in branchList)
            {
                var row = dataTable.NewRow();
                row["ID"] = i;
                dataTable.Rows.Add(row);
            }
            return dataTable;
        }

       
    }
}
