﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.SystemObjects;
using System.Data.Common;
using System.Data;
using US.Payment.Core.ResultNotifications;
using US.Common.Logging.API;
using US.Payment.Modules.Economy.API.Economy;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GenerateInvoiceAction : USDBActionBase<List<ExcelineInvoiceDetailDC>>
    {
        public List<ExcelineInvoiceDetailDC> _invoiceDetails = new List<ExcelineInvoiceDetailDC>();
        private bool _isMemberRecords = true;
        private string _user = string.Empty;
        private bool _isGenereteNow = false;
        private string _gymCode = string.Empty;
        public GenerateInvoiceAction(List<ExcelineInvoiceDetailDC> invoiceDetails, bool isMemberRecords, string user, bool isGenereteNow,string gymCode)
        {
            _invoiceDetails = invoiceDetails;
            _isMemberRecords = isMemberRecords;
            _user = user;
            _isGenereteNow = isGenereteNow;
            _gymCode=gymCode;
        }

        protected override List<ExcelineInvoiceDetailDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ExcelineInvoiceDetailDC> errorList = new List<ExcelineInvoiceDetailDC>();
            string spName = "USExceGMSScheduleForGeneratingInvoicesManually";
           // DbTransaction transaction = null;
            try
            {
                //transaction = connection.BeginTransaction();
                int creaditorNO = Convert.ToInt32(_invoiceDetails[0].CreditorNo);
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
               // cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentIDs", SqlDbType.Structured, GetDataTable()));
                cmd.ExecuteNonQuery();

                if (_isGenereteNow)
                {
                    USImportResult invoiceResult = Invoice.GenerateInvoices(creaditorNO, string.Empty, "E", _gymCode);
                    if (invoiceResult.ErrorOccured)
                    {
                        // transaction.Rollback();
                        foreach (USNotificationMessage message in invoiceResult.NotificationMessages)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), _user);
                        }
                    }
                    else
                    {//
                        //transaction.Commit();
                    }
                       
                }
            }catch(Exception ex)
            {
                throw ex;
            }
            return errorList;
        }

        private DataTable GetDataTable()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("MemberContractID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Amount", typeof(decimal)));
            dataTable.Columns.Add(new DataColumn("SponsorAmount", typeof(decimal)));
            dataTable.Columns.Add(new DataColumn("Discount", typeof(decimal)));
            dataTable.Columns.Add(new DataColumn("InstallmentNo", typeof(int)));
            dataTable.Columns.Add(new DataColumn("CorelationID", typeof(string)));
            dataTable.Columns.Add(new DataColumn("NoOfVisits", typeof(int)));
            dataTable.Columns.Add(new DataColumn("InstallmentDate", typeof(DateTime)));


            int index = 1;
            foreach (ExcelineInvoiceDetailDC invoiceDetailView in _invoiceDetails)
            {
                DataRow dataRow = dataTable.NewRow();
                dataRow["ID"] = invoiceDetailView.InstallmentId;
                dataRow["CorelationID"] = string.Empty;
                dataRow["MemberContractID"] = invoiceDetailView.MemberContractId;
                dataRow["Amount"] = invoiceDetailView.Amount;
                dataRow["Discount"] = invoiceDetailView.Discount;
                dataRow["SponsorAmount"] = invoiceDetailView.SponsoredAmount;
                dataRow["InstallmentNo"] = invoiceDetailView.InstallmentNo;
                dataRow["NoOfVisits"] = invoiceDetailView.NoOfVisits;
                dataRow["InstallmentDate"] = invoiceDetailView.InstallmentDate;
                dataTable.Rows.Add(dataRow);
                index++;
            }
            return dataTable;
        }
    }
}
