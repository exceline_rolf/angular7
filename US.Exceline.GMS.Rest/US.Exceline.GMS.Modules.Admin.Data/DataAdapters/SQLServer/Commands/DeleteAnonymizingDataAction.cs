﻿using System;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageAnonymizing;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteAnonymizingDataAction : USDBActionBase<int>
    {
        private readonly ExceAnonymizingDC _anonymizing = new ExceAnonymizingDC();
        private readonly int _branchId = 1;
        private readonly string _user = string.Empty;

        public DeleteAnonymizingDataAction(ExceAnonymizingDC anonymizing,string user, int branchId)
        {
            _branchId = branchId;
            _anonymizing = anonymizing;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            int result = -1;
            string spName = "USExceGMSAdminDeleteAnonymizingData";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Code", DbType.String, _anonymizing.Code));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                command.ExecuteNonQuery();
                result = 1;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
