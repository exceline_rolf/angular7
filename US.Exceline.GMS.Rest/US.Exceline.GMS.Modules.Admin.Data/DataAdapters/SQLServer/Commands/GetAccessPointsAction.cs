﻿// --------------------------------------------------------------------------
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : Martin Tømmerås
// Created Timestamp : "21.11.2018 15:29 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetAccessPointsAction : USDBActionBase<List<string>>
    {
        public GetAccessPointsAction()
        {

        }

        protected override List<string> Body(DbConnection connection)
        {
            List<string> accessPoints = new List<string>();
            string StoredProcedureName = "USExceGMSAdminGetMemberVisitsAccessPoints";
            try
            {
                string acpoint = string.Empty;
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    acpoint = Convert.ToString(reader["AccessControlType"]);
                    accessPoints.Add(acpoint);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return accessPoints;

        }
    }
}
