﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetInventoryItemsAction : USDBActionBase<List<ArticleDC>>
    {
        private int _branchId;

        public GetInventoryItemsAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<ArticleDC> Body(DbConnection connection)
        {
            List<ArticleDC> getInventoryList = new List<ArticleDC>();
            string storedProcedureName = "USExceGMSShopGetInventories";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ArticleDC inventory = new ArticleDC();
                    inventory.Id = Convert.ToInt32(reader["Id"].ToString());
                    inventory.Code = reader["ItemCode"].ToString();
                    inventory.Description = reader["Name"].ToString();
                    inventory.ActiveStatus = Convert.ToBoolean(reader["IsActive"]);
                    inventory.CategoryId = Convert.ToInt32(reader["CategoryId"].ToString());
                    inventory.Category = reader["CategoryName"].ToString();
                    inventory.DefaultPrice = Convert.ToDecimal(reader["CustomerPrice"].ToString());
                    inventory.EmployeePrice = Convert.ToDecimal(reader["EmployeePrice"].ToString());
                    inventory.Quantity = Convert.ToInt32(reader["Qty"].ToString());
                    inventory.ReOrderLevel = Convert.ToInt32(reader["ReOrderLevel"].ToString());
                    inventory.NotifyAlert = Convert.ToBoolean(reader["NotifyAlert"].ToString());
                    inventory.Discount = Convert.ToDecimal(reader["Discount"]);

                    getInventoryList.Add(inventory);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return getInventoryList;
        }
    }
}
