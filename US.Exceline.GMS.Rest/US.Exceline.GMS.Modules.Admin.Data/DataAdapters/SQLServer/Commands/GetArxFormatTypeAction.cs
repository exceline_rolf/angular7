﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetArxFormatTypeAction : USDBActionBase<List<ExceArxFormatType>>
    {
        protected override List<ExceArxFormatType> Body(DbConnection connection)
        {
            var arxFormateTypes = new List<ExceArxFormatType>();
            const string storedProcedureName = "USExceGMSGetArxFormatType";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var arxFormatType = new ExceArxFormatType
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            Name = Convert.ToString(reader["Name"])
                        };

                    arxFormateTypes.Add(arxFormatType);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return arxFormateTypes;
        }
    }
}
