﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetInventoryDetailAction : USDBActionBase<List<ArticleDC>>
    {
        private readonly int _branchId;
        private readonly int _inventoryId;

        public GetInventoryDetailAction(int branchId, int inventoryId)
        {
            _branchId = branchId;
            _inventoryId = inventoryId;
        }

        protected override List<ArticleDC> Body(DbConnection connection)
        {
            const string storedProcedure = "USExceGMSAdminGetInventoryDetail";
            List<ArticleDC> articleList = new List<ArticleDC>();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InventoryId", DbType.Int32, _inventoryId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ArticleDC article = new ArticleDC();
                    article.Id = Convert.ToInt32(reader["ID"]);
                    article.ArticleNo = reader["ArticleNo"].ToString();
                    article.Description = reader["Description"].ToString();
                    article.BarCode = reader["BarCode"].ToString();
                    article.StockLevel = Convert.ToInt32(reader["StockLevel"]);
                    article.Counted = Convert.ToInt32(reader["Counted"]);
                    article.PurchasedPrice = Convert.ToDecimal(reader["PurchasedPrice"]);
                    article.Deviation = Convert.ToInt32(reader["Deviation"]);
                    article.NetValue = Convert.ToDecimal(reader["NetValue"]);
                    if (reader["CountedValue"] != DBNull.Value)
                    article.CountedValue = Convert.ToDecimal(reader["CountedValue"]);
                    article.InventoryDetailId = Convert.ToInt32(reader["InventoryDetailId"]);

                    articleList.Add(article);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return articleList;
        }
    }
}
