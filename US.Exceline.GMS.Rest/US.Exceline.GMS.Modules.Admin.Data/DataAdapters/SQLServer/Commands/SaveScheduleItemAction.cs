﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
   public class SaveScheduleItemAction : USDBActionBase<int>
    {
       private DataTable _dataTableEntity;
       //private DataTable _dtActiveTimes;
       private DataTable _scheduleItemDays;
       private ScheduleItemDC _scheduleItem;
       private int _resId;
       private int _branchId;
      // private string _user = string.Empty;
       public SaveScheduleItemAction(ScheduleItemDC scheduleItem, int resId, int branchId)
       {
           _resId = resId;
           _branchId = branchId;
           _scheduleItem = scheduleItem;
           _dataTableEntity = GetEntityList(scheduleItem.InstructorIdList, scheduleItem.ResourceId); 
           //if (scheduleItem.ActiveTimes != null)
           //{
           //    _dtActiveTimes = GetActiveTimesDataTable(scheduleItem.ActiveTimes.ToList());
           //}

           if (scheduleItem.DayList != null && scheduleItem.DayList.Count > 0)
           {
               _scheduleItemDays = GetDayLst(scheduleItem.DayList);
           } 
       }

       private DataTable GetDayLst(IEnumerable<string> dayList)
       {
           _scheduleItemDays = new DataTable();
           _scheduleItemDays.Columns.Add(new DataColumn("Id", typeof(int)));
           _scheduleItemDays.Columns.Add(new DataColumn("Day", typeof(string)));
           int temp = 0;
           foreach (var day in dayList)
           {
               temp++;
               DataRow dataTableRow = _scheduleItemDays.NewRow();
               dataTableRow["Id"] = temp;
               dataTableRow["Day"] = day.Trim();
               _scheduleItemDays.Rows.Add(dataTableRow);
           }

           return _scheduleItemDays;
       }

        private DataTable GetEntityList(IEnumerable<int> empList,int resourceId)
        {
            _dataTableEntity = new DataTable();
            _dataTableEntity.Columns.Add(new DataColumn("ScheduleItemId", typeof(int)));
            _dataTableEntity.Columns.Add(new DataColumn("EntityId", typeof(int)));
            _dataTableEntity.Columns.Add(new DataColumn("EntityRoleType", typeof(string)));

            if (empList != null)
            {
                var enumerable = empList as int[] ?? empList.ToArray();
                if (enumerable.Any())
                {
                    foreach (int id in enumerable)
                    {
                        DataRow dataTableRow = _dataTableEntity.NewRow();
                        dataTableRow["ScheduleItemId"] = _scheduleItem.Id;
                        dataTableRow["EntityId"] = id;
                        dataTableRow["EntityRoleType"] = "EMP";
                        _dataTableEntity.Rows.Add(dataTableRow);
                    }
                }
            }

            if (resourceId > 0)
            {

                DataRow dataTableRow = _dataTableEntity.NewRow();
                dataTableRow["ScheduleItemId"] = _scheduleItem.Id;
                dataTableRow["EntityId"] = resourceId;
                dataTableRow["EntityRoleType"] = "RES";
                _dataTableEntity.Rows.Add(dataTableRow);

            }

            return _dataTableEntity;
        }

        //private DataTable GetActiveTimesDataTable(List<EntityActiveTimeDC> activeTimes)
        //{
        //    _dtActiveTimes = new DataTable();
        //    _dtActiveTimes.Columns.Add(new DataColumn("ScheduleItemId", typeof(int)));
        //    _dtActiveTimes.Columns.Add(new DataColumn("StartDateTime", typeof(DateTime)));  
        //    _dtActiveTimes.Columns.Add(new DataColumn("EndDateTime", typeof(DateTime)));

        //    if (_scheduleItem.ActiveTimes != null && _scheduleItem.ActiveTimes.Any())
        //    {
        //        foreach (EntityActiveTimeDC activeTime in _scheduleItem.ActiveTimes)
        //        {
        //            DataRow dr = _dtActiveTimes.NewRow();
        //            dr["ScheduleItemId"] = _scheduleItem.Id;
        //            dr["StartDateTime"] = activeTime.StartDateTime;
        //            dr["EndDateTime"] = activeTime.EndDateTime;
        //            _dtActiveTimes.Rows.Add(dr);
        //        }
        //    }
        //    return _dtActiveTimes;
        //}

        protected override int Body(DbConnection connection)
        {
            int scheduleItemId;
            const string storedProcedureName = "USExceGMSAdminResourcesSaveScheduleItem";
            try
            {

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                if (_resId > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _resId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _scheduleItem.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", DbType.Int32, _scheduleItem.ScheduleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Occurrence", DbType.String, _scheduleItem.Occurrence));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Day", DbType.String, _scheduleItem.Day));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Week", DbType.Int32, _scheduleItem.Week));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Month", DbType.String, _scheduleItem.Month));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Year", DbType.Int32, _scheduleItem.Year));
                if (_scheduleItem.StartTime != DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", DbType.DateTime, _scheduleItem.StartTime));
                if (_scheduleItem.EndTime != DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", DbType.DateTime, _scheduleItem.EndTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _scheduleItem.StartDate));
                if (_scheduleItem.EndDate == DateTime.MinValue)
                    _scheduleItem.EndDate = DateTime.MaxValue.Date;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _scheduleItem.EndDate));
                if (_scheduleItem.Id < 1) //for the create date
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedDateTime", DbType.DateTime, DateTime.Now));  
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedDateTime", DbType.DateTime, DateTime.Now));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _scheduleItem.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedUser", DbType.String, _scheduleItem.LastModifiedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus", DbType.Boolean, true));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsFixed", DbType.Boolean, false));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@WeekType", DbType.Int32, _scheduleItem.WeekType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MaxNoOfBooking", DbType.Int32, _scheduleItem.MaxNoOfBooking));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@HasBooking", DbType.Boolean, _scheduleItem.IsBooking));
                if (_scheduleItem.UpdateStatus > 0) //when UpdateStatus = 1(UpdatedBeforeScheduledItemStarted) UpdateStatus = 2(UpdatedAfterScheduledItemStarted)UpdateStatus = 3(EndDateChange)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@UpdateStatus", DbType.Int32, _scheduleItem.UpdateStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeId", DbType.Int32, _scheduleItem.EmpId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ResourceId", DbType.Int32, _scheduleItem.ResourceId));
                // cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsUpdatedAfterClassStarted", DbType.Boolean, _scheduleItem.IsUpdatedAfterClassStarted));
                // cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsOnlyEndDateUpdated", DbType.Boolean, _scheduleItem.IsOnlyEndDateUpdated));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityList", SqlDbType.Structured, _dataTableEntity));
               // cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimesList", SqlDbType.Structured, _dtActiveTimes));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemDays", SqlDbType.Structured, _scheduleItemDays));
              //  cmd.Parameters.Add(DataAcessUtils.CreateParam("@OutPutID", DbType.Int32, _scheduleItem.Id));


                SqlParameter param = new SqlParameter();
                param.ParameterName = "@OutPutID";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
                scheduleItemId = Convert.ToInt32(param.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return scheduleItemId;
        }
    }
}
