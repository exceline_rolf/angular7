﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteScheduleItemAction:USDBActionBase<int>
    {
        private int _scheduleItemId = -1;

        public DeleteScheduleItemAction(int scheduleItemId)
        {
            _scheduleItemId = scheduleItemId;
        }

        protected override int Body(DbConnection connection)
        {
            int status = -1;
            string StoredProcedureName = "USExceGMSJobDeleteScheduleItem";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleItemId", DbType.Int32, _scheduleItemId));
                cmd.ExecuteNonQuery();
                status = 1;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }
    }
}
