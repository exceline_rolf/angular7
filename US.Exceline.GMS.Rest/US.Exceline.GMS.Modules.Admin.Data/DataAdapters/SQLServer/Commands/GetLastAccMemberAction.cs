﻿using System;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetLastAccMemberAction : USDBActionBase<ExceACCMember>
    {
        private readonly int _branchId = 1;
        private readonly int _accTerminalId = -1;

        public GetLastAccMemberAction(int branchId, int accTerminalId)
        {
            _branchId = branchId;
            _accTerminalId = accTerminalId;
        }
        protected override ExceACCMember Body(DbConnection connection)
        {
            var accMember = new ExceACCMember();
            const string storedProcedure = "USExceGMSAdminGetLastAccMember";
            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@accTerminalId", DbType.Int32, _accTerminalId));
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    accMember.Id = Convert.ToInt32(reader["Id"]);
                    accMember.CustId = Convert.ToString(reader["CustId"]);
                    accMember.Name = Convert.ToString(reader["Name"]);
                    accMember.MemberCardNo = Convert.ToString(reader["MemberCardNo"]);
                    accMember.Gender = Convert.ToString(reader["Gender"]);
                    accMember.TemplateName = Convert.ToString(reader["TemplateName"]);
                    accMember.Born = Convert.ToString(reader["Born"]);
                    accMember.LastVisitDate = Convert.ToString(reader["LastVisitDate"]);
                    accMember.HomeGym = Convert.ToString(reader["HomeGym"]);
                    accMember.CreditBalance = Convert.ToDecimal(reader["Balance"]);
                    accMember.ContractEndDate = Convert.ToString(reader["ContractEndDate"]);
                    accMember.Messages.Add(Convert.ToString(reader["StatusMessage"]));
                    accMember.IsError = Convert.ToBoolean(reader["IsError"]);
                    accMember.SystemId = Convert.ToInt32(reader["SystemId"]);

                    accMember.IsFreezed = Convert.ToString(reader["IsFreezed"]);
                    accMember.IsContractATG = Convert.ToString(reader["IsContractATG"]);
                    accMember.MemberATGStatus = Convert.ToString(reader["MemberATGStatus"]);
                    if (reader["AntiDopingSignedDate"] != DBNull.Value)
                        accMember.AntiDopingDate = Convert.ToDateTime(reader["AntiDopingSignedDate"]).ToString("yyyy.MM.dd");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return accMember;
        }
    }
}
