﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetHolidayCategoryIdAction : USDBActionBase<int>
    {
        private int _categoryId;
        private string _categoryName;

        public GetHolidayCategoryIdAction(string categoryName)
        {
            this._categoryName = categoryName;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string storedProcedure = "USExceGMSAdminGetHolidayTypeCategoryId";
            _categoryId = -1;
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryName", System.Data.DbType.String, _categoryName));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    _categoryId = Convert.ToInt32(reader["HolidayTypeId"].ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _categoryId;
        }
    }
}
