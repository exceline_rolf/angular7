﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class SaveResourceActiveTimeRepeatableAction : USDBActionBase<int>
    {
        private List<EntityActiveTimeDC> _activeTimeList;
        private DataTable returnTable;
        private DataTable memberTable;
        private DataTable resourceTable;
        private readonly string _user = string.Empty;

        public SaveResourceActiveTimeRepeatableAction(List<EntityActiveTimeDC> activeTimeList, string user)
        {
            _activeTimeList = activeTimeList;
            _user = user;
        }

        private DataTable generateDataTable()
        {
            DataTable _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ID", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("SheduleItemId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("ActiveTimeId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("StartTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("EndTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("Comment", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("ArticleId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("SMSRemindered", typeof(Boolean)));
            _dataTable.Columns.Add(new DataColumn("ArrivalDateTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("IsPaid", typeof(Boolean)));
            _dataTable.Columns.Add(new DataColumn("aRItemNo", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("amount", typeof(decimal)));
            _dataTable.Columns.Add(new DataColumn("IsDeleted", typeof(Boolean)));
            _dataTable.Columns.Add(new DataColumn("user", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("task", typeof(Boolean)));
            _dataTable.Columns.Add(new DataColumn("IsExtraResChange", typeof(Boolean)));
            _dataTable.Columns.Add(new DataColumn("BookingCategoryId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("RoleType", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("branchId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("EntityId", typeof(Int32)));



            return _dataTable;
        }

        private DataTable generateMemberTable()
        {
            DataTable _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ActiveTimeId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("EntityId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("RoleId", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("ContractId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("Deleted", typeof(Boolean)));
            _dataTable.Columns.Add(new DataColumn("CreditBack", typeof(Boolean)));
            return _dataTable;
        }

        private DataTable generateResourceTable()
        {
            DataTable _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ActiveTimeId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("EntityId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("RoleId", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("ContractId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("Deleted", typeof(Boolean)));
            _dataTable.Columns.Add(new DataColumn("CreditBack", typeof(Boolean)));
            return _dataTable;
        }

        protected override int Body(DbConnection connection)
        {
            returnTable = new DataTable();
            memberTable = new DataTable();
            returnTable = generateDataTable();
            memberTable = generateMemberTable();
            resourceTable = generateResourceTable();

            string spName = "USExceGMSAdminSaveResourceScheduleItemActiveTimeRepeatable";

            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                int id = 0;

                foreach (var item in _activeTimeList)
                {
                    id = id + 1;
                    DataRow dataTableRow = returnTable.NewRow();
                    dataTableRow["ID"] = id;
                    dataTableRow["SheduleItemId"] = DBNull.Value;
                    dataTableRow["ActiveTimeId"] = item.Id;
                    dataTableRow["StartTime"] = item.StartDateTime;
                    dataTableRow["EndTime"] = item.EndDateTime;
                    dataTableRow["Comment"] = item.Comment;
                    dataTableRow["ArticleId"] = item.ArticleId;
                    dataTableRow["SMSRemindered"] = item.IsSmsReminder;
                    dataTableRow["ArrivalDateTime"] = DBNull.Value;
                    dataTableRow["IsPaid"] = item.IsPaid;
                    dataTableRow["aRItemNo"] = item.ArItemNo;
                    dataTableRow["amount"] = item.TotalPaid;
                    dataTableRow["IsDeleted"] = item.IsDeleted;
                    dataTableRow["user"] = _user;
                    dataTableRow["task"] = item.IsTask;
                    dataTableRow["IsExtraResChange"] = item.IsExtraResChange;
                    dataTableRow["BookingCategoryId"] = item.BookingCategoryId;
                    dataTableRow["RoleType"] = item.RoleType.ToUpper().Trim();
                    dataTableRow["branchId"] = item.BranchId;
                    dataTableRow["EntityId"] = item.EntityId;
                    returnTable.Rows.Add(dataTableRow);

                    if (item.BookingMemberList != null && item.BookingMemberList.Any() && memberTable.Rows.Count == 0)
                        foreach (var mem in item.BookingMemberList)
                        {
                            DataRow dataTableRow2 = memberTable.NewRow();
                            dataTableRow2["ActiveTimeId"] = DBNull.Value;
                            dataTableRow2["EntityId"] = mem.Id;
                            dataTableRow2["RoleId"] = "MEM";
                            dataTableRow2["ContractId"] = DBNull.Value;
                            dataTableRow2["Deleted"] = mem.IsDeleted;
                            dataTableRow2["CreditBack"] = mem.IsBookingCreditback;
                            memberTable.Rows.Add(dataTableRow2);
                        }

                    if (item.BookingResourceList != null && item.BookingResourceList.Any() && resourceTable.Rows.Count == 0)
                        foreach (var book in item.BookingResourceList)
                        {
                            DataRow dataTableRow3 = resourceTable.NewRow();
                            dataTableRow3["ActiveTimeId"] = DBNull.Value;
                            dataTableRow3["EntityId"] = book.Id;
                            dataTableRow3["RoleId"] = "RES";
                            dataTableRow3["ContractId"] = DBNull.Value;
                            dataTableRow3["Deleted"] = book.IsDeleted;
                            dataTableRow3["CreditBack"] = book.IsBookingCreditback;
                            resourceTable.Rows.Add(dataTableRow3);
                            memberTable.Rows.Add(dataTableRow3);
                        }
                }

                command.Parameters.Add(returnTable != null ? DataAcessUtils.CreateParam("@ActiveItemList", SqlDbType.Structured, returnTable)
                           : DataAcessUtils.CreateParam("@ActiveItemList", SqlDbType.Structured, null));

                command.Parameters.Add(memberTable != null ? DataAcessUtils.CreateParam("@MemberAndResourceList", SqlDbType.Structured, memberTable)
                           : DataAcessUtils.CreateParam("@MemberAndResourceList", SqlDbType.Structured, null));

                command.ExecuteNonQuery();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return 1;
        }
    }
}
