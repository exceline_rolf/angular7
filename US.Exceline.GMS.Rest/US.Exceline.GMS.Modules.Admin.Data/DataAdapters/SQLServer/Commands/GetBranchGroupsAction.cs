﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetBranchGroupsAction : USDBActionBase<List<ExcelineCreditorGroupDC>>
    {
        public GetBranchGroupsAction()
        {
        }

        protected override List<ExcelineCreditorGroupDC> Body(DbConnection connection)
        {
            List<ExcelineCreditorGroupDC> branchGroups = new List<ExcelineCreditorGroupDC>();
            string spName = "USExceGMSAdminGetBranchGroups";
            DbDataReader reader = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    ExcelineCreditorGroupDC item = new ExcelineCreditorGroupDC();
                    item.Id = Convert.ToInt32(reader["ID"]);
                    item.GroupName = reader["GroupName"].ToString();
                    item.DisplayName = reader["DisplayName"].ToString();
                    item.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    branchGroups.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            return branchGroups;
        }
    }
}
