﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "4/21/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteContractAction : USDBActionBase<int>
    {
        int _contractId;

        public DeleteContractAction(int contractId)
        {
            _contractId = contractId;
        }


        protected override int Body(DbConnection connection)
        {
            int result = -1;
            string StoredProcedureName = "USExceGMSAdminDeleteContract";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractId", DbType.String, _contractId));
                DbParameter outputPara = new SqlParameter();
                outputPara.DbType = DbType.Int32;
                outputPara.ParameterName = "@OutPutId";
                outputPara.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputPara);
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(outputPara.Value);
            }
            catch (Exception ex)
            {
                result = -1;
                throw ex;
            }
            return result;
        }
    }
}
