﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetActivityUnavailableTimeAction : USDBActionBase<List<ActivityTimeDC>>
    {
        public int _branchId = -1;
        private int _activityId = -1;
        private bool _isFromAceedControl = false;
        public GetActivityUnavailableTimeAction(int branchId, int activityId)
        {
            _branchId = branchId;
            _activityId = activityId;
        }
        protected override List<ActivityTimeDC> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSAdmingetActivityUnavailableTime";
            List<ActivityTimeDC> acvitiyTimes = new List<ActivityTimeDC>();
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                if(_activityId > 0)
                    command.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", System.Data.DbType.Int32, _activityId));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ActivityTimeDC activityTime = new ActivityTimeDC();
                    activityTime.Id = Convert.ToInt32(reader["Id"]);
                    activityTime.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    activityTime.ActivityName = Convert.ToString(reader["ActivityName"]);
                    activityTime.Startdate = Convert.ToDateTime(reader["StartDate"]);
                    activityTime.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    if(reader["StartTime"] != DBNull.Value)
                      activityTime.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    if(reader["EndTime"] != DBNull.Value)
                      activityTime.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    if (reader["Reason"] != DBNull.Value)
                        activityTime.Reason = Convert.ToString(reader["Reason"]);
                    activityTime.BranchId = Convert.ToInt32(reader["BranchId"]);
                    acvitiyTimes.Add(activityTime);
                }
                return acvitiyTimes;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
