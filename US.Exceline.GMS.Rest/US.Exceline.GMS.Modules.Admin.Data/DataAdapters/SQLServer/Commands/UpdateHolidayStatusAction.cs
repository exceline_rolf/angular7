﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Admin.HolydayManagement;
using System.Data.Common;
using US_DataAccess;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateHolidayStatusAction : USDBActionBase<bool>
    {
        private bool _activeStatus;
        private int _id;
        private DateTime _startDate;
        private DateTime _endDate;
        private string _day;
        private string _name;
        private int _year;
        private int _branchId;
        private string _holidayType;
        private List<DateTime> _datesList = new List<DateTime>();
        private DataTable _dataTable = null;

        public UpdateHolidayStatusAction(HolidayScheduleDC updateHoliday)
        {
            this._activeStatus = updateHoliday.ActiveStatus;
            this._id = updateHoliday.Id;
            this._startDate = updateHoliday.StartDate;
            this._endDate = updateHoliday.EndDate;
            this._day = updateHoliday.Day;
            this._name = updateHoliday.Name;
            this._year = updateHoliday.Year;
            this._branchId = updateHoliday.BranchId;
            this._holidayType = updateHoliday.HolidayCategoryName;
            this._datesList = updateHoliday.DatesList;

            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("Holiday", Type.GetType("System.DateTime")));

            foreach (DateTime date in _datesList)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["Holiday"] = date.Date;
                _dataTable.Rows.Add(_dataTableRow);
            }
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string storedProcedure = "USExceGMSAdminUpdateHolidayStatus";
            bool returnValue = false;

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ID", System.Data.DbType.Int32, _id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Year", System.Data.DbType.Int32, _year));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@HolidayType", System.Data.DbType.String, _holidayType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", System.Data.DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", System.Data.DbType.DateTime, _endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Day", System.Data.DbType.String, _day));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", System.Data.DbType.String, _name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus", System.Data.DbType.Boolean, _activeStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DatesList", SqlDbType.Structured, _dataTable));

                cmd.ExecuteNonQuery();

                returnValue = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return returnValue;
        }
    }
}
