﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class AddEmployeeTimeEntryAction : USDBActionBase<bool>
    {
        private EmployeeTimeEntryDC _empTimeEntry;

        public AddEmployeeTimeEntryAction(EmployeeTimeEntryDC empTimeEntry)
        {
            _empTimeEntry = empTimeEntry;
        }

        protected override bool Body(DbConnection connection)
        {
            bool isSuccess = false;
 	        string sp = "USExceGMSAdminAddEmployeeTimeEntry";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, sp);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _empTimeEntry.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeId", DbType.Int32, _empTimeEntry.EmployeeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@WorkDate", DbType.Date, _empTimeEntry.WorkDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartTime",DbType.Time   ,( DateTime.Parse(_empTimeEntry.StartTime.ToString()))));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndTime",DbType.Time, (DateTime.Parse(_empTimeEntry.EndTime.ToString()))));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TimeInsMinutes", DbType.Decimal,_empTimeEntry.TimeInMinutes));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TimeCategoryId", DbType.Int32, _empTimeEntry.TimeCategoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Comment",DbType.String, _empTimeEntry.Comment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser",DbType.String,_empTimeEntry.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedDate",DbType.DateTime, DateTime.Now));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _empTimeEntry.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractList", SqlDbType.Structured, GetDataTable(_empTimeEntry.ContractList)));
                cmd.ExecuteNonQuery();
                isSuccess = true;                 
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return isSuccess;
        }

        private DataTable GetDataTable(List<int> contractList)
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));

            if (contractList != null)
                foreach (var contract in contractList)
                {
                    var row = dataTable.NewRow();
                    row["ID"] = contract;
                    dataTable.Rows.Add(row);
                }
            return dataTable;
        }
    }
}
