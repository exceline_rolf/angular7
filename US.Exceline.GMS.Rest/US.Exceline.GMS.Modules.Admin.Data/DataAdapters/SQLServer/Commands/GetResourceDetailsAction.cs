﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.Common;
using System.Linq;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetResourceDetailsAction : USDBActionBase<List<ResourceDC>>
    {
        private int _branchId;
        private string _searchText;
        private bool _isActive = true;
        private int _categoryId;
        private int _activityId;
        private int _equipmentid;
        private string _gymCode = string.Empty;

        public GetResourceDetailsAction(int branchId, string searchText, int categoryId, int activityId, int equipmentid, bool isActive, string gymCode)
        {
            _branchId = branchId;
            _searchText = searchText;
            _isActive = isActive;
            _categoryId = categoryId;
            _activityId = activityId;
            _equipmentid = equipmentid;
            _gymCode = gymCode;

            if (!string.IsNullOrEmpty(_searchText))
            {
                string[] sTextArray = _searchText.Split(new char[] { ':' });
                if (sTextArray.Length == 1)
                {

                    if (sTextArray.Length > 1)
                    {
                        _searchText = sTextArray[0];
                    }
                }
                else
                {
                   // string[] textParts = sTextArray[1].Split(new char[] { ' ' });
                    if (sTextArray.Length > 1)
                    {
                        _searchText = sTextArray[1];
                    }

                }
            }
        }

        protected override List<ResourceDC> Body(DbConnection connection)
        {
            var resourceLst = new List<ResourceDC>();
            const string storedProcedureName = "USExceGMSAdminGetResourceDetailsNew";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                if (!string.IsNullOrEmpty(_searchText))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@SearchText", DbType.String, _searchText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveState", DbType.Boolean, _isActive));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsEquipment", DbType.Int32, _equipmentid));
                if (_categoryId > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryId", DbType.Int32, _categoryId));
                if (_activityId > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", DbType.Int32, _activityId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var resource = new ResourceDC();
                    resource.Id = Convert.ToInt32(reader["ResourceId"]);
                    resource.BranchId = Convert.ToInt32(reader["BranchId"]);
                    resource.Name = reader["Name"].ToString();
                    resource.Description = reader["Description"].ToString();
                    resource.CreatedDate = Convert.ToDateTime(reader["CreatedTime"]);
                    resource.CreatedUser = reader["CreatedUser"].ToString();
                    resource.ModifiedDate = Convert.ToDateTime(reader["LastModifiedDate"]);
                    resource.LastModifiedUser = reader["ModifiedUser"].ToString();
                    resource.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    resource.ResId = Convert.ToString(reader["ResId"]);
                    var categoryDc = new CategoryDC
                                            {
                                                Id = Convert.ToInt32(reader["CategoryId"]),
                                                Code = reader["CategoryCode"].ToString(),
                                                Name = reader["CategoryDescription"].ToString()
                                            };
                    resource.ResourceCategory = categoryDc;

                    var timeCategoryDc = new CategoryDC
                    {
                        Id = Convert.ToInt32(reader["TimeCategoryId"]),
                        Code = reader["TimeCategoryCode"].ToString(),
                        Name = reader["TimeCategoryName"].ToString()
                    };
                    resource.TimeCategory = timeCategoryDc;

                    if (reader["SalaryPerBooking"] != DBNull.Value)
                        resource.IsSalaryPerBooking = Convert.ToBoolean(reader["SalaryPerBooking"]);

                    if (reader["PurchasedDate"] != DBNull.Value)
                        resource.PurchasedDate = Convert.ToDateTime(reader["PurchasedDate"]);

                    if (reader["ServiceDate"] != DBNull.Value)
                        resource.MaintenanceDate = Convert.ToDateTime(reader["ServiceDate"]);

                    resource.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    resource.ActivityName = reader["ActivityName"].ToString();
                    resource.DaysForBookingPerActivity = Convert.ToInt32(reader["DaysForBooking"]);
                    resource.EquipmentId = Convert.ToInt32(reader["EquipmentId"]);
                    resource.ActivitySmsReminder = Convert.ToBoolean(reader["SmsReminder"]);
                    resource.IsEquipment = resource.EquipmentId != 0;
                    resource.ActivityId = Convert.ToInt32(reader["EquipmentId"]);
                  

                    var action = new GetEmpolyeeForResourceAction(resource.Id);
                   resource.EmpList = action.Execute(EnumDatabase.Exceline, _gymCode);

                  // GetResourceScheduleItemsAction actionSchedule = new GetResourceScheduleItemsAction(resource.Id, "RES", _gymCode);
                  // resource.Schedule = actionSchedule.Execute(EnumDatabase.Exceline, _gymCode);
                   //if (resource.ActivityId > 0)
                   //{
                   //    GetActivityUnavailableTimeAction actionActivityTime = new GetActivityUnavailableTimeAction(_branchId, resource.ActivityId);
                   //    resource.ActivityTimeLst = actionActivityTime.Execute(EnumDatabase.Exceline, _gymCode);
                   //}
                    

                    if (resource.EmpList != null && resource.EmpList.ToList().Any())
                    {
                        resource.EmpCount = resource.EmpList.Count();
                        resource.IsHasEmp = true;
                    }


                    resourceLst.Add(resource);
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return resourceLst;
        }



        //public ResourceDC RunOnTransation(DbTransaction dbTransaction, ResourceDC res)
        //{
        //    string StoredProcedureName = "USExceGMSAdminGetActivitiesForResource";
        //    try
        //    {

        //        DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
        //        cmd.Connection = dbTransaction.Connection;
        //        cmd.Transaction = dbTransaction;
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
        //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@id", DbType.Int32, res.Id));

        //        DbDataReader reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            ActivityDC ActivityItem = new ActivityDC();
        //            ActivityItem.Id = Convert.ToInt32(reader["ID"]);
        //            ActivityItem.Name = Convert.ToString(reader["Name"]);
        //            ActivityItem.BranchId = Convert.ToInt32(reader["BranchId"]);
        //            ActivityItem.IsActive = Convert.ToBoolean(reader["IsActive"]);

        //            res.ActivityList.Add(ActivityItem);
        //        }
        //        reader.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return res;

        //}
    }
}
