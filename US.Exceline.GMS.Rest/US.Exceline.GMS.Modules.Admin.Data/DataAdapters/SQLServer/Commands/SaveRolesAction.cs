﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US_DataAccess;
using US.GMS.Core.SystemObjects;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveRolesAction : USDBActionBase<bool>
    {
        private EmployeeDC _employeeDc;
        private bool _isSave = false;
        public SaveRolesAction(EmployeeDC employeeDc)
        {
            _employeeDc = employeeDc;
        }
        protected override bool Body(DbConnection connection)
        {
            try
            {
                DbTransaction dbtran = connection.BeginTransaction();
                DeleteEmployeesRolesAction saveAction = new DeleteEmployeesRolesAction(_employeeDc);
                _isSave = saveAction.RunOnTransation(dbtran);
                if (!_isSave)
                {
                    dbtran.Rollback();
                    _isSave = false;
                }
                else
                {
                    if (_isSave)
                    {
                        SaveEmployeesRolesAction saveRole = new SaveEmployeesRolesAction(_employeeDc);
                        saveRole.ParentTransaction = dbtran;
                        bool schItemId = saveRole.RunOnTransaction(dbtran);
                        if (!schItemId)
                        {
                            dbtran.Rollback();
                            _isSave = false;
                        }
                    }
                }
                dbtran.Commit();
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _isSave;
        }
    }
}
