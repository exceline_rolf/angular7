﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateEmployeesActivenesAction : USDBActionBase<string>
    {
        private int _instructorId;
        private bool _isActive;
        private string _roleId = string.Empty;
        public UpdateEmployeesActivenesAction(int instructorId, bool isActive, string roleId)
        {
            _instructorId = instructorId;
            _isActive = isActive;
            _roleId = roleId;
        }

        protected override string Body(DbConnection connection)
        {
            string result = string.Empty;
            string StoredProcedureName = "USExceGMSAdminUpdateEmployeesActiveness";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeesId", System.Data.DbType.Int32, _instructorId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeState", System.Data.DbType.Int32, _isActive));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@roleId", System.Data.DbType.String, _roleId));
                object obj = cmd.ExecuteScalar();
                result = obj.ToString();
            }
            catch (Exception ex)
            {
                result = "ERROR";
                throw new NotImplementedException("", ex);
            }
            return result;
        }
    }
}
