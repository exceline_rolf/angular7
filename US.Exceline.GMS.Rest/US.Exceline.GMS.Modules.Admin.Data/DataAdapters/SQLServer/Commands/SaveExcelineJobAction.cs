﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.Utils;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
   public class SaveExcelineJobAction : USDBActionBase<int>
    {
       private DataTable _dataTableEntity;
       private DataTable _dtActiveTimes;
       private ScheduleItemDC _scheduleItem;
       private int _resId;
       private int _branchId;
      // private string _user = string.Empty;
       public SaveExcelineJobAction(ScheduleItemDC scheduleItem, int branchId)
       {
           _branchId = branchId;
           _scheduleItem = scheduleItem;
           //_dataTableEntity = GetEntityList(scheduleItem.InstructorIdList, scheduleItem.ResourceId);
           if (_scheduleItem.ActiveTimes != null)
           {
               _dtActiveTimes = GetActiveTimesDataTable(_scheduleItem.ActiveTimes.ToList());
           }
       }

       private DataTable GetActiveTimesDataTable(List<EntityActiveTimeDC> activeTimes)
       {
           _dtActiveTimes = new DataTable();
           _dtActiveTimes.Columns.Add(new DataColumn("ScheduleItemId", typeof(int)));
           _dtActiveTimes.Columns.Add(new DataColumn("StartDateTime", typeof(DateTime)));
           _dtActiveTimes.Columns.Add(new DataColumn("EndDateTime", typeof(DateTime)));

           if (_scheduleItem.ActiveTimes != null && _scheduleItem.ActiveTimes.Any())
           {
               foreach (EntityActiveTimeDC activeTime in _scheduleItem.ActiveTimes)
               {
                   DataRow dr = _dtActiveTimes.NewRow();
                   dr["ScheduleItemId"] = _scheduleItem.Id;
                   dr["StartDateTime"] = activeTime.StartDateTime;
                   dr["EndDateTime"] = activeTime.EndDateTime;
                   _dtActiveTimes.Rows.Add(dr);
               }
           }
           return _dtActiveTimes;
       }

        protected override int Body(DbConnection connection)
        {
            int scheduleItemId;
            const string storedProcedureName = "USExceGMSJobSaveScheduleItem";
            try
            {

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _scheduleItem.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", DbType.Int32, _scheduleItem.ScheduleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityRoleType", DbType.String, _scheduleItem.EntityRoleType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@JobCategoryId", DbType.Int32, _scheduleItem.ExcelineJob.JobCategoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Occurrence", DbType.String, _scheduleItem.Occurrence));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Day", DbType.String, _scheduleItem.Day));
                if (_scheduleItem.StartTime != DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", DbType.DateTime, _scheduleItem.StartTime));
                if (_scheduleItem.EndTime != DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", DbType.DateTime, _scheduleItem.EndTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _scheduleItem.StartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _scheduleItem.EndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedDateTime", DbType.DateTime, DateTime.Now));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedDateTime", DbType.DateTime, DateTime.Now));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _scheduleItem.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedUser", DbType.String, _scheduleItem.LastModifiedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus", DbType.Boolean, true));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@WeekType", DbType.Int32, _scheduleItem.WeekType));
                //if (_scheduleItem.UpdateStatus > 0) //when UpdateStatus = 1(UpdatedBeforeScheduledItemStarted) UpdateStatus = 2(UpdatedAfterScheduledItemStarted)UpdateStatus = 3(EndDateChange)
                //    cmd.Parameters.Add(DataAcessUtils.CreateParam("@UpdateStatus", DbType.Int32, _scheduleItem.UpdateStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeId", DbType.Int32, _scheduleItem.EmpId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RoleId", DbType.Int32, _scheduleItem.RoleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Title", DbType.String, _scheduleItem.ExcelineJob.Title));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ExtenedInfo", DbType.Xml, XMLUtils.SerializeDataContractObjectToXML(
                    new ExtendedFieldInfoDC() { ExtendedFieldsList = _scheduleItem.ExcelineJob.ExtendedFieldsList })));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimesList", SqlDbType.Structured, _dtActiveTimes));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OutPut", DbType.Int32, _scheduleItem.Id));

                object obj = cmd.ExecuteScalar();
                scheduleItemId = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return scheduleItemId;
        }
    }
}
