﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveStockAction : USDBActionBase<decimal>
    {
        private readonly ArticleDC _article;
        private readonly string _user = string.Empty;
        private readonly int _branchId;

        public SaveStockAction(ArticleDC article,int branchId, string user)
        {
            _article = article;
            _user = user;
            _branchId = branchId;
        }
        protected override decimal Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSAdminSaveArticleStock";
            decimal result = -1;

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _article.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StockLevel", DbType.Int32, _article.StockLevel = (_article.IsStockAdd ? _article.StockLevel : (_article.StockLevel * -1))));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsVatAdded", DbType.Boolean, _article.IsVatAdded));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                if (_article.PurchasedPrice > 0 && _article.StockLevel > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@PurchasedPrice", DbType.Decimal, _article.PurchasedPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StockCategoryId", DbType.Int32, _article.StockCategoryId));

                var param = new SqlParameter
                    {
                        Precision = 7,
                        Scale = 2,
                        ParameterName = "@output",
                        SqlDbType = SqlDbType.Decimal,
                        Direction = ParameterDirection.Output
                    };
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
                result = Convert.ToDecimal(param.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
