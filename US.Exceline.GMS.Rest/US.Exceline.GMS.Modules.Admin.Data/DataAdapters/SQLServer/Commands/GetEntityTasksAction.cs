﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "5/14/2012 11:27:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{


    public class GetEntityTasksAction : USDBActionBase<List<ExcelineTaskDC>>
    {
        private int _entityId;
        private int _branchId;
        private string _entityRoleType;

        public GetEntityTasksAction(int entityId, int branchId, string entityRoleType)
        {
            _entityId = entityId;
            _branchId = branchId;
            _entityRoleType = entityRoleType;
        }

        protected override List<ExcelineTaskDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ExcelineTaskDC> taskList = new List<ExcelineTaskDC>();
            string storedProcedure = "USExceGMSAdminGetEmployeeTasks";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRoleType", DbType.String, _entityRoleType));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExcelineTaskDC task = new ExcelineTaskDC();
                    task.Id = Convert.ToInt32(reader["ID"]);
                    task.Title = Convert.ToString(reader["Name"]);
                    if (reader["DueDate"] != DBNull.Value)
                    {
                        task.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    }
                    else
                    {
                        task.DueDate = new DateTime();
                    }
                    if (reader["StartTime"] != DBNull.Value)
                    {
                        task.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    }
                    if (reader["EndTime"] != DBNull.Value)
                    {
                        task.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    }
                    if (task.EndTime > task.StartTime)
                    {
                        TimeSpan s = task.EndTime - task.StartTime;
                        task.EstimatedTime = (decimal)s.TotalHours;
                    }
                    else
                    {
                        TimeSpan s = task.StartTime - task.EndTime;
                        task.EstimatedTime = (decimal)s.TotalHours;
                    }
                    if (reader["TotalTimeSpent"] != DBNull.Value)
                    {
                        task.TotalTimeSpent = Convert.ToDecimal(reader["TotalTimeSpent"]);
                    }
                    if (reader["TaskStatusCategoryId"] != DBNull.Value)
                    {
                        task.TaskTemplateId = Convert.ToInt32(reader["TaskStatusCategoryId"]);
                    }

                    taskList.Add(task);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return taskList;
        }

    }
}
