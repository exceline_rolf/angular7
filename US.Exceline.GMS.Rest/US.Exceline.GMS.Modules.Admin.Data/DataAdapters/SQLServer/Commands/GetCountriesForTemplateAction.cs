﻿// --------------------------------------------------------------------------
// Copyright(c) <2013> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : DKA
// Created Timestamp : "5/5/2013 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetCountriesForTemplateAction : USDBActionBase<List<string>>
    {
        private int _templateId = -1;
        private int _branchId;

        public GetCountriesForTemplateAction(int templateId)
        {
            _templateId = templateId;
        }

        protected override List<string> Body(System.Data.Common.DbConnection connection)
        {
            List<string> countryList = new List<string>();
            string SPGetItems = "USExceGMSAdminGetCountriesByTemplateNumber";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, SPGetItems);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@templateID", DbType.Int32, _templateId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string countryId = Convert.ToString(reader["ID"]);
                    countryList.Add(countryId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return countryList;
        }
    }
}
