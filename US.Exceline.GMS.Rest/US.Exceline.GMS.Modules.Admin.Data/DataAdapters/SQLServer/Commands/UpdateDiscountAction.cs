﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System.Data.Common;
using US_DataAccess;
using System.Data;
using US.GMS.Core.DomainObjects.Admin;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateDiscountAction : USDBActionBase<bool>
    {
        private ShopDiscountDC _updateDiscount = new ShopDiscountDC();
        private bool _returnValue = false;


        public UpdateDiscountAction(ShopDiscountDC updateDiscount)
        {
            this._updateDiscount = updateDiscount;
        }

        protected override bool Body(DbConnection connection)
        {
            string StoredProcedure1 = "USExceGMSShopUpdateDiscount";
            string StoredProcedure2 = "USExceGMSShopDeleteInventoryDiscount";
            string StoredProcedure3 = "USExceGMSShopAddInventoryDiscount";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedure1);

                cmd.Parameters.Clear();
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _updateDiscount.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _updateDiscount.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _updateDiscount.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Discount", DbType.Double, _updateDiscount.Discount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _updateDiscount.StartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _updateDiscount.EndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _updateDiscount.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActive", DbType.Boolean, true));
                cmd.ExecuteNonQuery();

                int discountId = _updateDiscount.Id;
                DbCommand cmdDelete = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedure2);
                cmdDelete.Parameters.Add(DataAcessUtils.CreateParam("@ExceShopDiscountId", DbType.Int32, discountId));
                bool deleted = false;
                try
                {
                    cmdDelete.ExecuteNonQuery();
                    deleted = true;
                }
                catch
                {
                    throw;
                }

                if (deleted)
                {
                    if (discountId != 0)
                    {
                        DbCommand cmdItems = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedure3);
                        foreach (int itemId in _updateDiscount.ItemList)
                        {
                            cmdItems.Parameters.Clear();
                            cmdItems.Parameters.Add(DataAcessUtils.CreateParam("@ExceShopDiscountId", DbType.Int32, discountId));
                            cmdItems.Parameters.Add(DataAcessUtils.CreateParam("@ItemId", DbType.Int32, itemId));
                            cmdItems.ExecuteNonQuery();

                        }
                        _returnValue = true;
                    }
                }

            }
            catch
            {
                _returnValue = false;
            }
            return _returnValue;
        }
    }
}
