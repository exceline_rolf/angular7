﻿// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : Unicorngma
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Payments;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class AddUpdateRevenueAccountsAction : USDBActionBase<int>
    {
        private readonly AccountDC _revenueAccountDetail;
        private readonly string _user;
        public AddUpdateRevenueAccountsAction(AccountDC revenueAccountDetail,String user)
        {
            _revenueAccountDetail = revenueAccountDetail;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            int outputId = -1;
            const string storedProcedureName = "USExceGMSAdminAddEditRevenueAccounts";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccountId", DbType.String, _revenueAccountDetail.ID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccountNo", DbType.String, _revenueAccountDetail.AccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccountName", DbType.String, _revenueAccountDetail.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User",DbType.String,_user));
                DbParameter para1 = new SqlParameter();
                para1.DbType = DbType.Int32;
                para1.ParameterName = "@outPutID";
                para1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para1);
                cmd.ExecuteNonQuery();
                outputId = Convert.ToInt32(para1.Value);
            }

            catch
            {
                throw;
            }
            return outputId;
        }
    }
}
