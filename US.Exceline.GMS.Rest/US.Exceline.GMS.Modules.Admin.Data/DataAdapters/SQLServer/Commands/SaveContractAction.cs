﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "4/1/2012 10:39:08 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class SaveContractAction : USDBActionBase<int>
    {
        private PackageDC _package;
        private string _user = string.Empty;

        public SaveContractAction(PackageDC package, string user)
        {
            _package = package;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            int contractId = -1;
            string StoredProcedureName = "USExceGMSAdminAddEditContract";
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.String, _package.PackageId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _package.PackageName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _package.BranchId));
                if (_package.ContractTypeValue != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@packageTypeId", DbType.Int32, _package.ContractTypeValue.Id));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DiscountRateType ", DbType.String, _package.DiscountRate.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId ", DbType.Int32, _package.ContractActivity.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _package.StartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _package.EndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@numberOfInstallments", DbType.Int32, _package.NumOfInstallments));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@packagePrice", DbType.Decimal, _package.PackagePrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ratePerInstallment", DbType.Decimal, _package.RatePerInstallment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@enrollmentFee", DbType.Decimal, _package.EnrollmentFee));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@numberOfVisits", DbType.Int32, _package.NumOfVisits));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Boolean, _package.ActiveStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _package.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastModifiedUser", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractTypeId", DbType.Int32, _package.ContractTypeId));
                if (_package.MemberFeeArticle != null && _package.MemberFeeArticle.ArticleId > 1)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberFeeArticleId", DbType.Int32, _package.MemberFeeArticle.ArticleId));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractId", DbType.Int32, 0));
                if (_package.ContractActivity != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@articleId", DbType.Int32, _package.ContractActivity.Article.Id));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@articleId", DbType.Int32, _package.ArticleId));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lockinperiod", DbType.Int32, _package.LockInPeriod));
                if (_package.PackageCategory != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryId", DbType.Int32, _package.PackageCategory.Id));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isShownOnTheNet", DbType.Boolean, _package.IsShownOnNet));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@restPusMonth", DbType.Boolean, _package.RestPlusMonth));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateNo", DbType.String, _package.TemplateNumber));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceDetails", DbType.Boolean, _package.IsInvoiceDetail));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AutoRenew", DbType.Boolean, _package.AutoRenew));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@priceGuaranty", DbType.Int32, _package.PriceGuaranty));
                if (_package.AccessProfileId > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@accessProfileID", DbType.Int32, _package.AccessProfileId));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@accessProfileID", DbType.Int32, DBNull.Value));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isATG", DbType.Boolean, _package.IsATG));
                if (_package.FixDateOfContract.HasValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@fixDateOfContract", DbType.DateTime, _package.FixDateOfContract));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@creditDueDays", DbType.Int32, _package.CreditDueDays));
                if (_package.NextTemplateId != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@nextTemplateId", DbType.Int32, _package.NextTemplateId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@orderPrice", DbType.Decimal, _package.OrderPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startUpItemPrice", DbType.Decimal, _package.StartUpItemPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@everyMonthItemPrice", DbType.Decimal, _package.EveryMonthItemsPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@noOfMonths", DbType.Int32, _package.NoOfMonths));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@maxAge", DbType.Int32, _package.MaxAge));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@firstDueDate", DbType.DateTime, _package.FirstDueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@secondDueDate", DbType.DateTime, _package.SecondDueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InStock", DbType.Int32, _package.InStock));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDateOfContract", DbType.DateTime, _package.FixStartDateOfContract));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sorting", DbType.Int32, _package.SortingNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@inStockVisible", DbType.Boolean, _package.IsInStock));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@noClasses", DbType.Boolean, _package.IsNoClasses));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@noOfDays", DbType.Int32, _package.NoOfDays));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ATGStatus", DbType.Int32, _package.ATGStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amountForATG", DbType.Decimal, _package.AmountForATG));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@expressAvailable", DbType.Boolean, _package.ExpressAvailable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractConditionId", DbType.Int32, _package.ContractConditionId));

                DataTable branches = new DataTable();
                DataColumn col = null;
                col = new DataColumn("ID", typeof(Int32));
                branches.Columns.Add(col);
                foreach (var id in _package.BranchIdList)
                {
                    branches.Rows.Add(id);
                }
                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@branches";
                parameter.SqlDbType = System.Data.SqlDbType.Structured;
                parameter.Value = branches;
                cmd.Parameters.Add(parameter);

                DataTable regions = new DataTable();
                DataColumn col2 = null;
                col2 = new DataColumn("ID", typeof(Int32));
                regions.Columns.Add(col2);
                foreach (var id in _package.RegionIdList)
                {
                    regions.Rows.Add(id);
                }
                SqlParameter parameter2 = new SqlParameter();
                parameter2.ParameterName = "@regions";
                parameter2.SqlDbType = System.Data.SqlDbType.Structured;
                parameter2.Value = regions;
                cmd.Parameters.Add(parameter2);

                DataTable countries = new DataTable();
                DataColumn col3 = null;
                col3 = new DataColumn("ID", typeof(string));
                countries.Columns.Add(col3);
                foreach (var id in _package.CountryIdList)
                {
                    countries.Rows.Add(id);
                }
                SqlParameter parameter3 = new SqlParameter();
                parameter3.ParameterName = "@countries";
                parameter3.SqlDbType = System.Data.SqlDbType.Structured;
                parameter3.Value = countries;
                cmd.Parameters.Add(parameter3);

                object obj = cmd.ExecuteScalar();
                contractId = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                contractId = -1;
                transaction.Rollback();
                throw ex;
            }
            if (_package.Mode == "Edit")
            {
                try
                {
                    string spName = "USExceGMSAdminDeleteContractTemplateItems";
                    DbCommand cmd = GenericDBFactory.Factory.CreateCommand();
                    cmd.CommandText = spName;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = transaction.Connection;
                    cmd.Transaction = transaction;
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractId", DbType.Int32, contractId));
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }

            if (_package.EveryMonthItemList != null)
            {
                foreach (ContractItemDC contractItem in _package.EveryMonthItemList)
                {
                    if (contractItem.IsSelected)
                    {
                        string spAddons = "USExceGMSAdminSaveContractTemplateItems";
                        try
                        {
                            DbCommand cmdAddon = GenericDBFactory.Factory.CreateCommand();
                            cmdAddon.CommandText = spAddons;
                            cmdAddon.CommandType = CommandType.StoredProcedure;
                            cmdAddon.Connection = transaction.Connection;
                            cmdAddon.Transaction = transaction;
                            cmdAddon.Parameters.Add(DataAcessUtils.CreateParam("@contractId", DbType.Int32, contractId));
                            cmdAddon.Parameters.Add(DataAcessUtils.CreateParam("@articleId", DbType.Int32, contractItem.ArticleId));
                            cmdAddon.Parameters.Add(DataAcessUtils.CreateParam("@articlePrice", DbType.Decimal, contractItem.Price));
                            cmdAddon.Parameters.Add(DataAcessUtils.CreateParam("@isStartUpItem", DbType.Boolean, contractItem.IsStartUpItem));
                            cmdAddon.Parameters.Add(DataAcessUtils.CreateParam("@numberOfOrders", DbType.Int32, contractItem.NumberOfOrders));
                            cmdAddon.Parameters.Add(DataAcessUtils.CreateParam("@units", DbType.Int32, contractItem.Quantity));
                            cmdAddon.Parameters.Add(DataAcessUtils.CreateParam("@isActivityArticle", DbType.Boolean, contractItem.IsActivityArticle));
                            cmdAddon.Parameters.Add(DataAcessUtils.CreateParam("@startOrder", DbType.Int32, contractItem.StartOrder));
                            cmdAddon.Parameters.Add(DataAcessUtils.CreateParam("@endOrder", DbType.Int32, contractItem.EndOrder));
                            cmdAddon.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            contractId = -1;
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }

            if (_package.StartUpItemList != null)
            {
                foreach (ContractItemDC contractItem in _package.StartUpItemList)
                {
                    if (contractItem.IsSelected)
                    {
                        string spFree = "USExceGMSAdminSaveContractTemplateItems";
                        try
                        {
                            DbCommand cmdFree = GenericDBFactory.Factory.CreateCommand();
                            cmdFree.CommandText = spFree;
                            cmdFree.Connection = transaction.Connection;
                            cmdFree.CommandType = CommandType.StoredProcedure;
                            cmdFree.Transaction = transaction;
                            cmdFree.Parameters.Add(DataAcessUtils.CreateParam("@contractId", DbType.Int32, contractId));
                            cmdFree.Parameters.Add(DataAcessUtils.CreateParam("@articleId", DbType.Int32, contractItem.ArticleId));
                            cmdFree.Parameters.Add(DataAcessUtils.CreateParam("@articlePrice", DbType.Decimal, contractItem.Price));
                            cmdFree.Parameters.Add(DataAcessUtils.CreateParam("@isStartUpItem", DbType.Boolean, 1));
                            cmdFree.Parameters.Add(DataAcessUtils.CreateParam("@units", DbType.Int32, contractItem.Quantity));
                            cmdFree.Parameters.Add(DataAcessUtils.CreateParam("@isActivityArticle", DbType.Boolean, contractItem.IsActivityArticle));
                            cmdFree.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            contractId = -1;
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            transaction.Commit();
            return contractId;
        }
    }
}
