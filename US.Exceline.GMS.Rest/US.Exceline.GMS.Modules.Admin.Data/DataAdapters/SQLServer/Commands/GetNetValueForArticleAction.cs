﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetNetValueForArticleAction : USDBActionBase<Dictionary<decimal, decimal>>
   {
       private int _inventoryId;
       private int _articleId;
       private int _counted;
       private int _branchId;
       public GetNetValueForArticleAction(int inventoryId, int articleId, int counted, int branchId)
       {
           _inventoryId = inventoryId;
           _articleId = articleId;
           _counted = counted;
           _branchId = branchId;
       }
       protected override Dictionary<decimal, decimal> Body(DbConnection connection)
        {
            const string storedProcedure = "USExceGMSAdminGetNetValueForArticle";
            var result = new Dictionary<decimal, decimal>();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InventoryId", DbType.Int32, _inventoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleId", DbType.Int32, _articleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Counted", DbType.Int32, _counted));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    decimal netValue = Convert.ToDecimal(reader["NetValue"]);
                    decimal countedValue = Convert.ToDecimal(reader["CountedValue"]);
                    result.Add(netValue, countedValue);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
    }
}
