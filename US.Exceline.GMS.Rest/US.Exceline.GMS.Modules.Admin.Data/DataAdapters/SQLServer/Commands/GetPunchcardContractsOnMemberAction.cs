﻿// --------------------------------------------------------------------------
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : Martin Tømmerås
// Created Timestamp : 23.08.2018 - 08:45
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using System.Collections.Generic;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetPunchcardContractsOnMemberAction : USDBActionBase<List<ContractInfo>>
    {
        private int _memberId;
        private string _activity; 
        private List<ContractInfo> contractList = new List<ContractInfo>();

        public GetPunchcardContractsOnMemberAction(int memberId, string activity)
        {
            this._memberId = memberId;
            this._activity = activity;
        }

        protected override List<ContractInfo> Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSGetPunchcardContractsOnMember";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activity", DbType.String, _activity));

                DbDataReader reader = cmd.ExecuteReader();
                    
                while (reader.Read()) {

                    ContractInfo contract = new ContractInfo();

                    contract.ContractId = Convert.ToString(reader["ContractId"]);
                    contract.ContractNo = Convert.ToString(reader["Contractnumber"]);
                    contract.TemplateName = Convert.ToString(reader["TemplateName"]);
                    contract.AvailableVisits = Convert.ToString(reader["AvailableVisits"]);

                    contractList.Add(contract);
                };
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return contractList;
        }
    }
}
