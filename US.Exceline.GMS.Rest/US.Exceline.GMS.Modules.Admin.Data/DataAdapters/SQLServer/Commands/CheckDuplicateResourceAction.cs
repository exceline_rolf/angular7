﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
  public class CheckDuplicateResourceAction : USDBActionBase<int>
  {
      private int _sourceId;
      private int _destinationId;
      private DateTime _startDate;
      private DateTime _endDate;
      public CheckDuplicateResourceAction(int sourceId,int destinationId,DateTime startDate,DateTime endDate)
      {
          _sourceId = sourceId;
          _destinationId = destinationId;
          _startDate = startDate;
          _endDate = endDate;
      }

      protected override int Body(DbConnection connection)
      {
          int count = -1;
          const string storedProcedureName = "USExceGMSAdminCheckIsDuplicateResource";
          try
          {

              DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
              cmd.Parameters.Add(DataAcessUtils.CreateParam("@SourceentityId", DbType.Int32, _sourceId));
              cmd.Parameters.Add(DataAcessUtils.CreateParam("@DestinationentityId", DbType.Int32, _destinationId));
              cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _startDate));
              cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _endDate));

              DbParameter output = new SqlParameter();
              output.DbType = DbType.Int32;
              output.ParameterName = "@OutPut";
              output.Size = 50;
              output.Direction = ParameterDirection.Output;
              cmd.Parameters.Add(output);
              cmd.ExecuteNonQuery();
              count = Convert.ToInt32(output.Value);
          }
          catch (Exception)
          {
              throw ;
          }
          return count;
          
      }
  }
}
