﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;
using System.IO;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.API;
using US.GMS.Core.ResultNotifications;
using US.Common.Logging.API;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetTrainerDetailsByTrainerIdAction : USDBActionBase<TrainerDC>
    {
        private int _branchId = 0;
        private string _user = string.Empty;
        private int _trainerId = -1;
        private string _gymCode = string.Empty;

        public GetTrainerDetailsByTrainerIdAction(int branchId, string user, int trainerId, string gymCode)
        {
            _branchId = branchId;
            _user = user;
            _trainerId = trainerId;
            _gymCode = gymCode;

        }

        protected override TrainerDC Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminGetTrainerByTrainerId";
            TrainerDC trainer = new TrainerDC();
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@trainerId", DbType.Int32, _trainerId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    trainer.Id = Convert.ToInt32(reader["TrainerId"]);
                    trainer.CustId = Convert.ToString(reader["CustId"]);
                    trainer.EntNo = Convert.ToInt32(reader["EntNo"]);
                    trainer.BranchId = Convert.ToInt32(reader["BranchId"]);
                    trainer.FirstName = Convert.ToString(reader["FirstName"]);
                    trainer.LastName = Convert.ToString(reader["LastName"]);
                    trainer.Name = trainer.FirstName + " " + trainer.LastName;
                    trainer.BirthDay = Convert.ToDateTime(reader["BirthDate"]);
                    trainer.Address1 = Convert.ToString(reader["Address"]);
                    trainer.Address2 = Convert.ToString(reader["Address2"]);
                    trainer.Address3 = Convert.ToString(reader["Address3"]);
                    trainer.PostCode = Convert.ToString(reader["ZipCodde"]);
                    trainer.PostPlace = Convert.ToString(reader["ZipName"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Mobile"])))
                    {
                        trainer.Mobile = Convert.ToString(reader["Mobile"]).Trim();
                    }
                    if (reader["TeleWork"] != DBNull.Value)
                    {
                        trainer.Work = Convert.ToString(reader["TeleWork"]).Trim();
                    }
                    if (reader["TeleHome"] != DBNull.Value)
                    {
                        trainer.Private = Convert.ToString(reader["TeleHome"]).Trim();
                    }
                    trainer.Email = Convert.ToString(reader["Email"]);
                    trainer.Gender = Convert.ToString(reader["Gender"]);
                    trainer.EntityRoleType = "TRA";
                    trainer.Description = Convert.ToString(reader["Description"]);
                    trainer.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    trainer.ImagePath = Convert.ToString(reader["ImagePath"]);
                    CategoryDC categoryDC = new CategoryDC
  {
      Id = Convert.ToInt32(reader["CategoryId"]),
      Code = Convert.ToString(reader["CategoryCode"]),
      Name = Convert.ToString(reader["CategoryDescription"])
  };
                    trainer.Category = categoryDC;
                    trainer.RoleId = Convert.ToString(reader["RoleId"]);
                    trainer.IsHasTask = Convert.ToBoolean(reader["IsHasTask"]);
                    trainer.AssignedEntityId = Convert.ToInt32(reader["AssignedEntityId"]);
                    trainer.CommentForInactive = Convert.ToString(reader["Comment"]);
                    trainer.SourceEntity = Convert.ToInt32(reader["SourceEntity"]);
                    GetActivitiesForEmployeeAction action = new GetActivitiesForEmployeeAction(trainer.BranchId, trainer.Id, trainer.RoleId);
                    try
                    {
                        trainer.ActivityList = action.Execute(EnumDatabase.Exceline, _gymCode);
                    }
                    catch
                    {
                        throw;
                    }
                    if (!string.IsNullOrEmpty(trainer.ImagePath))
                    {
                        trainer.ProfilePicture = GetMemberProfilePicture(trainer.ImagePath);
                    }

                    try
                    {
                        trainer.Schedule = GetEntitySchedule(trainer.Id, trainer.RoleId, _user, _gymCode);
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException("", ex);
            }

            return trainer;
        }

        private byte[] GetMemberProfilePicture(string p)
        {
            if (File.Exists(p))
            {
                byte[] byteArray = System.IO.File.ReadAllBytes(p);
                return byteArray;
            }
            return new byte[0];
        }


        public ScheduleDC GetEntitySchedule(int entityId, string entityRoleType, string user, string gymCode)
        {
            OperationResult<ScheduleDC> result = GMSSchedule.GetEntitySchedule(entityId, entityRoleType, user, gymCode);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new ScheduleDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

    }
}

