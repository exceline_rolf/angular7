﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class SavePaymentBookingMemberAction : USDBActionBase<int>
    {
        private readonly int _activetimeId;
        private readonly int _memberId;
        private readonly bool _paid;
        private readonly int _aritemNo;
        private readonly decimal _amount;
        private int _articleId;
        private string _paymentType = string.Empty;
        public SavePaymentBookingMemberAction(int activetimeId, int articleId, int memberId, bool paid, int aritemNo, decimal amount, string paymentType)
        {
            _activetimeId = activetimeId;
            _memberId = memberId;
            _paid = paid;
            _aritemNo = aritemNo;
            _amount = amount;
            _articleId = articleId;
            _paymentType = paymentType;
        }
        protected override int Body(DbConnection connection)
        {
            int result ;
            const string storedProcedureName = "USExceGMSAdminPaidBookingMember";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Activetimeid", DbType.Int32, _activetimeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleId", DbType.Int32, _articleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Memberid", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Paid", DbType.Boolean, _paid));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Aritemno", DbType.Int32, _aritemNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Amount", DbType.Decimal, _amount));
                if (!string.IsNullOrEmpty(_paymentType))
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PaymentType", DbType.String, _paymentType));

                cmd.ExecuteNonQuery();
                result = 1;
            }
            catch
            {
                throw;
            }
            return result;
        }
    }
}
