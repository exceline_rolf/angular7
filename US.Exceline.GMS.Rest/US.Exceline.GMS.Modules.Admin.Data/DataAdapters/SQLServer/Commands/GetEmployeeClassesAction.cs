﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class GetEmployeeClassesAction : USDBActionBase<List<EmployeeClass>>
    {
        private int _empId = -1;
        private string _gymCode = string.Empty;

        public GetEmployeeClassesAction(int empId, string gymCode)
        {
            _empId = empId;
            _gymCode = gymCode;
        }

        protected override List<EmployeeClass> Body(System.Data.Common.DbConnection connection)
        {
            List<EmployeeClass> classList = new List<EmployeeClass>();
            const string storedProcedureName = "GetEmployeeClasses";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmpId", DbType.Int32, _empId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    EmployeeClass emplyeeClass = new EmployeeClass();

                    emplyeeClass.ClassId = Convert.ToInt32(reader["ID"]);
                    emplyeeClass.Day = Convert.ToString(reader["Day"]);
                    emplyeeClass.StartTime = Convert.ToDateTime(reader["StartTime"]).TimeOfDay;
                    emplyeeClass.EndTime = Convert.ToDateTime(reader["EndTime"]).TimeOfDay;
                    emplyeeClass.IsApproved = Convert.ToBoolean(reader["IsApproved"]);
                    emplyeeClass.ClassType = Convert.ToString(reader["Name"]);
                    emplyeeClass.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    emplyeeClass.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    emplyeeClass.EntityActiveTimeID = Convert.ToInt32(reader["EntityActiveTimeID"]);
                    emplyeeClass.BranchId = Convert.ToInt32(reader["BranchId"]);
                    emplyeeClass.BranchName = reader["BranchName"].ToString();

                    //List<DateTime> dateTimeList = new List<DateTime>();
                    //dateTimeList = EmployeeFacade.GetClassDateByEmployee(emplyeeClass.ClassId, _gymCode);
                    //emplyeeClass.DateList = dateTimeList;

                    classList.Add(emplyeeClass);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return classList;
        }
    }
}
