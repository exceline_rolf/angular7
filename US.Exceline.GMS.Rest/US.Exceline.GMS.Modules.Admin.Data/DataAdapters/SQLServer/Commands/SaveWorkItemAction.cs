﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.Utils;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
   public  class SaveWorkItemAction: USDBActionBase<int>
    {
       private GymEmployeeWorkDC _work = new GymEmployeeWorkDC();
       private int _branchId;
       private DateTime _startDateTime = DateTime.Now;
       private DateTime _endDateTime = DateTime.Now;
       public SaveWorkItemAction(GymEmployeeWorkDC work, int branchId)
       {
           _branchId = branchId;
           _work = work;
       }

        protected override int Body(DbConnection connection)
        {
            int workId = -1;
            const string storedProcedureName = "USExceGMSAdminSaveWorkItem";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _work.ScheduleItemId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeId", DbType.Int32, _work.EmployeeId));
                if (_work.Date != DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _work.Date));
                if (_work.Date != DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _work.Date));
                SqlParameter output = new SqlParameter("@OutPut", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                int outputId = Convert.ToInt32(output.Value);
                if (outputId > 0)
                {
                    workId = outputId;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return workId;
        }
    }
}
