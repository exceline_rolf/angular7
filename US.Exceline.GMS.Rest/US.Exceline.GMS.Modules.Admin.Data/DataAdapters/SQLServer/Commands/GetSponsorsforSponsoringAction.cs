﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetSponsorsforSponsoringAction : USDBActionBase<List<ExcelineMemberDC>>
    {
        private int _branchID = -1;
        private DateTime _startDate;
        private DateTime _endDate;
        public GetSponsorsforSponsoringAction(int branchID, DateTime startDate, DateTime endDate)
        {
            _branchID = branchID;
            _startDate = startDate;
            _endDate = endDate;
        }

        protected override List<ExcelineMemberDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ExcelineMemberDC> sponsors = new List<ExcelineMemberDC>();
            string spName = "USExceGMSAdminGetSponsors";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@StartDueDate", System.Data.DbType.Date, _startDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@EndDueDate", System.Data.DbType.Date, _endDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchID", System.Data.DbType.Int32, _branchID));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExcelineMemberDC sponsor = new ExcelineMemberDC();
                    sponsor.Id = Convert.ToInt32(reader["Id"]);
                    sponsor.CustId = Convert.ToString(reader["CustId"]);
                    sponsor.FirstName = Convert.ToString(reader["FirstName"]);
                    sponsor.LastName = Convert.ToString(reader["LastName"]);
                    sponsor.Name = sponsor.FirstName + sponsor.LastName;
                    if(reader["EndDate"] != DBNull.Value)
                       sponsor.GmEndDate = Convert.ToDateTime(reader["EndDate"]);
                    sponsor.Email = Convert.ToString(reader["Email"]);
                    sponsor.MobilePrefix = Convert.ToString(reader["TelMobilePrefix"]);
                    sponsor.Mobile = Convert.ToString(reader["TelMobile"]);
                    sponsor.IsSelected = true;
                    sponsors.Add(sponsor);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return sponsors;
        }
    }
}
