﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer
{
    class AddUpdateSystemSettingEconomySettingAction : USDBActionBase<int>
    {
        private SystemSettingEconomySettingDC _systemSettingEconomySetting;
        private string _user;

        public AddUpdateSystemSettingEconomySettingAction(SystemSettingEconomySettingDC SystemSettingEconomySetting, string user)
        {
            _systemSettingEconomySetting = SystemSettingEconomySetting;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            int outputId = -1;
            string StoredProcedureName = "USExceGMSAdminAddUpdateSystemSettingEconomySetting";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ID",                        DbType.Int32, _systemSettingEconomySetting.ID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditPeriod",              DbType.String, _systemSettingEconomySetting.CreditPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OneDueDateAtDate",          DbType.Boolean, _systemSettingEconomySetting.OneDueDateAtDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FixedDueDay",               DbType.Int32, _systemSettingEconomySetting.FixedDueDay));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsoringDay",             DbType.Int32, _systemSettingEconomySetting.SponsoringDay));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsoredDueDate",          DbType.Int32, _systemSettingEconomySetting.SponsoredDueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsorDueDay",             DbType.Int32, _systemSettingEconomySetting.SponsorDueDay));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsorInvoiceText",        DbType.String, _systemSettingEconomySetting.SponsorInvoiceText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractAutoRenewalDays",   DbType.Int32, _systemSettingEconomySetting.ContractAutoRenewalDays));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsSMSInvoice", DbType.Boolean, _systemSettingEconomySetting.IsSMSInvoice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DeviationFollowUp", DbType.Boolean, _systemSettingEconomySetting.DeviationFollowup));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DeviationFollowUpCompleted", DbType.Int32, _systemSettingEconomySetting.DeviationFollowupCompleted));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReminderDueDays", DbType.Int32, _systemSettingEconomySetting.ReminderDueDays));

                DbParameter para1 = new SqlParameter();
                para1.DbType = DbType.Int32;
                para1.ParameterName = "@outPutID";
                para1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para1);
                cmd.ExecuteNonQuery();
                outputId = Convert.ToInt32(para1.Value);
            }
            catch
            {
                throw;
            }
            return outputId;
        }
    }
}
