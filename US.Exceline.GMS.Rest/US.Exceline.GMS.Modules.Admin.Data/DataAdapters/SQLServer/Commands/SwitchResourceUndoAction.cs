﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SwitchResourceUndoAction : USDBActionBase<bool>
    {
        private int _resId;
        private DateTime _startDate;
        private DateTime _endDate;
        private string _user;
        private int _branchId;
        public SwitchResourceUndoAction(int resId, DateTime startDate, DateTime endDate, string user, int branchId)
       {
           _resId = resId;
           _startDate = startDate;
           _endDate = endDate;
           _user = user;
           _branchId = branchId;
       }
       protected override bool Body(DbConnection connection)
       {
           const string storedProcedureName = "USExceGMSAdminSwitchResourceUndo";

           try
           {
               DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@resourceId", DbType.Int32, _resId));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _startDate));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _endDate));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));

               DbParameter para = new SqlParameter();
               para.DbType = DbType.Boolean;
               para.ParameterName = "@outId";
               para.Direction = ParameterDirection.Output;
               cmd.Parameters.Add(para);
               cmd.ExecuteNonQuery();
               bool result = Convert.ToBoolean(para.Value);
               return result;
           }

           catch (Exception)
           {
               return false;
           }
       }
    }
}
