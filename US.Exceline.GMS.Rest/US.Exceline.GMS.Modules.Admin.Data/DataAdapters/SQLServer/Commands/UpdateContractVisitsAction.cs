﻿// --------------------------------------------------------------------------
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : Martin Tømmerås
// Created Timestamp : 23.08.2018 - 08:45
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using System.Collections.Generic;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateContractVisitsAction : USDBActionBase<int>
    {
        private int _selectedContractId;
        private int _punchedContractId;
        private int _bookingId;
        private int _returnVal;

        public UpdateContractVisitsAction(int selectedContractId, int punchedContractId, int bookingId)
        {
            this._selectedContractId = selectedContractId;
            this._punchedContractId = punchedContractId;
            this._bookingId = bookingId;
            this._returnVal = 0;
        }

        protected override int Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSUpdateContractVisitsAfterPunchardBooking";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@selectedId", DbType.Int32, _selectedContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@punchedId", DbType.Int32, _punchedContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@bookingId", DbType.Int32, _bookingId));

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@output";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
                this._returnVal = Convert.ToInt32(param.Value);
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return this._returnVal;
        }
    }
}
