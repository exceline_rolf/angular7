﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.SystemObjects;
using System.Data;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveEmployeesRolesAction : USDBActionBase<bool>
    {
        private EmployeeDC _employeeDc;
        private DataTable _dataTable = null;
        public SaveEmployeesRolesAction(EmployeeDC employeeDc)
        {
            _employeeDc = employeeDc;

        }

        protected override bool Body(DbConnection connection)
        {

            bool result = false;
            string StoredProcedureName = "USExceGMSAdminDeleteRoles";
            DbTransaction transaction = null;

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _employeeDc.Id));
                cmd.ExecuteNonQuery();
                result = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public bool RunOnTransaction(DbTransaction Transaction)
        {
            string StoredProcedureName = "USExceGMSAdminSaveRoleNew";
            bool result = false;
            try
            {
                if (_employeeDc.RoleActivityList != null && _employeeDc.RoleActivityList.Count > 0)
                {
                    foreach (RoleActivityDC role in _employeeDc.RoleActivityList)
                    {
                        DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                        cmd.Connection = Transaction.Connection;
                        cmd.Transaction = Transaction;
                        result = false;

                        if (role.ActivityList != null)
                            GetActivityItemLst(role.ActivityList);
                        try
                        {
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _employeeDc.Id));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@roleId", DbType.String, role.RoleId));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryId", DbType.Int32, role.CategoryId));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@email", DbType.String, _employeeDc.Email));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@mobile", DbType.String, _employeeDc.Mobile));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@telework", DbType.String, _employeeDc.Work));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@telehome", DbType.String, _employeeDc.Private));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@address1", DbType.String, _employeeDc.Address1));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@address2", DbType.String, _employeeDc.Address2));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@address3", DbType.String, _employeeDc.Address3));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@zip", DbType.String, _employeeDc.PostCode));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@postplace ", DbType.String, _employeeDc.PostPlace));
                            if (role.ActivityList != null)
                            {
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityItemList", SqlDbType.Structured, _dataTable));
                            }
                            else
                            {
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityItemList", SqlDbType.Structured, null));
                            }
                            cmd.ExecuteNonQuery();
                            result = true;
                        }


                        catch (Exception ex)
                        {
                            throw ex;
                        }

                    }
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        private DataTable GetActivityItemLst(List<ActivityDC> activityList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ActivityId", typeof(System.Int32)));
            foreach (ActivityDC activityItem in activityList)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["ActivityId"] = activityItem.Id;
                _dataTable.Rows.Add(_dataTableRow);
            }
            return _dataTable;
        }
    }
}
