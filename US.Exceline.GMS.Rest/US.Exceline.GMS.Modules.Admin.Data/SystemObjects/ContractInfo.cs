﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    [DataContract]
    public class ContractInfo
    {
        private string _contractId;
        [DataMember]
        public string ContractId
        {
            get { return _contractId; }
            set { _contractId = value; }
        }
        private string _templateName;
        [DataMember]
        public string TemplateName
        {
            get { return _templateName; }
            set { _templateName = value; }
        }

        private string _availableVisits;
        [DataMember]
        public string AvailableVisits
        {
            get { return _availableVisits; }
            set { _availableVisits = value;  }
        }
        public string _contractNo;
        [DataMember]
        public string ContractNo
        {
            get { return _contractNo; }
            set { _contractNo = value; }
        }
    }
}