﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.ResultNotifications;
using US.GMS.API;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Shop.API.ManageShop
{
    public class GMSShopManager
    {
        #region Voucher
        public static OperationResult<List<VoucherDC>> GetVouchersList(int branchId, string gymCode)
        {
            OperationResult<List<VoucherDC>> result = new OperationResult<List<VoucherDC>>();
            result = BusinessLogic.ManageShop.ShopManager.GetVouchersList(branchId, gymCode);
            return result;
        }

        public static OperationResult<Dictionary<string, decimal>> GetMemberEconomyBalances(string gymCode, int memberId)
        {
            OperationResult<Dictionary<string, decimal>> result = new OperationResult<Dictionary<string, decimal>>();
            result = BusinessLogic.ManageShop.ShopManager.GetMemberEconomyBalances(gymCode, memberId);
            return result;
        }

        public static OperationResult<bool> AddVoucher(int branchId, VoucherDC voucher, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result = BusinessLogic.ManageShop.ShopManager.AddVoucher(branchId, voucher, gymCode);
            return result;
        }

        public static OperationResult<int> EditVoucher(int branchId, VoucherDC voucher, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            result = BusinessLogic.ManageShop.ShopManager.EditVoucher(branchId, voucher, gymCode);
            return result;
        }
        #endregion

        #region Return List
        public static OperationResult<List<ReturnItemDC>> GetReturnItemList(int branchId, string gymCode)
        {
            OperationResult<List<ReturnItemDC>> result = new OperationResult<List<ReturnItemDC>>();
            result = BusinessLogic.ManageShop.ShopManager.GetReturnItemList(branchId, gymCode);
            return result;
        }

        public static OperationResult<int> AddReturnedItem(int branchId, ReturnItemDC returnedItem, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            result = BusinessLogic.ManageShop.ShopManager.AddReturnedItem(branchId, returnedItem, gymCode);
            return result;
        }

        public static OperationResult<int> EditReturnedItem(int branchId, ReturnItemDC editReturnedItem, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            result = BusinessLogic.ManageShop.ShopManager.EditReturnedItem(branchId, editReturnedItem, gymCode);
            return result;
        }
        #endregion

        public static OperationResult<List<ShopSalesItemDC>> GetDailySales(DateTime salesDate, int branchId, string gymCode)
        {
            OperationResult<List<ShopSalesItemDC>> result = new OperationResult<List<ShopSalesItemDC>>();
            result = BusinessLogic.ManageShop.ShopManager.GetDailySales(salesDate, branchId, gymCode);
            return result;
        }

        public static OperationResult<bool> AddEditUserHardwareProfile(int branchId, int hardwareProfileId, string user, string gymCode)
        {
            return BusinessLogic.ManageShop.ShopManager.AddEditUserHardwareProfile(branchId, hardwareProfileId, user, gymCode);
        }

        public static OperationResult<int> CheckEmployeeByEntNo(int entityNO, string gymCode)
        {
            return GMSMember.CheckEmployeeByEntNo(entityNO, gymCode);
        }

        public static OperationResult<List<SalePointDC>> GetSalesPointList(int branchID, string gymCode)
        {
            return BusinessLogic.ManageShop.ShopManager.GetSalesPointList(branchID, gymCode);
        }

        public static OperationResult<int> GetSelectedHardwareProfile(int branchId, string user, string gymCode)
        {
            return BusinessLogic.ManageShop.ShopManager.GetSelectedHardwareProfile(branchId, user, gymCode);
        }

        public static OperationResult<int> SavePointOfSale(int branchId, SalePointDC pointOfSale, string user, string gymCode)
        {
            return BusinessLogic.ManageShop.ShopManager.SavePointOfSale(branchId, pointOfSale, user, gymCode);
        }

        public static OperationResult<int> SaveWithdrawal(int branchId, WithdrawalDC withDrawal, string gymCode, string user)
        {
            return BusinessLogic.ManageShop.ShopManager.SaveWithdrawal(branchId, withDrawal, gymCode, user);
        }

        public static OperationResult<int> SaveOpenCashRegister(int branchId, CashRegisterDC cashRegister, string gymCode, string user)
        {
            return BusinessLogic.ManageShop.ShopManager.SaveOpenCashRegister(branchId, cashRegister, gymCode, user);
        }

        public static OperationResult<List<ShopSalesPaymentDC>> GetShopBillSummary(DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            return BusinessLogic.ManageShop.ShopManager.GetShopBillSummary(fromDate, toDate, branchId, gymCode);
        }

        public static OperationResult<ShopBillDetailDC> GetShopSaleBillDetail(int saleId, int branchId, string gymCode)
        {
            return BusinessLogic.ManageShop.ShopManager.GetShopSaleBillDetail(saleId, branchId, gymCode);
        }

        public static OperationResult<SalePointDC> GetSalesPointByMachineName(int branchID, string machineName, string gymCode)
        {
            return BusinessLogic.ManageShop.ShopManager.GetSalesPointByMachineName(branchID, machineName, gymCode);
        }
        public static OperationResult<List<WithdrawalDC>> GetWithdrawalByMemberId(DateTime startDate, DateTime endDate, WithdrawalTypes type, int memberId, string gymCode, string user)
        {
            return BusinessLogic.ManageShop.ShopManager.GetWithdrawalByMemberId(startDate, endDate, type, memberId, gymCode, user);
        }

        public static OperationResult<int> GetNextGiftVoucherNumber(string seqId, string subSeqId, string gymCode)
        {
            return BusinessLogic.ManageShop.ShopManager.GetNextGiftVoucherNumber(seqId, subSeqId, gymCode);
        }
        public static OperationResult<List<ShopSalesPaymentDC>> GetGiftVoucherSaleSummary(DateTime fromDate,
                                                                                          DateTime toDate,
                                                                                          int branchId, string gymCode)
        {
            return BusinessLogic.ManageShop.ShopManager.GetGiftVoucherSaleSummary(fromDate, toDate, branchId, gymCode);
        }

        public static OperationResult<int> ValidateGiftVoucherNumber(string voucherNumber,decimal payment, string gymCode, int branchID)
        {
            return BusinessLogic.ManageShop.ShopManager.ValidateGiftVoucherNumber(voucherNumber, payment, gymCode, branchID);
        }
        public static OperationResult<int> SaveGiftVoucherArticle(ArticleDC article, string gymCode)
        {
            return BusinessLogic.ManageShop.ShopManager.SaveGiftVoucherArticle(article, gymCode);
        }

        public static OperationResult<Dictionary<string, string>> GetGiftVoucherDetail(string gymCode)
        {
            return BusinessLogic.ManageShop.ShopManager.GetGiftVoucherDetail(gymCode);
        }

        public static OperationResult<bool> IsCashdrawerOpen(string machinename, string gymCode, string user)
        {
            return BusinessLogic.ManageShop.ShopManager.IsCashdrawerOpen(machinename, gymCode, user);
        }

        public static byte[] GetDailySettlementPrint(String dailySettlementId, String reconiliationId, String SalePointId, String branchId, String mode, String startDate, String endDate, String gymCode, String countedCashDraft)
        {
            byte[] res = new BusinessLogic.ManageShop.DailySettlementPDFPrint(dailySettlementId, reconiliationId, SalePointId, branchId, mode, startDate, endDate, gymCode, countedCashDraft).GetResult();
            return res;
        }

        public static int CheckIfReceiptIsValidForPrint(String invoiceNo, int branchId, String gymCode)
        {
            return BusinessLogic.ManageShop.ShopManager.CheckIfReceiptIsValidForPrint( invoiceNo,  branchId, gymCode);
        }

        public static OperationResult<List<InvoiceForReturnDC>> GetInvoicesForArticleReturn(int memberID, int branchId, string articleNo, int shopOnly, string gymCode, string user)
        {
            return BusinessLogic.ManageShop.ShopManager.GetInvoicesForArticleReturn(memberID, branchId, articleNo, shopOnly, gymCode, user);
        }
    }
}
