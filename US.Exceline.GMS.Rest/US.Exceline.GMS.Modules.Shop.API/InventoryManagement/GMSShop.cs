﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Admin;
using US.Exceline.GMS.Modules.Shop.BusinessLogic.InventoryManagement;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;
using System.Collections.ObjectModel;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.Exceline.GMS.Modules.Shop.API.InventoryManagement
{
    public class GMSShop
    {
        #region Add Shop Sales
        public static OperationResult<SaleResultDC> AddShopSales(String cardType, InstallmentDC installment, ShopSalesDC shopSaleDetails, PaymentDetailDC paymentDetails, int memberbranchId, int loggedBranchID, string user, string gymCode, bool isBookingPayment, string custRoleType, String sessionKey = "")
        {
            return ShopManager.AddShopSales(cardType, installment, shopSaleDetails, paymentDetails, memberbranchId, loggedBranchID, user, gymCode, isBookingPayment,custRoleType, sessionKey);
        }

        public static OperationResult<DailySettlementDC> GetDailySettlements(int dailySettlementId, string gymCode)
        {
            return ShopManager.GetDailySettlements(dailySettlementId, gymCode);
        }

        public static OperationResult<int> AddDailySettlements(DailySettlementDC dailysettlement, string gymCode)
        {
            return ShopManager.AddDailySettlements(dailysettlement, gymCode);
        }



        #endregion
    }
}
