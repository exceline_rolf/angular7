﻿using System;
using System.Collections.Generic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.GMS.Data.DataAdapters.ManageSystemSettings;

namespace US.GMS.BusinessLogic.ManageSystemSettings
{
    public class GymSettingManager
    {
        #region GetGymSettings
        public static OperationResult<string> GetGymSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                switch (gymSettingType)
                {
                    case GymSettingType.ACCESSTIME:
                        List<GymAccessSettingDC> gymAccessTimeSettings = GymSettingFacade.GetGymAccessSettings(branchId, gymCode, userName);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(gymAccessTimeSettings);
                        break;
                    case GymSettingType.GYMOPENTIME:
                        List<GymOpenTimeDC> gymOpenTimeList = GymSettingFacade.GetGymOpenTimes(branchId, gymCode, userName);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(gymOpenTimeList);
                        break;
                    case GymSettingType.REQUIRED:
                        List<GymRequiredSettingsDC> gymRequiredSettings = GymSettingFacade.GetGymRequiredSettings(branchId, gymCode, userName);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(gymRequiredSettings);
                        break;
                    case GymSettingType.MEMBERSEARCH:
                        List<GymMemberSearchSettingsDC> memberSearchSettings = GymSettingFacade.GetGymMemberSearchSettings(branchId, gymCode, userName);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(memberSearchSettings);
                        break;
                    case GymSettingType.HARDWARE:
                        List<ExceHardwareProfileDC> gymHardwareProfileSettings = GymSettingFacade.GetHardwareProfiles(branchId, gymCode, userName);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(gymHardwareProfileSettings);
                        break;
                    case GymSettingType.ECONOMY:
                        List<GymEconomySettingsDC> gymEconomySettings = GymSettingFacade.GetGymEconomySettings(branchId, gymCode, userName);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(gymEconomySettings);
                        break;
                    case GymSettingType.SMS:
                        List<GymSmsSettingsDC> gymSmsSettings = GymSettingFacade.GetGymSmsSettings(branchId, gymCode, userName);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(gymSmsSettings);
                        break;
                    case GymSettingType.OTHER:
                        List<GymOtherSettingsDC> otherSettings = GymSettingFacade.GetGymOtherSettings(branchId, gymCode, userName);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(otherSettings);
                        break;
                    case GymSettingType.INTEGRATION:
                        List<GymIntegrationSettingsDC> integrationSettings = GymSettingFacade.GetGymIntegrationSettings(branchId, gymCode);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(integrationSettings);
                        break;
                    case GymSettingType.EXCINTEGRATION:
                        List<GymIntegrationExcSettingsDC> integrationExcSettings = GymSettingFacade.GetGymIntegrationExcSettings(branchId, gymCode);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(integrationExcSettings);
                        break;
                    case GymSettingType.OTHERINTEGRATION:
                        OtherIntegrationSettingsDC otherintegrationSettings = GymSettingFacade.GetOtherIntegrationSettings(branchId, gymCode,systemName);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(otherintegrationSettings);
                        break;
                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting GymSetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<List<GymAccessSettingDC>> GetGymAccessTimeSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName)
        {
            OperationResult<List<GymAccessSettingDC>> result = new OperationResult<List<GymAccessSettingDC>>();
            try
            {
                result.OperationReturnValue = GymSettingFacade.GetGymAccessSettings(branchId, gymCode, userName);

            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in getting GymSetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }

        public static OperationResult<List<GymOpenTimeDC>> GetGymOpenTimes(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName)
        {
            OperationResult<List<GymOpenTimeDC>> result = new OperationResult<List<GymOpenTimeDC>>();
            try
            {
                result.OperationReturnValue = GymSettingFacade.GetGymOpenTimes(branchId, gymCode, userName);


            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in getting GymSetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }

        public static OperationResult<List<GymRequiredSettingsDC>> GetGymRequiredSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName)
        {
            OperationResult<List<GymRequiredSettingsDC>> result = new OperationResult<List<GymRequiredSettingsDC>>();
            try
            {
                result.OperationReturnValue = GymSettingFacade.GetGymRequiredSettings(branchId, gymCode, userName);


            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in getting GymSetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }

        public static OperationResult<List<GymMemberSearchSettingsDC>> GetGymMemberSearchSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName)
        {
            OperationResult<List<GymMemberSearchSettingsDC>> result = new OperationResult<List<GymMemberSearchSettingsDC>>();
            try
            {
                result.OperationReturnValue = GymSettingFacade.GetGymMemberSearchSettings(branchId, gymCode, userName);


            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in getting GymSetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }

        public static OperationResult<List<ExceHardwareProfileDC>> GetHardwareProfiles(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName)
        {
            OperationResult<List<ExceHardwareProfileDC>> result = new OperationResult<List<ExceHardwareProfileDC>>();
            try
            {
                result.OperationReturnValue = GymSettingFacade.GetHardwareProfiles(branchId, gymCode, userName);


            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in getting GymSetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }

        public static OperationResult<List<GymEconomySettingsDC>> GetGymEconomySettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName)
        {
            OperationResult<List<GymEconomySettingsDC>> result = new OperationResult<List<GymEconomySettingsDC>>();
            try
            {
                result.OperationReturnValue = GymSettingFacade.GetGymEconomySettings(branchId, gymCode, userName);


            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in getting GymSetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }

        public static OperationResult<List<GymSmsSettingsDC>> GetGymSmsSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName)
        {
            OperationResult<List<GymSmsSettingsDC>> result = new OperationResult<List<GymSmsSettingsDC>>();
            try
            {
                result.OperationReturnValue = GymSettingFacade.GetGymSmsSettings(branchId, gymCode, userName);


            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in getting GymSetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }

        public static OperationResult<List<GymOtherSettingsDC>> GetGymOtherSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName)
        {
            OperationResult<List<GymOtherSettingsDC>> result = new OperationResult<List<GymOtherSettingsDC>>();
            try
            {
                result.OperationReturnValue = GymSettingFacade.GetGymOtherSettings(branchId, gymCode, userName);


            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in getting GymSetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }

        public static OperationResult<List<GymIntegrationSettingsDC>> GetGymIntegrationSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName)
        {
            OperationResult<List<GymIntegrationSettingsDC>> result = new OperationResult<List<GymIntegrationSettingsDC>>();
            try
            {
                result.OperationReturnValue = GymSettingFacade.GetGymIntegrationSettings(branchId, gymCode);


            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in getting GymSetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }

        public static OperationResult<List<GymIntegrationExcSettingsDC>> GetGymIntegrationExcSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName)
        {
            OperationResult<List<GymIntegrationExcSettingsDC>> result = new OperationResult<List<GymIntegrationExcSettingsDC>>();
            try
            {
                result.OperationReturnValue = GymSettingFacade.GetGymIntegrationExcSettings(branchId, gymCode);


            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in getting GymSetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }

        public static OperationResult<OtherIntegrationSettingsDC> GetOtherIntegrationSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName)
        {
            OperationResult<OtherIntegrationSettingsDC> result = new OperationResult<OtherIntegrationSettingsDC>();
            try
            {
                result.OperationReturnValue = GymSettingFacade.GetOtherIntegrationSettings(branchId, gymCode, systemName);


            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in getting GymSetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }


        public static OperationResult<List<TerminalType>> GetTerminalTypes(string gymCode)
        {
            OperationResult<List<TerminalType>> result = new OperationResult<List<TerminalType>>();
            try
            {
                List<TerminalType> integrationSettings = GymSettingFacade.GetTerminalTypes(gymCode);
                result.OperationReturnValue = integrationSettings;
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Terminal Types" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteIntegrationSettingById(int id, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = GymSettingFacade.DeleteIntegrationSettingById(id, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in deleting integration setting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        #endregion

        #region SaveGymSettings
        public static OperationResult<bool> SaveGymSettings(int branchId, string settingsDetails, string gymCode, GymSettingType gymSettingType)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                switch (gymSettingType)
                {
                    case GymSettingType.ACCESSTIME:
                        GymAccessSettingDC accessTimeSettings = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(GymAccessSettingDC)) as GymAccessSettingDC;
                        result.OperationReturnValue = GymSettingFacade.SaveGymAccessSettings(branchId, accessTimeSettings, gymCode);
                        break;
                    case GymSettingType.GYMOPENTIME:
                        List<GymOpenTimeDC> gymOpenTimesList = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(List<GymOpenTimeDC>)) as List<GymOpenTimeDC>;
                        result.OperationReturnValue = GymSettingFacade.SaveGymOpenTimes(branchId, gymOpenTimesList, gymCode);
                        break;
                    case GymSettingType.REQUIRED:
                        GymRequiredSettingsDC requiredSettings = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(GymRequiredSettingsDC)) as GymRequiredSettingsDC;
                        result.OperationReturnValue = GymSettingFacade.SaveGymRequiredSettings(branchId, requiredSettings, gymCode);
                        break;
                    case GymSettingType.MEMBERSEARCH:
                        GymMemberSearchSettingsDC memberSearchSettings = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(GymMemberSearchSettingsDC)) as GymMemberSearchSettingsDC;
                        result.OperationReturnValue = GymSettingFacade.SaveGymMemberSearchSettings(branchId, memberSearchSettings, gymCode);
                        break;
                    case GymSettingType.HARDWARE:
                        List<ExceHardwareProfileDC> hardwareProfileSettingsList = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(List<ExceHardwareProfileDC>)) as List<ExceHardwareProfileDC>;
                        result.OperationReturnValue = GymSettingFacade.SaveHardwareProfile(branchId, hardwareProfileSettingsList, gymCode);
                        break;
                    case GymSettingType.ECONOMY:
                        GymEconomySettingsDC economySettings = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(GymEconomySettingsDC)) as GymEconomySettingsDC;
                        result.OperationReturnValue = GymSettingFacade.SaveGymEconomySettings(branchId, economySettings, gymCode);
                        break;
                    case GymSettingType.SMS:
                        GymSmsSettingsDC smsSettings = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(GymSmsSettingsDC)) as GymSmsSettingsDC;
                        result.OperationReturnValue = GymSettingFacade.SaveGymSmsSettings(branchId, smsSettings, gymCode);
                        break;
                    case GymSettingType.OTHER:
                        GymOtherSettingsDC otherSettings = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(GymOtherSettingsDC)) as GymOtherSettingsDC;
                        result.OperationReturnValue = GymSettingFacade.SaveGymOtherSettings(branchId, otherSettings, gymCode);
                        break;
                    case GymSettingType.INTEGRATION:
                        GymIntegrationSettingsDC integrationSettings = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(GymIntegrationSettingsDC)) as GymIntegrationSettingsDC;
                        result.OperationReturnValue = GymSettingFacade.SaveGymIntegrationSettings(branchId, integrationSettings, gymCode);
                        break;
                    case GymSettingType.EXCINTEGRATION:
                        GymIntegrationExcSettingsDC integrationExcSettings = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(GymIntegrationExcSettingsDC)) as GymIntegrationExcSettingsDC;
                        result.OperationReturnValue = GymSettingFacade.SaveGymIntegrationExcSettings(branchId, integrationExcSettings, gymCode);
                        break;
                    case GymSettingType.OTHERINTEGRATION:
                        OtherIntegrationSettingsDC otherIntegrationSettings = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(OtherIntegrationSettingsDC)) as OtherIntegrationSettingsDC;
                        result.OperationReturnValue = GymSettingFacade.AddUpdateOtherIntegrationSettings(branchId, otherIntegrationSettings, gymCode);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving Gym Settings" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion


        public static OperationResult<bool> SaveGymAccessSettings(int branchId, GymAccessSettingDC settingsDetails, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result.OperationReturnValue = GymSettingFacade.SaveGymAccessSettings(branchId, settingsDetails, gymCode);
            return result;
        }
        public static OperationResult<bool> SaveGymOpenTimes(int branchId, List<GymOpenTimeDC> settingsDetails, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result.OperationReturnValue = GymSettingFacade.SaveGymOpenTimes(branchId, settingsDetails, gymCode);
            return result;
        }
        public static OperationResult<bool> SaveGymRequiredSettings(int branchId, GymRequiredSettingsDC settingsDetails, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result.OperationReturnValue = GymSettingFacade.SaveGymRequiredSettings(branchId, settingsDetails, gymCode);
            return result;
        }
        public static OperationResult<bool> SaveGymMemberSearchSettings(int branchId, GymMemberSearchSettingsDC settingsDetails, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result.OperationReturnValue = GymSettingFacade.SaveGymMemberSearchSettings(branchId, settingsDetails, gymCode);
            return result;
        }
        public static OperationResult<bool> SaveHardwareProfile(int branchId, List<ExceHardwareProfileDC> settingsDetails, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result.OperationReturnValue = GymSettingFacade.SaveHardwareProfile(branchId, settingsDetails, gymCode);
            return result;
        }
        public static OperationResult<bool> SaveGymEconomySettings(int branchId, GymEconomySettingsDC settingsDetails, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result.OperationReturnValue = GymSettingFacade.SaveGymEconomySettings(branchId, settingsDetails, gymCode);
            return result;
        }
        public static OperationResult<bool> SaveGymSmsSettings(int branchId, GymSmsSettingsDC settingsDetails, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result.OperationReturnValue = GymSettingFacade.SaveGymSmsSettings(branchId, settingsDetails, gymCode);
            return result;
        }
        public static OperationResult<bool> SaveGymOtherSettings(int branchId, GymOtherSettingsDC settingsDetails, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result.OperationReturnValue = GymSettingFacade.SaveGymOtherSettings(branchId, settingsDetails, gymCode);
            return result;
        }
        public static OperationResult<bool> SaveGymIntegrationSettings(int branchId, GymIntegrationSettingsDC settingsDetails, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result.OperationReturnValue = GymSettingFacade.SaveGymIntegrationSettings(branchId, settingsDetails, gymCode);
            return result;
        }
        public static OperationResult<bool> SaveGymIntegrationExcSettings(int branchId, GymIntegrationExcSettingsDC settingsDetails, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result.OperationReturnValue = GymSettingFacade.SaveGymIntegrationExcSettings(branchId, settingsDetails, gymCode);
            return result;
        }
        public static OperationResult<bool> AddUpdateOtherIntegrationSettings(int branchId, OtherIntegrationSettingsDC settingsDetails, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result.OperationReturnValue = GymSettingFacade.AddUpdateOtherIntegrationSettings(branchId, settingsDetails, gymCode);
            return result;
        }


        #region GetSelectedGymSettings
        public static OperationResult<Dictionary<string, object>> GetSelectedGymSettings(List<string> settingColumnList, bool isGymSettings, int branchId, string gymCode)
        {
            OperationResult<Dictionary<string, object>> result = new OperationResult<Dictionary<string, object>>();
            try
            {
                result.OperationReturnValue = GymSettingFacade.GetSelectedGymSettings(settingColumnList, isGymSettings, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Selected Gym Settings" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

    }
}
