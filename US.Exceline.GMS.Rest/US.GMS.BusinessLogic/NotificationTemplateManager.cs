﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Data.DataAdapters;

namespace US.GMS.BusinessLogic
{
   public class NotificationTemplateManager
    {
       public static OperationResult<List<NotificationTemplateDC>> GetNotificationTemplatesByType(int templateTypeId, NotifyMethodType notifyMethod, int branchId, string gymCode)
       {
           OperationResult<List<NotificationTemplateDC>> result = new OperationResult<List<NotificationTemplateDC>>();
           try
           {
               result.OperationReturnValue = NotificationTemplateFacade.GetNotificationTemplatesByType(templateTypeId, notifyMethod, branchId, gymCode);
           }
           catch (Exception ex)
           {
               result.CreateMessage("Error in retriveing notification templates" + ex.Message, MessageTypes.ERROR);
           }
           return result;
       }
    }
}
