﻿using System;
using US.GMS.Core.ResultNotifications;
using US.GMS.Data.DataAdapters;

namespace US.GMS.BusinessLogic
{
    public class LoggManager
    {
        public static OperationResult<int> SaveShopXMLData(String xMLData, String sessionKey ,String gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = LoggFacade.SaveShopXMLData(xMLData, sessionKey, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in sheduling sponsor orders to invoice " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
