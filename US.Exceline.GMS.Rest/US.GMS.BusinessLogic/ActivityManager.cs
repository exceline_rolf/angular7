﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "6/28/2012 12:33:48 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.ResultNotifications;
using US.GMS.Data.DataAdapters;
using US.GMS.Core.SystemObjects;

namespace US.GMS.BusinessLogic
{
    public class ActivityManager
    {
        public static OperationResult<List<ActivityDC>> GetActivitiesForEntity(int entityId, string entityType, string gymCode, int branchId)
        {
            OperationResult<List<ActivityDC>> result = new OperationResult<List<ActivityDC>>();
            try
            {
                result.OperationReturnValue = ActivityFacade.GetActivitiesForEntity(entityId, entityType, gymCode,branchId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Activity Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
