﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.Data.DataAdapters;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ManageShop;

namespace US.GMS.BusinessLogic
{
    public class PaymentsManager
    {

        public static OperationResult<string> RegisterPayment(int branchId, int memberId, int articleNo, decimal paymentAmount, decimal discount, string createdUser, decimal invoiceAmount, PaymentTypes paymenttype, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = PaymentFacade.RegisterPayment(branchId, memberId, articleNo, paymentAmount, discount, createdUser, invoiceAmount, paymenttype, gymCode);
            }
            catch (Exception)
            {
                result.CreateMessage("Error on Registering Payments", MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<SaleResultDC> RegisterInstallmentPayment(int memberBranchID, int loggedbranchID, string user, Core.DomainObjects.ManageMemberships.InstallmentDC installment, PaymentDetailDC paymentDetail, string gymCode, int salePointID, ShopSalesDC salesDetails)
        {
            OperationResult<SaleResultDC> result = new OperationResult<SaleResultDC>();
            try
            {
                result.OperationReturnValue = PaymentFacade.RegisterInstallmentPayment(memberBranchID,loggedbranchID, user, installment, paymentDetail, gymCode, salePointID, salesDetails);
            }
            catch (Exception)
            {
                SaleResultDC saleResult = new SaleResultDC();
                saleResult.AritemNo = -1;
                saleResult.SaleStatus = SaleErrors.ERROR;
                result.OperationReturnValue = saleResult;
                result.CreateMessage("Error on Registering installent Payments", MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<SaleResultDC> GenerateInvoice(int invoiceBranchID, string user, Core.DomainObjects.ManageMemberships.InstallmentDC installment, PaymentDetailDC paymentDetail, string invoiceType, string gymCode, int memberBranchID)
        {
            OperationResult<SaleResultDC> result = new OperationResult<SaleResultDC>();
            try
            {
                result.OperationReturnValue = PaymentFacade.GenerateInvoice(invoiceBranchID, user, installment, paymentDetail, invoiceType, gymCode, memberBranchID);
            }
            catch (Exception)
            {
                SaleResultDC saleResult = new SaleResultDC();
                saleResult.AritemNo = -1;
                saleResult.SaleStatus = SaleErrors.ERROR;
                result.OperationReturnValue = saleResult;
                result.CreateMessage("Error on Generate Invoice", MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<SaleResultDC> RegisterInvoicePayment(PaymentDetailDC paymentDetails, string gymCode, int brnachId, ShopSalesDC salesDetails, string user)
        {
            OperationResult<SaleResultDC> result = new OperationResult<SaleResultDC>();
            try
            {
                result.OperationReturnValue = PaymentFacade.RegisterInvoicePayment(paymentDetails, gymCode, brnachId, salesDetails, user);
            }
            catch (Exception ex)
            {
                SaleResultDC saleResult = new SaleResultDC();
                saleResult.AritemNo = -1;
                saleResult.SaleStatus = SaleErrors.ERROR;
                result.OperationReturnValue = saleResult;
                result.CreateMessage("Error on RegisteAdding invoice Payments", MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> CancelPayment(int paymentID, bool keepthePayment, string user, string comment, string gymCode)
        {

            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = PaymentFacade.CancelPayment(paymentID, keepthePayment, user, comment, gymCode);
            }
            catch (Exception)
            {
                result.CreateMessage("Error on Cancelling Payments", MessageTypes.ERROR);
            }
            return result;
        }

       
    }
}
