﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Data.DataAdapters;

namespace US.GMS.BusinessLogic
{
    public class InvoiceManager
    {
        public static OperationResult<bool> ScheduleOrdersForInvoicing(int branchId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result.OperationReturnValue = InvoiceFacade.ScheduleOrdersForInvoicing(branchId, gymCode);
            return result;
        }

        public static OperationResult<bool> ScheduleSponsorOrdersForInvoicing(int branchId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = InvoiceFacade.ScheduleSponsorOrdersForInvoicing(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in sheduling sponsor orders to invoice " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<string> GetCreditorForBranch(int branchId, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = InvoiceFacade.GetCreditorForBranch(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Invoice details" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> CancelInvoice(int arItemNo, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = InvoiceFacade.CancelInvoice(arItemNo, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Cancelling invoices " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<OrderLineDC>> GetOrderLinesForAutoCreditNote(int branchId, int priority, string gymCode)
        {
            OperationResult<List<OrderLineDC>> result = new OperationResult<List<OrderLineDC>>();
            try
            {
                result.OperationReturnValue = InvoiceFacade.GetOrderLinesForAutoCreditNote(branchId, priority, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting OrderLines For Auto CreditNote " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<MemberInvoiceDC>> GetMemberInvoices(int memberID, int hit, string gymCode, string user)
        {
            OperationResult<List<MemberInvoiceDC>> result = new OperationResult<List<MemberInvoiceDC>>();
            try
            {
                result.OperationReturnValue = InvoiceFacade.GetMemberInvoices(memberID, hit, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting member invoices " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<ExcelineInvoiceDetailDC> GetInvoiceDetails(int arItemNo,int branchId, string gymCode)
        {
            OperationResult<ExcelineInvoiceDetailDC> result = new OperationResult<ExcelineInvoiceDetailDC>();
            try
            {
                result.OperationReturnValue = InvoiceFacade.GetInvoiceDetails(arItemNo,branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting  invoices details " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdateInvoice(int arItemNo, string comment, DateTime DueDate, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = InvoiceFacade.UpdateInvoice(arItemNo, comment, DueDate, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Updating the invoice arItem no: " + arItemNo.ToString() + " " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> AddCreditNote(CreditNoteDC creditNote, int branchId, bool isReturn, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = InvoiceFacade.AddCreditorNote(creditNote, branchId, isReturn,gymCode);
               
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in adding credit note" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> AddCreditNoteForBooking(CreditNoteDC creditNote,List<PayModeDC> payModeDcs , int branchId, bool isReturn, string gymCode)
        {
           var result = new OperationResult<int>();
           var invoice = GetInvoiceDetails(creditNote.ArItemNo, branchId, gymCode);
            if(invoice != null)
            {
                try
                {
                    int arItemNo = InvoiceFacade.AddCreditorNote(GetCreditorNote(invoice.OperationReturnValue, creditNote), branchId, isReturn, gymCode);
                   
                    if (arItemNo > 0)
                    {
                        result.OperationReturnValue = 1; //when save the credit note than return the 1
                        OperationResult<int> payCreditNoteresult = PayCreditNote(arItemNo, payModeDcs, creditNote.MemberId, creditNote.ShopAccoutId, gymCode, creditNote.User, creditNote.SalePointId, branchId);  
                        if (payCreditNoteresult.OperationReturnValue > 0)
                        {
                            result.OperationReturnValue = 2; //after succefully pay the credit not  than retun the 2
                        }
                    }
                        
                }
                catch (Exception ex)
                {
                    result.CreateMessage("Error in adding credit note for the booking" + ex.Message, MessageTypes.ERROR);
                }
            }
            return result;
        }

        private static CreditNoteDC GetCreditorNote(ExcelineInvoiceDetailDC invoiceDetail, CreditNoteDC creditNote)
        {
            creditNote.CustomerNo = invoiceDetail.CustId.ToString();
            creditNote.CreditorNumber = invoiceDetail.CreditorNo.ToString();
            creditNote.Kid = invoiceDetail.BasicDetails.KID;
            creditNote.InvoiceNo = invoiceDetail.InvoiceNo;
            creditNote.Amount = invoiceDetail.InvoiceAmount;
            creditNote.CaseNo = invoiceDetail.SubCaseNo;
            var creditNoteDetail = creditNote.Details.SingleOrDefault();
            if (creditNoteDetail != null)
            {
                var item = invoiceDetail.BasicDetails.ArticleList.FirstOrDefault(x => x.ArticleId.Equals(creditNoteDetail.ArticleId));
                if (item != null)
                {
                    creditNoteDetail.OrderlineId = item.Id;
                    creditNoteDetail.CreditedCount = item.Quantity;
                    creditNoteDetail.Amount = item.Price;
                }

            }
            
            return creditNote;
        }

        public static OperationResult<int> PayCreditNote(int arItemno, List<PayModeDC> paymodes, int memberId, int shopAccoutID, string gymCode, string user, int salePointId, int loggedbranchID)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = InvoiceFacade.PayCreditNote(arItemno, paymodes,memberId, shopAccoutID, gymCode, user, salePointId, loggedbranchID);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in adding credit payment" + ex.Message, MessageTypes.ERROR);
            }

            return result;
        }

        public static OperationResult<List<MemberInvoiceDC>> GetMemberInvoicesForContract(int memberID, int contractId, int hit, string gymCode, string user)
        {
            OperationResult<List<MemberInvoiceDC>> result = new OperationResult<List<MemberInvoiceDC>>();
            try
            {
                result.OperationReturnValue = InvoiceFacade.GetMemberInvoicesForContract(memberID,contractId, hit, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting member invoices " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

    }
}
