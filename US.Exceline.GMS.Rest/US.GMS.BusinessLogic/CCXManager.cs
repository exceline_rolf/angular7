﻿using System;
using US.USDF.Core.DomainObjects;
using US.GMS.Core.ResultNotifications;
using US.GMS.Data.DataAdapters;
using US.Payment.Core.BusinessDomainObjects;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;

namespace US.GMS.BusinessLogic
{
    public class CCXManager
    {
        public static OperationResult<USDFInvoiceInfo> GetAllInvoices(int chunkSize, string gymCode, int creditorNo, int batchsequenceNo, List<string> invoiceTypes)
        {
            OperationResult<USDFInvoiceInfo> result = new OperationResult<USDFInvoiceInfo>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.GetAllInvoices(chunkSize, gymCode, creditorNo, batchsequenceNo, invoiceTypes);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static OperationResult<USDFInvoiceInfo> GetDebtWarningInvoices(int chunkSize, string gymCode, int creditorNo, int batchsequenceNo)
        {
            OperationResult<USDFInvoiceInfo> result = new OperationResult<USDFInvoiceInfo>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.GetDebtWarningInvoices(chunkSize, gymCode, creditorNo, batchsequenceNo);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }



        public static OperationResult<USDFInvoiceInfo> GetDirectPayments(int chunkSize, string gymCode, int creditorNo, int batchSequenceNo)
        {
            OperationResult<USDFInvoiceInfo> result = new OperationResult<USDFInvoiceInfo>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.GetDirectPayments(chunkSize, gymCode, creditorNo, batchSequenceNo);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }



        public static OperationResult<USDFInvoiceInfo> GetInvoices(DateTime startDate, DateTime endDate, int chunkSize)
        {
            return null;
        }

        public static OperationResult<string> GetGymCodeForCreditor(int creditorNo)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.GetGymCodeForCreditor(creditorNo);
            }
            catch (Exception )
            {
                throw;
            }
            return result;
        }

        public static OperationResult<int> AddPaymentStatus(USPPaymentStatusFileRow paymentStatus, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.AddPaymentStatus(paymentStatus, gymCode);
            }
            catch (Exception )
            {
                throw;
            }
            return result;
        }

        public static OperationResult<int> AddNewCancelledStatus(US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPNewOrCancelledFileRow newCancelledStatus, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.AddNewCancelledStatus(newCancelledStatus, gymCode);
            }
            catch (Exception )
            {
                throw;
            }
            return result;
        }

        public static OperationResult<int> AddDataImportReceipt(DataImportReceiptDetail dataImportReceiptDetail, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.AddDataImportReceipt(dataImportReceiptDetail, gymCode);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }


        public static OperationResult<int> ImportDataImportReceipt( string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.ImportDataImportReceipt(gymCode);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static OperationResult<int> ImportNewCancelledStatus(string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.ImportNewCancelledStatus(gymCode);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static OperationResult<int> ImportPaymentStatus(string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.ImportPaymentStatus(gymCode);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static OperationResult<int> UpdateDWStatus(string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.UpdateDWStatus(gymCode);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static OperationResult<int> UpdateTemplateStatus(string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.UpdateTemplateStatus(gymCode);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static OperationResult<List<GymCompanyDC>> GetGymCompanies()
        {
            OperationResult<List<GymCompanyDC>> result = new OperationResult<List<GymCompanyDC>>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.GetGymCompanies();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static OperationResult<bool> UpdateExportStatus(string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.UpdateExportStatus(gymCode);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static OperationResult<string> GetClaimStatus(string gymCode, int hit, int status, DateTime date, ImportStatusDC advancedData)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                List<ImportStatusDC> statusList = new List<ImportStatusDC>();
                statusList = CCXCareFacade.GetClaimStatus(gymCode, hit, status, date, advancedData);
                result.OperationReturnValue = US.GMS.Core.Utils.XMLUtils.SerializeDataContractObjectToXML(statusList);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static OperationResult<bool> ReScheduleClaim(string gymCode, int itemNo)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.ReScheduleClaim(gymCode, itemNo);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static OperationResult<string> GetPaymentImportStatusXML(string gymCode, int hit, DateTime regDate)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                PaymentImportStatusDC status = new PaymentImportStatusDC();
                status = CCXCareFacade.GetPaymentImportStatus(gymCode, hit, regDate);
                result.OperationReturnValue = US.GMS.Core.Utils.XMLUtils.SerializeDataContractObjectToXML(status);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static OperationResult<List<ImportStatusDC>> GetClaimExportStatusForMember(string gymCode, int memberID, int hit,DateTime? transferDate, string type, string user)
        {
            OperationResult<List<ImportStatusDC>> result = new OperationResult<List<ImportStatusDC>>();
            try
            {
                List<ImportStatusDC> statusList = new List<ImportStatusDC>();
                statusList = CCXCareFacade.GetClaimExportStatusForMember(gymCode, memberID, hit, transferDate, type, user);
                result.OperationReturnValue = statusList; // US.GMS.Core.Utils.XMLUtils.SerializeDataContractObjectToXML(statusList);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static OperationResult<bool> MoveToShopAccount(int statusID, bool isImportStatus, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = CCXCareFacade.MoveToShopAccount(statusID, isImportStatus, gymCode);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static void UpdateAddress(List<USPAddress> address)
        {

        }
    }
}
