﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Data;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using System.Linq;
using System.Xml.Linq;
using System.Security.Cryptography;
using US.GMS.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class ExcelineInvoiceGenerator
    {
        static public String XMLData = String.Empty;
        static private byte[] xmlHash = null; 
        static public String output;
        static public String documentType;
        static public String VoucherId;
        static public String docId;
        static public Stream XMLStream = new MemoryStream();
        static public Byte[] result;

        static public String Getoutput()
        {
            return output;
        }

        static public void Setoutput(String input)
        {
            output = input;
        }

        public Byte[] GetResult()
        {
            return result;
        }

        static public void SetResult(Byte[] input)
        {
            result = input;
        }

        public ExcelineInvoiceGenerator(String fktnum,int branchid, String gymcode) {

            //Step1 Check if Invoice is generated previously

            InvoiceXMLData ExcistingXML = EconomyFacade.GetInvoicePrintRecord(fktnum, branchid, gymcode);

            //Retrieve the Data:
   
            InvoiceInfo invoiceInfo = EconomyFacade.GetInformationForInvoicePrint(fktnum, branchid, gymcode);

            if (invoiceInfo.CustomerId == null)
            {
                SetResult(Encoding.UTF8.GetBytes("NOTFOUND"));
                return;
            }
        
            // Generate XML Data:
            int orderlinecounter = 1;

            XElement CustomerInfo = 
                    new XElement("CustomerInfo",
                        new XElement("CustomerId", invoiceInfo.CustomerId),
                        new XElement("CustomerName", invoiceInfo.PayerName),
                        new XElement("CustomerAdress", invoiceInfo.PayerAdress),
                        new XElement("CustomerZipCode", invoiceInfo.PayerPostNumber),
                        new XElement("CustomerZipName", invoiceInfo.PayerPostPlace),
                        new XElement("ATGEnabled", invoiceInfo.IsATG),
                        new XElement("KID", invoiceInfo.KID),
                        new XElement("CustomerRefrence", invoiceInfo.CustomerRefrence),
                        new XElement("EmployeeNo", invoiceInfo.EmployeeNo),
                        new XElement("PaidOnBehalfOfCustId", invoiceInfo.PaidOnBehalfOfCustId),
                        new XElement("PaidOnBehalfOfName", invoiceInfo.PaidOnBehalfOfName)); 

            XElement GymInfo = new XElement("GymInfo",
                        new XElement("GymOrganizationNumber", invoiceInfo.SenderInfo.SenderOrganiztionNumber),
                        new XElement("GymAccountNumber", invoiceInfo.ReceiverAccountNumber),
                        new XElement("GymName", invoiceInfo.SenderInfo.SenderName),
                        new XElement("GymAdress", invoiceInfo.SenderInfo.SenderAdress),
                        new XElement("GymZipCode", invoiceInfo.SenderInfo.SenderZipCode),
                        new XElement("GymZipName", invoiceInfo.SenderInfo.SenderZipName),
                        new XElement("GymWeb", invoiceInfo.SenderInfo.SenderWebSite),
                        new XElement("GymPhoneNumber", invoiceInfo.SenderInfo.SenderPhoneNumber));

            List<XElement> iterator = new List<XElement>();
          
            
            for (var i = 0; i < invoiceInfo.OrderLines.Count; i++) {
                iterator.Add(
                          new XElement("OrderLine_" + orderlinecounter++.ToString(),
                                    new XElement("InvoiceNumber", invoiceInfo.InvoiceNumber),
                                    new XElement("ArticleDescription", invoiceInfo.OrderLines[i].ArticleDescription),
                                    new XElement("InvoicePeriod", invoiceInfo.OrderLines[i].PeriodSpan), 
                                    new XElement("NumberOfUnits", invoiceInfo.OrderLines[i].NumberOfUnits),
                                    new XElement("UnitPrice", invoiceInfo.OrderLines[i].UnitPrice),
                                    new XElement("VATRate", invoiceInfo.OrderLines[i].VATRate),
                                    new XElement("InvoiceGeneratedDate", invoiceInfo.InvoiceDate), 
                                    new XElement("DueDate", String.Empty),
                                    new XElement("VATAddition", invoiceInfo.OrderLines[i].VATAddition),
                                    new XElement("UnitPriceNoVAT", invoiceInfo.OrderLines[i].UnitPriceNoVAT),
                                    new XElement("TotalAmountNoVAT", invoiceInfo.OrderLines[i].TotalPriceNoVAT),
                                    new XElement("TotalAmount", invoiceInfo.OrderLines[i].TotalPrice),
                                    new XElement("Note", invoiceInfo.OrderLines[i].Note)));

            }

            var xmldoc = new XDocument(new XElement("InvoiceData",
                CustomerInfo,
                GymInfo,
                 new XElement("InvoiceOrderLines", from line in iterator select line)));

            XMLData = xmldoc.ToString(SaveOptions.DisableFormatting);

            //save the XML and a hash of the xml data to db before continue
            
            using (SHA512 sha512hash = SHA512.Create())
            {
                xmlHash = sha512hash.ComputeHash(Encoding.UTF8.GetBytes(XMLData));
            }
         
            if (ExcistingXML.hashValue != String.Empty && ( ExcistingXML.hashValue != Convert.ToBase64String(xmlHash)))
            {
                using (MemoryStream ms = new MemoryStream())
                {

                    EconomyFacade.InsertInvoicePrintDiscrepancyInstance( DateTime.Now, fktnum, ExcistingXML.InvoiceXML, XMLData, gymcode);

                    XMLData = ExcistingXML.InvoiceXML.Replace("<DueDate></DueDate>", "<DueDate>" + invoiceInfo.PaidByDate +  "</DueDate>");

                    Setoutput(new XMLtoHTMLConverter(XMLData, PdfTemplatesTypes.INVOICE_EXCELINE, gymcode, fktnum.Contains('-')).GetResultHTMLText());
                    var pdf = PdfGenerator.GeneratePdf(Getoutput(), PdfSharp.PageSize.A4);
                    pdf.Save(ms);
                 
                    SetResult(ms.ToArray());
                }
                
                return;
            }

            if(ExcistingXML.InvoiceXML == String.Empty)
            {
               EconomyFacade.InsertInvoicePrintRecord(branchid, fktnum, XMLData, Convert.ToBase64String(xmlHash), gymcode);
            }

            iterator.Select(c => { c.Element("DueDate").Value = invoiceInfo.PaidByDate; return c; }).ToList();


            xmldoc = new XDocument(new XElement("InvoiceData",
                CustomerInfo,
                GymInfo,
                 new XElement("InvoiceOrderLines", from line in iterator select line)));

            XMLData = xmldoc.ToString(SaveOptions.DisableFormatting);

            Setoutput(new XMLtoHTMLConverter(XMLData, PdfTemplatesTypes.INVOICE_EXCELINE, gymcode, fktnum.Contains('-')).GetResultHTMLText());
            
            Byte[] res = null;
            
            using (MemoryStream ms = new MemoryStream())
            {

                    var pdf = PdfGenerator.GeneratePdf(Getoutput(), PdfSharp.PageSize.A4);
                    pdf.Save(ms);
                    res = ms.ToArray();
                    SetResult(res);
            }
       
        }
    }
}
