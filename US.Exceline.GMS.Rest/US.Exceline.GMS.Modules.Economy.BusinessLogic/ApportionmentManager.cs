﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.Exceline.GMS.Modules.Economy.Data;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class ApportionmentManager
    {
        public static OperationResult<List<USPApportionmentTransaction>> GetInitialApportionmentData(string KID, string creditorNo, decimal amount, int ARNo, int caseNo, int paymentID, string paymentType, string gymCode)
        {
            OperationResult<List<USPApportionmentTransaction>> result = new OperationResult<List<USPApportionmentTransaction>>();
            try
            {
                result.OperationReturnValue = EconomyFacade.GetInitialApportionmentData(KID, creditorNo, amount, ARNo, caseNo, paymentID, paymentType, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Initial Apportionment Data . " + ex.Message, MessageTypes.ERROR);
                result.ErrorOccured = true;
                
            }
            return result;
        }


        public static OperationResult<ReverseConditions> ReverseApportionments(int paymentId, int arItem, string actionStatus, string user, string dbCode)
        {
            OperationResult<ReverseConditions> result = new OperationResult<ReverseConditions>();
            try
            {
                result.OperationReturnValue = EconomyFacade.ReverseApportionments(paymentId, arItem, actionStatus, user, dbCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Reverse Apportionments . " + ex.Message, MessageTypes.ERROR);
                result.ErrorOccured = true;
            }
            return result;
        }

        public static OperationResult<int> AddNotApportionmentTransactions(int ARNo, string KID, int errorPayment, string creditoNo, decimal mainAmount, int itemTypeID, List<USPApportionmentTransaction> transactionList, string user, string dbCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = EconomyFacade.AddNotApportionmentTransactions(ARNo, KID, errorPayment, creditoNo, mainAmount, itemTypeID, transactionList, user, dbCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Add Apportionment Transactions . " + ex.Message, MessageTypes.ERROR);
                result.ErrorOccured = true;
            }
            return result;
        }
    }
}
