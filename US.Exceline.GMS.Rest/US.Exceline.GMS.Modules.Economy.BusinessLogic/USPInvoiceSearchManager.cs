﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Data;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class USPInvoiceSearchManager
    {
        public static OperationResult<List<USPARItem>> AdvanaceInvoiceSearchXML(InvoiceSearchCriteria criteria, SearchModes searchMode, object constValue, string gymCode)
        {
            OperationResult<List<USPARItem>> result = new OperationResult<List<USPARItem>>();
            try
            {
                List<USPARItem> arItemList = InvoiceFacade.AdvanceInvoiceSearch(criteria, searchMode, constValue, gymCode);
                result.OperationReturnValue = arItemList;
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in SeachInvoices. " + ex.Message, MessageTypes.ERROR);
            }

            return result;
        }

        public static OperationResult<List<USPARItem>> SearchGeneralInvoiceDataXML(string searchValue, int invoiceType, string fieldType, SearchModes searchMode, object constValue, string gymCode, int paymentId)
        {
            OperationResult<List<USPARItem>> result = new OperationResult<List<USPARItem>>();
            try
            {
                int suggestion = 0;
                string fieldOne = string.Empty;
                string fieldTwo = string.Empty;
                if (string.IsNullOrEmpty(searchValue))
                {
                    suggestion = 4;
                }
                else
                {

                    string[] seachParts = searchValue.Trim().Split(new char[] { ' ' }, 2);


                    if (seachParts.Length > 1)
                    {
                        suggestion = InvoiceSearchHandler.GetSuggestion(seachParts[0], seachParts[1]);
                        fieldOne = seachParts[0];
                        fieldTwo = seachParts[1];
                    }
                    else
                    {
                        suggestion = InvoiceSearchHandler.GetSuggestion(seachParts[0], string.Empty);
                        fieldOne = seachParts[0];
                        fieldTwo = string.Empty;
                    }
                }
                List<USPARItem> aritemList = InvoiceFacade.GeneralInvoiceSearch(fieldOne, fieldTwo, suggestion, invoiceType, fieldType, searchMode, constValue, gymCode, paymentId);
                result.OperationReturnValue = aritemList;
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in SearchGeneralInvoiceData. " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
