﻿using System;
using System.Collections.Generic;
using System.IO;
using WebSupergoo.ABCpdf8;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class PDFManager
    {
        public string MergePDF(List<string> pdfPathList, string newFilePath)
        {
            try
            {
                var pdfRoot = Path.GetDirectoryName(newFilePath);
                if (pdfRoot != null && !Directory.Exists(pdfRoot))
                    Directory.CreateDirectory(pdfRoot);

                var newPDF = new Doc();

                foreach (var pdfPath in pdfPathList)
                {
                    var tempPDF = new Doc();
                    tempPDF.Read(pdfPath);
                    newPDF.Append(tempPDF);
                    tempPDF.Clear();
                }
                newPDF.Save(newFilePath);
                newPDF.Clear();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return newFilePath;
        }
    }
}
