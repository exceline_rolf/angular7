﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Data;
using CSharp_easy_RSA_PEM;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{ 

public class KeyHandlerForKasselov
{
    private RSACryptoServiceProvider myRSA;
    private string publicXMLKey;
    private string privateXMLKey;
    private string publicPEMKeyStr;
    private string privatePEMKeyStr;
    private byte[] signedData;
    private string xmlstring;

    public KeyHandlerForKasselov()
	{
     
    }

    public void createKey() {

        this.myRSA = new RSACryptoServiceProvider();
    }

    public void importKey(String gymCode) {
        List<KeyData> templist = EconomyFacade.GetKeyFromDB(gymCode);
        this.xmlstring = templist[0].PrivateKey;

    }

    public void loadKey() {
        this.myRSA.FromXmlString(this.xmlstring);
    }

    public void exportKeyAsXML() {
        this.publicXMLKey = this.myRSA.ToXmlString(false);
        this.privateXMLKey = this.myRSA.ToXmlString(true);
    }

    public void exportKeyAsPEM() {
        this.publicPEMKeyStr = Crypto.ExportPublicKeyToX509PEM(this.myRSA);
        this.privatePEMKeyStr = Crypto.ExportPrivateKeyToRSAPEM(this.myRSA);
    }

    public void signData(byte[] hashedData)
    {
            this.signedData = myRSA.SignHash(hashedData, "SHA1"); //HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);
    }
       
    public string getSignedData()
    {
        return Convert.ToBase64String(this.signedData);
    }

    public String getPublicPEMKey()
    {
        return this.publicPEMKeyStr;
    }
    
}

}
