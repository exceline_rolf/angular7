﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;
using US.Exceline.GMS.Modules.Economy.Data;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class OCRValidationRecordCollectionByCreditor
    {
        public List<OCRValidationRecord> ValidationRecordsList { get; set; }
        private string _gymCode = string.Empty;
        public OCRValidationRecordCollectionByCreditor(string creditorAccountNo, string gymCode)
        {
            CreditorAccountNo = creditorAccountNo;
            _gymCode = gymCode;
            ValidationRecordsList = new List<OCRValidationRecord>();
        }

        public string CreditorAccountNo { get; set; }

        public void AddRecord(OCRValidationRecord validationRecord)
        {
            ValidationRecordsList.Add(validationRecord);
        }

        public int WriteValidationLog(int fileID)
        {
            int savedRecordCount = 0;
            // Following LINQ query calculate and select data that are need to be inserted in to the database
            var recordsToBeWriteByCreditorAccountNos = from rec in ValidationRecordsList
                                                       // Group list items by record type
                                                       group rec by rec.ValidationRecordType into recordsCategerizedByRecordType
                                                       select new
                                                       {
                                                           // Select record type
                                                           RecordType = recordsCategerizedByRecordType.Key
                                                           ,
                                                           // Calculate sum of grouped (by record type) items
                                                           Sum = recordsCategerizedByRecordType.Sum(rec => rec.Amount)
                                                           ,
                                                           // Calculate line count of grouped (by record type) items
                                                           NumberOfRows = recordsCategerizedByRecordType.Count()
                                                           ,
                                                           // Select first non critical error message of the lis
                                                           NonCriticalErrorMsg = recordsCategerizedByRecordType.GetVeryFirstNonCriticalErrorMsg()
                                                       };
            foreach (var recordToBeWriten in recordsToBeWriteByCreditorAccountNos)
            {
                try
                {
                    savedRecordCount += OCRFacade.AddFileValidationSatatesRecord(CreditorAccountNo, fileID, recordToBeWriten.NumberOfRows,
                        recordToBeWriten.Sum, recordToBeWriten.RecordType.ToString(), recordToBeWriten.NonCriticalErrorMsg, string.Empty, "USPaymentImportTask", _gymCode);
                }
                catch
                {
                    throw;
                }
            }
            return savedRecordCount;
        }
    }
}
