﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Data;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class ReminderManager
    {
        public static OperationResult<bool> SendReminders(string GymCode, int branchID)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = EconomyFacade.SendReminders(GymCode, branchID);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Sending reminderes. " + ex.Message, MessageTypes.ERROR);
                result.ErrorOccured = true;
            }
            return result;
        }
    }
}
