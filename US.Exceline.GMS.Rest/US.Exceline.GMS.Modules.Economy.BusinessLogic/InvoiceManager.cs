﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Ccx;
using US.Exceline.GMS.Modules.Economy.Data;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class InvoiceManager
    {
        /// <summary>
        /// This method is called when orders are invoiced from system manually
        /// </summary>
        /// <param name="branchID"></param>
        /// <param name="startDueDate"></param>
        /// <param name="endDueDate"></param>
        /// <param name="orderType"></param>
        /// <param name="gymCode"></param>
        /// <returns></returns>
        public static OperationResult<string> GenerateInvoices(int branchID, DateTime? startDueDate, DateTime? endDueDate, string orderType, string user, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                //Get Installments 
                List<InvoicingOrder> orderIds = new List<InvoicingOrder>();

                //generate invoices
                orderIds = InvoiceFacade.GetOrdersforInvoice(branchID, startDueDate, endDueDate, orderType, gymCode);
                return GenerateInvoiceswithOrders(orderIds, "", "E", user, gymCode, branchID);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Orders " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        /// <summary>
        /// this method called when task generate invoices for gyms 
        /// </summary>
        /// <param name="branchID"></param>
        /// <param name="creditorNo"></param>
        /// <param name="invoiceKey"></param>
        /// <param name="orderType"></param>
        /// <param name="gymCode"></param>
        /// <returns></returns>
        public static OperationResult<string> GenerateInvoicesforBranch(int branchID, string invoiceKey, string orderType, string user, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                //Get Installments 
                List<InvoicingOrder> orderIds = new List<InvoicingOrder>();
                orderIds = InvoiceFacade.GetOrdersforInvoice(branchID, null, null, orderType, gymCode);
                //generate invoices
                return GenerateInvoiceswithOrders(orderIds, "", "", user, gymCode, branchID);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Orders " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        private static OperationResult<string> GenerateInvoiceswithOrders(List<InvoicingOrder> orderIds, string invoiceKey, string orderType, string user, string gymCode, int branchID)
        {
            return InvoiceFacade.GenerateInvoiceswithOrders(orderIds, invoiceKey, orderType, user, gymCode, branchID);
        }

        public static OperationResult<List<EstimatedInvoiceDetails>> GetEstimatedInvoiceDetails(List<int> gymIds, DateTime? startDueDate, DateTime? endDueDate, string gymCode)
        {
            OperationResult<List<EstimatedInvoiceDetails>> result = new OperationResult<List<EstimatedInvoiceDetails>>();
            List<EstimatedInvoiceDetails> invoiceEstimationList = new List<EstimatedInvoiceDetails>();

            try
            {
                for (int i = 0; i < gymIds.Count; i++)
                {
                    invoiceEstimationList.Add(InvoiceFacade.GetEstimatedInvoiceDetails(gymIds[i], startDueDate, endDueDate, gymCode));
                }

                result.OperationReturnValue = invoiceEstimationList;
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting invoice Estimations " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<EstimatedInvoiceDetails>> GetInvoicedAmounts(List<int> gymIds, DateTime? startDueDate, DateTime? endDueDate, string gymCode)
        {
            OperationResult<List<EstimatedInvoiceDetails>> result = new OperationResult<List<EstimatedInvoiceDetails>>();
            List<EstimatedInvoiceDetails> invoiceEstimationList = new List<EstimatedInvoiceDetails>();
            try
            {
                for (int i = 0; i < gymIds.Count; i++)
                {
                    invoiceEstimationList.Add(InvoiceFacade.GetInvoicedAmounts(gymIds[i], startDueDate, endDueDate, gymCode));
                }
                result.OperationReturnValue = invoiceEstimationList;
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting invoice Estimations " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> AddCreditorOrderLine(CreditorOrderLine OrderLine, string user, DbTransaction transaction)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = InvoiceFacade.AddCreditorOrderLine(OrderLine, user, transaction);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Add Creditor Order lines  . " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<IUSPClaim>> SelectedOrderLineInvoiceGenerator(List<int> creditorOrdeLineIdList, string invoiceKey, string orderType, string user, DbTransaction transaction, int istallmentID, int creditorNo, string gymCode)
        {
            OperationResult<List<IUSPClaim>> result = new OperationResult<List<IUSPClaim>>();
            try
            {
                result = InvoiceFacade.SelectedOrderLineInvoiceGenerator(creditorOrdeLineIdList, invoiceKey, orderType, user, transaction, istallmentID, creditorNo, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Add invoice from orderlines . " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<InvoiceDC>> GetInvoiceList(string category, DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            var result = new OperationResult<List<InvoiceDC>>();
            try
            {
                result.OperationReturnValue = InvoiceFacade.GetInvoiceList(category, fromDate, toDate, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Get Invoice List: " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static bool UpdatePDFFileParth(int id, string fileParth, int branchId, string gymCode, string user)
        {
            try
            {
                return InvoiceFacade.UpdatePDFFileParth(id, fileParth, branchId, gymCode, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string MergePDFFile(List<string> pdfPathList, string newPDFPath)
        {
            try
            {
                var pdfManager = new PDFManager();
                return pdfManager.MergePDF(pdfPathList, newPDFPath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int UpdateHistoryPDFPrintProcess(int id, int invoiceCount, int FailCount, int branchId, string user, string gymCode, string filePath)
        {
            try
            {
                return InvoiceFacade.UpdateHistoryPDFPrintProcess(id, invoiceCount, FailCount, branchId, user, gymCode, filePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<HistoryPDFPrint> GetHistoryPDFPrintDetail(DateTime fromDate, DateTime toDate, string gymCode)
        {
            try
            {
                return InvoiceFacade.GetHistoryPDFPrintDetail(fromDate, toDate, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int UpdateBulkPDFPrintDetails(int id, string docType, string dataIdList, string pdfFilePath, int noOfDoc, int failCount, int branchId, string user, string gymCode)
        {
            try
            {
                return InvoiceFacade.UpdateBulkPDFPrintDetails(id, docType, dataIdList, pdfFilePath, noOfDoc, failCount, branchId, user, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<BulkPDFPrint> GetBulkPDFPrintDetailList(string type, DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            try
            {
                return InvoiceFacade.GetBulkPDFPrintDetailList(type, fromDate, toDate, branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ServiceResponse UpdateCcxInvoice(List<Invoice> invoices)
        {
            var response = new ServiceResponse();
          
            try
            {
               var result= InvoiceFacade.UpdateCcxInvoice(invoices);
                if (result == 1)
                {
                    response.Status = 1;
                }
                else
                {
                    response.Status = 0;
                    response.ErrorMessage = "update failed";
                }
               
            }
            catch (Exception ex)
            {
                response.Status = 1;
                response.ErrorMessage = ex.Message;
            }

            return response;
            
        }

        public static List<Invoice> GetInvoice(int batchId)
        {
            try
            {
                return InvoiceFacade.GetCcxInvoiceDetail(batchId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
    }
}
