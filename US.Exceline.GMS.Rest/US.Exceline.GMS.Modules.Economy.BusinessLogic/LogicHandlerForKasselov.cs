﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Ccx;
using US.Exceline.GMS.Modules.Economy.Data;
using US.GMS.Core.ResultNotifications;
using System.Security.Cryptography;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class LogicHandlerForKasselov
    {
        public string fetchPublicKeyForSalePoint(String gymCode) {
            KeyHandlerForKasselov Key = new KeyHandlerForKasselov();
            Key.createKey();
            Key.importKey(gymCode);
            Key.loadKey();
            Key.exportKeyAsPEM();
            return Key.getPublicPEMKey();
        }

        public string signTransaction(byte[] hashedtransaction, String gymCode) {
            KeyHandlerForKasselov Key = new KeyHandlerForKasselov();
            Key.createKey();
            Key.importKey(gymCode);
            Key.loadKey();
            Key.signData(hashedtransaction);
            return Key.getSignedData();
        }

     
    }

}
