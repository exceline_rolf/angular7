﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace SessionService
{
    [ServiceContract]
    interface ISessionService
    {
        [OperationContract]
        int AddSessionKeyValue(string sessionKey, string sessionValue, string gymCode, int branchId = -1 , int salePointId = -1);

        [OperationContract]
        string GetSessionKeyValue(string sessionKey, string user);
    }
}
