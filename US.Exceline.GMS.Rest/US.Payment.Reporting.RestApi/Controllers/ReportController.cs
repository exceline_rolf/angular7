﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using US.Common.Logging.API;
using US.Common.Web.UI.Core;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.Payment.Reporting.Core.DomainObjects;
using US.Payment.Reporting.RestApi.Models;

namespace US.Payment.Reporting.RestApi.Controllers
{
    [RoutePrefix("api/ExcelineRapport")]
    public class ReportController : ApiController
    {
        [HttpGet]
        [Route("Test")]
        public HttpResponseMessage Test()
        {
           
          return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse("testimg message", ApiResponseStatus.ERROR,new List<string>()));
           
        }



        [HttpGet]
        [Route("ViewAllReports")]
        [Authorize]
        public HttpResponseMessage ViewAllReports()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                USLogEvent.WriteToFile("Report : ViewAllReports initiated.", user);
                var result = API.Reporting.ViewAllReports(user);
        
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<USReport>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetAllEntitiesWithProperties")]
        [Authorize]
        public HttpResponseMessage GetAllEntitiesWithProperties()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                USLogEvent.WriteToFile("Report : GetAllEntitiesWithProperties initiated.", user);            
                var result = API.Reporting.GetAllEntitiesWithProperties(user);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ReportEntity>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("RapportQuery")]
        [Authorize]
        public HttpResponseMessage TestQuery(TestQuery testQuery)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                USLogEvent.WriteToFile("Report : TestQuery initiated.", user);           
                var result = API.Reporting.TestQuery(testQuery.query, user);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<string>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetReportById")]
        [Authorize]
        public HttpResponseMessage GetReportById(int reportId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                USLogEvent.WriteToFile("Report : GetReportById initiated.", user);
                var result = API.Reporting.GetReportById(reportId, user);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new USReport(), ApiResponseStatus.ERROR, exceptionMsg));
            }



        }
        [HttpGet]
        [Route("GetScheduleListByReportId")]
        [Authorize]
        public HttpResponseMessage GetScheduleListByReportId(int reportId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                USLogEvent.WriteToFile("Report : GetScheduleListByReportId intiated.", user);
                var result = API.Reporting.GetScheduleListByReportId(reportId, user);           
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<string>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetExcelineReportServerPath")]
        [Authorize]
        public HttpResponseMessage GetExcelineReportServerPath()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = ConfigurationManager.AppSettings["ExcelineServerPathForReportView"].Trim();
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("DeleteReport")]
        [Authorize]
        public HttpResponseMessage DeleteReport(DeleteReport delReport)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
              

                var result = API.Reporting.DeleteReport(delReport.reportIdToBeDeleted, delReport.item, user);

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetRoleListByReportId")]
        [Authorize]
        public HttpResponseMessage GetRoleListByReportId(int reportId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = API.Reporting.GetRoleListByReportId(reportId, user);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<UserRole>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }




        [HttpPost]
        [Route("SaveReportConfiguration")]
        [Authorize]
        public HttpResponseMessage SaveReportConfiguration(SaveReport saveReport)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();


                var result = API.Reporting.SaveReportConfiguration(saveReport.report, saveReport.fileName, user);

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("SaveAsReportConfiguration")]
        [Authorize]
        public HttpResponseMessage SaveAsReportConfiguration(SaveReport saveReport)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();


                var result = API.Reporting.SaveReportConfiguration(saveReport.report, saveReport.fileName, user);

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("SaveEditedReportConfiguration")]
        [Authorize]
        public HttpResponseMessage SaveEditedReportConfiguration(SaveReport saveReport)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();


                var result = API.Reporting.SaveEditedReportConfiguration(saveReport.report, saveReport.fileName, user);

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }




        [HttpPost]
        [Route("UploadReportApi")]
        [Authorize]
        public HttpResponseMessage UploadFileApi()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                ReportConfigureSetting GymSettings = API.Reporting.GetReportSetting(user);
                string uploadPath = GymSettings.ReportSettingList.FirstOrDefault(x => x.Key == "FileUploadHandlerPath").KeyValue;
                   HttpResponseMessage response = new HttpResponseMessage();
                var httpRequest = HttpContext.Current.Request;

                if (httpRequest.Files.Count > 0)
                {
                    var filePath = "";
                    foreach (string file in httpRequest.Files)
                    {
                        //string rootpath = ConfigurationSettings.AppSettings["UploadScanDocRoot"];
                        //string BranchId = httpRequest.Headers["BranchId"].ToString();
                        // string CustId = httpRequest.Headers["CustId"].ToString();
                         string Category = httpRequest.Headers["Category"].ToString();
                        // string gymCode = ExceConnectionManager.GetGymCode(UserName).ToString();
                        //string fileFolder = rootpath + @"\" + gymCode + @"\" + BranchId + @"\" + CustId + @"\";
                        string fileFolder = uploadPath + @"\" + "Reports" + @"\" + Category + @"\";
                        if (!Directory.Exists(fileFolder))
                        {
                            Directory.CreateDirectory(fileFolder);
                        }
                        var postedFile = httpRequest.Files[file];
                        filePath = fileFolder + postedFile.FileName;
                        postedFile.SaveAs(filePath);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(filePath, ApiResponseStatus.OK));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }


        }


        public List<string> GetAllReportCategories(string CoreSessionUserName)
        {
            USLogEvent.WriteToFile("Report : GetAllReportCategories initiated.", CoreSessionUserName);
            return API.Reporting.GetAllReportCategories(CoreSessionUserName);
        }

        [HttpGet]
        [Route("GetAllReportCategories")]
        [Authorize]
        public HttpResponseMessage GetAllReportCategories()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                USLogEvent.WriteToFile("Report : GetAllReportCategories initiated.", user);
               


                var result = API.Reporting.GetAllReportCategories(user);

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetAllRoles")]
        [Authorize]
        public HttpResponseMessage GetAllRoles()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                USLogEvent.WriteToFile("Report : GetAllRoles initiated.", user);
                var result = API.Reporting.GetAllRoles(user);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        public ReportConfigureSetting GetReportSetting(string userName)
        {
            return API.Reporting.GetReportSetting(userName);
        }

        [HttpGet]
        [Route("GetReportSetting")]
        [Authorize]
        public HttpResponseMessage GetReportSetting()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = API.Reporting.GetReportSetting(user);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveReportSetting")]
        [Authorize]
        public HttpResponseMessage SaveReportSetting(ReportConfigureSetting settingItem)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = API.Reporting.SaveReportSetting(user, settingItem);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("UpdateCategory")]
        [Authorize]
        public HttpResponseMessage UpdateCategory(EditCategory editCategory)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                USLogEvent.WriteToFile("Report : UpdateCategory initiated.", user);
                var result = API.Reporting.UpdateCategory(editCategory.oldName, editCategory.newName, user);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("SaveCategory")]
        [Authorize]
        public HttpResponseMessage SaveCategory(string name)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                USLogEvent.WriteToFile("Report : SaveCategory initiated.", user);
                var result = API.Reporting.SaveCategory(name, user);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("SendReportOutPut")]
        [Authorize]
        public HttpResponseMessage SendReportOutPut(SendReport sendReport)
        {
            try
            {
                //outputType : 0-Email,1-SMS, 2- Print
                string user = Request.Headers.GetValues("UserName").First();
          
                var result = API.Reporting.SendReportOutPut(sendReport.considerationGDPR, user, sendReport.subject, sendReport.body, sendReport.isSelected, sendReport.outputType, sendReport.recipientList, sendReport.primaryColumn, sendReport.basedOn, sendReport.reminder, sendReport.addreminderFee, sendReport.branchID, sendReport.ReminderDueDate);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("GetScheduleTemplateList")]
        [Authorize]
        public HttpResponseMessage GetScheduleTemplateList(ScheTemListObj dataObj)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = API.Reporting.GetScheduleTemplateList(dataObj.category, dataObj.entity, user);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch(Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

    }
}
