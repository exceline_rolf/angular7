﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Payment.Reporting.RestApi.Models
{
    public class EditCategory
    {
   
        public string oldName { get; set; }
        public string newName { get; set; }
    }
}