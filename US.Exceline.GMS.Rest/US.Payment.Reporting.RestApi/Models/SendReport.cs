﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Payment.Reporting.RestApi.Models
{
    public class SendReport
    {
        

        public string subject { get; set; }
        public string body { get; set; }
        public bool isSelected { get; set; }
        public int outputType { get; set; }
        public List<string> recipientList { get; set; }
        public string primaryColumn { get; set; }
        public string basedOn { get; set; }
        public bool reminder { get; set; }
        public bool addreminderFee { get; set; }
        public int branchID { get; set; }
        public DateTime? ReminderDueDate { get; set; }
        public bool considerationGDPR { get; set; }
    }
}