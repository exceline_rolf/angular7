﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.Payment.Reporting.Core.DomainObjects;

namespace US.Payment.Reporting.RestApi.Models
{
    public class SaveReport
    {
        
        public USReport report { get; set; }
        public string fileName { get; set; }
     
    }
}