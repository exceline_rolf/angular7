﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Payment.Reporting.RestApi.Models
{
    public class DeleteReport
    {
        public int reportIdToBeDeleted { get; set; }
        public string item { get; set; }
    }
}