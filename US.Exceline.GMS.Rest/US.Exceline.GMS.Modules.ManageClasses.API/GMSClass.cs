﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.ManageClasses.BusinessLogic;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;


namespace US.Exceline.GMS.Modules.ManageClasses.API
{
    public class GMSClass
    {
        public static OperationResult<List<ExcelineClassDC>> SearchClass(string category, string searchText, string gymCode)
        {
            return ClassManager.SearchClass(category, searchText, gymCode);
        }

        public static OperationResult<List<ExcelineClassDC>> SearchClass(string category, string searchText, DateTime startDate, string gymCode)
        {
            return ClassManager.SearchClass(category, searchText, startDate, gymCode);
        }

        public static OperationResult<List<ExcelineClassDC>> SearchClass(string category, string searchText, DateTime startDate, DateTime endDate, string gymCode)
        {
            return ClassManager.SearchClass(category, searchText, startDate, endDate, gymCode);
        }

        public static OperationResult<int> GetNextClassId(string gymCode)
        {
            return ClassManager.GetNextClassId(gymCode);
        }

        public static OperationResult<int> SaveClass(ExcelineClassDC excelineClass, string gymCode)
        {
            return ClassManager.SaveClass(excelineClass, gymCode);
        }

        public static OperationResult<bool> UpdateClass(ExcelineClassDC excelineClass, string gymCode)
        {
            return ClassManager.UpdateClass(excelineClass, gymCode);
        }

        public static OperationResult<bool> DeleteClass(int classId, string gymCode)
        {
            return ClassManager.DeleteClass(classId, gymCode);
        }

        public static OperationResult<List<ExcelineClassDC>> GetClasses(string className, int branchId, string gymCode)
        {
            return ClassManager.GetClasses(className, branchId, gymCode);
        }

        public static OperationResult<List<ExcelineClassActiveTimeDC>> GetExcelineClassActiveTimes(int branchId, DateTime startTime, DateTime endTime, int entNO, string gymCode)
        {
            return ClassManager.GetExcelineClassActiveTimes(branchId, startTime, endTime, entNO, gymCode);
        }

        public static OperationResult<List<ExcelineMemberDC>> GetMembersForClass(int classId, string gymCode)
        {
            OperationResult<List<ExcelineMemberDC>> result = new OperationResult<List<ExcelineMemberDC>>();
            try
            {
                result = ClassManager.GetMembersForClass(classId, gymCode);
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public static OperationResult<List<TrainerDC>> GetTrainersForClass(int classId, string gymCode)
        {
            return ClassManager.GetTrainersForClass(classId, gymCode);
        }

        public static OperationResult<List<InstructorDC>> GetInstructorsForClass(int classId, string gymCode)
        {
            return ClassManager.GetInstructorsForClass(classId, gymCode);
        }

        public static OperationResult<List<ResourceDC>> GetResourceForClass(int classId, string gymCode)
        {
            return ClassManager.GetResourcesForClass(classId, gymCode);
        }

        public static OperationResult<ScheduleDC> GetClassSchedule(int classId, string gymCode)
        {
            return ClassManager.GetClassSchedule(classId, gymCode);
        }

        public static OperationResult<List<ExcelineClassActiveTimeDC>> GetClassHistory(int classId, List<ScheduleItemDC> scheduleItemList, string gymCode)
        {
            return ClassManager.GetClassHistory(classId, scheduleItemList, gymCode);
        }

        public static OperationResult<List<OrdinaryMemberDC>> GetMembersForClassBooking(int classId, int branchId, string searchText, string user, string gymCode)
        {
            return ClassManager.GetMembersForClassBooking(classId, branchId, searchText, user, gymCode);
        }

        public static OperationResult<List<ClassBookingActiveTimeDC>> GetClassActiveTimesForBooking(int branchId, DateTime startTime, DateTime endTime, int entNO, string gymCode)
        {
            return ClassManager.GetClassActiveTimesForBooking(branchId, startTime, endTime, entNO, gymCode); ;
        }

        public static OperationResult<List<MemberBookingDetailsDC>> GetMemberBookingDetails(int classId, int branchId, string user, string gymCode)
        {
            return ClassManager.GetMemberBookingDetails(classId, branchId, user, gymCode);
        }

        public static OperationResult<int> SaveMemberBookingDetails(int classId, OrdinaryMemberDC ordinaryMemberDC, int memberId, List<ClassBookingDC> bookingList, decimal totalBookingAmount, decimal totalAvailableAmount, string user, int branchId, string scheduleCategoryType, string gymCode)
        {
            return ClassManager.SaveMemberBookingDetails(classId, ordinaryMemberDC, memberId, bookingList, totalBookingAmount, totalAvailableAmount, user, branchId, scheduleCategoryType, gymCode);
        }

        public static OperationResult<bool> ApplyClassScheduleForNextYear(int classId, DateTime startdate, DateTime enddate, string name, int branchId, string createdUser, List<ScheduleItemDC> scheduleItemList, string gymCode)
        {
            return ClassManager.ApplyClassScheduleForNextYearAction(classId, startdate, enddate, name, branchId, createdUser, scheduleItemList, gymCode);
        }

        public static OperationResult<bool> GetVerificationOfAppliedClassScheduleAction(int classScheduleId, string gymCode)
        {
            return ClassManager.GetVerificationOfAppliedClassScheduleAction(classScheduleId, gymCode);
        }

        public static OperationResult<List<ArticleDC>> GetArticlesForClass(string gymCode)
        {
            return ClassManager.GetArticlesForClass(gymCode);
        }

        public static OperationResult<int> SaveClassBookingPayment(int memberId, int classId, BookingArticleDC bookingArticle, List<ClassBookingDC> PaidBookings, List<BookingPaymentDC> bookingPayments, decimal totalAmount, decimal paidAmount, decimal defaultPrice, string user, int branchId, string gymCode)
        {
            return ClassManager.SaveClassBookingPayment(memberId, classId, bookingArticle, PaidBookings, bookingPayments, totalAmount, paidAmount, defaultPrice, user, branchId, gymCode);
        }

        public static OperationResult<int> GetExcelineClassIdByName(string className, string gymCode)
        {
            return ClassManager.GetExcelineClassIdByName(className, gymCode);
        }
    }
}
