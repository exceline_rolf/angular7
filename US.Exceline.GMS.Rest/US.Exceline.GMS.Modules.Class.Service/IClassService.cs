﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageClassSettings;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;


namespace US.Exceline.GMS.Modules.Class.Service
{
    [ServiceContract]
    public interface IClassService
    {
        #region Class


        [OperationContract]
        string SaveClass(ExcelineClassDC excelineClass, int branchId, string user, string culture);
        [OperationContract]
        bool UpdateClass(ExcelineClassDC excelineClass, string user);
        [OperationContract]
        bool DeleteClass(int classId, string user);
        [OperationContract]
        List<ExcelineClassDC> GetClasses(string className, int branchId, string user);
        [OperationContract]
        List<ScheduleItemDC> GetScheduleItemsByClass(int classId, int branchId, string user);
        [OperationContract]
        List<ScheduleItemDC> GetScheduleItemsByClassIds(List<int> classIds, int branchId, string user);
        [OperationContract]
        List<TrainerDC> GetTrainersForClass(int branchId, string searchText, string user);
        [OperationContract]
        List<InstructorDC> GetInstructorsForClass(int branchId, string searchText, string user);
        [OperationContract]
        List<ExcelineMemberDC> GetMembersForClass(int branchId, string searchText, string user, int hit);
        [OperationContract]
        List<ResourceDC> GetResourcesForClass(int branchId, string searchText, string user);
        [OperationContract]
        int GetNextClassId(string user);
        [OperationContract]
        List<InstructorDC> GetInstructorsForClassUpdate(int classId, string user);
        [OperationContract]
        List<ResourceDC> GetResourcesForClassUpdate(int classId, string user);
        [OperationContract]
        int GetExcelineClassIdByName(int branchId, string user, string className);
        [OperationContract]
        List<ExcelineClassMemberDC> GetMembersByActiveTimeId(int branchId, int activeTimeId, string user);
        [OperationContract]
        int CheckClassActiveTimeOverlapWithInstructor(ScheduleItemDC scheduleItem, string user);
        [OperationContract]
        bool DeleteClassActiveTime(int actTimeID, string user);
        [OperationContract]
        int UpdateClassActiveTime(UpdateClassActiveTimeHelperDC updateClassActiveTimeDC, string user);
        [OperationContract]
        List<EntityActiveTimeDC> GetActiveTimesForClassScheduleItem(int scheduleItemId, string user);
        #endregion

        #region Category
        [OperationContract]
        List<CategoryDC> GetCategories(string type, string user, int branchId);
        [OperationContract]
        ScheduleDC GetClassSchedule(int classId, int branchId, string user);
        [OperationContract]
        List<CategoryTypeDC> GetCategoryTypes(string name, string user, int branchId);
        [OperationContract]
        int SaveCategory(CategoryDC category, string user, int branchId);
        #endregion

        #region Schedule
        [OperationContract]
        bool UpdateActiveTime(EntityActiveTimeDC activetime, string user, int branchId, bool isDeleted);
        [OperationContract]
        bool UpdateCalenderActiveTimes(List<EntityActiveTimeDC> activeTimeList, int branchId, string user);
        [OperationContract]
        List<EntityActiveTimeDC> GetEntityActiveTimes(int branchid, DateTime startDate, DateTime endDate, List<int> entityList, string entityRoleType, string user);
        #endregion

        #region Article
        [OperationContract]
        ArticleDC GetArticle(int articleId, int branchId, string user);
        #endregion

        #region Utility
        [OperationContract]
        List<CalendarHoliday> GetHolidays(DateTime calendarDate, string user, int branchId);
        #endregion
        [OperationContract]
        int UpdateTimeTableScheduleItems(List<ScheduleItemDC> scheduleItemList, int branchId, string user, string culture);

         [OperationContract]
        int SaveScheduleItem(ScheduleItemDC scheduleItem, int branchId, string culture, string user);

         [OperationContract]
         int DeleteScheduleItem(List<int> scheduleItemIdList, string user);

         [OperationContract]
         bool DeleteSchedule(ExcelineClassDC exceClass, string user);

         [OperationContract]
         List<InstructorDC> GetInstructors(int branchId, string searchText, string user, bool isActive);

        [OperationContract]
        List<ExceClassTypeDC> GetClassTypes(int branchId, string user);

        [OperationContract]
        int SaveActiveTimes(List<ScheduleItemDC> scheduleItemList, DateTime startDate, DateTime endDate, string user, string culture);

        [OperationContract]
        int UpdateClassCalendarActiveTime(EntityActiveTimeDC activeTime, string user);

        [OperationContract]
        List<String> GetClassKeywords(string user);

        [OperationContract]
        string GetGymSettings(int branchId, string user, GymSettingType gymSettingType);

        [OperationContract]
        GymOtherSettingsDC GetOtherGymSetting();

        [OperationContract]
        int UpdateSeasonEndDateWithClassActiveTimes(int branchId, int seasonId, DateTime endDate,string user);

        [OperationContract]
        bool UpdateSettingForUserRoutine(string key, string value, string user);

        [OperationContract]
        string GetListOrCalendarInitialViewByUser(string user);
    }
}
