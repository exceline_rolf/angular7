﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Class.API;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Admin.ManageClassSettings;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.Exceline.GMS.Modules.Admin.API.ManageEmployees;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.Exceline.GMS.Modules.Admin.API.ManageResources;
using US.Common.Logging.API;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.API;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.Exceline.GMS.Modules.Admin.API.ManageSystemSettings;

namespace US.Exceline.GMS.Modules.Class.Service
{
    public class ClassService : IClassService
    {
        #region Class

        public string SaveClass(ExcelineClassDC excelineClass, int branchId, string user, string culture)
        {
            OperationResult<string> result = new OperationResult<string>();
            result = GMSClass.SaveClass(excelineClass, branchId, ExceConnectionManager.GetGymCode(user), user, culture);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return "-1";
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineClassDC> GetClasses(string className, int branchId, string user)
        {
            OperationResult<List<ExcelineClassDC>> result = new OperationResult<List<ExcelineClassDC>>();
            result = GMSClass.GetClasses(className, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineClassDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ScheduleItemDC> GetScheduleItemsByClass(int classId, int branchId, string user)
        {
            OperationResult<List<ScheduleItemDC>> result = new OperationResult<List<ScheduleItemDC>>();
            result = GMSClass.GetScheduleItemsByClass(classId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ScheduleItemDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ScheduleItemDC> GetScheduleItemsByClassIds(List<int> classIds, int branchId, string user)
        {
            OperationResult<List<ScheduleItemDC>> result = new OperationResult<List<ScheduleItemDC>>();
            result = GMSClass.GetScheduleItemsByClassIds(classIds, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ScheduleItemDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateClass(ExcelineClassDC excelineClass, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result = GMSClass.UpdateClass(excelineClass, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteClass(int classId, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result = GMSClass.DeleteClass(classId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<TrainerDC> GetTrainersForClass(int branchId, string searchText, string user)
        {
            int trainerId = -1;
            OperationResult<List<TrainerDC>> trainerLst = GMSTrainer.GetTrainers(branchId, searchText, true, trainerId, ExceConnectionManager.GetGymCode(user));
            if (trainerLst.ErrorOccured)
            {
                foreach (NotificationMessage message in trainerLst.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<TrainerDC>();
            }
            else
            {
                return trainerLst.OperationReturnValue;
            }
        }

        public List<InstructorDC> GetInstructorsForClass(int branchId, string searchText, string user)
        {
            int instructorId = -1;
            OperationResult<List<InstructorDC>> instructorLst = GMSInstructor.GetInstructors(branchId, searchText, true, instructorId, ExceConnectionManager.GetGymCode(user));
            if (instructorLst.ErrorOccured)
            {
                foreach (NotificationMessage message in instructorLst.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InstructorDC>();
            }
            else
            {
                return instructorLst.OperationReturnValue;
            }
        }

        public List<ExcelineMemberDC> GetMembersForClass(int branchId, string searchText, string user, int hit)
        {
            int status = 1;  //active member status=1;
            OperationResult<List<ExcelineMemberDC>> result = GMSManageMembership.GetMembers(branchId, searchText, status, MemberSearchType.CLASS, MemberRole.MEM, user, ExceConnectionManager.GetGymCode(user), hit, false, false, string.Empty);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ResourceDC> GetResourcesForClass(int branchId, string searchText, string user)
        {
            OperationResult<List<ResourceDC>> result = GMSResources.GetResources(branchId, searchText, -1, -1, 2, true, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ResourceDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineClassMemberDC> GetMembersByActiveTimeId(int branchId, int activeTimeId, string user)
        {
            OperationResult<List<ExcelineClassMemberDC>> result = GMSClass.GetMembersByActiveTimeId(branchId, activeTimeId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineClassMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int GetNextClassId(string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            result = GMSClass.GetNextClassId(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<InstructorDC> GetInstructorsForClassUpdate(int classId, string user)
        {
            OperationResult<List<InstructorDC>> result = new OperationResult<List<InstructorDC>>();
            result = GMSClass.GetInstructorsForClass(classId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InstructorDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ResourceDC> GetResourcesForClassUpdate(int classId, string user)
        {
            OperationResult<List<ResourceDC>> result = new OperationResult<List<ResourceDC>>();
            result = GMSClass.GetResourceForClass(classId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ResourceDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CategoryDC> GetCategories(string type, string user, int branchId)
        {
            OperationResult<List<CategoryDC>> result = GMSCategory.GetCategoriesByType(type, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryDC>(); ;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public ScheduleDC GetClassSchedule(int classId, int branchId, string user)
        {
            OperationResult<ScheduleDC> result = new OperationResult<ScheduleDC>();
            result = GMSClass.GetClassSchedule(classId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new ScheduleDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CategoryTypeDC> GetCategoryTypes(string name, string user, int branchId)
        {
            OperationResult<List<CategoryTypeDC>> result = GMSCategory.GetCategoryTypes(name, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryTypeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveCategory(CategoryDC category, string user, int branchId)
        {
            OperationResult<int> result = GMSCategory.SaveCategory(category, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        //public List<ArticleDC> GetArticlesForClass(int branchId, string user)
        //{
        //    OperationResult<List<ArticleDC>> result = new OperationResult<List<ArticleDC>>();
        //    result = GMSClass.GetArticlesForClass(ExceConnectionManager.GetGymCode(user));
        //    if (result.ErrorOccured)
        //    {
        //        foreach (NotificationMessage message in result.Notifications)
        //        {
        //            USLogError.WriteToFile(message.Message, new Exception(), user);
        //        }
        //        return new List<ArticleDC>();
        //    }
        //    else
        //    {
        //        return result.OperationReturnValue;
        //    }
        //}

        public int GetExcelineClassIdByName(int branchId, string user, string className)
        {
            return GMSClass.GetExcelineClassIdByName(className, ExceConnectionManager.GetGymCode(user)).OperationReturnValue;
        }


        #endregion

        #region Schedule
        public bool UpdateActiveTime(EntityActiveTimeDC activetime, string user, int branchId, bool isDeleted)
        {
            OperationResult<bool> result = GMSSchedule.UpdateActiveTime(activetime, isDeleted, branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateCalenderActiveTimes(List<EntityActiveTimeDC> activeTimeList, int branchId, string user)
        {
            OperationResult<bool> result = GMSSchedule.UpdateCalenderActiveTimes(activeTimeList, branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<EntityActiveTimeDC> GetEntityActiveTimes(int branchid, DateTime startDate, DateTime endDate, List<int> entityList, string entityRoleType, string user)
        {
            OperationResult<List<EntityActiveTimeDC>> result = GMSSchedule.GetEntityActiveTimes(branchid, startDate, endDate, entityList, entityRoleType, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<EntityActiveTimeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        #region Article
        public ArticleDC GetArticle(int articleId, int branchId, string user)
        {
            OperationResult<ArticleDC> result = GMSArticle.GetArticleById(articleId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Utility
        public List<CalendarHoliday> GetHolidays(DateTime calendarDate, string user, int brnachId)
        {
            OperationResult<List<CalendarHoliday>> result = GMSUtility.GetHolidays(calendarDate, brnachId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CalendarHoliday>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        public List<InstructorDC> GetInstructors(int branchId, string searchText, string user, bool isActive)
        {
            var instructorId = -1;
            var result = GMSInstructor.GetInstructors(branchId, searchText, isActive, instructorId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InstructorDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int UpdateTimeTableScheduleItems(List<ScheduleItemDC> scheduleItemList, int branchId, string user, string culture)
        {
            //scheduleItem.ActiveTimes = (List<ActiveTimeDC>)US.GMS.Core.Utils.XMLUtils.DesrializeXMLToObject(scheduleItem.ActiveTimesString, typeof(List<ActiveTimeDC>));
            //scheduleItem.ActiveTimes =  XMLUtilities.DesrializeXMLToObject(scheduleItem.ActiveTimesString, typeof(List<ActiveTimeDC>)) as List<ActiveTimeDC>;
            OperationResult<int> result = GMSClass.UpdateTimeTableScheduleItems(scheduleItemList, branchId, ExceConnectionManager.GetGymCode(user), user, culture);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public int SaveScheduleItem(ScheduleItemDC scheduleItem, int branchId, string culture, string user)
        {
            //scheduleItem.ActiveTimes = (List<ActiveTimeDC>)US.GMS.Core.Utils.XMLUtils.DesrializeXMLToObject(scheduleItem.ActiveTimesString, typeof(List<ActiveTimeDC>));
            //scheduleItem.ActiveTimes =  XMLUtilities.DesrializeXMLToObject(scheduleItem.ActiveTimesString, typeof(List<ActiveTimeDC>)) as List<ActiveTimeDC>;
            int returnValue = -1;
            OperationResult<int> result = GMSClass.SaveScheduleItem(scheduleItem, branchId, ExceConnectionManager.GetGymCode(user),culture, user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                returnValue = result.OperationReturnValue;
                return returnValue;
            }
        }

        public int DeleteScheduleItem(List<int> scheduleItemIdList, string user)
        {
            //scheduleItem.ActiveTimes = (List<ActiveTimeDC>)US.GMS.Core.Utils.XMLUtils.DesrializeXMLToObject(scheduleItem.ActiveTimesString, typeof(List<ActiveTimeDC>));
            //scheduleItem.ActiveTimes =  XMLUtilities.DesrializeXMLToObject(scheduleItem.ActiveTimesString, typeof(List<ActiveTimeDC>)) as List<ActiveTimeDC>;
            OperationResult<int> result = GMSClass.DeleteScheduleItem(scheduleItemIdList, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExceClassTypeDC> GetClassTypes(int branchId, string user)
        {
            var result = ManageClassTypeSetting.GetClassTypesByBranch(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExceClassTypeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteSchedule(ExcelineClassDC exceClass, string user)
        {
            var result = GMSClass.DeleteSchedule(exceClass, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveActiveTimes(List<ScheduleItemDC> scheduleItemList, DateTime startDate, DateTime endDate, string user, string culture)
        {
            OperationResult<int> result = GMSClass.SaveActiveTimes(scheduleItemList, startDate, endDate, ExceConnectionManager.GetGymCode(user), culture);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int UpdateClassCalendarActiveTime(EntityActiveTimeDC activeTime, string user)
        {
            OperationResult<int> result = GMSClass.UpdateClassCalendarActiveTime(activeTime, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<String> GetClassKeywords(string user)
        {
            OperationResult<List<string>> result = GMSSystemSettings.GetClassKeyword(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<string>();
            }
            else
            {
                return result.OperationReturnValue;
            }

        }

        public string GetGymSettings(int branchId, string user, GymSettingType gymSettingType)
        {
            OperationResult<string> result = GMSManageGymSetting.GetGymSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }



        public int CheckClassActiveTimeOverlapWithInstructor(ScheduleItemDC scheduleItem, string user)
        {
            OperationResult<int> result = GMSSchedule.CheckScheduleOverlapForEmployee(scheduleItem, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -3;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public string GetGymSettings(int branchId, string user, GymSettingType gymSettingType, string systemName)
        {
            OperationResult<string> result = GMSManageGymSetting.GetGymSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public GymOtherSettingsDC GetOtherGymSetting()
        {
            return new GymOtherSettingsDC();
        }


        public int UpdateSeasonEndDateWithClassActiveTimes(int branchId, int seasonId, DateTime endDate, string user)
        {
            OperationResult<int> result = GMSClass.UpdateSeasonEndDateWithClassActiveTimes(branchId, seasonId, endDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public bool DeleteClassActiveTime(int actTimeID, string user)
        {
            OperationResult<bool> result = GMSClass.DeleteClassActiveTime(actTimeID, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public int UpdateClassActiveTime(UpdateClassActiveTimeHelperDC updateClassActiveTimeDC, string user)
        {
            OperationResult<int> result = GMSClass.UpdateClassActiveTime(updateClassActiveTimeDC, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -3;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<EntityActiveTimeDC> GetActiveTimesForClassScheduleItem(int scheduleItemId, string user)
        {
            OperationResult<List<EntityActiveTimeDC>> result = GMSClass.GetActiveTimesForClassScheduleItem(scheduleItemId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateSettingForUserRoutine(string key, string value, string user)
        {
            var result = GMSUser.UpdateSettingForUserRoutine(key, value, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string GetListOrCalendarInitialViewByUser(string user)
        {
            try
            {
                return GMSClass.GetListOrCalendarInitialViewByUser(user, ExceConnectionManager.GetGymCode(user));
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("ERROR: UpdateSettingForUserRoutine" + ex.Message, ex, user);
                return "Calendar";
            }
        }
    }
}

