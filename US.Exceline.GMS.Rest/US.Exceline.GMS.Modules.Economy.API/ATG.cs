﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.BusinessLogic;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.US.Payment.Core.BusinessDomainObjects;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.API
{
    public class ATG
    {
        public static OperationResult<List<IUSPCreditor>> GetATGCrediors(ATGRecordType ATGType, int branchID, DateTime startDate, DateTime endDate, string gymCode, string sendingNo)
        {
            return ATGManager.GetATGCrediors(ATGType, branchID, startDate, endDate, gymCode, sendingNo);
        }

        public static OperationResult<IFileLog> GenerateFile(DateTime startDate, DateTime endDate, List<IUSPCreditor> creditors, int status, string folderPath, string gymCode, string sendingNo)
        {
            OperationResult<IFileLog> result = new OperationResult<IFileLog>();
            string notify = string.Empty;
            IFileLog loggin = new USPFileLog();
            if (status == 0)
                notify = "0";
            else if(status == 2)
                notify = "2";

            result.OperationReturnValue = ATGManager.ProcessFile(startDate, endDate, notify, creditors, status, folderPath, gymCode, sendingNo);
            return result;  
        }

        public static ATGSummary GetATGSummary(DateTime startDate, DateTime endDate, List<IUSPCreditor> creditors, int status, string folderPath, string gymCode, string sendingNo)
        {
            OperationResult<ATGSummary> result = new OperationResult<ATGSummary>();
            string notify = string.Empty;
            IFileLog loggin = new USPFileLog();
            if (status == 0)
                notify = "0";
            else if (status == 2)
                notify = "2";

            result.OperationReturnValue = ATGManager.GetATGSummary(startDate, endDate, notify, creditors, status, folderPath, gymCode, sendingNo);
            return result.OperationReturnValue;  
        }
    }
}
