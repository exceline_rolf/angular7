﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.BusinessLogic;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.API
{
    public class DirectPayment
    {
        public static OperationResult<List<USPErrorPayment>> GetErrorPayments(DateTime from, DateTime to, string errorPaymentType, string GymCode, int branchId,string batchId=null)
        {
            return PaymentManager.GetErrorPaymentsByErrorType(from, to, errorPaymentType, GymCode, branchId, batchId);
        }

        public static OperationResult<bool> RemovePayment(int paymentID, int arItemno, string user, string gymCode, string type)
        {
            return PaymentManager.RemovePayment(paymentID, arItemno, user, gymCode, type);
        }

        public static OperationResult<int> MovePayment(int paymentID, int aritemno, string type, string user, string gymCode)
        {
            return PaymentManager.MovePayment(paymentID, aritemno, type, user, gymCode);
        }
    }
}
