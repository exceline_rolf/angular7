﻿using System;
using System.Text;
using US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.ManageMembership;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.SystemObjects.Enums;
using System.Collections.Generic;
using System.IO;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using System.Linq;
using System.Xml.Linq;
using PdfSharp.Pdf.IO;


namespace US.Exceline.GMS.Modules.ManageMembership.BusinessLogic.ManageMembership
{
    public class MemberContractPrint
    {
        static public String XMLData = String.Empty;
        static public String output;
        static public Byte[] result;

        static public String Getoutput()
        {
            return output;
        }

        static public void Setoutput(String input)
        {
            output = input;
        }

        public Byte[] GetResult()
        {
            return result;
        }

        static public void SetResult(Byte[] input)
        {
            result = input;
        }

        public String FillMeFrontAndBehind(String Input, int amount = 34)
        {
            String result = Input;
            String space = "&nbsp;";
            int inputLength = Input.Length;
            for (int i = inputLength; i <= amount; i++)
            {
                space += "&nbsp;";
            }
            return space + result + space;
        }

        public MemberContractPrint(int contractId, String gymcode) { 
            MemberContractPrintInfo memberData = new MemberContractPrintInfo();
            List<MonthlyServicesForContract> serviceData = new List<MonthlyServicesForContract>();
            memberData = ManageMembershipFacade.GetMemberContract(contractId, gymcode);
            serviceData = ManageMembershipFacade.GetMonthlyService(contractId, gymcode);

            XElement ContractInfo =
            new XElement("ContractInfo",
            new XElement("TrainingPeriodeStart", memberData.StartLockPeriod),
            new XElement("TrainingPeriodeStop", memberData.StopLockPeriod),
            new XElement("StartUpPrice", memberData.StrtUpItemPrice),
            new XElement("ServicePrice", memberData.MonthlyServicePrice),
            new XElement("AddonPrice", memberData.MonthlyAddonsPrice),
            new XElement("TotalPrice", memberData.TotPriceLockPeriod),
            new XElement("ContractSignDate", memberData.SignedDate),
            new XElement("MemberContractNo", memberData.MemberContractNo),
            new XElement("Template", memberData.Template));
            
            XElement CustomerInfo =
            new XElement("CustomerInfo",
            new XElement("FirstName", memberData.FirstName),
            new XElement("LastName",  memberData.LastName),
            new XElement("Adress", memberData.Address1),
            new XElement("Zip", memberData.PostalAddress),
            new XElement("MobileNumber", memberData.TelMobile),
            new XElement("PrivateNumber", memberData.TelPrivate),
            new XElement("WorkNumber", memberData.TelWork),
            new XElement("DateOfBirth", memberData.Birthdate),
            new XElement("MemberNo", memberData.MemberNo));


            XElement GymInfo =
            new XElement("GymInfo",
            new XElement("BranchId", memberData.BranchID),
            new XElement("GymAdress", memberData.GymAddress1),
            new XElement("GymName", memberData.GymName),
            new XElement("GymOrgNr", memberData.GymOrganizationNo),
            new XElement("GymAccountNo", memberData.GymAccountNo),
            new XElement("GymZip", memberData.GymPostalAddress),
            new XElement("GymCode", memberData.GymCode));

            List<XElement> iterator = new List<XElement>();
            if (memberData.ContractItemsCount != 0) {
                int linecounter = 1;

            for (var i = 0; i < memberData.ContractItemsCount; i++)
                {
                    iterator.Add(
                              new XElement("ContractItem_" + linecounter++.ToString(),
                                        new XElement("ItemDescription", serviceData[i].ItemName),
                                        new XElement("ItemPrice", serviceData[i].Price),
                                        new XElement("NumberOfItems", serviceData[i].Units),
                                        new XElement("StartOrder", serviceData[i].StartOrder),
                                        new XElement("EndOrder", serviceData[i].EndOrder)));

                }
            }

            XDocument xmldoc;

            if (memberData.ContractItemsCount > 0) { 
             xmldoc = new XDocument(new XElement("ContractPrintData",
                ContractInfo,
                CustomerInfo,
                GymInfo,
                 new XElement("ContractItems", from line in iterator select line)));
            }
            else
            {
                 xmldoc = new XDocument(new XElement("ContractPrintData",
                    ContractInfo,
                    CustomerInfo,
                    GymInfo));
            }
            XMLData = xmldoc.ToString(SaveOptions.DisableFormatting);

            //   String htmlAddonText = memberData.ContractCondition;

            //Save XMLDATA TO DB



            Setoutput(new Economy.BusinessLogic.XMLtoHTMLConverter(XMLData, PdfTemplatesTypes.MEMBERCONTRACT_EXCELINE, gymcode).GetResultHTMLText());

            Byte[] res = null;

            using (MemoryStream ms = new MemoryStream())
            {
                MemoryStream Stream1 = new MemoryStream();
                MemoryStream Stream2 = new MemoryStream();
                
                String replacementString = "<br><br><span>Jeg har lest og forstått innholdet i denne kontrakten.</span><br>";
                replacementString += @"<div style = ""line-height: 4px;"">";
                if (memberData.ParentId == -1) {
                    replacementString += @"<span style= ""margin-left: 24px;"">" + FillMeFrontAndBehind(DateTime.Now.Date.ToString("dd.MM.yyyy"), 22) + "</span><br>";
                    replacementString += @"<span style= ""border-top: 1px solid gray;margin-left: 24px;"">" + FillMeFrontAndBehind("Dato", 22) + "</span>";
                    replacementString += @"<span style= ""border-top: 1px solid gray;margin-left: 24px;"">" + FillMeFrontAndBehind("Medlemmets underskrift", 22) + "</span>";
                    replacementString += @"<span style= ""border-top: 1px solid gray;margin-left: 24px;"">" + FillMeFrontAndBehind(memberData.GymName, 22) + "</span>";
                }
                else
                {
                    replacementString += @"<span>" + FillMeFrontAndBehind(DateTime.Now.Date.ToString("dd.MM.yyyy"), 20) + "</span><br>";
                    replacementString += @"<span style= ""border-top: 1px solid gray;"">" + FillMeFrontAndBehind("Dato", 20) + "</span>";
                    replacementString += @"<span style= ""border-top: 1px solid gray;margin-left: 14px;"">" + FillMeFrontAndBehind("Medlemmets underskrift", 20) + "</span>";
                    replacementString += @"<span style= ""border-top: 1px solid gray;margin-left: 14px;"">" + FillMeFrontAndBehind("Underskrift foresatt", 20) + "</span>";
                    replacementString += @"<span style= ""border-top: 1px solid gray;margin-left: 14px;"">" + FillMeFrontAndBehind(memberData.GymName, 20) + "</span>";
                }
                replacementString += "</div>";

                PdfSharp.Pdf.PdfDocument pdf = new PdfSharp.Pdf.PdfDocument(ms);

                PdfSharp.Pdf.PdfDocument Contract = PdfGenerator.GeneratePdf(Getoutput(), PdfSharp.PageSize.A4);
                
                Contract.Save(Stream1, false);

                PdfSharp.Pdf.PdfDocument Conditions = PdfGenerator.GeneratePdf(memberData.ContractCondition.Replace("</body>", replacementString), PdfSharp.PageSize.A4);             
                Conditions.Save(Stream2, false);

                Contract= PdfReader.Open(Stream1, PdfDocumentOpenMode.Import);
                Conditions = PdfReader.Open(Stream2, PdfDocumentOpenMode.Import);

                for (int i = 0; i< Contract.PageCount; i++)
                {
                    pdf.AddPage(Contract.Pages[i]);
                }

                for (int i = 0; i < Conditions.PageCount; i++)
                {
                    pdf.AddPage(Conditions.Pages[i]);
                }
                  
               
                pdf.Save(ms);
                res = ms.ToArray();
                Stream1.Close(); Stream2.Close(); Stream1.Dispose(); Stream2.Dispose();
                
                SetResult(res);

            }

        }

    }
}
