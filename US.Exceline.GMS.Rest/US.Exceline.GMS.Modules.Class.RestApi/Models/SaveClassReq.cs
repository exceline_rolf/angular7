﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ManageClasses;

namespace US.Exceline.GMS.Modules.Class.RestApi.Models
{
    public class SaveClassReq
    {

        private ExcelineClassDC _excelineClass;

        public ExcelineClassDC ExcelineClass
        {
            get { return _excelineClass; }
            set { _excelineClass = value; }
        }

        private int _branchId;

        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _culture;

        public string Culture
        {
            get { return _culture; }
            set { _culture = value; }
        }

    }
}