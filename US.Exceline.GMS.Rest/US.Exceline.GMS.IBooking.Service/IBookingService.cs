﻿//using SwaggerWcf.Attributes;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using US.Exceline.GMS.IBooking.Core;
using US.Exceline.GMS.IBooking.Service.Response;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service
{
    [ServiceContract]
    public interface IBookingService
    {
        [OperationContract]       
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Members/{systemID}/{gymId}/{changedSinceDays}/{apiKey}")]
        ResponseMemberList GetMemberDataChanges(string systemID, string gymId, string changedSinceDays, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Members/{systemID}/{gymId}/{apiKey}")]
        ResponseMemberList GetMemberData(string systemID, string gymId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Member/{systemID}/{gymId}/{customerNo}/{apiKey}")]
        ResponseMemberList GetMemberById(string systemID, string gymId, string customerNo, string apiKey);

        
        
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "System/{systemId}/{apiKey}")]
        ResponseSystemDetails GetSystemData(string systemId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Gyms/{systemId}/{apiKey}")]
        ResponseBranchList GetGyms(string systemId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Offerings/{systemId}/{apiKey}")]
        ResponseWebOfferingList GetWebOfferings(string systemId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Instructors/{systemId}/{apiKey}")]
        ResponseInstructorList GetInstructors(string systemId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "ClassCategories/{systemId}/{apiKey}")]
        ResponseClassCategoryList GetClassCategories(string systemId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "ClassKeywords/{systemId}/{apiKey}")]
        ResponseClassKeywordList GetClassKeywords(string systemId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "ClassLevels/{systemId}/{apiKey}")]
        ResponseClassLevelList GetClassLevels(string systemId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "StartReasons/{systemId}/{apiKey}")]
        ResponseStartReasonList GetStartReasons(string systemId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "ClassTypes/{systemId}/{apiKey}")]
        ResponseClassTypeList GetClassTypes(string systemId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Classes/{systemId}/{gymId}/{fromDate}/{toDate}/{apiKey}")]
        ResponseClassScheduleCalenderList GetClassCalendar(string systemId, string gymId, string fromDate, string toDate, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "ShowUps/{systemId}/{gymId}/{fromDate}/{toDate}/{apiKey}")]
        ResponseExcelineShowUpList GetExcelingeShowUps(string systemId, string gymId, string fromDate, string toDate, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Payments/{systemId}/{gymId}/{fromDate}/{toDate}/{apiKey}")]
        ResponsePaymentList GetPayments(string systemId, string gymId, string fromDate, string toDate, string apiKey);


        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Member/Add/{apiKey}")]
        ResponseStatus RegisterNewMember(ExceIBookingNewMember member, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Class/Update/{apiKey}")]
        ResponseStatus UpdateClass(ExceIBookingUpdateClass updatedClass, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Member/Class/UpdateVisit/{apiKey}")]
        ResponseStatus UpdateMemberClassVisit(ExceIBookingMemberClassVisit classVisit, string apiKey);


        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Class/UpdateClassVisit/{apiKey}")]
        ResponseStatus UpdateMemberClassVisits(ExceIBookingClassVisit classVisit, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Member/Visit/Update/{apiKey}")]
        ResponseStatus UpdateMemberVisit(ExceIBookingMemberVisit visit, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Member/Update/{apiKey}")]
        ResponseStatus UpdateMember(ExceIBookingUpdateMember member, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Member/Cancel/{systemId}/{gymId}/{CustomerId}/{apiKey}")]
        ResponseStatus CancelIBookingContract(string systemId, string gymId, string customerId, string apiKey);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "resourceCalendar/{systemId}/{branchId}/{fromDate}/{toDate}/{apiKey}")]
        ResponseResourceCalendarList GetResourceCalendar(string systemId, string branchId, string fromDate, string toDate, string apiKey);

        //[OperationContract]
        //[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "resourceBookingTypes/{systemId}/{branchId}")]
        //ResponseResourceBookingTypeList GetResourceBookingTypes(string systemId, string branchId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "resourceBookingTypes/{systemId}/{branchId}/{apiKey}")]
        ResponseResourceBookingTypeList GetResourceBookingTypes(string systemId, string branchId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Getinvoices/{systemId}/{GymId}/{StartDate}/{EndDate}/{apiKey}")]
        ResponseInvoiceList GetInvoices(string systemId, string GymId, string StartDate, string EndDate, string apiKey);

        #region ResourceBooking

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "getResourceBookings/{systemId}/{branchId}/{fromDate}/{toDate}/{apiKey}")]
        ResponseResourceBookingList GetResourceBooking(string systemId, string branchId, string fromDate, string toDate, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "getResourceCategories/{systemId}/{branchId}/{apiKey}")]
        ResponseResourceCategorieList GetResourceCategories(string systemId, string branchId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "getResourceBookingCategories/{systemId}/{branchId}/{apiKey}")]
        ResponseResourceCategorieList GetResourceBookingCategories(string systemId, string branchId, string apiKey);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "getResources/{systemId}/{branchId}/{apiKey}")]
        ResponseResourceList GetResources(string systemId, string branchId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/addResourceBooking/{apiKey}")]
        ResponseResourceAddBooking AddResourceBooking(ExceIBookingResourceBooking resourceBooking, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/deleteResourceBooking/{systemId}/{branchId}/{bookingId}/{apiKey}")]
        ResponseStatus DeleteResourceBooking(string systemId, string branchId, string bookingId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/setResourceBookingAsPaid/{apiKey}")]
        ResponseStatus SetResourceBookingAsPaid(ExceIBookingResourceBookingPaid resourceBookingPaid, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            UriTemplate =
                "/getResourceSchedules/{systemId}/{branchId}/{fromDate}/{toDate}/{resourceId}/{apiKey}")]
        ResponseResourceScheduleList GetResourceSchedules(string systemId, string branchId, string fromDate, string toDate, string resourceId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            UriTemplate =
                "/getResourceAvailableTimes/{systemId}/{branchId}/{fromDate}/{toDate}/{resourceId}/{apiKey}"
            )]
        ResponseResourceAvailableTimeList GetResourceAvailableTimes(string systemId, string branchId, string fromDate, string toDate, string resourceId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Interests/{systemId}/{apiKey}")]
        ResponseInterestList GetInterests(string systemId, string apiKey);
        

        #endregion


        #region GetGyms
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "MemberGyms/{systemID}/{apiKey}")]
        ResponseMemberGymDetailList GetMemberGyms(string systemID, string apiKey);
        #endregion

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "getTrainingContracts/{systemId}/{gymId}/{changedSinceDate}/{apiKey}")]
        ResponseMemberModifiedList GetMembersUpdatedByDate(string systemId, string gymId, string changedSinceDate, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "GetAllContracts/{systemId}/{apiKey}")]
        ResponseAllContractList GetAllContracts(string systemId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Freeze/Add/{apiKey}")]
        ResponseStatus RegisterNewFreeze(ExceIBookingNewFreeze freeze, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "/Contracts/{systemID}/{customerNo}/{apiKey}")]
        ResponseContractList GetContractByMemberId(string systemID, string customerNo, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "/Freeze/ListFreeze/{systemID}/{custId}/{apiKey}")]
        ResponseFreezeList GetFreezeByCustId(string systemID, string custId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Resign/Add/{apiKey}")]
        ResponseStatus RegisterResign(ExceIBookingNewResign resign, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Members/getMembersChanged/{systemID}/{gymId}/{changedSinceDays}/{apiKey}")]
        ResponseAPIMemberList GetAPIMembers(string systemID, string gymId, string changedSinceDays, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "FreezeResignCategories/{systemId}/{apiKey}")]
        ResponseFreezeResignCategoryList GetFreezeResignCategories(string systemId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "BrisIdActiveFreezed/{systemId}/{apiKey}")]
        ResponseBrisIdActiveFreezedList GetBrisIdActiveFreezed(string systemId, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Members/NtnuiMemberChanges/{systemId}/{changedSinceDays}/{apiKey}")]
        ResponseMemberList NTNUIGetMemberDataChanges(string systemId, string changedSinceDays, string apiKey);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "/Members/GetVisitCount/{systemID}/{branchId}/{year}/{apiKey}")]
        ResponseVisitList GetVisitCount(string systemID, string branchId, string year, string apiKey);
    }
}
