﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseInstructorList
    {
        public ResponseInstructorList() { }
        public ResponseInstructorList(List<ExceIBookingInstructor> instructorList, ResponseStatus status)
        {
            Instructors = instructorList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingInstructor> Instructors { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}