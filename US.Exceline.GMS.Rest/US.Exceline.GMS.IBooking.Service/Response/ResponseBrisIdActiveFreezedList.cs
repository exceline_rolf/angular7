﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseBrisIdActiveFreezedList
    {
        public ResponseBrisIdActiveFreezedList() { }
        public ResponseBrisIdActiveFreezedList(List<int?> brisid, ResponseStatus status)
        {
            BrisActiveFreezed = brisid;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<int?> BrisActiveFreezed { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}