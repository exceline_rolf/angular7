﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    public class ResponseResourceList
    {
        public ResponseResourceList() { }
        public ResponseResourceList(List<ExceIBookingResource> resourceCategorieList, ResponseStatus status)
        {
            ResourceCategorieList = resourceCategorieList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingResource> ResourceCategorieList { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}