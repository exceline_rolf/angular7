﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseClassLevelList
    {
        public ResponseClassLevelList() { }
        public ResponseClassLevelList(List<ExceIBookingClassLevel> classlevels, ResponseStatus status)
        {
            ClassLevels = classlevels;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingClassLevel> ClassLevels { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}