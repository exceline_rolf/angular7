﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseMemberClassScheduleList
    {
        public ResponseMemberClassScheduleList() { }
        public ResponseMemberClassScheduleList(List<ExceIBookingClassTimeForMember> schedules, ResponseStatus status)
        {
            Schedules = schedules;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingClassTimeForMember> Schedules { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}