﻿using System;
using System.Runtime.Serialization;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseStatus
    {
        public ResponseStatus() { }

        public ResponseStatus(string statusCode, string statusMessage)
        {
            StatusCode = statusCode;
            StatusMessage = statusMessage;
        }

        [DataMember(Order = 1)]
        public string StatusCode { get; set; }

        [DataMember(Order = 2)]
        public string StatusMessage { get; set; }

        [DataMember(Order = 3)]
        public int CustomerId { get; set; }

        [DataMember(Order = 4)]
        public int GuardianCustomerId { get; set; }

        [DataMember(Order = 5)]
        public string  ExpirationDate { get; set; }
    }
}