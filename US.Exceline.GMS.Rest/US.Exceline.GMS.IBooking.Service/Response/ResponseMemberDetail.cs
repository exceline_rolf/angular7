﻿using US.GMS.Core.DomainObjects.IBooking;
using System.Runtime.Serialization;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseMemberDetail
    {
        public ResponseMemberDetail() { }
        public ResponseMemberDetail(ExceIBookingMember member, ResponseStatus status)
        {
            Member = member;
            Status = status;
        }

        [DataMember(Order = 1)]
        public ExceIBookingMember Member { get; set; }
        [DataMember(Order = 1)]
        public ResponseStatus Status { get; set; }
    }
}