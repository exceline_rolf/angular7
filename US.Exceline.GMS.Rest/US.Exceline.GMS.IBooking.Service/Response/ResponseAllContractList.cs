﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseAllContractList
    {
        public ResponseAllContractList() { }

        public ResponseAllContractList(List<ExceIBookingAllContracts> allContractList, ResponseStatus status)
        {
            Contracts = allContractList;
            Status = status;
        }
        [DataMember(Order = 1)]
        public List<ExceIBookingAllContracts> Contracts { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}