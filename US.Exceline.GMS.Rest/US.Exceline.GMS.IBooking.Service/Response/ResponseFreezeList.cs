﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseFreezeList
    {
        public ResponseFreezeList() { }
        public ResponseFreezeList(List<ExceIBookingListFreeze> FreezeList, ResponseStatus status)
        {
            Freeze = FreezeList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingListFreeze> Freeze { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}