﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseClassKeywordList
    {
        public ResponseClassKeywordList() { }
        public ResponseClassKeywordList(List<ExceIBookingClassKeyword> keywordsList, ResponseStatus status)
        {
            KeywordsList = keywordsList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingClassKeyword> KeywordsList { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}