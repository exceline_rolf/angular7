﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    public class ResponseResourceCategorieList
    {
        public ResponseResourceCategorieList() { }
        public ResponseResourceCategorieList(List<ExceIBookingResourceCategoty> resourceCategorieList, ResponseStatus status)
        {
            ResourceCategorieList = resourceCategorieList;
            Status = status;
        }

        [DataMember(Order = 1,Name = "")]
        public List<ExceIBookingResourceCategoty> ResourceCategorieList { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}