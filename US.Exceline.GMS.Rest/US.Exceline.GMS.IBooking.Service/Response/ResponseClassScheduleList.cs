﻿using System.Collections.Generic;
using US.GMS.Core.DomainObjects.IBooking;
using System.Runtime.Serialization;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseClassScheduleList
    {
        public ResponseClassScheduleList() { }
        public ResponseClassScheduleList(List<ExceIBookingClassTime> schedules, ResponseStatus status)
        {
            Schedules = schedules;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingClassTime> Schedules { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}