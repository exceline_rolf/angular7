﻿using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseSystemDetails
    {
        public ResponseSystemDetails() { }
        public ResponseSystemDetails(ExceIBookingSystem system, ResponseStatus status)
        {
            System = system;
            Status = status;
        }

        [DataMember(Order = 1)]
        public ExceIBookingSystem System { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}