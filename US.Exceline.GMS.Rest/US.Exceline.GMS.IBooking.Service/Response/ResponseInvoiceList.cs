﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    public class ResponseInvoiceList
    {
        public ResponseInvoiceList() { }
        public ResponseInvoiceList(List<ExceIBookingInvoice> invoices, ResponseStatus status)
        {
            Invoices = invoices;
            Status = status;
        }

        [DataMember(Order=1)]
        public List<ExceIBookingInvoice> Invoices { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}