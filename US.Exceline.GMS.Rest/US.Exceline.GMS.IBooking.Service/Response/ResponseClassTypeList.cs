﻿using System.Collections.Generic;
using US.GMS.Core.DomainObjects.IBooking;
using System.Runtime.Serialization;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseClassTypeList
    {
        public ResponseClassTypeList() { }
        public ResponseClassTypeList(List<ExceIBookingClassType> classTypeList, ResponseStatus status)
        {
            ClassTypes = classTypeList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingClassType> ClassTypes { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}