﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    public class ResponseResourceAvailableTimeList
    {
        public ResponseResourceAvailableTimeList() { }
        public ResponseResourceAvailableTimeList(List<ExceIBookingResourceAvailableTime> resourceAvailableList, ResponseStatus status)
        {
            ResourceAvailableList = resourceAvailableList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingResourceAvailableTime> ResourceAvailableList { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}