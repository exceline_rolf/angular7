﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    public class ResponseResourceScheduleList
    {
        public ResponseResourceScheduleList() { }
        public ResponseResourceScheduleList(List<ExceIBookingResourceSchedule> resourceScheduleList, ResponseStatus status)
        {
            ResourceScheduleList = resourceScheduleList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingResourceSchedule> ResourceScheduleList { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}