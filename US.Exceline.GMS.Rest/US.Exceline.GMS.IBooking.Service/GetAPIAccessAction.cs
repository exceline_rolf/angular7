﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Service
{
    public class GetAPIAccessAction : USDBActionBase<string>
    {

        private string _endPoint = string.Empty;
        private string _apiKey = string.Empty;
        private string _systemId = string.Empty;
        private string _branchId = string.Empty;



        public GetAPIAccessAction(string endPoint, string apiKey, string systemId, string branchId)
        {
            _endPoint = endPoint;
            _apiKey = apiKey;
            _systemId = systemId;
            _branchId = branchId;
        }

        public GetAPIAccessAction(string endPoint, string apiKey, string systemId)
        {
            _endPoint = endPoint;
            _apiKey = apiKey;
            _systemId = systemId;
            _branchId = null;
        }

        protected override string Body(System.Data.Common.DbConnection connection)
        {
            string value = string.Empty;
            string spName = "ExceWorkStationAPIValidateUser";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@EndPoint", System.Data.DbType.String, _endPoint));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ApiKey", System.Data.DbType.String, _apiKey));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SystemId", System.Data.DbType.Int32, _systemId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@gymId", System.Data.DbType.Int32, _branchId));
                value = command.ExecuteScalar().ToString();
                return value;
            }
            catch (Exception ex)
            {
                return string.Empty;
                // throw;
            }
        }
    }
}