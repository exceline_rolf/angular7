﻿using IO.Swagger.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.IBooking.Service.BRIS
{
    public class BRISSearchData
    {
        [JsonProperty("brukerDtos")]
        public List<BrukerDto> Results { get; set; }
    }
}