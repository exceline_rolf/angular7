﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging.API;
using US.Exceline.GMS.Modules.Shop.API;
using US.Exceline.GMS.Modules.Shop.API.InventoryManagement;
using US.Exceline.GMS.Modules.Shop.API.ManageShop;
using US.Exceline.GMS.Modules.Shop.RestApi.RequestObjects;
using US.GMS.API;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.Shop.RestApi.Controllers
{
    [RoutePrefix("api/ShopSettings")]
    public class ShopSettingsController : ApiController
    {

        
        [HttpGet]
        [Route("GetGymSettings")]
        [Authorize]
        public HttpResponseMessage GetGymSettings(int branchId, GymSettingType gymSettingType)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.GetGymSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("GetSelectedGymSettings")]
        [Authorize]
        public HttpResponseMessage GetSelectedGymSettings(GetSelectedGymSettingsRequest request)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.GetSelectedGymSettings(request.GymSetColNames, request.IsGymSettings, request.BranchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new Dictionary<string, object>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new Dictionary<string, object>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetConfigSettings")]
        [Authorize]
        public HttpResponseMessage GetConfigSettings(string type)
        {
            try
            {
                string config = string.Empty;
                string user = Request.Headers.GetValues("UserName").First();
                if (type == "RPT")
                {
                    config= ConfigurationManager.AppSettings["ReportViewerURL"].ToString();
                }
                
                var result =  config;
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("UpdateSettingForUserRoutine")]
        [Authorize]
        public HttpResponseMessage UpdateSettingForUserRoutine(string key, string value)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSUser.UpdateSettingForUserRoutine(key, value, user, ExceConnectionManager.GetGymCode(user));

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }
    }
}
