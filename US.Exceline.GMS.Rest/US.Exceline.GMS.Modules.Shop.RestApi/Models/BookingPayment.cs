﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Shop.RestApi.Models
{
    public class BookingPayment
    {
        public int BranchId { get; set; }
        public CreditNoteDC CreditNote { get; set; }
        public bool IsReturn { get; set; }
        public List<PayModeDC> PaymentModes { get; set; }
    }
}