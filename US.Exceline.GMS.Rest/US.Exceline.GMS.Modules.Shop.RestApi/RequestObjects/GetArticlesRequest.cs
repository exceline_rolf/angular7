﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Shop.RestApi.RequestObjects
{
    public class GetArticlesRequest
    {

        private int _branchId;

        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private ArticleTypes _categoryType;

        public ArticleTypes CategoryType
        {
            get { return _categoryType; }
            set { _categoryType = value; }
        }

        private string _keyword;

        public string Keyword
        {
            get { return _keyword; }
            set { _keyword = value; }
        }

        private CategoryDC _category;

        public CategoryDC Category
        {
            get { return _category; }
            set { _category = value; }
        }

        private bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        private bool _isFilerByGym;

        public bool IsFilerByGym
        {
            get { return _isFilerByGym; }
            set { _isFilerByGym = value; }
        }

    }
}