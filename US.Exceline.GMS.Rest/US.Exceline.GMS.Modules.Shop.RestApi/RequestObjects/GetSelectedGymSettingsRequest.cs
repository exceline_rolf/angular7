﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Shop.RestApi.RequestObjects
{
    public class GetSelectedGymSettingsRequest
    {
        private int _branchId;

        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private List<string> _gymSetColNames;

        public List<string> GymSetColNames
        {
            get { return _gymSetColNames; }
            set { _gymSetColNames = value; }
        }

        private bool _isGymSettings;

        public bool IsGymSettings
        {
            get { return _isGymSettings; }
            set { _isGymSettings = value; }
        }

    }
}