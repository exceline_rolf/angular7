﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Operations.BusinessLogic.Operations;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Operations.API.Operations
{
    public class GMSOperations
    {
        public static OperationResult<List<ExcACCCommand>> GetCommands(int branchId, string gymCode)
        {
            return OperationsManager.GetCommands(branchId, gymCode);
        }

        public static OperationResult<ExceACCSettings> GetSettings(int branchId, string gymCode)
        {
            return OperationsManager.GetSettings(branchId, gymCode);
        }

        public static OperationResult<List<ExceACCAccessProfileTime>> GetAccessTimes(int branchId, string gymCode)
        {
            return OperationsManager.GetAccessTimes(branchId, gymCode,"yyyy.MM.dd HH:mm:ss");
        }


        public static OperationResult<List<GymACCOpenTime>> GetOpenTimes(int branchId, string gymCode)
        {
            return OperationsManager.GetOpenTimes(branchId, gymCode);
        }

        public static OperationResult<List<ExceACCGymClosedTime>> GetClosedTimes(int branchId, string gymCode)
        {
            return OperationsManager.GetClosedTimes(branchId, gymCode);
        }

        public static OperationResult<List<ExceACCMember>> GetMembers(int branchId, string gymCode)
        {
            return OperationsManager.GetMembers(branchId, gymCode);
        }

        public static OperationResult<string> GetImage(int branchId, string gymCode, string customerNo)
        {
            return OperationsManager.GetImage(branchId, gymCode, customerNo);
        }

        public static OperationResult<string> GetARXMembers(int branchId, string gymCode, bool isAllMember = false)
        {
            return OperationsManager.GetARXMembers(branchId, gymCode, isAllMember);
        }

        public static OperationResult<int> GetSystemId(string gymCode)
        {
            return OperationsManager.GetSystemId(gymCode);
        }

        public static OperationResult<int> AddVisit(string gymCode, List<EntityVisitDC> visitLst)
        {
            return OperationsManager.AddVisitList(gymCode, visitLst);
        }

        public static OperationResult<int> RegisterControl(ExcACCAccessControl accessControl, string gymCode)
        {
            return OperationsManager.RegisterControl(accessControl, gymCode);
        }

        public static OperationResult<int> PingServer(int terminalId, string gymCode)
        {
            return OperationsManager.PingServer(terminalId, gymCode);
        }

        public static OperationResult<int> AddEventLogs(ExcAccessEvent accessEvent, string gymCode)
        {
            return OperationsManager.AddEventLogs(accessEvent, gymCode);
        }

        public static OperationResult<ExceACCTerminal> GetTerminalDetails(int terminalID, string gymCode)
        {
            return OperationsManager.GetTerminalDetails(terminalID, gymCode);
        }

        public static OperationResult<int> GetAvailableVisitByContractId(int contractId, string gymCode)
        {
            return OperationsManager.GetAvailableVisitByContractId(contractId, gymCode);
        }
    }
}
