﻿using SessionManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.ManageSesion.RestApi.Controllers
{
    public class SessionController : ApiController
    {
        [HttpGet]
        [Route("GetSessionValue")]
        [Authorize]
        public HttpResponseMessage GetSessionValue(string sessionKey)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                string sessionValue = Session.GetSession(sessionKey, ExceConnectionManager.GetGymCode(user));
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(sessionValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }
    }
}
