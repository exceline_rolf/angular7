﻿
using System.Runtime.Serialization;
namespace US.Payment.Core.SystemObjects.CustomSetting
{
    [DataContract]
    public class USPCustomSettingDataType
    {
        public USPCustomSettingDataType()
        {
            ID = -1;
            DataType = string.Empty;
            Description = string.Empty;
            DefaultValue = string.Empty;

        }
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string DataType { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string DefaultValue { get; set; }

    }
}
