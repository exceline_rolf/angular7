﻿using System;

namespace US.Payment.Core.SystemObjects
{
    public class SQLDBDataTypes
    {

        public static TypeCode GetNativeDataType(string sqlDataType)
        {
            TypeCode dbDataType = new TypeCode();
            switch (sqlDataType)
	        {
                case "xml":
                    dbDataType= TypeCode.String;
                    break;
                case "int":
                    dbDataType = TypeCode.Int32;
                    break;
                case "decimal":
                    dbDataType = TypeCode.Decimal;
                    break;
                case "varbinary":
                    dbDataType = TypeCode.Object;
                    break;
                case "smallint":
                    dbDataType = TypeCode.Int32;
                    break;
                case "datetime":
                    dbDataType = TypeCode.DateTime;
                    break;
                case "varchar":
                    dbDataType = TypeCode.String;
                    break;
                case "numeric":
                    dbDataType = TypeCode.Double;
                    break;
                case "nchar":
                    dbDataType = TypeCode.String;
                    break;
                case "smalldatetime":
                    dbDataType = TypeCode.DateTime;
                    break;
                case "float":
                    dbDataType = TypeCode.Decimal;
                    break;
                case "date":
                    dbDataType = TypeCode.DateTime;
                    break;

                case "char":
                    dbDataType = TypeCode.String;
                    break;

                case "bit":
                    dbDataType = TypeCode.String;
                    break;
                case "nvarchar":
                    dbDataType = TypeCode.String;
                    break;
		        
	        }


            return dbDataType;
        
        }

        public static string GetNativeDataTypeDefalutValue(string sqlDataType)
        {
            string dbDataType = string.Empty;
            switch (sqlDataType)
            {
                case "xml":
                    dbDataType = string.Empty;
                    break;
                case "int":
                    dbDataType = "-1";
                    break;
                case "decimal":
                    dbDataType = "0";
                    break;
                case "varbinary":
                    dbDataType = " ";
                    break;
                case "smallint":
                    dbDataType = "-1";
                    break;
                case "datetime":
                    dbDataType = DateTime.MinValue.ToString("yyyy-MM-dd");
                    break;
                case "varchar":
                    dbDataType = "";
                    break;
                case "numeric":
                    dbDataType = "0";
                    break;
                case "nchar":
                    dbDataType = "";
                    break;
                case "smalldatetime":
                    dbDataType = DateTime.MinValue.ToString("yyyy-MM-dd");
                    break;
                case "float":
                    dbDataType = "0";
                    break;
                case "date":
                    dbDataType = DateTime.MinValue.ToString("yyyy-MM-dd");
                    break;
                case "char":
                    dbDataType = "";                    
                    break;

                case "bit":
                    dbDataType = "0";
                    break;
                case "nvarchar":
                    dbDataType = "";
                    break;

            }


            return dbDataType;

        }
    }

   
}
