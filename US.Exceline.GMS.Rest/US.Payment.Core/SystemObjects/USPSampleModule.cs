﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace US.Payment.Core.SystemObjects
{
    [ServiceContract]
    public class USPSampleModule : USPModule 
    {
        private string _id = string.Empty;
        [DataMember]
        public override string ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        private string _displayName = string.Empty;
        [DataMember]
        public override string DisplayName
        {
            get
            {
                return _displayName;
            }
            set
            {
                _displayName = value;
            }
        }

        private string _moduleHome = string.Empty;
        [DataMember]
        public override string ModuleHome
        {
            get
            {
                return _moduleHome;
            }
            set
            {
                _moduleHome = value;
            }
        }

        private USPFeature _feature = new USPSampleFeature();
        public override USPFeature DeafaultFeature
        {
            get
            {
                return _feature;
            }
            set
            {
                _feature = value;
            }
        }
        private string _thumbnailImage = string.Empty;
        public override string ThumbnailImage
        {
            get
            {
                return _thumbnailImage;
            }
            set
            {
                _thumbnailImage = value;
            }
        }
        private List<USPFeature> _features = new List<USPFeature>();

        public override List<USPParameter> GetParamteList()
        {
            return new List<USPParameter>();
        }
        public override  object GetParamterValue(string name)
        {
            return "SmapleValue";
        }

        public override List<DomainObjects.SystemObjects.IOperation> GetOperations()
        {
            throw new NotImplementedException();
        }

        public override string ModulePackage
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
