﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.Payment.Core.DomainObjects.SystemObjects;

namespace US.Payment.Core.SystemObjects
{
    public abstract class USPFeature
    {
        public abstract string ID { get; set; }
        public abstract string DisplayName { get; set; }
        public abstract string FeatureHome { get; set; }
        public abstract string ThumbnailImage { get; set; }
        public abstract int Priority { get; set; }
        public abstract List<IOperation> GetOperations();
        //public abstract 
    }
}
