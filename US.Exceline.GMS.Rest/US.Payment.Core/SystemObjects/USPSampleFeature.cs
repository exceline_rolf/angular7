﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace US.Payment.Core.SystemObjects
{
    [ServiceContract]
    public class USPSampleFeature :USPFeature 
    {
        #region USPFeature Members
        private string _id = string.Empty;
        [DataMember]
        public override  string ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        private string _displayName = string.Empty;
        [DataMember]
        public override string DisplayName
        {
            get
            {
                return _displayName;
            }
            set
            {
                _displayName = value;
            }
        }
        private string _homeControl = string.Empty;
        [DataMember]
        public override string FeatureHome
        {
            get
            {
                return _homeControl;
            }
            set
            {
                _homeControl = value;
            }
        }
        private string _thnumbnailImage = string.Empty;
        [DataMember]
        public override string ThumbnailImage
        {
            get
            {
                return _thnumbnailImage;
            }
            set
            {
                _thnumbnailImage = value;
            }
        }
        private int _priority = -1;
        [DataMember]
        public override int Priority
        {
            get
            {
                return _priority;
            }
            set
            {
                _priority = value;
            }
        }

        #endregion


        public override List<DomainObjects.SystemObjects.IOperation> GetOperations()
        {
            throw new NotImplementedException();
        }
    }
}
