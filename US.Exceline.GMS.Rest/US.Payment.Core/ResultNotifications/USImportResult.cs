﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.Enums;
using System.Collections.ObjectModel;

namespace US.Payment.Core.ResultNotifications
{
    /// <summary>
    /// This class inherited from USImportResult
    /// Reprecent a method executing result when input to the method is a single object(ont a list).
    /// This class can use with methods
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class USImportResult<T> : USImportResult
    {
        private T _methodReturnValue;

        public T MethodReturnValue
        {
            get
            {
                return _methodReturnValue;
            }
            set
            {
                _methodReturnValue = value;
            }
        }
    }

    /// <summary>
    /// Reprecent a method executing result when input to the method is a single object(ont a list).
    /// This class can use with void methods(methods those not returning any value)
    /// </summary>
    public class USImportResult
    {
        public USImportResult()
        {
            _notificationMessages = new List<USNotificationMessage>();
            _errorOccured = false;
        }
        private object _tag2;
        /// <summary>
        /// User disired tag
        /// </summary>
        public object Tag2
        {
            get
            {
                return _tag2;
            }
            set
            {
                _tag2 = value;
            }
        }

        private object _tag1;
        /// <summary>
        /// User disired tag
        /// </summary>
        public object Tag1
        {
            get
            {
                return _tag1;
            }
            set
            {
                _tag1 = value;
            }
        }

        private object _tag3;
        /// <summary>
        /// User disired tag
        /// </summary>
        public object Tag3
        {
            get
            {
                return _tag3;
            }
            set
            {
                _tag3 = value;
            }
        }

        private object _tagSavedRecordID;
        /// <summary>
        /// Unique ID given in the USP to the successfuly saved redords. Can have value only for operations
        /// like adding, updaing
        /// </summary>
        public object TagSavedRecordID
        {
            get
            {
                return _tagSavedRecordID;
            }
            set
            {
                _tagSavedRecordID = value;
            }
        }
        public void AddUSNotificationMessage(USNotificationMessage notificationMessage)
        {
            if (notificationMessage.ErrorLevel == ErrorLevels.Error)
            {
                _errorOccured = true;
            }
            _notificationMessages.Add(notificationMessage);
        }
        public void AddUSNotificationMessage(IList<USNotificationMessage> notificationMessages)
        {
            foreach (USNotificationMessage notificationMessage in notificationMessages)
            {
                if (notificationMessage.ErrorLevel == ErrorLevels.Error)
                {
                    _errorOccured = true;
                }
                _notificationMessages.Add(notificationMessage);
            }
           
           
        }

        private bool _errorOccured;
        /// <summary>
        /// Whether opertation is succeeded
        /// </summary>
        public bool ErrorOccured
        {
            get
            {
                return _errorOccured;
            }
            set
            {
                _errorOccured = value;
            }
        }


        private List<USNotificationMessage> _notificationMessages;
        /// <summary>
        /// List of message
        /// </summary>
        public ReadOnlyCollection<USNotificationMessage> NotificationMessages
        {
            get
            {
                return new ReadOnlyCollection<USNotificationMessage>(_notificationMessages);
            }
        }
        
    }
}
