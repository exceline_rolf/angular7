﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.Enums;

namespace US.Payment.Core.ResultNotifications
{
    /// <summary>
    /// Reprecent a message when giving method executing results
    /// </summary>
    public class USNotificationMessage
    {
        public USNotificationMessage(USMessageTypes messageType, string message)
        {
            _messageType = messageType;
            _message = message;
        }
        public USNotificationMessage(ErrorLevels  errorLeve, string message)
        {
            _errorLevel = errorLeve;
            _message = message;
        }
        private USMessageTypes _messageType = USMessageTypes.ERROR;
        public USMessageTypes MessageType
        {
            get { return _messageType; }
            set { _messageType = value; }
        }
        private ErrorLevels _errorLevel = ErrorLevels.Error;

        /// <summary>
        /// Error leve, whether error,warning or info
        /// </summary>
        public ErrorLevels ErrorLevel
        {
            get
            {
                return _errorLevel;
            }
            set
            {
                _errorLevel = value;
            }
        }


        private string _message;
        /// <summary>
        /// Message
        /// </summary>
        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
            }
        }
    }
}
