﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.ResultNotifications
{
    public class OperationResultValue<T> : USOperationResultValue
    {
        private T _operationReturnValue;
        public T OperationReturnValue
        {
            get { return _operationReturnValue; }
            set { _operationReturnValue = value; }
        }
    }
}
