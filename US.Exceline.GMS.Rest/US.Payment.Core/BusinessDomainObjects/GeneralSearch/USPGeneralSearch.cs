﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.Service
// Coding Standard   : US Coding Standards
// Author            : AAB
// Modified          : ISU
// Created Timestamp : 01/12/2011 HH:MM  PM
// --------------------------------------------------------------------------

using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects.GeneralSearch
{
    [DataContract]
    public class USPGeneralSearch
    {
        private List<USPDebtorSearch> _debtorList = new List<USPDebtorSearch>();
        [DataMember]
        public List<USPDebtorSearch> DebtorList
        {
            get { return _debtorList; }
            set { _debtorList = value; }
        }
        private List<USPCaseSearch> _creditorList = new List<USPCaseSearch>();
        [DataMember]
        public List<USPCaseSearch> CreditorList
        {
            get { return _creditorList; }
            set { _creditorList = value; }
        }
        private List<USPCreditorSearch> _caseList = new List<USPCreditorSearch>();
        [DataMember]
        public List<USPCreditorSearch> CaseList
        {
            get { return _caseList; }
            set { _caseList = value; }
        }
        private List<USPSubCaseSearch> _subCaseList = new List<USPSubCaseSearch>();
        [DataMember]
        public List<USPSubCaseSearch> SubCaseList
        {
            get { return _subCaseList; }
            set { _subCaseList = value; }
        }

    }
}
