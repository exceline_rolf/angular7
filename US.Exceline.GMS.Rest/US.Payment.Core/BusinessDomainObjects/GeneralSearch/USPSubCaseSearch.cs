﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.Service
// Coding Standard   : US Coding Standards
// Author            : AAB
// Modified          : ISU
// Created Timestamp : 01/12/2011 HH:MM  PM
// --------------------------------------------------------------------------

using System.Runtime.Serialization;
namespace US.Payment.Core.BusinessDomainObjects.GeneralSearch
{
    [DataContract]
    public class USPSubCaseSearch
    {
        private int _subCaseNo;
        private string _KID;
        private string _invoiceNumber;
        private int _ArNo;
        private string _creditorNo;
        private string _creditorName;
        private string _debtorName;
        private string _hit;
        private int _dataSource = -1;
        private int _subCaseState = 0;
        private string _colour;

        public USPSubCaseSearch()
        {
            _subCaseNo = -1;
            _KID = string.Empty;
            _invoiceNumber = string.Empty;
            _ArNo = -1;
            _creditorNo = string.Empty;
            _creditorName = string.Empty;
            _debtorName = string.Empty;
            _hit = string.Empty;
            _dataSource = -1;
            _subCaseState = -1;
        }

        [DataMember]
        public int DataSource
        {
            get { return _dataSource; }
            set { _dataSource = value; }
        }
        [DataMember]
        public int SubCaseState
        {
            get { return _subCaseState; }
            set { _subCaseState = value; }
        }
        [DataMember]
        public int SubCaseNo
        {
            get { return _subCaseNo; }
            set { _subCaseNo = value; }
        }
        [DataMember]
        public string KID
        {
            get { return _KID; }
            set { _KID = value; }
        }
        [DataMember]
        public string InvoiceNumber
        {
            get { return _invoiceNumber; }
            set { _invoiceNumber = value; }
        }

        [DataMember]
        public int ArNo
        {
            get { return _ArNo; }
            set { _ArNo = value; }
        }

        [DataMember]
        public string CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }

        [DataMember]
        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }
        }

        [DataMember]
        public string DebtorName
        {
            get { return _debtorName; }
            set { _debtorName = value; }
        }
        private string _debtorNo;
        [DataMember]
        public string DebtorNo
        {
            get { return _debtorNo; }
            set { _debtorNo = value; }
        }

        [DataMember]
        public string Hit
        {
            get { return _hit; }
            set { _hit = value; }
        }
        [DataMember]
      public string Colour
        {
            get { return _colour; }
            set { _colour = value; }
        }
    }
}

//SELECT DISTINCT SubCaseNo,KID,Ref,T1.ARNo,C.CreditorInkassoID As CreditorNo, C.Name As CreditorName
//   ,D.CustId as DebtorNo , D.Name as DebtorName, T1.hit
//   FROM #temp T1
//   INNER JOIN dbo.USP_CreditorInfo C ON C.ARNo=T1.ARNo
//   INNER JOIN dbo.USP_DebitorInfo  D ON D.ARNo=T1.ARNo
//   Order by T1.ARNo
//   DROP TABLE #temp
