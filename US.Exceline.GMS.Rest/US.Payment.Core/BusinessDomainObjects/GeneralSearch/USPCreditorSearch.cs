﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.Service
// Coding Standard   : US Coding Standards
// Author            : AAB
// Modified          : ISU
// Created Timestamp : 01/12/2011 HH:MM  PM
// --------------------------------------------------------------------------

using System.Runtime.Serialization;
namespace US.Payment.Core.BusinessDomainObjects.GeneralSearch
{
    [DataContract]
    public class USPCreditorSearch
    {
        private int _dataSource = -1;
        [DataMember]
        public int DataSource
        {
            get { return _dataSource; }
            set { _dataSource = value; }
        }

        private string _hitField = string.Empty;
        [DataMember]
        public string HitField
        {
            get { return _hitField; }
            set { _hitField = value; }
        }
        
        private string _creditorNumber = string.Empty;
        [DataMember]
        public string CreditorNumber
        {
            get { return _creditorNumber; }
            set { _creditorNumber = value; }
        }
        private string _creditorName = string.Empty;
        [DataMember]
        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }
        }
        private string _teleHome = string.Empty;
        [DataMember]
        public string TeleHome
        {
            get { return _teleHome; }
            set { _teleHome = value; }
        }
        private string _teleMobile = string.Empty;
        [DataMember]
        public string TeleMobile
        {
            get { return _teleMobile; }
            set { _teleMobile = value; }
        }
        private string _teleWork = string.Empty;
        [DataMember]
        public string TeleWork
        {
            get { return _teleWork; }
            set { _teleWork = value; }
        }
        private string _email = string.Empty;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

       
    }
}

