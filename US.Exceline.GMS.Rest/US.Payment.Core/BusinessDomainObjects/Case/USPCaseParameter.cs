﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects.Case
{
    [DataContract]
    public class USPCaseParameter
    {
        //group 01

        private int _caseNo = -1;
        [DataMember]
        public int CaseNo
        {
            get { return _caseNo; }
            set { _caseNo = value; }
        }


        //private DateTime _closedDate = new DateTime();
        //[DataMember]
        //public DateTime ClosedDate
        //{
        //    get { return _closedDate; }
        //    set { _closedDate = value; }
        //}

        private DateTime _interestObselationDate = new DateTime();
        [DataMember] 
        public DateTime InterestObselationDate
        {
            get { return _interestObselationDate; }
            set { _interestObselationDate = value; }
        }

        private DateTime _interestFromDate = new DateTime();
        [DataMember]
        public DateTime InterestFromDate
        {
            get { return _interestFromDate; }
            set { _interestFromDate = value; }
        }

        private Decimal _interestCost = new Decimal();
        [DataMember]
        public Decimal InterestCost
        {
            get { return _interestCost; }
            set { _interestCost = value; }
        }

        //group2
        //private DateTime _paymentDemandDate = new DateTime();
        //[DataMember]
        //public DateTime PaymentDemandDate
        //{
        //    get { return _paymentDemandDate; }
        //    set { _paymentDemandDate = value; }
        //}

        private DateTime _vtSentDate = new DateTime();
        [DataMember]
        public DateTime VtSentDate
        {
            get { return _vtSentDate; }
            set { _vtSentDate = value; }
        }

        private DateTime _lastvtSentDate = new DateTime();
        [DataMember]
        public DateTime LastvtSentDate
        {
            get { return _lastvtSentDate; }
            set { _lastvtSentDate = value; }
        }

        private DateTime _payNoteDate = new DateTime();
        [DataMember]
        public DateTime PayNoteDate
        {
            get { return _payNoteDate; }
            set { _payNoteDate = value; }
        }

        private DateTime _closeDate = new DateTime();
        [DataMember]
        public DateTime CloseDate
        {
            get { return _closeDate; }
            set { _closeDate = value; }
        }

        private bool _interestCalcStatus = false;
        [DataMember]
        public bool InterestCalcStatus
        {
            get { return _interestCalcStatus; }
            set { _interestCalcStatus = value; }
        }

        private bool _reportStatus = false;
        [DataMember]
        public bool ReportStatus
        {
            get { return _reportStatus; }
            set { _reportStatus = value; }
        }

        private bool _feeStatus = false;
        [DataMember]
        public bool FeeStatus
        {
            get { return _feeStatus; }
            set { _feeStatus = value; }
        }

        //private bool _blockedForNewDemands = false;
        //[DataMember]
        //public bool BlockedForNewDemands
        //{
        //    get { return _blockedForNewDemands; }
        //    set { _blockedForNewDemands = value; }
        //}

        private string _caseType = string.Empty;
        [DataMember]
        public string CaseType
        {
            get { return _caseType; }
            set { _caseType = value; }
        }

        //private string _cref2 = string.Empty;
        //[DataMember]
        //public string Cref2
        //{
        //    get { return _cref2; }
        //    set { _cref2 = value; }
        //}

        //private string _caseConserns = string.Empty;
        //[DataMember]
        //public string CaseConserns
        //{
        //    get { return _caseConserns; }
        //    set { _caseConserns = value; }
        //}

        //private string _batchNo = string.Empty;
        //[DataMember]
        //public string BatchNo
        //{
        //    get { return _batchNo; }
        //    set { _batchNo = value; }
        //}

        //private decimal _interestPercentageCost = 0;
        //[DataMember]
        //public decimal InterestPercentageCost
        //{
        //    get { return _interestPercentageCost; }
        //    set { _interestPercentageCost = value; }
        //}

        private decimal _interestPrincipleSum = 0;
        [DataMember]
        public decimal InterestPrincipleSum
        {
            get { return _interestPrincipleSum; }
            set { _interestPrincipleSum = value; }
        }

        //private DateTime _dueDate = new DateTime();
        //[DataMember]
        //public DateTime DueDate
        //{
        //    get { return _dueDate; }
        //    set { _dueDate = value; }
        //}

        private DateTime _nextDueDate = new DateTime();
        [DataMember]
        public DateTime NextDueDate
        {
            get { return _nextDueDate; }
            set { _nextDueDate = value; }
        }

        private string _caseDescription = string.Empty;
        [DataMember]
        public string CaseDescription
        {
            get { return _caseDescription; }
            set { _caseDescription = value; }
        }

        private DateTime _lastPayNoteDate;
        [DataMember]
        public DateTime LastPayNoteDate
        {
            get { return _lastPayNoteDate; }
            set { _lastPayNoteDate = value; }
        }



    }
}
