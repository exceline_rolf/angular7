﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects.Case
{
    [DataContract]
    public class Enforcement
    {
        private int _enforcementId = -1;
        private int _caseNumber = -1;
        private string _enforcementType = string.Empty;
        private int _objectChargedNumber = -1;//ThingNo
        private string _objectChargedName = string.Empty;//assert description from assert tbl
        private int _thingType = -1;//0-4 from enforcementType 
        private DateTime _setUpDate = DateTime.MinValue;
        private double _faceValue = 0.0d;
        private DateTime _caducityDate = DateTime.MinValue;
        private DateTime _registrationDate = DateTime.MinValue;
        private string _comment = string.Empty;
        private double _valuationAmount = 0.0d;

        [DataMember]
        public double ValuationAmount
        {
            get { return _valuationAmount; }
            set { _valuationAmount = value; }
        }

        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        [DataMember]
        public DateTime RegistrationDate
        {
            get { return _registrationDate; }
            set { _registrationDate = value; }
        }

        [DataMember]
        public DateTime CaducityDate
        {
            get { return _caducityDate; }
            set { _caducityDate = value; }
        }

        [DataMember]
        public double FaceValue
        {
            get { return _faceValue; }
            set { _faceValue = value; }
        }

        [DataMember]
        public DateTime SetUpDate
        {
            get { return _setUpDate; }
            set { _setUpDate = value; }
        }

        [DataMember]
        public int ThingType
        {
            get { return _thingType; }
            set { _thingType = value; }
        }

        [DataMember]
        public string ObjectChargedName
        {
            get { return _objectChargedName; }
            set { _objectChargedName = value; }
        }

        [DataMember]
        public int ObjectChargedNumber
        {
            get { return _objectChargedNumber; }
            set { _objectChargedNumber = value; }
        }

        [DataMember]
        public string EnforcementType
        {
            get { return _enforcementType; }
            set { _enforcementType = value; }
        }

        [DataMember]
        public int CaseNumber
        {
            get { return _caseNumber; }
            set { _caseNumber = value; }
        }

        [DataMember]
        public int EnforcementId
        {
            get { return _enforcementId; }
            set { _enforcementId = value; }
        }
    }
}
