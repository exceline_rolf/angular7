﻿using System.Runtime.Serialization;
using US.Payment.Core.BusinessDomainObjects.BasicData;

namespace US.Payment.Core.BusinessDomainObjects.Case
{
    [DataContract]
    public class USPEmployer
    {
        private int _id = -1;
        private string _name = string.Empty;
        private string _address1 = string.Empty;
        private string _address2 = string.Empty;
        private PostalArea _postalArea = new PostalArea();
        private string _country = string.Empty;
        private string _organizationNo = string.Empty;
        private string _accountNo = string.Empty;
        private string _contactPerson = string.Empty;
        private bool _selected = false;

        [DataMember]
        public PostalArea PostalArea
        {
            get { return _postalArea; }
            set { _postalArea = value; }
        }


        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [DataMember]
        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        [DataMember]
        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        [DataMember]
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        [DataMember]
        public string OrganizationNo
        {
            get { return _organizationNo; }
            set { _organizationNo = value; }
        }

        [DataMember]
        public string AccountNo
        {
            get { return _accountNo; }
            set { _accountNo = value; }
        }

        [DataMember]
        public string ContactPerson
        {
            get { return _contactPerson; }
            set { _contactPerson = value; }
        }

        [DataMember]
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }
    }
}
