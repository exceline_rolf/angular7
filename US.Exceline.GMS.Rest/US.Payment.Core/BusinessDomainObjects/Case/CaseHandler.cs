﻿using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects.Case
{
    [DataContract]
    public class CaseHandler
    {
        private int _iD = -1;
        [DataMember]
        public int ID
        {
            get { return _iD; }
            set { _iD = value; }
        }

        private string _displayName = string.Empty;
        [DataMember]
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        private string _externalUserName = string.Empty;
        [DataMember]
        public string ExternalUserName
        {
            get { return _externalUserName; }
            set { _externalUserName = value; }
        }
    }
}
