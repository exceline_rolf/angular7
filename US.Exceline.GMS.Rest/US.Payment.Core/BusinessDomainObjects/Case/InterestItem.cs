﻿using System;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects.Case
{
    [DataContract]
    public class InterestItem
    {
        private int _ID = -1;
        [DataMember]
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        private int _caseNo = -1;
        [DataMember]
        public int CaseNo
        {
            get { return _caseNo; }
            set { _caseNo = value; }
        }
        private int _linkNo = -1;
        [DataMember]
        public int LinkNo
        {
            get { return _linkNo; }
            set { _linkNo = value; }
        }
        private int _subCaseNo = -1;
        [DataMember]
        public int SubCaseNo
        {
            get { return _subCaseNo; }
            set { _subCaseNo = value; }
        }
        private int _arItemNo = -1;
        [DataMember]
        public int ArItemNo
        {
            get { return _arItemNo; }
            set { _arItemNo = value; }
        }
        private decimal _baseAmount = 0;
        [DataMember]
        public decimal BaseAmount
        {
            get { return _baseAmount; }
            set { _baseAmount = value; }
        }
        private DateTime _fromDate;
        [DataMember]
        public DateTime FromDate
        {
            get { return _fromDate; }
            set { _fromDate = value; }
        }
        private DateTime _endDate;
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }
        private decimal _IRate = 0;
        [DataMember]
        public decimal IRate
        {
            get { return _IRate; }
            set { _IRate = value; }
        }
        private decimal _IAmount = 0;
        [DataMember]
        public decimal IAmount
        {
            get { return _IAmount; }
            set { _IAmount = value; }
        }
        private int _leapYear = 0;
        [DataMember]
        public int LeapYear
        {
            get { return _leapYear; }
            set { _leapYear = value; }
        }
        private bool _obsolete = false;
        [DataMember]
        public bool Obsolete
        {
            get { return _obsolete; }
            set { _obsolete = value; }
        }
        private int _IDays = 0;
        [DataMember]
        public int IDays
        {
            get { return _IDays; }
            set { _IDays = value; }
        }
        private DateTime _regDate;
        [DataMember]
        public DateTime RegDate
        {
            get { return _regDate; }
            set { _regDate = value; }
        }
        private string _user;
        [DataMember]
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }
    }
}
