﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment 
// Project Name      : US.Payment.PM.Dashboard
// Coding Standard   : US Coding Standards
// Author            : TMA
// Created Timestamp : 3/8/2012 HH:MM  PM
// --------------------------------------------------------------------------

using System;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects.Case
{
    [DataContract]
    public class PartPaymentSchema
    {
        private DateTime _dueDate = DateTime.MinValue;
        [DataMember]
        public  DateTime DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        private decimal _termAmount = 0;
        [DataMember]
        public  decimal TermAmount
        {
            get { return _termAmount; }
            set { _termAmount = value; }
        }

        private decimal _termCharge = 0;
        [DataMember]
        public  decimal TermCharge
        {
            get { return _termCharge; }
            set { _termCharge = value; }
        }

        private decimal _interest = 0;
        [DataMember]
        public  decimal Interest
        {
            get { return _interest; }
            set { _interest = value; }
        }

        private decimal _partPayment = 0;
        [DataMember]
        public  decimal PartPayment
        {
            get { return _partPayment; }
            set { _partPayment = value; }
        }

        private int _partPaymentNo = 1;
        [DataMember]
        public  int PartPaymentNo
        {
            get { return _partPaymentNo; }
            set { _partPaymentNo = value; }
        }
       
        private decimal _remainingBalance = 0;
        [DataMember]
        public  decimal RemainingBalance
        {
            get { return _remainingBalance; }
            set { _remainingBalance = value; }
        }
    }
}
