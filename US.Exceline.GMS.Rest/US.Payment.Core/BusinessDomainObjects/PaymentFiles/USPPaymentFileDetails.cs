﻿using System;

namespace US.Payment.Core.BusinessDomainObjects.PaymentFiles
{
    public class USPPaymentFileDetails
    {
        private string _fileName = string.Empty;
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }
        private DateTime _fileDate = DateTime.MinValue;
        public DateTime FileDate
        {
            get { return _fileDate; }
            set { _fileDate = value; }
        }
        private int _fileType = -1;
        public int FileType
        {
            get { return _fileType; }
            set { _fileType = value; }
        }
        private string _errorPaymentType = string.Empty;

        public string ErrorPaymentType
        {
            get { return _errorPaymentType; }
            set { _errorPaymentType = value; }
        }

        private double _totalNoOfRecordsInFile = 0;

        public double TotalNoOfRecordsInFile
        {
            get { return _totalNoOfRecordsInFile; }
            set { _totalNoOfRecordsInFile = value; }
        }
        private double _fileTotalAmount = 0;

        public double FileTotalAmount
        {
            get { return _fileTotalAmount; }
            set { _fileTotalAmount = value; }
        }
        private double _matchedRecords = 0;

        public double MatchedRecords
        {
            get { return _matchedRecords; }
            set { _matchedRecords = value; }
        }
        private double _matchedTotal = 0;

        public double MatchedTotal
        {
            get { return _matchedTotal; }
            set { _matchedTotal = value; }
        }
        private double _tooMuchPaidRecords = 0;

        public double TooMuchPaidRecords
        {
            get { return _tooMuchPaidRecords; }
            set { _tooMuchPaidRecords = value; }
        }
        private double _tooMuchPaidRecordsTotal = 0;

        public double TooMuchPaidRecordsTotal
        {
            get { return _tooMuchPaidRecordsTotal; }
            set { _tooMuchPaidRecordsTotal = value; }
        }
        private double _unKnownReocrds = 0;

        public double UnKnownReocrds
        {
            get { return _unKnownReocrds; }
            set { _unKnownReocrds = value; }
        }
        private double _unKnownRecordsTotal = 0;

        public double UnKnownRecordsTotal
        {
            get { return _unKnownRecordsTotal; }
            set { _unKnownRecordsTotal = value; }
        }
        private int fileId = -1;
        public int FileId
        {
            get { return fileId; }
            set { fileId = value; }
        }

    }
}
