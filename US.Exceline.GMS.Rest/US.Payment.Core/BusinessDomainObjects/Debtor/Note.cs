﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects.Debtor
{
    [DataContract]
    public class Note
    {

        private string _subject = string.Empty;
        [DataMember]
        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }

        private string _text = string.Empty;
        [DataMember]
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private string _direction = string.Empty;   //can be an enum
        [DataMember]
        public string Direction
        {
            get { return _direction; }
            set { _direction = value; }
        }

        private string _createdTime = string.Empty;
        [DataMember]
        public string CreatedTime
        {
            get { return _createdTime; }
            set { _createdTime = value; }
        }

    }
}
