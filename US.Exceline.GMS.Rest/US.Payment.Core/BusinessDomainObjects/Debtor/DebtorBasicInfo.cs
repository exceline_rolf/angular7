﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.Payment.Core.BusinessDomainObjects.Creditor;

namespace US.Payment.Core.BusinessDomainObjects.Debtor
{
    [DataContract]
    public class DebtorBasicInfo
    {
     
        private string _fullName = string.Empty;
        [DataMember]
        public string FullName
        {
            get { return _fullName; }
            set { _fullName = value; }
        }

        private string _firstName = string.Empty;
        [DataMember]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _lastName = string.Empty;
        [DataMember]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private string _debtorNo = string.Empty;
        [DataMember]
        public string DebtorNo
        {
            get { return _debtorNo; }
            set { _debtorNo = value; }
        }

        private int _noOfCreditors = 0;
        [DataMember]
        public int NoOfCreditors
        {
            get { return _noOfCreditors; }
            set { _noOfCreditors = value; }
        }

        private string _homePhoneNumber = string.Empty;
        [DataMember]
        public string HomePhoneNumber
        {
            get { return _homePhoneNumber; }
            set { _homePhoneNumber = value; }
        }

        private string _workfPhoneNumber = string.Empty;
        [DataMember]
        public string WorkfPhoneNumber
        {
            get { return _workfPhoneNumber; }
            set { _workfPhoneNumber = value; }
        }

        private string _mobileNumber = string.Empty;
        [DataMember]
        public string MobileNumber
        {
            get { return _mobileNumber; }
            set { _mobileNumber = value; }
        }

        private string _smsNumber = string.Empty;
        [DataMember]
        public string SmsNumber
        {
            get { return _smsNumber; }
            set { _smsNumber = value; }
        }

        private string _email = string.Empty;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _skype = string.Empty;
        [DataMember]
        public string Skype
        {
            get { return _skype; }
            set { _skype = value; }
        }

        
        private string _msn = string.Empty;
        [DataMember]
        public string Msn
        {
            get { return _msn; }
            set { _msn = value; }
        }

        private string _branch = string.Empty;
        [DataMember]
        public string Branch
        {
            get { return _branch; }
            set { _branch = value; }
        }

        private string _postalAddress = string.Empty;
        [DataMember]
        public string PostalAddress
        {
            get { return _postalAddress; }
            set { _postalAddress = value; }
        }

        private string _visitAddress = string.Empty;
        [DataMember]
        public string VisitAddress
        {
            get { return _visitAddress; }
            set { _visitAddress = value; }
        }


        private string _postalCode = string.Empty;
        [DataMember]
        public string PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }

        private string _postalName = string.Empty;
        [DataMember]
        public string PostalName
        {
            get { return _postalName; }
            set { _postalName = value; }
        }

        private string _city = string.Empty;
        [DataMember]
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        private int _municipalityId = -1;
        [DataMember]
        public int MunicipalityId
        {
            get { return _municipalityId; }
            set { _municipalityId = value; }
        }

        private string _municipalityCode = string.Empty;
        [DataMember]
        public string MunicipalityCode
        {
            get { return _municipalityCode; }
            set { _municipalityCode = value; }
        }

        private string _municipalityName = string.Empty;
        [DataMember]
        public string MunicipalityName
        {
            get { return _municipalityName; }
            set { _municipalityName = value; }
        }
        private string _countryCode = string.Empty;
        [DataMember]
        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }

        private string _countryName = string.Empty;
        [DataMember]
        public string CountryName
        {
            get { return _countryName; }
            set { _countryName = value; }
        }

        private int _score = 0;
        [DataMember]
        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        private decimal _payment = 0;
        [DataMember]
        public decimal Payment
        {
            get { return _payment; }
            set { _payment = value; }
        }

        private decimal _income = 0;
        [DataMember]
        public decimal Income
        {
            get { return _income; }
            set { _income = value; }
        }

        private string _usPaymentOpen = string.Empty;
        [DataMember]
        public string UsPaymentOpen
        {
            get { return _usPaymentOpen; }
            set { _usPaymentOpen = value; }
        }

        private string _collectionOpen = string.Empty;
        [DataMember]
        public string CollectionOpen
        {
            get { return _collectionOpen; }
            set { _collectionOpen = value; }
        }

        private DateTime _lastLoginPortal = DateTime.MinValue;
        [DataMember]
        public DateTime LastLoginPortal
        {
            get { return _lastLoginPortal; }
            set { _lastLoginPortal = value; }
        }

        private DateTime _lastPaymentDate = DateTime.MinValue;
        [DataMember]
        public DateTime LastPaymentDate
        {
            get { return _lastPaymentDate; }
            set { _lastPaymentDate = value; }
        }

        private DateTime _lastContactDate = DateTime.MinValue;
        [DataMember]
        public DateTime LastContactDate
        {
            get { return _lastContactDate; }
            set { _lastContactDate = value; }
        }

        private string _caseNo = string.Empty;
        [DataMember]
        public string CaseNo
        {
            get { return _caseNo; }
            set { _caseNo = value; }
        }


        private List<Note> _noteList = new List<Note>();
        [DataMember]
        public List<Note> NoteList
        {
            get { return _noteList; }
            set { _noteList = value; }
        }

        private List<CreditorBasicInfo> _creditors = new List<CreditorBasicInfo>();
        [DataMember]
        public List<CreditorBasicInfo> Creditors
        {
            get { return _creditors; }
            set { _creditors = value; }
        }

        private int _creditorGroupNo = -1;
        [DataMember]
        public int CreditorGroupNo
        {
            get { return _creditorGroupNo; }
            set { _creditorGroupNo = value; }
        }
        private string _creditorGroupName = string.Empty;
        [DataMember]
        public string CreditorGroupName
        {
            get { return _creditorGroupName; }
            set { _creditorGroupName = value; }
        }

        private DateTime _birthDate;
        [DataMember]
        public DateTime BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }

        private int _addressNo = -1;
        [DataMember]
        public int AddressNo
        {
            get { return _addressNo; }
            set { _addressNo = value; }
        }

        private int _debtorEntNo = -1;
        [DataMember]
        public int DebtorEntNo
        {
            get { return _debtorEntNo; }
            set { _debtorEntNo = value; }
        }

        private int _debtorContactEntNo = -1;
        [DataMember]
        public int DebtorContactEntNo
        {
            get { return _debtorContactEntNo; }
            set { _debtorContactEntNo = value; }
        }

        private int _debtorEntRoleId = -1;
        [DataMember]
        public int DebtorEntRoleId
        {
            get { return _debtorEntRoleId; }
            set { _debtorEntRoleId = value; }
        }

        private string _fax = string.Empty;
        [DataMember]
        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }

        private string _zipName = string.Empty;
        [DataMember]
        public string ZipName
        {
            get { return _zipName; }
            set { _zipName = value; }
        }

        private string _postalAddress1 = string.Empty;
        [DataMember]
        public string PostalAddress1
        {
            get { return _postalAddress1; }
            set { _postalAddress1 = value; }
        }

        private string _postalAddress2 = string.Empty;
        [DataMember]
        public string PostalAddress2
        {
            get { return _postalAddress2; }
            set { _postalAddress2 = value; }
        }

        private string _postalAddress3 = string.Empty;
        [DataMember]
        public string PostalAddress3
        {
            get { return _postalAddress3; }
            set { _postalAddress3 = value; }
        }
    }
}
