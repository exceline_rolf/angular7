﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects.Debtor
{
    [DataContract]
    public class USPAssert
    {
        private string _type = string.Empty;
        [DataMember]
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private DateTime _regdate;
        [DataMember]
        public DateTime Regdate
        {
            get { return _regdate; }
            set { _regdate = value; }
        }

        private int _debtorNo = -1;
        [DataMember]
        public int DebtorNo
        {
            get { return _debtorNo; }
            set { _debtorNo = value; }
        }

        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }


    }
}
