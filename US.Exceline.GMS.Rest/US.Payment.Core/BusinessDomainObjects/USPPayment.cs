﻿
namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPPayment : IUSPPayment, ITagEnabled
    {
        #region IUSPPayment Members


        private string _Txt = string.Empty;
        public string Txt
        {
            get
            {
                return _Txt;
            }
            set
            {
                _Txt = value;
            }
        }


        private int _ItemType = -1;
        public int ItemType
        {
            get
            {
                return _ItemType;
            }
            set
            {
                _ItemType = value;
            }
        }


        private string _Amount = string.Empty;
        public string Amount
        {
            get
            {
                return _Amount;
            }
            set
            {
                _Amount = value;
            }
        }


        private string _VouDate = string.Empty;
        public string VouDate
        {
            get
            {
                return _VouDate;
            }
            set
            {
                _VouDate = value;
            }
        }


        private string _DueDate = string.Empty;
        public string DueDate
        {
            get
            {
                return _DueDate;
            }
            set
            {
                _DueDate = value;
            }
        }


        private string _DStatus = string.Empty;
        public string DStatus
        {
            get
            {
                return _DStatus;
            }
            set
            {
                _DStatus = value;
            }
        }


        private int _ARNo = -1;
        public int ARNo
        {
            get
            {
                return _ARNo;
            }
            set
            {
                _ARNo = value;
            }
        }


        private string _Ref = string.Empty;
        public string Ref
        {
            get
            {
                return _Ref;
            }
            set
            {
                _Ref = value;
            }
        }


        private string _KID = string.Empty;
        public string KID
        {
            get
            {
                return _KID;
            }
            set
            {
                _KID = value;
            }
        }


        private string _CustId = string.Empty;
        public string CustId
        {
            get
            {
                return _CustId;
            }
            set
            {
                _CustId = value;
            }
        }


        private int _ARItemNo = -1;
        public int ARItemNo
        {
            get
            {
                return _ARItemNo;
            }
            set
            {
                _ARItemNo = value;
            }
        }


        private string _CreditorId = string.Empty;
        public string CreditorId
        {
            get
            {
                return _CreditorId;
            }
            set
            {
                _CreditorId = value;
            }
        }


        private string _ExtTransNo = string.Empty;
        public string ExtTransNo
        {
            get
            {
                return _ExtTransNo;
            }
            set
            {
                _ExtTransNo = value;
            }
        }


        private string _ExtCaseNo = string.Empty;
        public string ExtCaseNo
        {
            get
            {
                return _ExtCaseNo;
            }
            set
            {
                _ExtCaseNo = value;
            }
        }


        private string _Reference2 = string.Empty;
        public string Reference2
        {
            get
            {
                return _Reference2;
            }
            set
            {
                _Reference2 = value;
            }
        }
        private string _debtorAccountNumber = string.Empty;

        public string DebtorAccountNumber
        {
            get { return _debtorAccountNumber; }
            set { _debtorAccountNumber = value; }
        }



        private int _errorCode = -1;

        public int ErrorCode
        {
            get
            {
                return _errorCode;
            }
            set
            {
                _errorCode = value;
            }
        }

        private string _errorDescription = string.Empty;

        public string ErrorDescription
        {
            get
            {
                return _errorDescription;
            }
            set
            {
                _errorDescription = value;
            }
        }

        private string _fileName = string.Empty;
        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
            }
        }

        private int _fileId = -1;
        public int FileId
        {
            get
            {
                return _fileId;
            }
            set
            {
                _fileId = value;

            }
        }
        private string _source = string.Empty;
        public string Source
        {
            get
            {
                return _source;
            }
            set
            {
                _source = value;
            }
        }

        private int _voucherId = -1;
        public int VoucherId
        {
            get
            {
                return _voucherId;
            }
            set
            {
                _voucherId = value;
            }
        }
        private int _voucherDetailId = -1;
        public int VoucherDetailId
        {
            get
            {
                return _voucherDetailId;
            }
            set
            {
                _voucherId = value;
            }
        }

        #endregion

        #region ITagEnabled Members


        private string _Tag1 = string.Empty;
        public string Tag1
        {
            get
            {
                return _Tag1;
            }
            set
            {
                _Tag1 = value;
            }
        }


        private string _Tag2 = string.Empty;
        public string Tag2
        {
            get
            {
                return _Tag2;
            }
            set
            {
                _Tag2 = value;
            }
        }


        private string _Tag3 = string.Empty;
        public string Tag3
        {
            get
            {
                return _Tag3;
            }
            set
            {
                _Tag3 = value;
            }
        }

        #endregion



        private string _creditorAccountNo = string.Empty;

        public string CreditorAccountNo
        {
            get { return _creditorAccountNo; }
            set { _creditorAccountNo = value; }
        }

       
    }
}
