﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Windows;

namespace US.Payment.Core.BusinessDomainObjects
{
    [DataContract]
    public class USPGuarantor:IUSPGuarantor
    {
        private int _recordID = -1;
        [DataMember]
        public int RecordID
        {
            get
            {
                return _recordID;
            }
            set
            {
                _recordID = value;
            }
        }

        private string _type = string.Empty;
        [DataMember]
        public string Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        private bool _isDeleted = false;
        [DataMember]
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
            }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get
            {
                return _createdUser;
            }
            set
            {
                _createdUser = value;
            }
        }

        private string _modifiedUser = string.Empty;
        [DataMember]
        public string ModifiedUser
        {
            get
            {
                return _modifiedUser;
            }
            set
            {
                _modifiedUser = value;
            }
        }

        private string _typeName = string.Empty;
        [DataMember]
        public string TypeName
        {
            get
            {
                return _typeName;
            }
            set
            {
                _typeName = value;
            }
        }

        private int _caseNo = -1;
        [DataMember]
        public int CaseNumber
        {
            get
            {
                return _caseNo;
            }
            set
            {
                _caseNo = value;
            }
        }

        private bool _selected = false;
        [DataMember]
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        private decimal _amount = 0;
        [DataMember]
        public decimal Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }

        private string _dueDate = string.Empty;
        [DataMember]
        public string DueDate
        {
            get
            {
                return _dueDate;
            }
            set
            {
                _dueDate = value;
            }
        }

        private int _guarantorNumber = -1;
        [DataMember]
        public int GuarantorNumber
        {
            get
            {
                return _guarantorNumber;
            }
            set
            {
                _guarantorNumber = value;
            }
        }

        private string _guarantorName = string.Empty;
        [DataMember]
        public string GuarantorName
        {
            get
            {
                return _guarantorName;
            }
            set
            {
                _guarantorName = value;
            }
        }

        private string _guarantorType = string.Empty;
        [DataMember]
        public string GuarantorType
        {
            get
            {
                return _guarantorType;
            }
            set
            {
                _guarantorType = value;
            }
        }

        private string _address1 = string.Empty;
        [DataMember]
        public string Address1
        {
            get
            {
                return _address1;
            }
            set
            {
                _address1 = value;
            }
        }

        private string _address2 = string.Empty;
        [DataMember]
        public string Address2
        {
            get
            {
                return _address2;
            }
            set
            {
                _address2 = value;
            }
        }

        private int _zipCode = -1;
        [DataMember]
        public int ZipCode
        {
            get
            {
                return _zipCode;
            }
            set
            {
                _zipCode = value;
            }
        }

        private string _zipName = string.Empty;
        [DataMember]
        public string ZipName
        {
            get
            {
                return _zipName;
            }
            set
            {
                _zipName = value;
            }
        }

        private string _country = string.Empty;
        [DataMember]
        public string Country
        {
            get
            {
                return _country;
            }
            set
            {
                _country = value;
            }
        }

        private bool _isExist = false;
        [DataMember]
        public bool IsExist
        {
            get
            {
                return _isExist;
            }
            set
            {
                _isExist = value;
            }
        }

        private List<USPAddress> _guarantorAddress = new List<USPAddress>();
        [DataMember]
        public List<USPAddress> GuarantorAddress
        {
            get
            {
                return _guarantorAddress;
            }
            set
            {
                _guarantorAddress = value;
            }
        }

        private Visibility _viewEditAndRemoveOption = System.Windows.Visibility.Collapsed;
        [DataMember]
        public Visibility ViewEditAndRemoveOption
        {
            get { return _viewEditAndRemoveOption; }
            set { _viewEditAndRemoveOption = value; }
        }

                
    }
}
