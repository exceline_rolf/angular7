﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.DomainObjects;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPTransaction :IUSPTransaction
    {
        private DateTime _transactionDate = DateTime.Now;
        public DateTime TransactionDate
        {
            get
            {
                return _transactionDate;
            }
            set
            {
                _transactionDate = value;
            }
        }
        private int _caseNo = -1;
        public int CaseNo
        {
            get
            {
                return _caseNo;
            }
            set
            {
                _caseNo = value;
            }
        }
        private int _subCaseNo = -1;
        public int SubCaseNo
        {
            get
            {
                return _subCaseNo;
            }
            set
            {
                _subCaseNo = value;
            }
        }
        private int _arNo = -1;
        public int ARNo
        {
            get
            {
                return _arNo ;
            }
            set
            {
                _arNo = value;
            }
        }
        private int _arItemNo = -1;
        public int ARItemNo
        {
            get
            {
                return _arItemNo;
            }
            set
            {
                _arItemNo = value;
            }
        }
        private string _cid = string.Empty;
        public string CID
        {
            get
            {
                return _cid;
            }
            set
            {
                _cid = value;
            }
        }
        private string _pid = string.Empty;
        public string PID
        {
            get
            {
                return _pid;
            }
            set
            {
                _pid = value;
            }
        }
        private int _deplyed = -1;
        public int Delayed
        {
            get
            {
                return _deplyed;
            }
            set
            {
                _deplyed = value;
            }
        }
        private DateTime _voucherdate = DateTime.Now;
        public DateTime VoucherDate
        {
            get
            {
                return _voucherdate;
            }
            set
            {
                _voucherdate  = value;
            }
        }
        private DateTime _dueDate = DateTime.Now.AddDays(14);
        public DateTime DueDate
        {
            get
            {
                return _dueDate;
            }
            set
            {
                _dueDate = value;
            }
        }
        private DateTime _receievedDate = DateTime.Now;
        public DateTime ReceivedDate
        {
            get
            {
                return _receievedDate;
            }
            set
            {
                _receievedDate  = value;
            }
        }
        private DateTime _regDate = DateTime.Now;
        public DateTime RegDate
        {
            get
            {
                return _regDate;
            }
            set
            {
                _regDate  = value;
            }
        }
        private decimal _amount = 0;
        public decimal Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount  = value;
            }
        }
        private string _source = string.Empty;
        public string Source
        {
            get
            {
                return _source;
            }
            set
            {
                _source = value;
            }
        }
        private string _fileName = string.Empty;
        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName= value;
            }
        }
        private string _debtorAccountNo = string.Empty;
        public string DebtorAccountNo
        {
            get
            {
                return _debtorAccountNo;
            }
            set
            {
                _debtorAccountNo = value;
            }
        }
        private string _kid = string.Empty;
        public string KID
        {
            get
            {
                return _kid;
            }
            set
            {
                _kid = value;
            }
        }
        private string _invoiceNo = string.Empty;
        public string InvoiceNo
        {
            get
            {
                return _invoiceNo;
            }
            set
            {
                _invoiceNo = value;
            }
        }
        private string _referece1 = string.Empty;
        public string Reference1
        {
            get
            {
                return _referece1;
            }
            set
            {
                _referece1 = value;
            }
        }
        private int _apportionStatus = -1;
        public int ApportionStatus
        {
            get
            {
                return _apportionStatus;
            }
            set
            {
                _apportionStatus  = value;
            }
        }
        private int _voucherDetailID = -1;
        public int VoucherDetailId
        {
            get
            {
                return _voucherDetailID ;
            }
            set
            {
                _voucherDetailID  = value;
            }
        }
        private string _creditorAccountNo = string.Empty;
        public string CreditorAccountNo
        {
            get
            {
                return _creditorAccountNo;
            }
            set
            {
                _creditorAccountNo  = value;
            }
        }
        private DateTime _notifiedDate = DateTime.Now;
        public DateTime NotifiedDate
        {
            get
            {
                return _notifiedDate;
            }
            set
            {
                _notifiedDate = value;
            }
        }
        private string _reference2 = string.Empty;
        public string Reference2
        {
            get
            {
                return _reference2 ;
            }
            set
            {
                _reference2  = value;
            }
        }
        private string _contractKid = string.Empty;
        public string ContractKid
        {
            get
            {
                return _contractKid ;
            }
            set
            {
                _contractKid  = value;
            }
        }
        private DateTime _cancelDate = DateTime.MinValue;
        public DateTime CancelDate
        {
            get
            {
                return _cancelDate;
            }
            set
            {
                _cancelDate = value;
            }
        }
        private string _externalTransactionNo = string.Empty;
        public string ExternalTransactionNo
        {
            get
            {
                return _externalTransactionNo;
            }
            set
            {
                _externalTransactionNo = value;
            }
        }
        private int _vatable = -1;
        public int VATLiability
        {
            get
            {
                return _vatable;
            }
            set
            {
                _vatable = value;
            }
        }
        private int _isPrinted = -1;
        public int IsPrinted
        {
            get
            {
                return _isPrinted;
            }
            set
            {
                _isPrinted = value;
            }
        }
        private string _printFilePath = string.Empty;
        public string PrintFilePath
        {
            get
            {
                return _printFilePath;
            }
            set
            {
                _printFilePath = value;
            }
        }
        private TransactionProfile _transProfile = new TransactionProfile();
        public TransactionProfile TransactionProfile
        {
            get
            {
                return _transProfile;
            }
            set
            {
                _transProfile = value;
            }
        }

        private string _user = string.Empty;
        public string User
        {
            get
            {
                return _user;
            }
            set
            {
                _user = value;
            }
        }

        private string _transType = string.Empty;
        public string TransType
        {
            get
            {
                return _transType;
            }
            set
            {
                _transType = value;
            }
        }

        private string _transText = string.Empty;
        public string TransText
        {
            get
            {
                return _transText;
            }
            set
            {
                _transText = value;
            }
        }

        private int _paymentId = -1;
        public int PaymentId
        {
            get
            {
                return _paymentId;
            }
            set
            {
                _paymentId = value;
            }
        }

    }
}
