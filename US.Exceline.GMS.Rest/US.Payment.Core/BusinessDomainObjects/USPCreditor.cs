﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
   public class USPCreditor:IUSPCreditor
    {

      
           #region IUSPCreditor Members

           private string _incassoID = string.Empty;
           private string _name = string.Empty;

           public string CreditorInkassoID
           {
               get
               {
                   return _incassoID;
               }
               set
               {
                   _incassoID = value;
               }
           }

           public string CreditorName
           {
               get
               {
                   return _name;
               }
               set
               {
                   _name = value;
               }
           }
           private string _accountNo = string.Empty;
           public string CreditorAccountNo
           {
               get
               {
                   return _accountNo;
               }
               set
               {
                   _accountNo = value;
               }
           }
           private int _entityID = 0;
           public int CreditorEntityID
           {
               get
               {
                   return _entityID;
               }
               set
               {
                   _entityID = value;
               }
           }
           private int _roleID = 0;
           public int CreditorEntityRoleID
           {
               get
               {
                   return _roleID;
               }
               set
               {
                   _roleID = value;
               }
           }
           private string _personNo = string.Empty;
           public string CreditorPersonNo
           {
               get
               {
                   return _personNo;
               }
               set
               {
                   _personNo = value;
               }
           }
           private int _debtorCount = 0;
           public int CreditorDebtorCount
           {
               get
               {
                   return _debtorCount;
               }
               set
               {
                   _debtorCount = value;
               }
           }

           private List<IUSPAddress> _Address = new List<IUSPAddress>();
           public List<IUSPAddress> CreditorAddress
           {
               get
               {
                   return _Address;
               }
               set
               {
                   _Address = value;
               }
           }

           private string _BirthDay = string.Empty;
           public string CreditorBirthDay
           {
               get
               {
                   return _BirthDay;
               }
               set
               {
                   _BirthDay = value;
               }
           }

           private string _invoPrintStatus = string.Empty;
           public string CreditorInvoPrintStatus
           {
               get
               {
                   return _invoPrintStatus;
               }
               set
               {
                   _invoPrintStatus = value;
               }
           }

           #endregion

           private List<USPFileLog> _filesToPrint;
           public List<USPFileLog> CreditorFilesToPrint
           {
               get { return _filesToPrint; }
               set { _filesToPrint = value; }
           }

           private int _printAllow;
           public int CreditorPrintAllow
           {
               get { return _printAllow; }
               set { _printAllow = value; }
           }




         
    }
}
