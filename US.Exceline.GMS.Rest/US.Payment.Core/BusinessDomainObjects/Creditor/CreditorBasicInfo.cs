﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects.Creditor
{
    [DataContract]
    public class CreditorBasicInfo
    {
        private string _creditorNo = string.Empty;
        [DataMember]
        public string CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }

        private string _creditorName = string.Empty;
        [DataMember]
        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }
        }

        private decimal _balance = 0;
        [DataMember]
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        private DateTime _atgDate;
        [DataMember]
        public DateTime AtgDate
        {
            get { return _atgDate; }
            set { _atgDate = value; }
        }

        private DateTime _regDate;
        [DataMember]
        public DateTime RegDate
        {
            get { return _regDate; }
            set { _regDate = value; }
        }

        private string _companyID = string.Empty;
        [DataMember]
        public string CompanyID
        {
            get { return _companyID; }
            set { _companyID = value; }
        }

        private string _directNo = string.Empty;
        [DataMember]
        public string DirectNo
        {
            get { return _directNo; }
            set { _directNo = value; }
        }

        private string _mobileNo = string.Empty;
        [DataMember]
        public string MobileNo
        {
            get { return _mobileNo; }
            set { _mobileNo = value; }
        }

        private string _emailID = string.Empty;
        [DataMember]
        public string EmailID
        {
            get { return _emailID; }
            set { _emailID = value; }
        }

        private string _skypeID = string.Empty;
        [DataMember]
        public string SkypeID
        {
            get { return _skypeID; }
            set { _skypeID = value; }
        }

        private string _msnID = string.Empty;
        [DataMember]
        public string MsnID
        {
            get { return _msnID; }
            set { _msnID = value; }
        }
        private int _creditorGroupNo = -1;
        [DataMember]
        public int CreditorGroupNo
        {
            get { return _creditorGroupNo; }
            set { _creditorGroupNo = value; }
        }

        private string _creditorGroupName = string.Empty;
        [DataMember]
        public string CreditorGroupName
        {
            get { return _creditorGroupName; }
            set { _creditorGroupName = value; }
        }

        private string _sms = string.Empty;
        [DataMember]
        public string SMS
        {
            get { return _sms; }
            set { _sms = value; }
        }     

        private string _creditorInkassoId = string.Empty;
        [DataMember]
        public string CreditorInkassoId
        {
            get { return _creditorInkassoId; }
            set { _creditorInkassoId = value; }
        }

        private string _creditorGroup = string.Empty;
        [DataMember]
        public string CreditorGroup
        {
            get { return _creditorGroup; }
            set { _creditorGroup = value; }
        }

        private string _remitAccountNo = string.Empty;
        [DataMember]
        public string RemitAccountNo
        {
            get { return _remitAccountNo; }
            set { _remitAccountNo = value; }
        }

        private string _eNTNo = string.Empty;
        [DataMember]
        public string ENTNo
        {
            get { return _eNTNo; }
            set { _eNTNo = value; }
        }

        private string _creditorStatus = string.Empty;
        [DataMember]
        public string CreditorStatus
        {
            get { return _creditorStatus; }
            set { _creditorStatus = value; }
        }

        private string _profileWorkflow = string.Empty;
        [DataMember]
        public string ProfileWorkflow
        {
            get { return _profileWorkflow; }
            set { _profileWorkflow = value; }
        }
        private string _profileWorkflowcategory = string.Empty;
        [DataMember]
        public string ProfileWorkflowcategory
        {
            get { return _profileWorkflowcategory; }
            set { _profileWorkflowcategory = value; }
        }

        private string _workflowInstnaceId = string.Empty;
        [DataMember]
        public string WorkflowInstnaceId
        {
            get { return _workflowInstnaceId; }
            set { _workflowInstnaceId = value; }
        }
    }
}
