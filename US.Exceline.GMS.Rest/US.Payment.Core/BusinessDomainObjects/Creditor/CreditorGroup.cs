﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using US.Payment.Core.DomainObjects.DomainObjects.Bureau;
using System.Runtime.Serialization;


namespace US.Payment.Core.BusinessDomainObjects.Creditor
{
    [DataContract]
    public class CreditorGroup
    {
        private int _ID = -1;
        [DataMember]
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        //public int ID { get; set; }

        
        private string _groupName = string.Empty;
         [DataMember]
        public string GroupName
        {
            get { return _groupName; }
            set { _groupName = value; }
        }
        //public string GroupName { get; set; }


        private int _activeStatus = -1;
         [DataMember]
        public int ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }        
        //public int ActiveStatus { get; set; }      
        
        
        
        private List<BureauCreditor> _creditors = new List<BureauCreditor>();
         [DataMember]
        public List<BureauCreditor> Creditors
        {
            get { return _creditors; }
            set { _creditors = value; }
        }

        private List<BMD> _BMDs = new List<BMD>();
        [DataMember]
        public List<BMD> BMDs
        {
            get { return _BMDs; }
            set { _BMDs = value; }
        }
        
        private string _discription = string.Empty;
        [DataMember]

        public string Discription
        {
            get { return _discription; }
            set { _discription = value; }
        }
             
        
        private bool _isBMDData = false;
         [DataMember]
        public bool IsBMDData
        {
            get { return _isBMDData; }
            set { _isBMDData = value; }
        }
        

        private bool _isSelected = false;
         [DataMember]
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }
        private string _displayName = string.Empty;
         [DataMember]
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        private int _creditorsCount = 0;
         [DataMember]

        public int CreditorsCount
        {
            get { return _creditorsCount; }
            set { _creditorsCount = value; }
        }


    }
}
