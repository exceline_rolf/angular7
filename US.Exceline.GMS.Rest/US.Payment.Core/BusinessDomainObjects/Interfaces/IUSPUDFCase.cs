﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
   public interface IUSPUDFCase
    {
        int USPSubCaseNo { get; set; }
        string CaseNumber { get; set; }
        string CaseReportGroup { get; set; }
        string CaseStatus { get; set; }
        string CaseType { get; set; }
        string CID { get; set; }
        IUSPAddress Address { get; set; }
        string CBirthdate { get; set; }
        string CDebtype { get; set; }
        string CFirstName { get; set; }
        string CHome { get; set; }
        string CLastname { get; set; }
        string CMail { get; set; }
        string CMobile { get; set; }
        string COrganisation { get; set; }
        string CSosialsecurity { get; set; }
        string CWork { get; set; }
        string DebtWarningDate { get; set; }
        bool IsSuccess { get; set; }
        string KID { get; set; }
        string PayNoteDate { get; set; }
        string PID { get; set; }
        List<IUSPUDFTransaction> Transactions { get; set; }
    }
}
