﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public interface IUSPGuarantor
    {
        int CaseNumber { get; set; }
        string Type { get; set; }
        decimal Amount { get; set; }
        string DueDate { get; set; }
        int GuarantorNumber { get; set; }
        string GuarantorName { get; set; }
        string GuarantorType { get; set; }
        List<USPAddress> GuarantorAddress { get; set; }
        int RecordID { get; set; }
        bool IsDeleted { get; set; }
        string TypeName { get; set; }
        bool IsExist { get; set; }
        
    }
}
