﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public interface IUSPExportFileDetails
    {
        int FileType { get; set; }
        string Inkasso { get; set; }
        int FileNo { get; set; }
    }
}
