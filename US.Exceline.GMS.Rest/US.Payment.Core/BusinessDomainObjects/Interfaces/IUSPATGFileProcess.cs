﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
   public interface IUSPATGFileProcess
    {
       List<IUSPCreditor> CreditorList { get; set; }
       string SourceFilePath { get; set; }
       string DestinationFilePath { get; set; }
       string ProcessPath { get; set; }
       string ConnectionString { get; set; }
       string FolderPath { get; set; }
       string Notificattion { get; set; }
       int stateValue { get; set; }
      

    }
}
