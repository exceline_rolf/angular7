﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
   public interface IUSPHistory : ITagEnabled
    {
        string FileName{ get; set; }
        int Error{ get; set; }
        string Comment { get; set; }
       
    }
}
