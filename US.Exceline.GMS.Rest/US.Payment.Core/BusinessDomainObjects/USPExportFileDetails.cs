﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPExportFileDetails : IUSPExportFileDetails
    {
    #region IUSPExportFileDetails Members


private  int  _FileType=-1;
public int  FileType
{
	  get 
	{ 
		 return _FileType; 
	}
	  set 
	{ 
		_FileType = value;
	}
}


private  string  _Inkasso=string.Empty;
public string  Inkasso
{
	  get 
	{ 
		 return _Inkasso; 
	}
	  set 
	{ 
		_Inkasso = value;
	}
}


private  int  _FileNo=-1;
public int  FileNo
{
	  get 
	{
        return _FileNo; 
	}
	  set 
	{
        _FileNo = value;
	}
}

#endregion
}
}
