﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects
{
    [DataContract]
    public class USPExtention
    {
        private List<USPExtentionItem> _itemList ;

        public USPExtention()
        {
            _itemList = new List<USPExtentionItem>();
        
        }
         [DataMember]
        public List<USPExtentionItem> ItemList
        {
            get { return _itemList; }
            set { _itemList = value; }
        }
    }

    [DataContract]
    public class USPExtentionItem
    {
        private string _key ;
        private string _value;
        public USPExtentionItem()
        {
            _key = string.Empty;
            _value = string.Empty;
        }
        [DataMember]
        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }
        [DataMember]
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    
    }
}
