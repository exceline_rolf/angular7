﻿
using US.Payment.Core.BusinessDomainObjects.Interfaces;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPCompany : IUSPCompany
    { 
        #region IUSPCompany Members


        public USPCompany()
        {
            companyId = -1;
            CompanyName = string.Empty;
            Address1 = string.Empty;
            Address2 = string.Empty;
            City = string.Empty;
            state = string.Empty;
            Country = string.Empty;
            CountryId = string.Empty;
            Fax = string.Empty;
            Email = string.Empty;
            GeneralTelephoneNo = string.Empty;
            VATRegNo = string.Empty;
            Account = string.Empty;
            EnterpriseNo = string.Empty;
            ClientAccount = string.Empty;
            Currency = string.Empty;
            MarkForReminder = -1;
            ReminderLetter = -1;
            MarkForDebtCollection = -1;
            TransferDebtCollectionDays = -1;
            DefaultUnfreezeDaysForInvoice = -1;
            DefaultUnfreezeDaysForDebtor = -1;

        }
        
        public int companyId {get;set;}  
        public string CompanyName{get;set;}
        public string Address1{get;set;}
        public string Address2{get;set;}
        public string City{get;set;}
        public string state{get;set;}
        public string Country{get;set;}
        public string CountryId{get;set;}
        public string Fax{get;set;}
        public string Email{get;set;}
        public string GeneralTelephoneNo{get;set;}
        public string VATRegNo{get;set;}
        public string Account{get;set;}
        public string EnterpriseNo{get;set;}
        public string ClientAccount{get;set;}
        public string Currency{get;set;}
        public int MarkForReminder{get;set;}
        public int ReminderLetter{get;set;}
        public int MarkForDebtCollection{get;set;}
        public int TransferDebtCollectionDays{get;set;}
        public int DefaultUnfreezeDaysForInvoice{get;set;}
        public int DefaultUnfreezeDaysForDebtor { get; set; }
        

        #endregion
    }
}
