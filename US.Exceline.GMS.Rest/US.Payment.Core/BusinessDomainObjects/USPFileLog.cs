﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.BusinessDomainObjects;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPFileLog : IFileLog
    {
        #region IFileLog Members
        private int _fileID=-1;
        public int FileId
        {
            get { return _fileID;}
            set { _fileID = value; }
        }
        private string _fileName = string.Empty;
        public string FileName
        {
            get { return _fileName;}
            set{_fileName=value;}
        }

        private string _fileType = string.Empty;
        public string FileType
        {
            get { return _fileType; }
            set { _fileType = value; }
        }

        private string _inout = string.Empty;
        public string INOUT
        {
            get { return _inout; }
            set { _inout = value; }
        }

        private int _fileDirection=-1;
        public int FileDirection
        {
            get { return _fileDirection; }
            set { _fileDirection = -value; }
        }
        private string _sourcePath = string.Empty;
        public string SourcePath
        {
            get { return _sourcePath; }
            set { _sourcePath = value; }
        }

        private string _destination = string.Empty;
        public string DestinationPath
        {
            get { return _destination; }
            set { _destination = value; }
        }

        private string _processPath = string.Empty;
        public string ProcessPath
        {
            get { return _processPath; }
            set { _processPath = value; }
        }

        private int _status = -1;
        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }
        private DateTime _sourceTime = new DateTime();
        public DateTime SourceTime
        {
            get { return _sourceTime; }
            set { _sourceTime = value; }
        }

        private DateTime _destinationTime = new DateTime();
        public DateTime DestinationTime
        {
            get { return _destinationTime; }
            set { _destinationTime = value; }
        }
        private DateTime _scehTime = new DateTime();
        public DateTime ScheduleTime
        {
            get { return _scehTime;}
            set { _scehTime = value; }
        }
        private DateTime _processTime = new DateTime();
        public DateTime ProcessTime
        {
            get { return _processTime; }
            set{_processTime=value;}
        }

        private string _user = string.Empty;
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }
        private string _userProcess = string.Empty;
        public string UserProcess
        {
            get { return _userProcess; }
            set { _userProcess = value; }
        }
        private string _message1 = string.Empty;
        public string Message1
        {
            get;
            set;
        }

        private string _message2 = string.Empty;
        public string Message2
        {
            get { return _message2; }
            set { _message2 = value; }
        }

        #endregion

        #region IFileLog Members

        private List<string> _notifications = new List<string>();
        public List<string> NotificationMessages
        {
            get { return _notifications; }
            set { _notifications = value; }
        }

        #endregion

        #region IFileLog Members

        private List<int> _succededARItemList = new List<int>();
        public List<int> succededARItemList
        {
            get { return _succededARItemList; }
            set { _succededARItemList = value; }
        }

        private List<string> _succededErrorPaymentList = new List<string>();
        public List<string> succededErrorPaymentList
        {
            get { return _succededErrorPaymentList; }
            set { _succededErrorPaymentList = value; }
        }

        #endregion

        #region IFileLog Members

        private bool _errorOccured = false;

        public bool ErrorOccured
        {
            get { return _errorOccured; }
            set { _errorOccured = value; }
        }

        #endregion
        #region IFileLog Members

        private int _printingStatus ;

        public int PrintingStatus
        {
            get { return _printingStatus; }
            set { _printingStatus = value; }
        }

        #endregion

        private int _isSelectedToPrint;

        public int IsSelectedToPrint
        {
            get { return _isSelectedToPrint; }
            set { _isSelectedToPrint = value; }
        }

        private int _isPrinted;

        public int IsPrinted
        {
            get { return _isPrinted; }
            set { _isPrinted = value; }
        }
    }
}
