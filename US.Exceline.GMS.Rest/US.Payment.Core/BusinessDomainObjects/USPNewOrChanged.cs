﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPNewOrChanged : IUSPNewOrChanged
    {
    #region IUSPNewOrChanged Members


private  string  _FBOCounter=string.Empty;
public string  FBOCounter
{
	  get 
	{ 
		 return _FBOCounter; 
	}
	  set 
	{ 
		_FBOCounter = value;
	}
}


private  string  _RegisterType=string.Empty;
public string  RegisterType
{
	  get 
	{ 
		 return _RegisterType; 
	}
	  set 
	{ 
		_RegisterType = value;
	}
}


private  string  _KID=string.Empty;
public string  KID
{
	  get 
	{ 
		 return _KID; 
	}
	  set 
	{ 
		_KID = value;
	}
}


private  string  _Warning=string.Empty;
public string  Warning
{
	  get 
	{ 
		 return _Warning; 
	}
	  set 
	{ 
		_Warning = value;
	}
}

#endregion

#region ITagEnabled Members


private  string  _Tag1=string.Empty;
public string  Tag1
{
	  get 
	{ 
		 return _Tag1; 
	}
	  set 
	{ 
		_Tag1 = value;
	}
}


private  string  _Tag2=string.Empty;
public string  Tag2
{
	  get 
	{ 
		 return _Tag2; 
	}
	  set 
	{ 
		_Tag2 = value;
	}
}


private  string  _Tag3=string.Empty;
public string  Tag3
{
	  get 
	{
        return _Tag3; 
	}
	  set 
	{
        _Tag3 = value;
	}
}

#endregion
}
}
