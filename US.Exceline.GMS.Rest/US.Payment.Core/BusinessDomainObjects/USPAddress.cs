﻿using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects
{
    [DataContract]
    public class USPAddress : IUSPAddress
    {
        #region IUSPAddress Members


        private int _addrNo = -1;
        [DataMember]
        public int addrNo
        {
            get
            {
                return _addrNo;
            }
            set
            {
                _addrNo = value;
            }
        }


        private string _Address1 = string.Empty;
        [DataMember]
        public string Address1
        {
            get
            {
                return _Address1;
            }
            set
            {
                _Address1 = value;
            }
        }


        private string _Address2 = string.Empty;
        [DataMember]
        public string Address2
        {
            get
            {
                return _Address2;
            }
            set
            {
                _Address2 = value;
            }
        }


        private string _Address3 = string.Empty;
        [DataMember]
        public string Address3
        {
            get
            {
                return _Address3;
            }
            set
            {
                _Address3 = value;
            }
        }


        private string _ZipCode = string.Empty;
        [DataMember]
        public string ZipCode
        {
            get
            {
                return _ZipCode;
            }
            set
            {
                _ZipCode = value;
            }
        }


        private string _CountryCode = string.Empty;
        [DataMember]
        public string CountryCode
        {
            get
            {
                return _CountryCode;
            }
            set
            {
                _CountryCode = value;
            }
        }


        private string _AddSource = string.Empty;
        [DataMember]
        public string AddSource
        {
            get
            {
                return _AddSource;
            }
            set
            {
                _AddSource = value;
            }
        }


        private string _City = string.Empty;
        [DataMember]
        public string City
        {
            get
            {
                return _City;
            }
            set
            {
                _City = value;
            }
        }


        private string _TelMobile = string.Empty;
        [DataMember]
        public string TelMobile
        {
            get
            {
                return _TelMobile;
            }
            set
            {
                _TelMobile = value;
            }
        }


        private string _TelWork = string.Empty;
        [DataMember]
        public string TelWork
        {
            get
            {
                return _TelWork;
            }
            set
            {
                _TelWork = value;
            }
        }


        private string _TelHome = string.Empty;
        [DataMember]
        public string TelHome
        {
            get
            {
                return _TelHome;
            }
            set
            {
                _TelHome = value;
            }
        }


        private string _Email = string.Empty;
        [DataMember]
        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                _Email = value;
            }
        }


        private string _Fax = string.Empty;
        [DataMember]
        public string Fax
        {
            get
            {
                return _Fax;
            }
            set
            {
                _Fax = value;
            }
        }


        private string _MSN = string.Empty;
        [DataMember]
        public string MSN
        {
            get
            {
                return _MSN;
            }
            set
            {
                _MSN = value;
            }
        }


        private string _Skype = string.Empty;
        [DataMember]
        public string Skype
        {
            get
            {
                return _Skype;
            }
            set
            {
                _Skype = value;
            }
        }


        private string _SMS = string.Empty;
        [DataMember]
        public string SMS
        {
            get
            {
                return _SMS;
            }
            set
            {
                _SMS = value;
            }
        }


        private bool _IsDefault = new bool();
        [DataMember]
        public bool IsDefault
        {
            get
            {
                return _IsDefault;
            }
            set
            {
                _IsDefault = value;
            }
        }


        private string _AddressName = string.Empty;
        [DataMember]
        public string AddressName
        {
            get
            {
                return _AddressName;
            }
            set
            {
                _AddressName = value;
            }
        }


        private string _Branch = string.Empty;
        [DataMember]
        public string Branch
        {
            get
            {
                return _Branch;
            }
            set
            {
                _Branch = value;
            }
        }


        private string _PostCode = string.Empty;
        [DataMember]
        public string PostCode
        {
            get
            {
                return _PostCode;
            }
            set
            {
                _PostCode = value;
            }
        }


        private string _Municipality = string.Empty;
        [DataMember]
        public string Municipality
        {
            get
            {
                return _Municipality;
            }
            set
            {
                _Municipality = value;
            }
        }

        #endregion
    }
}
