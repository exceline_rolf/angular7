﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPPaymentReceipt : IUSPPaymentReceipt
    {
    #region IUSPPaymentReceipt Members


private  string  _KID=string.Empty;
public string  KID
{
	  get 
	{ 
		 return _KID; 
	}
	  set 
	{ 
		_KID = value;
	}
}


private  bool  _ErrorOccured=false;
public bool  ErrorOccured
{
	  get 
	{ 
		 return _ErrorOccured; 
	}
	  set 
	{ 
		_ErrorOccured = value;
	}
}


private  string  _ErrorMessage=string.Empty;
public string  ErrorMessage
{
	  get 
	{ 
		 return _ErrorMessage; 
	}
	  set 
	{ 
		_ErrorMessage = value;
	}
}


private  string  _Ref=string.Empty;
public string  Ref
{
	  get 
	{
        return _Ref; 
	}
	  set 
	{
        _Ref = value;
	}
}

#endregion
}
}
