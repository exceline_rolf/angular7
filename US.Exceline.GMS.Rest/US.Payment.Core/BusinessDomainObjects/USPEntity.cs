﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.Payment.Core.BusinessDomainObjects.BasicData;

namespace US.Payment.Core.BusinessDomainObjects
{
    [DataContract]
    public class USPEntity
    {
        private string _firstName = string.Empty;
        private string _lastName = string.Empty;
        private string _persionNo = string.Empty;
        private DateTime _birthDay = DateTime.MinValue;
        private string _reference1 = string.Empty;
        private string _address1 = string.Empty;
        private string _address2 = string.Empty;
        private string _address3 = string.Empty;
        private PostalArea _postalArea = new PostalArea();
        private string _countryId = string.Empty;
        private string _Addresource = string.Empty;
        private string _teleMoble = string.Empty;
        private string _teleWork = string.Empty;
        private string _teleHome = string.Empty;
        private string _email = string.Empty;
        private string _msn = string.Empty;
        private string _skype = string.Empty;
        private string _fax = string.Empty;
        private string _sms = string.Empty;
        private string _gender = string.Empty;
        private int _entNo = -1;
        private USPExtention _entityExtention = new USPExtention();
        private string _entityType = string.Empty;
        private int _refEntNo = -1;

      
        [DataMember]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        [DataMember]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        [DataMember]
        public string PersionNo
        {
            get { return _persionNo; }
            set { _persionNo = value; }
        }

        [DataMember]
        public DateTime BirthDay
        {
            get { return _birthDay; }
            set { _birthDay = value; }
        }

        [DataMember]
        public string Reference1
        {
            get { return _reference1; }
            set { _reference1 = value; }
        }

        [DataMember]
        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        [DataMember]
        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        [DataMember]
        public string Address3
        {
            get { return _address3; }
            set { _address3 = value; }
        }

        [DataMember]
        public PostalArea PostalArea
        {
            get { return _postalArea; }
            set { _postalArea = value; }
        }

        [DataMember]
        public string CountryId
        {
            get { return _countryId; }
            set { _countryId = value; }
        }

        [DataMember]
        public string Addresource
        {
            get { return _Addresource; }
            set { _Addresource = value; }
        }

        [DataMember]
        public string TeleMoble
        {
            get { return _teleMoble; }
            set { _teleMoble = value; }
        }

        [DataMember]
        public string TeleWork
        {
            get { return _teleWork; }
            set { _teleWork = value; }
        }

        [DataMember]
        public string TeleHome
        {
            get { return _teleHome; }
            set { _teleHome = value; }
        }

        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        [DataMember]
        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }

        [DataMember]
        public string Msn
        {
            get { return _msn; }
            set { _msn = value; }
        }

        [DataMember]
        public string Skype
        {
            get { return _skype; }
            set { _skype = value; }
        }

        [DataMember]
        public string Sms
        {
            get { return _sms; }
            set { _sms = value; }
        }

        [DataMember]
        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        [DataMember]
        public int EntNo
        {
            get { return _entNo; }
            set { _entNo = value; }
        }

        [DataMember]
        public USPExtention EntityExtention
        {
            get { return _entityExtention; }
            set { _entityExtention = value; }
        }

        /// <summary>
        /// DEB,CRE,EMPLOYER,CONTACT
        /// </summary>
        [DataMember]
        public string EntityType
        {
            get { return _entityType; }
            set { _entityType = value; }
        }
         [DataMember]
        public int RefEntNo
        {
            get { return _refEntNo; }
            set { _refEntNo = value; }
        }
    }
}
