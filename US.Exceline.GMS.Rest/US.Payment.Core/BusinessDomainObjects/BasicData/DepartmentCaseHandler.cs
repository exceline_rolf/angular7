﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment
// Coding Standard   : US Coding Standards
// Author            : GMA
// Created Timestamp : "22/08/2012 17:01:15
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System.Collections.Generic;
using System.Runtime.Serialization;
using System;
using US.Payment.Core.BusinessDomainObjects.BasicData;

namespace US.Payment.Core.BusinessDomainObjects.BasicData
{
   [DataContract]
   public class DepartmentCaseHandler
    {
       private int _id = -1;
       private string _displayName = string.Empty;
       private int _departmentNo = -1;
       private bool _isSelected=false;

       [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

       [DataMember]
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

       [DataMember]
       public int DepartmentNo
       {
           get { return _departmentNo; }
           set { _departmentNo = value; }
       }
       [DataMember]
       public bool IsSelected
       {
           get { return _isSelected; }
           set { _isSelected = value; }
       }

   }
}
