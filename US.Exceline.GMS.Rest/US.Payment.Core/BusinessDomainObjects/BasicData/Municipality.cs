﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment
// Coding Standard   : US Coding Standards
// Author            : GMA
// Created Timestamp : "22/08/2012 17:01:51
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Windows;
using System;

namespace US.Payment.Core.BusinessDomainObjects.BasicData
{
    [DataContract]
    public class Municipality
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _code = string.Empty;
        [DataMember]
        public string Code
        {
            get { return _code; }
            set 
            {
                _code = value; 
            }
        }
        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private bool _selected = false ;
        [DataMember]
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }
        private Visibility _viewEditAndRemoveOption = System.Windows.Visibility.Collapsed ;
        [DataMember]
        public Visibility ViewEditAndRemoveOption
        {
            get { return _viewEditAndRemoveOption; }
            set { _viewEditAndRemoveOption = value; }
        }
        private string _user = string.Empty;
        [DataMember]
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }

        private string _displayName = string.Empty;
        [DataMember]
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }
    }
}
