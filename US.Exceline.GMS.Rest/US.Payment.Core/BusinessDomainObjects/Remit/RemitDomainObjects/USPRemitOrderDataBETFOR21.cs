﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.Remit.RemitDomainObjects
{
    public class USPRemitOrderDataBETFOR21:IUSPRemitOrderDataBETFOR21 
    {

        #region IUSPRemitOrderDataBETFOR21 Members
        private string _applicationHeader = string.Empty;
        public string BETFOR21_ApplicationHeader
        {
            get { return _applicationHeader; }
            set { _applicationHeader = value; }
        }

        private string _transactionCode = string.Empty;
        public string BETFOR21_TransactionCode
        {
            get { return _transactionCode; }
            set { _transactionCode = value; }
        }

        private string _enterpriseNumber = string.Empty;
        public string BETFOR21_EnterpriseNumber
        {
            get { return _enterpriseNumber; }
            set { _enterpriseNumber = value; }
        }

        private string _accountNumber = string.Empty;
        public string BETFOR21_AccountNumber
        {
            get { return _accountNumber; }
            set { _accountNumber = value; }
        }

        private string _sequenceControlField = string.Empty;
        public string BETFOR21_SequenceControlField
        {
            get { return _sequenceControlField; }
            set { _sequenceControlField = value; }
        }

        private string _referenceNumber = string.Empty;
        public string BETFOR21_ReferenceNumber
        {
            get { return _referenceNumber; }
            set { _referenceNumber = value; }
        }

        private string _paymentDate = string.Empty;
        public string BETFOR21_PaymentDate
        {
            get { return _paymentDate; }
            set { _paymentDate = value; }
        }

        private string _ownRefOrder = string.Empty;
        public string BETFOR21_OwnRefOrder
        {
            get { return _ownRefOrder; }
            set { _ownRefOrder = value; }
        }

        private string _reserved = string.Empty;
        public string BETFOR21_Reserved
        {
            get { return _reserved; }
            set { _reserved = value; }
        }

        private string _payeesAccount = string.Empty;
        public string BETFOR21_PayeesAccount
        {
            get { return _payeesAccount; }
            set { _payeesAccount = value; }
        }

        private string _payeesName = string.Empty;
        public string BETFOR21_PayeesName
        {
            get { return _payeesName; }
            set { _payeesName = value; }
        }

        private string _address1 = string.Empty;
        public string BETFOR21_Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        private string _address2 = string.Empty;
        public string BETFOR21_Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        private string _postalCode = string.Empty;
        public string BETFOR21_PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }

        private string _town = string.Empty;
        public string BETFOR21_Town
        {
            get { return _town; }
            set { _town = value; }
        }

        private string _amountToOwnAccount = string.Empty;
        public string BETFOR21_AmountToOwnAccount
        {
            get { return _amountToOwnAccount; }
            set { _amountToOwnAccount = value; }
        }

        private string _textCode = string.Empty;
        public string BETFOR21_TextCode
        {
            get { return _textCode; }
            set { _textCode = value; }
        }

        private string _transactionCategory = string.Empty;
        public string BETFOR21_TransactionCategory
        {
            get { return _transactionCategory; }
            set { _transactionCategory = value; }
        }

        private string _deletion = string.Empty;
        public string BETFOR21_Deletion
        {
            get { return _deletion; }
            set { _deletion = value; }
        }

        private string _totalAmount = string.Empty;
        public string BETFOR21_TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }

        private string _reserved1;
        public string BETFOR21_Reserved1
        {
            get { return _reserved1; }
            set { _reserved1 = value; }
        }

        private string _valueDate;
        public string BETFOR21_ValueDate
        {
            get { return _valueDate; }
            set { _valueDate = value; }
        }

        private string _reserved2;
        public string BETFOR21_Reserved2
        {
            get { return _reserved2; }
            set { _reserved2 = value; }
        }

        #endregion
    }
}
