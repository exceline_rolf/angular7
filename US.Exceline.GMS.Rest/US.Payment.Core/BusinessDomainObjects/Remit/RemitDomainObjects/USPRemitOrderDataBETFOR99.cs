﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.Remit.RemitDomainObjects
{
    public class USPRemitOrderDataBETFOR99:IUSPRemitOrderDataBETFOR99
    {
        #region IUSPRemitOrderDataBETFOR99 Members

        private string _applicationHeader =string.Empty;
        public string BETFOR99_ApplicationHeader
        {
            get { return _applicationHeader; }
            set { _applicationHeader = value; }
        }

        private string _transactionCode = string.Empty;
        public string BETFOR99_TransactionCode
        {
            get { return _transactionCode; }
            set { _transactionCode = value; }
        }

        private string _enterpriseNumber = string.Empty;
        public string BETFOR99_EnterpriseNumber
        {
            get { return _enterpriseNumber; }
            set { _enterpriseNumber = value; }
        }

        private string _rserved1 = string.Empty;
        public string BETFOR99_Rserved1
        {
            get { return _rserved1; }
            set { _rserved1 = value; }
        }

        private string _rserved2 = string.Empty;
        public string BETFOR99_Rserved2
        {
            get { return _rserved2; }
            set { _rserved2 = value; }
        }

        private string _sequenceControlField = string.Empty;
        public string BETFOR99_SequenceControlField
        {
            get { return _sequenceControlField; }
            set { _sequenceControlField = value; }
        }

        private string _productionDate = string.Empty;
        public string BETFOR99_ProductionDate
        {
            get { return _productionDate; }
            set { _productionDate = value; }
        }

        private string _rserved3 = string.Empty;
        public string BETFOR99_Rserved3
        {
            get { return _rserved3; }
            set { _rserved3 = value; }
        }

        private string _noOfTransactions = string.Empty;
        public string BETFOR99_NoOfTransactions
        {
            get { return _noOfTransactions; }
            set { _noOfTransactions = value; }
        }

        private string _rserved4 = string.Empty;
        public string BETFOR99_Rserved4
        {
            get { return _rserved4; }
            set { _rserved4 = value; }
        }

        private string _d1HashValue = string.Empty;
        public string BETFOR99_D1HashValue
        {
            get { return _d1HashValue; }
            set { _d1HashValue = value; }
        }

        private string _d2HashValue = string.Empty;
        public string BETFOR99_D2HashValue
        {
            get { return _d2HashValue; }
            set { _d2HashValue = value; }
        }

        private string _seal1 = string.Empty;
        public string BETFOR99_Seal1
        {
            get { return _seal1; }
            set { _seal1 = value; }
        }

        private string _rserved5 = string.Empty;
        public string BETFOR99_Rserved5
        {
            get { return _rserved5; }
            set { _rserved5 = value; }
        }

        private string _userId2 = string.Empty;
        public string BETFOR99_UserId2
        {
            get { return _userId2; }
            set { _userId2 = value; }
        }

        private string _rserved6 = string.Empty;
        public string BETFOR99_Rserved6
        {
            get { return _rserved6; }
            set { _rserved6 = value; }
        }

        private string _seal2 = string.Empty;
        public string BETFOR99_Seal2
        {
            get { return _seal2; }
            set { _seal2 = value; }
        }

        private string _rserved7 = string.Empty;
        public string BETFOR99_Rserved7
        {
            get { return _rserved7; }
            set { _rserved7 = value; }
        }

        private string _security = string.Empty;
        public string BETFOR99_Security
        {
            get { return _security; }
            set { _security = value; }
        }

        private string _language = string.Empty;
        public string BETFOR99_Language
        {
            get { return _language; }
            set { _language = value; }
        }

        private string _version = string.Empty;
        public string BETFOR99_Version
        {
            get { return _version; }
            set { _version = value; }
        }

        private string _interface = string.Empty;
        public string BETFOR99_Interface
        {
            get { return _interface; }
            set { _interface = value; }
        }

        private string _controlField = string.Empty;
        public string BETFOR99_ControlField
        {
            get { return _controlField; }
            set { _controlField = value; }
        }

        private string _rserved8 = string.Empty;
        public string BETFOR99_Rserved8
        {
            get { return _rserved8; }
            set { _rserved8 = value; }
        }

        #endregion
    }
}
