﻿
namespace US.Payment.Core.BusinessDomainObjects.Remit
{
    public interface IUSPRemitApplicationHeader
    {
        //Position 1-2(2) 
        //Default Value=AH
        string AH_Id
        {
            get;
            set;
        }

        // Position 3(1) 
        string AH_Version
        {
            get ; 
            set ; 
        }

        // Position 4-5(2)
        //Default Value =02 for processing reply
        string AH_ReplyCode
        {
            get ; 
            set ; 
        }

        // Position 6-9(4)
        //Default Value =TBRI
        string AH_ProcedureId
        {
            get ; 
            set ; 
        }
        
        //Position 10-13(4)
        //Format =MMDD
        string AH_TransDate
        {
            get ; 
            set ; 
        }

        //Position 14-19(6)
        //Default= 1 for each day
        string AH_DaysSequenceNo
        {
            get ; 
            set ; 
        }

        //Position 20-27(8)
        //Reserved
        string AH_TransCode
        {
            get ; 
            set ;         
        }

        //Position 28-38(11)
        //Reserved
        string AH_EndUserId
        {
            get ; 
            set ; 
        }

        //Position 39-40(2)
        //default=04 (for payment services)
        string AH_NumberOf80
        {
            get ;
            set ; 
        }
    }
}
