﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.Remit
{
    public interface IUSPRemitOrderDataBETFOR99
    {

        //Positon(Length) 1-40(40)
        //Value =
        string BETFOR99_ApplicationHeader
        {
            get;
            set;
        }

        //Positon(Length) 41-48(8)
        //Value =BETFOR99
        string BETFOR99_TransactionCode
        {
            get;
            set;
        }

        //Positon(Length) 49-59(11)
        //Value =Company Number
        string BETFOR99_EnterpriseNumber
        {
            get;
            set;
        }

        //Positon(Length) 60-70(11)
        //Value =empty
        string BETFOR99_Rserved1
        {
            get;
            set;
        }

        //Positon(Length) 71-74(4)
        //Value =auto incremented 1 greater than before record
        string BETFOR99_SequenceControlField
        {
            get;
            set;
        }

        //Positon(Length) 75-80(6)
        //Value =empty
        string BETFOR99_Rserved2
        {
            get;
            set;
        }

        //Positon(Length) 81-84(4)
        //Value =empty
        string BETFOR99_ProductionDate
        {
            get;
            set;
        }

        //Positon(Length) 85-103(19)
        //Value =Empty
        string BETFOR99_Rserved3
        {
            get;
            set;
        }

        //Positon(Length) 104-108(5)
        //Value =BETFOR99_SequenceControlField (last record value)
        string BETFOR99_NoOfTransactions
        {
            get;
            set;
        }

        //Positon(Length) 109-231(123)
        //Value =empty
        string BETFOR99_Rserved4
        {
            get;
            set;
        }

        //Positon(Length) 232-239(8)
        //Value =Empty
        string BETFOR99_D1HashValue
        {
            get;
            set;
        }

        //Positon(Length) 240-247(8)
        //Value =Empty
        string BETFOR99_D2HashValue
        {
            get;
            set;
        }

        //Positon(Length) 248-254(7)
        //Value =Empty
        string BETFOR99_Seal1
        {
            get;
            set;
        }

        //Positon(Length) 255(1)
        //Value =Empty
        string BETFOR99_Rserved5
        {
            get;
            set;
        }

        //Positon(Length) 256-260(5)
        //Value =Empty
        string BETFOR99_UserId2
        {
            get;
            set;
        }

        //Positon(Length) 261-263(3)
        //Value =Empty
        string BETFOR99_Rserved6
        {
            get;
            set;
        }

        //Positon(Length) 264-270(7)
        //Value =Empty
        string BETFOR99_Seal2
        {
            get;
            set;
        }

        //Positon(Length) 271(1)
        //Value =Empty
        string BETFOR99_Rserved7
        {
            get;
            set;
        }

        //Positon(Length) 272-275(4)
        //Value =Empty
        string BETFOR99_Security
        {
            get;
            set;
        }

        //Positon(Length) 276(1)
        //Value =Empty
        string BETFOR99_Language
        {
            get;
            set;
        }

        //Positon(Length) 277(1)
        //Value =Empty
        string BETFOR99_Version
        {
            get;
            set;
        }

        //Positon(Length) 278(1)
        //Value =Empty
        string BETFOR99_Interface
        {
            get;
            set;
        }

        //Positon(Length) 279-296(18)
        //Value =Empty
        string BETFOR99_ControlField
        {
            get;
            set;
        }

        //Positon(Length) 297-320(20)
        //Value =Empty
        string BETFOR99_Rserved8
        {
            get;
            set;
        }

    }
}
