﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPPaymentStatus : IUSPPaymentStatus
    {
    #region IUSPPaymentStatus Members


private  string  _ID=string.Empty;
public string  ID
{
	  get 
	{ 
		 return _ID; 
	}
	  set 
	{ 
		_ID = value;
	}
}


private  string  _InkassoID=string.Empty;
public string  InkassoID
{
	  get 
	{ 
		 return _InkassoID; 
	}
	  set 
	{ 
		_InkassoID = value;
	}
}


private  string  _FileRow=string.Empty;
public string  FileRow
{
	  get 
	{ 
		 return _FileRow; 
	}
	  set 
	{ 
		_FileRow = value;
	}
}


private  string  _RecordTypeTxt=string.Empty;
public string  RecordTypeTxt
{
	  get 
	{
        return _RecordTypeTxt; 
	}
	  set 
	{
        _RecordTypeTxt = value;
	}
}

private int _RowType = -1;
public int RowType
{
    get { return _RowType; }
    set { _RowType = value; }
}

#endregion
}
}
