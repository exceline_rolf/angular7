﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects
{
    [DataContract]
    public class USPDebtor : IUSPDebtor
    {
        #region IUSPDebtor Members


        private string _FirstName = string.Empty;

        [DataMember]
        public string DebtorFirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }


        private string _SecondName = string.Empty;
        [DataMember]
        public string DebtorSecondName
        {
            get
            {
                return _SecondName;
            }
            set
            {
                _SecondName = value;
            }
        }


        private string _custumerID = string.Empty;
        [DataMember]
        public string DebtorcustumerID
        {
            get
            {
                return _custumerID;
            }
            set
            {
                _custumerID = value;
            }
        }


        private string _BirthDay = string.Empty;
        [DataMember]
        public string DebtorBirthDay
        {
            get
            {
                return _BirthDay;
            }
            set
            {
                _BirthDay = value;
            }
        }


        private string _PersonNo = string.Empty;
        [DataMember]
        public string DebtorPersonNo
        {
            get
            {
                return _PersonNo;
            }
            set
            {
                _PersonNo = value;
            }
        }


        private List<IUSPAddress> _AddressList = new List<IUSPAddress>();
        [DataMember]
        public List<IUSPAddress> DebtorAddressList
        {
            get
            {
                return _AddressList;
            }
            set
            {
                _AddressList = value;
            }
        }


        private int _EntityID = -1;
        [DataMember]
        public int DebtorEntityID
        {
            get
            {
                return _EntityID;
            }
            set
            {
                _EntityID = value;
            }
        }


        private int _EntityRoleID = -1;
        [DataMember]
        public int DebtorEntityRoleID
        {
            get
            {
                return _EntityRoleID;
            }
            set
            {
                _EntityRoleID = value;
            }
        }

        #endregion
    }
}
