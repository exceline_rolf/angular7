﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.OCR
{
   public interface IUSPOCRAssignmentRecord
    {
      US.Payment.Core.Enums.OCRRecordTypes RecordTypeOfFirstRecord
        {
            get ; set ; 
        }
       IUSPOCRStartAssignmentRecord StartAssignmentRecord
        {
            get;
            set;
        }

       List<IUSPOCRTransactionRecord> TransactionRecords
        {
            get;
            set;
        }

       IUSPOCREndAssignmentRecord EndAssignmentRecord
        {
            get;
            set;
        }
    }
}
