﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.OCR
{
    public interface IUSPOCRTransactionAmountItem2 : IUSPOCRTransactionMainPart
    {
      
         string FromNo
        {
            get;
            set;
        }

       
         string AgreementID
        {
            get;
            set;
        }

       
         string Filler
        {
            get;
            set;
        }

        
         string PaymentDate
        {
            get;
            set;
        }

       
         string DebetAccount
        {
            get;
            set;
        }
    }
}
