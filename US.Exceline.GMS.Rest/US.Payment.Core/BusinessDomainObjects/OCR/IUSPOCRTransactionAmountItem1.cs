﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.OCR
{
    public interface IUSPOCRTransactionAmountItem1 : IUSPOCRTransactionMainPart
    {
        
         string TransDate
        {
            get;
            set;
        }

       
         string CentralID
        {
            get;
            set;
        }

        
         string DayCode
        {
            get;
            set;
        }

        
         string PartSettlement
        {
            get;
            set;
        }

        
         string Counter
        {
            get;
            set;
        }

        
         string Sign
        {
            get;
            set;
        }

       
         string Amount
        {
            get;
            set;
        }

       
         string KID
        {
            get;
            set;
        }

       
         string FullerOrCardType
        {
            get;
            set;
        }
    }
}
