﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.OCR
{
    public interface IUSPOCRStartAssignmentRecord
    {
       string FormatCode
        {
            get ; 
            set ;
        }

       string ServiceCode
        {
            get;
            set;
        }
       string TaskType
        {
            get;
            set;
        }

       US.Payment.Core.Enums.OCRRecordTypes RecordType
        {
            get;
            set;
        }

        string AgreementID
        {
            get;
            set;
        }

       string TaskNumber
        {
            get;
            set;
        }

       string RecipientAccountNumber
        {
            get;
            set;
        }
    }
}
