﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.OCR
{
    public interface IUSPOCRErrorRecord : IUSPOCRTransactionRecord
    {
        

        //public OCRErrorRecord(string errorMsg)
        //{
        //    _errorMessage = errorMsg;
        //}
         string ErrorMessage
        {
           get;
            set;
        }

        #region IUSPOCRTransactionRecord Members

         int LineIndexOfFirstPart
        {
            get;
            set;

        }

         int LineIndexOfSecondPart
        {
            get;
            set;
        }

        #endregion
    }
}
