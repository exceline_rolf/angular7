﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.Enums;

namespace US.Payment.Core.BusinessDomainObjects.OCR
{
    public interface IUSPOCRTransactionMainPart
    {
       
         string FormatCode
        {
            get;
            set;
        }

        
         string ServiceCode
        {
           get;
            set;
        }
        
         string TransType
        {
           get;
            set;
        }

       
         OCRRecordTypes RecordType
        {
            get;
            set;
        }

        
         string TransNumber
        {
            get;
            set;
        }
    }
}
