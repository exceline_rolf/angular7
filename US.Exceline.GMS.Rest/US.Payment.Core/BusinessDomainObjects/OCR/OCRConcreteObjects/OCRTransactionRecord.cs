﻿// --------------------------------------------------------------------------
// Copyright(c) 2008 UnicornSolutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USAR
// Project Name      : US Payment
// Coding Standard   : US Coding Standards
// Author            : MRA
// Created Timestamp : 25/11/2009 10:30  AM
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.BusinessDomainObjects.OCR;

namespace US.Payment.Core.BusinessDomainObjects.OCR.OCRConcreteObjects
{
    public class OCRTransactionRecord : IUSPOCRTransactionRecord
    {

        #region IUSPOCRTransactionRecord Members

        public int LineIndexOfFirstPart
        {
            get;
            set; 
        }

        public int LineIndexOfSecondPart
        {
            get;
            set; 
        }

        #endregion
    }
}
