﻿// --------------------------------------------------------------------------
// Copyright(c) 2008 UnicornSolutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USAR
// Project Name      : US Payment
// Coding Standard   : US Coding Standards
// Author            : MRA
// Created Timestamp : 25/11/2009 10:30  AM
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.OCR.OCRConcreteObjects
{
    public class OCRErrorRecord : OCRTransactionRecord
    {
        private string _errorMessage;

        public OCRErrorRecord(string errorMsg)
        {
            _errorMessage = errorMsg;
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
        }
    }
}
