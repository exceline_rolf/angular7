﻿// --------------------------------------------------------------------------
// Copyright(c) 2008 UnicornSolutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USAR
// Project Name      : US Payment
// Coding Standard   : US Coding Standards
// Author            : MRA
// Created Timestamp : 25/11/2009 10:30  AM
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.OCR.OCRConcreteObjects
{
    public class OCRTransactionAmountItem1 : OCRTransactionMainPart
    {
        private string _transDate;
        /// <summary>
        /// Trans Date.
        /// Index of OCR file : 16-21
        /// </summary>
        public string TransDate
        {
            get { return _transDate; }
            set { _transDate = value; }
        }

        private string _centralID;
        /// <summary>
        /// Sentral ID.
        /// Index of OCR file : 22-23
        /// </summary>
        public string CentralID
        {
            get { return _centralID; }
            set { _centralID = value; }
        }

        private string _dayCode;
        /// <summary>
        /// Day Code.
        /// Index of OCR file : 24-25
        /// </summary>
        public string DayCode
        {
            get { return _dayCode; }
            set { _dayCode = value; }
        }

        private string _partSettlement;
        /// <summary>
        /// Part Settlement.
        /// Index of OCR file : 26
        /// </summary>
        public string PartSettlement
        {
            get { return _partSettlement; }
            set { _partSettlement = value; }
        }

        private string _counter;
        /// <summary>
        /// Index of OCR file : 27-31
        /// </summary>
        public string Counter
        {
            get { return _counter; }
            set { _counter = value; }
        }

        private string _sign;
        /// <summary>
        /// Neg. amount.
        /// Index of OCR file : 32
        /// </summary>
        public string Sign
        {
            get { return _sign; }
            set { _sign = value; }
        }

        private string _amount;
        /// <summary>
        /// Index of OCR file : 33-49
        /// </summary>
        public string Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private string _kid;
        /// <summary>
        /// Index of OCR file : 50-74
        /// </summary>
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }

        private string _fuller_cardType;
        /// <summary>
        /// Index of OCR file : 75-76
        /// </summary>
        public string FullerOrCardType
        {
            get { return _fuller_cardType; }
            set { _fuller_cardType = value; }
        }
    }
}
