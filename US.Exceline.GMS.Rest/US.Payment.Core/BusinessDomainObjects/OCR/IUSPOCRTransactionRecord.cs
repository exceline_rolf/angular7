﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.OCR
{
   public interface IUSPOCRTransactionRecord
    {

      int LineIndexOfFirstPart
        {
            get ;
            set ; 
        }

       int LineIndexOfSecondPart
        {
            get;
            set;
        }
    }
}
