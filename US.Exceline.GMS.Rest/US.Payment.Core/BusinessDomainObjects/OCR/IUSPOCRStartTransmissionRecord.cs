﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.OCR
{
    public interface IUSPOCRStartTransmissionRecord
    {
        string FormatCode
        {
            get ; 
            set ; 
        }

        string ServiceCode
        {
            get ; 
            set ; 
        }

        string SendingType
        {
            get ; 
            set ; 
        }

      US.Payment.Core.Enums.OCRRecordTypes RecordType
        {
            get ; 
            set ; 
        }

        string DataSender
        {
            get ; 
            set ; 
        }

        string SendingNumber
        {
            get ; 
            set ; 
        }

        string DataRecipient
        {
            get ; 
            set ; 
        }
    }
}
