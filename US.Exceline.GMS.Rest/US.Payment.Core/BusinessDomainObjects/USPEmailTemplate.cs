﻿

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPEmailTemplate
    {
        public USPEmailTemplate()
        {
            Name = string.Empty;
            OuterXml = string.Empty;
            CompanyID = -1;
            TemplateID = -1;
        }
        public int TemplateID { get; set; }
        public string Name { get; set; }
        public string OuterXml { get; set; }
        public int CompanyID { get; set; }
    }
}
