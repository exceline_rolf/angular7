﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Configurations
{
    public class ConfigItem
    {
        private string _key = string.Empty;

        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }

        private string _value = string.Empty;

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
