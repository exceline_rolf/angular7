﻿using System;
using System.IO;
using System.Xml.Serialization;
using US.Payment.Core.Exceptions;

namespace US.Payment.Core.Configurations
{
    internal static class USPConfigurationReader
    {
        private static USPConfigurations _configObj = null;
        private static CustomConfigurations _workStationConfig = null;
        public static USPConfigurations GetUSPCongfiuration()
        {
            if (_configObj != null)
                return _configObj;

            try
            {
                string fileName =new  FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).Directory.FullName + @"\USPConfigurations.xml";

                // Following if find the file in read location when the .dll is called from a web application
                if (!File.Exists(fileName))
                {
                    fileName = System.Configuration.ConfigurationSettings.AppSettings["USP_Configuration_File_Path"];
                    //if (!Directory.Exists(@"C:\USPLog\"))
                    //{
                    //    Directory.CreateDirectory(@"C:\USPLog\");
                    //}
                    //File.WriteAllText(@"C:\USPLog\USPConfig.txt", "USP CONFIG");
                    //File.WriteAllText(@"C:\USPLog\USPConfig.txt", fileName);
                }

                using (TextReader textReader = new StreamReader(fileName))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(USPConfigurations));
                    object deserializedObject = serializer.Deserialize(textReader);
                    serializer = null;
                    _configObj = (USPConfigurations)deserializedObject;
                    return _configObj;
                }
                
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("Failed to read USP Configurations from file [USPConfigurations.xml]. " + ex.Message, ex);
            }
        }
    }
}
