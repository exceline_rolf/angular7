﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Configurations
{
    public class CustomConfigurations
    {
        private List<ConfigItem> _configurations = new List<ConfigItem>();

        public List<ConfigItem> Configurations
        {
            get { return _configurations; }
            set { _configurations = value; }
        }
    }
}
