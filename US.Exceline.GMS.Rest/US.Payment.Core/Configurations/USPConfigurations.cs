﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Configurations
{
    public class USPConfigurations
    {
        public string USPConnectionString {get; set;}
        public string LOGConnectionString { get; set; }
        public string PaymentMachineConnectionString { get; set; }
        public string ReportServerConnectionString { get; set; }

        private CustomConfigurations _extendedConfiguratiions = new CustomConfigurations();
        public CustomConfigurations ExtendedConfiguratiions
        {
            get { return _extendedConfiguratiions; }
            set { _extendedConfiguratiions = value; }
        }
    }
}
