﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.Enums;


namespace US.Payment.Core.DomainObjects
{
   public interface IUSPDirectDeduct
    {
        string AccountNumber { get; set; }
       string ContractNumber{ get; set; }
        string ContractExpire{ get; set; }
       string LastContact{ get; set; }
        string ContractKID{ get; set; }
        DirectDeductStatus Status{ get; set; }
        string InstallmentNumber{ get; set; }
        string InstallmentKID{ get; set; }
       string TransmissionNumberBBS{ get; set; }
        string Balance{ get; set; }
        string DueBalance{ get; set; }
        string LastReminder{ get; set; }
        CollectingStatus CollectingStatus{ get; set; }
        string InkassoTest{ get; set; }
        string BranchNumber{ get; set; }
        string DueDate{ get; set; }
        string Amount{ get; set; }
        string DebtorName{ get; set; }
       string NameInContract{ get; set; }
        string LastVisit{ get; set; }
       string PrintDate{ get; set; }
        string OriginalDueDate{ get; set; }
        string ReminderFee{ get; set; }
        string InvoiceAmount { get; set; }

             
    }
}
