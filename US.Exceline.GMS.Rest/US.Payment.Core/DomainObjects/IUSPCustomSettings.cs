﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPCustomSetting
    {
         int ID { get; set; }
         int FieldID { get; set; }
         string Name { get; set; }
         string DisplayName { get; set; }
         int DataTypeID { get; set; }
         object Value { get; set; }
         object DefaultValue { get; set; }
         string DataType { get; set; }
    }
}
