﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
   public interface IUSPPortalInvoice
    {
         string InvoiceRef{ get; set; }
       string InvoiceDate{ get; set; }
        string InvoiceDueDate{ get; set; }
        double InvoiceAmount{ get; set; }
       double InvoiceBalance{ get; set; }
       string Kid { get; set; }
    }
}
