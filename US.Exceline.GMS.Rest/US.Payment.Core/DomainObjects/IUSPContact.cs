﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPContact
    {
         string TelMobile { get; set; }
       string TelHome { get; set; }
        string TelWork { get; set; }
        string EMail { get; set; }
        bool IsDefault { get; set; }
        string Email { get; set; }
        string Fax { get; set; }
        string MSN { get; set; }
        string Skype { get; set; }
        string SMS { get; set; }
        string LastName { get; set; }
        string FirstName { get; set; }
        string Title { get; set; }
        string Position { get; set; }
        string Address { get; set; }
        string TelDirect { get; set; }
        
    }
}
