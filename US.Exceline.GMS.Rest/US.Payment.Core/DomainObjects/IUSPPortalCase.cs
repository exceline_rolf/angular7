﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
   public interface IUSPPortalCase
    {
         List<IUSPPortalInvoice> InvoiceList{ get; set; }
         List<IUSPPortalPayment> PaymentList{ get; set; }
        int CaseNo{ get; set; }
        string Name{ get; set; }
        string Address1{ get; set; }
        string Address2{ get; set; }
        string PostalCode{ get; set; }
        string PostalArea{ get; set; }
        string CustomerID { get; set; }
    }
}
