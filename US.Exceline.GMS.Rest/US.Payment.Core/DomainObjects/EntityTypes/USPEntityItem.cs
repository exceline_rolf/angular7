﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects.EntityTypes
{
    public class USPEntityItem
    {
        private string _name = string.Empty;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _xmlFileName = string.Empty;
        public string XMLFileName
        {
            get { return _xmlFileName; }
            set { _xmlFileName = value; }
        }
    }
}
