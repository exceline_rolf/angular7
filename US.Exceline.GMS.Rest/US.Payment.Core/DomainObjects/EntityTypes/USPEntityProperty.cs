﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.WorkflowController.DomainObjects.Activity
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : 30/06/2011 HH:MM  PM
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects.EntityTypes
{
    public class USPEntityProperty 
    {
        private string _id = string.Empty;
        public string ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        private object _value = string.Empty;
        public object Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }
        private string _type = string.Empty;
        public string Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }
        private string _displayName = string.Empty;
        public string DisplayName
        {
            get
            {
                return _displayName;
            }
            set
            {
                _displayName = value;
            }
        }
        private string _toolTip = string.Empty;
        public string ToolTip
        {
            get
            {
                return _toolTip;
            }
            set
            {
                _toolTip = value;
            }
        }

        bool _isRepeatingDataItem = false;
        public bool IsRepeatingDataItem
        {
            get
            {
                return _isRepeatingDataItem;
            }
            set
            {
                _isRepeatingDataItem = value;
            }
        }

        //private List<IProperty> _repeatingParameters = null;
        //public List<IProperty> RepeatingParameters
        //{
        //    get
        //    {
        //        return _repeatingParameters;
        //    }
        //    set
        //    {
        //        _repeatingParameters = value;
        //    }
        //}
    }
}
