﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.WorkflowController.DomainObjects.Activity
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : 30/06/2011 HH:MM  PM
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.SystemObjects.CustomSetting;

namespace US.Payment.Core.DomainObjects.EntityTypes
{
    public class USPEntityType
    {
        private string _name = string.Empty;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private List<USPEntityProperty> _properties = new List<USPEntityProperty>();
        public List<USPEntityProperty> Properties
        {
            get { return _properties; }
            set { _properties = value; }
        }

        private List<USPCustomSettingMetaData> _customerSettings = new List<USPCustomSettingMetaData>();
        public List<USPCustomSettingMetaData> CustomerSettings
        {
            get { return _customerSettings; }
            set { _customerSettings = value; }
        }
    }
}
