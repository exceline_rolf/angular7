﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPAddress
    {
        int addrNo { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string Address3 { get; set; }
        string ZipCode { get; set; }
        string CountryCode { get; set; }
        string AddSource { get; set; }
        string City { get; set; }
        string TelMobile { get; set; }
        string TelWork { get; set; }
        string TelHome { get; set; }
        string Email { get; set; }
        string Fax { get; set; }
        string MSN { get; set; }
        string Skype { get; set; }
        string SMS { get; set; }
        bool IsDefault { get; set; }
        string AddressName { get; set; }
        string Branch { get; set; }
        string PostCode { get; set; }
        string Municipality { get; set; }

    }
}
