﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPCancellation
    {
       int NumberOfClaims{ get; set; }
       string CreditorName{ get; set; }
       string CreditorID{ get; set; }
       DateTime FromDate{ get; set; }
       DateTime ToDate{ get; set; }
       string FileName{ get; set; }
       string TranmissionID{ get; set; }
       double TotalAmount{ get; set; }
       bool Cancel{ get; set; }
       bool CancelWCredit { get; set; }
       
    }
}
