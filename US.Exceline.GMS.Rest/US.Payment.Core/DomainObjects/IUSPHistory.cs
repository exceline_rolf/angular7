﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
   public interface IUSPHistory
    {
        string FileName{ get; set; }
        int Error{ get; set; }
        string Comment { get; set; }
       
    }
}
