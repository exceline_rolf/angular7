﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
   public interface IUSPDPHistory
    {
       string FileName{ get; set; }
       int SMrecodes{ get; set; }
       int DBrecodes{ get; set; }
       int errorRecodes { get; set; }
        
    }
}
