﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.DomainObjects
{
    [DataContract]
    public class TransactionProfileSpList
    {
        private List<string> _findDestinationSpList = new List<string>();
        private List<string> _saveTransactionSpList = new List<string>();
        private List<string> _distributeTransactionSpList = new List<string>();

        [DataMember]
        public List<string> FindDestinationSpList
        {
            get { return _findDestinationSpList; }
            set { _findDestinationSpList = value; }
        }

        [DataMember]
        public List<string> SaveTransactionSpList
        {
            get { return _saveTransactionSpList; }
            set { _saveTransactionSpList = value; }
        }

        [DataMember]
        public List<string> DistributeTransactionSpList
        {
            get { return _distributeTransactionSpList; }
            set { _distributeTransactionSpList = value; }
        }
    }
}
