﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
   public interface IUSPSearch
    {
      string CustId { get; set; }
 		string ARNo{ get; set; }
        string Name{ get; set; }
        int EntId{ get; set; }
        string RoleId{ get; set; }
        int CaseNo { get; set; }
    }
}
