﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPInvoicePrintingInfoForCredicare
    {
        int CreditiorNumber { get; set; }
        string CreditorName { get; set; }
        string DebtorNumber { get; set; }
        string DebtorName { get; set; }
        string FileName { get; set; }
        string InvoicePath { get; set; }
        string InvoiceNo { get; set; }
        string RegDate { get; set; }
    }
}
