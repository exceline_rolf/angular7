﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPARItemHistory
    {
       int ARItemNumber { get; set; }
       string ItemType { get; set; }
       string StatusText { get; set; }
       string StatusDate { get; set; }
    }
}
