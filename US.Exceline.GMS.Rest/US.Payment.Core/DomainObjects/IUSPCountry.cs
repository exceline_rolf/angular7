﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPCountry
    {
        string CountryId { get; set; }
        string CountryName { get; set; }
    }
}
