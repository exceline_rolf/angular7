﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPDelayType
    {
        int ID{ get; set; }
        string Text { get; set; }
        
    }
}
