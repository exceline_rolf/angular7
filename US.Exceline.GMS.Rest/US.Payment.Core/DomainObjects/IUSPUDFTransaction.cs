﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
   public interface IUSPUDFTransaction
    {
        string CID{ get; set; }
        bool IsSuccess{ get; set; }
       string PID{ get; set; }
        double TAmount{ get; set; }
        string TCaseNumber{ get; set; }
        string TCreditinvoice{ get; set; }
       string TDate{ get; set; }
        string TDueDate{ get; set; }
        string TInfo{ get; set; }
        double TInterest{ get; set; }
        string TKid{ get; set; }
        string TNumber{ get; set; }
       string TReference1{ get; set; }
       string TReference2{ get; set; }
        string TStatus{ get; set; }
        string TType{ get; set; }
        int ARItemNo { get; set; }
    }
}
