﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPPaymentStatus
    {
          string ID{ get; set; }
        string InkassoID{ get; set; }
        string FileRow{ get; set; }
        string RecordTypeTxt { get; set; }
    }
}
