﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSUDFTransferedResult
    {
        string ExternalCaseID { get; set; }
        List<TransactionResult> TransactionResults { get; set; }

    }
        
        public interface TransactionResult
        {
            int ARItemNo{ get; set; }
           string ExternalTransID{ get; set; }
            
    }
}