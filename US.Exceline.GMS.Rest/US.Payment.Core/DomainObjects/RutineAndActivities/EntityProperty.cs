﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.DomainObjects.RutineAndActivities
{
    [DataContract]
    public  class EntityProperty
    {
        private string _displayName = string.Empty;
        [DataMember]
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }
        private string _iD = string.Empty;
        [DataMember]
        public string ID
        {
            get { return _iD; }
            set { _iD = value; }
        }
        private bool _isRepeatingDataItem = false;
        [DataMember]
        public bool IsRepeatingDataItem
        {
            get { return _isRepeatingDataItem; }
            set { _isRepeatingDataItem = value; }
        }
        private List<EntityProperty> _repeatingParameters = new List<EntityProperty>();
        [DataMember]
        public List<EntityProperty> RepeatingParameters
        {
            get { return _repeatingParameters; }
            set { _repeatingParameters = value; }
        }
        private string _toolTip = string.Empty;
        [DataMember]
        public string ToolTip
        {
            get { return _toolTip; }
            set { _toolTip = value; }
        }
        private string _type = string.Empty;
        [DataMember]
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
       
    }
}
