﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects.RutineAndActivities
{
    public class SQLSPParameter
    {
        private string _name = string.Empty;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _dataType = string.Empty;

        public string DataType
        {
            get { return _dataType; }
            set { _dataType = value; }
        }
        private ValueType _valueType = ValueType.Fix;

        public ValueType ValueType
        {
            get { return _valueType; }
            set { _valueType = value; }
        }
        private int _dataTypeLength = 100;

        public int DataTypeLength
        {
            get { return _dataTypeLength; }
            set { _dataTypeLength = value; }
        }
        //private string _parameterDirection = string.Empty;

        //public string ParameterDirection
        //{
        //    get { return _parameterDirection; }
        //    set { _parameterDirection = value; }
        //}
        private string _value = string.Empty;
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
        //private int _dataTypeLength = -1;

        //public int DataTypeLength
        //{
        //    get { return _dataTypeLength; }
        //    set { _dataTypeLength = value; }
        //}
        private string _parameterDirection = string.Empty;

        public string ParameterDirection
        {
            get { return _parameterDirection; }
            set { _parameterDirection = value; }
        }
    }
    public enum ValueType
    {
        Parameter = 0,
        Fix = 1
    }
}
