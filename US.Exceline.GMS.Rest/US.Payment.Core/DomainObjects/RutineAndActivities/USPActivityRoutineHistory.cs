﻿using System;
using System.Runtime.Serialization;

namespace US.Payment.Core.DomainObjects.RutineAndActivities
{
    [DataContract]
    public class USPActivityRoutineHistory
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _wFState = string.Empty;
        [DataMember]
        public string WFState
        {
            get { return _wFState; }
            set { _wFState = value; }
        }
        private int _activityId = -1;
        [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }
        private int _routineId = -1;
        [DataMember]
        public int RoutineId
        {
            get { return _routineId; }
            set { _routineId = value; }
        }
        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        private DateTime _scheduleDate = DateTime.MinValue;
        [DataMember]
        public DateTime ScheduleDate
        {
            get { return _scheduleDate; }
            set { _scheduleDate = value; }
        }
        private DateTime _executedDate = DateTime.MinValue;
        [DataMember]
        public DateTime ExecutedDate
        {
            get { return _executedDate; }
            set { _executedDate = value; }
        }
        private string _scheduledBy = string.Empty;
        [DataMember]
        public string ScheduledBy
        {
            get { return _scheduledBy; }
            set { _scheduledBy = value; }
        }
        private string _status = string.Empty;
        [DataMember]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        private bool _iconVisibilityActivity  = false;
        [DataMember]
        public bool IconVisibilityActivity
        {
            get { return _iconVisibilityActivity; }
            set { _iconVisibilityActivity = value; }
        }

        private bool _iconVisibilityRoutine = false;
        [DataMember]
        public bool IconVisibilityRoutine
        {
            get { return _iconVisibilityRoutine; }
            set { _iconVisibilityRoutine = value; }
        }

        private string _errorText = string.Empty;
        [DataMember]
        public string ErrorText
        {
            get { return _errorText; }
            set { _errorText = value; }
        }

    }
}
