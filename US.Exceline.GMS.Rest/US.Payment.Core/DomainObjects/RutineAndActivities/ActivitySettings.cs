﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.DomainObjects.RutineAndActivities
{
    [DataContract]
    public class ActivitySettings
    {

        private string _type = string.Empty;
        [DataMember]
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
        

        private string _dllName = string.Empty;
        [DataMember]
        public string DllName
        {
            get { return _dllName; }
            set { _dllName = value; }
        }
    }
}
