﻿using System.Runtime.Serialization;

namespace US.Payment.Core.DomainObjects.RutineAndActivities
{
    [DataContract]
    public class ExecutableItem
    {
        private string _executionType = string.Empty;
        [DataMember]
        public string ExecutionType
        {
            get { return _executionType; }
            set { _executionType = value; }
        }
        private string _executionName = string.Empty;
        [DataMember]
        public string ExecutionName
        {
            get { return _executionName; }
            set { _executionName = value; }
        }
        private int _entitytNo = -1;
        [DataMember]
        public int EntityNo
        {
            get { return _entitytNo; }
            set { _entitytNo = value; }
        }
        private string _workflowType = string.Empty;
        [DataMember]
        public string WorkflowType
        {
            get { return _workflowType; }
            set { _workflowType = value; }
        }
    }
}
