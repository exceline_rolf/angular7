﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects.RutineAndActivities
{
   public class SPActivitySettings
    {
        private string _spName = string.Empty;

        public string SpName
        {
            get { return _spName; }
            set { _spName = value; }
        }
        private List<SQLSPParameter> _parameterList = new List<SQLSPParameter>();

        public List<SQLSPParameter> ParameterList
        {
            get { return _parameterList; }
            set { _parameterList = value; }
        }
    }
}
