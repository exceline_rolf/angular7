﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.WorkflowController.DomainObjects.Activity
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : 24/06/2011 HH:MM  PM
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Workflows
{
    public class DBSettings
    {
        private string _dbName = string.Empty;
        public string DB_Setting_DBName
        {
            get { return _dbName; }
            set { _dbName = value; }
        }

        private string _spName = string.Empty;
        public string DB_Setting_SPName
        {
            get { return _spName; }
            set { _spName = value; }
        }

        private List<Parameter> _parameterList = new List<Parameter>();
        public List<Parameter> ParameterList
        {
            get { return _parameterList; }
            set { _parameterList = value; }
        }
    }
}
