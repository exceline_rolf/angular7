﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.WorkflowController.DomainObjects
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : 24/06/2011 HH:MM  PM
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Workflows
{
    public class USPActivityHistory
    {
        private int _id = 0;
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _invoiceNumber = 0;
        public int InvoiceNumber
        {
            get { return _invoiceNumber; }
            set { _invoiceNumber = value; }
        }

        private string _activityName = string.Empty;
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        private DateTime _runningDate = DateTime.MinValue;
        public DateTime RunningDate 
        {
            get { return _runningDate; }
            set { _runningDate = value; }
        }
    }
}
