﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Exceptions
{
    public class USPInvalidInvoiceException: Exception
    {
        public USPInvalidInvoiceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
