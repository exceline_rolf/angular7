﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Exceptions
{
    public class USPInvalidDebtorException : Exception
    {
        public USPInvalidDebtorException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
