﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Exceptions
{
    public class USLogDBException:Exception
    {
        public USLogDBException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
