﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Exceptions
{
    public class USPOperationFailedException : Exception
    {
        public USPOperationFailedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
