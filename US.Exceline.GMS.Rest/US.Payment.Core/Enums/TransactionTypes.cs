﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Enums
{
   
        public enum TransactionTypes
        {
            TransactionGiroDebetAccount = 10,
            StandingOrder = 11,
            DirectRemit = 12,
            CompanyTerminalGiro = 13,
            PaydInBank = 14,
            DirectDeductATG = 15,
            PaydOverPhone = 16,
            PaidCashInBank = 17,
            RevercingWithKID = 18,
            PurchaseWithKID = 19,
            RevercingWithText = 20,
            PurchaseWithText = 21
        }
        public enum OCRRecordTypes
        {
            StartSending = 10,
            StartRecordAssingment = 20,
            StartTransactionAmountItem1 = 30,
            StartTransactionAmountItem2 = 31,
            EndRecordAssignment = 88,
            EndSending = 89,
            RecordDirectDeduct = 70
        }
   
}
