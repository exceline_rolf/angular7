﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Enums
{
    public enum DirectDeductStatus
    {
       New = 1,
       Approved = 6,
       Deleted = 7
    }

  
   
    public enum CollectingStatus
    {
       NoStatus = 0,
       NewDirectDeduct = 1,
       MandatoryNotApprovedWhenFileSent = 2,
       SentButNotPaid = 3,
       MandatoryDeleted = 4,
       UnpaidInvoice = 8,
       UnpaidReminder = 9,

    }

    public enum FreezingType
    {
        FreezeDebtor,
        FreezeInvoice
    }

    
    public enum FreezingStatesResult
    {
        ARFreezed,
       ARUnfreezed,
        CaseFreezed,
        CaseUnfreezed
    }
    /// <summary>
    /// Type of invoices that are handled by the USP. Assigned integer value is the database
    /// Item Type equivalent
    /// </summary>
    public enum InvoiceTypes : int
    {
        DirectDeduct = 1,
        InvoiceForPrint = 2,
        DebtWarning = 4,
        Sponser = 7,
        Invoice = 3,
        DB = 11,
        SM = 12,
        OP=5,
        OL=8,
        CL=13,     
        PaymentDocumentForPrint = 14,
        PaymentMemo=10,
        Default=-1,
        ClassInvoice = 100,
        ShopInvoice = 101,
        DRPInvoice = 102
    }

    public enum ErrorLevels
    {
        Error,
        Warning,
        Info
    }
    public enum ATGRecordType
    {
        NotifyByBank=1,
        NotifyByCrediCare=0,
        Cancellation=2
    }
}
