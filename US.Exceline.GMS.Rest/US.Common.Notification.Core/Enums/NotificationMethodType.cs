﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Common.Notification.Core.Enums
{
    public enum NotificationMethodType
    {

        EMAIL,
        SMS,
        POPUP,
        POST,
        EVENTLOG,
        ERRORLOG,
        NONE,
        OTHER
    }
}
