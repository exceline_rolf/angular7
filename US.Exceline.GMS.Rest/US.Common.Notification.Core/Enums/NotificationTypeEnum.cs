﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Common.Notification.Core.Enums
{
    public enum NotificationTypeEnum
    {
        Errors = 1,
        Warning = 2,
        TODO = 3,
        Messages = 4
    }
}
