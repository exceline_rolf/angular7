﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace US.Common.Notification.Core.Utils
{
    public class NotificationUtils
    {
        public static string GenerateNotificationLinkXML(string linkText, string module, string operation, string feature, Dictionary<string, string> arguments)
        {
            var xml = new XElement("NotificationLink",
                                new XElement("LinkText", linkText),
                                new XElement("Module", module),
                                new XElement("Operation", operation),
                                new XElement("Feature", feature),
                                new XElement("Arguments",
                                    from a in arguments
                                    select new XElement(a.Key, a.Value))
                            );
            return xml.ToString();
        }
    }
}
