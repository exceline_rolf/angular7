﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Common.Notification.Core.DomainObjects
{
    public class ActionGridRow
    {
        public int ActionId { get; set; }
        public string Comment { get; set; }
        public string AssgnAction { get; set; }
        public string ReAssignAction { get; set; }
        public float TimeSpent { get; set; }
    }
}
