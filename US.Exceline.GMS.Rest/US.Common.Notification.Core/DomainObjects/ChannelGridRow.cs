﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Common.Notification.Core.DomainObjects
{
    public class ChannelGridRow
    {
        public int ChannelId { get; set; }
        public DateTime NotificationDueDate { get; set; }
        public string Occurance { get; set; }
        public string NotificationMessage { get; set; }
        public DateTime NotificationNextDueDate { get; set; }
        public string AssignChannel { get; set; }
        public bool ActiveStatus { get; set; }
        public string USCTemplateCategory { get; set; }
        public string USCTemplateName { get; set; }
        public string NotificationType { get; set; }
    }
}
