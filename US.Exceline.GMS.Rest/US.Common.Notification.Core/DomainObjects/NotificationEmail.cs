﻿
namespace US.Common.Notification.Core.DomainObjects
{
    public class NotificationEmail
    {
        public int NotificationID { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public string Attachment { get; set; }
        public string Sender { get; set; }
        public string Receiver { get; set; }
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
    }
}
