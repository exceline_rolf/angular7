﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Common.Notification.Core.DomainObjects
{
    public class USNotificationAction
    {
        public int Id { get; set; }
        public int NotificationId { get; set; }
        public string Comment { get; set; }
        public string AssgnId { get; set; }
        public string ReAssignId { get; set; }
        public float TimeSpent { get; set; }
    }
}
