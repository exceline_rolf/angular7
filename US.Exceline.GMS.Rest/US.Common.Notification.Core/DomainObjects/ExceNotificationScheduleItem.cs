﻿
namespace US.Common.Notification.Core.DomainObjects
{
    public class ExceNotificationScheduleItem
    {
        public int Id { get; set; }
        public int ScheduleID { get; set; }
        public string Template { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string NotificationId { get; set; }
    }
}
