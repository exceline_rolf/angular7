﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.Common.Notification.Core.Enums;

namespace US.Common.Notification.Core.DomainObjects
{
    [DataContract]
    public class USCommonNotificationDC
    {
        private int _notificationId = -1;
        [DataMember]
        public int NotificationId
        {
            get { return _notificationId; }
            set { _notificationId = value; }
        }

        private int _branchID = -1;
        [DataMember]
        public int BranchID
        {
            get { return _branchID; }
            set { _branchID = value; }
        }        

        private int _memberID = -1;
        [DataMember]
        public int MemberID
        {
            get { return _memberID; }
            set { _memberID = value; }
        }

        private bool _isTemplateNotification = false;
        [DataMember]
        public bool IsTemplateNotification
        {
            get { return _isTemplateNotification; }
            set { _isTemplateNotification = value; }
        }

        private TextTemplateEnum _type = TextTemplateEnum.NONE;
        [DataMember]
        public TextTemplateEnum Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private NotificationMethodType _method = NotificationMethodType.NONE;
        [DataMember]
        public NotificationMethodType Method
        {
            get { return _method; }
            set { _method = value; }
        }

        private Dictionary<TemplateFieldEnum, string> _parameterString = new Dictionary<TemplateFieldEnum, string>();

        [DataMember]
        public Dictionary<TemplateFieldEnum, string> ParameterString
        {
            get { return _parameterString; }
            set { _parameterString = value; }
        }

        private string _title = string.Empty;
        [DataMember]
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _cretaedUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get { return _cretaedUser; }
            set { _cretaedUser = value; }
        }

        private string _role = string.Empty; //   MEM/EMP
        [DataMember]
        public string Role
        {
            get { return _role; }
            set { _role = value; }
        }

        private NotificationTypeEnum _notificationType = NotificationTypeEnum.Messages;
        [DataMember]
        public NotificationTypeEnum NotificationType
        {
            get { return _notificationType; }
            set { _notificationType = value; }
        }

        private NotificationSeverityEnum _severity = NotificationSeverityEnum.Minor;
        [DataMember]
        public NotificationSeverityEnum Severity
        {
            get { return _severity; }
            set { _severity = value; }
        }

        private NotificationStatusEnum _status = NotificationStatusEnum.New;
        [DataMember]
        public NotificationStatusEnum Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }       

        private string _attachmentFileParth = string.Empty;
        [DataMember]
        public string AttachmentFileParth
        {
            get { return _attachmentFileParth; }
            set { _attachmentFileParth = value; }
        }

        private string _senderDescription = string.Empty;
        [DataMember]
        public string SenderDescription
        {
            get { return _senderDescription; }
            set { _senderDescription = value; }
        }

        private string _tempateText = string.Empty; 
        [DataMember]
        public string TempateText
        {
            get { return _tempateText; }
            set { _tempateText = value; }
        }

        private Dictionary<int, string> _memberIdList = new Dictionary<int, string>();
          [DataMember]
        public Dictionary<int, string> MemberIdList
        {
            get { return _memberIdList; }
            set { _memberIdList = value; }
        }

        private string _recordType = string.Empty;
        [DataMember]
        public string RecordType
        {
            get { return _recordType; }
            set { _recordType = value; }
        }

        private int _severityId;
        [DataMember]
        public int SeverityId
        {
            get { return _severityId; }
            set { _severityId = value; }
        }

        private int _statusId;
        [DataMember]
        public int StatusId
        {
            get { return _statusId; }
            set { _statusId = value; }
        }

        private int _typeId;
        [DataMember]
        public int TypeId
        {
            get { return _typeId; }
            set { _typeId = value; }
        }



    }
}
