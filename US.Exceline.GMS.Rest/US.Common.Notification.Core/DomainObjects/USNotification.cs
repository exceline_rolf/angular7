﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using US.Common.Notification.Core.Enums;

namespace US.Common.Notification.Core.DomainObjects
{
    public class USNotification
    {
        public int NotificationId { get; set; }
        public string Module { get; set; }
        public TextTemplateEnum TextTemplate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public TimeSpan TimeStamp { get; set; }
        public DateTime CreatedDate { get; set; }
      
        public string CreatedUser { get; set; }
        public string CreatedUserRoleId { get; set; }
        public int TypeId { get; set; }
        public int SeverityId { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? LastAttendDate { get; set; }
        public int StatusId { get; set; }
    }
}
