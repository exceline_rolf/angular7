﻿
namespace US.Common.Notification.Core.DomainObjects
{
   
    public class NotificationTemplateText
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _methodID;
        public int MethodID
        {
            get { return _methodID; }
            set { _methodID = value; }
        }
        
        private int _typeID;
        public int TypeID
        {
            get { return _typeID; }
            set { _typeID = value; }
        }

        private string _text;
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

    }
}
