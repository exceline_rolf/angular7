﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Common.Notification.Core.Enums;

namespace US.Common.Notification.Core.DomainObjects
{
  public class TemplateField
    {

      private TemplateFieldEnum _key = TemplateFieldEnum.NONE;

      public TemplateFieldEnum Key
        {
            get { return _key; }
            set { _key = value; }
        }


        private string _value = string.Empty;

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
        
    }
}
