﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Common.Notification.RestApi.Models
{
    public class SearchNotifications
    {
      
        public int branchId { get; set; }
        public string title { get; set; }

        public string assignTo { get; set; }
        public DateTime? receiveDateFrom { get; set; }
        public DateTime? receiveDateTo { get; set; }
        public DateTime? dueDateFrom { get; set; }
        public DateTime? dueDateTo { get; set; }
        public List<int> severityIdList { get; set; }
        public List<int> typeIdList { get; set; }
        public int statusId { get; set; }

    }
}