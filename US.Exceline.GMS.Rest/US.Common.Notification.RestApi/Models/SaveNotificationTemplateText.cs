﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.Common.Notification.Core.DomainObjects;

namespace US.Common.Notification.RestApi.Models
{
    public class SaveNotificationTemplateText
    {
      
        public int entityId { get; set; }
        public NotificationTemplateText templatetText { get; set; }

        public int branchId { get; set; }

    }
}