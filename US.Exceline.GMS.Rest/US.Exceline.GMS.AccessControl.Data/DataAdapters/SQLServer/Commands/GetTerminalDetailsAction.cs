﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.AccessControl.Data.DataAdapters.SQLServer.Commands
{
    public class GetTerminalDetailsAction : USDBActionBase<List<ExceAccessControlTerminalDetailDC>>
    {
        private readonly int _branchId;

        public GetTerminalDetailsAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<ExceAccessControlTerminalDetailDC> Body(DbConnection connection)
        {
            const string storedProcedure = "USExceGMSAccessControlGetTerminals";
            List<ExceAccessControlTerminalDetailDC> terminalDetailList = new List<ExceAccessControlTerminalDetailDC>();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExceAccessControlTerminalDetailDC terminalDetail = new ExceAccessControlTerminalDetailDC();
                    terminalDetail.Id = Convert.ToInt32(reader["ID"]);
                    terminalDetail.Name = Convert.ToString(reader["Name"]);
                    terminalDetail.Port = Convert.ToInt32(reader["Port"]);
                    terminalDetail.IsOnline = Convert.ToBoolean(reader["IsOnline"]);
                    terminalDetail.Location = Convert.ToString(reader["Location"]);
                    terminalDetail.TypeId = Convert.ToString(reader["TypeID"]);
                    terminalDetail.IsSecondIdentification = Convert.ToBoolean(reader["IsSecondIden"]);
                    terminalDetail.SalePointId = Convert.ToInt32(reader["SalePointId"]);
                    terminalDetailList.Add(terminalDetail);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return terminalDetailList;
        }
    }
}
