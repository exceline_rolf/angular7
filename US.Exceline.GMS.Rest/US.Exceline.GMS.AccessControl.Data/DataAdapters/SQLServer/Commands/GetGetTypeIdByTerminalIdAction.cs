﻿using System;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.AccessControl.Data.DataAdapters.SQLServer.Commands
{
    public class GetGetTypeIdByTerminalIdAction : USDBActionBase<ExceAccessControlTerminalDetailDC>
    {
        private readonly int _terminalId;

        public GetGetTypeIdByTerminalIdAction(int terminalId)
        {
            _terminalId = terminalId;
        }

        protected override ExceAccessControlTerminalDetailDC Body(DbConnection connection)
        {
            const string storedProcedure = "USExceGMSAccessControlGetTerminalType";
            ExceAccessControlTerminalDetailDC terminalDetail = new ExceAccessControlTerminalDetailDC();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@terminalId", DbType.Int32, _terminalId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    terminalDetail.TypeId = Convert.ToString(reader["TypeID"]);
                    terminalDetail.Name = Convert.ToString(reader["TerminalName"]);
                    terminalDetail.IsPaid = Convert.ToBoolean(reader["IsPaid"]);
                    terminalDetail.SalePointId = Convert.ToInt32(reader["SalePointId"]);
                    terminalDetail.ArticleNo = Convert.ToString(reader["ArticleNo"]);
                    terminalDetail.ArticleName = Convert.ToString(reader["ArticleName"]);
                    terminalDetail.ArticlePrice = Convert.ToDecimal(reader["ArticlePrice"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return terminalDetail;
        }
    }
}
