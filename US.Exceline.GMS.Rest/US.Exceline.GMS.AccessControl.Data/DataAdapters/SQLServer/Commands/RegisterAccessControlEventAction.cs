﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.AccessControl.Data.DataAdapters.SQLServer.Commands
{
    public class RegisterAccessControlEventAction : USDBActionBase<bool>
    {
        private readonly ExceAccessControlEventDC _accessControlEventDetail;

        public RegisterAccessControlEventAction(ExceAccessControlEventDC accessControlEventDetail)
        {
            _accessControlEventDetail = accessControlEventDetail;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSAccessControlRegisterAccessControlEvent";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CardNo", DbType.String, _accessControlEventDetail.CardNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TerminalId", DbType.Int32, _accessControlEventDetail.TerminalId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TerminalType", DbType.String, _accessControlEventDetail.TerminalType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Message", DbType.String, _accessControlEventDetail.Message));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccessControlType", DbType.String, _accessControlEventDetail.AccessControlType));
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Boolean;
                output.ParameterName = "@OutPut";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                return Convert.ToBoolean(output.Value);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
