﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.AccessControl.Data.DataAdapters.SQLServer.Commands
{
    public class CheckGatPurchaseAuthenticationAction : USDBActionBase<ExceAccessControlAuthenticationDC>
    {
        private string _cardNumber;
        private int _branchId;
        private int _terminalId;

        public CheckGatPurchaseAuthenticationAction(string cardNumber, int branchId, int terminalId)
        {
            this._cardNumber = cardNumber;
            this._terminalId = terminalId;
            this._branchId = branchId;
        }

        protected override ExceAccessControlAuthenticationDC Body(System.Data.Common.DbConnection connection)
        {
            ExceAccessControlAuthenticationDC authenticationDC = new ExceAccessControlAuthenticationDC();
            string storedProcedure = "USExceGMSAccessControlCheckAuthenticationForGatPurchase";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@cardNo", DbType.String, _cardNumber));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@terminalId", DbType.Int32, _terminalId));
                DbDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    authenticationDC.Balance = Convert.ToDecimal(reader["Balance"]);
                    authenticationDC.MemberId = Convert.ToInt32(reader["MemberId"]);
                    authenticationDC.IsValidMember = reader["IsValidMember"] == DBNull.Value ? (bool?)null : true;
                    authenticationDC.BranchId = Convert.ToInt32(reader["BranchId"]);
                    authenticationDC.CheckFingerPrint = Convert.ToBoolean(reader["CheckFingerPrint"]);                    
                }
                else
                {
                    authenticationDC.Balance = 0.00M;
                    authenticationDC.IsValidMember = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return authenticationDC;
        }
    }
}
