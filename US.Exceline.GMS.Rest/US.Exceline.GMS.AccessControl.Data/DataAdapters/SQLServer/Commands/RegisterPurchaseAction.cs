﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.AccessControl.Data.DataAdapters.SQLServer.Commands
{
    public class RegisterPurchaseAction : USDBActionBase<bool>
    {
        private readonly ExceAccessControlRegisterPurchaseDC _purchaseDetail;

        public RegisterPurchaseAction(ExceAccessControlRegisterPurchaseDC purchaseDetail)
        {
            _purchaseDetail = purchaseDetail;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSAccessControlRegisterPurchase";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CardNo", DbType.String, _purchaseDetail.CardNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Balance", DbType.Decimal, _purchaseDetail.Balance));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _purchaseDetail.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ItemCode", DbType.String, _purchaseDetail.ItemCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ItemPrice", DbType.Decimal, _purchaseDetail.ItemPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TerminalId", DbType.Int32, _purchaseDetail.TerminalId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsVending", DbType.Boolean, _purchaseDetail.IsVending));
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Boolean;
                output.ParameterName = "@OutPut";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                return Convert.ToBoolean(output.Value);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
