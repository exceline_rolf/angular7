﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Common.RestAPI.JsonObjects
{
    public class UserCultureInfo
    {
        public string userName { get; set; }
        public string culture { get; set; }
    }
}