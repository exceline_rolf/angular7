﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Exceline.GMS.IBooking.Core
{
    [DataContract]
    public class ExceIBookingClassVisit
    {
        [DataMember(Order = 1)]
        public string SystemId { get; set; }
        [DataMember(Order = 2)]
        public string GymID { get; set; }
        [DataMember(Order = 3)]
        public string ClassID { get; set; }
        [DataMember(Order = 4)]
        public List<MemberVisitStatus> Visits { get; set; }

    }



}
