﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.IBooking.Core
{
    public class ContractResignDetails
    {
        public int ID { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public int OrderCount { get; set; }
        public decimal OrderAmount { get; set; }
        public DateTime? LastDueDate { get; set; }
    }
}
