﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.IBooking.Core
{
    public class BookingDetails
    {
        public int ArticleID { get; set; }
        public int ArticleCategoryID { get; set; }
        public string ArticleName { get; set; }
        public int BookingMemberID { get; set; }
        public int ResourceID { get; set; }
        public int GymID { get; set; }
    }
}
