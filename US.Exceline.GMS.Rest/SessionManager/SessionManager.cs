﻿
namespace SessionManager
{
    public class Session
    {
        public static int AddSessionValue(string sessionKey, string sessionValue, string gymCode, int branchId = -1, int salePointId = -1)
        {
            var action = new AddSessionValueAction(sessionKey, sessionValue, branchId, salePointId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public static string GetSession(string sessionKey, string gymCode)
        {
            var action = new GetSessionValueAction(sessionKey);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public static int GetReconciliationSession(string sessionKey, int dailySettlementID, string user, string gymCode)
        {
            var action = new GetReconciliationAction(sessionKey, dailySettlementID, user);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }
    }
}
