﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace SessionManager
{
    public class AddSessionValueAction : USDBActionBase<int>
    {
        private string _sessionKey = string.Empty;
        private string _sessionValue = string.Empty;
        private int _branchId = -1;
        private int _salePointId = -1;
        public AddSessionValueAction(string sessionKey, string sessionValue, int branchId = -1, int salePointId= -1)
        {
            _sessionKey = sessionKey;
            _sessionValue = sessionValue;
            _branchId = branchId;
            _salePointId = salePointId;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSAddSessionValue";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@SessionKey", System.Data.DbType.String, _sessionKey));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SessionValue", System.Data.DbType.String, _sessionValue));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@salePointId", System.Data.DbType.Int32, _salePointId));
                command.ExecuteNonQuery();
                return 1;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }
    }
}
