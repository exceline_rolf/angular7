﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using US.Common.Logging.API;
using US.Common.Service.ConfigurationsFromDB;
using US.Common.Service.DataContracts;
using US.Common.USSAdmin.API;
using US.Common.Web.UI.Core.SystemObjects;
using US.Payment.Core;
using US.Payment.Core.Enums;
using US.Payment.Core.ResultNotifications;


namespace US.Common.Service
{
    [AspNetCompatibilityRequirements
    (RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
   public class CommonService : ICommonService
    {
        #region IPolicyRetriever Members

        public Stream ProvidePolicyFile()
        {
            string result = @"<?xml version=""1.0"" encoding=""utf-8""?>
<access-policy>
    <cross-domain-access>
        <policy>
            <allow-from http-request-headers=""*"">
                <domain uri=""*""/>
            </allow-from>
            <grant-to>
                <resource path=""/"" include-subpaths=""true""/>
            </grant-to>
        </policy>
    </cross-domain-access>
</access-policy>";

            if (WebOperationContext.Current != null)
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";
            return new MemoryStream(Encoding.UTF8.GetBytes(result));
        }


        #endregion

       public bool EditUserBasicInfo(bool isPasswdChange, USPUserDC uspUser, string loggedUser)
       {
           USPUser user = new USPUser();
           user.Id = uspUser.UserID;
           user.DisplayName = uspUser.DisplayName;
           user.email = uspUser.Email;
           user.passowrd = uspUser.Password;
           user.IsLoginFirstTime = uspUser.IsLoginFirstTime;

           OperationResultValue<bool> result = AdminUserSettings.EditUSPUserBasicInfo(isPasswdChange, user, loggedUser);
           if (result.ErrorOccured)
           {
               foreach (USNotificationMessage message in result.Notifications)
               {
                   USLogError.WriteToFile(message.Message, new Exception(), loggedUser);
               }
               return false;
           }
           else
           {
               return result.OperationReturnValue;
           }
       }

       public USPUserDC GetUSPUserInfo(string userName)
       {
           USPUserDC uspUserDC = new USPUserDC();

           US.Common.Logging.API.USLogEvent.WriteToFile("Get User's Profile Process Info Started", userName);
           USImportResult<USPUserProfileInfo> uspUserProfileInfo = AdminUserSettings.GetUserProfileInfo(userName);
           US.Common.Logging.API.USLogEvent.WriteToFile("Get User's Profile Info finished", userName);

           if (uspUserProfileInfo.ErrorOccured)
           {
               foreach (USNotificationMessage message in uspUserProfileInfo.NotificationMessages)
               {
                   if (message.ErrorLevel == ErrorLevels.Error)
                   {
                       USLogError.WriteToFile(message.Message, new Exception(), userName);
                   }
               }
               return new USPUserDC();
           }
           else
           {
               uspUserDC.UserID = uspUserProfileInfo.MethodReturnValue.UserId;
               uspUserDC.UserName = uspUserProfileInfo.MethodReturnValue.UserName;
               uspUserDC.ExternalUserName = uspUserProfileInfo.MethodReturnValue.ExternalUserName;
               uspUserDC.HomePage = uspUserProfileInfo.MethodReturnValue.UserHome;
               uspUserDC.UserID = uspUserProfileInfo.MethodReturnValue.UserId;
               uspUserDC.Branches = uspUserProfileInfo.MethodReturnValue.Branches;
               uspUserDC.Email = uspUserProfileInfo.MethodReturnValue.Email;
               uspUserDC.DisplayName = uspUserProfileInfo.MethodReturnValue.DisplayName;
               uspUserDC.IsLoginFirstTime = uspUserProfileInfo.MethodReturnValue.IsLoginFirstTime;
               uspUserDC.IsActive = uspUserProfileInfo.MethodReturnValue.IsActive;
               uspUserDC.Culture = uspUserProfileInfo.MethodReturnValue.Culture;
               uspUserDC.CulturalDisplayName = uspUserProfileInfo.MethodReturnValue.CulturalDisplayName;
               

               foreach (var item in uspUserProfileInfo.MethodReturnValue.ModuleList)
               {
                   USPModuleDC mod = new USPModuleDC();
                   mod.DeafaultFeature = GetServiceFeature(item.DeafaultFeature);
                   mod.DisplayName = item.DisplayName;
                   mod.ModuleId = item.ID;
                   mod.Features = GetServiceFeatures(item.Features);
                   mod.ID = item.Name;  
                   mod.ModuleHome = item.ModuleHome;
                   mod.Operations = GetServiceOparations(item.Operations);
                   mod.ThumbnailImage = item.ThumbnailImage;
                   mod.ModuleColor = item.ModuleColor;
                   mod.ModulePackage = item.ModulePackage;
                   mod.CulturalDisplayName = item.CulturalDisplayName;
                   uspUserDC.ModuleList.Add(mod);
               }
               uspUserDC.SubOperationList = uspUserProfileInfo.MethodReturnValue.UserSubOperationList;
               uspUserDC.LockTime = GetLockTime();
               return uspUserDC;
           }
       }
       private int GetLockTime()
       {
           try
           {
               string lockTime = USPRunTimeVariables.GetConfigurationByName("LickTime");
               return int.Parse(lockTime);
           }
           catch
           {

           }
           return 100;
       }

       private List<USPFeatureDC> GetServiceFeatures(List<USPFeature> list)
       {
           if (list == null)
           {
               return null;
           }
           List<USPFeatureDC> features = new List<USPFeatureDC>();

           foreach (var item in list)
           {
               features.Add(GetServiceFeature(item));
           }
           return features;
       }

       private USPFeatureDC GetServiceFeature(USPFeature uSPFeature)
       {
           if (uSPFeature == null)
           {
               return null;
           }
           USPFeatureDC feature = new USPFeatureDC();
           if (uSPFeature != null)
           {
               feature.DisplayName = uSPFeature.DisplayName;
               feature.HomeUIControl = uSPFeature.HomeUIControl;
               feature.ID = uSPFeature.Name;
               feature.FeatureId = uSPFeature.ID;
               feature.Operations = GetServiceOparations(uSPFeature.Operations);
               feature.Priority = uSPFeature.Priority;
               feature.ThumbnailImage = uSPFeature.ThumbnailImage;
               feature.FeatureColor = uSPFeature.FeaturColor;
               feature.CulturalDisplayName = uSPFeature.CulturalDisplayName;
           }
           return feature;
       }

       private List<OperationDC> GetServiceOparations(List<USPOperation> list)
       {
           if (list == null)
           {
               return null;
           }
           List<OperationDC> oparations = new List<OperationDC>();
           foreach (var item in list)
           {
               oparations.Add(GetServiceOparation(item));
           }

           return oparations;
       }

       private OperationDC GetServiceOparation(USPOperation item)
       {
           if (item == null)
           {
               return null;
           }
           OperationDC operation = new OperationDC();
           operation.DisplayName = item.DisplayName;
           operation.ID = item.Name;
           operation.OperationId = item.ID;
           operation.OperationHome = item.OperationHome;
           operation.OperationNameSpace = item.OperationNameSpace;
           operation.ThumbnailImage = item.ThumbnailImage;
           operation.OperationColor = item.OperationColor;
           operation.Mode = item.Mode;
           operation.RoleId = item.RoleId;
           operation.IsAddedToWorkStation = item.IsAddedToWorkStation;
           operation.ExtendedInfo = item.OperationExInfo;
           operation.SubOperationList = GetSubOperationList(item.SubOperationList);
           operation.OperationPackage = item.OperationPackage;
           operation.Group = item.Group;
           operation.CulturalDisplayName = item.CulturalDisplayName;
           return operation;
       }
       public List<SubOperationDC> GetSubOperationList(List<USPSubOperation> subOpList)
       {
           List<SubOperationDC> subOperationList = new List<SubOperationDC>();
           foreach (USPSubOperation uspso in subOpList)
           {
               SubOperationDC sodc = new SubOperationDC();
               sodc.Name = uspso.Name;
               sodc.DisplayName = uspso.DisplayName;
               sodc.ExtendedInfo = uspso.ExtendedInfo;
               sodc.ID = uspso.Id.ToString();
               sodc.OperationColor = uspso.OperationColor;
               sodc.OperationId = uspso.OperationID;
               sodc.ThumbnailImage = uspso.ThumbnailImage;
               sodc.SubOperationHome = uspso.SubOperationHome;
               sodc.SubOperationPackage = uspso.SubOperationPackage;
               subOperationList.Add(sodc);
           }
           return subOperationList;
       }
      


       public LoginInfoDC GetLoginInfo()
       {
           LoginInfoDC loginInfo = GetLoginInfoData();
           return loginInfo;
       }

       private LoginInfoDC GetLoginInfoData()
       {
           LoginInfoDC loginInfo = new LoginInfoDC();
           try
           {
               var readRegKey = ReadRegistryKey.Values();

               if (readRegKey == null || readRegKey[0] != "False")
               {
                   loginInfo.Mode = ConfigurationManager.AppSettings["LoginMode"];
                   loginInfo.LoginModulePackage = ConfigurationManager.AppSettings["LoginModulePackage"];
                   loginInfo.AssemblyName = ConfigurationManager.AppSettings["LoginViewAssembly"];
                   loginInfo.LoginView = ConfigurationManager.AppSettings["LoginView"];
               }
               else
               {
                   var config = new GetConfiguration { DBConnectionString = readRegKey[1] };

                   loginInfo.Mode = config.Key(15, "LoginMode");
                   loginInfo.LoginModulePackage = config.Key(15, "LoginModulePackage");
                   loginInfo.AssemblyName = config.Key(15, "LoginViewAssembly");
                   loginInfo.LoginView = config.Key(15, "LoginView");
               }
           }
           catch
           {
               loginInfo.Mode = "Windows";
               //Log here...
           }
           return loginInfo;
       }

       public USPUserDC getUser()
       {
           return new USPUserDC();
       }


       public bool SaveUserCulture(string culture, string userName)
       {
           OperationResultValue<bool> result = AdminUserSettings.SaveUserCulture(culture, userName);
           if (result.ErrorOccured)
           {
               foreach (USNotificationMessage message in result.Notifications)
               {
                   USLogError.WriteToFile(message.Message, new Exception(), userName);
               }
               return false;
           }
           else
           {
               return result.OperationReturnValue;
           }
       }
    }
}
