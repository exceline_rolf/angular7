﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.Common.Service.DataContracts
{
    [DataContract]
    public class OperationDC
    {
        [DataMember]
        public string OperationHome { get; set; }
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public int OperationId { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public string Group { get; set; }
        [DataMember]
        public string ThumbnailImage { get; set; }
        [DataMember]
        public string OperationColor { get; set; }
        [DataMember]
        public string Mode { get; set; }
        [DataMember]
        public int IsAddedToWorkStation { get; set; }
        [DataMember]
        public int RoleId { get; set; }
        [DataMember]
        public string OperationNameSpace { get; set; }
        [DataMember]
        public string OperationPath { get; set; }
        [DataMember]
        public OperationExtendedInfo ExtendedInfo { get; set; }
        [DataMember]
        public List<SubOperationDC> SubOperationList { get; set; }
        [DataMember]
        public string OperationPackage { get; set; }
        [DataMember]
        public string CulturalDisplayName { get; set; }
    }
}
