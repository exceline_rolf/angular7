﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Common.Service.DataContracts
{
    [DataContract]
    public class USPModuleDC
    {
        private List<USPFeatureDC> _features = new List<USPFeatureDC>();
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public int ModuleId { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public USPFeatureDC DeafaultFeature { get; set; }
        [DataMember]
        public string ThumbnailImage { get; set; }
        [DataMember]
        public string ModuleHome { get; set; }
        [DataMember]
        public string ModulePackage { get; set; }
        [DataMember]
        public string ModuleColor { get; set; }
        [DataMember]
        public List<USPFeatureDC> Features
        {
            get { return _features; }
            set { _features = value; }
        }
        private List<OperationDC> _operations = new List<OperationDC>();
        [DataMember]
        public List<OperationDC> Operations
        {
            get { return _operations; }
            set { _operations = value; }
        }
        [DataMember]
        public string CulturalDisplayName { get; set; }
    }
}
