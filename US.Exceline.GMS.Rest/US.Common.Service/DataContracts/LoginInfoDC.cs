﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Common.Service.DataContracts
{
    [DataContract]
    public class LoginInfoDC
    {
        private string _mode = string.Empty;
        [DataMember]
        public string Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }
        private string _loginModulePackage = string.Empty;
        [DataMember]
        public string LoginModulePackage
        {
            get { return _loginModulePackage; }
            set { _loginModulePackage = value; }
        }
        private string _assemblyName = string.Empty;
        [DataMember]
        public string AssemblyName
        {
            get { return _assemblyName; }
            set { _assemblyName = value; }
        }
        private string _loginView = string.Empty;
        [DataMember]
        public string LoginView
        {
            get { return _loginView; }
            set { _loginView = value; }
        }

    }
}
