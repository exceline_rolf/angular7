﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.Common.Service.DataContracts
{
    [DataContract]
    public class SubOperationDC
    {
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public int OperationId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public string ThumbnailImage { get; set; }
        [DataMember]
        public string OperationColor { get; set; }
        [DataMember]
        public OperationExtendedInfo ExtendedInfo { get; set; }
        [DataMember]
        public string SubOperationHome { set; get; }
        [DataMember]
        public string SubOperationPackage { set; get; }
        [DataMember]
        public string CulturalDisplayName { get; set; }
    }
}
