﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.Data
{
    public class ATGFacade
    {
        private static ISQLServerEconomyDataAdapter GetDataAdapter()
        {
            return new SQLServerEconomyDataAdapter();
        }

        public static List<Creditor> GetDDCreditors(int DDSt, int branchID, DateTime startDate, DateTime endDate , string gymcode)
        {
            return GetDataAdapter().GetDDCreditors(DDSt, branchID, startDate, endDate, gymcode);
        }

        public static ApplicationSetting GetApplicationSetting(string gymCode)
        {
            return GetDataAdapter().GetApplicationSetting(gymCode);
        }

        public static string GetATGTransactionID(string gymCode)
        {
            return GetDataAdapter().GetATGTransactionID(gymCode);
        }

        public static DirectDeductInfo GetDirectDeduct(DateTime startDate, DateTime endDate, string creditorEntityID, int status, string gymCode, string sendingNo)
        {
            return GetDataAdapter().GetDirectDeduct(startDate, endDate, creditorEntityID, status, gymCode, sendingNo);
        }

        public static bool UpdateStatus(List<DDHistory> historyEnt, string gymCode)
        {
            return GetDataAdapter().UpdateStatus(historyEnt, gymCode);
        }

        public static void SetCancellationStatus(string sendingNo, string gymCode)
        {
            GetDataAdapter().SetCancellationStatus(sendingNo, gymCode);
        }
    }
}
