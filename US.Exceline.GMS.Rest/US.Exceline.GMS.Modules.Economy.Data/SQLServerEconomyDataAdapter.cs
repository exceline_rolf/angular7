﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Ccx;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;
using US.Exceline.GMS.Modules.Economy.Data.Commands;
using US.Exceline.GMS.Modules.Economy.Data.Commands.ATG;
using US.Exceline.GMS.Modules.Economy.Data.Commands.OCR;
using US.GMS.Core.ResultNotifications;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data
{
    public class SQLServerEconomyDataAdapter : ISQLServerEconomyDataAdapter
    {
        public List<USPErrorPayment> GetErrorPaymentDetails(string errorPaymentType, DateTime fromDate, DateTime toDate, string gymCode, int branchid,string batchId=null)
        {
            var action = new GetErrorPaymentDetailsAction(errorPaymentType, fromDate, toDate, branchid,batchId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public List<USPARItem> AdvanceInvoiceSearch(Core.SystemObjects.InvoiceSearchCriteria criteria, Core.SystemObjects.SearchModes searchMode, object constValue, string gymCode)
        {
            var seachAction = new SearchInvoicesAction(criteria, searchMode, constValue);
            var arItemList = seachAction.Execute(EnumDatabase.Exceline, gymCode);
            return arItemList;
        }

        public List<USPARItem> GeneralInvoiceSearch(string searchValue, string secondValue, int suggest, int invoiceType, string fieldType, Core.SystemObjects.SearchModes searchMode, object constValue, string gymCode, int paymentID)
        {
            var action = new GeneralInvoiceSearchAction(searchValue, secondValue, suggest, fieldType, invoiceType, searchMode, constValue, paymentID);
            var arItemList = action.Execute(EnumDatabase.Exceline, gymCode);
            return arItemList;
        }

        public List<USPApportionmentTransaction> GetInitialApportionmentData(string KID, string creditorNo, decimal amount, int ARNo, int caseNo, int paymentID, string paymentType, string gymCode)
        {
            var action = new GetInitialApportionmentDataAction(KID, creditorNo, amount, ARNo, caseNo, paymentID, paymentType, gymCode);
            var initialLIst = action.Execute(EnumDatabase.Exceline, gymCode);
            return initialLIst;
        }

        public Core.SystemObjects.Enums.ReverseConditions ReverseApportionments(int paymentId, int arItem, string actionStatus, string user, string gymCode)
        {
            var reverseAction = new ReverseApportionmentMainAction(paymentId, arItem, actionStatus, user);
            return reverseAction.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddNotApportionmentTransactions(int ArNo, string KID, int errorpaymentID, string creditoNo, decimal mainAmount, int itemTypeID, List<USPApportionmentTransaction> transactionList, string errorType, DateTime voucherDate, string user, string gymCode)
        {
            int result = -1;
            foreach (var item in transactionList)
            {
                var addApportionments = new AddNotApportionmentTransationAndDataAction(ArNo, KID, errorpaymentID, creditoNo, mainAmount, itemTypeID, item, user);
                result = addApportionments.Execute(EnumDatabase.Exceline, gymCode);
            }
            return result;
        }

        public bool SendReminders(string GymCode, int branchID)
        {
            var action = new SendRemindersAction(branchID);
            return action.Execute(EnumDatabase.Exceline, GymCode);
        }

        public List<Creditor> GetDDCreditors(int DDSt, int branchID, DateTime startDate, DateTime endDate, string gymcode)
        {
            var action = new GetDDCreditorsAction(DDSt, branchID, startDate, endDate);
            return action.Execute(EnumDatabase.Exceline, gymcode);
        }

        public ApplicationSetting GetApplicationSetting(string gymCode)
        {
            var action = new GetApplicationSetting();
            return action.Execute(EnumDatabase.Exceline, gymCode); 
        }

        public string GetATGTransactionID(string gymCode)
        {
            var action = new GetTransactionIDAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public DirectDeductInfo GetDirectDeduct(DateTime startDate, DateTime endDate, string creditorEntityID, int status, string gymCode, string sendingNo)
        {
            var action = new ReadDDAction(startDate, endDate, creditorEntityID, status, sendingNo);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool UpdateStatus(List<DDHistory> historyEnt, string gymCode)
        {
            foreach (DDHistory item in historyEnt)
            {
                try
                {
                    var action = new UpdateStatusAction(item);
                    action.Execute(EnumDatabase.Exceline, gymCode);
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return true;    
        }

        public int AddFileValidationSatatesRecord(string creditorNo, int fileID, int recordCounte, decimal sum, string recordTypeString, string message1, string message2, string applicaton, string gymCode)
        {
            var action = new AddFileValidationStatusRecordAction(creditorNo, fileID, recordCounte, sum, recordTypeString, message1, message2, applicaton);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddFileStatus(int fileId, DateTime time, string status, string msg1, string msg2, string gymCode)
        {
            var action = new AddFileStatusAction(fileId, time, status, msg1, msg2);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public Creditor GetCreditorInkassoIDbyAccountNumber(string accountNumber, string checkType, string gymCode)
        {
            var action = new ReadCreditorByAccountNoAction(accountNumber, checkType);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public SortedList GetRecordTypes(string gymCode)
        {
            var action = new GetRecordTypesListAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int GetFileIDbyFileName(string fileNameWithoutPath, string gymCode)
        {
            var action = new GetFileIdAction(fileNameWithoutPath);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public Creditor GetCreditorEntityRoleIDbyAccountNumber(string creditorAccountNumber, string checkType, string gymCode)
        {
            var action = new GetCreditorEntRoleIDandInkassoByAccountNo(creditorAccountNumber, checkType);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public void UpdateWarningType(string kid, string credAccountNo, int status, string gymCode)
        {
            var action = new UpdateWarningTypeAction(kid, credAccountNo, status);
            action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public string UpdateDirectDeductCancellationStatus(string cancellationKID, string credAccountNo, string status, string gymCode)
        {
            var action = new UpdateDDCancellationStatusAction(cancellationKID, credAccountNo, status);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public void UpdateOCRAllRecords(string fileName, string kid, string status, string creditorAccountNo, string gymCode)
        {
            var action = new AddOcrAllRecordAction(fileName, kid, status, creditorAccountNo);
            action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveFileLog(string fileName, string folderPath, string user, string fileType, string gymCode)
        {
            var action = new SaveFileLogAction(fileName, folderPath, user, fileType);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool AddPaymentStatus(List<ImportStatus> payments, string gymCode)
        {
            var action = new AddPaymentStatusAction(payments);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ImportOCRPayments(int fileID, string gymCode)
        {
            var action = new ImportOCRAction(fileID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool AddCancellationStatus(List<CancellationStatus> statusList, string gymCode)
        {
            var action = new AddCancellationStatusAction(statusList);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ImportCancellations(string gymCode)
        {
            var action = new ImportCancellationsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<InvoicingOrder> GetOrdersforInvoice(int branchID, DateTime? startDueDate, DateTime? endDueDate, string orderType, string gymCode)
        {
            var action = new GetOrdersforInvoicingAction(branchID, startDueDate, endDueDate, orderType);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public OperationResult<string> GenerateInvoiceswithOrders(List<InvoicingOrder> orderIds, string invoiceKey, string orderType, string user, string gymCode, int branchID)
        {
            var action = new GenerateInvoiceAction(orderIds, invoiceKey, orderType, user, gymCode, branchID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public EstimatedInvoiceDetails GetEstimatedInvoiceDetails(int branchID, DateTime? startDueDate, DateTime? endDueDate, string gymCode)
        {
            var action = new GetEstimatedInvoiceAmountAction(branchID, startDueDate, endDueDate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public EstimatedInvoiceDetails GetInvoicedAmounts(int branchID, DateTime? startDueDate, DateTime? endDueDate, string gymCode)
        {
            var action = new GetInvoicedAmountsAction(branchID, startDueDate, endDueDate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public void AddProcessLog(List<string> messages, string user, int fileId, string gymCode)
        {
            foreach (string msg in messages)
            {
                var action = new AddImportProcessLogAction(msg, user, fileId);
                action.Execute(EnumDatabase.Exceline, gymCode);
            }
        }

        public void AddImportHistory(OCRImportSummary oCRImportSummary, string user, string gymCode)
        {
            var action = new AddOCRImportHistoryAction(oCRImportSummary, user);
           action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<OCRImportSummary> GetOCRImportHistory(DateTime? startDate, DateTime? endDate, string gymCode)
        {
            var action = new GetOCRImportHistoryAction(startDate, endDate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddCreditorOrderLine(CreditorOrderLine OrderLine, string user, DbTransaction transaction)
        {
            var action = new AddCreditorInvoicingorderLineAction(OrderLine);
            return action.RunOnTransaction(transaction);
        }


        public OperationResult<List<IUSPClaim>> SelectedOrderLineInvoiceGenerator(List<int> creditorOrdeLineIdList, string invoiceKey, string orderType, string user, DbTransaction transaction, int installmentID, int creditorNo, string gymCode)
        {
            var action = new AddInvoiceWithOrderlinesAction(creditorOrdeLineIdList, invoiceKey, orderType, user, transaction, installmentID, creditorNo, gymCode);
            return action.RunOnTransaction(transaction);
        }

        public PaymentProcessResult RegisterTransaction(List<IUSPTransaction> transactionList, DbTransaction transaction)
        {
            var action = new RegisterTransactionAction(transactionList);
            return action.RunOnTransaction(transaction);
        }


        public List<EconomyFileLogData> GetEconomyFileLogData(DateTime fromDate, DateTime toDate, string source, string filterBy, string gymCode)
        {
            var action = new GetEconomyFileLogSummaryAction(fromDate,toDate,source,filterBy);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<EconomyFileLogData> GetEconomyFileLogDetail(EconomyLogFileHelper economyLogFileHelper)
        {
            var action = new GetEconomyFileLogDetailsAction(economyLogFileHelper);
            return action.Execute(EnumDatabase.Exceline, economyLogFileHelper.GymCode);
        }

        public int GetCcxEnabledStatus(int branchId, string gymCode)
        {
            var action = new GetCcxEnabledStatusAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<InvoiceDC> GetInvoiceList(string category, DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            try
            {
                var action = new GetInvoiceListAction(category, fromDate, toDate, branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdatePDFFileParth(int id, string fileParth, int branchId, string gymCode, string user)
        {
            try
            {
                var action = new UpdatePDFFileParthAction(id, fileParth, branchId, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateHistoryPDFPrintProcess(int id, int invoiceCount, int FailCount, int branchId, string user, string gymCode, string filePath)
        {
            try
            {
                var action = new UpdateHistoryPDFPrintProcessAction(id, invoiceCount, FailCount, branchId, user, filePath);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<HistoryPDFPrint> GetHistoryPDFPrintDetail(DateTime fromDate, DateTime toDate, string gymCode)
        {
            try
            {
                var action = new GetHistoryPDFPrintDetailAction(fromDate, toDate);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool RemovePayment(int paymentID, int arItemNo,  string user, string gymCode, string type)
        {
            try
            {
                RemovePaymentAction action = new RemovePaymentAction(paymentID, arItemNo, user, type);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int MovePayment(int paymentID, int aritemno, string type, string user, string gymCode)
        {
            try
            {
               MovePaymentAction action = new MovePaymentAction(paymentID, aritemno, type, user);
               return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void SetCancellationStatus(string sendingNo, string gymCode)
        {
            try
            {
                SetCancellationStatusAction action = new SetCancellationStatusAction(sendingNo);
                action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PaymentImportLogDC> GetPaymentImportProcessLog(DateTime fromDate, DateTime toDate, string gymCode)
        {
            try
            {
                var action = new GetPaymentImportProcessLogAction(fromDate, toDate);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ClassDetail> GetClassSalary(List<int> branchIdList, List<int> employeeIdList, List<int> categoryIdList, DateTime fromDate, DateTime toDate, string gymCode)
        {
            try
            {
                var action = new GetClassSalaryAction(branchIdList, employeeIdList, categoryIdList, fromDate, toDate);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ClassFileDetail> GetClassFileDetails(string gymCode)
        {
            try
            {
                var action = new GetClassFileDetailsAction();
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveClassFileDetail(string fileName, string filePath, string user, string gymCode)
        {
            try
            {
                var action = new SaveClassFileDetailAction(fileName, filePath, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateBulkPDFPrintDetails(int id, string docType, string dataIdList, string pdfFilePath, int noOfDoc, int failCount, int branchId, string user, string gymCode)
        {
            try
            {
                var action = new UpdateBulkPDFPrintDetailsAction(id, docType, dataIdList, pdfFilePath, noOfDoc, failCount, branchId, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BulkPDFPrint> GetBulkPDFPrintDetailList(string type, DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            try
            {
                var action = new GetBulkPDFPrintDetailListAction(type, fromDate, toDate, branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int UpdateCcxInvoice(List<Invoice> invoices)
        {
            try
            {
                var action = new UpdateCcxInvoiceAction(invoices);
                return action.Execute(EnumDatabase.WorkStation);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Invoice> GetCcxInvoiceDetail(int batchId)
        {
            try
            {
                var action = new GetCcxInvoiceDetailAction(batchId);
                return action.Execute(EnumDatabase.WorkStation);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AccountNamesAndNumbers> GetAccountsForKasseFil()
        {
            var action = new GetAccounts();
            return action.Execute(EnumDatabase.Exceline);
        }


        public List<KeyData> GetKeyFromDB(String gymCode)
        {
            try
            {
                var action = new GetKeyFromDBAction();
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch ( Exception ex)
            {
                throw ex;
            }
        }
        
       public int AddPreviousSignatureForNextTransaction(int SalePointID, int branchID, string signature, String gymCode)
        {
            try
            {
                var action = new AddPreviousSignatureForNextTransactionAction( SalePointID,  branchID,  signature);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int FinalizeTransaction(int SalePointID, int branchID, string signature, String transDate, String transTime, int nr, decimal transAmntIn, decimal transAmntEx, String gymCode)
        {
            try
            {
                var action = new FinalizeTransactionAction(SalePointID, branchID, signature, transDate, transTime, nr, transAmntIn, transAmntEx);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       public String GetLastTransaction(int salePointID, int branchID, String gymCode)
        {
            try
            {
                var action = new GetLastTransactionAction(salePointID, branchID);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DataForLedger> ViewSkatteEtatenLedger(DateTime FromDate, DateTime ToDate, int branchID, String gymCode)
        {
            try
            {
                var action = new ViewSkatteEtatenLedgerAction(FromDate,ToDate,branchID);
                return action.Execute(EnumDatabase.Exceline, gymCode);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<KeyData> GetSalePointsByBranch(int branchId, String gymCode)
        {
            try
            {
                GetSalePointsByBranchAction action = new GetSalePointsByBranchAction(branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public InvoiceInfo GetInformationForInvoicePrint(String invoiceRef, int branchId, String gymCode)
        {
            try
            {
                GetInformationForInvoicePrintAction action = new GetInformationForInvoicePrintAction(invoiceRef, branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public double GetVatRateFromInvoiceNo(int invoiceNo, String gymCode)
        {
            try
            {
                GetVatRateFromInvoiceNoAction action = new GetVatRateFromInvoiceNoAction(invoiceNo);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int InsertInvoicePrintRecord( int branchId, String invoiceRef,String xmlData, String hashValue, String gymCode)
        {
            try
            {
                InsertInvoicePrintRecordAction action = new InsertInvoicePrintRecordAction(branchId, invoiceRef, xmlData,hashValue);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public InvoiceXMLData GetInvoicePrintRecord(String invoiceRef, int branchId, String gymCode)
        {
            try
            {
                GetInvoicePrintRecordAction action = new GetInvoicePrintRecordAction(branchId,invoiceRef);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetGymCompanyId(String gymCode)
        {
            try
            {
                GetGymCompanyIdAction action = new GetGymCompanyIdAction();
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertInvoicePrintDiscrepancyInstance(DateTime timeStamp, String invoiceNo, String originalXml, String newXml, String gymCode)
        {
            try
            {
                InsertInvoicePrintDiscrepancyInstanceAction action = new InsertInvoicePrintDiscrepancyInstanceAction(timeStamp,  invoiceNo,  originalXml,  newXml);
                action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ExcelineSponsorMembersForPrint> ExcelineGetSponsoredMembersInvoicePrint(String sponsorARItemNo, String gymCode)
        {
            try
            {

                ExcelineGetSponsoredMembersInvoicePrint action = new ExcelineGetSponsoredMembersInvoicePrint(sponsorARItemNo, gymCode);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ExcelineSponsorInfoForPrint ExcelineGetSponsorInfoForInvoicePrint(String sponsorARItemNo, String gymCode)
        {
            try
            {
                ExcelineGetSponsorInfoForInvoicePrint action = new ExcelineGetSponsorInfoForInvoicePrint(sponsorARItemNo, gymCode);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
