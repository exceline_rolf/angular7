﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    class DistributePaymentAction
    {
        private IUSPTransaction _transactionInfo;
        public DistributePaymentAction(IUSPTransaction paymentInfo)
        {
            _transactionInfo = paymentInfo;
        }
        public PaymentProcessStepResult RunOnTransaction(DbTransaction transaction, TransactionProfile profile)
        {
            PaymentProcessStepResult result = new PaymentProcessStepResult();
            result.ResultStatus = true;
            DbDataReader dataReader = null;
            result.StepName = "Distributing Transaction";
            try
            {
                DbConnection connection = transaction.Connection;
                DbCommand command = connection.CreateCommand();
                command.Transaction = transaction;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = profile.DistributeTransactionSp;
                command.Parameters.Add(DataAcessUtils.CreateParam("@TranactionDate", DbType.DateTime, _transactionInfo.TransactionDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CaseNo", DbType.Int32, _transactionInfo.CaseNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SubCaseNo", DbType.Int32, _transactionInfo.SubCaseNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ARNo", DbType.Int32, _transactionInfo.ARNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ARItemNo", DbType.Int32, _transactionInfo.ARItemNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@TransType", DbType.String, _transactionInfo.TransType));
                command.Parameters.Add(DataAcessUtils.CreateParam("@TransText", DbType.String, _transactionInfo.TransText));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Delayed", DbType.Int32, _transactionInfo.Delayed));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VoucherDate", DbType.DateTime, _transactionInfo.VoucherDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@DueDate", DbType.DateTime, _transactionInfo.DueDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ReceivedDate", DbType.DateTime, _transactionInfo.ReceivedDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@RegDate", DbType.DateTime, _transactionInfo.RegDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Amount", DbType.Decimal, _transactionInfo.Amount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Source", DbType.String, _transactionInfo.Source));
                command.Parameters.Add(DataAcessUtils.CreateParam("@FileName", DbType.String, _transactionInfo.FileName));
                command.Parameters.Add(DataAcessUtils.CreateParam("@DebtorAccountNo", DbType.String, _transactionInfo.DebtorAccountNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@KID", DbType.String, _transactionInfo.KID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CID", DbType.String, _transactionInfo.CID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceNo", DbType.String, _transactionInfo.InvoiceNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Reference1", DbType.String, _transactionInfo.Reference1));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ApportionStatus", DbType.Int32, _transactionInfo.ApportionStatus));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VoucherDetailId", DbType.Int32, _transactionInfo.VoucherDetailId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PID", DbType.String, _transactionInfo.PID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CreditorAccountNo", DbType.String, _transactionInfo.CreditorAccountNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@NotifiedDate", DbType.DateTime, _transactionInfo.NotifiedDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Reference2", DbType.String, _transactionInfo.Reference2));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ContractKid", DbType.String, _transactionInfo.ContractKid));
                //command.Parameters.Add(DataAcessUtils.CreateParam("@CancelDate", DbType.DateTime, _transactionInfo.CancelDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ExternalTransactionNo", DbType.String, _transactionInfo.ExternalTransactionNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VATLiability", DbType.Int32, _transactionInfo.VATLiability));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsPrinted", DbType.Int32, _transactionInfo.IsPrinted));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PrintFilePath", DbType.String, _transactionInfo.PrintFilePath));
                command.Parameters.Add(DataAcessUtils.CreateParam("@TranactionProfileID", DbType.Int32, _transactionInfo.TransactionProfile.Id));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _transactionInfo.User));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PaymentId", DbType.Int32, _transactionInfo.PaymentId));

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                result.ResultStatus = false;
                result.MessageList.Add("Distribute Transaction Failed Error - " + ex.Message);
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return result;
        }
    }
}
