﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class ViewSkatteEtatenLedgerAction : USDBActionBase<List<DataForLedger>>
    {
        private DateTime _FromDate = DateTime.Now;
        private DateTime _ToDate = DateTime.Now;
        private int _branchID = -1 ;
        

        public ViewSkatteEtatenLedgerAction(DateTime FromDate, DateTime ToDate, int branchID)
        {
            _FromDate = FromDate;
            _ToDate = ToDate;
            _branchID = branchID;

        }


        protected override List<DataForLedger> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ViewSkatteEtatenLedger";
            DbDataReader reader = null;
            List<DataForLedger> economyFileLogData = new List<DataForLedger>();



            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@FromDate", System.Data.DbType.DateTime, _FromDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ToDate", System.Data.DbType.DateTime, _ToDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchID", System.Data.DbType.Int32, _branchID));
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    DataForLedger fileLogData = new DataForLedger
                    {

                        signature = Convert.ToString(reader["signature"]),
                        transDate = Convert.ToString(reader["transDate"]),
                        transTime = Convert.ToString(reader["transTime"]),
                        nr = Convert.ToInt32(reader["nr"]),
                        transAmntIn = Convert.ToDouble(reader["transAmntIn"]),
                        transAmntEx = Convert.ToDouble(reader["transAmntEx"]),
                        branchId = Convert.ToInt16(reader["branchId"]),
                        SalePointID = Convert.ToInt16(reader["SalePointID"])

                    };
                    economyFileLogData.Add(fileLogData);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }


            finally
            {

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            return economyFileLogData;
        }
    }
}
