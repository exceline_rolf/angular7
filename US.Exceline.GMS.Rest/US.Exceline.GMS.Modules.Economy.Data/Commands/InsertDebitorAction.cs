﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class InsertDebitorAction : USDBActionBase<Debitor>
    {
        private Debitor _debitor = new Debitor();
        private CAddress _address = new CAddress();

        public InsertDebitorAction(Debitor debitor, CAddress addr)
        {
            _debitor = debitor;
            _address = addr;      
        }

        protected override Debitor Body(System.Data.Common.DbConnection connection)
        {
            return null;
        }

        public Debitor RunOnTransaction(DbTransaction transaction)
        {
            try
            {
                 _debitor.AddrNo = _address.AddrNo;
                if(_debitor.EntID <= 0)
                {
                    InsertEnt debitorAction = new InsertEnt(_debitor);
                    _debitor.EntID = debitorAction.RunOnTransaction(transaction);
                }

                if (_debitor.EntID == -1)
                {
                    return null;
                }
                else
                {
                    InsertEntRole roleaction = new InsertEntRole(_debitor.EntID, "DEB", _debitor.Reference.ToString());
                    _debitor.EntRoleID = roleaction.RunOnTransaction(transaction);

                    if (_debitor.EntRoleID == -1)
                    {
                        return null;
                    }
                    else
                    {
                        AddAddress addressAction = new AddAddress(_address, _debitor.EntRoleID);
                        addressAction.RunOnTransaction(transaction);
                    }
                }
                return _debitor;
            }
            catch(Exception ex)
            {
                throw new Exception("Failed to Save Debtor :"+ ex.Message, ex);
            }
        }
    }
}
