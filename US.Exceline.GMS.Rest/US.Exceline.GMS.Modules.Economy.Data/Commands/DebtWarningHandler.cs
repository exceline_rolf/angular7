﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    internal class DebtWarningHandler : ClaimHandler
    {
        /// <summary>
        /// Update debt warning transer date to current date. Transfer date will be current date
        /// </summary>
        /// <param name="userName"></param>
        /// <returns>True if succeeded</returns>
        public DebtWarningHandler(string userName)
        {
            //ClaimPersistingManager = new ClaimPersititngManager(EnumDatabase.USP, userName);
        }

        //Changed By LKA
        public int UpdateDebtWarningRecord(IUSPClaim claim)
        {
        //    string transferDate = string.Empty;
        //        // In current USP development transfer date is set to CURRENT_DATE in the SP. Value of transfer date is not send to the SP
        //    // This value is set to Empty because follwoning method should have that parameter
        //    // 20131218 New Change Request AAB Update DW Amount (AAB) 
        //    int updatedArItemNo =
        //        ClaimPersistingManager.UpdateDebtWarningTrasnsferDate(int.Parse(claim.Creditor.CreditorInkassoID),
        //                                                              claim.Debtor.DebtorcustumerID, claim.InvoiceNumber,
        //                                                              transferDate, claim.Amount);

        //    if (updatedArItemNo < 0)
        //    {
        //        throw new USPOperationFailedException("Failed to update debt warning date for record with InkassoID["
        //                                              + claim.Creditor.CreditorInkassoID + "] , Customer ID[" +
        //                                              claim.Debtor.DebtorcustumerID + "] and Invoice Number[" +
        //                                              claim.InvoiceNumber + "]", null);
        //    }
        //    // Update ARItemDetails for the Debtwarning

        //    var directDeduct = new DirectDeduct
        //        {
        //            AccountNo = claim.AccountNumber,
        //            Balance = claim.Balance,
        //            BranchNo = claim.BranchNumber,
        //            CollectingStatus = (CollectingStatus) (int) claim.CollectingStatus,
        //            CollectingText = claim.InkassoTest,
        //            ContractExpire = claim.ContractExpire,
        //            ContractKID = claim.ContractKID,
        //            ContractNo = claim.ContractNumber,
        //            DueBalance = claim.DueBalance,
        //            InstallmentKID = claim.KID,
        //            InstallmentNo = claim.InstallmentNumber,
        //            LastContract = claim.LastContract,
        //            LastReminder = claim.LastReminder,
        //            StatusMendatory = (DirectDeductStatus) (int) claim.Status,
        //            TrancemissionNo = claim.TransmissionNumberBBS,
        //            InvoiceAmount = claim.InvoiceCharge.ToString(),
        //            nameInContract = claim.NameInContract,
        //            lastVisit = claim.LastVisit,
        //            printDate = claim.PrintDate,
        //            originalDue = claim.OriginalDueDate,
        //            reminderfee = claim.ReminderFee.ToString(),
        //            InvoiceRef = claim.InvoiceRef,
        //            Group = claim.Group
        //        };
        //    ClaimPersistingManager.UpdateARItemDetailsForDW(directDeduct, updatedArItemNo);

        //    return updatedArItemNo;
            return -1;
        }



        /// <summary>
        /// Debt Warning sepecific logic to add Debt Warning claim
        /// </summary>
        /// <param name="claim">Claim to be added</param>
        /// <param name="arItemNo">ARItem No of saved ARItem record</param>
        /// <returns>USImportResult object containing error,warn,infos</returns>
        internal override OperationResult SaveClaim(DbTransaction transaction, IUSPClaim claim, int arItemNo)
        {
            //// Create object that send as result of method invocation
            //var result = new USImportResult {Tag1 = claim.Tag1, Tag2 = claim.Tag2};
            //// Assign tags of claim to returning result

            //try
            //{
            //    int updatedArItemNo = UpdateDebtWarningRecord(claim);
            //    result.TagSavedRecordID = updatedArItemNo;
            //}
            //catch (USPOperationFailedException ex)
            //{
            //    // Add error messages to sending result
            //    var message = new USNotificationMessage(ErrorLevels.Error, ex.Message);
            //    result.ErrorOccured = true;
            //    result.AddUSNotificationMessage(message);
            //}
            //catch (Exception ex)
            //{
            //    // Add error messages to sending result
            //    var message = new USNotificationMessage(ErrorLevels.Error,
            //                                            "Failed to update debt warning date for record with InkassoID["
            //                                            + claim.Creditor.CreditorInkassoID + "] , Customer ID[" +
            //                                            claim.Debtor.DebtorcustumerID + "] and Invoice Number[" +
            //                                            claim.InvoiceNumber + "]. Because " + ex.Message);
            //    result.ErrorOccured = true;
            //    result.AddUSNotificationMessage(message);
            //}
            //return result;

            return new OperationResult();
        }
    }
}
