﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetGymCompanyIdAction : USDBActionBase<int>
    {
        private int gymCompanyId  = 0;

        public GetGymCompanyIdAction()
        {

        }

        protected override int Body(DbConnection connection)
        {
            string spName = "Exceline.GetGymCompanyId";
            DbDataReader reader = null;



            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    
                    {

                        gymCompanyId = Convert.ToInt32(reader["GymCompanyID"]);
                        
                    };
                
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }


            finally
            {

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            return gymCompanyId;
        }
    }
}
