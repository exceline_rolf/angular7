﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class UpdateStatusAction : USDBActionBase<bool>
    {
        public string creditornum;
        public string firstDue;
        public string lastDue;
        public string file;
        public string totalAm;

        private DDHistory historyItem;
        public UpdateStatusAction(DDHistory item)
        {
            this.historyItem = item;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USP_InsertDDHistory";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@arNo", DbType.Int32, historyItem.ArNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@filename", DbType.String, historyItem.FileName.Trim()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@orderId", DbType.String, historyItem.OrderId.Trim()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@transactionId", DbType.String, historyItem.TransactionID.Trim()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@canceled", DbType.String, historyItem.Cancellatrion.Trim()));
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
