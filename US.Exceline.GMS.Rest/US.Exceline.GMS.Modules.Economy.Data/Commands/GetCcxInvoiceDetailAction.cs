﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Ccx;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetCcxInvoiceDetailAction : USDBActionBase<List<Invoice>>
    {
        private readonly int _batchId;
        public GetCcxInvoiceDetailAction(int batchId)
        {
            _batchId = batchId;
        }
        protected override List<Invoice> Body(DbConnection connection)
        {
            const string spName = "ExceWorkStationGetCcxInvoiceDetail";
            DbDataReader reader = null;
            var invoiceList = new List<Invoice>();
            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BatchId", DbType.Int32, _batchId));
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var invoice = new Invoice
                        {
                            ReferenceId = Convert.ToInt32(reader["ReferenceId"]),
                            StatusId = Convert.ToInt32(reader["Status"])
                        };

                    invoiceList.Add(invoice);
                }
                return invoiceList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Dispose();
            }
           
        }
    }
}
