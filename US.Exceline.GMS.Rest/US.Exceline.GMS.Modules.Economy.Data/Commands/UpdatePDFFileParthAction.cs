﻿using System;
using System.Data;
using System.Data.Common;
using System.IO;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class UpdatePDFFileParthAction : USDBActionBase<bool>
    {
        private readonly int _id = -1;
        private readonly string _fileParth = string.Empty;
        private readonly int _branchId = -1;
        private readonly string _user = string.Empty;

        public UpdatePDFFileParthAction(int id, string fileParth, int branchId, string user)
        {
            _id = id;
            _fileParth = fileParth;
            _branchId = branchId;
            _user = user;
        }
        
        protected override bool Body(DbConnection connection)
        {
            try
            {
                var fileName = Path.GetFileName(_fileParth);
                const string storedProcedureName = "USExceGMSUpdatePDFFileParth";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fileParth", DbType.String, _fileParth));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fileName", DbType.String, fileName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
