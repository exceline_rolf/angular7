﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class AddImportProcessLogAction : USDBActionBase<bool>
    {
        private string _message = string.Empty;
        private int _fileID = -1;
        private string _user = string.Empty;

        public AddImportProcessLogAction(string message, string user, int fileId)
        {
            _message = message;
            _fileID = fileId;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSOCRImportLog";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@FileID", System.Data.DbType.Int32, _fileID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Description", System.Data.DbType.String, _message));
                command.ExecuteNonQuery();

            }
            catch (Exception) { }
            return true;
        }
    }
}
