﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    class SaveARandARRole : USDBActionBase<int>
    {
        private string _custID;
        private string _custName;
        private string _inkassoId;
        private string _ref1;
        private string _ref2;
        private int _customerEntID;

        public SaveARandARRole(string custID, string inkassoID, int customerEntID, string customerName, string ref1, string ref2, int parentARNo, string transferDate)
        {
            _custID = custID.Trim();
            _custName = customerName.Trim();
            _inkassoId = inkassoID.Trim();
            _ref1 = ref1.Trim();
            _ref2 = ref2.Trim();
            _customerEntID = customerEntID;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int savedARNo = -1;
            try
            {
                string storedProcedureName = "dbo.USP_AddARwithARRole";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Cusid", DbType.String, (object)_custID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@cutomerEntID", DbType.String, (object)_customerEntID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, (object)_custName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InkassoId", DbType.String, _inkassoId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ref1", DbType.String, (object)_ref1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ref2", DbType.String, _ref2));
                savedARNo = int.Parse(cmd.ExecuteScalar().ToString());

            }
            catch (Exception)
            {
                throw;
            }
            return savedARNo;

        }

        public int RunOntransaction(DbTransaction transaction)
        {
            int savedARNo = -1;
            try
            {
                string storedProcedureName = "dbo.USP_AddARwithARRole";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Transaction = transaction;
                cmd.Connection = transaction.Connection;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Cusid", DbType.String, (object)_custID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@cutomerEntID", DbType.String, (object)_customerEntID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, (object)_custName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InkassoId", DbType.String, _inkassoId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ref1", DbType.String, (object)_ref1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ref2", DbType.String, _ref2));
                savedARNo = int.Parse(cmd.ExecuteScalar().ToString());

            }
            catch (Exception)
            {
                throw;
            }
            return savedARNo;
        }
    }
}
