﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    class AddFileValidationStatusRecordAction : USDBActionBase<int>
    {
        private int _recordCount;
        private decimal _sum;
        private string _recordTypeString;
        private int _fileID;
        private string _creditorNo;
        private string _message1;
        private string _message2;
        private string _applicaton;

        public AddFileValidationStatusRecordAction(string creditorNo, int fileID, int recordCounte, decimal sum, string recordTypeString, string message1, string message2, string applicaton)
        {
            _recordCount = recordCounte;
            _sum = sum;
            _recordTypeString = recordTypeString;
            _fileID = fileID;
            if (string.IsNullOrEmpty(creditorNo))
            {
                _creditorNo = "-1";
                _message1 = "Unknown Creditor";
            }
            else
            {
                _creditorNo = creditorNo;
                _message1 = message1;
            }

            _message2 = message2;
            _applicaton = applicaton;
        }
        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int result;
            try
            {
                string storedProcedureName = "USP_AddAddFileValidationSatate";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fileID", DbType.String, _fileID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@product", DbType.String, "USP"));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@creditorNo", DbType.String, _creditorNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@numofRaw", DbType.Int32, _recordCount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _sum));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@group1", DbType.String, _recordTypeString));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@group2", DbType.String, string.Empty));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@application", DbType.String, _applicaton));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@msg2", DbType.String, _message2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@msg1", DbType.String, _message1));
                result = Convert.ToInt32(cmd.ExecuteScalar());

            }
            catch (Exception ex)
            {
                US_DataAccess.core.Exception.USPException exp = new US_DataAccess.core.Exception.USPException();
                //exp.logg(ex.Message);
                exp.ErrorMessage = ex.Message;
                throw exp;
            }
            return result;
        }
    }
}
