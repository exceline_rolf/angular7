﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class UpdateDDCancellationStatusAction : USDBActionBase<string>
    {
        private string _kid;
        //private string _creditorInkassoID;
        private string _status;
        private string _creditorAccountNo;

        public UpdateDDCancellationStatusAction(string kid, string creditorAccountNo, string status)
        {
            _kid = kid;
            //_creditorInkassoID = creditorInkassoID.Trim();
            _status = status;
            _creditorAccountNo = creditorAccountNo;
        }
        protected override string Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USP_CancelDDWithKidAndAccount";

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = connection;

                // cmd.Connection = GenericDBFactory.GetConnection(EnumDatabase.USP);


                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Kid", DbType.String, _kid.Trim()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CredAccountno", DbType.String, _creditorAccountNo.Trim()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Status", DbType.String, Int32.Parse(_status.Trim())));
                cmd.ExecuteNonQuery();
                return _kid;
            }
            catch (Exception ex)
            {
                US_DataAccess.core.Exception.USPException exp = new US_DataAccess.core.Exception.USPException();
                //exp.logg(ex.Message);
                exp.ErrorMessage = ex.Message;
                throw exp;
            }

        }
    }
}
