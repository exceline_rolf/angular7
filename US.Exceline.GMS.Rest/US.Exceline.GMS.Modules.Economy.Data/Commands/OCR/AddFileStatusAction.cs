﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class AddFileStatusAction : USDBActionBase<int>
    {
        private int _fileId;
        private string _status;
        private string _msg1;
        private string _msg2;

        public AddFileStatusAction(int fileId, DateTime time, string status, string msg1, string msg2)
        {
            _fileId = fileId;
            _status = status;
            _msg1 = msg1;
            _msg2 = msg2;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int result;
            try
            {
                string storedProcedureName = "USP_AddFileStatus";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FileId", DbType.Int32, _fileId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FileStatus", DbType.String, _status));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Msg1", DbType.String, _msg1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Msg2", DbType.String, _msg2));
                result = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                US_DataAccess.core.Exception.USPException exp = new US_DataAccess.core.Exception.USPException();
                exp.ErrorMessage = ex.Message;
                throw exp;
            }
            return result;
        }
    }
}
