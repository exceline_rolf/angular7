﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class AddCancellationStatusAction : USDBActionBase<bool>
    {
        private List<CancellationStatus> _statusList;
        public AddCancellationStatusAction(List<CancellationStatus> statusList)
        {
            _statusList = statusList;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSAddCancellationStatus";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                DataTable paymentTable = GetStatusTable(_statusList);
                command.Parameters.Add(DataAcessUtils.CreateParam("@CancellationStatus", SqlDbType.Structured, paymentTable));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private DataTable GetStatusTable(List<CancellationStatus> payments)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("RegDate", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("RecordType", typeof(string)));
            dataTable.Columns.Add(new DataColumn("PID", typeof(string)));
            dataTable.Columns.Add(new DataColumn("BNumber", typeof(string)));
            dataTable.Columns.Add(new DataColumn("CID", typeof(string)));
            dataTable.Columns.Add(new DataColumn("KID", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Status", typeof(string)));
            dataTable.Columns.Add(new DataColumn("StatusDate", typeof(string)));
            dataTable.Columns.Add(new DataColumn("RefernceID", typeof(string)));
            dataTable.Columns.Add(new DataColumn("BatchID", typeof(string)));
            dataTable.Columns.Add(new DataColumn("DataSource", typeof(string)));
            dataTable.Columns.Add(new DataColumn("FileID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("AccountNo", typeof(string)));


            foreach (CancellationStatus importStatus in payments)
            {
                DataRow dataTableRow = dataTable.NewRow();
                dataTableRow["ID"] = -1;
                dataTableRow["RegDate"] = DateTime.Now;
                dataTableRow["RecordType"] = importStatus.RecordType;
                dataTableRow["PID"] = importStatus.PID;
                dataTableRow["BNumber"] = importStatus.BNumber;
                dataTableRow["CID"] = importStatus.CID;
                dataTableRow["KID"] = importStatus.KID;
                dataTableRow["Status"] = importStatus.Status;
                dataTableRow["StatusDate"] = importStatus.StatusDate;
                dataTableRow["RefernceID"] = importStatus.ReferenceId;
                dataTableRow["BatchID"] = importStatus.BatchID;
                dataTableRow["DataSource"] = importStatus.DataSource;
                dataTableRow["FileID"] = importStatus.FileID;
                dataTableRow["AccountNo"] = importStatus.AccountNo;
                dataTable.Rows.Add(dataTableRow);
            }
            return dataTable;
        }
    }
}
