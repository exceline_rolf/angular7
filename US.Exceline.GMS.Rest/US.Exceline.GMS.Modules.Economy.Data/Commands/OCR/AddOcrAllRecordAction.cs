﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class AddOcrAllRecordAction : USDBActionBase<bool>
    {
        private string _fileName, _kid, _status, _credAccountNo;
        public AddOcrAllRecordAction(string fileName, string kid, string status, string creditorAccountNo)
        {
            _fileName = fileName.Trim();
            _kid = kid.Trim();
            _status = status.Trim();
            _credAccountNo = creditorAccountNo;
        }
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "dbo.USP_InsertDDAllStatus";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = connection;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@credAccountNo", DbType.String, _credAccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fileName", DbType.String, _fileName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@kid", DbType.String, _kid));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@status", DbType.String, _status));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@regadate", DbType.String, DateTime.Now.ToString("yyyy-MM-dd")));

                if (cmd.ExecuteNonQuery() != -1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                US_DataAccess.core.Exception.USPException exp = new US_DataAccess.core.Exception.USPException();
                exp.ErrorMessage = ex.Message;
                throw exp;
            }
        }

        #region IDirectDBAccess<bool> Members

        public bool ExecuteCommand(DbConnection connection)
        {
            return Body(connection);
        }

        #endregion
    }
}
