﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class AddOCRImportHistoryAction : USDBActionBase<bool>
    {
        private OCRImportSummary _ocrImportSummary = null;
        private string _user = string.Empty;

        public AddOCRImportHistoryAction(OCRImportSummary oCRImportSummary, string user)
        {
            _ocrImportSummary = oCRImportSummary;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spname = "ExceGMSAddOCRImportProcessHitory";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spname);
                command.Parameters.Add(DataAcessUtils.CreateParam("@FileId", System.Data.DbType.Int32, _ocrImportSummary.FileId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PaymentCount", System.Data.DbType.Int32, _ocrImportSummary.PaymentsCount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@StatusCount", System.Data.DbType.Int32, _ocrImportSummary.StatusCount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PaymentSum", System.Data.DbType.Decimal, _ocrImportSummary.PaymentsSum));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ErrorAccPaymentCount", System.Data.DbType.Int32, _ocrImportSummary.ErrorAccPayments));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ErrorAccStatusAcount", System.Data.DbType.Int32, _ocrImportSummary.ErrorAccStatus));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ErrorAccPaymentSum", System.Data.DbType.Decimal, _ocrImportSummary.ErrorAccountPaymentSum));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ImportedUser", System.Data.DbType.String, _user));

                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
