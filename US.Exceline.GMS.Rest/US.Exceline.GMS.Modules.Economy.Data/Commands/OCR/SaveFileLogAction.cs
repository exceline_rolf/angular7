﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class SaveFileLogAction : USDBActionBase<int>
    {   
        private string _fileName;
        private string _fileType;
        private string _inOut;
        private string _sourcePath;
        private string _destinationPath;
        private string _processPath;
        private int _status;
        private DateTime _sourceTime;
        private DateTime _destinationTime;
        private DateTime _scheduleTime;
        private DateTime _processTime;  
        private string _message1;
        private string _message2;
        private string _user = string.Empty;

        public SaveFileLogAction(string filepath, string folderpath, string user, string fileType)
        {
            _fileName = Path.GetFileName(filepath);
            _destinationPath = folderpath;
            _fileType = fileType;
            _sourcePath = filepath;
            _user = user;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int result;
            try
            {
                string storedProcedureName = "USP_AddFileLog";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FileName", DbType.String, _fileName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FileType", DbType.String, _fileType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InOut", DbType.String, _inOut));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SourcePath", DbType.String, _sourcePath));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DestPath", DbType.String, _destinationPath));
                
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserReg", DbType.String, _user));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@FileId", DbType.Int32, 0));

                SqlParameter outPara = new SqlParameter("@FileId", 0);
                outPara.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(outPara);
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(outPara.Value);

            }
            catch (Exception ex)
            {
                US_DataAccess.core.Exception.USPException exp = new US_DataAccess.core.Exception.USPException();
                //exp.logg(ex.Message);
                exp.ErrorMessage = ex.Message;
                throw exp;
            }
            return result;
        }
    }
}
