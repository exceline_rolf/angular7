﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class ImportCancellationsAction : USDBActionBase<bool>
    {
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSCCXImportNewCancelled";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
