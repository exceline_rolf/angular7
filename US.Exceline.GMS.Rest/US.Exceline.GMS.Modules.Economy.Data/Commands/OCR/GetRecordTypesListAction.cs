﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class GetRecordTypesListAction : USDBActionBase<SortedList>
    {
        protected override SortedList Body(System.Data.Common.DbConnection connection)
        {
            SortedList recordTypes = new SortedList();
            try
            {
                string storedProcedureName = "dbo.USP_GetRecordTypes";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);

                DbDataReader dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    recordTypes.Add(dataReader["ItemTypeID"], dataReader["ItemType"]);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return recordTypes;
        }
    }
}
