﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class UpdateWarningTypeAction : USDBActionBase<bool>
    {
        private string _kid;
        // private string _inkassoID;
        private int _status;
        private string _creditorAccountNo;

        public UpdateWarningTypeAction(string kid, string credAccountNo, int status)
        {
            _kid = kid.Trim();
            // _inkassoID = inkassoID.Trim();
            _status = status;
            _creditorAccountNo = credAccountNo.Trim();
        }
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USP_CancelDDWithKidAndAccount";

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = connection;

                // cmd.Connection = GenericDBFactory.GetConnection(EnumDatabase.USP);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Kid", DbType.String, _kid));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CredAccountno", DbType.String, _creditorAccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Status", DbType.Int32, _status));
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                US_DataAccess.core.Exception.USPException exp = new US_DataAccess.core.Exception.USPException();
                //exp.logg(ex.Message);
                exp.ErrorMessage = ex.Message;
                throw exp;
            }

        }

    }
}
