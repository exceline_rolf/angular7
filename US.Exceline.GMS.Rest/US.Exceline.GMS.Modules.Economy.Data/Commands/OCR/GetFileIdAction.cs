﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class GetFileIdAction : USDBActionBase<int>
    {
        private string  _fileName;
        public GetFileIdAction(string fileName)
        {
            _fileName = fileName; 
        }     
        
        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int result=-1;
            try
            {
                string storedProcedureName = "USP_GetFileId";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fileName", DbType.String, _fileName));               
                result = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
