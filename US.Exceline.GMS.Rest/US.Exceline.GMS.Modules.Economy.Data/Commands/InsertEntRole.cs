﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    class InsertEntRole : USDBActionBase<int>
    {
        private int _intNo;
        private string _roleID;
        private string _reference;
        public InsertEntRole(int intNo, string roleID, string reference)
        {
            _intNo = intNo;
            _roleID = roleID.Trim();
            _reference = reference.Trim();
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            return 1;
        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            int entRoleID;
            string inkosso = "";
            try
            {
                string storedProcedureName = "dbo.USP_AddEntRole";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Transaction = transaction;
                cmd.Connection = transaction.Connection;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntNo", DbType.Int32, _intNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RoleId", DbType.String, _roleID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Ref", DbType.String, _reference));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@inkassoID", DbType.String, inkosso));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entroleid", DbType.Int32, -1));
                entRoleID = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception)
            {
                throw;
            }
            return entRoleID;
        }
    }
}
