﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetPaymentImportProcessLogAction : USDBActionBase<List<PaymentImportLogDC>>
    {
        private DateTime _fromDate;
        private DateTime _toDate;

        public GetPaymentImportProcessLogAction(DateTime fromDate, DateTime toDate)
        {
            _fromDate = fromDate;
            _toDate = toDate;
        }

        protected override List<PaymentImportLogDC> Body(DbConnection connection)
        {
            var paymentLog = new List<PaymentImportLogDC>();
            DbDataReader dataReader = null;
            try
            {
                const string storedProcedureName = "dbo.USExceGMSGetPaymentImportProcessLog";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", DbType.DateTime, _fromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@toDate", DbType.DateTime, _toDate));

                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    var item = new PaymentImportLogDC
                    {
                        Id = Convert.ToInt32(dataReader["ID"]),
                        CreatedDate = Convert.ToDateTime(dataReader["CreatedDate"]),
                        CreatedUser = Convert.ToString(dataReader["CreatedUser"]),
                        Description = Convert.ToString(dataReader["Description"]),
                        KID = Convert.ToString(dataReader["KID"])
                    };
                    paymentLog.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                    dataReader.Dispose();
                }
            }
            return paymentLog;
        }
    }
}
