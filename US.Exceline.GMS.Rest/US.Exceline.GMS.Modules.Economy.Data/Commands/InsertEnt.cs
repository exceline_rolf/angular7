﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class InsertEnt : USDBActionBase<int>
    {
        private string name=string.Empty;
        private string personNo = string.Empty;
        private string born = string.Empty;
        private int entID =-1;
        private string incassoId = string.Empty;
        private string custId = string.Empty;
        private string roleId = string.Empty;
        private string _firstName = string.Empty;
        private string _lastName = string.Empty;
        private string _title = string.Empty;
        private string _userName = string.Empty;
        private bool _isAddressKnown;
         

        public InsertEnt(Debitor  DBObj)
        {
            this.name = DBObj.FirstName.Trim() + " " + DBObj.LastName.Trim();
            this.personNo = DBObj.PersonNo.Trim();
            this.born = DBObj.Born.Trim();
            entID = DBObj.EntID;
            this.incassoId = DBObj.IncassoId.Trim();
            this.custId = DBObj.cid.Trim();
            this.roleId = DBObj.RoleId.Trim();
            _firstName=DBObj.FirstName.Trim();
            _lastName = DBObj.LastName.Trim();
            _title = DBObj.Title;


        }

        public InsertEnt(Debitor DBObj,string userName)
        {
            this.name = DBObj.FirstName.Trim() + " " + DBObj.LastName.Trim();
            this.personNo = DBObj.PersonNo.Trim();
            this.born = DBObj.Born.Trim();
            entID = DBObj.EntID;
            this.incassoId = DBObj.IncassoId.Trim();
            this.custId = DBObj.cid.Trim();
            this.roleId = DBObj.RoleId.Trim();
            _firstName = DBObj.FirstName.Trim();
            _lastName = DBObj.LastName.Trim();
            _title = DBObj.Title;
            _userName = userName;
            _isAddressKnown = DBObj.IsAddressKnown;
        }

        public InsertEnt(InternalPerson DBObj)
        {
            this.name = DBObj.Name.Trim();
            this.roleId = DBObj.RoleID;
        }


        protected override int Body(System.Data.Common.DbConnection connection)
        {
            return 1;
        }

        public  int RunOnTransaction(DbTransaction transaction)
        {
            try
            {
                string storedProcedureName = "dbo.USP_AddEnt";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Transaction = transaction;
                cmd.Connection = transaction.Connection;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, name));
                if (!string.IsNullOrEmpty(born))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@born", DbType.String, born));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@personNo", DbType.String, personNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@inkassoid", DbType.String, incassoId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Cusid", DbType.String, custId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Roleid", DbType.String, roleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@firstName", DbType.String, _firstName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastName", DbType.String, _lastName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@title", DbType.String, _title));
                
                object obj = cmd.ExecuteScalar();
                entID = Convert.ToInt32(obj);
            }
            catch (Exception)
            {
                throw;
            }
            return entID;

        }
    }
}
