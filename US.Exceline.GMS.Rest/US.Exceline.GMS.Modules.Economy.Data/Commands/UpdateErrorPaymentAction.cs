﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    class UpdateErrorPaymentAction : USDBActionBase<int>
    {
        private int _paymentId = -1;
        private string _errorType = string.Empty;
        private int _apportionId = -1;
        private int _aritemNo = -1;

        public UpdateErrorPaymentAction(int paymentId, string errorType, int apportionId, int aritemNo)
        {
            _paymentId = paymentId;
            _errorType = errorType;
            _apportionId = apportionId;
            _aritemNo = aritemNo;
        }
        protected override int Body(DbConnection connection)
        {
            throw new NotImplementedException();
        }
        public int RunOnTransation(DbTransaction dbTransaction)
        {
            int _result = -1;
            try
            {
                string storedProcedure = "dbo.USP_UpdateErrorPayment";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Connection = dbTransaction.Connection;
                cmd.Transaction = dbTransaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PaymentId", DbType.Int32, _paymentId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ErrorType", DbType.String, _errorType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AritemNo", DbType.Int32, _aritemNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ApportionID", DbType.Int32, _apportionId));
                cmd.ExecuteNonQuery();
                _result = 1;
            }
            catch (Exception)
            {
                throw;
            }
            return _result;

        }
    }
}
