﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    class CreateOrderAction : USDBActionBase<int>
    {
        private int _creditorNo = -1;
        private string _orderType = string.Empty;
        private string _coReleationKey = string.Empty;
        public CreateOrderAction(int CreditorNo, string orderType, string coRelationKey)
        {
            _creditorNo = CreditorNo;
            _orderType = orderType;
            _coReleationKey = coRelationKey;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            return 1;
        }

        public int RunOnTrnsaction(DbTransaction transaction)
        {
            try
            {
                string spName = "ExceGMS_INV_CreateOrder";
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure,spName);
                command.Transaction = transaction;
                command.Connection = transaction.Connection;
                command.Parameters.Add(DataAcessUtils.CreateParam("@creditorNo", System.Data.DbType.Int32, _creditorNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@orderType", System.Data.DbType.String, _orderType));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CoReationKey", System.Data.DbType.String, _coReleationKey));

                SqlParameter outpara = new SqlParameter();
                outpara.ParameterName = "@OrderNo";
                outpara.Direction = System.Data.ParameterDirection.Output;
                outpara.DbType = System.Data.DbType.Int32;
                command.Parameters.Add(outpara);

                command.ExecuteNonQuery();
                return Convert.ToInt32(outpara.Value);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
