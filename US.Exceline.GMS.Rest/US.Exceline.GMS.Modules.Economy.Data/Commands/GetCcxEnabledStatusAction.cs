﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetCcxEnabledStatusAction:USDBActionBase<int>
    {
        private readonly int _branchId;

        public GetCcxEnabledStatusAction(int branchId)
        {
            _branchId = branchId;
        }
        protected override int Body(System.Data.Common.DbConnection dbConnection)
        {
            const string spName = "USExceGMSShopGetGymCCXStatus";
            var status = -1;
            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                var obj = cmd.ExecuteScalar();
                status = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }
    }
}
