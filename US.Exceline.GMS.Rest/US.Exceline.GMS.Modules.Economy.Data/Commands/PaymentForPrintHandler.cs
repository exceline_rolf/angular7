﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    internal class PaymentForPrintHandler : ClaimHandler
    {
        public PaymentForPrintHandler(string userName)
        {
           // ClaimPersistingManager = new ClaimPersititngManager(US_DataAccess.EnumDatabase.USP, userName);
        }

        internal override OperationResult SaveClaim(DbTransaction transaction, IUSPClaim claim, int arItemNo)
        {
            // noting to done here. because when invoice is saved no aditional will be saved.
            // Only ARItem related data will be saved
            return new OperationResult();
        }
    }
}
