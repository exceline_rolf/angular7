﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class ReverseApportionmentMainAction : USDBActionBase<ReverseConditions>
    {
        private int _paymentId = -1;
        private int _arItem = -1;
        private string _user = string.Empty;
        private string _actionStatus = string.Empty;
        private DbTransaction _transaction = null;
        public ReverseApportionmentMainAction(int paymentID, int arItem, string actionStatus, string user)
        {
            _paymentId = paymentID;
            _arItem = arItem;
            _actionStatus = actionStatus;
            _user = user;
        }

        protected override ReverseConditions Body(System.Data.Common.DbConnection connection)
        {
            _transaction = connection.BeginTransaction();
            try
            {
                int reverseResult = -1;

                if (_actionStatus == "Mapped")
                {
                    ReverseApportionmentByArItemNoAction reverseAction = new ReverseApportionmentByArItemNoAction(_paymentId, _arItem, _user);
                    reverseResult = reverseAction.RunOnTransaction(_transaction);
                }
                else
                {
                    ReverseApportionmentsAction reverseAction = new ReverseApportionmentsAction(_paymentId, 0, _user);
                    reverseResult = reverseAction.RunOnTransaction(_transaction);
                }

                if (reverseResult == 1)
                {
                    _transaction.Commit();
                    return ReverseConditions.SUCCESS;
                }
                else if (reverseResult == 2)
                {
                    _transaction.Rollback();
                    return ReverseConditions.APPREMITTED;
                }
                else if (reverseResult == 4)
                {
                    _transaction.Rollback();
                    return ReverseConditions.NOAPPORTIONMENTS;
                }
                else
                {
                    _transaction.Rollback();
                    return ReverseConditions.ERRROR;
                }
            }
            catch (Exception)
            {
                _transaction.Rollback();
                throw;
            }
        }
    }
}
