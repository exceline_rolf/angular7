﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetEconomyFileLogSummaryAction : USDBActionBase<List<EconomyFileLogData>>
    {
        private readonly DateTime _fromDate;
        private readonly DateTime _toDate;
        private readonly string _source;
        private readonly string _filterBy;

        public GetEconomyFileLogSummaryAction(DateTime fromDate, DateTime toDate, string source, string filterBy)
        {
            _fromDate = fromDate;
            _toDate = toDate;
            _source = source;
            _filterBy = filterBy;
        }

        protected override List<EconomyFileLogData> Body(DbConnection dbConnection)
        {
            var economyFileLogData = new List<EconomyFileLogData>();
            const string storedProcedureName = "USExceGMSGetEconomyFileLogSummary";

            var cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
            cmd.Connection = dbConnection;
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@FromDate", System.Data.DbType.DateTime, _fromDate));
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@ToDate", System.Data.DbType.DateTime, _toDate));
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@Source", System.Data.DbType.String, _source));
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@FilterBy", System.Data.DbType.String, _filterBy));

            try
            {
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var fileLogData = new EconomyFileLogData
                        {
                            Date = Convert.ToDateTime(reader["Date"].ToString()),
                            DateOfFirstOpening = Convert.ToDateTime(reader["Date"].ToString()),
                            TotalDeduction = Convert.ToDecimal(reader["TotalDeduction"]),
                            BatchID = reader["BatchID"].ToString(),
                            NumberOfDeviations = Convert.ToInt32(reader["NoOFDeviations"].ToString()),
                            Type = reader["ItemType"].ToString(),
                            TypeCode = reader["ItemTypeCode"].ToString(),
                            Status = reader["Status"].ToString()
                        };
                    if (_filterBy == "Export")
                    {
                        fileLogData.DateOfFirstOpening = null;
                        fileLogData.IsDeviationLogVisible = false;
                    }else if (fileLogData.Type == "ATG Status")
                    {
                        fileLogData.IsDeviationLogVisible = false;
                    }
                    else
                    {
                        fileLogData.IsDeviationLogVisible = true;
                    }
                    economyFileLogData.Add(fileLogData);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return economyFileLogData;
        }
    }
}
