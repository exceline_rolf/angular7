﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class SearchInvoicesAction : USDBActionBase<List<USPARItem>>
    {
        private InvoiceSearchCriteria _searchCriteria = null;
        private SearchModes _searchMode = SearchModes.NONE;
        //private object _constValue = null;
        public SearchInvoicesAction(InvoiceSearchCriteria criteria, SearchModes searchMode, object constValue)
        {
            _searchCriteria = criteria;
            _searchMode = searchMode;
            //_constValue = constValue;
        }

        protected override List<USPARItem> Body(System.Data.Common.DbConnection connection)
        {
            List<USPARItem> arItemList = new List<USPARItem>();
            try
            {
                DbCommand searchCommand = connection.CreateCommand();
                searchCommand.CommandText = "USP_AdvanceInvoiceSearch";
                searchCommand.CommandType = System.Data.CommandType.StoredProcedure;
                searchCommand.CommandTimeout = int.MaxValue;

                if (_searchCriteria.SubCaseNumber != 0)
                    searchCommand.Parameters.Add(new SqlParameter("@SubCaseNo", _searchCriteria.SubCaseNumber));
                if (_searchCriteria.CreditorGroupID != -1)
                    searchCommand.Parameters.Add(new SqlParameter("@creditorGroupId", _searchCriteria.CreditorGroupID));
                if (_searchCriteria.ARNumber != 0)
                    searchCommand.Parameters.Add(new SqlParameter("@ARNo", _searchCriteria.ARNumber));
                if (_searchCriteria.InvoiceNumber != string.Empty && (_searchCriteria.InvoiceNumber != "0"))
                    searchCommand.Parameters.Add(new SqlParameter("@InvoiceNo", _searchCriteria.InvoiceNumber));
                if (_searchCriteria.KID != string.Empty && _searchCriteria.KID != null)
                    searchCommand.Parameters.Add(new SqlParameter("@Kid", _searchCriteria.KID));
                if (_searchCriteria.CustomerName != string.Empty && _searchCriteria.CustomerName != null)
                    searchCommand.Parameters.Add(new SqlParameter("@CustomerName", _searchCriteria.CustomerName));
                if (_searchCriteria.CreditorName != string.Empty)
                    searchCommand.Parameters.Add(new SqlParameter("@CreditorName", _searchCriteria.CreditorName));
                if (_searchCriteria.CreditorNumber != string.Empty && _searchCriteria.CreditorNumber != null)
                    searchCommand.Parameters.Add(new SqlParameter("@CreditorNumber", _searchCriteria.CreditorNumber));
                if (_searchCriteria.Amount != 0)
                    searchCommand.Parameters.Add(new SqlParameter("@Amount", _searchCriteria.Amount));
                if (_searchCriteria.CustomerNumber != string.Empty && _searchCriteria.CustomerNumber != null)
                    searchCommand.Parameters.Add(new SqlParameter("@CustomerNumber", _searchCriteria.CustomerNumber));

                searchCommand.Parameters.Add(new SqlParameter("@invoiceType", _searchCriteria.InvoiceType));
                searchCommand.Parameters.Add(new SqlParameter("@startDate", _searchCriteria.StartDate));
                searchCommand.Parameters.Add(new SqlParameter("@endDate", _searchCriteria.EndDate));

                if (_searchMode == SearchModes.AR)
                {
                    searchCommand.Parameters.Add(new SqlParameter("@fixedField", "AR"));
                }
                if (_searchMode == SearchModes.CREDITOR)
                {
                    searchCommand.Parameters.Add(new SqlParameter("@fixedField", "CreditorNo"));
                }
                if (_searchMode == SearchModes.CREDITORGROUP)
                {
                    searchCommand.Parameters.Add(new SqlParameter("@fixedField", "CreditorGroup"));
                }

                DbDataReader reader = searchCommand.ExecuteReader();
                while (reader.Read())
                {
                    USPARItem arItem = new USPARItem();
                    arItem.InkassoID = reader["CreditorInkassoID"].ToString();
                    arItem.cusID = reader["CustId"].ToString();
                    arItem.DebtorName = reader["debtorName"].ToString();
                    arItem.KID = reader["KID"].ToString();
                    arItem.InvoiceNumber = reader["Ref"].ToString();
                    arItem.Amount = (reader["Amount"] != DBNull.Value) ? Convert.ToDecimal(reader["Amount"]) : 0;
                    arItem.ARItemNo = reader["ARItemNo"].ToString();
                    if (reader["Regdate"] != DBNull.Value)
                        arItem.RegDate = Convert.ToDateTime(reader["Regdate"]);
                    if (reader["VoucherDate"] != DBNull.Value)
                        arItem.InvoicedDate = Convert.ToDateTime(reader["VoucherDate"]);
                    if (reader["DueDate"] != DBNull.Value)
                        arItem.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    arItem.CreditorName = reader["creditorName"].ToString();
                    arItem.Creditor = arItem.InkassoID + " - " + arItem.CreditorName;
                    arItem.Debtor = arItem.cusID + " - " + arItem.DebtorName;
                    arItem.Hit = reader["SearchFields"].ToString().Trim(',');
                    if (reader["SubCaseNo"] != DBNull.Value)
                        arItem.SubCaseNo = Convert.ToInt32(reader["SubCaseNo"]);

                    if (reader["CaseNo"] != DBNull.Value)
                        arItem.CaseNo = Convert.ToInt32(reader["CaseNo"]);
                    if (_searchMode == SearchModes.AR)
                    {
                        arItem.IsCheckBoxEnabled = false;
                    }
                    else
                    {
                        arItem.IsCheckBoxEnabled = true;
                    }

                    arItemList.Add(arItem);
                }
                return arItemList;
            }
            catch (Exception)
            {
                //log the exception
            }
            return arItemList;
        }
    }
}
