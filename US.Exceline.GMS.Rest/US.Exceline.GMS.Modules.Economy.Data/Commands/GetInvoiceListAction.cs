﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetInvoiceListAction : USDBActionBase<List<InvoiceDC>>
    {
        private readonly string _category = string.Empty;
        private readonly DateTime _fromDate = DateTime.Now;
        private readonly DateTime _toDate = DateTime.Now;
        private readonly int _branchId = -1;

        public GetInvoiceListAction(string category, DateTime fromDate, DateTime toDate, int branchId)
        {
            _category = category;
            _fromDate = fromDate;
            _toDate = toDate;
            _branchId = branchId;
        }
        
        protected override List<InvoiceDC> Body(DbConnection connection)
        {
            const string spName = "USExceGMSGetInvoiceList";
            DbDataReader reader = null;
            var invoiceDetailList = new List<InvoiceDC>();
            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@category", DbType.String, _category));
                command.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", DbType.DateTime, _fromDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@toDate", DbType.DateTime, _toDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var invoiceDetail = new InvoiceDC();
                    invoiceDetail.ArItemNo      = Convert.ToInt32(reader["ArItemNo"]);
                    invoiceDetail.CustNo        = Convert.ToString(reader["CustNo"]);
                    invoiceDetail.CustName      = Convert.ToString(reader["CustName"]);
                    invoiceDetail.Ref           = Convert.ToString(reader["Ref"]);
                    invoiceDetail.KId           = Convert.ToString(reader["KID"]);
                    invoiceDetail.InvocieDate   = Convert.ToDateTime(reader["InvocieDate"]);
                    invoiceDetail.DueDate       = Convert.ToDateTime(reader["DueDate"]);
                    invoiceDetail.InvoiceAmount = Convert.ToDecimal(reader["InvoiceAmount"]);
                    invoiceDetail.Balance       = Convert.ToDecimal(reader["Balance"]);
                    invoiceDetail.ContractNo    = Convert.ToString(reader["ContractNo"]);
                    invoiceDetail.GymName       = Convert.ToString(reader["GymName"]);
                    invoiceDetail.InvoiceCategory = Convert.ToString(reader["InvoiceCategory"]);

                    invoiceDetailList.Add(invoiceDetail);
                }
                return invoiceDetailList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Dispose();
            }
        }
    }
}
