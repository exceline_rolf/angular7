﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class AddOrderLine : USDBActionBase<int>
    {
        //private int _orderLine = 0;
        private int _arItemNo = 0;
        private string _inkassoId = string.Empty;
        private string _cutId = string.Empty;
        private string _lastName = string.Empty;
        private string _firstName = string.Empty;
        private string _nameOnContract = string.Empty;
        private string _address = string.Empty;
        private string _zipCode = string.Empty;
        private string _postPlace = string.Empty;
        private decimal _discountAmount = 0;
        private string _employeeNo = string.Empty;
        private string _sponsorRef = string.Empty;
        private int _visits = 0;
        private string _lastVisit = string.Empty;
        private decimal _amount = 0;
        private string _amountThisOrderLine = string.Empty;
        private int _sourceInstallmentID = -1;

        public AddOrderLine(OrderLine orderLine)
        {
            // _orderLine = orderLine.OrderLineID;
            _arItemNo = orderLine.ArItemNo;
            _inkassoId = orderLine.InkassoId.Trim();
            _cutId = orderLine.CutId.Trim();
            _lastName = orderLine.LastName.Trim();
            _firstName = orderLine.FirstName.Trim();
            _nameOnContract = orderLine.NameOnContract.Trim();
            _address = orderLine.Address.Trim();
            _zipCode = orderLine.ZipCode.Trim();
            _postPlace = orderLine.PostPlace.Trim();
            //_discountAmount = orderLine.DiscountAmount.Trim();
            if (string.IsNullOrEmpty(orderLine.DiscountAmount))
            {
                _discountAmount = 0;
            }
            else
            {
                _discountAmount = Convert.ToDecimal(orderLine.DiscountAmount.Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator).Replace(",", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator));
            }
            _employeeNo = orderLine.EmployeeNo.Trim();
            _sponsorRef = orderLine.SponsorRef.Trim();
            _visits = orderLine.Visit;
            _lastVisit = orderLine.LastVisit.Trim();
            _sourceInstallmentID = orderLine.SourceInstallmentID;
            //_amount = orderLine.Amount.Trim();
            if (string.IsNullOrEmpty(orderLine.DiscountAmount))
            {
                _discountAmount = 0;
            }
            else
            {
                _amount = Convert.ToDecimal(orderLine.Amount.Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator).Replace(",", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator));
            }
            _amountThisOrderLine = orderLine.AmountThisOrderLines.Trim();
        }


        protected override int Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USP_AddOrderLine";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);


                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ARItemNo", DbType.Int32, _arItemNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditorInkassoID", DbType.String, _inkassoId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CusId", DbType.String, _cutId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastName", DbType.String, _lastName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FirstName", DbType.String, _firstName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NameOnContract", DbType.String, _nameOnContract));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Address", DbType.String, _address));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ZipCode", DbType.String, _zipCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PostPlace", DbType.String, _postPlace));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DiscountAmount", DbType.Decimal, _discountAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeNo", DbType.String, _employeeNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsorRef", DbType.String, _sponsorRef));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Visits", DbType.Int32, _visits));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AmountOrderline", DbType.String, _amountThisOrderLine));
                if (!string.IsNullOrEmpty(_lastVisit))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastVisit", DbType.String, _lastVisit));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Amount", DbType.Decimal, _amount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SourceInstallmentID", DbType.Int32, _sourceInstallmentID));

                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            return 1;

        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            try
            {
                string storedProcedureName = "USP_AddOrderLine";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Transaction = transaction;
                cmd.Connection = transaction.Connection;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ARItemNo", DbType.Int32, _arItemNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditorInkassoID", DbType.String, _inkassoId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CusId", DbType.String, _cutId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastName", DbType.String, _lastName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FirstName", DbType.String, _firstName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NameOnContract", DbType.String, _nameOnContract));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Address", DbType.String, _address));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ZipCode", DbType.String, _zipCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PostPlace", DbType.String, _postPlace));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DiscountAmount", DbType.Decimal, _discountAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeNo", DbType.String, _employeeNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsorRef", DbType.String, _sponsorRef));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Visits", DbType.Int32, _visits));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AmountOrderline", DbType.String, _amountThisOrderLine));
                if (!string.IsNullOrEmpty(_lastVisit))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastVisit", DbType.String, _lastVisit));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Amount", DbType.Decimal, _amount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SourceInstallmentID", DbType.Int32, _sourceInstallmentID));
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            return 1;
        }
       
    }
}
