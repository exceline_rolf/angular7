﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class InsertInvoicePrintRecordAction : USDBActionBase<int>
    {
        int _branchId = 0;
        String _invoiceNo = String.Empty;
        String _XMLData = String.Empty;
        String _HashValue = String.Empty;

        public InsertInvoicePrintRecordAction(int branchId, String invoiceNo, String xmlData, String hashValue)
        {
            _branchId = branchId;
            _invoiceNo = invoiceNo;
            _XMLData = xmlData;
            _HashValue = hashValue;
        }


        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "exceline.InsertInvoicePrintRecord";

            try
            {


                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceNo", DbType.String, _invoiceNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@XMLdata", DbType.String, _XMLData));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Hashvalue", DbType.String, _HashValue));


                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return 1;
           
        }
    }
}

