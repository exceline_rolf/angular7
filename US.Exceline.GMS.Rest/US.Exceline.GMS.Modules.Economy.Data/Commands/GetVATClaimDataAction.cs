﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetVATClaimDataAction : USDBActionBase<List<USPVATClaim>>
    {
        private int _creditorNo = -1;
        private string _invoiceNo = string.Empty;
        public GetVATClaimDataAction(int creditorNo, string invoiceNo)
        {
            _creditorNo = creditorNo;
            _invoiceNo = invoiceNo;
        }

        protected override List<USPVATClaim> Body(System.Data.Common.DbConnection connection)
        {
            return new List<USPVATClaim>();
        }

        public List<USPVATClaim> RunOntransaction(DbTransaction transaction)
        {
            DbDataReader reader = null;
            string spname = "ExceGMS_INV_GetCreditorInvoiceForImport";
            try
            {
                List<USPVATClaim> uspVATClaimList = new List<USPVATClaim>();
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spname);
                command.Transaction = transaction;
                command.Connection = transaction.Connection;

                command.Parameters.Add(DataAcessUtils.CreateParam("@CreditorNo", System.Data.DbType.Int32, _creditorNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceNo", System.Data.DbType.String, _invoiceNo));

                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    USPVATClaim vatOrderlineData = new USPVATClaim();

                    vatOrderlineData.CreditorNo = Convert.ToString(reader["CreditorNo"]);
                    vatOrderlineData.Amount = Convert.ToDecimal(reader["Amount"]);
                    vatOrderlineData.Addr1 = Convert.ToString(reader["Addr1"]);
                    vatOrderlineData.Addr2 = Convert.ToString(reader["Addr2"]);
                    vatOrderlineData.Addr3 = Convert.ToString(reader["Addr3"]);
                    vatOrderlineData.CountryId = Convert.ToString(reader["CountryId"].ToString());
                    vatOrderlineData.DebtorFirstName = reader["DebtorFirstName"].ToString();
                    vatOrderlineData.DebtorLastName = reader["DebtorLastName"].ToString();
                    vatOrderlineData.DebtorNo = reader["DebtorNo"].ToString();
                    vatOrderlineData.DebtorEntNo = Convert.ToInt32(reader["DebtorEntNo"]);
                    if (!string.IsNullOrEmpty(reader["Born"].ToString()))
                    {
                        vatOrderlineData.DebtorBorn = Convert.ToDateTime(reader["Born"]);
                    }
                    vatOrderlineData.Email = Convert.ToString(reader["Email"]);
                    vatOrderlineData.Fax = Convert.ToString(reader["Fax"]);
                    vatOrderlineData.ZipName = Convert.ToString(reader["ZipName"]);
                    vatOrderlineData.ZipCode = Convert.ToString(reader["ZipCode"]);
                    vatOrderlineData.TelWork = Convert.ToString(reader["TelWork"]);
                    vatOrderlineData.TelMobile = Convert.ToString(reader["TelMobile"]);
                    vatOrderlineData.TelHome = Convert.ToString(reader["TelHome"]);
                    vatOrderlineData.RegDate = Convert.ToDateTime(reader["Regdate"]);
                    vatOrderlineData.MSN = Convert.ToString(reader["MSN"]);
                    vatOrderlineData.Id = Convert.ToInt32(reader["Id"]);
                    vatOrderlineData.Fax = Convert.ToString(reader["Fax"]);
                    vatOrderlineData.InvoicedDate = Convert.ToDateTime(reader["InvoicedDate"]);
                    vatOrderlineData.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    vatOrderlineData.InvoiceTypeId = Convert.ToInt32(reader["ItemTypeId"]);
                    //vatOrderlineData.PaidDate = Convert.ToDateTime(reader["Regdate"]));
                    vatOrderlineData.InvoiceNo = Convert.ToString(reader["InvoiceNo"]);
                    //vatOrderlineData.DueBalance =Convert.ToString(reader["DueBalance"]);
                    vatOrderlineData.AccountNumber = Convert.ToString(reader["AccountNo"]);
                    vatOrderlineData.ContractNumber = Convert.ToString(reader["ContractNo"]);
                    vatOrderlineData.ContractExpire = Convert.ToString(reader["ContractExp"]);
                    vatOrderlineData.LastContract = Convert.ToString(reader["LastContract"]);
                    vatOrderlineData.ContractKID = Convert.ToString(reader["ContractKID"]);
                    vatOrderlineData.StatusMandatory = Convert.ToString(reader["StatusMandatory"]);
                    vatOrderlineData.InstallmentNumber = Convert.ToString(reader["InstallmentNo"]);
                    vatOrderlineData.InstallmentKID = Convert.ToString(reader["InstallmentKID"]);
                    vatOrderlineData.TransmissionNumberBBS = Convert.ToString(reader["TBBS"]);
                    vatOrderlineData.Balance = Convert.ToString(reader["Balance"]);
                    vatOrderlineData.DueBalance = Convert.ToString(reader["DueBalance"]);
                    vatOrderlineData.LastReminder = Convert.ToString(reader["LastReminder"]);
                    vatOrderlineData.CollectingStatus = Convert.ToString(reader["CollectingStatus"]);//CollText
                    vatOrderlineData.CollText = Convert.ToString(reader["CollText"]);//BranchNo
                    vatOrderlineData.BranchNo = Convert.ToString(reader["BranchNo"]);
                    vatOrderlineData.NameInContract = Convert.ToString(reader["NameOnContract"]);
                    vatOrderlineData.LastVisit = Convert.ToString(reader["LastVisit"]);
                    vatOrderlineData.OriginalDueDate = Convert.ToString(reader["OriginalDue"]);
                    string decimalSeperator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                    vatOrderlineData.ReminderFee = Convert.ToDecimal(reader["ReminderFee"].ToString().Replace(".", decimalSeperator).Replace(",", decimalSeperator));
                    vatOrderlineData.MessageText = Convert.ToString(reader["MessageText"]);
                    vatOrderlineData.IsNeededWritenDoc = Convert.ToString(reader["IsNeededWritenDoc"]);
                    vatOrderlineData.InvoiceText = Convert.ToString(reader["Text"]);
                    vatOrderlineData.InvoiceCharge = Convert.ToDecimal(reader["InvoiceCharge"]);
                    uspVATClaimList.Add(vatOrderlineData);
                }

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                return uspVATClaimList;
            }
            catch (Exception)
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                throw;
            }
        }
    }
}
