﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetApportionmentCaseNoAction : USDBActionBase<List<int>>
    {
        private string _KID = string.Empty;
        private string _creditorNo = string.Empty;
        private int _arNo = -1;
        public GetApportionmentCaseNoAction(string KID, string creditorNo, int ARNo)
        {
            _KID = KID;
            _creditorNo = creditorNo;
            _arNo = ARNo;
        }

        protected override List<int> Body(System.Data.Common.DbConnection connection)
        {
            List<int> CaseNoList = new List<int>();
            DbDataReader reader = null;
            try
            {
                DbCommand command = connection.CreateCommand();
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandTimeout = int.MaxValue;
                command.CommandText = "USP_GetApportionmentCaseNo";
                command.Parameters.Add(new SqlParameter("@KID", _KID));
                command.Parameters.Add(new SqlParameter("@creditorNo", _creditorNo));
                command.Parameters.Add(new SqlParameter("@VoucherARNo", _arNo));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["CaseNo"] != null)
                        CaseNoList.Add(Convert.ToInt32(reader["CaseNo"]));
                }

            }
            catch (Exception)
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                throw;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return CaseNoList;
        }
    }
}
