﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetClassFileDetailsAction : USDBActionBase<List<ClassFileDetail>>
    {
        public GetClassFileDetailsAction()
        {

        }
        protected override List<ClassFileDetail> Body(DbConnection connection)
        {
            const string spName = "USExceGMSGetClassFileDetails";
            DbDataReader reader = null;
            var classDetailList = new List<ClassFileDetail>();
            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, spName);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var classDetail = new ClassFileDetail
                    {
                        FileName = Convert.ToString(reader["FileName"]),
                        CreatedDate = Convert.ToDateTime(reader["CreatedDate"])
                    };
                    classDetailList.Add(classDetail);
                }
                return classDetailList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Dispose();
            }
        }
    }
}
