﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class AddAddress : USDBActionBase<int>
    {
        private readonly string _address1 = string.Empty;
        private readonly string _address2 = string.Empty;
        private readonly string _address3 = string.Empty;
        private readonly string _addressSource = string.Empty;
        private readonly string _zip = string.Empty;
        private readonly string _zipName = string.Empty;
        private readonly string _countryCode = string.Empty;
        private readonly string _telMobile = string.Empty;
        private readonly string _telWork = string.Empty;
        private readonly string _telHome = string.Empty;
        private readonly string _fax = string.Empty;
        private readonly string _email = string.Empty;
        private readonly string _msn = string.Empty;
        private readonly string _skype = string.Empty;
        private readonly string _sms = string.Empty;
        private readonly string _userName = string.Empty;
        private readonly string _municipality = string.Empty;
        private readonly bool _isDefault;
        private readonly int _entRoleId;
        private int _result;

        public AddAddress(CAddress addr, int entRoleId)
        {
            try
            {
                _entRoleId = entRoleId;
                _address1 = string.IsNullOrEmpty(addr.Address1) ? string.Empty : addr.Address1.Trim();
                _address2 = string.IsNullOrEmpty(addr.Address2) ? string.Empty : addr.Address2.Trim();
                _address3 = string.IsNullOrEmpty(addr.Address3) ? string.Empty : addr.Address3.Trim();
                _addressSource = string.IsNullOrEmpty(addr.AddSource) ? string.Empty : addr.AddSource.Trim();
                //this.zip = string.IsNullOrEmpty(addr.Zip) ? string.Empty : addr.Zip.Trim();
                _countryCode = string.IsNullOrEmpty(addr.CountryCode) ? string.Empty : addr.CountryCode.Trim();
                _zipName = string.IsNullOrEmpty(addr.ZipName) ? string.Empty : addr.ZipName.Trim();
                _zip = string.IsNullOrEmpty(addr.Zip) ? string.Empty : addr.Zip.Trim();
                _telMobile = string.IsNullOrEmpty(addr.TelMobile) ? string.Empty : addr.TelMobile.Trim();
                _telWork = string.IsNullOrEmpty(addr.TelWork) ? string.Empty : addr.TelWork.Trim();
                _telHome = string.IsNullOrEmpty(addr.TelHome) ? string.Empty : addr.TelHome.Trim();
                _fax = string.IsNullOrEmpty(addr.Fax) ? string.Empty : addr.Fax.Trim();
                _email = string.IsNullOrEmpty(addr.Email) ? string.Empty : addr.Email.Trim();
                _msn = string.IsNullOrEmpty(addr.MSN) ? string.Empty : addr.MSN.Trim();
                _skype = string.IsNullOrEmpty(addr.Skype) ? string.Empty : addr.Skype.Trim();
                _sms = string.IsNullOrEmpty(addr.SMS) ? string.Empty : addr.SMS.Trim();
                _municipality = string.IsNullOrEmpty(addr.Municipality) ? string.Empty : addr.Municipality.Trim();
                _isDefault = addr.IsDefault;
            }
            catch
            {
                throw;
            }
        }

        public AddAddress(CAddress addr, string userName, int entRoleId)
        {
            try
            {
                _entRoleId = entRoleId;
                _address1 = string.IsNullOrEmpty(addr.Address1) ? string.Empty : addr.Address1.Trim();
                _address2 = string.IsNullOrEmpty(addr.Address2) ? string.Empty : addr.Address2.Trim();
                _address3 = string.IsNullOrEmpty(addr.Address3) ? string.Empty : addr.Address3.Trim();
                _addressSource = string.IsNullOrEmpty(addr.AddSource) ? string.Empty : addr.AddSource.Trim();
                //this.zip = string.IsNullOrEmpty(addr.Zip) ? string.Empty : addr.Zip.Trim();
                _countryCode = string.IsNullOrEmpty(addr.CountryCode) ? string.Empty : addr.CountryCode.Trim();
                _zipName = string.IsNullOrEmpty(addr.ZipName) ? string.Empty : addr.ZipName.Trim();
                _zip = string.IsNullOrEmpty(addr.Zip) ? string.Empty : addr.Zip.Trim();
                _telMobile = string.IsNullOrEmpty(addr.TelMobile) ? string.Empty : addr.TelMobile.Trim();
                _telWork = string.IsNullOrEmpty(addr.TelWork) ? string.Empty : addr.TelWork.Trim();
                _telHome = string.IsNullOrEmpty(addr.TelHome) ? string.Empty : addr.TelHome.Trim();
                _fax = string.IsNullOrEmpty(addr.Fax) ? string.Empty : addr.Fax.Trim();
                _email = string.IsNullOrEmpty(addr.Email) ? string.Empty : addr.Email.Trim();
                _msn = string.IsNullOrEmpty(addr.MSN) ? string.Empty : addr.MSN.Trim();
                _skype = string.IsNullOrEmpty(addr.Skype) ? string.Empty : addr.Skype.Trim();
                _sms = string.IsNullOrEmpty(addr.SMS) ? string.Empty : addr.SMS.Trim();
                _municipality = string.IsNullOrEmpty(addr.Municipality) ? string.Empty : addr.Municipality.Trim();
                _isDefault = addr.IsDefault;
                //this._userName = string.IsNullOrEmpty(addr.UserName) ? string.Empty : addr.UserName.Trim();
                _userName = userName;
            }
            catch
            {
                throw;
            }
        }
        
        protected override int Body(DbConnection connection)
        {
            try
            {
                const string storedProcedureName = "dbo.USP_AddAddr";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Addr1", DbType.String, _address1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Addr2", DbType.String, _address2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Addr3", DbType.String, _address3));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ZipCode", DbType.String, _zip));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AddResource", DbType.String, _addressSource));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ZipName", DbType.String, _zipName));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@username", DbType.String, _userName));

                // This chcek is done to avoid foerign key violation (to avoid to send empty string)                
                if (!string.IsNullOrEmpty(_countryCode))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@CountryCode", DbType.String, _countryCode));
                }

                if (_entRoleId > 1)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entRoleId", DbType.Int32, _entRoleId));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@telMobile", DbType.String, _telMobile));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@telWork", DbType.String, _telWork));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@telHome", DbType.String, _telHome));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@email", DbType.String, _email));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fax", DbType.String, _fax));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@msn", DbType.String, _msn));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@skype", DbType.String, _skype));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sms", DbType.String, _sms));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@municipality", DbType.String, _municipality));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isDefault", DbType.Boolean, _isDefault));
                //if (addrNo > 1)
                //{
                //    cmd.Parameters.Add(DataAcessUtils.CreateParam("@addrNo", DbType.Int32, addrNo));
                //}
                _result = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                _result = -1;
                throw ex;
            }
            return _result;

        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            try
            {
                const string storedProcedureName = "dbo.USP_AddAddr";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Transaction = transaction;
                cmd.Connection = transaction.Connection;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Addr1", DbType.String, _address1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Addr2", DbType.String, _address2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Addr3", DbType.String, _address3));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ZipCode", DbType.String, _zip));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AddResource", DbType.String, _addressSource));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ZipName", DbType.String, _zipName));

                // This chcek is done to avoid foerign key violation (to avoid to send empty string)                
                if (!string.IsNullOrEmpty(_countryCode))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@CountryCode", DbType.String, _countryCode));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entRoleId", DbType.Int32, _entRoleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@telMobile", DbType.String, _telMobile));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@telWork", DbType.String, _telWork));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@telHome", DbType.String, _telHome));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@email", DbType.String, _email));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fax", DbType.String, _fax));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@msn", DbType.String, _msn));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@skype", DbType.String, _skype));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sms", DbType.String, _sms));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@city", DbType.String, city));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@municipality", DbType.String, _municipality));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isDefault", DbType.Boolean, _isDefault));

                _result = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                _result = -1;
                throw new Exception("Error in adding address " + ex.Message);

                    
            }
            return _result;
        }
    }
}
