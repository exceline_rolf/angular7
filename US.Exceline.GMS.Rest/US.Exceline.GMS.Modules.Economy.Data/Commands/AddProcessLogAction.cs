﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class AddProcessLogAction : USDBActionBase<int>
    {
        private string _user = string.Empty;
        private string _description = string.Empty; 
        private int _orderId = -1;

        public AddProcessLogAction(string user, string description, int orderID)
        {
            _user = user;
            _description = description;
            _orderId = orderID;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSAddInvoiceGenerationLog";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@OrderID", System.Data.DbType.Int32, _orderId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Description", System.Data.DbType.String, _description));
                command.ExecuteNonQuery();
               
            }
            catch (Exception) { }
            return 1;
        }
    }
}
