﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class AddArItemDetails : USDBActionBase<int>
    {
        private string accountNo;
        private string contractNo;
        private string contractExp;
        private string lastContract;
        private string contractKid;
        private string statusMen;
        private string insNo;
        private string insKID;
        private string transmissionNo;
        private string balance;
        private string dueBalance;
        private string lastReminder;
        private string collectingStutas;
        private string collectingtext;
        private string branchNo;

        private string nameOncontract;
        private string lastvisit;
        private string printDate;
        private string OriDueDate;
        private string reminderFee;
        private string invoiceAmount = string.Empty;
        private int ARItemNo;
        private int DDNo = -1;
        private string InvoiceRef = string.Empty;
        private string _group = string.Empty;

        public AddArItemDetails(DirectDeduct DDObj, int ARNo1)
        {

            this.accountNo = DDObj.AccountNo.Trim();
            this.contractNo = DDObj.ContractNo.Trim();
            this.contractExp = DDObj.ContractExpire.Trim();
            this.lastContract = DDObj.LastContract.Trim();
            this.contractKid = DDObj.ContractKID.Trim();
            this.statusMen = ((int)DDObj.StatusMendatory).ToString().Trim();
            this.insNo = DDObj.InstallmentNo.Trim();
            this.insKID = DDObj.InstallmentKID.Trim();
            this.transmissionNo = DDObj.TrancemissionNo.Trim();
            this.balance = DDObj.Balance.Trim();
            this.dueBalance = DDObj.DueBalance.Trim();
            this.lastReminder = DDObj.LastReminder.Trim();
            this.collectingStutas = ((int)DDObj.CollectingStatus).ToString().Trim();
            this.collectingtext = DDObj.CollectingText.Trim();
            this.branchNo = DDObj.BranchNo.Trim();
            this.ARItemNo = ARNo1;

            this.nameOncontract = DDObj.nameInContract.Trim();
            this.lastvisit = DDObj.lastVisit.Trim();
            this.printDate = DDObj.printDate.Trim();
            this.OriDueDate = DDObj.originalDue.Trim();
            this.reminderFee = DDObj.reminderfee.Trim();
            this.invoiceAmount = DDObj.InvoiceAmount.Trim();
            _group = DDObj.Group;

            if (DDObj.InvoiceRef != null)
            {
                this.InvoiceRef = DDObj.InvoiceRef;
            }
        }


        protected override int Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "dbo.USP_AddARItemDetails";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);


                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@addressNo", DbType.Int32, accountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ARItemNo", DbType.Int32, ARItemNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AcNo", DbType.String, accountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContactNo", DbType.String, contractNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContactExp", DbType.String, contractExp));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastContact", DbType.String, lastContract));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractKid", DbType.String, contractKid));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AIGStatus", DbType.String, statusMen));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InsNo", DbType.String, insNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InsKID", DbType.String, insKID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TBBS", DbType.String, transmissionNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Balance", DbType.Decimal, IsValidDecimal(balance)));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DueBalance", DbType.Decimal, IsValidDecimal(dueBalance)));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastReminder", DbType.String, lastReminder));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CollectingStatus", DbType.String, collectingStutas));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CollectingText", DbType.String, collectingtext));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchNo", DbType.String, branchNo));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@nameoncontract", DbType.String, nameOncontract));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastvisit", DbType.String, lastvisit));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@printdate", DbType.String, printDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@oreginaldue", DbType.String, OriDueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@reminderfee", DbType.String, reminderFee));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceAmount", DbType.String, invoiceAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceRef", DbType.String, InvoiceRef));//@Group
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Group", DbType.String, _group));
                DDNo = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                DDNo = -1;
                US_DataAccess.core.Exception.USPException exp = new US_DataAccess.core.Exception.USPException();
                
                exp.ErrorMessage = ex.Message;
                throw exp;
            }
            return DDNo;

        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            try
            {
                string storedProcedureName = "dbo.USP_AddARItemDetails";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Transaction = transaction;
                cmd.Connection = transaction.Connection;

                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@addressNo", DbType.Int32, accountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ARItemNo", DbType.Int32, ARItemNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AcNo", DbType.String, accountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContactNo", DbType.String, contractNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContactExp", DbType.String, contractExp));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastContact", DbType.String, lastContract));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractKid", DbType.String, contractKid));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AIGStatus", DbType.String, statusMen));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InsNo", DbType.String, insNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InsKID", DbType.String, insKID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TBBS", DbType.String, transmissionNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Balance", DbType.Decimal, IsValidDecimal(balance)));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DueBalance", DbType.Decimal, IsValidDecimal(dueBalance)));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastReminder", DbType.String, lastReminder));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CollectingStatus", DbType.String, collectingStutas));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CollectingText", DbType.String, collectingtext));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchNo", DbType.String, branchNo));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@nameoncontract", DbType.String, nameOncontract));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastvisit", DbType.String, lastvisit));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@printdate", DbType.String, printDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@oreginaldue", DbType.String, OriDueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@reminderfee", DbType.String, reminderFee));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceAmount", DbType.String, invoiceAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceRef", DbType.String, InvoiceRef));//@Group
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Group", DbType.String, _group));
                DDNo = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception("Error in adding aritem details " + ex.Message, ex);
            }
            return DDNo;
        }


        public static decimal IsValidDecimal(string val)
        {
            string tempval = string.Empty;
            string currentCul = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
            if (currentCul.Equals("nb-NO"))
            {
                tempval = val.Replace(".", ",");
            }
            else
            {
                tempval = val;
            }
            try
            {
                decimal decVal = decimal.Parse(tempval);
                return decVal;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        #region IDirectDBAccess<int> Members

        public int ExecuteCommand(DbConnection con)
        {
            connection = con;
            return Body(connection);
        }

        #endregion
    }
}
