﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetTransactionProfileAction
    {
        private IUSPTransaction _transactionInfo;
        public GetTransactionProfileAction(IUSPTransaction transaction)
        {
            _transactionInfo = transaction;
        }
        public PaymentProcessStepResult RunOnTranaction(DbTransaction transaction)
        {
            PaymentProcessStepResult result = new PaymentProcessStepResult();
            result.ResultStatus = true;
            result.Transaction = _transactionInfo;
            DbDataReader dataReader = null;
            result.Transaction.TransactionProfile = new TransactionProfile();
            result.StepName = "Get Transaction Profile";
            try
            {
                DbConnection connection = transaction.Connection;
                DbCommand command = connection.CreateCommand();
                command.Transaction = transaction;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandText = "USP_GetTransactionProfileByCreditorNo";
                if (_transactionInfo.CaseNo > 0)
                    command.Parameters.Add(DataAcessUtils.CreateParam("@CaseNo", DbType.Int32, _transactionInfo.CaseNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CreditorId", DbType.String, _transactionInfo.PID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CategoryName", DbType.String, "BasicData"));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BMDName", DbType.String, "TransactionProfiles"));
                command.Parameters.Add(DataAcessUtils.CreateParam("@DisplayKeyName", DbType.String, "Standard"));
                dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    result.Transaction.TransactionProfile.Id = int.Parse(dataReader["ID"].ToString());
                    result.Transaction.TransactionProfile.Name = dataReader["Name"].ToString();
                    result.Transaction.TransactionProfile.DisplayName = dataReader["DisplayName"].ToString();
                    result.Transaction.TransactionProfile.SaveTransactionSp = dataReader["TransactionSaveSP"].ToString();
                    result.Transaction.TransactionProfile.FindDestinationSp = dataReader["FindDestinationSP"].ToString();
                    result.Transaction.TransactionProfile.DistributeTransactionSp = dataReader["TransactionDistributeSP"].ToString();
                }
                else
                {
                    throw new Exception("There is no Transaction Profile configured.");
                }

            }
            catch (Exception ex)
            {
                result.ResultStatus = false;
                result.MessageList.Add("Get Transaction Profile. Error - " + ex.Message);
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return result;
        }
    }
}
