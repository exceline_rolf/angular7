﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class AddCreditInvoiceAction : USDBActionBase<string>
    {
        private int _creditorNo = -1;
        private int _orderNo = -1;
        private int _coRelationID = -1;

        public AddCreditInvoiceAction(int CreditorNo, int OrderNo, int coRelationID)
        {
            _creditorNo = CreditorNo;
            _orderNo = OrderNo;
            _coRelationID = coRelationID;
        }

        protected override string Body(System.Data.Common.DbConnection connection)
        {
            return string.Empty;
        }

        public string RunOnTransaction(DbTransaction transaction)
        {
            string spName = "USExceGMS_INV_GetInvoiceDetails";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Transaction = transaction;
                command.Connection = transaction.Connection;
                command.Parameters.Add(DataAcessUtils.CreateParam("@creditorNo", System.Data.DbType.Int32, _creditorNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@orderNo", System.Data.DbType.Int32, _orderNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentID", System.Data.DbType.Int32, _coRelationID));
                SqlParameter outpara = new SqlParameter();
                outpara.ParameterName = "@OutputInvoiceNo";
                outpara.Direction = System.Data.ParameterDirection.Output;
                outpara.SqlDbType = System.Data.SqlDbType.Int;
                command.Parameters.Add(outpara);
                command.ExecuteNonQuery();
                return Convert.ToString(outpara.Value).Trim();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
