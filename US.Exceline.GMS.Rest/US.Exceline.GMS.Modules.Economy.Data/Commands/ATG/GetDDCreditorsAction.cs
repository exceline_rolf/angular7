﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.ATG
{
    public class GetDDCreditorsAction : USDBActionBase<List<Creditor>>
    {

        private int directSt;
        private int _branchID = -1;
        private DateTime _startDate;
        private DateTime _endDate;

        public GetDDCreditorsAction(int status, int branchID, DateTime startDate, DateTime endDate)
        {
            this.directSt = status;
            _branchID = branchID;
            _startDate = startDate;
            _endDate = endDate;
        }

        protected override List<Creditor> Body(System.Data.Common.DbConnection connection)
        {
            List<Creditor> ddList = new List<Creditor>();
            try
            {
                string storedProcedureName = "dbo.USP_GetCreditorListWithDD";

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@creSt", DbType.Int32, directSt));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDueDate", DbType.Date, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDueDate", DbType.Date, _endDate));
                DbDataReader ddReader = cmd.ExecuteReader();

                while (ddReader.Read())
                {
                    Creditor cre = new Creditor();
                    cre.EntID = Convert.ToString(ddReader["EntNo"]).Trim();           
                    cre.Name = Convert.ToString(ddReader["Name"]).Trim();            
                    cre.AccountNo = Convert.ToString(ddReader["Accountno"]).Trim();  
                    cre.Notify = (ddReader.GetInt32(3) == -1) ? "0" : (Convert.ToString(ddReader["NotifyBank"]).Trim());
                    ddList.Add(cre);
                }

                return ddList;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
