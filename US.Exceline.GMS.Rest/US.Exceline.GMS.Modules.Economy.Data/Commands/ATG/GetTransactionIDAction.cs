﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.ATG
{
    public class GetTransactionIDAction : USDBActionBase<string>
    {
        protected override string Body(System.Data.Common.DbConnection connection)
        {
            string newID = string.Empty;
            try
            {
                string storedProcedureName = "dbo.USP_GetTransNo";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                newID = Convert.ToString(cmd.ExecuteScalar());
            }
            catch (Exception)
            {
                throw;
            }
            return newID;
        }
    }
}
