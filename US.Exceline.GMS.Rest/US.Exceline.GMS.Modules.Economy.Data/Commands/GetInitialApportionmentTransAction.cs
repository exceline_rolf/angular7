﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetInitialApportionmentTransAction : USDBActionBase<List<USPApportionmentTransaction>>
    {
        private string _creditorNo = string.Empty;
        private decimal _amount = 0;
        private int _caseNo = 0;
        private int _arNo = 0;
        private int _lastRowID = 0;
        private int _paymentID = -1;
        private string _paymentType = string.Empty;

        public GetInitialApportionmentTransAction(string creditorNo, decimal amount, int caseNo, int arNo, int lastRowID, int paymentID, string paymentType)
        {
            _amount = amount;
            _creditorNo = creditorNo;
            _caseNo = caseNo;
            _arNo = arNo;
            _lastRowID = lastRowID;
            _paymentID = paymentID;
            _paymentType = paymentType;
        }
        protected override List<USPApportionmentTransaction> Body(System.Data.Common.DbConnection connection)
        {
            List<USPApportionmentTransaction> transactionList = new List<USPApportionmentTransaction>();
            DbDataReader reader = null;
            try
            {
                //to do
                // profile sp should get from bmd
                DbCommand comm = connection.CreateCommand();
                comm.CommandText = "USP_APRN_Profile_CreateStandardApportionmentProposal";
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.CommandTimeout = int.MaxValue;
                comm.Parameters.Add(new SqlParameter("@creditorNo", _creditorNo));
                comm.Parameters.Add(new SqlParameter("@amount", _amount));
                comm.Parameters.Add(new SqlParameter("@subCaseNo", _caseNo));
                comm.Parameters.Add(new SqlParameter("@ARNo", _arNo));
                comm.Parameters.Add(new SqlParameter("@paymentID", _paymentID)); //No need of payment ID for proposal
                comm.Parameters.Add(new SqlParameter("@paymentType", _paymentType));

                reader = comm.ExecuteReader();
                while (reader.Read())
                {

                    USPApportionmentTransaction trans = new USPApportionmentTransaction();
                    trans.ArNo = Convert.ToInt32(reader["ArNo"]);
                    trans.ArItemNo = Convert.ToInt32(reader["ArItemNo"]);
                    if (reader["PaymentId"] != DBNull.Value)
                        trans.PaymentID = Convert.ToInt32(reader["PaymentId"]);
                    trans.Amount = Convert.ToDecimal(reader["Amount"]);
                    trans.CreditorSum = Convert.ToDecimal(reader["CreditorSum"]);
                    trans.BureauSum = Convert.ToDecimal(reader["BureauSum"]);
                    if (reader["ProfileID"] != DBNull.Value)
                        trans.ProfileId = Convert.ToInt32(reader["ProfileID"]);
                    trans.RegDate = Convert.ToDateTime(reader["RegDate"]);
                    trans.CreatedUser = reader["CreatedUser"].ToString();
                    trans.Balance = Convert.ToDecimal(reader["Balance"]);
                    trans.Text = reader["Text"].ToString();
                    trans.InvoiceNo = reader["Ref"].ToString();
                    trans.CaseNo = Convert.ToInt32(reader["ParentID"].ToString());
                    trans.SubCaseNo = Convert.ToInt32(reader["CaseNo"].ToString());
                    trans.TotalBalance = Convert.ToDecimal(reader["TotalBalance"]);
                    trans.ItemTypeId = Convert.ToInt32(reader["ItemTypeID"]);
                    trans.KID = reader["KID"].ToString();
                    trans.DebitorSum = Convert.ToDecimal(reader["DebtorSum"]);
                    if (reader["CreditorNo"] != DBNull.Value)
                    {
                        trans.CreditorNo = Convert.ToString(reader["CreditorNo"]);
                    }
                    if (reader["VATAmount"] != DBNull.Value)
                    {
                        trans.VATAmount = Convert.ToDecimal(reader["VATAmount"]);
                    }
                    if (reader["VATBasis"] != DBNull.Value)
                    {
                        trans.VATBasis = Convert.ToDecimal(reader["VATBasis"]);
                    }
                    if (reader["VATPaidByDebtor"] != DBNull.Value)
                    {
                        trans.VATPaidByDebitor = Convert.ToDecimal(reader["VATPaidByDebtor"]);
                    }
                    if (reader["VATDeducted"] != DBNull.Value)
                    {
                        trans.VATDeducted = Convert.ToDecimal(reader["VATDeducted"]);
                    }
                    if (reader["VATCode"] != DBNull.Value)
                    {
                        trans.VATCode = Convert.ToInt32(reader["VATCode"]);
                    }
                    trans.RowNo = _lastRowID;
                    _lastRowID++;
                    transactionList.Add(trans);
                }
            }
            catch (Exception)
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                throw;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return transactionList;
        }
    }
}
