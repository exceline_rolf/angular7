﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class UpdateBulkPDFPrintDetailsAction : USDBActionBase<int>
    {
        private readonly int _id = -1;
        private readonly string _docType = string.Empty;
        private readonly string _dataIdList = string.Empty;
        private readonly string _pdfFilePath = string.Empty;
        private readonly int _noOfDoc = -1;
        private readonly int _failCount = -1;
        private readonly int _branchId = -1;
        private readonly string _user = string.Empty;

        public UpdateBulkPDFPrintDetailsAction(int id, string docType, string dataIdList, string pdfFilePath, int noOfDoc, int failCount, int branchId, string user)
        {
            _id = id;
            _docType = docType;
            _dataIdList = dataIdList;
            _pdfFilePath = pdfFilePath;
            _noOfDoc = noOfDoc;
            _failCount = failCount;
            _branchId = branchId;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSUpdateBulkPDFPrint";
            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@id", DbType.Int32, _id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@docType", DbType.String, _docType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@dataIdList", DbType.String, _dataIdList));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@pdfFilePath", DbType.String, _pdfFilePath));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@noOfDocument", DbType.Int32, _noOfDoc));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@failCount", DbType.Int32, _failCount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                DbParameter para = new SqlParameter();
                para.DbType = DbType.Int32;
                para.ParameterName = "@outId";
                para.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para);

                cmd.ExecuteNonQuery();
                return Convert.ToInt32(para.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
