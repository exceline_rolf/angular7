﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class AddNotApportionmentTransationAndDataAction : USDBActionBase<int>
    {
        //private int _arNo = -1;
        //private string _kid = string.Empty;
        //private string _creditorNo = string.Empty;
        //private decimal _mainAmount = 0;
        //private int _itemTypeID = -1;
        //private int _errorPaymentID = 0;
        private string _user = string.Empty;
        private USPApportionmentTransaction _transaction;
        private DbTransaction _dbTransaction = null;

        public AddNotApportionmentTransationAndDataAction(int ARNo, string KID, int errorPaymentID, string creditoNo, decimal mainAmount, int itemTypeID, USPApportionmentTransaction transaction, string user)
        {
            _transaction = transaction;
            _user = user;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int _result = -1;

            try
            {
                _dbTransaction = connection.BeginTransaction();


                int itemType = (_transaction.ItemTypeId != 25) ? _transaction.ItemTypeId : 15;
                DbCommand cmd = _dbTransaction.Connection.CreateCommand();
                cmd.Transaction = _dbTransaction;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USP_AddNotApportionTransactionAndData";

                // Common Param
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ARNo", DbType.Int32, _transaction.ArNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditorNumber", DbType.String, _transaction.CreditorNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ErrorPaymentId", DbType.Int32, _transaction.PaymentID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ARItemNo", DbType.Int32, _transaction.ArItemNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ItemTypeId", DbType.Int32, itemType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Amount", DbType.Decimal, _transaction.Amount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PaymentID", DbType.Int32, _transaction.PaymentID));
                // AR Item Param
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@KID", DbType.String, _transaction.KID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CaseNo", DbType.Int32, _transaction.SubCaseNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Ref", DbType.String, _transaction.InvoiceNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Txt", DbType.String, _transaction.Text));
                // Apportion Param
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditorSum", DbType.Decimal, _transaction.CreditorSum));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BureauSum", DbType.Decimal, _transaction.BureauSum));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DebitorSum", DbType.Decimal, _transaction.DebitorSum));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ProfileID", DbType.Int32, _transaction.ProfileId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDeleted", DbType.Int32, 0));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VATAmount", DbType.Decimal, _transaction.VATAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VATBasis", DbType.Decimal, _transaction.VATBasis));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VATPaidByDebtor", DbType.Decimal, _transaction.VATPaidByDebitor));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VATDeducted", DbType.Decimal, _transaction.VATDeducted));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VATCode", DbType.Int32, _transaction.VATCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AppotionID", DbType.Int32, _transaction.ApportionID));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OutPutId", DbType.Int32, 0));

                if (_transaction.PaymentVoucherNo != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@PaymentVoucherNo", DbType.Int32, _transaction.PaymentVoucherNo));
               
                _result = Convert.ToInt32(cmd.ExecuteScalar());
                cmd.Parameters.Clear();


                if (_transaction.PaymentID > 0)
                {
                    UpdateErrorPaymentAction commandExecuter = new UpdateErrorPaymentAction(_transaction.PaymentID, _transaction.ErrorType, _transaction.ExceedPaymentApportionId, _transaction.ExceedPaymentAritemNo);
                    commandExecuter.RunOnTransation(_dbTransaction);
                }

                _dbTransaction.Commit();
                _result = 1;

            }
            catch (Exception)
            {
                _dbTransaction.Rollback();
                throw;
            }
            return _result;
        }
    }
}
