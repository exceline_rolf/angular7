﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetSponsorOrderLinesAction : USDBActionBase<List<IUSPOrderLine>>
    {
        private int _orderNo = -1;

        public GetSponsorOrderLinesAction(int orderNo)
        {
            _orderNo = orderNo;
        }

        protected override List<IUSPOrderLine> Body(System.Data.Common.DbConnection connection)
        {
            return new List<IUSPOrderLine>();
        }

        public List<IUSPOrderLine> RunOnTransaction(DbTransaction transaction)
        {
            DbDataReader reader = null;
            List<IUSPOrderLine> OrderLineList = new List<IUSPOrderLine>();
            try
            {
                string storedProcedureName = "dbo.USP_GetSponsorOrderLines";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Transaction = transaction;
                cmd.Connection = transaction.Connection;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OrderNo", DbType.Int32, _orderNo));
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    IUSPOrderLine orderLine = new USPOrderLine();
                    orderLine.Address = Convert.ToString(reader["Address"]).Trim();
                    orderLine.Amount = Convert.ToString(reader["Balance"]).Trim().Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator).Replace(",", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                    orderLine.AmountThisOrderLines = Convert.ToString(reader["Amount"]).Trim().Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator).Replace(",", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                    orderLine.CustId = Convert.ToString(reader["CustId"]).Trim();
                    orderLine.DiscountAmount = Convert.ToString(reader["Discount"]).Trim().Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator).Replace(",", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                    orderLine.EmployeeNo = Convert.ToString(reader["EmployeeNo"]).Trim();
                    orderLine.FirstName = Convert.ToString(reader["FirstName"]).Trim();
                    orderLine.LastName = Convert.ToString(reader["LastName"]).Trim();
                    orderLine.InkassoId = Convert.ToString(reader["CreditorNo"]).Trim();
                    orderLine.LastVisit = Convert.ToString(reader["LastVisitDate"]).Trim();
                    orderLine.NameOncontract = Convert.ToString(reader["NameOnContract"]).Trim();
                    orderLine.PostPlace = Convert.ToString(reader["ZipName"]).Trim();
                    orderLine.ZipCode = Convert.ToString(reader["ZipCode"]).Trim();
                    orderLine.Visit = Convert.ToInt32(reader["NoOfVisits"]);
                    orderLine.SponsorRef = Convert.ToString(reader["SponsorReference"]);
                    orderLine.SourceInstallmentID = Convert.ToInt32(reader["SourceInstallmentID"]);
                    OrderLineList.Add(orderLine);
                }

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            catch (Exception)
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                throw;
            }
            return OrderLineList;
        }
    }
}
