﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.Exceline.GMS.Modules.Economy.Core.Utills;
using US.Exceline.GMS.Modules.Economy.Data.Commands;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.Data
{
    internal abstract class ClaimHandler
    {
        private List<string> _inkassoIds = new List<string>();

        internal abstract OperationResult SaveClaim(DbTransaction transaction, IUSPClaim claim, int arItemNo);

        public OperationResult SaveClaim(DbTransaction transaction, IUSPClaim claim, int fileId, string fileName, string userName)
        {
            // ## Check Creditor
            if (IsCreditorAvailableByInkassoID(transaction, claim.Creditor.CreditorInkassoID))
            {
                // ## Save debtor. Debtor will be added if the debtor is not exist
                Debitor savedDebtor = AddDebitor(transaction, claim.Debtor, claim.Creditor.CreditorInkassoID, claim.InvoiceNumber);

                if (savedDebtor != null)
                {
                    int arItemNo = -1;
                    int savedArItemNo = -1;
                    //Check availability of invoice
                    if (!IsInvoiceAvailable(transaction, claim.Creditor.CreditorInkassoID, claim.Debtor.DebtorcustumerID, claim.InvoiceNumber, ref arItemNo))
                    {
                        AddARItemWithDetails(transaction, claim, savedDebtor, ref savedArItemNo, userName);
                        return new OperationResult() { TagSavedID = savedArItemNo };//todo add more specific details
                    }
                    else
                    {
                       
                        throw new Exception("Duplicate invoice found", null);
                    }
                }
                else
                {
                    throw new Exception("Failed to add the debtor with customer ID[" + claim.Debtor.DebtorcustumerID + "] under credtor with Inkasso ID [" + claim.Creditor.CreditorInkassoID + "]", null);
                }
            }
            else
            {
                throw new Exception("Could not find creditor with given inkassoID[" + claim.Creditor.CreditorInkassoID + "]", null);
            }
        }

        private void AddARItemWithDetails(DbTransaction transaction, IUSPClaim claim, Debitor savedDebtor, ref int savedARItemNo, string userName)
        {
            try
            {
                claim.Debtor.DebtorEntityRoleID = savedDebtor.EntRoleID;
                savedARItemNo = SaveInvoiceToDatabase(transaction, claim, userName);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to add invoice " + ex.Message, ex);
            }

            try
            {
                // ## Save AR Items Details
                SaveARItemDetails(transaction,claim, savedARItemNo);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save AR Item Details " + ex.Message, ex);
            }
           

            // ## Handle the claim based on the type(DD,DW,IP,etc) and return Import Result(USImportResult)
            // ## Following method is implemented in responsible child class
            SaveClaim(transaction, claim, savedARItemNo);
           // addingResult.TagSavedRecordID = savedARItemNo;
        }

        public DateTime StringDateToDate(string date)
        {

            string[] parts = date.Split(new char[] { Constents.DATE_SEPERATOR_DASH });
            try
            {
                int year = int.Parse(parts[0]);
                int month = int.Parse(parts[1]);
                int day = int.Parse(parts[2]);
                return new DateTime(year, month, day);
            }
            catch
            {
                parts = date.Split(new char[] { Constents.DATE_SEPERATOR_BLACK_SLASH });
                try
                {
                    int year = int.Parse(parts[0]);
                    int month = int.Parse(parts[1]);
                    int day = int.Parse(parts[2]);
                    return new DateTime(year, month, day);
                }
                catch
                {
                    parts = date.Split(new char[] { Constents.DATE_SEPERATOR_DOT });
                    try
                    {
                        int year = int.Parse(parts[0]);
                        int month = int.Parse(parts[1]);
                        int day = int.Parse(parts[2]);
                        return new DateTime(year, month, day);

                    }
                    catch (Exception)
                    {

                        throw new Exception("Date is not in correct format. DW date conversion");
                    }

                }
            }
        }


        /// <summary>
        /// Save Debtor. If the debtor is not exist debtor will be added(existance check in SP)
        /// </summary>
        /// <param name="debtor">Debtor</param>
        /// <param name="creditorInkassoId">Inkasso ID of creditor which belongs the debtor</param>
        /// <param name="invoiceNumber">Invoice number. This is required when adding EntRole for the debtor</param>
        /// <returns>true if the operation succeeded</returns>
        public Debitor AddDebitor(DbTransaction transaction, IUSPDebtor debtor, string creditorInkassoId, string invoiceNumber)
        {
            try
            {
                Debitor adebitor = new Debitor
                {
                    Born = debtor.DebtorBirthDay,
                    FirstName = debtor.DebtorFirstName,
                    LastName = debtor.DebtorSecondName,
                    PersonNo = debtor.DebtorPersonNo,
                    cid = debtor.DebtorcustumerID,
                    IncassoId = creditorInkassoId,
                    RoleId = "DEB",
                    Reference = invoiceNumber,
                    EntID = debtor.DebtorEntityID
                };

                CAddress addr = new CAddress();
                if (debtor.DebtorAddressList != null && debtor.DebtorAddressList.Count > 0)
                {
                    addr.Address1 = debtor.DebtorAddressList[0].Address1;
                    addr.Address2 = debtor.DebtorAddressList[0].Address2;
                    addr.Address3 = debtor.DebtorAddressList[0].Address3;
                    addr.AddSource = debtor.DebtorAddressList[0].AddSource;
                    addr.CountryCode = debtor.DebtorAddressList[0].CountryCode;
                    addr.Zip = debtor.DebtorAddressList[0].ZipCode;
                    addr.ZipName = debtor.DebtorAddressList[0].City;
                    addr.TelHome = debtor.DebtorAddressList[0].TelHome;
                    addr.TelMobile = debtor.DebtorAddressList[0].TelMobile;
                    addr.TelWork = debtor.DebtorAddressList[0].TelWork;
                    addr.Email = debtor.DebtorAddressList[0].Email;
                    addr.Fax = debtor.DebtorAddressList[0].Fax;
                    addr.MSN = debtor.DebtorAddressList[0].MSN;
                    addr.SMS = debtor.DebtorAddressList[0].SMS;
                    addr.Skype = debtor.DebtorAddressList[0].Skype;
                }

                // Empty string should be filled
                InsertDebitorAction debitInsertAction = new InsertDebitorAction(adebitor, addr);
                adebitor = debitInsertAction.RunOnTransaction(transaction);

                if (adebitor == null)
                {
                    return null;
                }
                return adebitor;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in adding debitor " + ex.Message, null);
            }
        }
        public bool IsCreditorAvailableByInkassoID(DbTransaction transaction, string creditorInkassoId)//,ref USImportResult<bool> operationResult)
        {
            try
            {
                if (_inkassoIds.Contains(creditorInkassoId))
                {
                    return true;
                }
                else
                {
                    GetCreditorByInkassoID action = new GetCreditorByInkassoID(Convert.ToInt32(creditorInkassoId));
                    if (action.RunOnTransaction(transaction) == null)
                    {
                        return false;
                    }
                    else
                    {
                        _inkassoIds.Add(creditorInkassoId);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsInvoiceAvailable(DbTransaction transaction, string creditor, string debtor, string invoiceNumber, ref int arItemNo)
        {
            ReadARItem action = new ReadARItem(Convert.ToInt32(creditor), debtor, invoiceNumber);
            arItemNo = action.RunOnTransaction(transaction);

            if (arItemNo > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        private int SaveARItemDetails(DbTransaction transaction, IUSPClaim claim, int arItemNo)
        {
            var directDeduct = new DirectDeduct
            {
                AccountNo = claim.AccountNumber,
                Balance = claim.Balance,
                BranchNo = claim.BranchNumber,
                CollectingStatus = (CollectingStatus)(int)claim.CollectingStatus,
                CollectingText = claim.InkassoTest,
                ContractExpire = claim.ContractExpire,
                ContractKID = claim.ContractKID,
                ContractNo = claim.ContractNumber,
                DueBalance = claim.DueBalance,
                InstallmentKID = claim.KID,
                InstallmentNo = claim.InstallmentNumber,
                LastContract = claim.LastContract,
                LastReminder = claim.LastReminder,
                StatusMendatory = (DirectDeductStatus)(int)claim.Status,
                TrancemissionNo = claim.TransmissionNumberBBS,
                InvoiceAmount = claim.InvoiceCharge.ToString(),
                nameInContract = claim.NameInContract,
                lastVisit = claim.LastVisit,
                printDate = claim.PrintDate,
                originalDue = claim.OriginalDueDate,
                reminderfee = claim.ReminderFee.ToString(),
                InvoiceRef = claim.InvoiceRef,
                Group = claim.Group
            };

            // Empty string should be filled
            // save record
            int detailsId = SaveARItemDetails(transaction, directDeduct, arItemNo);
            //entNo = DebitorManager.AddDebitor(adebitor, addr, DebitorManager.IsDebtorAvailable(inkassoID, custID));
            if (detailsId < 0)
                throw new Exception("Failed save claim details ", null);
            return detailsId;
        }

        public int SaveARItemDetails(DbTransaction transaction, DirectDeduct directDeduct, int ARItemNo)
        {
            AddArItemDetails action = new AddArItemDetails(directDeduct, ARItemNo);
            return action.RunOnTransaction(transaction);
        }

        /// <summary>
        /// Add claim (to ARItem)
        /// </summary>
        /// <param name="claim"></param>
        /// <param name="userName"></param>
        /// <returns>ARItem No of saved ARItem record</returns>
        private int SaveInvoiceToDatabase(DbTransaction transaction, IUSPClaim claim, string userName)
        {
            var arToBeSaved = new AR
            {
                CID = claim.Debtor.DebtorcustumerID,
                Name = string.Empty,
                TransferDate = claim.TransferDate.ToString("MM-dd-yyyy")
            };

            var arItem = new ARItem
            {
                Text = claim.Text,
                DueDate = claim.DueDate,
                Amount = claim.Amount,
                InvoiceNumber = claim.InvoiceNumber,
                InvoicedDate = claim.InvoicedDate,
                ARNo = arToBeSaved.ARNo,
                InvoicePath = claim.InvoicePath,
                PrintDate = claim.InvoicePrintDate,
                IsPrinted = claim.IsPrinted,
                itemType = InvoiceTypeConverter.ConvertInvoiceTypeToItemTypeID(claim.InvoiceType),
                KID = claim.KID,
                payDate = claim.PaidDate,
                ContractKID = claim.ContractKID
            };

            if (claim.TransferDate > DateTime.MinValue)
            {
                arItem.TransferDate = claim.TransferDate.ToString("MM-dd-yyyy");
            }

            const int addressNo = 1; // In old USP this "addressNo" always was "1"(It was hard coded). This is used only as a reference in a database table
            // This addressNo is not making any scence

            const int parentArNo = -1; // In old USP(ExlineCliamImportTask) this "parentARNo" always was "-1"(It was hard coded).
            // save record
            int savedArNo = SaveARWithinTransaction(transaction,arToBeSaved, arItem, claim.Creditor.CreditorInkassoID, claim.Debtor.DebtorEntityRoleID, addressNo, parentArNo, userName);
            if (savedArNo <= 0)
                throw new Exception("Could not retrivie valid AR Number for save AR Records", null);
            return savedArNo;
        }

        public int SaveARWithinTransaction(DbTransaction transaction, AR arToBeSaved, ARItem arItem, string creditor, int eRole, int addressNo, int parentARNo, string userName)
        {
            SaveARComplete action = new SaveARComplete(arToBeSaved, arItem, creditor, eRole, addressNo, parentARNo, userName);
            return action.RunOnTransaction(transaction);
        }

    }
}
