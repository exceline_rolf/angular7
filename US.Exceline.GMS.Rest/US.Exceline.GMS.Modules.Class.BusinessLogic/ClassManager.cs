﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Class.Data.DataAdapters;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;
using System.Globalization;
using System.IO;


namespace US.Exceline.GMS.Modules.Class.BusinessLogic
{
    public class ClassManager
    {
        public static  OperationResult<List<ExcelineClassDC>> SearchClass(string category, string searchText, string gymCode)
        {
            OperationResult<List<ExcelineClassDC>> result = new OperationResult<List<ExcelineClassDC>>();
            try
            {
                result.OperationReturnValue = ClassFacade.SearchClass(category, searchText, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Searching Classes" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<List<ExcelineClassDC>> GetClasses(string className, int branchId, string gymCode)
        {
            OperationResult<List<ExcelineClassDC>> result = new OperationResult<List<ExcelineClassDC>>();
            try
            {
                result.OperationReturnValue = ClassFacade.GetClasses(className, branchId, gymCode);
            }

            catch(Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Classes" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ScheduleItemDC>> GetScheduleItemsByClass(int classId, int branchId, string gymCode)
        {
            OperationResult<List<ScheduleItemDC>> result = new OperationResult<List<ScheduleItemDC>>();
            try
            {
                result.OperationReturnValue = ClassFacade.GetScheduleItemsByClass(classId, branchId, gymCode);
            }

            catch(Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting ScheduleItems" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ScheduleItemDC>> GetScheduleItemsByClassIds(List<int> classIds, int branchId, string gymCode)
        {
            OperationResult<List<ScheduleItemDC>> result = new OperationResult<List<ScheduleItemDC>>();
            try
            {
                result.OperationReturnValue = ClassFacade.GetScheduleItemsByClassIds(classIds, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting ScheduleItems" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<List<ExcelineClassDC>> SearchClass(string category, string searchText, DateTime startDate, string gymCode)
        {
            OperationResult<List<ExcelineClassDC>> result = new OperationResult<List<ExcelineClassDC>>();
            try
            {
                result.OperationReturnValue = ClassFacade.SearchClass(category, searchText, startDate, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Searching Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<List<ExcelineClassDC>> SearchClass(string category, string searchText, DateTime startDate, DateTime endDate, string gymCode)
        {
            OperationResult<List<ExcelineClassDC>> result = new OperationResult<List<ExcelineClassDC>>();
            try
            {
                result.OperationReturnValue = ClassFacade.SearchClass(category, searchText, startDate, endDate, gymCode);

            }catch(Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Searching Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<string> SaveClass(ExcelineClassDC excelineClass, int branchId, string gymCode, string user, string culture)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                if (excelineClass.Schedule.SheduleItemList != null)
                {
                    foreach (ScheduleItemDC item in excelineClass.Schedule.SheduleItemList)
                    {
                        //DateTime StartDate = item.StartDate;
                        //DateTime EndDate = DateTime.Now.Date.AddMonths(1);
                        //if (StartDate.Date < DateTime.Now.Date)
                        //{
                        //    StartDate = DateTime.Now.Date;
                        //}
                        //if (EndDate > item.EndDate.Date)
                        //{
                        //    EndDate = item.EndDate.Date;
                        //}
                        //item.ActiveTimes.Clear();
                        //item.ActiveTimes = GetWeekActiveTimes(item, StartDate, EndDate, culture);


                        DateTime StartDate = item.StartDate;
                        DateTime EndDate = item.EndDate;
                        if (StartDate.Date <= DateTime.Now.Date)
                            StartDate = DateTime.Now.Date.AddDays(1);

                        item.ActiveTimes.Clear();
                        if (excelineClass.IsCopyClass)
                        {
                            StartDate = excelineClass.Schedule.StartDate;
                            EndDate = excelineClass.Schedule.EndDate ?? excelineClass.Schedule.StartDate;
                        }
                        item.ActiveTimes = GetWeekActiveTimes(item, StartDate, EndDate, culture);
                    }
                }
                result.OperationReturnValue = ClassFacade.SaveClass(excelineClass, branchId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Saving Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<bool> UpdateClass(ExcelineClassDC excelineClass, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ClassFacade.UpdateClass(excelineClass, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Updateing Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<bool> DeleteClass(int classId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ClassFacade.DeleteClass(classId, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Deleting Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }       

        public static  OperationResult<int> GetNextClassId(string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ClassFacade.GetNextClassId(gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Next ClassId" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineClassActiveTimeDC>> GetExcelineClassActiveTimes(int branchId, DateTime startTime, DateTime endTime, int entNO, string gymCode)
        {
            OperationResult<List<ExcelineClassActiveTimeDC>> result = new OperationResult<List<ExcelineClassActiveTimeDC>>();
            try
            {
                result.OperationReturnValue = ClassFacade.GetExcelineClassActiveTimes(branchId, startTime, endTime, entNO, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Getting classes Active Times " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<List<InstructorDC>> GetInstructorsForClass(int classId, string gymCode)
        {
            OperationResult<List<InstructorDC>> result = new OperationResult<List<InstructorDC>>();
            try
            {
                result.OperationReturnValue = ClassFacade.GetInstructorsForClass(classId, gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Instructors For Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static  OperationResult<List<ResourceDC>> GetResourcesForClass(int classId, string gymCode)
        {
            OperationResult<List<ResourceDC>> result = new OperationResult<List<ResourceDC>>();
            try
            {
                result.OperationReturnValue = ClassFacade.GetresourcesForClass(classId, gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Resources For Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineClassMemberDC>> GetMembersByActiveTimeId(int branchId, int activeTimeId, string gymCode)
        {
            OperationResult<List<ExcelineClassMemberDC>> result = new OperationResult<List<ExcelineClassMemberDC>>();
            try
            {
                result.OperationReturnValue = ClassFacade.GetMembersByActiveTimeId(branchId,activeTimeId, gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Members For Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        

        public static  OperationResult<ScheduleDC> GetClassSchedule(int classId, string gymCode)
        {
            OperationResult<ScheduleDC> result = new OperationResult<ScheduleDC>();
            try
            {
                result.OperationReturnValue = ClassFacade.GetClassSchedule(classId, gymCode);
            }

            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in  Getting Class Schedule" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        //public static  OperationResult<List<ArticleDC>> GetArticlesForClass(string gymCode)
        //{
        //    OperationResult<List<ArticleDC>> result = new OperationResult<List<ArticleDC>>();
        //    try
        //    {
        //        result.OperationReturnValue = ClassFacade.GetArticlesForClass(gymCode);
        //    }

        //    catch (Exception ex)
        //    {
        //        result.ErrorOccured = true;
        //        result.CreateMessage("Error in  Getting Articles For Class" + ex.Message, MessageTypes.ERROR);
        //    }
        //    return result;
        //}


        public static OperationResult<int> GetExcelineClassIdByName(string className, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ClassFacade.GetExcelineClassIdByName(className, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Getting Exceline ClassId By Name" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> UpdateTimeTableScheduleItems(List<ScheduleItemDC> scheduleItemList, int branchId, string gymCode, string user, string culture)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                foreach(ScheduleItemDC scheduleItem in scheduleItemList)
                {
                    DateTime StartDate = scheduleItem.StartDate;
                    DateTime EndDate = scheduleItem.EndDate;
                    //DateTime EndDate = DateTime.Now.Date.AddMonths(1);

                    if (StartDate.Date <= DateTime.Now.Date)
                    {
                        StartDate = DateTime.Now.Date.AddDays(1);
                    }
                    //if (EndDate > scheduleItem.EndDate.Date)
                    //{
                    //    EndDate = scheduleItem.EndDate.Date;
                    //}
                    scheduleItem.ActiveTimes = GetWeekActiveTimes(scheduleItem, StartDate, EndDate, culture);
                }
                result.OperationReturnValue = ClassFacade.UpdateTimeTableScheduleItems(scheduleItemList, branchId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Updating schedule items" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<int> DeleteScheduleItem(List<int> scheduleItemIdList, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ClassFacade.DeleteScheduleItem(scheduleItemIdList, gymCode, user);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Deleting schedule item" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveActiveTimes(List<ScheduleItemDC> scheduleItemList, DateTime startDate, DateTime endDate, string gymCode, string culture)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                foreach (ScheduleItemDC item in scheduleItemList)
                {
                    item.ActiveTimes.Clear();
                    GenerateActiveTimesForTheGivenPeriod(startDate, endDate, item, culture);
                }
                result.OperationReturnValue = ClassFacade.SaveActiveTimes(scheduleItemList, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Generating ActiveTimes" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> UpdateClassCalendarActiveTime(EntityActiveTimeDC activeTime, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ClassFacade.UpdateClassCalendarActiveTime(activeTime,user,gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Updating Calendar item" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static void GenerateActiveTimesForTheGivenPeriod(DateTime startDate, DateTime endDate, ScheduleItemDC scheduleItem, string culture)
        {
            DateTime activeTimesStartDate = new DateTime();
            DateTime activeTimesEndDate = new DateTime();
            if (scheduleItem.EndDate >= startDate && scheduleItem.StartDate <= endDate)
            {
                if (scheduleItem.StartDate >= startDate && scheduleItem.EndDate <= endDate)
                {
                    activeTimesStartDate = scheduleItem.StartDate;
                    activeTimesEndDate = scheduleItem.EndDate;
                }
                else if (scheduleItem.StartDate < startDate && scheduleItem.EndDate <= endDate)
                {
                    activeTimesStartDate = startDate;
                    activeTimesEndDate = scheduleItem.EndDate;
                }
                else if (scheduleItem.StartDate >= startDate && scheduleItem.EndDate > endDate)
                {
                    activeTimesStartDate = scheduleItem.StartDate;
                    activeTimesEndDate = endDate;
                }
                else
                {
                    activeTimesStartDate = startDate;
                    activeTimesEndDate = endDate;
                }
                scheduleItem.StartDate = activeTimesStartDate;
                scheduleItem.EndDate = activeTimesEndDate;
                scheduleItem.ActiveTimes = GetWeekActiveTimes(scheduleItem, activeTimesStartDate, activeTimesEndDate, culture);
            }
        }

        public static OperationResult<string> CheckActiveTimeOverlapWithClass(ScheduleItemDC scheduleItem,string culture, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                if (scheduleItem.ActiveTimeID <= 0 && (scheduleItem.ActiveTimes == null || !scheduleItem.ActiveTimes.Any()))
                {
                    if (scheduleItem.IsFixed)
                    {
                        scheduleItem.ActiveTimes = GetWeekActiveTimes(scheduleItem, scheduleItem.StartDate.Date, scheduleItem.EndDate.Date, culture);
                    }
                    else //if (scheduleItem.StartDate.Date < DateTime.Now.AddMonths(1).Date)
                    {
                        DateTime StartDate = scheduleItem.StartDate;
                        DateTime EndDate = scheduleItem.EndDate;
                        if (StartDate.Date < DateTime.Now.Date)
                        {
                            StartDate = DateTime.Now.Date;
                        }
                        //if (EndDate > scheduleItem.EndDate.Date)
                        //{
                        //    EndDate = scheduleItem.EndDate.Date;
                        //}
                        scheduleItem.ActiveTimes = GetWeekActiveTimes(scheduleItem, StartDate, EndDate, culture);
                    }  
                } 
                
                result.OperationReturnValue = ClassFacade.CheckActiveTimeOverlapWithClass(scheduleItem, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Check ActiveTime Overlap With Class" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveScheduleItem(ScheduleItemDC scheduleItem, int branchId, string gymCode,string culture, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                if(scheduleItem.IsFixed)
                {
                    scheduleItem.ActiveTimes = GetWeekActiveTimes(scheduleItem, scheduleItem.StartDate.Date, scheduleItem.EndDate.Date, culture);
                }
                else //if (scheduleItem.StartDate.Date < DateTime.Now.AddMonths(1).Date)
                {
                    DateTime StartDate = scheduleItem.StartDate;
                    DateTime EndDate = scheduleItem.EndDate;
                    if (StartDate.Date < DateTime.Now.Date)
                    {
                        StartDate = DateTime.Now.Date;
                    }
                    //if (EndDate > scheduleItem.EndDate.Date)
                    //{
                    //    EndDate = scheduleItem.EndDate.Date;
                    //}
                    scheduleItem.ActiveTimes = GetWeekActiveTimes(scheduleItem, StartDate, EndDate, culture);
                }                
                result.OperationReturnValue = ClassFacade.SaveScheduleItem(scheduleItem, branchId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Saving schedule item" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        private static List<EntityActiveTimeDC> GetWeekActiveTimes(ScheduleItemDC scheduleItem, DateTime StartDate, DateTime EndDate, string culture)
        {

            switch (scheduleItem.Day)
            {
                case "Sunday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Sunday, StartDate, EndDate,culture);

                case "Monday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Monday, StartDate, EndDate, culture);

                case "Tuesday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Tuesday, StartDate, EndDate, culture);

                case "Wednesday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Wednesday, StartDate, EndDate, culture);

                case "Thursday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Thursday, StartDate, EndDate, culture);

                case "Friday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Friday, StartDate, EndDate, culture);

                case "Saturday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Saturday, StartDate, EndDate, culture);

                case "søndag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Sunday, StartDate, EndDate, culture);

                case "mandag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Monday, StartDate, EndDate, culture);

                case "tirsdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Tuesday, StartDate, EndDate, culture);

                case "onsdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Wednesday, StartDate, EndDate, culture);

                case "torsdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Thursday, StartDate, EndDate, culture);

                case "fredag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Friday, StartDate, EndDate, culture);

                case "lørdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Saturday, StartDate, EndDate, culture);

                case "Søndag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Sunday, StartDate, EndDate, culture);

                case "Mandag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Monday, StartDate, EndDate, culture);

                case "Tirsdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Tuesday, StartDate, EndDate, culture);

                case "Onsdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Wednesday, StartDate, EndDate, culture);

                case "Torsdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Thursday, StartDate, EndDate, culture);

                case "Fredag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Friday, StartDate, EndDate, culture);

                case "Lørdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Saturday, StartDate, EndDate, culture);
            }
            return new List<EntityActiveTimeDC>();
        }
        private static List<EntityActiveTimeDC> GenerateActiveTimes(ScheduleItemDC scheduleItem, DayOfWeek dayofWeek, DateTime StartDate, DateTime EndDate, string culture)
        {
            List<EntityActiveTimeDC> activeTimes = new List<EntityActiveTimeDC>();
            
            CultureInfo ci = CultureInfo.CreateSpecificCulture(string.IsNullOrEmpty(culture) ? "nb-NO" : culture);
            TimeSpan duration = EndDate - StartDate;
            DateTime activestartDate = StartDate;

           

            if (scheduleItem.WeekType == 2)
            {
                for (int i = 0; i < duration.Days + 1; i++)
                {
                   
                    if (activestartDate.DayOfWeek == dayofWeek)
                    {
                        EntityActiveTimeDC activeTime = new EntityActiveTimeDC();
                        activeTime.SheduleItemId = scheduleItem.Id;
                        activeTime.StartDate = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.StartTime.ToShortTimeString());
                        activeTime.EndDate = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.EndTime.ToShortTimeString());
                        activeTime.StartDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.StartTime.ToShortTimeString());
                        activeTime.EndDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.EndTime.ToShortTimeString());
                        activeTimes.Add(activeTime);
                    }
                    activestartDate = activestartDate.AddDays(1);
                }
            }
            else
            {

                int durationDays = duration.Days;

                DateTimeFormatInfo dfi = DateTimeFormatInfo.GetInstance(ci);
                Calendar cal = dfi.Calendar;
              //  int weekNumber = cal.GetWeekOfYear(activestartDate, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
              //  int oddEvenStatus = weekNumber % 2;
                //if (oddEvenStatus != scheduleItem.WeekType)
                //{
                //    activestartDate = activestartDate.AddDays(7);
                //    durationDays = duration.Days - 7;
                //}

              
                 
                for (int i = 0; i < durationDays + 1; i++)
                {
                    
                    if (activestartDate.DayOfWeek == dayofWeek)
                    {
                        int localweekNumber = cal.GetWeekOfYear(activestartDate, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
                        if ((localweekNumber % 2) == scheduleItem.WeekType)
                        {
                            EntityActiveTimeDC activeTime = new EntityActiveTimeDC();
                            activeTime.SheduleItemId = scheduleItem.Id;
                            activeTime.StartDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.StartTime.ToShortTimeString());
                            activeTime.EndDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.EndTime.ToShortTimeString());
                            activeTimes.Add(activeTime);
                            if (scheduleItem.WeekType != 2)
                            {
                                activestartDate = activestartDate.AddDays(13);
                                i += 13;
                            }
                        }
                        else
                        {
                            activestartDate = activestartDate.AddDays(6);
                            i += 6;
                        }
                        
                    }
                    activestartDate = activestartDate.AddDays(1);
                }

            }
            return activeTimes;
        }

        public static OperationResult<bool> DeleteSchedule(ExcelineClassDC exceClass, string gymCode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ClassFacade.DeleteSchedule(exceClass, gymCode, user);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Deleting schedule" + ex.Message, MessageTypes.ERROR);
            }
            return result;               
        }

        public static OperationResult<int> UpdateSeasonEndDateWithClassActiveTimes(int branchId, int seasonId, DateTime endDate, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ClassFacade.UpdateSeasonEndDateWithClassActiveTimes(branchId, seasonId, endDate, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error updating season end date" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteClassActiveTime(int actTimeID, string gymCode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ClassFacade.DeleteClassActiveTime(actTimeID, gymCode, user);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error deleting class active time" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> UpdateClassActiveTime(UpdateClassActiveTimeHelperDC helperObj, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ClassFacade.UpdateClassActiveTime(helperObj, gymCode, user);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error updating class active time" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<EntityActiveTimeDC>> GetActiveTimesForClassScheduleItem(int scheduleItemId, string gymCode)
        {
            OperationResult<List<EntityActiveTimeDC>> result = new OperationResult<List<EntityActiveTimeDC>>();
            try
            {
                result.OperationReturnValue = ClassFacade.GetActiveTimesForClassScheduleItem(scheduleItemId, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error getting class active time" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<Data.SystemObjects.ScheduleData> GetSeasonAndClassInfo(int branchId, int scheduleId, String gymCode)
        {
            OperationResult<Data.SystemObjects.ScheduleData> result = new OperationResult<Data.SystemObjects.ScheduleData>();
            try
            {
                result.OperationReturnValue = ClassFacade.GetSeasonAndClassInfo(branchId, scheduleId, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error getting season and classinfo" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static string GetListOrCalendarInitialViewByUser(string user, string gymCode)
        {
            try
            {
                return ClassFacade.GetListOrCalendarInitialViewByUser(user, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
 