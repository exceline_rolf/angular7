﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Economy.RestApi.Models
{
    public class RemovePaymentDetails
    {
        public int paymentID { get; set; }
        public int aritemNo { get; set; }
        public string user { get; set; }
        public string type { get; set; }
    }
}