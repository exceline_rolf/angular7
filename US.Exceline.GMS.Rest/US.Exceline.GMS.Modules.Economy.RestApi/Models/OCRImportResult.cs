﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Economy.RestApi.Models
{
    public class OCRImportResult
    {
        public int Status { get; set; }
        public List<string> Messages { get; set; }
    }
}