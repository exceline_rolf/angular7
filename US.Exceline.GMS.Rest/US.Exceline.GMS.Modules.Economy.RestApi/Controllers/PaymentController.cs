﻿using Ionic.Zip;
using SessionManager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using US.Common.Logging;
using US.Common.Logging.API;
using US.Communication.API;
using US.Exceline.GMS.Modules.Economy.API;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;
using US.Exceline.GMS.Modules.Economy.RestApi.Models;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.Economy.RestApi.Controllers
{
    [RoutePrefix("payment")]
    public class PaymentController : ApiController
    {
        [HttpGet]
        [Route("GetErrorPaymentsByType")]
        [Authorize]
        public HttpResponseMessage GetErrorPaymentsByDateRangeAndErrorPaymentType(string from, string to, string errorPaymentType, int branchId, string batchId = null)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<List<US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPErrorPayment>> result = US.Exceline.GMS.Modules.Economy.API.DirectPayment.GetErrorPayments(Convert.ToDateTime(from), Convert.ToDateTime(to), errorPaymentType, ExceConnectionManager.GetGymCode(user), branchId, batchId);
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPErrorPayment>(), ApiResponseStatus.ERROR, errorMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }

        [HttpPost]
        [Route("RemovePayment")]
        [Authorize]
        public HttpResponseMessage RemovePayment(RemovePaymentDetails paymentremoveDetails)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<bool> result = DirectPayment.RemovePayment(paymentremoveDetails.paymentID, paymentremoveDetails.aritemNo, user, ExceConnectionManager.GetGymCode(user), paymentremoveDetails.type);
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(true, ApiResponseStatus.OK));
        }

        [HttpGet]
        [Route("GetOCRImportHistory")]
        [Authorize]
        public HttpResponseMessage GetOCRImportHistory(string startDate, string endDate)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<List<OCRImportSummary>> result = OCR.GetOCRImportHistory(Convert.ToDateTime(startDate), Convert.ToDateTime(endDate), ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<OCRImportSummary>(), ApiResponseStatus.ERROR, errorMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }

        [HttpPost]
        [Route("MovePayment")]
        [Authorize]
        public HttpResponseMessage MovePayment(MovePaymentDetails movePaymentDetails)
        {

            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<int> result = DirectPayment.MovePayment(movePaymentDetails.paymentID, movePaymentDetails.aritemno, movePaymentDetails.type, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }

        [HttpGet]
        [Route("DeviationPrint")]
        [Authorize]
        public HttpResponseMessage GenerateDeviationPrint(string fromDate, string toDate, string branchId)
        {
            string user = Request.Headers.GetValues("UserName").First();
            var result = new PDFPrintResultDC();
            try
            {
                string pdfViewerURL = ConfigurationManager.AppSettings["PDFViewerURL"];
                var gymCode = ExceConnectionManager.GetGymCode(user);
                var template = ConfigurationManager.AppSettings["USC_DeviationLog_Print_App_Path"];

                var dataDictionary = new Dictionary<string, string>() { { "fromDate", fromDate }, { "toDate", toDate }, { "branchId", branchId.ToString() }, { "gymCode", gymCode } };
                var pdf = TemplateManager.ExecuteTemplate(dataDictionary, template, "PDF", gymCode).FirstOrDefault();
                if (pdf != null)
                {
                    result.FileParth = pdf.Messages.First(mssge => mssge.Header.ToLower().Equals("filepath")).Message;
                    if (!string.IsNullOrEmpty(result.FileParth))
                    {
                        var filePath = string.Empty;
                        //manipulate to get the PDF path
                        filePath = result.FileParth;
                        int indexOFRoot = filePath.IndexOf("PDF");
                        string restOfPath = filePath.Substring(indexOFRoot + 4, filePath.Length - (indexOFRoot + 4));
                        string[] pathItems = restOfPath.Split(new char[] { '\\' });
                        string path = string.Empty;

                        if (pathItems.Length > 0)
                        {
                            pathItems.ToList<string>().ForEach(X => path += "/" + X);
                        }
                        filePath = pdfViewerURL + path;
                        result.FileParth = filePath;
                    }
                }

                var isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, Convert.ToInt32(branchId), ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (var message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                USLogger.Log(ex.Message, new List<string> { "InvoicePrint", "Service" }, SeverityFilter.Error, null);
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new PDFPrintResultDC(), ApiResponseStatus.ERROR, new List<string>() { ex.Message }));
            }
        }

        [HttpPost]
        [Route("AddSessionValue")]
        [Authorize]
        public HttpResponseMessage AddSessionValue(SessionDetails sessionValue)
        {
             string user = Request.Headers.GetValues("UserName").First();
             int result = Session.AddSessionValue(sessionValue.sessionKey, sessionValue.value, ExceConnectionManager.GetGymCode(user));
             return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
        }

        [HttpGet]
        [Route("GetPaymentImportProcess")]
        [Authorize]
        public HttpResponseMessage GetPaymentImportProcessLog(string fromDate, string toDate)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<List<PaymentImportLogDC>> result = OCR.GetPaymentImportProcessLog(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<PaymentImportLogDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }

        [HttpGet]
        [Route("GetOCRSummary")]
        [Authorize]
        public HttpResponseMessage GetOCRSummary(string filePath)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                string folderPath = ConfigurationManager.AppSettings["OCRFileLocation"];
                if (!Directory.Exists(folderPath + @"\ErrorFiles"))
                {
                    Directory.CreateDirectory(folderPath + @"\ErrorFiles");
                }

                OperationResult<OCRSummary> result = OCR.GetSummary(filePath, user, folderPath, ExceConnectionManager.GetGymCode(user));
                foreach (NotificationMessage message in result.Notifications)
                {
                    result.OperationReturnValue.Messages.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch(Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("UploadOCRFile")]
        [Authorize]
        public HttpResponseMessage UploadOCRFile()
        {
            string user = Request.Headers.GetValues("UserName").First();
            string filePath = string.Empty;

            try
            {
                var request = HttpContext.Current.Request;
                string folderPath = ConfigurationManager.AppSettings["OCRFileLocation"];
                folderPath = folderPath + @"\" + ExceConnectionManager.GetGymCode(user);
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                if (request.Files.Count > 0)
                {
                    foreach (string file in request.Files)
                    {
                        var savingFile = request.Files[file];
                        filePath = folderPath + @"\" + savingFile.FileName;
                        if (File.Exists(filePath))
                        {
                            filePath = "DUPLICATE";
                        }
                        else
                        {
                            savingFile.SaveAs(filePath);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(filePath, ApiResponseStatus.OK));
                }
                else
                {
                    List<string> errorMsg = new List<string>();
                    errorMsg.Add("No file found");
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
            }catch(Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("ImportOCR")]
        [Authorize]
        public HttpResponseMessage ImportOCR(string filePath, string fileType)
        {
            string user = Request.Headers.GetValues("UserName").First();
            try
            {
                string folderPath = ConfigurationManager.AppSettings["OCRFileLocation"];
                if (!Directory.Exists(folderPath + @"\ErrorFiles"))
                {
                    Directory.CreateDirectory(folderPath + @"\ErrorFiles");
                }

                OperationResult<int> importResult = OCR.ImportOCRFile(filePath, folderPath, user, fileType, ExceConnectionManager.GetGymCode(user), 1);
                OCRImportResult ocrResult = new OCRImportResult();
                ocrResult.Messages = new List<string>();

                if (importResult.OperationReturnValue > 0)
                {
                    if (importResult.Notifications.Count(X => X.MessageType == MessageTypes.WARNING) == 0)
                    {
                        ocrResult.Status = 1; //"Imported Successfully"; 
                    }
                    else
                    {
                        ocrResult.Status = 2; // few records not imported 
                    }
                }
                else
                {
                    ocrResult.Status = importResult.OperationReturnValue;
                }

                foreach (NotificationMessage message in importResult.Notifications)
                {
                    ocrResult.Messages.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(ocrResult, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetErrorFiles")]
        [Authorize]
        public HttpResponseMessage DownloadErrorFiles(string filePath)
        {
            string filepath = string.Empty;
            HttpResponseMessage response = null;
            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                string directoryName = Path.GetDirectoryName(filePath) + "\\ErrorFiles\\" + Path.GetFileName(filePath);
                string[] files = Directory.GetFiles(directoryName);

                if (files.Length > 0)
                {
                    foreach (string fname in files)
                    {
                        zip.AddFile(fname, "ErrorFiles");
                    }
                   
                    string zipName = "ErrorFiles_" + DateTime.Today.ToString() + ".zip";
                    if (!string.IsNullOrEmpty(filePath))
                    {
                        zipName = "Errors_" + Path.GetFileName(filePath) + ".zip";
                    }

                    zip.Save(directoryName + @"\" + zipName);
                    filepath = directoryName + @"\" + zipName;
                }

                var fileStream = new FileStream(filepath, FileMode.Open, FileAccess.Read);
                response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StreamContent(fileStream)
                };

                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = Path.GetFileName(fileStream.Name)
                };
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
                return response;
            }
        }
    }
}
