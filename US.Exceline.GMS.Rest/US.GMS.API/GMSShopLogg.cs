﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.BusinessLogic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using System.Xml.Linq;
using System.Linq;


namespace US.GMS.API
{
    public class GMSShopLogg
    {

        public static OperationResult<int> SaveShopXMLData(ExcelineShopLoggData LoggData , String gymCode)
        {
            XElement SalePointInfo =
                new XElement("SalePointInfo",
                new XElement("BranchId", LoggData.salePointInfo.BranchId),
                new XElement("MachineName", LoggData.salePointInfo.MachineName),
                new XElement("SalePointId", LoggData.salePointInfo.SalePointId),
                new XElement("UserName", LoggData.salePointInfo.UserName),
                new XElement("SessionsKey", LoggData.salePointInfo.SessionsKey),
                new XElement("TerminalActive", LoggData.salePointInfo.TerminalActive),
                new XElement("Date", LoggData.salePointInfo.Date),
                new XElement("Time", LoggData.salePointInfo.Time),
                new XElement("CreditorNo", LoggData.salePointInfo.CreditorNo));

            XElement PostSaleInfo =
                new XElement("PostSaleInfo",
                new XElement("InstallmentId", LoggData.postSaleInfo.InstallmentId),
                new XElement("ARNo", LoggData.postSaleInfo.ARNo),
                new XElement("ShopSaleId", LoggData.postSaleInfo.ShopSaleId),
                new XElement("PaymentId", LoggData.postSaleInfo.PaymentId));

            XElement CustomerInfo =
                new XElement("CustomerInfo",
                new XElement("IsDefaultCustomer", LoggData.customerInfo.IsDefaultCustomer),
                new XElement("CustomerId", LoggData.customerInfo.CustomerId),
                new XElement("MemberId", LoggData.customerInfo.MemberId),
                new XElement("MemberBranchId", LoggData.customerInfo.MemberBranchId),
                new XElement("RoleType", LoggData.customerInfo.RoleType));

            List<XElement> iterator1 = new List<XElement>();
            List<XElement> iterator2 = new List<XElement>();

            int orderlinecounter = 1;

            for (var i = 0; i < LoggData.paymentTypeContent.PaymentTypes.Count; i++)
            {
                iterator1.Add(
                          new XElement("PaymentType_" + orderlinecounter++.ToString(),
                                    new XElement("Type", LoggData.paymentTypeContent.PaymentTypes[i].Type),
                                    new XElement("TypeCode", LoggData.paymentTypeContent.PaymentTypes[i].TypeCode),
                                    new XElement("TypeId", LoggData.paymentTypeContent.PaymentTypes[i].TypeId),
                                    new XElement("Amount", LoggData.paymentTypeContent.PaymentTypes[i].Amount)));

            }

            orderlinecounter = 1;

            for (var i = 0; i < LoggData.shopCartContent.CartItems.Count; i++)
            {
                iterator2.Add(
                          new XElement("CartItem_" + orderlinecounter++.ToString(),
                                    new XElement("CartitemNo", LoggData.shopCartContent.CartItems[i].CartitemNo.ToString()),
                                    new XElement("ArticleId", LoggData.shopCartContent.CartItems[i].ArticleId),
                                    new XElement("ArticleName", LoggData.shopCartContent.CartItems[i].ArticleName),
                                    new XElement("ArticlePrice", LoggData.shopCartContent.CartItems[i].ArticlePrice),
                                    new XElement("CurrentArticleStock", LoggData.shopCartContent.CartItems[i].CurrentArticleStock),
                                    new XElement("DiscountPercentage", LoggData.shopCartContent.CartItems[i].DiscountPercentage),
                                    new XElement("DiscountAmount", LoggData.shopCartContent.CartItems[i].DiscountAmount),
                                    new XElement("NumberOfItems", LoggData.shopCartContent.CartItems[i].NumberOfItems),
                                    new XElement("MVA", LoggData.shopCartContent.CartItems[i].MVA),
                                    new XElement("UnitPriceExcludingMVA", LoggData.shopCartContent.CartItems[i].UnitPriceExcludingMVA),
                                    new XElement("TotalPriceExcludingMVA", LoggData.shopCartContent.CartItems[i].TotalPriceExcludingMVA),
                                    new XElement("DiscountComment", LoggData.shopCartContent.CartItems[i].DiscountComment),
                                    new XElement("ReturnComment", LoggData.shopCartContent.CartItems[i].ReturnComment),
                                    new XElement("InstallmentShareId", LoggData.shopCartContent.CartItems[i].InstallmentShareId),
                                    new XElement("ShopSaleItemId", LoggData.shopCartContent.CartItems[i].ShopSaleItemId),
                                    new XElement("ARItemNo", LoggData.shopCartContent.CartItems[i].ARItemNo),
                                    new XElement("OrderLineId", LoggData.shopCartContent.CartItems[i].OrderLineId)));

            }

           var SaleXMLData = new XDocument(new XElement("SaleXMLData",
               SalePointInfo,
               CustomerInfo,
               new XElement("PaymentTypeContent", from line in iterator1 select line),
               new XElement("ShopCartContent", from line in iterator2 select line),
               PostSaleInfo));

           String XMLData = SaleXMLData.ToString(SaveOptions.DisableFormatting);

     
            return LoggManager.SaveShopXMLData(XMLData, LoggData.salePointInfo.SessionsKey, gymCode);
        }
    }
}
