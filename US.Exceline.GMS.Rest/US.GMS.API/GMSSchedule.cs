﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.ResultNotifications;
using US.GMS.BusinessLogic;
using US.GMS.Core.SystemObjects;

namespace US.GMS.API
{
    public class GMSSchedule
    {
        public static OperationResult<bool> UpdateActiveTime(EntityActiveTimeDC activeTime, bool isDelete,int branchId,string user, string gymCode)
        {
            return ScheduleManager.UpdateActiveTime(activeTime, isDelete,branchId,user, gymCode);
        }

        public static OperationResult<bool> UpdateCalenderActiveTimes(List<EntityActiveTimeDC> activeTimeList, int branchId, string user, string gymCode)
        {
            return ScheduleManager.UpdateCalenderActiveTimes(activeTimeList,branchId,user, gymCode);
        }

        public static OperationResult<bool> UpdateSheduleItems(ScheduleDC schedule, string gymCode)
        {
            return ScheduleManager.UpdateSheduleItems(schedule, gymCode);
        }

        public static OperationResult<List<EntityActiveTimeDC>> GetEntityActiveTimes(int branchId, DateTime startDate, DateTime endDate, List<int> entityList, string entityType, string gymCode, string user)
        {
            return ScheduleManager.GetEntityActiveTimes(branchId, startDate, endDate, entityList, entityType, gymCode, user);
        }

        public static OperationResult<ScheduleDC> GetEntitySchedule(int entityId, string entityRoleType, string user, string gymCode)
        {
            return ScheduleManager.GetEntitySchedule(entityId, entityRoleType, user, gymCode);
        }

        public static OperationResult<bool> DeleteScheduleItem(int scheduleItemId, bool activeStatus, string user, string gymCode)
        {
            return ScheduleManager.DeleteScheduleItem(scheduleItemId, activeStatus, user, gymCode);
        }


        public static OperationResult<bool> SaveShedule(int id, string name, string roleId, int branchId, ScheduleDC sheduleDc, string user, string gymCode)
        {
            return ScheduleManager.SaveShedule(id, name, roleId, branchId, sheduleDc, user, gymCode);
        }

        public static OperationResult<List<ScheduleItemDC>> GetScheduleItemsByScheduleId(int scheduleId, string gymCode)
        {
            return ScheduleManager.GetScheduleItemsByScheduleId(scheduleId, gymCode);
        }

        public static OperationResult<int> CheckScheduleOverlapForEmployee(ScheduleItemDC scheduleItem, string gymCode)
        {
            return ScheduleManager.CheckScheduleOverlapForEmployee(scheduleItem,gymCode);
        }
    }
}
