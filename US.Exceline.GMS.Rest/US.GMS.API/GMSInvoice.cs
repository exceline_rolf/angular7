﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.BusinessLogic;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.Payment.Core.ResultNotifications;

namespace US.GMS.API
{
    public class GMSInvoice
    {
        public static OperationResult<bool> ScheduleOrdersForInvoicing(int branchId, string gymCode)
        {
            return InvoiceManager.ScheduleOrdersForInvoicing(branchId, gymCode);
        }
        public static OperationResult<bool> ScheduleSponsorOrdersForInvoicing(int branchId, string gymCode)
        {
            return InvoiceManager.ScheduleSponsorOrdersForInvoicing(branchId, gymCode);
        }
        public static OperationResult<string> GetCreditorForBranch(int branchId, string gymCode)
        {
            return InvoiceManager.GetCreditorForBranch(branchId, gymCode);
        }

        public static OperationResult<bool> CancelInvoice(int arItemNo, string user, string gymCode)
        {
            return InvoiceManager.CancelInvoice(arItemNo, user, gymCode);
        }

        public static OperationResult<List<MemberInvoiceDC>> GetMemberInvoices(int memberID, int hit, string gymCode, string user)
        {
            return InvoiceManager.GetMemberInvoices(memberID, hit, gymCode, user);
        }

        public static OperationResult<ExcelineInvoiceDetailDC> GetInvoiceDetails(int arItemNo,int branchId, string gymCode)
        {
            return InvoiceManager.GetInvoiceDetails(arItemNo,branchId, gymCode);
        }

        public static OperationResult<List<OrderLineDC>> GetOrderLinesForAutoCreditNote(int branchId, int priority, string gymCode)
        {
            return InvoiceManager.GetOrderLinesForAutoCreditNote(branchId, priority, gymCode);
        }

        public static OperationResult<bool> UpdateInvoice(int arItemNo, string comment, DateTime DueDate, string gymCode)
        {
            return InvoiceManager.UpdateInvoice(arItemNo, comment, DueDate, gymCode);
        }

        public static OperationResult<int> AddCreditNote(CreditNoteDC creditNote, int branchId, bool isReturn, string gymCode)
        {
            return InvoiceManager.AddCreditNote(creditNote, branchId, isReturn,gymCode);
        }

        public static OperationResult<int> AddCreditNoteForBooking(CreditNoteDC creditNote, List<PayModeDC> payModeDcs, int branchId, bool isReturn, string gymCode)
        {
            return InvoiceManager.AddCreditNoteForBooking(creditNote,payModeDcs, branchId, isReturn, gymCode);
        }

        public static OperationResult<int> PayCreditNote(int arItemno, List<PayModeDC> paymodes, int memberId, int shopAccountId, string gymCode, string user, int salePointId, int loggedBranchID)
        {
            return InvoiceManager.PayCreditNote(arItemno, paymodes, memberId, shopAccountId, gymCode, user, salePointId, loggedBranchID);
        }

        public static OperationResult<List<MemberInvoiceDC>> GetMemberInvoicesForContract(int memberId, int contractId, int hit, string gymCode, string user)
        {
            return InvoiceManager.GetMemberInvoicesForContract(memberId, contractId, hit, gymCode, user);
        }
    }
}
