﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.BusinessLogic;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.GMS.API
{
    public class GMSCommonBooking
    {
        public static OperationResult<bool> CheckCommonBookingAvailability(int activeTimeId, int entityId, int branchId, string entityType, DateTime startDateTime, DateTime endDateTime, string user, string gymCode)
        {
            return CommonBookingManager.CheckCommonBookingAvailability(activeTimeId, entityId, branchId, entityType, startDateTime, endDateTime, user, gymCode);
        }

        public static OperationResult<bool> SaveCommonBookingDetails(CommonBookingDC commonBookingDC, ScheduleDC scheduleDC, int branchId, string user, string scheduleCategoryType,string gymCode)
        {
            return CommonBookingManager.SaveCommonBookingDetails(commonBookingDC,scheduleDC, branchId, user,scheduleCategoryType,gymCode);
        }
    }
}
