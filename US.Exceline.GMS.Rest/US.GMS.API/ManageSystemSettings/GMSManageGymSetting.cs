﻿using System.Collections.Generic;
using US.GMS.BusinessLogic.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;

namespace US.GMS.API.ManageSystemSettings
{
    public class GMSManageGymSetting
    {

        public static OperationResult<string> GetGymSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName=null)
        {
            return GymSettingManager.GetGymSettings(branchId, gymCode, gymSettingType,userName,systemName);
        }


        public static OperationResult<List<GymAccessSettingDC>> GetGymAccessTimeSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName = null)
        {
            return GymSettingManager.GetGymAccessTimeSettings(branchId, gymCode, gymSettingType, userName, systemName);
        }
        public static OperationResult<List<GymOpenTimeDC>> GetGymOpenTimes(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName = null)
        {
            return GymSettingManager.GetGymOpenTimes(branchId, gymCode, gymSettingType, userName, systemName);
        }
        public static OperationResult<List<GymRequiredSettingsDC>> GetGymRequiredSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName = null)
        {
            return GymSettingManager.GetGymRequiredSettings(branchId, gymCode, gymSettingType, userName, systemName);
        }
        public static OperationResult<List<GymMemberSearchSettingsDC>> GetGymMemberSearchSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName = null)
        {
            return GymSettingManager.GetGymMemberSearchSettings(branchId, gymCode, gymSettingType, userName, systemName);
        }
        public static OperationResult<List<ExceHardwareProfileDC>> GetHardwareProfiles(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName = null)
        {
            return GymSettingManager.GetHardwareProfiles(branchId, gymCode, gymSettingType, userName, systemName);
        }
        public static OperationResult<List<GymEconomySettingsDC>> GetGymEconomySettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName = null)
        {
            return GymSettingManager.GetGymEconomySettings(branchId, gymCode, gymSettingType, userName, systemName);
        }
        public static OperationResult<List<GymSmsSettingsDC>> GetGymSmsSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName = null)
        {
            return GymSettingManager.GetGymSmsSettings(branchId, gymCode, gymSettingType, userName, systemName);
        }
        public static OperationResult<List<GymOtherSettingsDC>> GetGymOtherSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName = null)
        {
            return GymSettingManager.GetGymOtherSettings(branchId, gymCode, gymSettingType, userName, systemName);
        }
        public static OperationResult<List<GymIntegrationSettingsDC>> GetGymIntegrationSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName = null)
        {
            return GymSettingManager.GetGymIntegrationSettings(branchId, gymCode, gymSettingType, userName, systemName);
        }
        public static OperationResult<List<GymIntegrationExcSettingsDC>> GetGymIntegrationExcSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName = null)
        {
            return GymSettingManager.GetGymIntegrationExcSettings(branchId, gymCode, gymSettingType, userName, systemName);
        }
        public static OperationResult<OtherIntegrationSettingsDC> GetOtherIntegrationSettings(int branchId, string gymCode, GymSettingType gymSettingType, string userName, string systemName = null)
        {
            return GymSettingManager.GetOtherIntegrationSettings(branchId, gymCode, gymSettingType, userName, systemName);
        }


        public static OperationResult<List<TerminalType>> GetTerminalTypes(string gymCode)
        {
            return GymSettingManager.GetTerminalTypes(gymCode);
        }

        public static OperationResult<bool> SaveGymSettings(int branchId, string accessTimeDetails, string gymCode, GymSettingType gymSettingType)
        {
            return GymSettingManager.SaveGymSettings(branchId, accessTimeDetails, gymCode, gymSettingType);
        }


        public static OperationResult<bool> SaveGymAccessSettings(int branchId, GymAccessSettingDC settingsDetails, string gymCode)
        {
            return GymSettingManager.SaveGymAccessSettings(branchId, settingsDetails, gymCode);
        }

        public static OperationResult<bool> SaveGymOpenTimes(int branchId, List<GymOpenTimeDC> settingsDetails, string gymCode)
        {

            return GymSettingManager.SaveGymOpenTimes(branchId, settingsDetails, gymCode);

        }
        public static OperationResult<bool> SaveGymRequiredSettings(int branchId, GymRequiredSettingsDC settingsDetails, string gymCode)
        {
            return GymSettingManager.SaveGymRequiredSettings(branchId, settingsDetails, gymCode);
        }
        public static OperationResult<bool> SaveGymMemberSearchSettings(int branchId, GymMemberSearchSettingsDC settingsDetails, string gymCode)
        {
            return GymSettingManager.SaveGymMemberSearchSettings(branchId, settingsDetails, gymCode);
        }
        public static OperationResult<bool> SaveHardwareProfile(int branchId, List<ExceHardwareProfileDC> settingsDetails, string gymCode)
        {
            return GymSettingManager.SaveHardwareProfile(branchId, settingsDetails, gymCode);
        }
        public static OperationResult<bool> SaveGymEconomySettings(int branchId, GymEconomySettingsDC settingsDetails, string gymCode)
        {
            return GymSettingManager.SaveGymEconomySettings(branchId, settingsDetails, gymCode);
        }
        public static OperationResult<bool> SaveGymSmsSettings(int branchId, GymSmsSettingsDC settingsDetails, string gymCode)
        {
            return GymSettingManager.SaveGymSmsSettings(branchId, settingsDetails, gymCode);
        }
        public static OperationResult<bool> SaveGymOtherSettings(int branchId, GymOtherSettingsDC settingsDetails, string gymCode)
        {
            return GymSettingManager.SaveGymOtherSettings(branchId, settingsDetails, gymCode);
        }
        public static OperationResult<bool> SaveGymIntegrationSettings(int branchId, GymIntegrationSettingsDC settingsDetails, string gymCode)
        {
            return GymSettingManager.SaveGymIntegrationSettings(branchId, settingsDetails, gymCode);
        }
        public static OperationResult<bool> SaveGymIntegrationExcSettings(int branchId, GymIntegrationExcSettingsDC settingsDetails, string gymCode)
        {
            return GymSettingManager.SaveGymIntegrationExcSettings(branchId, settingsDetails, gymCode);
        }
        public static OperationResult<bool> AddUpdateOtherIntegrationSettings(int branchId, OtherIntegrationSettingsDC settingsDetails, string gymCode)
        {
            return GymSettingManager.AddUpdateOtherIntegrationSettings(branchId, settingsDetails, gymCode);
        }


        public static OperationResult<Dictionary<string, object>> GetSelectedGymSettings(List<string> gymSettingColumnList, bool isGymSettings, int branchId, string gymCode)
        {
            return GymSettingManager.GetSelectedGymSettings(gymSettingColumnList, isGymSettings, branchId, gymCode);
        }

        public static OperationResult<bool> DeleteIntegrationSettingById(int id, string getGymCode)
        {
            return GymSettingManager.DeleteIntegrationSettingById(id, getGymCode);
        }
    }
}
