﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "5/3/2012 5:07:50 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.ResultNotifications;
using US.GMS.BusinessLogic.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Notification;
using US.Common.Notification.Core.DomainObjects;

namespace US.GMS.API.ManageSystemSettings
{
    public class GMSManageTask
    {

        #region Task Templates
        public static OperationResult<bool> SaveTaskTemplate(TaskTemplateDC taskTemplate, bool isEdit, string gymCode)
        {
            return TaskManager.SaveTaskTemplate(taskTemplate, isEdit, gymCode);
        }

        public static OperationResult<bool> UpdateTaskTemplate(TaskTemplateDC taskTemplate, string gymCode)
        {
            return TaskManager.UpdateTaskTemplate(taskTemplate, gymCode);
        }

        public static OperationResult<bool> DeleteTaskTemplate(int taskTemplateId, string gymCode)
        {
            return TaskManager.DeleteTaskTemplate(taskTemplateId, gymCode);
        }


        public static OperationResult<bool> DeActivateTaskTemplate(int taskTemplateId, string gymCode)
        {
            return TaskManager.DeActivateTaskTemplate(taskTemplateId, gymCode);
        }

        public static OperationResult<List<TaskTemplateDC>> GetTaskTemplates(TaskTemplateType templateType, int branchId, int templateId, string gymCode)
        {
            return TaskManager.GetTaskTemplates(templateType, branchId, templateId, gymCode);
        }

        public static OperationResult<List<ExtendedTaskTemplateDC>> GetTaskTemplateExtFields(int extFieldId, string gymCode)
        {
            return TaskManager.GetTaskTemplateExtFields(extFieldId, gymCode);
        }

        public static OperationResult<List<ExtendedFieldDC>> GetExtFieldsByCategory(int categoryId,string categoryType,string gymCode)
        {
            return TaskManager.GetExtFieldsByCategory(categoryId, categoryType, gymCode);
        }

        public static OperationResult<int> SaveEntityTask(ExcelineTaskDC excelineTask, string gymCode)
        {
            return TaskManager.SaveEntityTask(excelineTask, gymCode);
        }

        #endregion

        #region Exceline Task
        public static OperationResult<int> SaveExcelineTask(ExcelineTaskDC excelineTask, string gymCode)
        {
            return TaskManager.SaveExcelineTask(excelineTask, gymCode);
        }

        public static OperationResult<int> EditExcelineTask(ExcelineTaskDC excelineTask, string gymCode)
        {
            return TaskManager.EditExcelineTask(excelineTask, gymCode);
        }

        public static OperationResult<List<ExcelineTaskDC>> GetExcelineTasks(int branchId, bool isFxied, int templateId, int assignTo, string roleType, int followupMemberId, string gymCode)
        {
            return TaskManager.GetExcelineTasks(branchId, isFxied, templateId, assignTo, roleType, followupMemberId, gymCode);
        }

        public static OperationResult<List<ExcelineTaskDC>> SearchExcelineTask(string searchText, TaskTemplateType type, int followUpMemId, int branchId, bool isFxied, string gymCode)
        {
            return TaskManager.SearchExcelineTask(searchText, type, followUpMemId, branchId, isFxied, gymCode);
        }

        public static OperationResult<bool> SaveTaskWithOutSchedule(ExcelineTaskDC excelineTask, string gymCode)
        {
            return TaskManager.SaveTaskWithOutSchedule(excelineTask, gymCode);
        }
        #endregion

        #region Setting

        public static OperationResult<decimal> GetSessionTimeOutAfter(int branchId, string gymCode)
        {
            return TaskManager.GetSessionTimeOutAfter(branchId, gymCode);
        }

        #endregion


        #region Exceline Task Category
        public static OperationResult<bool> SaveExcelineTaskCategory(ExcelineTaskCategoryDC taskCategory, bool isEdit, string gymCode)
        {
            return TaskManager.SaveTaskCategory(taskCategory,isEdit, gymCode);
        }

        public static OperationResult<List<ExcelineTaskCategoryDC>> GetTaskCategories(int branchId, string gymCode)
        {
            return TaskManager.GetTaskCategories(branchId, gymCode);
        }

        public static OperationResult<bool> DeleteTaskCategory(int taskCategoryId, string gymCode)
        {
            return TaskManager.DeleteTaskCategory(taskCategoryId, gymCode);
        }
        #endregion

        #region Exceline Job Category
        public static OperationResult<bool> SaveExcelineJobCategory(ExcelineJobCategoryDC jobCategory, bool isEdit, string gymCode)
        {
            return TaskManager.SaveJobCategory(jobCategory, isEdit, gymCode);
        }

        public static OperationResult<List<ExcelineJobCategoryDC>> GetJobCategories(int branchId, string gymCode)
        {
            return TaskManager.GetJobCategories(branchId, gymCode);
        }

        public static OperationResult<bool> DeleteJobCategory(int jobCategoryId, string gymCode)
        {
            return TaskManager.DeleteJobCategory(jobCategoryId, gymCode);
        }

        public static OperationResult<bool> AddEventToNotifications(USCommonNotificationDC notification, string gymCode)
        {
            return TaskManager.AddEventToNotifications(notification, gymCode);
        }
        #endregion
    }
}
