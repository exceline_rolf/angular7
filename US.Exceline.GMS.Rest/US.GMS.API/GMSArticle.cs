﻿using System;
using System.Collections.Generic;
using US.GMS.BusinessLogic;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;

namespace US.GMS.API
{
    public class GMSArticle
    {
        public static OperationResult<int> SaveArticle(ArticleDC articleDc, string user, int branchId, int activityCategoryId, string gymCode)
        {
            try
            {
                return ArticleManager.SaveArticle(articleDc, user, branchId, activityCategoryId, gymCode);
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OperationResult<bool> ImportArticleList(List<ArticleDC> articleList, string user, int branchID,  string gymCode)
        {
            return ArticleManager.ImportArticleList( articleList,  user,  branchID,   gymCode);
        }

        public static OperationResult<List<ArticleDC>> GetArticles(int branchId, string user, ArticleTypes articleType, string keyWord, CategoryDC category, int activityId, bool? isObsalate, bool activeState, string gymCode, bool isFilterByBranch)
        {
            return ArticleManager.GetArticles(branchId, user, articleType, keyWord, category, activityId, isObsalate, activeState, gymCode, isFilterByBranch);
        }

        public static OperationResult<ArticleDC> GetArticleById(int articleId, string gymCode)
        {
            return ArticleManager.GetArticleById(articleId, gymCode);
        }

        public static OperationResult<ArticleDC> GetInvoiceFee(int branchId, string gymCode)
        {
            return ArticleManager.GetInvoiceFee(branchId, gymCode);
        }

        public static OperationResult<bool> UploadArticle(List<ArticleDC> articleList, string user, int branchId, string gymCode)
        {
            return ArticleManager.UploadArticle(articleList, user,branchId, gymCode);
        }

        public static OperationResult<List<ArticleDC>> GetArticlesSearch(int branchId, string keyWord, string gymCode)
        {
            return ArticleManager.GetArticlesSearch(branchId, keyWord, gymCode);
        }
    }
}
