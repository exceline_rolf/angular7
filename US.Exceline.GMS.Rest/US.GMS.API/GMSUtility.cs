﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : "8/8/2012 4:35:07 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.BusinessLogic;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;

namespace US.GMS.API
{
    public class GMSUtility
    {
        public static OperationResult<string> GetCityByPostalCode(string postalCode, string gymCode)
        {
            return UtilityManager.GetCityForPostalCode(postalCode, gymCode);
        }

        public static OperationResult<List<CalendarHoliday>> GetHolidays(DateTime calendarStartDate, int branchId, string gymCode)
        {
            return UtilityManager.GetHolidays(calendarStartDate, branchId, gymCode);
        }
    }
}
