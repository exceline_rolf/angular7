﻿using System;
using US.GMS.BusinessLogic;
using US.GMS.Core.ResultNotifications;

namespace US.GMS.API
{
    public class GMSUser
    {
        public static OperationResult<bool> UpdateSettingForUserRoutine(string key, string value, string user, string gymCode)
        {
            try
            {
                return UserManager.UpdateSettingForUserRoutine(key, value, user, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
