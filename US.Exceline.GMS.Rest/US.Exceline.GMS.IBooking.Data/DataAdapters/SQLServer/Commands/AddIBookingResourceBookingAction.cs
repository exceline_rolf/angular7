﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
   public class AddIBookingResourceBookingAction : USDBActionBase<int>
   {
       private ExceIBookingResourceBooking _resourceBooking;
       private DataTable _dataTable;
       private DbTransaction _dbTransaction = null;
       private int _extraResParentId;

       public AddIBookingResourceBookingAction(ExceIBookingResourceBooking resourceBooking)
       {
           _resourceBooking = resourceBooking;
           _dataTable = GetMemberLst(_resourceBooking.CustomerIdList, _resourceBooking.ExtraResourceIdList);
       }

       private DataTable GetMemberLst(List<int> memLst,List<int> resLst)
       {
           _dataTable = new DataTable();
           _dataTable.Columns.Add(new DataColumn("ActiveTimeId", Type.GetType("System.Int32")));
           _dataTable.Columns.Add(new DataColumn("EntityId", Type.GetType("System.Int32")));
           _dataTable.Columns.Add(new DataColumn("RoleId", Type.GetType("System.String")));
           if (memLst != null && memLst.Any())
               foreach (var item in memLst)
               {
                   DataRow dataTableRow = _dataTable.NewRow();
                   dataTableRow["EntityId"] = item;
                   dataTableRow["RoleId"] = "MEM";
                   _dataTable.Rows.Add(dataTableRow);
               }

           if (resLst != null && resLst.Any())
               foreach (var item in resLst)
               {
                   DataRow dataTableRow = _dataTable.NewRow();
                   dataTableRow["EntityId"] = item;
                   dataTableRow["RoleId"] = "RES";
                   _dataTable.Rows.Add(dataTableRow);
               }
           return _dataTable;
       }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSIBookingAddBooking";
            int result = -1;
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Branchid", DbType.Int32, _resourceBooking.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Scheduleitemid", DbType.Int32, _resourceBooking.ScheduleItemId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Fromdatetime", DbType.DateTime, _resourceBooking.FromDateTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Todatetime", DbType.DateTime, _resourceBooking.ToDateTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Resourceid", DbType.Int32, _resourceBooking.ResourceId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BookingCategoryId", DbType.Int32, _resourceBooking.BookingCategoryId));
                cmd.Parameters.Add(_dataTable != null
                                       ? DataAcessUtils.CreateParam("@Excememberboking", SqlDbType.Structured,
                                                                    _dataTable)
                                       : DataAcessUtils.CreateParam("@Excememberboking", SqlDbType.Structured, null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Note", DbType.String, _resourceBooking.Note));
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@OutId";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(output.Value);
            
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        //protected override int Body(DbConnection connection)
        //{
        //    const string storedProcedureName = "USExceGMSIBookingAddBooking";
        //    int result = -1;
        //    try
        //    {
        //        _dbTransaction = connection.BeginTransaction();
        //       // DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

        //        if (_resourceBooking.ScheduleItemIdList != null)
        //            foreach (var item in _resourceBooking.ScheduleItemIdList)
        //            {
        //                DbCommand cmd = _dbTransaction.Connection.CreateCommand();
        //                cmd.Transaction = _dbTransaction;
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.CommandText = storedProcedureName;

        //                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _resourceBooking.BranchId));
        //                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, item.Value));
        //                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FromDateTime", DbType.DateTime, _resourceBooking.FromDateTime));
        //                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ToDateTime", DbType.DateTime, _resourceBooking.ToDateTime));
        //                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ResourceId", DbType.Int32, item.Key));
        //                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BookingCategoryId", DbType.Int32, _resourceBooking.BookingCategoryId));
        //                if (_extraResParentId > 0 && _resourceBooking.ResourceId != item.Key)
        //                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ParentId", DbType.Int32, _extraResParentId));
        //                cmd.Parameters.Add(_dataTable != null
        //                                       ? DataAcessUtils.CreateParam("@Excememberboking", SqlDbType.Structured,
        //                                                                    _dataTable)
        //                                       : DataAcessUtils.CreateParam("@Excememberboking", SqlDbType.Structured, null));

        //                DbParameter output = new SqlParameter();
        //                output.DbType = DbType.Int32;
        //                output.ParameterName = "@OutId";
        //                output.Direction = ParameterDirection.Output;
        //                cmd.Parameters.Add(output);
        //                cmd.ExecuteNonQuery();
        //                result =  Convert.ToInt32(output.Value);
                        

        //                if (result <= 0)
        //                {
        //                    _dbTransaction.Rollback();
        //                    break;
        //                }

        //                if (_resourceBooking.ResourceId == item.Key)
        //                _extraResParentId = result;

        //                cmd.Parameters.Clear();
        //            }

        //        _dbTransaction.Commit(); 
        //    }
        //    catch (Exception)
        //    {
        //        _dbTransaction.Rollback();
        //        throw ;
        //    }
        //    return result;
        //}

      
    }
}
