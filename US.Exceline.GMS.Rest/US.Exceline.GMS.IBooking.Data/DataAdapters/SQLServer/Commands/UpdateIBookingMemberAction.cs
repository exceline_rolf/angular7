﻿using System;
using System.Data.SqlClient;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateIBookingMemberAction : USDBActionBase<int>
    {
        private readonly ExceIBookingUpdateMember _member;

        public UpdateIBookingMemberAction(ExceIBookingUpdateMember member)
        {
            _member = member;
        }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSIBookingUpdateMember";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@systemId", DbType.Int32, _member.SystemId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _member.GymId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@customerNo", DbType.Int32, _member.CustomerNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@firstName", DbType.String, _member.NewFirstname));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastName", DbType.String, _member.NewLastName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address1", DbType.String, _member.NewAddress));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@postCode", DbType.String, _member.NewPostcode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@postPlace", DbType.String, _member.NewPostplace));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@privateTeleNo", DbType.String, _member.NewPhonePrivate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@workTeleNo", DbType.String, _member.NewPhoneWork));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@email", DbType.String, _member.NewEmail));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@accountNumber", DbType.String, _member.NewAccountnumber));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PinCode", DbType.String, _member.PinCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@telMobile", DbType.String, _member.NewPhoneMobile));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@telMobileCountryCode", DbType.String, _member.NewPhoneMobileCountryCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SendEmail", DbType.Boolean, _member.SendEmail));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SendSMS", DbType.Boolean, _member.SendSMS));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CustomerClub", DbType.Boolean, _member.CustomerClub));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberCardNo", DbType.String, _member.MemCardNo));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@OutID";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                return Convert.ToInt32(output.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
