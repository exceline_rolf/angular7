﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractByMemberIdAction : USDBActionBase<List<ExceIBookingContractFreeze>>
    {        
        private readonly string _customerId;
        private readonly int _companyId;

        public GetContractByMemberIdAction(string customerId, int systemId)
        {
            _customerId = customerId;
            _companyId = systemId;
        }

        protected override List<ExceIBookingContractFreeze> Body(System.Data.Common.DbConnection connection)
        {
            List<ExceIBookingContractFreeze> memberContractList = new List<ExceIBookingContractFreeze>();
            string spName = "USExceGMSIBookingListMemberContracts";
            DbDataReader reader = null;
            try
            {
                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", System.Data.DbType.String, _customerId));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExceIBookingContractFreeze contract = new ExceIBookingContractFreeze();
                    contract.Id = Convert.ToInt32(reader["ID"]);
                    contract.MemberID = Convert.ToInt32(reader["MemberID"]);
                    contract.ContractNo = Convert.ToString(reader["ContractNo"]);
                    contract.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    contract.TemplateName = Convert.ToString(reader["TemplateName"]);
                    contract.ActivityName = Convert.ToString(reader["ActivityName"]);
                    if (reader["CreatedDate"] != DBNull.Value)
                        contract.SignedDate = ((Convert.ToDateTime(reader["CreatedDate"])).ToUniversalTime() - epoch).TotalMilliseconds.ToString();

                    if (reader["ContractEndDate"] != DBNull.Value)                                                                    
                        contract.EndDate = ((Convert.ToDateTime(reader["ContractEndDate"])).ToUniversalTime() - epoch).TotalMilliseconds.ToString();

                    if (reader["ContractStartDate"] != DBNull.Value)
                        contract.StartDate = ((Convert.ToDateTime(reader["ContractStartDate"])).ToUniversalTime() - epoch).TotalMilliseconds.ToString();

                    if (reader["RenewedDate"] != DBNull.Value)
                        contract.RenewedDate = ((Convert.ToDateTime(reader["RenewedDate"])).ToUniversalTime() - epoch).TotalMilliseconds.ToString();

                    if (reader["ResignedDate"] != DBNull.Value)
                        contract.ResignedDate = ((Convert.ToDateTime(reader["ResignedDate"])).ToUniversalTime() - epoch).TotalMilliseconds.ToString();

                    if (reader["ClosedDate"] != DBNull.Value)
                        contract.Closedate = ((Convert.ToDateTime(reader["ClosedDate"])).ToUniversalTime() - epoch).TotalMilliseconds.ToString();

                    if (reader["LockInPeriodEndDate"] != DBNull.Value)
                        contract.LockinPeriodEndDate = ((Convert.ToDateTime(reader["LockInPeriodEndDate"])).ToUniversalTime() - epoch).TotalMilliseconds.ToString();

                    if (reader["OriginalEndDate"] != DBNull.Value)
                        contract.OriginalEndDate = ((Convert.ToDateTime(reader["OriginalEndDate"])).ToUniversalTime() - epoch).TotalMilliseconds.ToString();

                    contract.NoOfOrders = Convert.ToInt32(reader["NumberofInstallments"]);
                    contract.StartUpItemsPrice = Convert.ToDecimal(reader["StartUpItemPrice"]);
                    contract.MonthlyitemPrice = Convert.ToDecimal(reader["EveryMonthItemPrice"]);
                    contract.TotalPrice = Convert.ToDecimal(reader["PackagePrice"]);
                    contract.Status = Convert.ToString(reader["Status"]);
                    contract.AccessProfileId = Convert.ToInt32(reader["AccessProfileId"]);
                    contract.BranchId = Convert.ToInt32(reader["BranchId"]);
                    contract.RemainingVisits = Convert.ToInt32(reader["Visits"]);
                    contract.ContractTypeCode = Convert.ToString(reader["ContractTypeCode"]);
                    contract.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    contract.IsGroupContract = Convert.ToBoolean(reader["IsGroupContract"]);
                    contract.IsFreezed = Convert.ToBoolean(reader["IsFreezed"]);

                    if (reader["FreezeFromDate"] != DBNull.Value)                        
                        contract.FreezeFromDate = ((Convert.ToDateTime(reader["FreezeFromDate"])).ToUniversalTime() - epoch).TotalMilliseconds.ToString();
                    

                    if (reader["FreezeToDate"] != DBNull.Value)
                        contract.FreezeToDate = ((Convert.ToDateTime(reader["FreezeToDate"])).ToUniversalTime() - epoch).TotalMilliseconds.ToString();

                    if (reader["FreezeMonthsThisYear"] != DBNull.Value)
                    { 
                        contract.FreezeMonthsThisYear = Convert.ToInt32(reader["FreezeMonthsThisYear"]);
                    }
                    else
                    {
                        contract.FreezeMonthsThisYear = 0;
                    }
                    contract.FreezeAllowed = Convert.ToInt32(reader["FreezeAllowed"]);
                    contract.InvoiceFreeMonths = Convert.ToString(reader["InvoiceFreeMonths"]);

                    if (reader["LastDueDate"] != DBNull.Value)
                        contract.LastDueDate = ((Convert.ToDateTime(reader["LastDueDate"])).ToUniversalTime() - epoch).TotalMilliseconds.ToString();

                    contract.MemberShipType = Convert.ToInt32(reader["MembershipType"]);
                    



                    memberContractList.Add(contract);
                    
                }

                if (reader != null)
                    reader.Close();

                return memberContractList;
            }
            catch (Exception)
            {
                throw;
            }

        }

    }
}
