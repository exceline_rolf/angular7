﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.IBooking.Core;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetARXSettingsAction : USDBActionBase<ARXSetting>
    {
        private int _systemID = -1;

        public GetARXSettingsAction(int systemID)
        {
            _systemID = systemID;
        }

        protected override ARXSetting Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceWorkStationGetIBookingARXSettings";
            ARXSetting arxsetting = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@SystemID", System.Data.DbType.Int32, _systemID));
                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    arxsetting = new ARXSetting();
                    arxsetting.Id = Convert.ToInt32(reader["ID"]);
                    arxsetting.GymCode = Convert.ToString(reader["GymCode"]);
                    arxsetting.ServiceURL = Convert.ToString(reader["FileUploadUrl"]);
                    arxsetting.UserName = Convert.ToString(reader["FileUploadUserName"]);
                    arxsetting.Password = Convert.ToString(reader["FileUploadPassword"]);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return arxsetting;
        }
    }
}
