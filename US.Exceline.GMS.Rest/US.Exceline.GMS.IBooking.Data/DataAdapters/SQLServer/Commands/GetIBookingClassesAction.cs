﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingClassesAction : USDBActionBase<List<ExceIBookingClass>>
    {
        private int _branchId;

        public GetIBookingClassesAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<ExceIBookingClass> Body(DbConnection connection)
        {
            List<ExceIBookingClass> classList = new List<ExceIBookingClass>();
            string storedProcedure = "USExceGMSIBookingGetClassses";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExceIBookingClass excClass = new ExceIBookingClass();
                    excClass.ClassId = Convert.ToInt32(reader["ClassId"]);
                    excClass.ClassName = reader["ClassName"].ToString();
                    excClass.Description = reader["Description"].ToString();
                    excClass.MaxNumOfParticipants = Convert.ToInt32(reader["MaxNumberOfParticipants"]);
                    excClass.Category = reader["Category"].ToString();
                    excClass.Payable = Convert.ToBoolean(reader["IsPayable"]) ? "Yes" : "No";
                    excClass.DefaultPrice = reader["ClassDefaultPrice"] != DBNull.Value ? Convert.ToDecimal(reader["ClassDefaultPrice"]) : 0;
                    classList.Add(excClass);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return classList;
        }
    }
}
