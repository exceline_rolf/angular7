﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateIBookingPaymentsDetailsAction : USDBActionBase<bool>
    {
        protected int _bookingID = -1;
        protected int _memberID = -1;
        protected int _aritemNo = -1;
        protected decimal _amount = 0;
        protected string _refID = string.Empty;
        public UpdateIBookingPaymentsDetailsAction(int bookingID, int memberID, int arItemNO, decimal amount, string refID)
        {
            _bookingID = bookingID;
            _memberID = memberID;
            _aritemNo = arItemNO;
            _amount = amount;
            _refID = refID;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USGMSIBookingUpdateBookingPaymentDetails";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BookingID", System.Data.DbType.Int32, _bookingID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", System.Data.DbType.Int32, _memberID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNO", System.Data.DbType.Int32, _aritemNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Amount", System.Data.DbType.Decimal, _amount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ItemType", System.Data.DbType.String, "BS"));
                command.Parameters.Add(DataAcessUtils.CreateParam("@RefID", System.Data.DbType.String, _refID));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
