﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingMemberChangesAction : USDBActionBase<List<ExceIBookingMember>>
    {
        private readonly int _branchId;
        private readonly int _changedSinceDays;
        private readonly int _companyId;
        public GetIBookingMemberChangesAction(int branchId, int changedSinceDays, int companyID)
        {
            _branchId = branchId;
            _changedSinceDays = changedSinceDays;
            _companyId = companyID;
        }
        protected override List<ExceIBookingMember> Body(DbConnection connection)
        {
            List<ExceIBookingMember> memberList = new List<ExceIBookingMember>();
            const string storedProcedure = "USExceGMSIBookingGetMemberChanges";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@changedSinceDays", DbType.Int32, _changedSinceDays));

                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExceIBookingMember member = new ExceIBookingMember();
                    member.CustomerNo = Convert.ToString(reader["CustomerNumber"]);
                    member.GymID = Convert.ToString(reader["BranchId"]);
                    member.FirstName = reader["FirstName"].ToString();
                    member.LastName = reader["LastName"].ToString();
                    member.BirthDate = Convert.ToString(reader["BirthDate"]);
                    member.Address1 = reader["Address1"].ToString();
                    member.Address2 = reader["Address2"].ToString();
                    member.Address3 = reader["Address3"].ToString();
                    member.PostCode = reader["PostCode"].ToString();
                    member.PostPlace = reader["PostPlace"].ToString();
                    member.Mobile = Convert.ToString(reader["Mobile"]).Trim();
                    member.WorkTeleNo = Convert.ToString(reader["TeleWork"]).Trim();
                    member.PrivateTeleNo = Convert.ToString(reader["TeleHome"]).Trim();
                    member.Email = reader["Email"].ToString();
                    member.Gender = reader["Gender"].ToString();
                    member.Role = reader["Role"].ToString();
                    member.Active = (Convert.ToBoolean(reader["ActiveStatus"]) ? "YES" : "NO");
                    member.ExpieryDate = Convert.ToString(reader["ExpieryDate"]);
                    member.CardNumber = Convert.ToString(reader["CardNumber"]).Trim();
                    member.NoCommercial = (Convert.ToBoolean(reader["SendAdvertisement"]) ? "YES" : "NO");
                    member.RegisterdDate = Convert.ToString(reader["RegisterdDatetime"]);
                    member.LastVisitDate = Convert.ToString(reader["LastVisitDateTime"]); 
                    member.FirmNumber = _companyId.ToString();
                    member.DepartmentNumber = Convert.ToString(reader["BranchId"]);
                    member.CurrentOffering = Convert.ToString(reader["CurrentOffering"]);
                    member.UpdatedDateTime = Convert.ToString(reader["UpdatedDateTime"]); 
                    member.AutoGiro = Convert.ToString(reader["ATGKid"]).Trim();
                    member.ContractNumber = Convert.ToString(reader["ContractNumber"]).Trim();
                    member.ContractStartdate = Convert.ToString(reader["ContractStartdate"]);
                    member.ContractSaldo = Convert.ToDecimal(reader["ContractRemainingBalance"]);
                    member.ContractMaxrate = Convert.ToDecimal(reader["MaxRate"]);
                    member.ContractAvtGiroStatus = Convert.ToString(reader["ATGStatus"]).Trim();
                    member.CountryCode = Convert.ToString(reader["CountryCode"]);
                    member.GatCarNumber = Convert.ToString(reader["GATCardNo"]);
                    member.AccessProfileID = Convert.ToInt32(reader["AccessProfileId"]);
                    member.FreezeStartDate = Convert.ToString(reader["FreezeStartDate"]);
                    member.FreezeEndDate = Convert.ToString(reader["FreezeEndDate"]);
                    member.InstructorID = Convert.ToInt32(reader["InstructorId"]);
                    member.PinCode = Convert.ToString(reader["PinCode"]);
                    member.GuestCardNo = Convert.ToString(reader["GuestCardNo"]);
                    member.SendEmail = Convert.ToBoolean(reader["SendEmail"]);
                    member.SendSMS = Convert.ToBoolean(reader["SendSMS"]);
                    member.CustomerClub = Convert.ToBoolean(reader["CustomerClub"]);
                    if (reader["EmployeeId"] != DBNull.Value)
                        member.EmployeeId = Convert.ToString(reader["EmployeeId"]);
                    memberList.Add(member);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return memberList;
        }
    }
}
