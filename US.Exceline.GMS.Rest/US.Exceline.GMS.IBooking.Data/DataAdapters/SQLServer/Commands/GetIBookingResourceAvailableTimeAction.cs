﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingResourceAvailableTimeAction : USDBActionBase<List<ExceIBookingResourceAvailableTime>>
    {
        private readonly int _branchId;
        private readonly int _resourceId;
        public GetIBookingResourceAvailableTimeAction(int branchId, int resourceId)
        {
            _branchId = branchId;
            _resourceId = resourceId;
        }

        protected override List<ExceIBookingResourceAvailableTime> Body(DbConnection connection)
        {
            var exceAvailableTime = new List<ExceIBookingResourceAvailableTime>();
            const string storedProcedure = "USExceGMSIBookingGetResourceAvailableTime";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Branchid", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Resourceid", DbType.Int32, _resourceId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var exceIBookingResourceSchedule = new ExceIBookingResourceAvailableTime
                        {
                            Id = Convert.ToInt32(reader["ID"]),
                            Day = reader["Day"].ToString(),
                            FromTime = reader["FromTime"].ToString(),
                            ToTime = reader["ToTime"].ToString()
                        };

                    exceAvailableTime.Add(exceIBookingResourceSchedule);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return exceAvailableTime;
        }
    
    }
}
