﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingInvoicesAction : USDBActionBase<List<ExceIBookingInvoice>>
    {
        private int _branchID =-1;
        private DateTime _startDate;
        private DateTime _endDate;
        private string _documentService = string.Empty;
        private string _folderPath = string.Empty;

        public GetIBookingInvoicesAction(int branchID, DateTime startDate, DateTime endDate, string documentService, string folderPath)
        {
            _branchID = branchID;
            _startDate = startDate;
            _endDate = endDate;
            _documentService = documentService;
            _folderPath = folderPath;
        }

        protected override List<ExceIBookingInvoice> Body(System.Data.Common.DbConnection connection)
        {
            List<ExceIBookingInvoice> invoices = new List<ExceIBookingInvoice>();
            string spName = "USExceGMSIBookingGetInvoices";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", System.Data.DbType.Date, _startDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", System.Data.DbType.Date, _endDate));
                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    ExceIBookingInvoice invoice = new ExceIBookingInvoice();
                    invoice.Id = Convert.ToInt32(reader["ARItemNo"]);
                    invoice.InvoiceNo = Convert.ToString(reader["Ref"]);
                    invoice.InvoiceDate = Convert.ToString(reader["VoucherDate"]);
                    invoice.DueDate = Convert.ToString(reader["DueDate"]);
                    invoice.ContractNumber = Convert.ToString(reader["MemberContractNo"]);
                    invoice.Amount = Convert.ToDecimal(reader["Amount"]);
                    invoice.Balance = Convert.ToDecimal(reader["Balance"]);
                    invoice.CustomerId = Convert.ToString(reader["CustId"]);
                    invoice.OriginalCustomerId = Convert.ToString(reader["OriginalCustomer"]);

                    string documentPath = Convert.ToString(reader["FilePath"]);
                    if (!string.IsNullOrEmpty(documentPath))
                    {
                        documentPath = documentPath.Replace(_folderPath, "");
                        documentPath = documentPath.Replace(@"\", @"/");
                        invoice.URL = _documentService + documentPath;
                    }
                    else
                    {
                        invoice.URL = string.Empty;
                    }
                    invoices.Add(invoice);
                }
                return invoices;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
