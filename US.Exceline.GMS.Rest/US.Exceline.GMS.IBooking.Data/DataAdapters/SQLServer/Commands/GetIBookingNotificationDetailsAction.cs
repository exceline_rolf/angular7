﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingNotificationDetailsAction : USDBActionBase<ExceIBookingNotificationDetails>
    {
        private string _templateNo = string.Empty;
        private int _branchID = -1;
        private string _type = string.Empty;
        public GetIBookingNotificationDetailsAction(string templateNo, int branchID, string type)
        {
            _templateNo = templateNo;
            _branchID = branchID;
            _type = type;
        }
        protected override ExceIBookingNotificationDetails Body(System.Data.Common.DbConnection connection)
        {
            ExceIBookingNotificationDetails notificationDetails = null;
            string spName = "USExceGMSIBookingGetNotificationDetail";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@TemplateNo", System.Data.DbType.String, _templateNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Type", System.Data.DbType.String, _type));
                
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    notificationDetails = new ExceIBookingNotificationDetails();
                    notificationDetails.BranchId = _branchID;
                    notificationDetails.GymName = Convert.ToString(reader["GymName"]);
                    notificationDetails.ATGAmount = Convert.ToDecimal(reader["AmountForATG"]);
                }
                return notificationDetails;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
