﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    class GetAllContractsAction : USDBActionBase<List<ExceIBookingAllContracts>>
    {
        private readonly int _systemId;

        public GetAllContractsAction(int systemId)
        {
            _systemId = systemId;
        }

        protected override List<ExceIBookingAllContracts> Body(DbConnection connection)
        {
            List<ExceIBookingAllContracts> contractList = new List<ExceIBookingAllContracts>();
            const string storedProcedure = "USExceGMSIBookingGetAllContracts";

            string tempValue = string.Empty;
            try
            {
                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExceIBookingAllContracts contract = new ExceIBookingAllContracts();

                    contract.ContractID = Convert.ToInt32(reader["ContractID"]);
                    contract.CustomerID = Convert.ToInt32(reader["CustId"]);
                    contract.TemplateNo = Convert.ToInt32(reader["TemplateNo"]);
                    contract.ContractTempName = reader["TemplateName"].ToString();
                    if (reader["ContractStartDate"] != DBNull.Value)
                        contract.ContractStartDate = Convert.ToString(reader["ContractStartDate"]);
                    if (reader["ContractEndDate"] != DBNull.Value)
                        contract.ContractEndDate = Convert.ToString(reader["ContractEndDate"]);
                    contract.Activity = reader["Activity"].ToString();
                    contract.BranchId = Convert.ToInt32(reader["BranchId"]);
                    contract.AvailableVisits = Convert.ToInt32(reader["AvailableVisits"]);
                    contract.RecordedVisits = Convert.ToInt32(reader["RecordedVisits"]);
                    contract.ContractType = Convert.ToInt32(reader["ContractType"]);

                    contractList.Add(contract);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return contractList;
        }
    }
}