﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingInstructorsAction : USDBActionBase<List<ExceIBookingInstructor>>
    {
        public GetIBookingInstructorsAction()
        {
        }
        protected override List<ExceIBookingInstructor> Body(DbConnection connection)
        {
            List<ExceIBookingInstructor> instructorList = new List<ExceIBookingInstructor>();
            const string storedProcedure = "USExceGMSIBookingGetInstructors";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExceIBookingInstructor excInstructor = new ExceIBookingInstructor();
                    excInstructor.InstructorId = Convert.ToInt32(reader["InstructorId"]);
                    excInstructor.InstructorName = reader["FirstName"].ToString();
                    excInstructor.GymId = Convert.ToInt32(reader["BranchId"]);
                    instructorList.Add(excInstructor);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return instructorList;
        }
    }
}
