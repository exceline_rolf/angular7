﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class CancelBookingPaymentOrderAction : USDBActionBase<bool>
    {
        protected int _bookingID = -1;
        protected int _orderId = -1;
        protected string _description = string.Empty;

        public CancelBookingPaymentOrderAction(int bookingID, int OrderID, string description)
        {
            _bookingID = bookingID;
            _orderId = OrderID;
            _description = description;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USGMSIBookingCancelPaymentOrder";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BookingID", System.Data.DbType.Int32, _bookingID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@OrderID", System.Data.DbType.Int32, _orderId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Description", System.Data.DbType.String, _description));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
