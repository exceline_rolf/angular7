﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.IBooking.Core;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetBookingDetailsAction : USDBActionBase<BookingDetails>
    {
        private int _bookingID = -1;
        public GetBookingDetailsAction(int bookingID)
        {
            _bookingID = bookingID;
        }
        protected override BookingDetails Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USGMSIBookingGetBookingDetails";
            BookingDetails bookingDetails = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BookingID", System.Data.DbType.Int32, _bookingID));

                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    bookingDetails = new BookingDetails();
                    bookingDetails.ArticleID = Convert.ToInt32(reader["ArticleID"]);
                    bookingDetails.ArticleName = Convert.ToString(reader["ArticleName"]);
                    bookingDetails.BookingMemberID = Convert.ToInt32(reader["MemberId"]);
                    bookingDetails.ResourceID = Convert.ToInt32(reader["ResourceID"]);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return bookingDetails;
        }
    }
}
