﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingGymsAction : USDBActionBase<List<ExceIBookingGym>>
    {
        public GetIBookingGymsAction()
        {
        }
        protected override List<ExceIBookingGym> Body(DbConnection connection)
        {
            List<ExceIBookingGym> branchList = new List<ExceIBookingGym>();
            const string storedProcedure = "USExceGMSIBookingGetBranches";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExceIBookingGym excBranch = new ExceIBookingGym();
                    excBranch.GymId = Convert.ToInt32(reader["BranchId"]);
                    excBranch.GymName = reader["BranchName"].ToString();
                    excBranch.Address1 = reader["Addr1"].ToString();
                    excBranch.Address2 = reader["Addr2"].ToString();
                    excBranch.Address3 = reader["Addr3"].ToString();
                    excBranch.PostCode = reader["ZipCode"].ToString();
                    excBranch.PostPlace = reader["ZipName"].ToString();
                    excBranch.GymAccountNo = reader["AccountNo"].ToString();
                    branchList.Add(excBranch);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return branchList;
        }
    }
}
