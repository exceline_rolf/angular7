﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractSummariesForIBookingAction : USDBActionBase<List<ContractSummaryDC>>
    {
        private string _customerNo = string.Empty;
        private string _gymCode = string.Empty;
        public GetContractSummariesForIBookingAction(string customerNO, string gymCode)
        {
            _customerNo = customerNO;
            _gymCode = gymCode;
        }

        protected override List<ContractSummaryDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ContractSummaryDC> memberContractList = new List<ContractSummaryDC>();
            string spName = "USExceGMSIBookingGetMemberContractsSummary";
            DbDataReader reader = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@customerNo", System.Data.DbType.String, _customerNo));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ContractSummaryDC contract = new ContractSummaryDC();
                    contract.Id = Convert.ToInt32(reader["ID"]);
                    contract.ContractNo = Convert.ToString(reader["ContractNo"]);
                    contract.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    contract.TemplateName = Convert.ToString(reader["TemplateName"]);
                    contract.ActivityName = Convert.ToString(reader["ActivityName"]);
                    if (reader["CreatedDate"] != DBNull.Value)
                        contract.SignedDate = Convert.ToDateTime(reader["CreatedDate"]);
                    if (reader["ContractEndDate"] != DBNull.Value)
                        contract.EndDate = Convert.ToDateTime(reader["ContractEndDate"]);

                    if (reader["ContractStartDate"] != DBNull.Value)
                        contract.StartDate = Convert.ToDateTime(reader["ContractStartDate"]);

                    if (reader["RenewedDate"] != DBNull.Value)
                        contract.RenewedDate = Convert.ToDateTime(reader["RenewedDate"]);
                    contract.NoOfOrders = Convert.ToInt32(reader["NumberofInstallments"]);
                    contract.StartUpItemsPrice = Convert.ToDecimal(reader["StartUpItemPrice"]);
                    contract.MonthlyitemPrice = Convert.ToDecimal(reader["EveryMonthItemPrice"]);
                    contract.TotalPrice = Convert.ToDecimal(reader["PackagePrice"]);
                    contract.Status = Convert.ToString(reader["Status"]);
                    contract.AccessProfileId = Convert.ToInt32(reader["AccessProfileId"]);
                    contract.BranchId = Convert.ToInt32(reader["BranchId"]);
                    contract.RemainingVisits = Convert.ToInt32(reader["Visits"]);
                    contract.ContractTypeCode = Convert.ToString(reader["ContractTypeCode"]);
                    contract.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    contract.IsGroupContract = Convert.ToBoolean(reader["IsGroupContract"]);
                    contract.IsFreezed = Convert.ToBoolean(reader["IsFreezed"]);
                    memberContractList.Add(contract);
                }

                if (reader != null)
                    reader.Close();

                foreach (ContractSummaryDC summary in memberContractList)
                {
                    GetIBookingContractAccessTimesAction action = new GetIBookingContractAccessTimesAction(summary.AccessProfileId, summary.BranchId);
                    summary.AccesTimes = action.Execute(EnumDatabase.Exceline, _gymCode);
                }
                return memberContractList;
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
