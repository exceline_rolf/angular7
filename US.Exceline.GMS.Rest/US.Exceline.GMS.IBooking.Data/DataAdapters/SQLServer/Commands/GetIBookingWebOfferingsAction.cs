﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingWebOfferingsAction : USDBActionBase<List<ExceIBookingWebOffering>>
    {
        public GetIBookingWebOfferingsAction()
        {
        }
        protected override List<ExceIBookingWebOffering> Body(DbConnection connection)
        {
            List<ExceIBookingWebOffering> webOfferingList = new List<ExceIBookingWebOffering>();
            const string storedProcedure = "USExceGMSIBookingGetTemplates";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExceIBookingWebOffering exceWebOffering = new ExceIBookingWebOffering();
                    exceWebOffering.OfferingId = Convert.ToInt32(reader["OfferingId"]);
                    exceWebOffering.GymId = Convert.ToInt32(reader["BranchId"]);
                    exceWebOffering.OfferingName = reader["OfferingName"].ToString();
                    exceWebOffering.ActivityType = Convert.ToInt32(reader["ActivityType"]);
                    exceWebOffering.FromDate = Convert.ToString(reader["FromDate"]);
                    exceWebOffering.ToDate = Convert.ToString(reader["ToDate"]);
                    exceWebOffering.InitialPrice = Convert.ToDecimal(reader["InitialPrice"]);
                    exceWebOffering.MonthlyPrice = Convert.ToDecimal(reader["MonthlyPrice"]);
                    exceWebOffering.AvtaleGiro = Convert.ToBoolean(reader["IsATG"]);
                    exceWebOffering.RestPlusMonth = Convert.ToBoolean(reader["RestPluesMonth"]);
                    exceWebOffering.Condition = Convert.ToString(reader["Condition"]);
                    exceWebOffering.ShowOnNet = Convert.ToBoolean(reader["IsShownOnTheNet"]);
                    webOfferingList.Add(exceWebOffering);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return webOfferingList;
        }
    }
}
