﻿using System;
using System.Data.SqlClient;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateIBookingClassAction : USDBActionBase<int>
    {
        private readonly ExceIBookingUpdateClass _exceClass;

        public UpdateIBookingClassAction(ExceIBookingUpdateClass exceClass)
        {
            _exceClass = exceClass;
        }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSIBookingUpdateClass";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _exceClass.GymId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _exceClass.ClassId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@visitCount", DbType.Int32, _exceClass.VisitCount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@visitPercentage", DbType.Decimal, _exceClass.VisitPercentage));
                if(!string.IsNullOrEmpty(_exceClass.NewInstructorId))
                   cmd.Parameters.Add(DataAcessUtils.CreateParam("@newInstructorId", DbType.Int32, _exceClass.NewInstructorId));
                if(!string.IsNullOrEmpty(_exceClass.OriginalInstructorId))
                   cmd.Parameters.Add(DataAcessUtils.CreateParam("@OriginalInstructor", DbType.Int32, _exceClass.OriginalInstructorId));
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@OutID";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                return Convert.ToInt32(output.Value);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
