﻿using System;
using System.Collections.Generic;
using System.Linq;
using US_DataAccess;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingClassTypesAction : USDBActionBase<List<ExceIBookingClassType>>
    {

        public GetIBookingClassTypesAction()
        {

        }
        protected override List<ExceIBookingClassType> Body(DbConnection connection)
        {
            List<ExceIBookingClassType> classTypeList = new List<ExceIBookingClassType>();
            const string storedProcedure = "USExceGMSIBookingGetClassTypes";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExceIBookingClassType classType = new ExceIBookingClassType();
                    classType.ClassCategoryIds=new List<int>();
                    classType.ClassKeywordIds = new List<int>();
                    classType.ClasstypeId = Convert.ToInt32(reader["Id"]);
                    classType.ClasstypeName = reader["Name"].ToString();
                    if (reader["ClassGroupId"] != DBNull.Value)
                    {
                        classType.ClassGroup = Convert.ToInt32(reader["ClassGroupId"]);
                    }

                    if (reader["CategoryIds"] != DBNull.Value)
                    {
                        string[] ids = Convert.ToString(reader["CategoryIds"]).Split(new char[] { ',' });
                        foreach(string id in ids)
                            classType.ClassCategoryIds .Add(Convert.ToInt32(id));
                    }

                    if (reader["KeywordIds"] != DBNull.Value)
                    {
                        string[] ids = Convert.ToString(reader["KeywordIds"]).Split(new char[] { ',' });
                        foreach (string id in ids)
                            classType.ClassKeywordIds.Add(Convert.ToInt32(id));
                    }
                    if (reader["ClassLevelId"] != DBNull.Value)
                        classType.ClassLevelId = Convert.ToInt32(reader["ClassLevelId"]);
                    classType.ColorCode = Convert.ToString(reader["Colour"]);
                    classType.ClassDescription = Convert.ToString(reader["Comment"]);
                    classType.EnglishComment = Convert.ToString(reader["EnglishComment"]);
                    classTypeList.Add(classType);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return classTypeList;
        }
    }
}
