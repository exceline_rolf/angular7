﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingClassCategoriesAction : USDBActionBase<List<ExceIBookingClassCategory>>
    {

        public GetIBookingClassCategoriesAction()
        {
         
        }

        protected override List<ExceIBookingClassCategory> Body(DbConnection connection)
        {
            List<ExceIBookingClassCategory> classList = new List<ExceIBookingClassCategory>();
            const string storedProcedure = "USExceGMSIBookingGetClassCategories";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExceIBookingClassCategory excClassCategory = new ExceIBookingClassCategory();
                    excClassCategory.ClasscategoryId = Convert.ToInt32(reader["ClassCategoryId"]);
                    excClassCategory.ClasscategoryName = reader["ClassCategoryName"].ToString();
                    classList.Add(excClassCategory);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return classList;
        }
    }
}
