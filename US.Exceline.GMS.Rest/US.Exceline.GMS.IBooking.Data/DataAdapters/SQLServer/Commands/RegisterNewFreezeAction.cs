﻿using System;
using System.Data.SqlClient;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data;
using System.Globalization;
using System.Collections.Generic;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class RegisterNewFreezeAction : USDBActionBase<int>
    {
        private readonly ExceIBookingNewFreeze _freezeItem;       

        public RegisterNewFreezeAction(ExceIBookingNewFreeze freeze)
        {
            _freezeItem = freeze;
        }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSIBookingAddFreeze";
            
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", DbType.Int32, _freezeItem.MemberContractid));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryId", DbType.Int32, _freezeItem.CategoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", DbType.DateTime, DateTime.ParseExact(_freezeItem.FromDate, "yyyy.MM.dd", CultureInfo.InvariantCulture)));                
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@note", DbType.String, _freezeItem.Note));                
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@freezeMonths", DbType.Int32, _freezeItem.FreezeMonths));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@extendedStatus", DbType.Boolean, _freezeItem.IsExtended));
          

                object obj = cmd.ExecuteScalar();
                int freezeItemId = Convert.ToInt32(obj);
                return freezeItemId;
                //return 1;

            }
            catch (Exception ex)
            {
                return -1;
            }
     
        }
    }
}
