﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
   public class GetIBookingResourceSwitchDataRangeAction : USDBActionBase<List<ExceIBookingResourceScheduleTime>>
   {
       private readonly int _scheduleItemId = -1;
       private readonly DateTime _fromDate;
       private readonly DateTime _toDate;
       public GetIBookingResourceSwitchDataRangeAction(int scheduleItemId,DateTime fromDate,DateTime toDate)
       {
           _scheduleItemId = scheduleItemId;
           _fromDate = fromDate;
           _toDate = toDate;
       }
        protected override List<ExceIBookingResourceScheduleTime> Body(DbConnection connection)
        {
            var exceIBookingList = new List<ExceIBookingResourceScheduleTime>();
            const string storedProcedure = "USExceGMSIBookingGetResourceSwitchDataRange";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Scheduleitemid", DbType.Int32, _scheduleItemId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Fromdate", DbType.DateTime, _fromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Todate", DbType.DateTime, _toDate));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var exceIBookingResourceSchedule = new ExceIBookingResourceScheduleTime();
                    exceIBookingResourceSchedule.Id = Convert.ToInt32(reader["ID"]);
                    exceIBookingResourceSchedule.FromTime = reader["FromTime"].ToString();
                    exceIBookingResourceSchedule.ToTime = reader["ToTime"].ToString();
                  
                    exceIBookingList.Add(exceIBookingResourceSchedule);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return exceIBookingList;
        }
    }
}
