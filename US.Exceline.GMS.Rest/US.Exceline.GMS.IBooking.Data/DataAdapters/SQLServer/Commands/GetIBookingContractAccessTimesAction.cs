﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingContractAccessTimesAction : USDBActionBase<List<ContractAccessTimeDC>>
    {
        private int _accessProfileId = -1;
        private int _branchId = -1;
        public GetIBookingContractAccessTimesAction(int accessProfileID, int branchID)
        {
            _accessProfileId = accessProfileID;
            _branchId = branchID;
        }

        protected override List<ContractAccessTimeDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ContractAccessTimeDC> accesTimes = new List<ContractAccessTimeDC>();
            string spName = "USExceGMSIBookingGetContractTime";
            DbDataReader reader = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@AccessProfileId", System.Data.DbType.Int32, _accessProfileId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));

                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ContractAccessTimeDC time = new ContractAccessTimeDC();
                    time.Day = Convert.ToString(reader["AccessDay"]);
                    if (reader["FromTime"] != DBNull.Value)
                        time.InTime = Convert.ToDateTime(reader["FromTime"]);
                    if (reader["ToTime"] != DBNull.Value)
                        time.OutTime = Convert.ToDateTime(reader["ToTime"]);
                    accesTimes.Add(time);
                }
                return accesTimes;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
