﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class AddIBookingStatusAction : USDBActionBase<bool>
    {
        public ExceIBookingNewMember _newMember = null;
        private int _status = -1;
        private string _description = string.Empty;

        public AddIBookingStatusAction(ExceIBookingNewMember newMember, int status, string description)
        {
            _newMember = newMember;
            _status = status;
            _description = description;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USGMSIBookingAddHistory ";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@FirstName", System.Data.DbType.String, _newMember.FirstName));
                command.Parameters.Add(DataAcessUtils.CreateParam("@LastName", System.Data.DbType.String, _newMember.LastName));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Mobile", System.Data.DbType.String, _newMember.Mobile));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Status", System.Data.DbType.Int32, _status));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Description", System.Data.DbType.String, _description));
                command.ExecuteNonQuery();
                
            }
            catch (Exception)
            {
            }
            return true;
        }
    }
}
