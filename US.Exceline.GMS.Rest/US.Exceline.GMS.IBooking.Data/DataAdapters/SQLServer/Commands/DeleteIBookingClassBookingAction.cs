﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteIBookingClassBookingAction : USDBActionBase<int>
    {
        private int _systemId;
        private int _branchId;
        private int _memberId;
        private int _classId;

        public DeleteIBookingClassBookingAction(int systemId, int gymId, int classId, int customerId)
        {
            _systemId = systemId;
            _branchId = gymId;
            _memberId = customerId;
            _classId = classId;
        }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSIBookingDeleteClassBooking";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.Boolean;
                output.ParameterName = "@OutId";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                return Convert.ToInt32(output.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
