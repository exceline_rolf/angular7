﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingClassSchedulesAction : USDBActionBase<List<ExceIBookingClassCalendar>>
    {
        private readonly int _branchId;
        private readonly DateTime _fromDate;
        private readonly DateTime _toDate;

        public GetIBookingClassSchedulesAction(int branchId, DateTime fromDate, DateTime toDate)
        {
            _branchId = branchId;
            _fromDate = fromDate;
            _toDate = toDate;
        }

        protected override List<ExceIBookingClassCalendar> Body(DbConnection connection)
        {
            List<ExceIBookingClassCalendar> excelineClassScheduleList = new List<ExceIBookingClassCalendar>();
            const string storedProcedureName = "USExceGMSIBookingGetClassScheduleByTimePeriod";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", DbType.DateTime, _fromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@toDate", DbType.DateTime, _toDate));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExceIBookingClassCalendar excelineClassSchedule = new ExceIBookingClassCalendar();
                    excelineClassSchedule.InstructorIds = new List<int>();
                    excelineClassSchedule.ClassId = Convert.ToInt32(reader["ClassId"]);
                    excelineClassSchedule.ClassDate = Convert.ToString(reader["ClassDate"]);
                    excelineClassSchedule.ClassStarttime = Convert.ToString(reader["StartDateTime"]);
                    excelineClassSchedule.ClassEndtime = Convert.ToString(reader["EndDateTime"]);
                    excelineClassSchedule.MaxParticipants = Convert.ToInt32(reader["ClassMaxParticipants"]);
                    excelineClassSchedule.ClasstypeId = Convert.ToInt32(reader["ClassTypeId"]);

                    if (reader["InstructorIds"] != DBNull.Value)
                    {
                        string[] ids = Convert.ToString(reader["InstructorIds"]).Split(new char[] { ',' });
                        foreach (string id in ids)
                            excelineClassSchedule.InstructorIds.Add(Convert.ToInt32(id));
                    }
                    excelineClassSchedule.ClassRoom = Convert.ToString(reader["ClassRoom"]);
                    excelineClassSchedule.VisitPercentage = Convert.ToInt32(reader["VisitPercentage"]);
                    excelineClassSchedule.VisitCount = Convert.ToInt32(reader["VisitCount"]);
                    excelineClassScheduleList.Add(excelineClassSchedule);
                }
                return excelineClassScheduleList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
