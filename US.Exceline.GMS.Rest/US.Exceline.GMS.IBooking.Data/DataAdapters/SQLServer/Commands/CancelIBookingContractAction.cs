﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.Exceline.GMS.IBooking.Core;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class CancelIBookingContractAction : USDBActionBase<ContractResignDetails>
    {

        private int _systemId;
        private int _branchId;
        private int _custID;
        private DateTime _endDate = DateTime.MinValue;

        public CancelIBookingContractAction(int systemId, int gymId, int customerId)
        {
            _systemId = systemId;
            _branchId = gymId;
            _custID = customerId;
        }

        protected override ContractResignDetails Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSIBookingUpdatContract";
            ContractResignDetails contractDetails = null;
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CustID", DbType.Int32, _custID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    contractDetails = new ContractResignDetails();
                    contractDetails.ID = Convert.ToInt32(reader["ID"]);
                    if(reader["ContractEndDate"] != DBNull.Value)
                    {
                        contractDetails.ContractEndDate = Convert.ToDateTime(reader["ContractEndDate"]);
                    }
                   
                    contractDetails.OrderCount = Convert.ToInt32(reader["NumberOfOrders"]);
                    contractDetails.OrderAmount = Convert.ToDecimal(reader["TotalAmount"]);

                    if (reader["LastDueDate"] != DBNull.Value)
                    {
                        contractDetails.LastDueDate = Convert.ToDateTime(reader["LastDueDate"]);
                    }
                }

                return contractDetails;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
