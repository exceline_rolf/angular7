﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingCategoriesByTypeAction : USDBActionBase<List<CategoryDC>>
    {
        private readonly string _type;

        public GetIBookingCategoriesByTypeAction(string type)
        {
            _type = type;
        }

        protected override List<CategoryDC> Body(DbConnection connection)
        {
            List<CategoryDC> categoryList = new List<CategoryDC>();
            const string storedProcedureName = "USExceGMSIBookingGetCategoriesByType";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, _type));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    CategoryDC category = new CategoryDC();
                    category.Id = Convert.ToInt32(reader["ID"]);
                    category.TypeId = Convert.ToInt32(reader["CategoryTypeId"]);
                    category.Code = reader["Code"].ToString();
                    category.Name = reader["Description"].ToString();
                    //category.CategoryImage = reader["Image"];//TODO:
                    category.CreatedUser = reader["CreatedUser"].ToString();
                    category.LastModifiedUser = reader["LastModifiedUser"].ToString();

                    if (!string.IsNullOrEmpty(reader["CreatedDateTime"].ToString()))
                    {
                        category.CreatedDate = Convert.ToDateTime(reader["CreatedDateTime"]);
                    }
                    if (!string.IsNullOrEmpty(reader["LastModifiedDateTime"].ToString()))
                    {
                        category.CreatedDate = Convert.ToDateTime(reader["LastModifiedDateTime"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ActiveStatus"].ToString()))
                    {
                        category.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    }
                    if (reader["Color"] != DBNull.Value)
                        category.Color = reader["Color"].ToString();

                    categoryList.Add(category);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return categoryList;
        }
    }
}
