﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetTodayOrderAction : USDBActionBase<List<InstallmentDC>>
    {
        public int _memberID = -1;
        private string _gymCode = string.Empty;
        private int _memberContractID = -1;
        public GetTodayOrderAction(int memberID, int memberContractID, string gymCode)
        {
            _memberID = memberID;
            _gymCode = gymCode;
            _memberContractID = memberContractID;
        }

        protected override List<InstallmentDC> Body(System.Data.Common.DbConnection connection)
        {
            List<InstallmentDC> installmentsList = new List<InstallmentDC>();
            string spName = "USExceGMSIBookingGettodayOrders";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", System.Data.DbType.Int32, _memberID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberContractID", System.Data.DbType.Int32, _memberContractID));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    InstallmentDC installment = new InstallmentDC();
                    installment.Id = Convert.ToInt32(reader["ID"]);
                    installment.TableId = Convert.ToInt32(reader["ID"]);
                    installment.MemberId = Convert.ToInt32(reader["MemberId"]);
                    installment.MemberContractId = Convert.ToInt32(reader["MemberContractId"]);
                    installment.MemberContractNo = Convert.ToString(reader["MemberContractNo"]);
                    installment.Text = Convert.ToString(reader["Text"]);
                    installment.InstallmentDate = Convert.ToDateTime(reader["InstallmentDate"]);
                    installment.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    installment.Amount = Convert.ToDecimal(reader["Amount"]);
                    if (reader["AddOnPrice"] != DBNull.Value)
                        installment.AdonPrice = Convert.ToDecimal(reader["AddOnPrice"]);
                    if (reader["Discount"] != DBNull.Value)
                        installment.Discount = Convert.ToDecimal(reader["Discount"]);
                    if (reader["SponsoredAmount"] != DBNull.Value)
                        installment.SponsoredAmount = Convert.ToDecimal(reader["SponsoredAmount"]);
                    installment.Comment = reader["Comment"].ToString();
                    if (reader["OriginalMatuarity"] != DBNull.Value)
                        installment.OriginalDueDate = Convert.ToDateTime(reader["OriginalMatuarity"]);
                    installment.Balance = Convert.ToDecimal(reader["Balance"]);
                    if (reader["InvoiceGeneratedDate"] != DBNull.Value)
                        installment.InvoiceGeneratedDate = Convert.ToDateTime(reader["InvoiceGeneratedDate"]);
                    installment.IsInvoiced = Convert.ToBoolean(reader["InvoiceGenerated"]);
                    installment.InstallmentNo = Convert.ToInt32(reader["InstallmentNo"]);
                    if (reader["TrainingPeriodStart"] != DBNull.Value)
                        installment.TrainingPeriodStart = Convert.ToDateTime(reader["TrainingPeriodStart"]);
                    if (reader["TrainingPeriodEnd"] != DBNull.Value)
                        installment.TrainingPeriodEnd = Convert.ToDateTime(reader["TrainingPeriodEnd"]);
                    installment.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    installment.IsOtherPay = Convert.ToBoolean(reader["IsOtherPay"]);
                    installment.ContractName = Convert.ToString(reader["TemplateName"]);
                    installment.ActivityName = Convert.ToString(reader["ActivityName"]);
                    installment.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    installment.OriginalCustomer = Convert.ToString(reader["OriginalMember"]);
                    installment.EstimatedOrderAmount = Convert.ToDecimal(reader["EstimatedOrderAmount"]);
                    installment.OrderNo = Convert.ToString(reader["OrderNo"]);
                    installment.ActivityPrice = Convert.ToDecimal(reader["ActivityPrice"]);
                    if (reader["EstimatedInvoiceDate"] != DBNull.Value)
                        installment.EstimatedInvoiceDate = Convert.ToDateTime(reader["EstimatedInvoiceDate"]);
                    installment.AdonText = Convert.ToString(reader["AdonText"]);
                    installment.PayerName = Convert.ToString(reader["PayerName"]);
                    if (reader["PayerID"] != DBNull.Value)
                        installment.PayerId = Convert.ToInt32(reader["PayerID"]);
                    installment.IsSpOrder = Convert.ToBoolean(reader["IsSpOrder"]);
                    installment.IsInvoiceFeeAdded = Convert.ToBoolean(reader["IsInvoiceFee"]);
                    installment.BranchName = Convert.ToString(reader["BranchName"]);
                    installment.ServiceAmount = Convert.ToDecimal(reader["ServiceAmount"]);
                    installment.ItemAmount = Convert.ToDecimal(reader["ItemAmount"]);
                    installment.InstallmentType = Convert.ToString(reader["InstallmentType"]);
                    installment.MemberName = Convert.ToString(reader["MemberName"]);
                    installmentsList.Add(installment);
                }
                reader.Close();

                foreach (InstallmentDC item in installmentsList)
                {
                    GetOrderShareAction adonAction = new GetOrderShareAction(item.Id);
                    item.AddOnList = adonAction.Execute(EnumDatabase.Exceline, _gymCode);
                }
                return installmentsList;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
