﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingClassMemberBookingsAction : USDBActionBase<List<ExceIBookingClassMemberBooking>>
    {
        private int _classId;
        private int _branchId;


        public GetIBookingClassMemberBookingsAction(int classId, int branchId)
        {
            _classId = classId;
            _branchId = branchId;
        }

        protected override List<ExceIBookingClassMemberBooking> Body(DbConnection connection)
        {
            List<ExceIBookingClassMemberBooking> _bookingDetails = new List<ExceIBookingClassMemberBooking>();
            string StoredProcedureName = "USExceGMSIBookingGetMemberBookingDetails";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExceIBookingClassMemberBooking bookingDetail = new ExceIBookingClassMemberBooking();
                    bookingDetail.CustomerId = reader["MemberCustId"].ToString();
                    bookingDetail.ClassId = _classId;
                    bookingDetail.TimeId = Convert.ToInt32(reader["ActiveTimeId"]);
                    bookingDetail.Participants = Convert.ToInt32(reader["NumberOfParticipants"]);
                    bookingDetail.ClassStartTime = reader["StartDateTime"] != DBNull.Value ? Convert.ToDateTime(reader["StartDateTime"]) : (Nullable<DateTime>)null;
                    bookingDetail.ClassEndTime = reader["EndDateTime"] != DBNull.Value ? Convert.ToDateTime(reader["EndDateTime"]) : (Nullable<DateTime>)null;
                    bookingDetail.Paid = Convert.ToBoolean(reader["Paid"]);
                    _bookingDetails.Add(bookingDetail);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _bookingDetails;
        }
    }
}
