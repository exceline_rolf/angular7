﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    class GetFreezeResignCategoriesAction : USDBActionBase<List<ExceIBookingFreezeResignCategories>>
    {

        public GetFreezeResignCategoriesAction()
        {

        }

        protected override List<ExceIBookingFreezeResignCategories> Body(DbConnection connection)
        {
            List<ExceIBookingFreezeResignCategories> classList = new List<ExceIBookingFreezeResignCategories>();
            const string storedProcedure = "USExceGMSIBookingGetFreezeResignCategories";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExceIBookingFreezeResignCategories excFreezeResignCategories = new ExceIBookingFreezeResignCategories();
                    excFreezeResignCategories.ID = Convert.ToInt32(reader["ID"]);
                    excFreezeResignCategories.CategoryTypeId = Convert.ToInt32(reader["CategoryTypeId"]);
                    excFreezeResignCategories.CategoryName = reader["Name"].ToString();
                    excFreezeResignCategories.ActiveStatus = Convert.ToInt32(reader["ActiveStatus"]);
                    excFreezeResignCategories.Code = reader["Code"].ToString();
                    classList.Add(excFreezeResignCategories);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return classList;
        }

    }
}
