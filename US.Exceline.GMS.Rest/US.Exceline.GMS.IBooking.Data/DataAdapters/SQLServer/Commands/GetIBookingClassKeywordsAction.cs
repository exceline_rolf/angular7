﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingClassKeywordsAction : USDBActionBase<List<ExceIBookingClassKeyword>>
    {
        public GetIBookingClassKeywordsAction()
        {
        }

        protected override List<ExceIBookingClassKeyword> Body(DbConnection connection)
        {
            List<ExceIBookingClassKeyword> classList = new List<ExceIBookingClassKeyword>();
            const string storedProcedure = "USExceGMSIBookingGetClassKeywords";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                ;
                while (reader.Read())
                {
                    ExceIBookingClassKeyword keyWord = new ExceIBookingClassKeyword();
                    keyWord.KeyWordId = Convert.ToInt32(reader["Id"]);
                    keyWord.ClassKeywordName = Convert.ToString(reader["KeyWord"]);
                    classList.Add(keyWord);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return classList;
        }
    }
}
