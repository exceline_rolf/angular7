﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetFreezeByCustIdAction : USDBActionBase<List<ExceIBookingListFreeze>>
    {
        private readonly string _custId;
        private readonly int _companyId;

        public GetFreezeByCustIdAction(string custId, int systemId)
        {
            _custId = custId;
            _companyId = systemId;
        }

        protected override List<ExceIBookingListFreeze> Body(System.Data.Common.DbConnection connection)
        {
            List<ExceIBookingListFreeze> memberFreezeList = new List<ExceIBookingListFreeze>();
            string spName = "USExceGMSIBookingGetFreezeContractDetails";
            DbDataReader reader = null;
            try
            {
                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@custId", System.Data.DbType.String, _custId));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExceIBookingListFreeze freeze = new ExceIBookingListFreeze();
                    freeze.Id = Convert.ToInt32(reader["ID"]);
                    freeze.MemberContractid = Convert.ToInt32(reader["MemberContractId"]);
                    freeze.CategoryId = Convert.ToInt32(reader["CategoryId"]);                   

                    if (reader["FromDate"] != DBNull.Value)
                        freeze.FromDate = ((Convert.ToDateTime(reader["FromDate"])).ToUniversalTime() - epoch).TotalMilliseconds.ToString();

                    if (reader["ToDate"] != DBNull.Value)
                        freeze.ToDate = ((Convert.ToDateTime(reader["ToDate"])).ToUniversalTime() - epoch).TotalMilliseconds.ToString();

                    freeze.Note = Convert.ToString(reader["Note"]);
                    freeze.FreezeDays = Convert.ToInt32(reader["FreezeDays"]);
                    freeze.ShiftCount = Convert.ToInt32(reader["ShiftCount"]);
                    freeze.ShiftStartInstallmentId = Convert.ToInt32(reader["ShiftStartInstallmentId"]);
                    freeze.ActiveStatus = Convert.ToInt32(reader["ActiveStatus"]);
                    freeze.FreezeMonths = Convert.ToInt32(reader["FreezeMonths"]);
                    freeze.IsExtended = Convert.ToBoolean(reader["ExtendedStatus"]);

                    if (reader["ContractEndDate"] != DBNull.Value)
                        freeze.ContractEndDate = ((Convert.ToDateTime(reader["ContractEndDate"])).ToUniversalTime() - epoch).TotalMilliseconds.ToString();

                    memberFreezeList.Add(freeze);

                }

                if (reader != null)
                    reader.Close();

                return memberFreezeList;
            }
            catch (Exception)
            {
                throw;
            }

        }

    }
}
