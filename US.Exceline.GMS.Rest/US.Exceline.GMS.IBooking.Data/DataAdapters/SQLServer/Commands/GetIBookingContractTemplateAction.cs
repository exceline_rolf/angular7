﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingContractTemplateAction : USDBActionBase<PackageDC>
    {
        private string _templateNo = string.Empty;
        private int _branchId = -1;
        private string _gymCode = string.Empty;
        private int _memberId = -1;
        public GetIBookingContractTemplateAction(string templateNo, int branchId, string gymCode, int memberID)
        {
            _templateNo = templateNo;
            _branchId = branchId;
            _gymCode = gymCode;
            _memberId = memberID;
        }
        protected override PackageDC Body(DbConnection connection)
        {
            string spName = "USExceGMSIBookingGetContractTemplate";
             PackageDC package = null;
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateNo", DbType.String, _templateNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", DbType.String, _memberId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    package = new PackageDC();
                    package.ContractTypeValue = new US.GMS.Core.DomainObjects.Common.CategoryDC();
                    package.PackageId = Convert.ToInt32(reader["Id"]);
                    package.PackageName = reader["Name"].ToString();
                    package.BranchId = Convert.ToInt32(reader["BranchId"]);

                    package.ContractTypeValue.Id = Convert.ToInt32(reader["PackageTypeId"].ToString());
                    package.ContractTypeValue.Code = Convert.ToString(reader["PackageType"].ToString());

                    package.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    package.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    package.PackagePrice = Convert.ToDecimal(reader["PackagePrice"]);
                    package.NumOfInstallments = Convert.ToInt32(reader["NumberOfInstallments"]);
                    package.RatePerInstallment = Convert.ToDecimal(reader["RatePerInstallment"]);
                    package.EnrollmentFee = Convert.ToDecimal(reader["EnrollmentFee"]);
                    package.NumOfVisits = Convert.ToInt32(reader["NumberOfVisits"]);
                    package.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    package.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    package.ActivityName = Convert.ToString(reader["ActivityName"]);
                    package.BranchName = reader["BranchName"].ToString();
                    package.RestPlusMonth = Convert.ToBoolean(reader["RestPlusMonth"]);
                    package.LockInPeriod = Convert.ToInt32(reader["LockInPeriod"]);
                    package.PriceGuaranty = Convert.ToInt32(reader["PriceGuaranty"]);
                    package.AutoRenew = Convert.ToBoolean(reader["AutoRenew"]);
                    package.IsInvoiceDetail = Convert.ToBoolean(reader["InvoiceDetail"]);
                    package.ArticleNo = Convert.ToInt32(reader["ArticleId"]);
                    package.ArticleText = Convert.ToString(reader["ArticleText"]);
                    package.AccessProfileId = Convert.ToInt32(reader["AccessProfileId"]);
                    package.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    if (reader["FixDateOfContract"] != DBNull.Value)
                        package.FixDateOfContract = Convert.ToDateTime(reader["FixDateOfContract"]);
                    if (reader["SecondDueDate"] != DBNull.Value)
                        package.SecondDueDate = Convert.ToDateTime(reader["SecondDueDate"]);
                    package.NextTemplateId = Convert.ToInt32(reader["NextTemplateNo"]);
                    package.OrderPrice = Convert.ToDecimal(reader["OrderPrice"]);
                    package.StartUpItemPrice = Convert.ToDecimal(reader["StartupItemPrice"]);
                    package.EveryMonthItemsPrice = Convert.ToDecimal(reader["EveryMonthItemPrice"]);
                    package.NextContractTemplateName = Convert.ToString(reader["NextTemplateName"]);
                    package.NoOfMonths = Convert.ToInt32(reader["NoOfMonths"]);
                    package.NoOfDays = Convert.ToInt32(reader["NoOfDays"]);
                    package.TemplateNumber = Convert.ToString(reader["TemplateNo"]);
                    package.MaxAge = Convert.ToInt32(reader["MaxAge"]);
                    if (reader["FirstDueDate"] != DBNull.Value)
                        package.FirstDueDate = Convert.ToDateTime(reader["FirstDueDate"]);
                    package.PackageCategory.Id = Convert.ToInt32(reader["PackateCatId"]);
                    package.PackageCategory.Name = Convert.ToString(reader["PackageCatName"]);
                    package.PackageCategory.Code = Convert.ToString(reader["PackageTypeCode"]);
                    if (reader["InStock"] != DBNull.Value)
                        package.InStock = Convert.ToInt32(reader["InStock"]);
                    package.CreditDueDays = Convert.ToInt32(reader["CreditDueDays"]);
                    if (reader["SecondDueDate"] != DBNull.Value)
                        package.SecondDueDate = Convert.ToDateTime(reader["SecondDueDate"]);
                    package.CreditDueDateSetting = Convert.ToInt32(reader["DuePaymentsDate"]);
                    package.UseTodayAsDueDate = Convert.ToBoolean(reader["useTodaysAsDueDate"]);
                    if (reader["StartDateOfContract"] != DBNull.Value)
                        package.FixStartDateOfContract = Convert.ToDateTime(reader["StartDateOfContract"]);
                    package.NextTemplateNo = Convert.ToString(reader["NextTemplateNo"]);
                    package.SortingNo = Convert.ToInt32(reader["Priority"]);
                    package.CreditPeriod = Convert.ToInt32(reader["CreditPeriod"]);
                    package.MemberCreditPeriod = Convert.ToInt32(reader["MemberCreditPeriod"]);
                    package.PostPay = Convert.ToBoolean(reader["PostPay"]);
                   
                    if (string.IsNullOrEmpty(package.NextTemplateNo) || package.NextTemplateNo.Trim().Equals("0"))
                        package.NextTemplateNo = string.Empty;

                }

                if (package != null)
                {
                    GetContractTemplateItemsAction itemAction = new GetContractTemplateItemsAction(package.PackageId, _branchId);
                    List<ContractItemDC> items = itemAction.Execute(EnumDatabase.Exceline, _gymCode);
                    foreach (ContractItemDC contractItem in items)
                    {
                        if (contractItem.IsStartUpItem)
                            package.StartUpItemList.Add(contractItem);
                        else
                            package.EveryMonthItemList.Add(contractItem);
                    }
                }
                return package;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
