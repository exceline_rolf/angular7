﻿using System;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US.GMS.Core.SystemObjects;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class SaveIBookingMemberBookingsAction : USDBActionBase<int>
    {
        private ExceIBookingBookDetail _bookingDetails;
        private string _user = "IBooking";
        private string _scheduleCategoryType = ScheduleCategoryTypes.CLASSBOOKING.ToString();

        public SaveIBookingMemberBookingsAction(ExceIBookingBookDetail bookingDetails)
        {
            _bookingDetails = bookingDetails;
        }

        protected override int Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSIBookingSaveMemberBookings";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberCustId", DbType.String, _bookingDetails.CustomerId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _bookingDetails.ClassId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _bookingDetails.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleCategoryType", DbType.String, _scheduleCategoryType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                DataTable bookingDT = new DataTable();
                DataColumn col = null;
                col = new DataColumn("ActiveTimeId", typeof(Int32));
                bookingDT.Columns.Add(col);
                col = new DataColumn("Participants", typeof(Int32));
                bookingDT.Columns.Add(col);
                col = new DataColumn("IsBooked", typeof(Boolean));
                bookingDT.Columns.Add(col);

                foreach (var booking in _bookingDetails.Times)
                {
                    bookingDT.Rows.Add(booking.TimeId, booking.Participants, true);
                }

                SqlParameter parameter = new SqlParameter();
                //The parameter for the SP must be of SqlDbType.Structured 
                parameter.ParameterName = "@bookingDetails";
                parameter.SqlDbType = System.Data.SqlDbType.Structured;
                parameter.Value = bookingDT;
                cmd.Parameters.Add(parameter);

                cmd.ExecuteScalar();
                return 1;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
