﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.IBooking.Data.SystemObjects;
using US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.IBooking;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.Exceline.GMS.IBooking.Core;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.API;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters
{
    public class ExcelineIBookingFacade
    {
        private static IBookingDataAdapter GetDataAdapter()
        {
            return new SQLServerIBookingDataAdapter();
        }

        public static List<ExceIBookingMember> GetIBookingMembers(string gymCode, int branchId, int changedSinceDays, int systemId)
        {
            return GetDataAdapter().GetIBookingMemberChanges(gymCode, branchId, changedSinceDays, systemId);
        }

        public static List<ExceIBookingMember> GetIBookingMembers(string gymCode, int branchId, int systemId)
        {
            return GetDataAdapter().GetIBookingMembers(gymCode, branchId, systemId);
        }

        public static ContractResignDetails CancelIBookingContract(int systemId, int gymId, int customerId, string gymCode)
        {
            return GetDataAdapter().CancelIBookingContract(systemId, gymId, customerId, gymCode);
        }

        public static int DeleteIBookingClassBooking(int systemId, int gymId, int classId, int customerId, string gymCode)
        {
            return GetDataAdapter().DeleteIBookingClassBooking(systemId, gymId, classId, customerId, gymCode);
        }
        public static int AddIBookingClassBooking(int systemId, int gymId, int classId, int customerId, string gymCode)
        {
            return GetDataAdapter().AddIBookingClassBooking(systemId, gymId, classId, customerId, gymCode);
        }


        public static List<ExceIBookingMember> GetIBookingMemberById(string gymCode, int branchId, string customerNo, int systemId)
        {
            return GetDataAdapter().GetIBookingMemberById(gymCode, branchId, customerNo, systemId);
        }

        public static ExceIBookingSystem GetIBookingSystemDetails(string gymCode, int systemId)
        {
            return GetDataAdapter().GetIBookingSystemDetails(gymCode, systemId);
        }

        public static List<ExceIBookingGym> GetIBookingGyms(string gymCode)
        {
            return GetDataAdapter().GetIBookingGyms(gymCode);
        }

        public static List<ExceIBookingWebOffering> GetIBookingWebOfferings(string gymCode)
        {
            return GetDataAdapter().GetIBookingWebOfferings(gymCode);
        }

        public static List<ExceIBookingInstructor> GetIBookingInstructors(string gymCode)
        {
            return GetDataAdapter().GetIBookingInstructors(gymCode);
        }

        public static List<ExceIBookingClassCategory> GetIBookingClassCategories(string gymCode)
        {
            return GetDataAdapter().GetIBookingClassCategories(gymCode);
        }

        public static List<ExceIBookingClassKeyword> GetIBookingClassKeyword(string gymCode)
        {
            return GetDataAdapter().GetIBookingClassKeyword(gymCode);
        }

        public static List<CategoryDC> GetCategoriesByType(string type, string gymCode)
        {
            return GetDataAdapter().GetCategoriesByType(type, gymCode);
        }

        public static List<ExceIBookingStartReason> GetIBookingStartReasons(string gymCode)
        {
            return GetDataAdapter().GetIBookingStartReasons(gymCode);
        }

        public static List<ExceIBookingClassType> GetIBookingClassTypes(string gymCode)
        {
            return GetDataAdapter().GetIBookingClassTypes(gymCode);
        }

        public static List<ExceIBookingClassCalendar> GetIBookingClassSchedules(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            return GetDataAdapter().GetIBookingClassSchedules(gymCode, branchId, fromDate, toDate);
        }

        public static List<ExceIBookingShowUp> GetIBookingExcelineShowUps(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            return GetDataAdapter().GetIBookingExcelineShowUps(gymCode, branchId, fromDate, toDate);
        }

        public static List<ExceIBookingPayment> GetIBookingPayments(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            return GetDataAdapter().GetIBookingPayments(gymCode, branchId, fromDate, toDate);
        }

        public static string RegisterNewIBookingMember(string gymCode, ExceIBookingNewMember member)
        {
            return GetDataAdapter().RegisterNewIBookingMember(gymCode, member);
        }

        public static int UpdateIBookingClass(string gymCode, ExceIBookingUpdateClass exceClass)
        {
            return GetDataAdapter().UpdateIBookingClass(gymCode, exceClass);
        }

        public static int UpdateIBookingMemberClassVisit(string gymCode, ExceIBookingMemberClassVisit visit)
        {
            return GetDataAdapter().UpdateIBookingMemberClassVisit(gymCode, visit);
        }

        public static int UpdateIBookingMemberVisit(string gymCode, EntityVisitDC visit)
        {
            return GetDataAdapter().UpdateIBookingMemberVisit(gymCode, visit);
        }

        public static int UpdateIBookingMember(string gymCode, ExceIBookingUpdateMember member)
        {
            return GetDataAdapter().UpdateIBookingMember(gymCode, member);
        }

        public static string GetGymCodeByCompanyId(string systemId)
        {
            return GetDataAdapter().GetGymCodeByCompanyId(systemId);
        }

        public static bool GetIBookingEntityValid(int entityId, string entityType, int branchId, string gymCode)
        {
            return GetDataAdapter().GetIBookingEntityValid(entityId, entityType, branchId, gymCode);
        }

        public static PackageDC GetContractTemplate(string templateNo, int branchId, string gymCode, int memberID)
        {
            return GetDataAdapter().GetContractTemplate(templateNo, branchId, gymCode, memberID);
        }

        public static List<ContractSummaryDC> GetContractSummaries(string customerNo, string gymCode)
        {
            return GetDataAdapter().GetContractSummaries(customerNo, gymCode);
        }

        public static ExceIBookingNotificationDetails GetTemplateDetails(string gymCode, string templateNo, int branchID, string type)
        {
            return GetDataAdapter().GetNotificationDetails(gymCode, templateNo, branchID, type);
        }

        public static List<InstallmentDC> GetTodayOrder(string gymCode, int savedMemberID, int memberContractID)
        {
            return GetDataAdapter().GetTodayOrder(gymCode, savedMemberID, memberContractID);
        }

        public static List<ExceIBookingResource> GetResources(string gymCode, int branchId)
        {
            return GetDataAdapter().GetResources(gymCode, branchId);
        }


        public static List<ExceIBookingResourceBooking> GetResourceBooking(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            return GetDataAdapter().GetResourceBooking(gymCode, branchId, fromDate, toDate);
        }

        public static int AddResourceBooking(string gymCode, ExceIBookingResourceBooking resourceBooking)
        {
            return GetDataAdapter().AddResourceBooking(gymCode, resourceBooking);
        }

        public static int DeleteResourceBooking(string gymCode, int bookingId, int branchId)
        {
            return GetDataAdapter().DeleteResourceBooking(gymCode, bookingId, branchId);
        }

        public static Dictionary<int, int> ValidateAvailableForBooking(string gymCode, List<int> resourceIdList, DateTime startDateTime, DateTime endDateTime)
        {
            return GetDataAdapter().ValidateAvailableForBooking(gymCode, resourceIdList, startDateTime, endDateTime);
        }

        public static List<ExceIBookingResourceSchedule> GetResourceSchedules(string gymCode, int branchId, int resourceId, DateTime fromDate, DateTime toDate)
        {
            return GetDataAdapter().GetResourceSchedules(gymCode, branchId, resourceId, fromDate, toDate);
        }

        public static List<ExceIBookingResourceAvailableTime> GetResourceAvailableTimes(string gymCode, int branchId, int resourceId)
        {
            return GetDataAdapter().GetResourceAvailableTimes(gymCode, branchId, resourceId);
        }

        public static List<ExceIBookingInvoice> GetInvoices(int branchID, string gymCode, DateTime startDate, DateTime endDate, string documentService, string folderPath)
        {
            return GetDataAdapter().GetInvoices(branchID, gymCode, startDate, endDate, documentService, folderPath);
        }


        public static List<ExceIBookingInterest> GetIBookingInterests(string gymCode)
        {
            return GetDataAdapter().GetIBookingInterests(gymCode);
        }

        public static void AddStatus(ExceIBookingNewMember NewMember, int status, string description, string gymCode)
        {
            GetDataAdapter().AddStatus(NewMember, status, description, gymCode);
        }

        public static ARXSetting GetARXSettings(int systemId)
        {
            return GetDataAdapter().GetARXSettings(systemId);
        }

        public static person GetARXData(int savedMemberID, int memberContractID, string gymCode)
        {
            return GetDataAdapter().GetARXData(savedMemberID, memberContractID, gymCode);
        }

        public static int AddClassVisit(string gymID, string classID, List<MemberVisitStatus> visits, string gymCode)
        {
            return GetDataAdapter().AddClassVisit(gymID, classID, visits, gymCode);
        }

        public static void SetPDFFileParth(int id, string fileParth, string flag, int branchId, string gymCode, string user)
        {
            GetDataAdapter().SetPDFFileParth(id, fileParth, flag, branchId, gymCode, user);
        }

        public static BookingDetails GetBookingDetails(int bookingID, string gymCode)
        {
            return GetDataAdapter().GetBookingDetails(bookingID, gymCode);
        }

        public static int AddShopOrder(InstallmentDC installment, int branchID, int memberBranchID, string gymCode)
        {
            return GetDataAdapter().AddShopOrder(installment, branchID, memberBranchID, gymCode);
        }

        public static OperationResult<SaleResultDC> PayIBookingInvoice(int branchID, string user, InstallmentDC installment, PaymentDetailDC paymentDetails, string gymCode)
        {
            return GetDataAdapter().PayIBookingInvoice(branchID, user, installment, paymentDetails, gymCode);
        }

        public static bool UpdateBookingPaymentDetails( int bookingID, int memberID, int arItemNO, decimal amount, string refID, string gymCode)
        {
            return GetDataAdapter().UpdateBookingPaymentDetails(bookingID, memberID, arItemNO, amount, refID, gymCode);
        }

        public static bool CancelPaymentOrder(int bookingID, int orderID, string notificationMessage, string gymCode)
        {
            return GetDataAdapter().CancelPaymentOrder(bookingID, orderID, notificationMessage, gymCode);
        }

        public static bool UpdateBRISData(OrdinaryMemberDC member, string gymCode)
        {
            return GetDataAdapter().UpdateBRISData(member, gymCode);
        }

        public static List<ExceIBookingMemberGymDetail> GetIBookingMemberGyms(string gymCode )
        {
        return GetDataAdapter().GetIBookingMemberGyms(gymCode);
        }

        public static List<ExceIBookingModifiedMember> GetMembersUpdated(DateTime date, int systemId, int branchId, string gymCode)
        {
            return GetDataAdapter().GetMembersUpdated(date, systemId, branchId, gymCode);
        }
        
        public static List<ExceIBookingAllContracts> GetAllContracts(int systemId, string gymCode)
        {
            return GetDataAdapter().GetAllContracts(systemId, gymCode);
        }

        public static int RegisterNewFreeze(string gymCode, ExceIBookingNewFreeze freeze)
        {
            return GetDataAdapter().RegisterNewFreeze(gymCode, freeze);
        }

        public static int RegisterResign(string gymCode, ExceIBookingNewResign resign)
        {
            return GetDataAdapter().RegisterResign(gymCode, resign);
        }

        public static List<ExceIBookingContractFreeze> GetContractByMemberId(string gymCode, string customerNo, int systemId)
        {
            return GetDataAdapter().GetContractByMemberId(gymCode, customerNo, systemId);
        }

        public static List<ExceIBookingListFreeze> GetFreezeByCustId(string gymCode, string custId, int systemId)
        {
            return GetDataAdapter().GetFreezeByCustId(gymCode, custId, systemId);
        }


        public static List<ExceAPIMember> GetAPIMembers(string gymCode, int branchId, int changedSinceDays, int systemId)
        {
            return GetDataAdapter().GetAPIMembers(gymCode, branchId, changedSinceDays, systemId);
        }

        public static List<ExceIBookingFreezeResignCategories> GetFreezeResignCategories(string gymCode)
        {
            return GetDataAdapter().GetFreezeResignCategories(gymCode);
        }

        public static List<int?> GetBrisIdActiveFreezed(string gymCode)
        {
            return GetDataAdapter().GetBrisIdActiveFreezed(gymCode);
        }

        public static List<ExceIBookingMember> NTNUIGetIBookingMembers(string gymCode, int changedSinceDays, int systemId)
        {
            return GetDataAdapter().NTNUIGetIBookingMemberChanges(gymCode, changedSinceDays, systemId);
        }

        public static List<ExceIBookingVisitCount> GetVisitCount(string gymCode, int branchId, int year, int systemId)
        {
            return GetDataAdapter().GetVisitCount(gymCode, branchId, year, systemId);
        }

    }
}
