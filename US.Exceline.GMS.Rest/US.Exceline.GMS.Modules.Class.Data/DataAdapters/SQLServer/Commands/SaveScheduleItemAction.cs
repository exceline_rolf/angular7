﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class SaveScheduleItemAction : USDBActionBase<int>
    {
        private DataTable _dataTableEntity;
        private DataTable _dtActiveTimes;
        private ScheduleItemDC _scheduleItem;
        private int _branchId;
        private bool _isCopySchedule;
        public SaveScheduleItemAction(ScheduleItemDC scheduleItem, int branchId, string user, bool isCopySchedule)
        {
            _scheduleItem = scheduleItem;
            _isCopySchedule = isCopySchedule;
            if (_scheduleItem.Id > 0)
                _scheduleItem.LastModifiedUser = user;
            else _scheduleItem.CreatedUser = user;
            
            _dataTableEntity = GetEntityList(scheduleItem.InstructorIdList, scheduleItem.ResourceIdList);
            _branchId = branchId;
            if (scheduleItem.ActiveTimes != null)
            {
                _dtActiveTimes = GetActiveTimesDataTable(scheduleItem.ActiveTimes.ToList());
            }
        }

        private DataTable GetEntityList(IEnumerable<int> insList, IEnumerable<int> resList)
        {
            _dataTableEntity = new DataTable();
            _dataTableEntity.Columns.Add(new DataColumn("ScheduleItemId", typeof(int)));
            _dataTableEntity.Columns.Add(new DataColumn("EntityId", typeof(int)));
            _dataTableEntity.Columns.Add(new DataColumn("EntityRoleType", typeof(string)));

            if (insList != null)
            {
                var enumerable = insList as int[] ?? insList.ToArray();
                if (enumerable.Any())
                {
                    foreach (int id in enumerable)
                    {
                        DataRow dataTableRow = _dataTableEntity.NewRow();
                        dataTableRow["ScheduleItemId"] = _scheduleItem.Id;
                        dataTableRow["EntityId"] = id;
                        dataTableRow["EntityRoleType"] = "INS";
                        _dataTableEntity.Rows.Add(dataTableRow);
                    }
                }
            }

            if (resList != null)
            {
                var enumerable = resList as int[] ?? resList.ToArray();
                if (enumerable.Any())
                {
                    foreach (int id in enumerable)
                    {
                        DataRow dataTableRow = _dataTableEntity.NewRow();
                        dataTableRow["ScheduleItemId"] = _scheduleItem.Id;
                        dataTableRow["EntityId"] = id;
                        dataTableRow["EntityRoleType"] = "RES";
                        _dataTableEntity.Rows.Add(dataTableRow);
                    }
                }
            }
            return _dataTableEntity;
        }
        private DataTable GetActiveTimesDataTable(List<EntityActiveTimeDC> activeTimes)
        {
            _dtActiveTimes = new DataTable();
            _dtActiveTimes.Columns.Add(new DataColumn("ScheduleItemId", typeof(int)));
            _dtActiveTimes.Columns.Add(new DataColumn("StartDateTime", typeof(DateTime)));
            _dtActiveTimes.Columns.Add(new DataColumn("EndDateTime", typeof(DateTime)));

            if (_scheduleItem.ActiveTimes != null && _scheduleItem.ActiveTimes.Any())
            {
                foreach (EntityActiveTimeDC activeTime in _scheduleItem.ActiveTimes)
                {
                    DataRow dr = _dtActiveTimes.NewRow();
                    dr["ScheduleItemId"] = _scheduleItem.Id;
                    dr["StartDateTime"] = activeTime.StartDateTime;
                    dr["EndDateTime"] = activeTime.EndDateTime;
                    _dtActiveTimes.Rows.Add(dr);
                }
            }
            return _dtActiveTimes;
        }

        protected override int Body(DbConnection connection)
        {
            int scheduleItemId;
            const string storedProcedureName = "USExceGMSClassesSaveScheduleItem";
            try
            {

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _scheduleItem.Id));
                // cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, -1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsCopyClass", DbType.Boolean, _isCopySchedule));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", DbType.Int32, _scheduleItem.ScheduleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Occurrence", DbType.String, _scheduleItem.Occurrence));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Day", DbType.String, _scheduleItem.Day));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Week", DbType.Int32, _scheduleItem.Week));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Month", DbType.String, _scheduleItem.Month));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Year", DbType.Int32, _scheduleItem.Year));
                if (_scheduleItem.StartTime != DateTime.MinValue)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", DbType.DateTime, _scheduleItem.StartTime));
                if (_scheduleItem.EndTime != DateTime.MinValue)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", DbType.DateTime, _scheduleItem.EndTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _scheduleItem.StartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _scheduleItem.EndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedDateTime", DbType.DateTime, DateTime.Now));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedDateTime", DbType.DateTime, DateTime.Now));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _scheduleItem.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedUser", DbType.String, _scheduleItem.LastModifiedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassTypeId", DbType.Int32, _scheduleItem.ClassTypeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus", DbType.Boolean, true));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsFixed", DbType.Boolean, _scheduleItem.IsFixed));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@WeekType", DbType.Int32, _scheduleItem.WeekType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MaxNoOfBooking", DbType.Int32, _scheduleItem.MaxNoOfBooking));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsUpdatedAfterClassStarted", DbType.Boolean, _scheduleItem.IsUpdatedAfterClassStarted));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsOnlyEndDateUpdated", DbType.Boolean, _scheduleItem.IsOnlyEndDateUpdated));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EditOptionId", DbType.Int32, _scheduleItem.EditOptionId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityList", SqlDbType.Structured, _dataTableEntity));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimesList", SqlDbType.Structured, _dtActiveTimes));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NumberOfParticipants", SqlDbType.Int, _scheduleItem.NumberOfParticipants));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OutPutID", DbType.Int32, _scheduleItem.Id));
                object obj = cmd.ExecuteScalar();
                scheduleItemId = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return scheduleItemId;
        }
    }
}
