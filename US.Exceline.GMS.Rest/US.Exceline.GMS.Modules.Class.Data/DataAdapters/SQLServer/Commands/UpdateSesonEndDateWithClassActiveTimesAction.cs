﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateSesonEndDateWithClassActiveTimesAction : USDBActionBase<int>
    {
        private int _seasonId;
        private DateTime _endDate;
        private int _branchId;

        public UpdateSesonEndDateWithClassActiveTimesAction(int seasonId, int branchId, DateTime endDate)
        {
            this._seasonId = seasonId;
            this._branchId = branchId;
            this._endDate = endDate;       
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int _updatedStatus = 0;
            try
            {
                const string storedProcedureName = "USExceGMSUpdateClassesEndDateWithSeason";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@seasonId", DbType.Int32, _seasonId));
                SqlParameter output = new SqlParameter("@OUTPUTVAL", SqlDbType.Int);                
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                _updatedStatus = Convert.ToInt32(output.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _updatedStatus;
        }
    }
}
