﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Collections.ObjectModel;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class GetClassScheduleAction : USDBActionBase<ScheduleDC>
    {
        private int _classId = -1;
        private int classScheduleId = -1;
        private DateTime StartDate;
        private DateTime EndDate;
        private DateTime ClassStartDate;
        private DateTime ClassEndDate;
        private int Id = -1;
        private ScheduleDC _schedule = null;

        public GetClassScheduleAction(int classId)
        {
            _classId = classId;
        }

        public GetClassScheduleAction(int classId,ScheduleDC schedule)
        {
            _classId = classId;
            _schedule = schedule;
        }

        protected override ScheduleDC Body(DbConnection connection)
        {
            ScheduleDC classSchedule;
            if (_schedule != null)
             classSchedule = _schedule;
            else
              classSchedule = new ScheduleDC();

            classSchedule.SheduleItemList = new ObservableCollection<ScheduleItemDC>();
            string StoredProcedureName = "USExceGMSManageClassesGetClassSchedule";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ScheduleTypes type = new ScheduleTypes();
                    ScheduleItemDC scheduleItem = new ScheduleItemDC();
                    classScheduleId = Convert.ToInt32(reader["ClassScheduleId"]);
                    Id = Convert.ToInt32(reader["EntityScheduleId"]);
                    ClassStartDate = Convert.ToDateTime(reader["ClassStartDate"]);
                    ClassEndDate = Convert.ToDateTime(reader["ClassEndDate"]); 
                    StartDate = Convert.ToDateTime(reader["StartDate"]);
                    EndDate = Convert.ToDateTime(reader["EndDate"]);
                    classSchedule.StartDate = ClassStartDate;
                    classSchedule.EndDate = ClassEndDate;
                    string occuence = (reader["Occurrence"].ToString());
                    if (!String.IsNullOrEmpty(occuence))
                    type = (ScheduleTypes)Enum.Parse(typeof(ScheduleTypes),occuence);
                    classSchedule.Occurrence = type;
                    scheduleItem.Occurrence = type;
                    scheduleItem.ScheduleId = Convert.ToInt32(reader["EntityScheduleId"]);
                    scheduleItem.Day = reader["Day"].ToString();
                    scheduleItem.Week = Convert.ToInt32(reader["Week"]);
                    scheduleItem.Month = reader["Month"].ToString();
                    scheduleItem.Year = Convert.ToInt32(reader["Year"]);
                    scheduleItem.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    scheduleItem.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    scheduleItem.StartDate = StartDate;
                    scheduleItem.EndDate = EndDate;
                    scheduleItem.ActiveStatus = true;
                    scheduleItem.LastActiveTimeGeneratedDate = Convert.ToDateTime(reader["ActiveTimeGeneratedDate"]);
                    scheduleItem.Id = Convert.ToInt32(reader["Id"]);
                    scheduleItem.ClassTypeId = Convert.ToInt32(reader["ClassTypeId"]);
                    classSchedule.SheduleItemList.Add(scheduleItem);                    
                }

                if (classScheduleId != 0)
                    classSchedule.Id = classScheduleId;
                else
                    classSchedule.Id = Id;                
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return classSchedule;
        }
    }
}
