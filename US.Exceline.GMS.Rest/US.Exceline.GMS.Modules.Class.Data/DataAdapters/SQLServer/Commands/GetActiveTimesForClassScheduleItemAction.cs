﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class GetActiveTimesForClassScheduleItemAction:USDBActionBase<List<EntityActiveTimeDC>>
    {
        private int _scheduleItemId = -1;

        public GetActiveTimesForClassScheduleItemAction(int scheduleItemId)
        {
            this._scheduleItemId = scheduleItemId;    
        }
        protected override List<EntityActiveTimeDC> Body(System.Data.Common.DbConnection connection)
        {
            List<EntityActiveTimeDC> activeTimes = new List<EntityActiveTimeDC>();
            string storedProcedure = "USExceGMSGetActiveTimesForClassScheduleItem";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", System.Data.DbType.Int32, _scheduleItemId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    EntityActiveTimeDC activeTime = new EntityActiveTimeDC();
                    activeTime.Id = Convert.ToInt32(reader["ID"]);
                    activeTime.SheduleItemId = Convert.ToInt32(reader["SchduleItemID"]);
                    activeTime.StartDateTime = Convert.ToDateTime(reader["StartDateTime"]);
                    activeTime.EndDateTime = Convert.ToDateTime(reader["EndDateTime"]);
                    activeTimes.Add(activeTime);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return activeTimes;
        }
    }
}
