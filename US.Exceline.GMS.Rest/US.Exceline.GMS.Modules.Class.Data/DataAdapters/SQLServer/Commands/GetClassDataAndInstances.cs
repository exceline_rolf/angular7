﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;
using US.Exceline.GMS.Modules.Class.Data.SystemObjects;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class GetClassDataAndInstances : USDBActionBase<ScheduleData>
    {
        /* PRIVATE VARIABLE DECLERATIONS*/
        
        int _branchId = -1;
        int _seasonId = -1;

        public GetClassDataAndInstances(int BranchId, int SeasonId)
        {
            _branchId = BranchId;
            _seasonId = SeasonId;
            

        }

        protected override ScheduleData Body(DbConnection connection)
        {
           
            string StoredProcedureName1 = "GetSeasonDetailsForClass";
            string StoredProcedureName2 = "GetClassMembersOfSeason";
            string StoredProcedureName3 = "GetClassDetailInfo";
            string StoredProcedureName4 = "GetInstructorsForClass";
            string StoredProcedureName5 = "GetResourcesForClass";
            string StoredProcedureName6 = "GetClassInstanceDetails";
            ScheduleData schedule = new ScheduleData();
            //Variables used for comparison, to determin changes
            List<InstructorData> originalInstructorList = new List<InstructorData>();
            List<ResourceData> originalResourceList = new List<ResourceData>();

            try
            {
                DbCommand cmd1 = CreateCommand(CommandType.StoredProcedure, StoredProcedureName1);
                cmd1.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _seasonId));
                cmd1.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                DbDataReader reader1 = cmd1.ExecuteReader();
                

                while (reader1.Read())
                {
                    schedule.BranchId = _branchId;
                    schedule.Id = _seasonId;
                    schedule.Name = Convert.ToString(reader1["Name"]);
                    schedule.StartDate = Convert.ToDateTime(reader1["StartDate"]);
                    schedule.EndDate = Convert.ToDateTime(reader1["EndDate"]);
                }
                reader1.Close();
            }

            catch (Exception ex1)
            {
                throw ex1;
            }

            try
            {
                DbCommand cmd2 = CreateCommand(CommandType.StoredProcedure, StoredProcedureName2);
                cmd2.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", DbType.Int32, _seasonId));
                DbDataReader reader2 = cmd2.ExecuteReader();
                List<ClassData> classList = new List<ClassData>();

                while (reader2.Read())
                {
                    ClassData classDataElement = new ClassData();
                    
                    classDataElement.ScheduleItemId = Convert.ToInt32(reader2["ScheduleItemId"]);
                    classDataElement.NameOfClass = Convert.ToString(reader2["NameOfClass"]);
                    classDataElement.MaxNoOfBookings = Convert.ToInt32(reader2["MaxNoOfBookings"]);
                    classDataElement.DayOfTheWeek = Convert.ToInt32(reader2["DayOfTheWeek"]);
                    classDataElement.StartDate = Convert.ToDateTime(reader2["StartDate"]);
                    classDataElement.EndDate = Convert.ToDateTime(reader2["EndDate"]);
                    classDataElement.StartTime = Convert.ToDateTime(reader2["Start"]);
                    classDataElement.EndTime = Convert.ToDateTime(reader2["End"]);
                    classDataElement.ClassTypeId = Convert.ToInt32(reader2["ClassTypeId"]);
                    classDataElement.OriginalInstanceTimes = GetDatesForClass(classDataElement.DayOfTheWeek, Convert.ToDateTime(classDataElement.StartDate), Convert.ToDateTime(classDataElement.EndDate));
                    classList.Add(classDataElement);
                }

                reader2.Close();
                schedule.ClassList = classList;
            }
            catch (Exception ex2)
            {
                throw ex2;
            }

            foreach( var elem in schedule.ClassList) {

                try
                {
                    DbCommand cmd3 = CreateCommand(CommandType.StoredProcedure, StoredProcedureName3);
                    cmd3.Parameters.Add(DataAcessUtils.CreateParam("@ClassTypeId", DbType.Int32, elem.ClassTypeId));
                    DbDataReader reader3 = cmd3.ExecuteReader();
                    ClassInfoData classInfo = new ClassInfoData();

                    while (reader3.Read())
                    {

                        classInfo.Id = Convert.ToInt32(reader3["ID"]);
                        classInfo.GroupId = Convert.ToInt32(reader3["ClassGroupId"]);
                        classInfo.GroupName = Convert.ToString(reader3["ClassGroupName"]);
                        classInfo.Name = Convert.ToString(reader3["Name"]);
                        classInfo.ClassLevelId = Convert.ToInt32(reader3["ClassLevelId"]);
                        classInfo.Colour = Convert.ToString(reader3["Colour"]);
                        classInfo.Comment = Convert.ToString(reader3["Comment"]);
                        classInfo.MaxNoOfBookings = Convert.ToInt32(reader3["MaxNoOfBookings"]);

                    }

                    elem.ClassInfo = classInfo;
                    reader3.Close();
                }
                catch (Exception ex3)
                {
                    throw ex3;
                }

                
                try
                {
                    DbCommand cmd4 = CreateCommand(CommandType.StoredProcedure, StoredProcedureName4);
                    cmd4.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, elem.ScheduleItemId));
                    cmd4.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", DbType.Int32, _seasonId));
                    DbDataReader reader4 = cmd4.ExecuteReader();
                    List<InstructorData> instructors = new List<InstructorData>();

                    while (reader4.Read())
                    {
                        InstructorData instructor = new InstructorData();
                        instructor.DisplayName = Convert.ToString(reader4["InstructorName"]);
                        instructor.ActiveStatus = Convert.ToInt32(reader4["ActiveStatus"]);
                        instructor.Id = Convert.ToInt32(reader4["ID"]);
                        instructor.Description = Convert.ToString(reader4["InstructorDescription"]);
                       
                        instructors.Add(instructor);
                    }

                    elem.InstructorList = instructors;
                    originalInstructorList =  instructors;
                    reader4.Close();
                }
                catch (Exception ex4)
                {
                    throw ex4;
                }

                
                try
                {
                    DbCommand cmd5 = CreateCommand(CommandType.StoredProcedure, StoredProcedureName5);
                    cmd5.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, elem.ScheduleItemId));
                    cmd5.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", DbType.Int32, _seasonId));
                    DbDataReader reader5 = cmd5.ExecuteReader();
                    List<ResourceData> resources = new List<ResourceData>();

                    while (reader5.Read())
                    {
                        ResourceData resource = new ResourceData();
                        resource.DisplayName = Convert.ToString(reader5["ResourceName"]);
                        resource.ActiveStatus = Convert.ToInt32(reader5["ActiveStatus"]);
                        resource.Id = Convert.ToInt32(reader5["ID"]);
                        resource.CategoryId = Convert.ToInt32(reader5["CategoryId"]);
                        resource.ActivityId = Convert.ToInt32(reader5["ActivityId"]);

                        resources.Add(resource);
                    }

                    elem.ResourceList = resources;
                    originalResourceList = resources;
                    reader5.Close();
                }
                catch (Exception ex5)
                {
                    throw ex5;
                }

                List<ClassInstanceDetails> classInstanceDetails = new List<ClassInstanceDetails>();
                try
                {
                   
                    DbCommand cmd6 = CreateCommand(CommandType.StoredProcedure, StoredProcedureName6);
                    cmd6.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, elem.ScheduleItemId));
                    DbDataReader reader6 = cmd6.ExecuteReader();

                    while (reader6.Read())
                    {
                        ClassInstanceDetails instanceDetails = new ClassInstanceDetails(originalInstructorList, originalResourceList);

                        instanceDetails.Id = Convert.ToInt32(reader6["ID"]);
                        instanceDetails.Date = Convert.ToDateTime(reader6["EndDate"]);
                        instanceDetails.DayOfTheWeek = Convert.ToInt32(reader6["DayOfTheWeek"]);
                        instanceDetails.StartTime = Convert.ToDateTime(reader6["Start"]);
                        instanceDetails.EndTime = Convert.ToDateTime(reader6["End"]);
                        instanceDetails.Altered = Convert.ToBoolean(reader6["Altered"]);
                        instanceDetails.Occurence = Convert.ToString(reader6["Occurrence"]);
                        instanceDetails.MaxNoOfBookings = Convert.ToInt32(reader6["MaxNoOfBooking"]);
                        if (Convert.IsDBNull(reader6["ModifiedDate"]) == true) {

                            instanceDetails.LastModifiedDateTime = null;
                        }
                        else {

                            instanceDetails.LastModifiedDateTime = Convert.ToDateTime(reader6["ModifiedDate"]);
                        }
                        instanceDetails.LastModifiedUser = Convert.ToString(reader6["ModifiedByUser"]);

                        classInstanceDetails.Add(instanceDetails);

                    }

                    reader6.Close();
                }
                catch (Exception ex6)
                {
                    throw ex6;
                }
                for(var otherElem = 0; otherElem < classInstanceDetails.Count; otherElem++) {

                    while (elem.OriginalInstanceTimes.Count <= classInstanceDetails.Count)
                    {
                        elem.OriginalInstanceTimes.Add(DateTime.Now);
                    }
                        
                        try
                        {
                            DbCommand cmd7 = CreateCommand(CommandType.StoredProcedure, StoredProcedureName4);
                            cmd7.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, elem.ScheduleItemId));
                            cmd7.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", DbType.Int32, _seasonId));
                            cmd7.Parameters.Add(DataAcessUtils.CreateParam("@DateforInstance", DbType.DateTime, classInstanceDetails[otherElem].Date));
                            DbDataReader reader7 = cmd7.ExecuteReader();
                            List<InstructorData> instanceInstructors = new List<InstructorData>();

                        while (reader7.Read())
                            {
                                InstructorData instanceInstructor = new InstructorData();
                                instanceInstructor.DisplayName = Convert.ToString(reader7["InstructorName"]);
                                instanceInstructor.ActiveStatus = Convert.ToInt32(reader7["ActiveStatus"]);
                                instanceInstructor.Id = Convert.ToInt32(reader7["ID"]);
                                instanceInstructor.Description = Convert.ToString(reader7["InstructorDescription"]);

                                instanceInstructors.Add(instanceInstructor);
                            }

                        classInstanceDetails[otherElem].InstanceInstructorList = instanceInstructors;

                            reader7.Close();
                        }
                        catch (Exception ex7)
                        {
                            throw ex7;
                        }


                        
                        try
                        {
                            DbCommand cmd8 = CreateCommand(CommandType.StoredProcedure, StoredProcedureName5);
                            cmd8.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, elem.ScheduleItemId));
                            cmd8.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", DbType.Int32, _seasonId));
                            cmd8.Parameters.Add(DataAcessUtils.CreateParam("@DateforInstance", DbType.DateTime, classInstanceDetails[otherElem].Date));
                            DbDataReader reader8 = cmd8.ExecuteReader();
                            List<ResourceData> instanceResources = new List<ResourceData>();

                            while (reader8.Read())
                            {
                                ResourceData instanceResource = new ResourceData();
                                instanceResource.DisplayName = Convert.ToString(reader8["ResourceName"]);
                                instanceResource.ActiveStatus = Convert.ToInt32(reader8["ActiveStatus"]);
                                instanceResource.Id = Convert.ToInt32(reader8["ID"]);
                                instanceResource.CategoryId = Convert.ToInt32(reader8["CategoryId"]);
                                instanceResource.ActivityId = Convert.ToInt32(reader8["ActivityId"]);

                                instanceResources.Add(instanceResource);
                            }

                        classInstanceDetails[otherElem].InstanceResourceList = instanceResources;
                            reader8.Close();
                        }
                        catch (Exception ex8)
                        {
                            throw ex8;
                        }

                        AlteredClassData AlteredData = new AlteredClassData(originalInstructorList, classInstanceDetails[otherElem].InstanceInstructorList, originalResourceList, classInstanceDetails[otherElem].InstanceResourceList);

                        AlteredData.LastModifiedDateTime = classInstanceDetails[otherElem].LastModifiedDateTime;
                        AlteredData.LastModifiedUser = classInstanceDetails[otherElem].LastModifiedUser;
                        AlteredData.OldDate = elem.OriginalInstanceTimes[otherElem];
                        AlteredData.NewDate = classInstanceDetails[otherElem].Date;
                        AlteredData.OldStartTime = elem.StartTime;
                        AlteredData.NewStartTime = classInstanceDetails[otherElem].StartTime;
                        AlteredData.OldEndTime = elem.EndTime;
                        AlteredData.NewEndTime = classInstanceDetails[otherElem].EndTime;
                        AlteredData.OldMaxAmountOfBookings = elem.MaxNoOfBookings;
                        AlteredData.NewMaxAmountOfBookings = classInstanceDetails[otherElem].MaxNoOfBookings;
                        AlteredData.EndTimeChanged = AlteredData.checkIfEndTimeChanged();
                        AlteredData.DateChanged = AlteredData.checkIfDateChanged();
                        AlteredData.MaxAmountOfBookingsChanged = AlteredData.checkIfMaxAmountOfBookingsChanged();
                        AlteredData.InstructorListChanged = AlteredData.checkIfInstructorListChanged();
                        AlteredData.ResourceListChanged = AlteredData.checkIfResourceListChanged();

                    classInstanceDetails[otherElem].AlterationSpecs = AlteredData;
                        

                }

                elem.ClassInstanceList = classInstanceDetails;
            }
            return schedule;
        }

        public List<DateTime> GetDatesForClass(int dayOfTheWeek, DateTime StartDate, DateTime EndDate)
        {
            List<DateTime> resultList = new List<DateTime>();
            DateTime nextEligibleDate = new DateTime();
            switch (dayOfTheWeek)
            {
                case (1): //Sunday
                    for (int i = 0; i < 7; i++)
                    {
                        if ((StartDate.AddDays(i)).DayOfWeek == DayOfWeek.Sunday)
                        {
                            nextEligibleDate = StartDate.AddDays(i);
                        }
                    }
                    break;

                case (2): // Monday
                    for (int i = 0; i < 7; i++)
                    {
                        if ((StartDate.AddDays(i)).DayOfWeek == DayOfWeek.Monday)
                        {
                            nextEligibleDate = StartDate.AddDays(i);
                        }
                    }
                    break;

                case (3): // Tuesday
                    for (int i = 0; i < 7; i++)
                    {
                        if ((StartDate.AddDays(i)).DayOfWeek == DayOfWeek.Tuesday)
                        {
                            nextEligibleDate = StartDate.AddDays(i);
                        }
                    }
                    break;

                case (4): // Wednesday
                    for (int i = 0; i < 7; i++)
                    {
                        if ((StartDate.AddDays(i)).DayOfWeek == DayOfWeek.Wednesday)
                        {
                            nextEligibleDate = StartDate.AddDays(i);
                        }
                    }
                    break;

                case (5): // Thursday
                    for (int i = 0; i < 7; i++)
                    {
                        if ((StartDate.AddDays(i)).DayOfWeek == DayOfWeek.Thursday)
                        {
                            nextEligibleDate = StartDate.AddDays(i);
                        }
                    }
                    break;

                case (6): // Friday
                    for (int i = 0; i < 7; i++)
                    {
                        if ((StartDate.AddDays(i)).DayOfWeek == DayOfWeek.Friday)
                        {
                            nextEligibleDate = StartDate.AddDays(i);
                        }
                    }
                    break;

                case (7): //Saturday
                    for (int i = 0; i < 7; i++)
                    {
                        if ((StartDate.AddDays(i)).DayOfWeek == DayOfWeek.Saturday)
                        {
                            nextEligibleDate = StartDate.AddDays(i);
                        }
                    }
                    break;
            }

            while (nextEligibleDate <= EndDate)
            {
                resultList.Add(nextEligibleDate);
                nextEligibleDate = nextEligibleDate.AddDays(7);
            }

            return resultList;
        }

    }


}

