﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class CreateActiveTimesForExistingEntityIdsAction : USDBActionBase<bool>
    {
        private int _activeTimeId = -1;
        private int _entityId = -1;
        private string _roleType = string.Empty;
        private DateTime _startDateTime;
        private DateTime _endDateTime;

        public CreateActiveTimesForExistingEntityIdsAction(int activeTimeId, int entityId, string entRoleType, DateTime startDateTime, DateTime endDateTime)
        {
            _activeTimeId = activeTimeId;
            _entityId = entityId;
            _roleType = entRoleType;
            _startDateTime = startDateTime;
            _endDateTime = endDateTime;
        }
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            return false;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            bool _isCreated = false;
            string storedProcedure = "USEXCEGMSAddActiveTimesForExistingEntityIds";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimeId", System.Data.DbType.Int32,_activeTimeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityId", System.Data.DbType.Int32, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityRoleType", System.Data.DbType.String, _roleType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDateTime", System.Data.DbType.DateTime, _startDateTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDateTime ", System.Data.DbType.DateTime, _endDateTime));

                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;

                cmd.ExecuteNonQuery();
                _isCreated = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _isCreated;
        }
    }
}
