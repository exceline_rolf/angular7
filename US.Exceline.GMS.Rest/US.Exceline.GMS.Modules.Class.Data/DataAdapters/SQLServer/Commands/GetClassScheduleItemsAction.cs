﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Collections.ObjectModel;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class GetClassScheduleItemsAction : USDBActionBase<List<ScheduleItemDC>>
    {
        private int _classId = -1;
        private List<ScheduleItemDC> _scheduleItems = null;
        private string _gymCode = string.Empty;

        public GetClassScheduleItemsAction(int classId, string gymCode)
        {
            _classId = classId;
            _gymCode = gymCode;
        }

        protected override List<ScheduleItemDC> Body(DbConnection connection)
        {
            _scheduleItems = new List<ScheduleItemDC>();
            string StoredProcedureName = "USExceGMSClassesGetScheduleItems";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ScheduleTypes type = new ScheduleTypes();
                    ScheduleItemDC scheduleItem = new ScheduleItemDC();
                    string occuence = (reader["Occurrence"].ToString());
                    if(!String.IsNullOrEmpty(occuence))
                    type = (ScheduleTypes)Enum.Parse(typeof(ScheduleTypes),occuence);
                    scheduleItem.Occurrence = type;
                    scheduleItem.ScheduleId = Convert.ToInt32(reader["ScheduleId"]);
                    scheduleItem.Day = reader["Day"].ToString();
                    scheduleItem.Week = Convert.ToInt32(reader["Week"]);
                    scheduleItem.Month = reader["Month"].ToString();
                    scheduleItem.Year = Convert.ToInt32(reader["Year"]);
                    scheduleItem.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    scheduleItem.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    scheduleItem.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    scheduleItem.FilterDay = GetFilterDayOfWeek(scheduleItem.Day);
                    scheduleItem.EndDate = Convert.ToDateTime(reader["EndDate"]);;
                    scheduleItem.ActiveStatus = true;
                    scheduleItem.LastActiveTimeGeneratedDate = Convert.ToDateTime(reader["ActiveTimeGeneratedDate"]);
                    scheduleItem.Id = Convert.ToInt32(reader["Id"]);
                    scheduleItem.ClassId = _classId;

                    var action1 = new GetEntityIdForScheduleItemAction(scheduleItem.Id,"INS");
                    scheduleItem.InstructorIdList = action1.Execute(EnumDatabase.Exceline, _gymCode);

                    var action3 = new GetEntityIdNameForScheduleItemAction(scheduleItem.Id, "INS");
                    scheduleItem.InstructorIdNameList = action3.Execute(EnumDatabase.Exceline, _gymCode);

                    var action2 = new GetEntityIdForScheduleItemAction(scheduleItem.Id, "RES");
                    scheduleItem.ResourceIdList = action2.Execute(EnumDatabase.Exceline, _gymCode);

                    var action4 = new GetEntityIdNameForScheduleItemAction(scheduleItem.Id, "RES");
                    scheduleItem.ResourceIdNameList = action4.Execute(EnumDatabase.Exceline, _gymCode);

                    scheduleItem.ClassTypeId = Convert.ToInt32(reader["ClassTypeId"]);
                    scheduleItem.ResourceId = Convert.ToInt32(reader["ResourceId"]);
                    scheduleItem.WeekType = Convert.ToInt32(reader["WeekType"]);
                    scheduleItem.EntityRoleType = Convert.ToString(reader["EntityRoleType"]);
                    if (reader["MaxNoOfBooking"] != DBNull.Value)
                    {
                        scheduleItem.MaxNoOfBooking = Convert.ToInt32(reader["MaxNoOfBooking"]);
                    }
                    scheduleItem.ClassNumber = Convert.ToInt32(reader["ClassNumber"]);
                    scheduleItem.IsFixed = Convert.ToBoolean(reader["IsFixed"]);
                    scheduleItem.ClassType.ClassGroupId = Convert.ToInt32(reader["ClassGroupId"]);
                    scheduleItem.ClassType.ClassLevelId = Convert.ToInt32(reader["ClassLevelId"]);
                    _scheduleItems.Add(scheduleItem);                    
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _scheduleItems;
        }

        private int GetFilterDayOfWeek(string dayOfWeek)
        {
            int filterDayOfWeek = -1;
            switch (dayOfWeek)
            {
                case "Sunday":
                    filterDayOfWeek = 7;
                    break;
                case "Monday":
                    filterDayOfWeek = 1;
                    break;
                case "Tuesday":
                    filterDayOfWeek = 2;
                    break;
                case "Wednesday":
                    filterDayOfWeek = 3;
                    break;
                case "Thursday":
                    filterDayOfWeek = 4;
                    break;
                case "Friday":
                    filterDayOfWeek = 5;
                    break;
                case "Saturday":
                    filterDayOfWeek = 6;
                    break;
            }
            return filterDayOfWeek;
        }       
    }
}
