﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class SaveMemberBookingsAction : USDBActionBase<int>
    {
        private int _classId;
        private int _memberId;
        private List<ClassBookingDC> _bookingList;
        private OrdinaryMemberDC _ordinaryMemberDC;
        private decimal _totalBookingAmount;
        private decimal _totalAvailableAmount;
        private string _scheduleCategoryType;
        private string _user;
        private int _branchId;

        public SaveMemberBookingsAction(int classId, OrdinaryMemberDC ordinaryMemberDC, int memberId, List<ClassBookingDC> bookingList, decimal totalBookingAmount, decimal totalAvailableAmount, string scheduleCategoryType, string user, int branchId)
        {
            this._classId = classId;
            this._memberId = memberId;
            this._bookingList = bookingList;
            this._ordinaryMemberDC = ordinaryMemberDC; 
            this._totalAvailableAmount = totalAvailableAmount;
            this._totalBookingAmount = totalBookingAmount;
            this._scheduleCategoryType = scheduleCategoryType;
            this._user = user;
            this._branchId = branchId;
        }

        protected override int Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSManageClassesSaveMemberBookings";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleCategoryType", DbType.String, _scheduleCategoryType));
                	 
	

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@totalBookingAmount", DbType.Decimal, _totalBookingAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@totalAvailableAmount", DbType.Decimal, _totalAvailableAmount));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@firstName", DbType.String, _ordinaryMemberDC.FirstName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastName", DbType.String, _ordinaryMemberDC.LastName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@birthdate", DbType.DateTime, _ordinaryMemberDC.BirthDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@personNo", DbType.String, _ordinaryMemberDC.PersonNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@email", DbType.String, _ordinaryMemberDC.Email));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@mobile", DbType.String, _ordinaryMemberDC.Mobile));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@telework", DbType.String, _ordinaryMemberDC.WorkTeleNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@telehome", DbType.String, _ordinaryMemberDC.PrivateTeleNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address1", DbType.String, _ordinaryMemberDC.Address1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address2", DbType.String, _ordinaryMemberDC.Address2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address3", DbType.String, _ordinaryMemberDC.Address3));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@zip", DbType.String, _ordinaryMemberDC.PostCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@postplace", DbType.String, _ordinaryMemberDC.PostPlace));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gender", DbType.String, _ordinaryMemberDC.Gender));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@description", DbType.String, _memberDC.d));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@age", DbType.Int32, _ordinaryMemberDC.Age));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberBranchId", DbType.Int32, _ordinaryMemberDC.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _ordinaryMemberDC.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastModifiedUser", DbType.String, _ordinaryMemberDC.ModifiedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberActiveStatus", DbType.String, _ordinaryMemberDC.ActiveStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@image", DbType.Binary, _ordinaryMemberDC.ProfilePicture));
                if (_ordinaryMemberDC.MemberCategory != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberCategoryid", DbType.Int32, _ordinaryMemberDC.MemberCategory.Id));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberCategoryid", DbType.Int32, 0));
                }

                DataTable bookingDT = new DataTable();
                DataColumn col = null;
                col = new DataColumn("ActiveTimeId", typeof(Int32));
                bookingDT.Columns.Add(col);
                col = new DataColumn("Participants", typeof(Int32));
                bookingDT.Columns.Add(col);
                col = new DataColumn("IsBooked", typeof(Boolean));
                bookingDT.Columns.Add(col);

                foreach (var booking in _bookingList)
                {
                    bookingDT.Rows.Add(booking.ActiveTimeId, booking.Participants, booking.IsBooked);
                }

                SqlParameter parameter = new SqlParameter();
                //The parameter for the SP must be of SqlDbType.Structured 
                parameter.ParameterName = "@bookingDetails";
                parameter.SqlDbType = System.Data.SqlDbType.Structured;
                parameter.Value = bookingDT;
                cmd.Parameters.Add(parameter);

                cmd.ExecuteScalar();
                return 1;
            }

            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
