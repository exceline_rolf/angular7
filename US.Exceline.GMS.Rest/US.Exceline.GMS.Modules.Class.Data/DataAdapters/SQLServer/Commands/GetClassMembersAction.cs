﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class GetClassMembersAction : USDBActionBase<List<ExcelineClassMemberDC>>
    {
       int _activeTimeId = 0;
       int _branchId = 0;

       public GetClassMembersAction(int branchId,int activeTimeId)
       {
            _activeTimeId = activeTimeId;
           _branchId = branchId;
       }

       protected override List<ExcelineClassMemberDC> Body(DbConnection connection)
       {
           List<ExcelineClassMemberDC> _excelineClassMemberLst = new List<ExcelineClassMemberDC>();
           string StoredProcedureName = "USExceGMSGetMemberDetailsByClass";

           try
           {
               DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeTimeId", DbType.Int32, _activeTimeId));
               DbDataReader reader = cmd.ExecuteReader();

               while (reader.Read())
               {
                   ExcelineClassMemberDC member = new ExcelineClassMemberDC();
                   member.MemberNumber = Convert.ToInt32(reader["MemberNumber"]);
                   member.MemberName = reader["MemberName"].ToString();
                   if (reader["VisitTime"] != DBNull.Value)
                   {
                       member.VisitTime = Convert.ToDateTime(reader["VisitTime"]);
                   }
                   _excelineClassMemberLst.Add(member);
               }              
           }

           catch(Exception ex)
           {
               throw ex;
           }

           return _excelineClassMemberLst;
       }
    }
}
