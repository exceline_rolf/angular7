﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteScheduleItemAction:USDBActionBase<int>
    {
        private int _scheduleItemId = -1;
        private string _user = string.Empty;

        public DeleteScheduleItemAction(int scheduleItemId,string user)
        {
            _scheduleItemId = scheduleItemId;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            int status = -1;
            const string storedProcedureName = "USExceGMSClassesDeleteScheduleItem";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleItemId", DbType.Int32, _scheduleItemId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _user));
                cmd.ExecuteNonQuery();
                status = 1;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }
    }
}
