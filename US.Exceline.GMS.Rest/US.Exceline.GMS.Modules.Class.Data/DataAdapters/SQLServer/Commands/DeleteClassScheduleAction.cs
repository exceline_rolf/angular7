﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ManageClasses;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteClassScheduleAction:USDBActionBase<bool>
    {
        private ExcelineClassDC _excelineClass;
        private string _user = string.Empty;

        public DeleteClassScheduleAction(ExcelineClassDC excelineClass, string user)
        {
            _excelineClass = excelineClass;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            bool _isDeleted = false;
            string StoredProcedureName = "USExceGMSClassesDeleteSchedule";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _excelineClass.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                cmd.ExecuteNonQuery();
                _isDeleted = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _isDeleted;
        }
    }
}
