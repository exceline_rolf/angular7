﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateExistingClassActiveTimeAction : USDBActionBase<bool>
    {
        private readonly int _activeTimeId = -1;
        private readonly DateTime _startDate;
        private readonly DateTime _endDate;
        private readonly int _maxNoOfBookings;
        private readonly int _noOfParticipants;

        public UpdateExistingClassActiveTimeAction(int activeTimeId, DateTime startDate, DateTime endDate, int maxNoOfBookings, int numberOfParticipants)
        {
            _activeTimeId = activeTimeId;
            _startDate = startDate;
            _endDate = endDate;
            _maxNoOfBookings = maxNoOfBookings;
            _noOfParticipants = numberOfParticipants;

        }
        protected override bool Body(System.Data.Common.DbConnection dbConnection)
        {
            return false;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            bool isUpdated = false;
            const string storedProcedure = "USEXCEGMSUpdateExistingClassActiveTime";

            try
            {
                var cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassActiveTimeId", System.Data.DbType.Int32, _activeTimeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDateTime", System.Data.DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDateTime", System.Data.DbType.DateTime, _endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MaxNoOfBookings", System.Data.DbType.Int32,_maxNoOfBookings));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NoOfParticipants", System.Data.DbType.Int32, _noOfParticipants));

                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;

                cmd.ExecuteNonQuery();
                isUpdated = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isUpdated;
        }
    }
}
