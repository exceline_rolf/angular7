﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateClassActiveTimeAction : USDBActionBase<int>
    {
        private readonly UpdateClassActiveTimeHelperDC _updateClassActiveTimeObj;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        private string _user = string.Empty;

        public UpdateClassActiveTimeAction(UpdateClassActiveTimeHelperDC updateClassActiveTimeObj, string user)
        {
            _updateClassActiveTimeObj = updateClassActiveTimeObj;
            _user = user;
        }
        protected override int Body(System.Data.Common.DbConnection dbConnection)
        {
            var overlappedStatus = 0;
            var isOverlapped = false;
            var dbTransaction = dbConnection.BeginTransaction();

            try
            {

                //foreach (var insId in _updateClassActiveTimeObj.AllINSIDList)
                //{
                //    const string storedProcName = "USEXCEGMSCheckEntityActiveTimeOverlapWIthEntityType";

                //    var cmd = CreateCommand(CommandType.StoredProcedure, storedProcName);
                //    cmd.Transaction = dbTransaction;
                //    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassActiveTimeID", DbType.Int32, _updateClassActiveTimeObj.ActiveTimeId));
                //    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityId", DbType.Int32, insId));
                //    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntRoleType", DbType.String, "INS"));
                //    cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", DbType.DateTime, _updateClassActiveTimeObj.StartDateTime));
                //    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", DbType.DateTime, _updateClassActiveTimeObj.EndDateTime));

                //    var outputVal = new SqlParameter();
                //    outputVal.DbType = DbType.Int32;
                //    outputVal.ParameterName = "@OutPutVal";
                //    outputVal.Direction = ParameterDirection.Output;
                //    cmd.Parameters.Add(outputVal);

                //    var errorMessage = new SqlParameter();
                //    errorMessage.DbType = DbType.String;
                //    errorMessage.Size = 3000;
                //    errorMessage.ParameterName = "@ErrorMessage";
                //    errorMessage.Direction = ParameterDirection.Output;
                //    cmd.Parameters.Add(errorMessage);

                //    cmd.ExecuteNonQuery();
                //    overlappedStatus = Convert.ToInt32(outputVal.Value);
                //    if (overlappedStatus != -1) continue;
                //    isOverlapped = true;
                //    break;
                //}

                //if (!isOverlapped && overlappedStatus != -1)
                //{
                //    foreach (var resId in _updateClassActiveTimeObj.AllRESIDList)
                //    {
                //        const string storedProcName = "USEXCEGMSCheckEntityActiveTimeOverlapWIthEntityType";

                //        var cmd = CreateCommand(CommandType.StoredProcedure, storedProcName);
                //        cmd.Transaction = dbTransaction;
                //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassActiveTimeID", DbType.Int32, _updateClassActiveTimeObj.ActiveTimeId));
                //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityId", DbType.Int32, resId));
                //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntRoleType", DbType.String, "RES"));
                //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", DbType.DateTime, _updateClassActiveTimeObj.StartDateTime));
                //        cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", DbType.DateTime, _updateClassActiveTimeObj.EndDateTime));

                //        var outputVal = new SqlParameter();
                //        outputVal.DbType = DbType.Int32;
                //        outputVal.ParameterName = "@OutPutVal";
                //        outputVal.Direction = ParameterDirection.Output;
                //        cmd.Parameters.Add(outputVal);

                //        var errorMessage = new SqlParameter();
                //        errorMessage.DbType = DbType.String;
                //        errorMessage.Size = 3000;
                //        errorMessage.ParameterName = "@ErrorMessage";
                //        errorMessage.Direction = ParameterDirection.Output;
                //        cmd.Parameters.Add(errorMessage);

                //        cmd.ExecuteNonQuery();
                //        overlappedStatus = Convert.ToInt32(outputVal.Value);

                //        if (overlappedStatus == -2)
                //        {
                //            isOverlapped = true;
                //            break;
                //        }
                //    }
                //}

                //if (!isOverlapped)
                //{
                    var updateAction = new UpdateClassActiveTimeWithEntityActiveTimeAction(_updateClassActiveTimeObj.ActiveTimeId, _updateClassActiveTimeObj.StartDateTime, _updateClassActiveTimeObj.EndDateTime, _updateClassActiveTimeObj.MaxNumberOfBookings, _updateClassActiveTimeObj.NoOfParticipants, _updateClassActiveTimeObj.AddedINSIDList, _updateClassActiveTimeObj.AddedRESIDList, _updateClassActiveTimeObj.DeletedINSIDList, _updateClassActiveTimeObj.DeletedRESIDList);
                    var isUpdated = updateAction.RunOnTransaction(dbTransaction);
                    if (isUpdated)
                    {
                        overlappedStatus = 0;
                        dbTransaction.Commit();
                    }
                //}
            }
            catch (Exception ex)
            {
                dbTransaction.Rollback();
                throw ex;
            }
            return overlappedStatus;
        }
    }
}


