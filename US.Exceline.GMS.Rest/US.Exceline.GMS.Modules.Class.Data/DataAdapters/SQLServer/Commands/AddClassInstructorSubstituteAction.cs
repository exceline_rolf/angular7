﻿using System;
using System.Data;
using System.Data.Common;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class AddClassInstructorSubstituteAction : USDBActionBase<bool>
    {
        private readonly UpdateClassActiveTimeHelperDC _updateClassActiveTimeObj;

        public AddClassInstructorSubstituteAction(UpdateClassActiveTimeHelperDC updateClassActiveTimeObj)
        {
            _updateClassActiveTimeObj = updateClassActiveTimeObj;
        }

        protected override bool Body(System.Data.Common.DbConnection dbConnection)
        {
            return false;
        }

        public void RunOnTransaction(DbTransaction dbTransaction)
        {
            try
            {
                foreach (var deletedIns in _updateClassActiveTimeObj.DeletedINSIDList)
                {
                    const string storedProcedureName = "USExceGMSAddClassInstructorSubstitute";

                    var command = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                    command.Transaction = dbTransaction;
                    command.Connection = dbTransaction.Connection;

                    command.Parameters.Add(DataAcessUtils.CreateParam("@ClassActiveTimeId", DbType.Int32,
                                                                      _updateClassActiveTimeObj.ActiveTimeId));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@StartDateTime", DbType.DateTime,
                                                                      _updateClassActiveTimeObj.StartDateTime));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@FormerInstructorId", DbType.Int32, deletedIns));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@AddedInstructorId", DbType.Int32, null));
                    command.ExecuteScalar();
                }

                foreach (var addedIns in _updateClassActiveTimeObj.AddedINSIDList)
                {
                    const string storedProcedureName = "USExceGMSUpdateClassInstructorSubstitute";

                    var command = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                    command.Transaction = dbTransaction;
                    command.Connection = dbTransaction.Connection;
                    command.Parameters.Add(DataAcessUtils.CreateParam("@ClassActiveTimeId", DbType.Int32,
                                                                      _updateClassActiveTimeObj.ActiveTimeId));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@StartDateTime", DbType.DateTime,
                                                                      _updateClassActiveTimeObj.StartDateTime));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@FormerInstructorId", DbType.Int32, null));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@AddedInstructorId", DbType.Int32, addedIns));
                    command.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
