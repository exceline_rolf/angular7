﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class GetEntityIdForScheduleItemAction : USDBActionBase<List<int>>
   {
       private int _scheduleItemId;
       private string _role = string.Empty;
       public GetEntityIdForScheduleItemAction(int scheduleItemId,string role)
       {
           _scheduleItemId = scheduleItemId;
           _role = role;
       }

       protected override List<int> Body(DbConnection connection)
       {
           var entityIdList = new List<int>();
           const string storedProcedureName = "GetEntityIdForScheduleItem";

           try
           {
               DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _scheduleItemId));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@Role", DbType.String, _role));
               DbDataReader reader = cmd.ExecuteReader();

               while (reader.Read())
               {
                   int entityId = Convert.ToInt32(reader["EntityId"]);
                   entityIdList.Add(entityId);
               }
           }

           catch (Exception ex)
           {
               throw ex;
           }

           return entityIdList;
       }
   }
}
