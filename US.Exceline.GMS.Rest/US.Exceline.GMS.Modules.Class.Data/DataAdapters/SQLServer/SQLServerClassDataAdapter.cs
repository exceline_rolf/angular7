﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Class.Data.SystemObjects;
using US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Data.DataAdapters.SQLServer.Commands;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer
{
    class SQLServerClassDataAdapter : IClassDataAdapter
    {
        public List<ExcelineClassDC> SearchClass(string category, string searchText, string gymCode)
        {
            return new List<ExcelineClassDC>();
        }

        public List<ExcelineClassDC> SearchClass(string category, string searchText, DateTime startDate, string gymCode)
        {
            return new List<ExcelineClassDC>();
        }

        public List<ExcelineClassDC> SearchClass(string category, string searchText, DateTime startDate, DateTime endDate, string gymCode)
        {
            return new List<ExcelineClassDC>();
        }

        public string SaveClass(ExcelineClassDC excelineClass, int branchId, string gymCode, string user)
        {
            AddClassDetailsAction addclassdetailsAction = new AddClassDetailsAction(excelineClass, branchId, gymCode, user);
            string classscheduleId = addclassdetailsAction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
            return classscheduleId;
        }
        public List<ScheduleItemDC> GetScheduleItemsByClass(int classId, int branchId, string gymCode)
        {
            GetClassScheduleItemsAction action = new GetClassScheduleItemsAction(classId, gymCode);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }
        public List<ScheduleItemDC> GetScheduleItemsByClassIds(List<int> classIds, int branchId, string gymCode)
        {
            List<ScheduleItemDC> totalItems = new List<ScheduleItemDC>();
            foreach (int classId in classIds)
            {
                GetClassScheduleItemsAction action = new GetClassScheduleItemsAction(classId, gymCode);
                totalItems.AddRange(action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode));
            }
            return totalItems;
        }
        public List<ExcelineClassDC> GetClasses(string className, int branchId, string gymCode)
        {
            List<ExcelineClassDC> classes = new List<ExcelineClassDC>();
            GetClassDetailsAction getAction = new GetClassDetailsAction(className, branchId);
            classes = getAction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
            return classes;
        }

        public int GetNextClassId(string gymCode)
        {
            int _nextClassId;
            GetNextClassIdAction action = new GetNextClassIdAction();
            _nextClassId = action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
            return _nextClassId;
        }

        public bool UpdateClass(ExcelineClassDC excelineClass, string gymCode)
        {
            bool _isUpdated = false;
            int output = 0;
            UpdateClassDetailsAction updateAction = new UpdateClassDetailsAction(excelineClass);
            output = updateAction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
            if (output > 0)
                _isUpdated = true;
            return _isUpdated;
        }

        public bool DeleteClass(int classId, string gymCode)
        {
            DeleteClassAction deleteAction = new DeleteClassAction(classId);
            return deleteAction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineClassActiveTimeDC> GetClassActiveTimes(int branchId, DateTime startDate, DateTime endDate, int entNo, string gymCode)
        {
            GetClassActiveTimesAction action = new GetClassActiveTimesAction(branchId, startDate, endDate, entNo);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public List<InstructorDC> GetInstructorsForClass(int classId, string gymCode)
        {
            GetInstructorsForClassAction action = new GetInstructorsForClassAction(classId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public List<ResourceDC> GetResourcesForClass(int classId, string gymCode)
        {
            GetResourcesForClassAction action = new GetResourcesForClassAction(classId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public ScheduleDC GetClassSchedule(int classId, string gymCode)
        {
            GetClassScheduleAction action = new GetClassScheduleAction(classId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public int GetExcelineClassIdByName(string className, string gymCode)
        {
            GetExcelineClassIdAction action = new GetExcelineClassIdAction(className);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public int UpdateTimeTableScheduleItems(List<ScheduleItemDC> scheduleItemList, int branchId, string gymCode, string user)
        {
            int isSaved = 0;
            foreach (ScheduleItemDC scheduleItem in scheduleItemList)
            {
                //CheckActiveTimeOverlapAction overlapAction = new CheckActiveTimeOverlapAction(scheduleItem);
                //int _overlappedStatus = overlapAction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);

                //if (_overlappedStatus < 0)
                //{
                //    isSaved = _overlappedStatus;
                //    break;
                //}
                //else
                //{
                    SaveScheduleItemAction action = new SaveScheduleItemAction(scheduleItem, branchId, user,false);
                    isSaved = action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
               // }
            }
            return isSaved;
        }

        public int SaveActiveTimes(List<ScheduleItemDC> scheduleItemList, string gymCode)
        {
            int isSaved = 0;
            foreach (ScheduleItemDC scheduleItem in scheduleItemList)
            {
                isSaved = 0;
                if (scheduleItem.ActiveTimes.Count > 0)
                {
                    SaveClassEntityActiveTimesAction action = new SaveClassEntityActiveTimesAction(scheduleItem);
                    isSaved = action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
                }
                else
                {
                    isSaved = 1;
                }
            }
            return isSaved;
        }

        public string CheckActiveTimeOverlapWithClass(ScheduleItemDC scheduleItem, string gymCode)
        {
            CheckActiveTimeOverlapWithClassAction action = new CheckActiveTimeOverlapWithClassAction(scheduleItem);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public int SaveScheduleItem(ScheduleItemDC scheduleItem, int branchId, string gymCode, string user)
        {
            int result = -2;
            //CheckActiveTimeOverlapAction checkOverlap = new CheckActiveTimeOverlapAction(scheduleItem);
            //result = checkOverlap.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);

            //if (result == 0)
            //{
                SaveScheduleItemAction action = new SaveScheduleItemAction(scheduleItem, branchId, user,false);
                result = action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);                
            //}           
            return result;
        }

        public int UpdateClassCalendarActiveTime(EntityActiveTimeDC activeTime, string user, string gymCode)
        {
            UpdateClassCalendarActiveTimeAction action = new UpdateClassCalendarActiveTimeAction(activeTime, user);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public int DeleteScheduleItem(List<int> scheduleItemIdList, string gymCode,string user)
        {
            int isDeleted = 0;
            foreach (int id in scheduleItemIdList)
            {
                US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands.DeleteScheduleItemAction action = new US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands.DeleteScheduleItemAction(id, user);
                isDeleted = action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
            }
            return isDeleted;
        }

        public bool DeleteSchedule(ExcelineClassDC exceClass, string gymCode, string user)
        {
            DeleteClassScheduleAction action = new DeleteClassScheduleAction(exceClass, user);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineClassMemberDC> GetMembersByActiveTimeId(int branchId, int activeTimeId, string gymCode)
        {
            GetClassMembersAction action = new GetClassMembersAction(branchId, activeTimeId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }


        public int UpdateSeasonEndDateWithClassActiveTimes(int branchId, int seasonId, DateTime endDate, string gymCode)
        {
            UpdateSesonEndDateWithClassActiveTimesAction action = new UpdateSesonEndDateWithClassActiveTimesAction(seasonId, branchId, endDate);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);            
        }


        public bool DeleteClassActiveTime(int activeTimeId, string gymCode, string user)
        {
            DeleteClassActiveTimeAction action = new DeleteClassActiveTimeAction(activeTimeId, user);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline,gymCode);
        }


        public int UpdateClassActiveTime(US.GMS.Core.SystemObjects.UpdateClassActiveTimeHelperDC helperObj, string gymCode, string user)
        {
            UpdateClassActiveTimeAction action = new UpdateClassActiveTimeAction(helperObj, user);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }


        public List<EntityActiveTimeDC> GetActiveTimesForClassScheduleItemAction(int scheduleItemId, string gymCode)
        {
            GetActiveTimesForClassScheduleItemAction action = new GetActiveTimesForClassScheduleItemAction(scheduleItemId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public string GetListOrCalendarInitialViewByUser(string user, string gymCode)
        {
            try
            {
                var action = new GetListOrCalendarInitialViewByUserAction(user);
                return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ScheduleData GetSeasonAndClassInfo(int branchId, int scheduleId, String gymCode)
        {
            try
            {
                GetClassDataAndInstances action = new GetClassDataAndInstances(branchId,scheduleId);
                return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
            }
            catch ( Exception ex)
            {
                throw ex;
            }
        }

    }
}
