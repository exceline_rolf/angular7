﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Class.Data.SystemObjects
{
    public class AlteredClassData
    {
        private List<InstructorData> _originalInstructorList;
        private List<InstructorData> _newInstructorList;
        private List<ResourceData> _originalResourceList;
        private List<ResourceData> _newResourceList;

        public AlteredClassData(List<InstructorData> OriginalInstructorList, List<InstructorData> NewInstructorList, List<ResourceData> OriginalResourceList, List<ResourceData> NewResourceList)
        {
            _originalInstructorList = OriginalInstructorList;
            _newInstructorList = NewInstructorList;
            _originalResourceList = OriginalResourceList;
            _newResourceList = NewResourceList;
            _addedInstructorsList = _newInstructorList.Except(_originalInstructorList).ToList();
            _removedInstructorsList = _originalInstructorList.Except(_newInstructorList).ToList();
            _addedResourceList = _newResourceList.Except(_originalResourceList).ToList();
            _removedResourceList = _originalResourceList.Except(_newResourceList).ToList();

        }

        public AlteredClassData() {
            _originalInstructorList = new List<InstructorData>();
            _newInstructorList = new List<InstructorData>();
            _originalResourceList = new List<ResourceData>();
            _newResourceList = new List<ResourceData>();
        }

        private DateTime? _oldDate = null;
        public DateTime?  OldDate
        {
            get { return _oldDate; }
            set { _oldDate = value; }
        }

        private DateTime? _newDate = null;
        public DateTime? NewDate
        {
            get { return _newDate; }
            set { _newDate = value; }
        }

        private Boolean _dateChanged = false;
        public Boolean DateChanged
        {
            get { return _dateChanged; }
            set { _dateChanged = value; }
        }

        public Boolean checkIfDateChanged()
        {
            if (_oldDate.HasValue && _newDate.HasValue)
            {
                return (_oldDate.Value.Date == _newDate.Value.Date) ? false : true;
            }
            else
            {
                return (_oldDate == _newDate) ? false : true;
            }
            
        }

        private DateTime? _oldStartTime = null;
        public DateTime? OldStartTime
        {
            get { return _oldStartTime; }
            set { _oldStartTime = value; }
        }

        private DateTime? _newStartTime = null;
        public DateTime? NewStartTime
        {
            get { return _newStartTime; }
            set { _newStartTime = value; }
        }


        private DateTime? _oldEndTime = null;
        public DateTime? OldEndTime
        {
            get { return _oldEndTime; }
            set { _oldEndTime = value; }
        }

        private DateTime? _newEndTime = null;
        public DateTime? NewEndTime
        {
            get { return _newEndTime; }
            set { _newEndTime = value; }
        }

        private Boolean? _startTimeChanged = null;
        public Boolean? StartTimeChanged
        {
            get { return _startTimeChanged; }
            set { _startTimeChanged = value; }
        }

        public Boolean checkIfStartTimeChanged()
        {
            return (_oldStartTime == _newStartTime) ? false : true;
        }

        private Boolean? _endTimeChanged = null;
        public Boolean? EndTimeChanged
        {
            get { return _endTimeChanged; }
            set { _endTimeChanged = value; }
        }

        public Boolean checkIfEndTimeChanged()
        {
            if (_oldEndTime.HasValue && _newEndTime.HasValue)
            {
                return (_oldEndTime.Value.TimeOfDay == _newEndTime.Value.TimeOfDay) ? false : true;
            }
            else
            {
                return (_oldEndTime == _newEndTime) ? false : true;
            }
           
        }

        private int? _oldMaxAmountOfBookings = null;
        public int? OldMaxAmountOfBookings
        {
            get { return _oldMaxAmountOfBookings; }
            set { _oldMaxAmountOfBookings = value; }
        }

        private int _newMaxAmountOfBookings = -1;
        public int NewMaxAmountOfBookings
        {
            get { return _newMaxAmountOfBookings; }
            set { _newMaxAmountOfBookings = value; }
        }

        private Boolean? _maxAmountOfBookingsChanged = null;
        public Boolean? MaxAmountOfBookingsChanged
        {
            get { return _maxAmountOfBookingsChanged; }
            set { _maxAmountOfBookingsChanged = value; }
        }

        public Boolean checkIfMaxAmountOfBookingsChanged()
        {
            return (_oldMaxAmountOfBookings == _newMaxAmountOfBookings) ? false : true;
        }

        private List<InstructorData> _addedInstructorsList = new List<InstructorData>();
        public List<InstructorData> AddedInstructorsList
        {
          //  set { _addedInstructorsList = _newInstructorList.Except(_originalInstructorList).ToList(); }
            get { return _addedInstructorsList; }
        }

        private List<InstructorData> _removedInstructorsList = new List<InstructorData>();
        public List<InstructorData> RemovedInstructorsList
        {
           // set { _removedInstructorsList = _originalInstructorList.Except(_newInstructorList).ToList(); } 
            get { return _removedInstructorsList; }
        }

        private Boolean? _instructorListChanged = null;
        public Boolean? InstructorListChanged
        {
            get { return _instructorListChanged; }
            set { _instructorListChanged = value; }
        }

        public Boolean checkIfInstructorListChanged()
        {
                
            return ((AddedInstructorsList.Any() || RemovedInstructorsList.Any()) && (AddedInstructorsList.Except(RemovedInstructorsList).ToList().Count() > 0 || RemovedInstructorsList.Except(AddedInstructorsList).ToList().Count > 0)) ? true : false;
        }

        private Boolean? _resourceListChanged = null;
        public Boolean? ResourceListChanged
        {
            get { return _resourceListChanged; }
            set { _resourceListChanged = value; }
        }

        private List<ResourceData> _addedResourceList = new List<ResourceData>();
        public List<ResourceData> AddedResourceList
        {
            get { return _addedResourceList; }
        }

        private List<ResourceData> _removedResourceList = new List<ResourceData>();
        public List<ResourceData> RemovedResourceList
        {

            get { return _removedResourceList;  }
        }

        public Boolean checkIfResourceListChanged()
        {
            return ((_addedResourceList.Any() || _removedResourceList.Any()) && (_addedResourceList.Except(_removedResourceList).ToList().Count() > 0 || _removedResourceList.Except(_addedResourceList).ToList().Count() > 0) ) ? true : false;
        }

        private DateTime? _lastModifiedDateTime = null;
        public DateTime? LastModifiedDateTime
        {
            get { return _lastModifiedDateTime; }
            set { _lastModifiedDateTime = value; }
        }

        private String _lastModifiedUser = String.Empty;
        public String LastModifiedUser
        {
            get { return _lastModifiedUser; }
            set { _lastModifiedUser = value; }
        }
    }
}
