﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Class.Data.SystemObjects
{
    public class ResourceData : IEquatable<ResourceData>
    {
        private int _id = -1;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _activeStatus = -1;
        public int ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private int _branchId = -1;
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private String _displayName = String.Empty;
        public String DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        private int _categoryId = -1;
        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }

        private int _activityId = -1;
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        public override bool Equals(object obj)
        {
            var other = obj as ResourceData;
            if (other == null) return false;

            return Equals(other);
        }

        public bool Equals(ResourceData other)
        {
            if (other == null)
            {
                return false;
            }

            return other.Id == _id;
        }

        public override int GetHashCode() => (Id).GetHashCode();
    }
}
