﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.BusinessLogic
// Coding Standard   : US Coding Standards
// Author            : DKA
// Created Timestamp : "10/19/2013 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageMember;
using US.GMS.Core.DomainObjects.Common;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageJobs;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Globalization;

namespace US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageJobs
{
    public class JobManager
    {
        public static OperationResult<int> SaveExcelineJob(ScheduleItemDC scheduleItem, int branchId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                scheduleItem.ActiveTimes = GetWeekActiveTimes(scheduleItem, scheduleItem.StartDate, scheduleItem.EndDate);
                result.OperationReturnValue = JobFacade.SaveExcelineJob(scheduleItem, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Job Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ScheduleItemDC>> GetJobScheduleItems(int branchId, string gymCode)
        {
            OperationResult<List<ScheduleItemDC>> result = new OperationResult<List<ScheduleItemDC>>();
            try
            {
                result.OperationReturnValue = JobFacade.GetJobScheduleItems(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Job ScheduleItems" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineCommonTaskDC>> GetAllTasksByEmployeeId(int branchId, int employeeId, string gymCode, string user)
        {
            OperationResult<List<ExcelineCommonTaskDC>> result = new OperationResult<List<ExcelineCommonTaskDC>>();
            try
            {
                result.OperationReturnValue = JobFacade.GetAllTasksByEmployeeId(branchId, employeeId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Tasks" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineCommonTaskDC>> GetFollowUpByEmployeeId(int branchId, int employeeId, string gymCode, string user, int hit)
        {
            var result = new OperationResult<List<ExcelineCommonTaskDC>>();
            try
            {
                result.OperationReturnValue = JobFacade.GetFollowUpByEmployeeId(branchId, employeeId, gymCode, user, hit);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Follow up" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineCommonTaskDC>> GetJobsByEmployeeId(int branchId, int employeeId, string gymCode, string user, int hit)
        {
            var result = new OperationResult<List<ExcelineCommonTaskDC>>();
            try
            {
                result.OperationReturnValue = JobFacade.GetJobsByEmployeeId(branchId, employeeId, gymCode, user, hit);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Jobs" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> DeleteScheduleItem(List<int> scheduleItemIdList, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = JobFacade.DeleteScheduleItem(scheduleItemIdList, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Deleting schedule item" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> AssignTaskToEmployee(ExcelineCommonTaskDC commonTask, int employeeId, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = JobFacade.AssignTaskToEmployee(commonTask, employeeId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Assigning task to employee" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        private static List<EntityActiveTimeDC> GetWeekActiveTimes(ScheduleItemDC scheduleItem, DateTime StartDate, DateTime EndDate)
        {
            switch (scheduleItem.Day)
            {
                case "Sunday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Sunday, StartDate, EndDate);

                case "Monday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Monday, StartDate, EndDate);

                case "Tuesday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Tuesday, StartDate, EndDate);

                case "Wednesday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Wednesday, StartDate, EndDate);

                case "Thursday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Thursday, StartDate, EndDate);

                case "Friday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Friday, StartDate, EndDate);

                case "Saturday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Saturday, StartDate, EndDate);

                case "søndag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Sunday, StartDate, EndDate);

                case "mandag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Monday, StartDate, EndDate);

                case "tirsdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Tuesday, StartDate, EndDate);

                case "onsdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Wednesday, StartDate, EndDate);

                case "torsdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Thursday, StartDate, EndDate);

                case "fredag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Friday, StartDate, EndDate);

                case "lørdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Saturday, StartDate, EndDate);
            }
            return new List<EntityActiveTimeDC>();
        }
        private static List<EntityActiveTimeDC> GenerateActiveTimes(ScheduleItemDC scheduleItem, DayOfWeek dayofWeek, DateTime StartDate, DateTime EndDate)
        {
            List<EntityActiveTimeDC> activeTimes = new List<EntityActiveTimeDC>();
            TimeSpan duration = EndDate - StartDate;
            DateTime activestartDate = StartDate;
            if (scheduleItem.WeekType == 2)
            {
                for (int i = 0; i < duration.Days + 1; i++)
                {
                    if (activestartDate.DayOfWeek == dayofWeek)
                    {
                        EntityActiveTimeDC activeTime = new EntityActiveTimeDC();
                        activeTime.SheduleItemId = scheduleItem.Id;
                        activeTime.StartDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.StartTime.ToShortTimeString());
                        activeTime.EndDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.EndTime.ToShortTimeString());
                        activeTimes.Add(activeTime);
                    }
                    activestartDate = activestartDate.AddDays(1);
                }
            }
            else
            {
                int durationDays = duration.Days;
                DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                System.Globalization.Calendar cal = dfi.Calendar;
                int weekNumber = cal.GetWeekOfYear(activestartDate, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
                int oddEvenStatus = weekNumber % 2;
                if (oddEvenStatus != scheduleItem.WeekType)
                {
                    activestartDate = activestartDate.AddDays(7);
                    durationDays = duration.Days - 7;
                }
                for (int i = 0; i < durationDays + 1; i++)
                {
                    if (activestartDate.DayOfWeek == dayofWeek)
                    {
                        EntityActiveTimeDC activeTime = new EntityActiveTimeDC();
                        activeTime.SheduleItemId = scheduleItem.Id;
                        activeTime.StartDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.StartTime.ToShortTimeString());
                        activeTime.EndDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.EndTime.ToShortTimeString());
                        activeTimes.Add(activeTime);
                        if (scheduleItem.WeekType != 2)
                        {
                            activestartDate = activestartDate.AddDays(7);
                            i += 7;
                        }
                    }
                    activestartDate = activestartDate.AddDays(1);
                }

            }
            return activeTimes;
        }

    }
}
