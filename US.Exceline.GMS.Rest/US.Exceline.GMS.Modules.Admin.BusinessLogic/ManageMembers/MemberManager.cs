﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.BusinessLogic
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "06/06/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageMember;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageMembers
{
    public class MemberManager
    {

         public static OperationResult<List<string>> GetAccessPoints(string gymCode)
        {
            OperationResult<List<string>> result = new OperationResult<List<string>>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetAccessPoints(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Access points" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<EntityVisitDC>> GetMemberVisits(DateTime startDate, DateTime endDate, int branchId, string gymCode)
        {
            OperationResult<List<EntityVisitDC>> result = new OperationResult<List<EntityVisitDC>>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetMemberVisits(startDate, endDate, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Member Visits" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<List<ExcelineMemberDC>> GetSponsorsforSponsoring(int branchId, DateTime startDate, DateTime endDate, string gymCode)
        {
            OperationResult<List<ExcelineMemberDC>> result = new OperationResult<List<ExcelineMemberDC>>();
            try
            {
                List<ExcelineMemberDC> sponsors = new List<ExcelineMemberDC>();
                sponsors = MemberFacade.GetSponsorsforSponsoring(branchId, startDate, endDate, gymCode);
                result.OperationReturnValue = sponsors;
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Sponsors" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> GetSponsoringProcessStatus(string guiID, string gymCode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetSponsoringProcessStatus(guiID, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in checking  sponsoring  status" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<bool> GenerateSponsorOrders(US.GMS.Core.SystemObjects.SponsorShipGenerationDetailsDC sponsorDetails, string gymCode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = MemberFacade.GenerateSponsorOrders(sponsorDetails, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Sponsors" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
