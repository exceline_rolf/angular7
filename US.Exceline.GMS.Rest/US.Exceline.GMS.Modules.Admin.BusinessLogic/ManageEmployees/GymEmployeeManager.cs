﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            :
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.ResultNotifications;
using System.IO;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageResources;
using System.Globalization;
using System.Drawing;
using System.Configuration;
using US.GMS.BusinessLogic;

namespace US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageEmployees
{
    public class GymEmployeeManager
    {
        public static OperationResult<string> SaveGymEmployee(GymEmployeeDC employee, string imageFolderPath, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.SaveGymEmployee(employee, gymCode);
                if (!string.IsNullOrEmpty(employee.Image))
                {
                    string imagePath = StringToImage(employee.Image, employee.Id + employee.Mobile, imageFolderPath, gymCode);
                    //string imagePath = byteArrayToImage(employee.ProfilePicture, employee.FirstName + employee.Mobile, imageFolderPath);
                    employee.ImagePath = imagePath;
                }

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Employee Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

      

        public static string StringToImage(string base64ImgString, string employeeId, string imageFolderPath,string gymCode)
        {
            string imagePath = string.Empty;
            int gymId = GymDetailHandler.GetGymCompanyId(gymCode).OperationReturnValue;


            if (!string.IsNullOrEmpty(base64ImgString))
            {
                try
                {
                    if (!Directory.Exists(imageFolderPath+'/'+gymId))
                    {
                        Directory.CreateDirectory(imageFolderPath + '/' + gymId);
                    }
                    if (File.Exists(imageFolderPath + '/' + gymId + '/' + employeeId + ".jpg"))
                    {
                        File.Delete(imageFolderPath + employeeId + ".jpg");
                    }
                    imagePath = imageFolderPath + '/' + gymId + '/' + employeeId + ".jpg";

                    string[] imageStringInfo = base64ImgString.Split(',');
                    byte[] imageBytes = Convert.FromBase64String(imageStringInfo[1]);
                    Image image;
                    using (MemoryStream ms = new MemoryStream(imageBytes))
                    {
                        image = Image.FromStream(ms);
                        image.Save(imagePath);
                    }
                    
                }
                catch (Exception)
                {

                    throw;
                }   
            }
            return imagePath;
        }

        public static string byteArrayToImage(byte[] byteArrayIn, string memberName, string imageFolderPath)
        {
            if (byteArrayIn != null)
            {
                string imagePath = string.Empty;
                try
                {
                    if (!Directory.Exists(imageFolderPath))
                    {
                        Directory.CreateDirectory(imageFolderPath);
                    }
                    if (File.Exists(imageFolderPath + memberName + ".file"))
                    {
                        File.Delete(imageFolderPath + memberName + ".file");
                    }
                    ByteArrayToFile(imageFolderPath + memberName + ".file", byteArrayIn);
                    imagePath = imageFolderPath + memberName + ".file";
                }
                catch
                {
                    return string.Empty;
                }
                return imagePath;
            }
            return string.Empty;
        }

        public static bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
        {
            try
            {
                System.IO.FileStream _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(_ByteArray, 0, _ByteArray.Length);
                _FileStream.Close();
                return true;
            }
            catch
            {
            }
            return false;
        }

        public static OperationResult<int> UpdateGymEmployee(GymEmployeeDC employee, int branchId, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.UpdateGymEmployee(employee, branchId, user,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Employee Updating" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        //public static OperationResult<bool> DeleteGymEmployee(int employeeId, int branchId, string user, string gymCode)
        //{
        //    OperationResult<bool> result = new OperationResult<bool>();
        //    try
        //    {
        //        result.OperationReturnValue = GymEmployeeFacade.DeleteGymEmployee(employeeId, branchId, user,  gymCode);
        //    }
        //    catch (Exception ex)
        //    {
        //        result.CreateMessage("Error in Gym Employee Deleting" + ex.Message, MessageTypes.ERROR);
        //    }
        //    return result;
        //}

        public static OperationResult<bool> DeActivateGymEmployee(int employeeId, int branchId, string user, string gymCode)
        {

            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.DeActivateGymEmployee(employeeId, branchId, user,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Employee Deactivating" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<GymEmployeeDC>> GetGymEmployees(int branchId, string searchText, bool isActive, int roleId, string gymCode)
        {

            OperationResult<List<GymEmployeeDC>> result = new OperationResult<List<GymEmployeeDC>>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.GetGymEmployees(branchId, searchText, isActive,roleId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Employee Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }



        public static OperationResult<GymEmployeeDC> GetGymEmployeeById(int branchId, int employeeId, string gymCode)
        {
            OperationResult<GymEmployeeDC> result = new OperationResult<GymEmployeeDC>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.GetGymEmployeeById(branchId, employeeId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Employee Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<GymEmployeeDC> GetGymEmployeeByEmployeeId(int branchId, string user, int employeeId, string gymCode)
        {

            OperationResult<GymEmployeeDC> result = new OperationResult<GymEmployeeDC>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.GetGymEmployeeByEmployeeId(branchId, user,employeeId,  gymCode);

                if (!string.IsNullOrEmpty(result.OperationReturnValue.ImagePath))
                {
                    // adding for the image path url
                    int gymId = GymDetailHandler.GetGymCompanyId(gymCode).OperationReturnValue;
                    result.OperationReturnValue.ImageURLDomain = ConfigurationManager.AppSettings["ImageUrl"] + '/' +
                        gymId + '/' + employeeId + ".jpg";
                    /////////////////////////////////////////////
                }
                else
                {
                    result.OperationReturnValue.ImageURLDomain = string.Empty;
                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Employee Getting by gym employeeid" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> ValidateEmployeeFollowUp(int employeeId, DateTime endDate, string gymCode)
        {

            var result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.ValidateEmployeeFollowUp(employeeId, endDate, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in validate employee followup" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<GymEmployeeRoleDurationDC>> GetGymEmployeeRoleDuration(int employeeId, string gymCode)
        {

            OperationResult<List<GymEmployeeRoleDurationDC>> result = new OperationResult<List<GymEmployeeRoleDurationDC>>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.GetGymEmployeeRoleDuration(employeeId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in GetGym Employee Role Duration by gym employeeid" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        public static OperationResult<List<GymEmployeeWorkPlanDC>> GetGymEmployeeWorkPlan(int employeeId, string gymCode)
        {

            OperationResult<List<GymEmployeeWorkPlanDC>> result = new OperationResult<List<GymEmployeeWorkPlanDC>>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.GetGymEmployeeWorkPlan(employeeId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in GetGym Employee Work Plan by gym employeeid" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<GymEmployeeApprovalDC>> GetGymEmployeeApprovals(int employeeId, string approvalType, string gymCode)
        {
            OperationResult<List<GymEmployeeApprovalDC>> result = new OperationResult<List<GymEmployeeApprovalDC>>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.GetGymEmployeeApprovals(employeeId,approvalType, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in GetGym Employee Approvals by gym employeeid" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool>  AddEmployeeTimeEntry(EmployeeTimeEntryDC timeEntry, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.AddEmployeeTimeEntry (timeEntry,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Adding  gym employee time entry" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<EmployeeTimeEntryDC>> GetEmployeeTimeEntries(int employeeId, string gymCode)
        {
            OperationResult<List<EmployeeTimeEntryDC>> result = new OperationResult<List<EmployeeTimeEntryDC>>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.GetEmployeeTimeEntries(employeeId ,gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting GetGym time Entries" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveGymEmployeeWorkScheduleItem(ScheduleItemDC scheduleItem,int resourceId, int branchId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                if (scheduleItem.StartDate.Date < DateTime.Now.AddMonths(1).Date)
                {
                    DateTime startDate = scheduleItem.StartDate;
                    DateTime endDate = scheduleItem.EndDate.Date;
                    if (startDate.Date <= DateTime.Now.Date)
                    {
                        startDate = DateTime.Now.Date.AddDays(1);
                    }
                    if (endDate > scheduleItem.EndDate.Date)
                    {
                        endDate = scheduleItem.EndDate.Date;
                    }
                    scheduleItem.ActiveTimes = GetWeekActiveTimes(scheduleItem, startDate, endDate);
                }

                result.OperationReturnValue = GymEmployeeFacade.SaveGymEmployeeWork(scheduleItem,resourceId, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Saving schedule item" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveWorkActiveTime(GymEmployeeWorkPlanDC work, int branchId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.SaveWorkActiveTime(work,branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Updating work" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveWorkItem(GymEmployeeWorkDC work, int branchId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.SaveWorkItem(work, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Adding work" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        private static List<EntityActiveTimeDC> GetWeekActiveTimes(ScheduleItemDC scheduleItem, DateTime startDate, DateTime endDate)
        {
            switch (scheduleItem.Day)
            {
                case "Sunday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Sunday, startDate, endDate);

                case "Monday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Monday, startDate, endDate);

                case "Tuesday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Tuesday, startDate, endDate);

                case "Wednesday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Wednesday, startDate, endDate);

                case "Thursday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Thursday, startDate, endDate);

                case "Friday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Friday, startDate, endDate);

                case "Saturday":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Saturday, startDate, endDate);

                case "søndag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Sunday, startDate, endDate);

                case "mandag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Monday, startDate, endDate);

                case "tirsdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Tuesday, startDate, endDate);

                case "onsdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Wednesday, startDate, endDate);

                case "torsdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Thursday, startDate, endDate);

                case "fredag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Friday, startDate, endDate);

                case "lørdag":
                    return GenerateActiveTimes(scheduleItem, DayOfWeek.Saturday, startDate, endDate);
            }
            return new List<EntityActiveTimeDC>();
        }
        private static List<EntityActiveTimeDC> GenerateActiveTimes(ScheduleItemDC scheduleItem, DayOfWeek dayofWeek, DateTime startDate, DateTime endDate)
        {
            List<EntityActiveTimeDC> activeTimes = new List<EntityActiveTimeDC>();
            TimeSpan duration = endDate - startDate;
            DateTime activestartDate = startDate;
            if (scheduleItem.WeekType == 2)
            {
                for (int i = 0; i < duration.Days + 1; i++)
                {
                    if (activestartDate.DayOfWeek == dayofWeek)
                    {
                        EntityActiveTimeDC activeTime = new EntityActiveTimeDC();
                        activeTime.SheduleItemId = scheduleItem.Id;
                        activeTime.StartDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.StartTime.ToShortTimeString());
                        activeTime.EndDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.EndTime.ToShortTimeString());
                        activeTimes.Add(activeTime);
                        if (scheduleItem.WeekType != 2)
                        {
                            activestartDate = activestartDate.AddDays(7);
                            i += 7;
                        }
                    }
                    activestartDate = activestartDate.AddDays(1);
                }
            }
            else
            {
                int durationDays = duration.Days;
                DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                Calendar cal = dfi.Calendar;
                int weekNumber = cal.GetWeekOfYear(activestartDate, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
                int oddEvenStatus = weekNumber % 2;
                if (oddEvenStatus != scheduleItem.WeekType)
                {
                    activestartDate = activestartDate.AddDays(7);
                    durationDays = duration.Days - 7;
                }
                for (int i = 0; i < durationDays + 1; i++)
                {
                    if (activestartDate.DayOfWeek == dayofWeek)
                    {
                        EntityActiveTimeDC activeTime = new EntityActiveTimeDC();
                        activeTime.SheduleItemId = scheduleItem.Id;
                        activeTime.StartDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.StartTime.ToShortTimeString());
                        activeTime.EndDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.EndTime.ToShortTimeString());
                        activeTimes.Add(activeTime);
                        if (scheduleItem.WeekType != 2)
                        {
                            activestartDate = activestartDate.AddDays(7);
                            i += 7;
                        }
                    }
                    activestartDate = activestartDate.AddDays(1);
                }

            }
            return activeTimes;
        }


        public static OperationResult<bool> ApproveAllEmployeeTimes(int employeeId, bool isAllApproved, string timeIdList, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.ApproveEmployeeTimes(employeeId, isAllApproved,timeIdList,gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Approving Employee times" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<int> AdminApproveEmployeeWork(GymEmployeeWorkDC work, bool isApproved, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.AdminApproveEmployeeWork(work, isApproved, user, gymCode);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in Approving Employee times" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteGymEmployeeActiveTime(int activeTimeId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.DeleteGymEmployeeActiveTime(activeTimeId, gymCode);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in Delete GymEmployee ActiveTime" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<EmployeeEventDC>> GetEmployeeEvents(int employeeId, string gymCode)
        {
            OperationResult<List<EmployeeEventDC>> result = new OperationResult<List<EmployeeEventDC>>();
            try
            {
               result.OperationReturnValue =  GymEmployeeFacade.GetEmployeeEvents(employeeId, gymCode);
            }
            catch(Exception ex)
            {
                result.CreateMessage("Error in Getting Gym Employee Events" + ex.Message, MessageTypes.ERROR);            
            }

            return result;
        }

        public static OperationResult<List<EmployeeJobDC>> GetEmployeeJobs(int employeeId, string gymCode)
        {
            OperationResult<List<EmployeeJobDC>> result = new OperationResult<List<EmployeeJobDC>>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.GetEmployeeJobs(employeeId,gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Gym Employee Jobs" + ex.Message, MessageTypes.ERROR);  
            }
            return result;
        }

        public static OperationResult<List<EmployeeBookingDC>> GetEmployeeBookings(int employeeId, string gymCode)
        {
            OperationResult<List<EmployeeBookingDC>> result = new OperationResult<List<EmployeeBookingDC>>();
            try
            {
                result.OperationReturnValue = GymEmployeeFacade.GetEmployeeBookings(employeeId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Employee Bookings" + ex.Message, MessageTypes.ERROR);
            }
            return result;
            
        }


         

    }
}
