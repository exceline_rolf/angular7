﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "3/29/2012 8:25:06 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.ResultNotifications;
using System.IO;

namespace US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageEmployees
{
    public class TrainerManager
    {
        public static string byteArrayToImage(byte[] byteArrayIn, string memberName, string imageFolderPath)
        {
            if (byteArrayIn != null)
            {
                string imagePath = string.Empty;
                try
                {
                    if (!Directory.Exists(imageFolderPath))
                    {
                        Directory.CreateDirectory(imageFolderPath);
                    }
                    if (File.Exists(imageFolderPath + memberName + ".file"))
                    {
                        File.Delete(imageFolderPath + memberName + ".file");
                    }
                    ByteArrayToFile(imageFolderPath + memberName + ".file", byteArrayIn);
                    imagePath = imageFolderPath + memberName + ".file";
                }
                catch
                {
                    return string.Empty;
                }
                return imagePath;
            }
            return string.Empty;
        }

        public static bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
        {
            try
            {
                System.IO.FileStream _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(_ByteArray, 0, _ByteArray.Length);
                _FileStream.Close();
                return true;
            }
            catch
            {
            }
            return false;
        }

        public static OperationResult<List<TrainerDC>> GetTrainers(int branchId, string searchText, bool isActive, int trainerId, string gymCode)
        {
            OperationResult<List<TrainerDC>> result = new OperationResult<List<TrainerDC>>();
            try
            {
                result.OperationReturnValue = TrainerFacade.GetTrainers(branchId, searchText, isActive, trainerId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Trainer Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
