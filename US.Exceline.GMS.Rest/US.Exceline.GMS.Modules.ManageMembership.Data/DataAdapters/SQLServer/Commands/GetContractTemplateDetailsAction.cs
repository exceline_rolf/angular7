﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/14/2012 5:39:19 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractTemplateDetailsAction : USDBActionBase<PackageDC>
    {
        private string _type;
        private int _templateId;

        public GetContractTemplateDetailsAction(string type, int templateId)
        {
            _type = type;
            _templateId = templateId;
        }

        protected override PackageDC Body(System.Data.Common.DbConnection connection)
        {

            string StoredProcedureName = "USExceGMSManageMembershipGetContractDetailsById";
            PackageDC package = null;
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@templateId", DbType.Int32, _templateId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, _type));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    package = new PackageDC();
                    package.ContractTypeValue = new US.GMS.Core.DomainObjects.Common.CategoryDC();
                    package.PackageId = Convert.ToInt32(reader["Id"]);
                    package.PackageName = reader["Name"].ToString();
                    package.ContractTypeValue.Id = Convert.ToInt32(reader["PackageTypeId"].ToString());
                    package.ContractTypeValue.Code = Convert.ToString(reader["PackageType"].ToString());
                    package.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    package.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    package.PackagePrice = Convert.ToDecimal(reader["PackagePrice"]);
                    package.NumOfInstallments = Convert.ToInt32(reader["NumberOfInstallments"]);
                    package.RatePerInstallment = Convert.ToDecimal(reader["RatePerInstallment"]);
                    package.NumOfVisits = Convert.ToInt32(reader["NumberOfVisits"]);
                    package.NoOfVisits = Convert.ToInt32(reader["NumberOfVisits"]);
                    package.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    package.IsBookingActivity = Convert.ToBoolean(reader["IsBookingActivity"]);
                    package.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    package.ActivityName = Convert.ToString(reader["ActivityName"]);
                    package.RestPlusMonth = Convert.ToBoolean(reader["RestPlusMonth"]);
                    package.LockInPeriod = Convert.ToInt32(reader["LockInPeriod"]);
                    package.PriceGuaranty = Convert.ToInt32(reader["PriceGuaranty"]);
                    package.AutoRenew = Convert.ToBoolean(reader["AutoRenew"]);
                    package.IsInvoiceDetail = Convert.ToBoolean(reader["InvoiceDetail"]);
                    package.ArticleNo = Convert.ToInt32(reader["ArticleId"]);
                    package.ArticleText = Convert.ToString(reader["ArticleText"]);
                    package.AccessProfileId = Convert.ToInt32(reader["AccessProfileId"]);
                    package.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    if (reader["FixDateOfContract"] != DBNull.Value)
                        package.FixDateOfContract = Convert.ToDateTime(reader["FixDateOfContract"]);
                   
                    package.NextTemplateId = Convert.ToInt32(reader["NextTemplateNo"]);
                    package.OrderPrice = Convert.ToDecimal(reader["OrderPrice"]);
                    package.StartUpItemPrice = Convert.ToDecimal(reader["StartupItemPrice"]);
                    package.EveryMonthItemsPrice = Convert.ToDecimal(reader["EveryMonthItemPrice"]);
                    package.NextContractTemplateName = Convert.ToString(reader["NextTemplateName"]);
                    package.NoOfMonths = Convert.ToInt32(reader["NoOfMonths"]);
                    package.NoOfDays = Convert.ToInt32(reader["NoOfDays"]);
                    package.TemplateNumber = Convert.ToString(reader["TemplateNo"]);
                    package.MaxAge = Convert.ToInt32(reader["MaxAge"]);
                    if (reader["FirstDueDate"] != DBNull.Value)
                        package.FirstDueDate = Convert.ToDateTime(reader["FirstDueDate"]);
                    package.PackageCategory.Id = Convert.ToInt32(reader["PackateCatId"]);
                    package.PackageCategory.Name = Convert.ToString(reader["PackageCatName"]);
                    package.PackageCategory.Code = Convert.ToString(reader["PackageTypeCode"]);
                    if (reader["StartDateOfContract"] != DBNull.Value)
                        package.FixStartDateOfContract = Convert.ToDateTime(reader["StartDateOfContract"]);
                    package.NextTemplateNo = Convert.ToString(reader["NextTemplateNo"]);
                    package.SortingNo = Convert.ToInt32(reader["Priority"]);
                    if (_type != "ID")
                    {
                        package.ContractConditionId = Convert.ToInt32(reader["ContractConditionId"]);
                        package.ContractCondition = Convert.ToString(reader["ContractCondition"]);
                    }
                    if (string.IsNullOrEmpty(package.NextTemplateNo) || package.NextTemplateNo.Trim().Equals("0"))
                    package.NextTemplateNo = string.Empty;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return package;
        }

    }
}
