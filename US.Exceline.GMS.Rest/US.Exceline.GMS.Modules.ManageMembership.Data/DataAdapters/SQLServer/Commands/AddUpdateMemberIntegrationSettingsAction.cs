﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class AddUpdateMemberIntegrationSettingsAction : USDBActionBase<bool>
    {
        private readonly MemberIntegrationSettingDC _memberIntegrationSettingsDetail;

        public AddUpdateMemberIntegrationSettingsAction(MemberIntegrationSettingDC memberIntegrationSettingsDetail)
        {
            _memberIntegrationSettingsDetail = memberIntegrationSettingsDetail;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "dbo.USExceGMSAddUpdateMemberIntegrationSettings";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _memberIntegrationSettingsDetail.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _memberIntegrationSettingsDetail.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberIntegrationSettingsDetail.MemberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Token", DbType.String, _memberIntegrationSettingsDetail.Token));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserId", DbType.String, _memberIntegrationSettingsDetail.UserId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FacilityUserId", DbType.String, _memberIntegrationSettingsDetail.FacilityUserId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SystemName", DbType.String, _memberIntegrationSettingsDetail.SystemName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _memberIntegrationSettingsDetail.User));

                DbParameter para1 = new SqlParameter();
                para1.DbType = DbType.Int32;
                para1.ParameterName = "@outPutID";
                para1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para1);
                cmd.ExecuteNonQuery();
                var outputId = Convert.ToInt32(para1.Value);
                if (outputId > 0)
                {
                    return true;
                }
                return false;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
