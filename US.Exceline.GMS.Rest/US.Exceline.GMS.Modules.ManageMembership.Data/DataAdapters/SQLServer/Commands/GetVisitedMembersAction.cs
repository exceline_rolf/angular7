﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US_DataAccess;


namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetVisitedMembersAction : USDBActionBase<List<MemberForMemberlist>>
    {
        private string _user;

        public GetVisitedMembersAction(string user)
        {
            _user = user;     

        }

        protected override List<MemberForMemberlist> Body(DbConnection connection)
        {
            var memberList = new List<MemberForMemberlist>();
            const string storedProcedureName = "USExceGMSManageMembershipGetVisitedMembers";

            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@username", DbType.String, _user));

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var member = new MemberForMemberlist();
                    member.ID = Convert.ToInt32(reader["ID"]);
                    member.FirstName = reader["FirstName"].ToString();
                    member.LastName = reader["LastName"].ToString();
                    member.Age = Convert.ToInt32(reader["Age"]);
                    member.RoleId = reader["RoleId"].ToString();
                    member.Email = reader["Email"].ToString();
                    member.BranchID = Convert.ToInt32(reader["BranchId"]);
                    member.CustId = reader["CustomerNo"].ToString();
                    member.Mobile = reader["Mobile"].ToString();

                    memberList.Add(member);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return memberList;
        }

    }
}
