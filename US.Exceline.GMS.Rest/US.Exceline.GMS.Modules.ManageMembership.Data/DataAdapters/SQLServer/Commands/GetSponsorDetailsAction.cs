﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetSponsorDetailsAction : USDBActionBase<SponsoredMemberDC>
    {

        private int _memberId = -1;

        public GetSponsorDetailsAction(int memberId)
        {
            _memberId = memberId;
        }
        protected override SponsoredMemberDC Body(DbConnection connection)
        {
            SponsoredMemberDC sponsoredMember = new SponsoredMemberDC();
            const string storedProcedureName = "USExceGMSManageMembershipGetCurrentSponsorDetails";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["Id"] != DBNull.Value)
                        sponsoredMember.Id = Convert.ToInt32(reader["Id"]);
                    if (reader["SponsorId"] != DBNull.Value)
                        sponsoredMember.SponsorId = Convert.ToInt32(reader["SponsorId"]);
                    if (reader["StartDate"] != DBNull.Value)
                        sponsoredMember.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    if (reader["EndDate"] != DBNull.Value)
                        sponsoredMember.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    if (reader["LevelId"] != DBNull.Value)
                        sponsoredMember.SponsoredLevelId = Convert.ToInt32(reader["LevelId"]);
                    if (reader["LevelName"] != DBNull.Value)
                        sponsoredMember.SponsoredLevelName = Convert.ToString(reader["LevelName"]);
                    if (reader["SposorNo"] != DBNull.Value)
                        sponsoredMember.SponsorNo = Convert.ToInt32(reader["SposorNo"]);
                    if (reader["SponsorName"] != DBNull.Value)
                        sponsoredMember.SponsorName = Convert.ToString(reader["SponsorName"]);
                    if (reader["SponsorRoleId"] != DBNull.Value)
                        sponsoredMember.Role = Convert.ToString(reader["SponsorRoleId"]);
                    if (reader["SponsorBranchId"] != DBNull.Value)
                        sponsoredMember.SponsorBranchId = Convert.ToInt32(reader["SponsorBranchId"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sponsoredMember;
        }
    }
}
