﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    class ChangePrePaidAccountAction: USDBActionBase<double>
    {
        private string _increase = string.Empty;
        private double _amount;
        private string _user = string.Empty;
        private int _memberId;
        private int _branchId;

        public ChangePrePaidAccountAction(string increase, double amount, int branchId, string user, int memberId)
        {
            _increase = increase;
            _amount = amount;
            _user = user;
            _memberId = memberId;
            _branchId = branchId;
        }

        protected override double Body(DbConnection connection)
        {
            string StoredProcedureName = "USExcelineGMSUpdatePrePaidAccountBalance";
            double result;

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                if (_increase == "0")
                {
                    _amount = _amount * (-1);
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _amount));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, "DEB"));
                } else if (_increase == "1")
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _amount));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, "CRE"));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@NewBalance";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();

                result = Convert.ToDouble(output.Value);
 
            }

            catch
            {
                throw;
            }
            return result;
        }
    }
}
