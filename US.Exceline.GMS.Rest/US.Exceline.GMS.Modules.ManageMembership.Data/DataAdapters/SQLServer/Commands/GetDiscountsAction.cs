﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/7/2012 4:19:11 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Admin;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetDiscountsAction : USDBActionBase<List<DiscountDC>>
    {
        private int _branchId = 0;
        private int _discountIntType = -1;
        private DiscountType _discountType = DiscountType.NONE;

        public GetDiscountsAction(int branchId, DiscountType discountType)
        {
            _branchId = branchId;
            _discountType = discountType;
            if (discountType == DiscountType.ACTIVITY)
            {
                _discountIntType = 0;
            }
            if (discountType == DiscountType.GROUP)
            {
                _discountIntType = 1;
            }
            else if (discountType == DiscountType.SHOP)
            {
                _discountIntType = 2;
            }
        }

        protected override List<DiscountDC> Body(DbConnection connection)
        {
            List<DiscountDC> _discountCategoryList = new List<DiscountDC>();

            if (_discountIntType != 2)
            {
                string StoredProcedureName = "USExceGMSManageMembershipGetDiscounts";

                try
                {
                    DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@discountType", DbType.Int32, _discountIntType));

                    DbDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DiscountDC _discountcategory = new DiscountDC();
                        _discountcategory.DiscountType = _discountType;
                        _discountcategory.Name = Convert.ToString(reader["Name"]);
                        _discountcategory.Id = Convert.ToInt32(reader["ID"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(reader["TypeId"]).Trim()))
                        {
                            _discountcategory.TypeId = Convert.ToInt32(reader["TypeId"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(reader["ModeId"]).Trim()))
                        {
                            _discountcategory.ModeId = Convert.ToInt32(reader["ModeId"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(reader["MinVisits"]).Trim()))
                        {
                            _discountcategory.MinVisitsNo = Convert.ToInt32(reader["MinVisits"]);
                        }
                        _discountcategory.TypeName = Convert.ToString(reader["TypeName"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(reader["MinMembersCount"]).Trim()))
                        {
                            _discountcategory.MinMemberNo = Convert.ToInt32(reader["MinMembersCount"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(reader["Discount"]).Trim()))
                        {
                            if (_discountcategory.DiscountType == DiscountType.ACTIVITY)
                            {
                                _discountcategory.DiscountPercentage = Convert.ToDecimal(reader["Discount"]);
                                _discountcategory.ActivityName = Convert.ToString(reader["ActivityName"]);
                                _discountcategory.ActivityDiscountName = Convert.ToString(reader["Name"]);
                            }
                            else if (_discountcategory.DiscountType == DiscountType.GROUP)
                            {
                                if (Convert.ToString(reader["LowMode"]) == "FIXEDAMOUNT")
                                {
                                    _discountcategory.LowAmount = Convert.ToDecimal(reader["LowDiscount"]);
                                    _discountcategory.IsLowAmoutEnable = true;
                                }
                                else //if (Convert.ToString(reader["LowMode"]) == "PERCENTATGE")
                                {
                                    _discountcategory.LowPercentage = Convert.ToDecimal(reader["LowDiscount"]);
                                    _discountcategory.IsLowPercentageEnable = true;
                                }
                                if (Convert.ToString(reader["HighMode"]) == "PERCENTATGE")
                                {
                                    _discountcategory.HighPercentage = Convert.ToDecimal(reader["HighDiscout"]);
                                    _discountcategory.IsHighPercentageEnable = true;
                                }
                                else //if (Convert.ToString(reader["HighMode"]) == "FIXEDAMOUNT")
                                {
                                    _discountcategory.HighAmount = Convert.ToDecimal(reader["HighDiscout"]);
                                    _discountcategory.IsHighAmoutEnable = true;
                                }
                                _discountcategory.PercentageModeId = Convert.ToInt32(reader["PecentageModeId"]);
                                _discountcategory.AmountModeId = Convert.ToInt32(reader["AmountModeId"]);
                                _discountcategory.DiscountAmount = Convert.ToDecimal(reader["Discount"]);
                                _discountcategory.GroupDiscountName = Convert.ToString(reader["Name"]);
                            }
                            else
                            {
                                _discountcategory.DiscountAmount = Convert.ToDecimal(reader["Discount"]);
                                _discountcategory.GroupDiscountName = Convert.ToString(reader["Name"]);
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(reader["MinPayment"]).Trim()))
                        {
                            _discountcategory.MinPayment = Convert.ToDecimal(reader["MinPayment"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(reader["ActivityId"]).Trim()))
                        {
                            _discountcategory.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(reader["TrailTime"]).Trim()))
                        {
                            _discountcategory.TrailTime = Convert.ToDateTime(reader["TrailTime"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(reader["IsActive"]).Trim()))
                        {
                            _discountcategory.IsActive = Convert.ToBoolean(reader["IsActive"]);
                        }
                        _discountCategoryList.Add(_discountcategory);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else if (_discountIntType == 2)
            {
                string storedProcedure = "USExceGMSShopGetDiscounts";
                string storedProcedure2 = "USExceGMSAdminGetDiscountsItems";

                try
                {
                    DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                    DbDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        DiscountDC discount = new DiscountDC();
                        discount.Id = Convert.ToInt32(reader["ID"].ToString());
                        discount.ArticleId = Convert.ToInt32(reader["ArticleID"].ToString());
                        discount.Name = reader["Name"].ToString();
                        discount.IsActive = Convert.ToBoolean(reader["IsActive"]);
                        discount.Description = reader["Description"].ToString();
                        discount.CategoryName = reader["CategoryName"].ToString();
                        discount.CategoryCode = reader["CategoryCode"].ToString();
                        discount.DiscountAmount = Convert.ToDecimal(reader["Discount"].ToString());
                        discount.Discount = Convert.ToDouble(reader["Discount"].ToString());
                        discount.DiscountPercentage = Convert.ToDecimal(reader["Discount"].ToString());
                        discount.DiscountString = discount.DiscountAmount.ToString() + " " + "%";
                        discount.StartDate = Convert.ToDateTime(reader["StartDate"].ToString());
                        discount.EndDate = Convert.ToDateTime(reader["EndDate"].ToString());
                        discount.NumberOfFreeItems = Convert.ToInt32(reader["NoOfFreeItems"].ToString());
                        discount.MinNumberOfArticles = Convert.ToInt32(reader["MinNoOfArticles"].ToString());
                        if (discount.MinNumberOfArticles > 0)
                        {
                            discount.MinNoContent = discount.MinNumberOfArticles.ToString();
                        }
                        else
                        {
                            discount.MinNoContent = "";
                        }
                        if (!(discount.MinNumberOfArticles <= 0 && discount.NumberOfFreeItems <= 0))
                        {
                            discount.CampaignText = discount.MinNumberOfArticles.ToString() + "  for  " + discount.NumberOfFreeItems.ToString();
                        }
                        discount.VendorId = Convert.ToInt32(reader["VendorId"].ToString());
                        discount.VendorName = reader["VendorName"].ToString();
                        discount.TypeId = Convert.ToInt32(reader["DiscountTypeId"].ToString());
                        discount.ArticleCategoryId = Convert.ToInt32(reader["ArticleCategoryId"].ToString());
                        discount.DiscountType = DiscountType.SHOP;
                        _discountCategoryList.Add(discount);
                    }

                    reader.Close();
                    foreach (DiscountDC discount in _discountCategoryList)
                    {
                        if (discount.Id != 0)
                        {
                            DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure2);
                            cmd2.Parameters.Add(DataAcessUtils.CreateParam("@disId", DbType.Int32, discount.Id));
                            cmd2.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                            DbDataReader reader2 = cmd2.ExecuteReader();

                            while (reader2.Read())
                            {
                                ArticleDC article = new ArticleDC();
                                int itemId;
                                string itemName;

                                itemId = Convert.ToInt32(reader2["ItemId"].ToString());
                                article.Id = itemId;
                                itemName = reader2["ItemName"].ToString();
                                article.Description = itemName;
                                article.Code = reader2["ItemCode"].ToString();
                                article.Discount = Convert.ToDecimal(reader2["Discount"].ToString());
                                article.Code = reader2["ItemCode"].ToString();
                                discount.ItemList.Add(itemId);
                                discount.ItemNames.Add(itemName);
                                discount.ArticleList.Add(article);
                            }
                            reader2.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return _discountCategoryList;
        }
    }
}
