﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateMemberInstallment : USDBActionBase<bool>
    {
        private InstallmentDC _installment;

        public UpdateMemberInstallment(InstallmentDC installment)
        {
            _installment = installment;
        }

        protected override bool Body(DbConnection connection)
        {
            bool _isUpdated = false;
            string StoredProcedureName = "USExceGMSUpdateInstallment";

            try
            {
                DataTable adonData = GetDataTable(_installment.AddOnList, _installment.Id);
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@installmentId", DbType.Int32, _installment.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@installmentDate", DbType.DateTime, _installment.InstallmentDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@dueDate", DbType.DateTime, _installment.DueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _installment.Amount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@balance", DbType.Decimal, _installment.Balance));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@printedDate", DbType.DateTime, _installment.PrintedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@addOnPrice", DbType.Decimal, _installment.AdonPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@discount", DbType.Decimal, _installment.Discount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@group", DbType.String, _installment.Group));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponsoredAmount", DbType.Decimal, _installment.SponsoredAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@priceListItem", DbType.String, _installment.PriceListItem));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@originalDueDate", DbType.DateTime, _installment.OriginalDueDate));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isATG", DbType.Boolean, _installment.IsATG));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Debit", DbType.Boolean, _installment.IsDebit));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DeleteRequest", DbType.Boolean, _installment.IsDeleteRequest));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@treat", DbType.Boolean, _installment.IsTreat));

                if (_installment.PayerId != -1)
                {
                    if (_installment.MemberId != _installment.PayerId)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@otherPay", DbType.Boolean, true));
                    else
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@otherPay", DbType.Boolean, false));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@otherPay", DbType.Boolean, false));
                }

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@trainingStartDate", DbType.DateTime, _installment.TrainingPeriodStart));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@trainingEndDate", DbType.DateTime, _installment.TrainingPeriodEnd));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityPrice", DbType.Decimal, _installment.ActivityPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, _installment.Comment));
                if (_installment.PayerId != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@payerID", DbType.Int32, _installment.PayerId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isInvoiceFeeAdded", DbType.Boolean, _installment.IsInvoiceFeeAdded));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AdonText", DbType.String, _installment.AdonText));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@serviceAmount", DbType.Decimal, _installment.ServiceAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@itemAmount", DbType.Decimal, _installment.ItemAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@adonsList", SqlDbType.Structured, adonData));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Text", DbType.String, _installment.Text));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@estimatedinvoicedDate", DbType.DateTime, _installment.EstimatedInvoiceDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _installment.CreatedUser));
                cmd.ExecuteNonQuery();
                _isUpdated = true;
            }
            catch (Exception)
            {
                throw;
            }
            return _isUpdated;
        }

        private DataTable GetDataTable(List<ContractItemDC> adonsList, int installmentID)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("InstallmentID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Text", typeof(string)));
            dataTable.Columns.Add(new DataColumn("ArticleNo", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Price", typeof(decimal)));
            dataTable.Columns.Add(new DataColumn("IsStartUpItem", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Quantity", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Discount", typeof(int)));
            dataTable.Columns.Add(new DataColumn("IsActivityArticle", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Description", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Priority", typeof(int)));
            dataTable.Columns.Add(new DataColumn("VoucherNo", typeof(string)));
            dataTable.Columns.Add(new DataColumn("VoucherExpireDate", typeof(string)));

            foreach (ContractItemDC adon in adonsList)
            {
                if (string.IsNullOrEmpty(adon.Description))
                {
                    adon.Description = string.Empty;
                }

                if (adon.ArticleId > 0)
                {
                    DataRow dataTableRow = dataTable.NewRow();
                    dataTableRow["InstallmentID"] = installmentID;
                    dataTableRow["Text"] = adon.ItemName;
                    dataTableRow["ArticleNo"] = adon.ArticleId;
                    dataTableRow["Price"] = adon.Price;
                    dataTableRow["IsStartUpItem"] = adon.IsStartUpItem;
                    dataTableRow["Quantity"] = adon.Quantity;
                    dataTableRow["Discount"] = adon.Discount;
                    dataTableRow["IsActivityArticle"] = adon.IsActivityArticle;
                    dataTableRow["Description"] = adon.Description;
                    dataTableRow["Priority"] = adon.Priority;
                    dataTableRow["VoucherNo"] = adon.VoucherNo;
                    dataTableRow["VoucherExpireDate"] = adon.ExpiryDate;
                    dataTable.Rows.Add(dataTableRow);
                }
                else
                {

                }
            }
            return dataTable;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            bool _isUpdated = false;
            string StoredProcedureName = "USExceGMSUpdateInstallment";
            DbTransaction tran = transaction;
            try
            {
                DataTable adonData = GetDataTable(_installment.AddOnList, _installment.Id);

                DbCommand cmd = new SqlCommand();
                cmd.Connection = tran.Connection;
                cmd.Transaction = tran;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = StoredProcedureName;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@installmentId", DbType.Int32, _installment.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@installmentDate", DbType.DateTime, _installment.InstallmentDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@dueDate", DbType.DateTime, _installment.DueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _installment.Amount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@balance", DbType.Decimal, _installment.Balance));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@printedDate", DbType.DateTime, _installment.PrintedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@addOnPrice", DbType.Decimal, _installment.AdonPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@discount", DbType.Decimal, _installment.Discount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@group", DbType.String, _installment.Group));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponsoredAmount", DbType.Decimal, _installment.SponsoredAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@priceListItem", DbType.String, _installment.PriceListItem));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@originalDueDate", DbType.DateTime, _installment.OriginalDueDate));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isATG", DbType.Boolean, _installment.IsATG));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Debit", DbType.Boolean, _installment.IsDebit));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DeleteRequest", DbType.Boolean, _installment.IsDeleteRequest));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@treat", DbType.Boolean, _installment.IsTreat));

                if (_installment.PayerId != -1)
                {
                    if (_installment.MemberId != _installment.PayerId)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@otherPay", DbType.Boolean, true));
                    else
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@otherPay", DbType.Boolean, false));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@otherPay", DbType.Boolean, false));
                }

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@invoiceGeneratedDate", DbType.DateTime, _installment.InvoiceGeneratedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@trainingStartDate", DbType.DateTime, _installment.TrainingPeriodStart));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@trainingEndDate", DbType.DateTime, _installment.TrainingPeriodEnd));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityPrice", DbType.Decimal, _installment.ActivityPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, _installment.Comment));
                if (_installment.PayerId != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@payerID", DbType.Int32, _installment.PayerId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isInvoiceFeeAdded", DbType.Boolean, _installment.IsInvoiceFeeAdded));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AdonText", DbType.String, _installment.AdonText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@serviceAmount", DbType.Decimal, _installment.ServiceAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@itemAmount", DbType.Decimal, _installment.ItemAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@adonsList", SqlDbType.Structured, adonData));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Text", DbType.String, _installment.Text));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@estimatedinvoicedDate", DbType.DateTime, _installment.EstimatedInvoiceDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _installment.CreatedUser));
                cmd.ExecuteNonQuery();
                _isUpdated = true;
            }
            catch (Exception)
            {
                throw;
            }
            return _isUpdated;
        }
    }
}
