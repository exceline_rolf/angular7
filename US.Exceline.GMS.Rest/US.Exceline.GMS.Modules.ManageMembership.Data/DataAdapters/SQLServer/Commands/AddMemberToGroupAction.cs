﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/28/2012 3:38:49 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class AddMemberToGroupAction : USDBActionBase<bool>
    {
        private int _memberId = -1;
        private int _groupId = -1;


        public AddMemberToGroupAction(int memberId, int groupId, string user)
        {
            _memberId = memberId;
            _groupId = groupId;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSManageMembershipAddMemberToGroup";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberID", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@groupID", DbType.Int32, _groupId));
                cmd.ExecuteNonQuery();
                result = true;
            }
            catch 
            {
                throw;
            }
            return result;
        }
    }
}
