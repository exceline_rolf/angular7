﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : "4/25/2012 13:35:25
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    class GetMemberContractDetailsAction : USDBActionBase<MemberContractDC>
    {
        private int _contractId = -1;
        private string _gymCode = string.Empty;
             
        public GetMemberContractDetailsAction(int contractId, int branchId,string gymCode)
        {
            _contractId = contractId;
            _gymCode = gymCode;
        }

        protected override MemberContractDC Body(System.Data.Common.DbConnection connection)
        {
            string storedProcedureName = "USExceGMSManageMembershipGetMemberContractDetails";
            DbDataReader reader = null;
            MemberContractDC memberContract = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ContractId", System.Data.DbType.Int32, _contractId));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    memberContract = new MemberContractDC();
                    memberContract.Id = Convert.ToInt32(reader["ID"]);
                    memberContract.MemberId = Convert.ToInt32(reader["MemberId"]);
                    memberContract.MemberContractNo = Convert.ToString(reader["ContractNo"]);
                    if (reader["MemberName"] != DBNull.Value)
                        memberContract.MemberName = reader["MemberName"].ToString();
                    if (reader["MemberCustId"] != DBNull.Value)
                        memberContract.MemberCustId = reader["MemberCustId"].ToString();
                    if (reader["ContractId"] != DBNull.Value)
                        memberContract.ContractId = Convert.ToInt32(reader["ContractId"]);
                    if (reader["EnrollmentFee"] != DBNull.Value)
                        memberContract.EnrollmentFee = Convert.ToDecimal(reader["EnrollmentFee"]);
                    if (reader["AmountPerInstallment"] != DBNull.Value)
                        memberContract.AmountPerInstallment = Convert.ToDecimal(reader["AmountPerInstallment"]);
                    if (reader["RemainingBalance"] != DBNull.Value)
                        memberContract.RemainingBalance = Convert.ToDecimal(reader["RemainingBalance"]);
                    if (reader["AccountNo"] != DBNull.Value)
                        memberContract.AccountNo = Convert.ToString(reader["AccountNo"]);
                    if (reader["NumberofInstallments"] != DBNull.Value)
                        memberContract.NoOfInstallments = Convert.ToInt32(reader["NumberofInstallments"]);
                    if (reader["NumberOfVisits"] != DBNull.Value)
                        memberContract.NumberOfVisits = Convert.ToInt32(reader["NumberOfVisits"]);
                    if (reader["RecordedVisits"] != DBNull.Value)
                        memberContract.RecordedVisits = Convert.ToInt32(reader["RecordedVisits"]);    
                    if (reader["AvailableVisits"] != DBNull.Value)
                        memberContract.AvailableVisits = Convert.ToInt32(reader["AvailableVisits"]);
                    if (reader["ContractStartDate"] != DBNull.Value)
                        memberContract.ContractStartDate = Convert.ToDateTime(reader["ContractStartDate"]);
                    if (reader["ContractEndDate"] != DBNull.Value)
                        memberContract.ContractEndDate = Convert.ToDateTime(reader["ContractEndDate"]);

                    if (reader["RenewEffectiveDate"] != DBNull.Value)
                        memberContract.RenewEffectiveDate = Convert.ToDateTime(reader["RenewEffectiveDate"]);
                        
                    if (reader["TrainerId"] != DBNull.Value)
                        memberContract.TrainerId = Convert.ToInt32(reader["TrainerId"]);
                    if (reader["CreatedUser"] != DBNull.Value)
                        memberContract.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    if (reader["BranchId"] != DBNull.Value)
                        memberContract.BranchId = Convert.ToInt32(reader["BranchId"]);
                    if (reader["CreatedDate"] != DBNull.Value)
                        memberContract.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                    if (reader["IsDeductFromBank"] != DBNull.Value)
                        memberContract.IsDeductFromBank = Convert.ToBoolean(reader["IsDeductFromBank"]);
                    if (reader["IsFreezed"] != DBNull.Value)
                        memberContract.IsFreezed = Convert.ToBoolean(reader["IsFreezed"]);
                    if (reader["FreezedBy"] != DBNull.Value)
                        memberContract.FreezedBy = Convert.ToString(reader["FreezedBy"]);
                    if (reader["EmployeeRef"] != DBNull.Value)
                        memberContract.EmployeeReference = Convert.ToString(reader["EmployeeRef"]);
                    if (reader["EmployeeNo"] != DBNull.Value)
                        memberContract.EmployeeNo = Convert.ToString(reader["EmployeeNo"]);
                    if (reader["RenewedDate"] != DBNull.Value)
                        memberContract.RenewedDate = Convert.ToDateTime(reader["RenewedDate"]);
                    if (reader["FreezeCategoryId"] != DBNull.Value)
                        memberContract.FreezedCategoryId = Convert.ToInt32(reader["FreezeCategoryId"]);
                    if (reader["ActivityId"] != DBNull.Value)
                        memberContract.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    if (reader["ActivityName"] != DBNull.Value)
                        memberContract.ActivityName = Convert.ToString(reader["ActivityName"]);
                    if (reader["PackageType"] != DBNull.Value)
                        memberContract.ContractTypeName = Convert.ToString(reader["PackageType"]);
                    if (reader["PackageTypeId"] != DBNull.Value)
                        memberContract.ContractTypeId = Convert.ToInt32(reader["PackageTypeId"]);
                    if (reader["IsATG"] != DBNull.Value)
                        memberContract.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    if (reader["ContractDescription"] != DBNull.Value)
                        memberContract.ContractDescription = Convert.ToString(reader["ContractDescription"]);
                    if (reader["PackagePrice"] != DBNull.Value)
                        memberContract.ContractPrize = Convert.ToDecimal(reader["PackagePrice"]);
                    if (reader["Package"] != DBNull.Value)
                        memberContract.ContractName = Convert.ToString(reader["Package"]);
                    if (reader["BalanceTopay"] != DBNull.Value)
                        memberContract.RemainingBalance = Convert.ToDecimal(reader["BalanceTopay"]);
                    if (reader["ExtraActivityId"] != DBNull.Value)
                        memberContract.ExtracActivityId = Convert.ToInt32(reader["ExtraActivityId"]);
                    if (reader["BranchName"] != DBNull.Value)
                        memberContract.BranchName = reader["BranchName"].ToString();
                    
                    if (reader["SettingID"] != DBNull.Value)
                        memberContract.SettingId = Convert.ToInt32(reader["SettingID"]);
                    if (reader["SposerID"] != DBNull.Value)
                        memberContract.SponserId = Convert.ToInt32(reader["SposerID"]);
                    if (reader["EmployeeCategoryId"] != DBNull.Value)
                        memberContract.EmployeeCategoryId = Convert.ToInt32(reader["EmployeeCategoryId"]);
                    if (reader["SponserContractId"] != DBNull.Value)
                        memberContract.SponserContractId = Convert.ToInt32(reader["SponserContractId"]);
                    if (reader["ClosedDate"] != DBNull.Value)
                    {
                        memberContract.Status = "Closed";
                        memberContract.IsActive = false;
                    }
                    else
                    {
                        memberContract.IsActive = true;
                    }
                    memberContract.LockInPeriod = Convert.ToInt32(reader["LockInperiod"]);
                    memberContract.GuarantyPeriod = Convert.ToInt32(reader["GuarantyPeriod"]);
                    memberContract.IsInvoiceDetails = Convert.ToBoolean(reader["InvoiceDetails"]);
                    memberContract.AutoRenew = Convert.ToBoolean(reader["AutoRenew"]);
                    memberContract.ArticleId = Convert.ToInt32(reader["ArticleId"]);
                    memberContract.ArticleName = Convert.ToString(reader["ArticleText"]);
                    memberContract.RestPlusMonth = Convert.ToBoolean(reader["RestPlusMonth"]);
                    memberContract.AccessProfileId = Convert.ToInt32(reader["AccessProfileId"]);
                    memberContract.NoOfMonths = Convert.ToInt32(reader["NoOfMonths"]);

                    if(reader["FixDateOfContract"] != DBNull.Value)
                     memberContract.FixDateOfContract = Convert.ToDateTime(reader["FixDateOfContract"]);

                    if (reader["LockInPeriodEndDate"] != DBNull.Value)
                        memberContract.LockInPeriodUntilDate = Convert.ToDateTime(reader["LockInPeriodEndDate"]);
                    memberContract.OrderPrice = Convert.ToDecimal(reader["OrderPrice"]);
                    memberContract.StartUpItemPrice = Convert.ToDecimal(reader["StartUpItemPrice"]);
                    memberContract.EveryMonthItemPrice = Convert.ToDecimal(reader["EveryMonthItemPrice"]);
                    if (reader["PriceGuarantyUntillDate"] != DBNull.Value)
                        memberContract.PriceGuarantyUntillDate = Convert.ToDateTime(reader["PriceGuarantyUntillDate"]);
                    memberContract.TemplateNo = Convert.ToString(reader["TemplateNo"]);
                    memberContract.CategoryId = Convert.ToInt32(reader["CategoryId"]);
                    memberContract.ContractCategoryName = Convert.ToString(reader["CategoryName"]);
                    //memberContract.Status = Convert.ToString(reader["Status"]);

                    if (reader["FirstDueDate"] != DBNull.Value)
                        memberContract.FirstDueDate = Convert.ToDateTime(reader["FirstDueDate"]);
                    if (reader["SecondDueDate"] != DBNull.Value)
                        memberContract.SecondDueDate = Convert.ToDateTime(reader["SecondDueDate"]);
                    if (reader["CreditDueDays"] != DBNull.Value)
                        memberContract.CreditoDueDays = Convert.ToInt32(reader["CreditDueDays"]);
                    memberContract.TemplateName = Convert.ToString(reader["TemplateName"]);
                    memberContract.NextTemplateNo = Convert.ToString(reader["NextTemplateNo"]);
                    memberContract.NextTemplateName = Convert.ToString(reader["NextTemplateName"]);
                    if(reader["NextTemplateId"] != DBNull.Value)
                       memberContract.NextTemplateId = Convert.ToInt32(reader["NextTemplateId"]);
                    memberContract.ContractTypeCode = Convert.ToString(reader["ContractTypeCode"]);
                    memberContract.GroupContractMemberCount = Convert.ToInt32(reader["GorupMemberCount"]);
                    memberContract.IsMemberContract = Convert.ToBoolean(reader["IsMemberContract"]);
                    memberContract.SignUpCategoryId = Convert.ToInt32(reader["SignUpCategoryId"]);
                    memberContract.NoOfMonths = GetMonthDifference(memberContract.ContractStartDate, memberContract.ContractEndDate);
                    memberContract.ContractPDFFillParth = Convert.ToString(reader["ContractPDFFillParth"]);
                    memberContract.ATGPDFFillParth = Convert.ToString(reader["ATGPDFFillParth"]);
                    memberContract.RestPlusMonthAmount = Convert.ToDecimal(reader["RestPlueMonthAmount"]);
                    memberContract.NextTemplateIdOfTemplate = Convert.ToInt32(reader["NextTemplateIDofTemplate"]);
                    if (reader["ClosedDate"] != DBNull.Value)
                    {
                        memberContract.ClosedDate = Convert.ToDateTime(reader["ClosedDate"]);
                    }
                    memberContract.ServiceAmount = Convert.ToDecimal(reader["ServiceAmount"]);
                    memberContract.FixedDueDay = Convert.ToInt32(reader["DueDay"]);


                    memberContract.MemberFeeArticleID = Convert.ToInt32(reader["MemberFeeArticleID"]);
                    memberContract.MemberFeeArticleName = Convert.ToString(reader["MemberFeeArticleName"]);

                    if (reader["MemberFeeMonth"] != DBNull.Value)
                    {
                        memberContract.MemberFeeMonth = Convert.ToInt32(reader["MemberFeeMonth"]);
                    }
                     if (reader["MemberFeeLastInvoicedDate"] != DBNull.Value)
                    {
                        memberContract.MemFeeLastInvoicedDate = Convert.ToDateTime(reader["MemberFeeLastInvoicedDate"]);
                    }
                     memberContract.IsGroup = Convert.ToBoolean(reader["IsGroup"]);
                     memberContract.ResignCategory = Convert.ToString(reader["ResignCategory"]);
                     memberContract.MemberCreditPeriod = Convert.ToInt32(reader["MemberCreditPeriod"]);
                     memberContract.CreditPeriod = Convert.ToInt32(reader["CreditPeriod"]);
                     memberContract.ContractConditionId = Convert.ToInt32(reader["ContractConditionId"]);
                     memberContract.ContractCondition = Convert.ToString(reader["ContractCondition"]);
                }
                reader.Close();

                GetMemberContractItemsAction itemAction = new GetMemberContractItemsAction(memberContract.Id, memberContract.ArticleId);
                List<ContractItemDC> items = itemAction.Execute(EnumDatabase.Exceline, _gymCode);
                if (items.Count > 0)
                {
                    memberContract.EveryMonthItemList = items.Where(x => x.IsStartUpItem == false).ToList<ContractItemDC>();
                    memberContract.StartUpItemList = items.Where(x => x.IsStartUpItem == true).ToList<ContractItemDC>();
                }

                GetContractMembersAction groupMemberAction = new GetContractMembersAction(memberContract.Id);
                memberContract.GroupMembers = groupMemberAction.Execute(EnumDatabase.Exceline, _gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            return memberContract;
        }

        private static int GetMonthDifference(DateTime startDate,DateTime endDate)
        {
            // if dates were passed in wrong order, swap 'em
            if (startDate > endDate)
            {
                var temp = startDate;
                startDate = endDate;
                endDate = temp;
            }

            var count = 0;
            var tempDate = startDate;

            while ((tempDate = GetNextMonth(tempDate)) <= endDate)
            {
                count++;
            }

            Console.WriteLine("From {0} to {1} is {2} month{3}.",
              startDate.ToShortDateString(), endDate.ToShortDateString(),
              count, count == 1 ? "" : "s");

            return count;
        }

        private static DateTime GetNextMonth(DateTime date)
        {
            var month = date.Month;
            var day = date.Day;
            var year = date.Year;

            var nextDateMonth = month == 12 ? 1 : month + 1;
            var nextDateYear = month == 12 ? year + 1 : year;

            DateTime nextDate;

            while (!DateTime.TryParse(nextDateMonth + "/" + day
              + "/" + nextDateYear, out nextDate))
            {
                // if it didn't parse right, 
                // then the month must not have that many days
                day--;
            }

            return nextDate;
        }
    }
}
