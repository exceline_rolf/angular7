﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    class DeleteDocumentDataAction : USDBActionBase<bool>
    {
        private int _docId = -1;
        public DeleteDocumentDataAction(int docId)
        {
            _docId=docId;
        }
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSMemberDeleteDocumentData";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FileId", DbType.Int32, _docId));
                cmd.ExecuteNonQuery();

                result = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
