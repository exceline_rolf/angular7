﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using US_DataAccess;
using US.GMS.Core.SystemObjects;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetClassesForMemberAction : USDBActionBase<List<ClassDetailDC>>
    {
        private readonly int _memberId;
        private readonly int _branchId;
        private readonly DateTime? _classDate;
        private readonly string _user = string.Empty;

        public GetClassesForMemberAction(int branchId, int memberId, DateTime? classdate, string user)
        {
            _memberId = memberId;
            _branchId = branchId;
            _classDate = classdate;
            _user = user;
        }

        protected override List<ClassDetailDC> Body(DbConnection connection)
        {
            var classList = new List<ClassDetailDC>();
            const string storedProcedureName = "USExceGMSAdminGetClassForMember";
            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                if(_classDate.HasValue)
                    command.Parameters.Add(DataAcessUtils.CreateParam("@ClassDate", DbType.DateTime, _classDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _user));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var classItem = new ClassDetailDC();
                    classItem.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    classItem.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    classItem.ClassType = Convert.ToString(reader["ClassType"]).Trim();
                    classItem.ClassGroup = Convert.ToString(reader["ClassGroup"]).Trim();
                    classItem.ActiveTimeId = Convert.ToInt32(reader["ActiveTimeId"]);
                    classItem.VisitTime = Convert.ToDateTime(reader["VisitTime"]);
                    classItem.GymName = Convert.ToString(reader["GymName"]).Trim();
                    classItem.ClassLevelId = Convert.ToInt32(reader["ClassLevelId"]);
                    if (reader["ClassKeywords"] != DBNull.Value)
                    {
                        classItem.ClassKeywordList = reader["ClassKeywords"].ToString().Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    }
                    classItem.Day = Convert.ToString(reader["Day"]).Trim();
                    classItem.Location = Convert.ToString(reader["Location"]).Trim();
                    classList.Add(classItem);
                }
                reader.Close();
                foreach (var item in classList)
                {
                    string instructors = "";
                    item.InstructorList = GetInstructorByActiveTime(item.ActiveTimeId, out instructors);
                    if (item.InstructorList != null && item.InstructorList.ToList().Any())
                    {
                        item.InstructorCount = item.InstructorList.Count();
                        item.IsHasInstructor = true;
                    }
                    item.InstructorStrn = instructors;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return classList;
        }

        private Dictionary<int, string> GetInstructorByActiveTime(int activeTimeId, out string instrctorStrn)
        {
            const string storedProcedureName = "USExceGMSAdminGetInstructorsByActiveTimeId";
            try
            {
                instrctorStrn = "";
                var instructorList = new Dictionary<int, string>();
                DbCommand command = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@activeTimeId", DbType.Int32, activeTimeId));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int id = Convert.ToInt32(reader["InstructorID"]);
                    string name = reader["InstructorName"].ToString().Trim();
                    instrctorStrn += " " + name;
                    if (!instructorList.ContainsKey(id))
                    {
                        instructorList.Add(id, name);
                    }
                }
                reader.Close();
                return instructorList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
