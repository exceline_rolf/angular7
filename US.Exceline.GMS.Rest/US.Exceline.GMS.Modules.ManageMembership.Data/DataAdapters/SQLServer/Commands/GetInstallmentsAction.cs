﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetInstallmentsAction : USDBActionBase<List<InstallmentDC>>
    {
        private int _memberContractId = -1;
        private string _gymCode = string.Empty;
        public GetInstallmentsAction(int memberContractId, string gymCode)
        {
            _memberContractId = memberContractId;
            _gymCode = gymCode;
        }

        protected override List<InstallmentDC> Body(System.Data.Common.DbConnection connection)
        {
            List<InstallmentDC> installmentsList = new List<InstallmentDC>();
            string spName = "USExceGMSManageMembershipGetInstallments";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", System.Data.DbType.Int32, _memberContractId));

                DbDataReader reader = command.ExecuteReader();
                int count = 1;
                while (reader.Read())
                {
                    InstallmentDC installment = new InstallmentDC();
                    installment.InstallmentId = count;
                    installment.Id = Convert.ToInt32(reader["ID"]);
                    installment.TableId = Convert.ToInt32(reader["ID"]);
                    installment.MemberId = Convert.ToInt32(reader["MemberId"]);
                    installment.MemberContractId = Convert.ToInt32(reader["MemberContractId"]);
                    installment.MemberContractNo = Convert.ToString(reader["MemberContractNo"]);
                    installment.Text = Convert.ToString(reader["Text"]);
                    installment.InstallmentDate = Convert.ToDateTime(reader["InstallmentDate"]);
                    installment.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    installment.Amount = Convert.ToDecimal(reader["Amount"]);
                    if (reader["AddOnPrice"] != DBNull.Value)
                        installment.AdonPrice = Convert.ToDecimal(reader["AddOnPrice"]);
                    if (reader["Discount"] != DBNull.Value)
                        installment.Discount = Convert.ToDecimal(reader["Discount"]);
                    if (reader["SponsoredAmount"] != DBNull.Value)
                        installment.SponsoredAmount = Convert.ToDecimal(reader["SponsoredAmount"]);
                    installment.Comment = reader["Comment"].ToString();
                    if (reader["OriginalMatuarity"] != DBNull.Value)
                        installment.OriginalDueDate = Convert.ToDateTime(reader["OriginalMatuarity"]);
                    installment.Balance = Convert.ToDecimal(reader["Balance"]);
                    if (reader["InvoiceGeneratedDate"] != DBNull.Value)
                        installment.InvoiceGeneratedDate = Convert.ToDateTime(reader["InvoiceGeneratedDate"]);
                    installment.IsInvoiced = Convert.ToBoolean(reader["InvoiceGenerated"]);
                    installment.InstallmentNo = Convert.ToInt32(reader["InstallmentNo"]);
                    if (reader["TSDate"] != DBNull.Value)
                        installment.TrainingPeriodStart = Convert.ToDateTime(reader["TSDate"]);
                    if (reader["TEDate"] != DBNull.Value)
                        installment.TrainingPeriodEnd = Convert.ToDateTime(reader["TEDate"]);
                    installment.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    installment.IsOtherPay = Convert.ToBoolean(reader["IsOtherPay"]);
                    installment.ContractName = Convert.ToString(reader["TemplateName"]);
                    installment.ActivityName = Convert.ToString(reader["ActivityName"]);
                    installment.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    installment.OriginalCustomer = Convert.ToString(reader["OriginalCustomer"]);
                    installment.EstimatedOrderAmount = Convert.ToDecimal(reader["EstimatedOrderAmount"]);
                    installment.OrderNo = Convert.ToString(reader["OrderNo"]);
                    installment.ActivityPrice = Convert.ToDecimal(reader["ActivityPrice"]);
                    if (reader["EstimatedInvoiceDate"] != DBNull.Value)
                        if (reader["EstimatedInvoiceDate"] != DBNull.Value)
                            installment.EstimatedInvoiceDate = Convert.ToDateTime(reader["EstimatedInvoiceDate"]);
                    installment.AdonText = Convert.ToString(reader["AdonText"]);
                    installment.PayerName = Convert.ToString(reader["PayerName"]);
                    if (reader["PayerID"] != DBNull.Value)
                        installment.PayerId = Convert.ToInt32(reader["PayerID"]);
                    installment.IsSpOrder = Convert.ToBoolean(reader["IsSpOrder"]);
                    installment.IsInvoiceFeeAdded = Convert.ToBoolean(reader["IsInvoiceFeeAdded"]);
                    installment.BranchName = Convert.ToString(reader["BranchName"]);
                    if (reader["NoOfVisits"] != DBNull.Value)
                        installment.NoOfVisits = Convert.ToInt32(reader["NoOfVisits"]);
                    installment.IsLocked = Convert.ToBoolean(reader["IsLocked"]);
                    installment.ServiceAmount = Convert.ToDecimal(reader["ServiceAmount"]);
                    installment.ItemAmount = Convert.ToDecimal(reader["ItemAmount"]);
                    installment.InstallmentType = Convert.ToString(reader["InstallmentType"]);
                    installment.SponsorName = Convert.ToString(reader["SponsorName"]);
                    installment.TemplateId = Convert.ToInt32(reader["TemplateId"]);
                    installment.MemberName = Convert.ToString(reader["MemberName"]);
                    installment.MinVisits = Convert.ToInt32(reader["MinVisits"]);
                    installment.MemberName = Convert.ToString(reader["MemberName"]);
                    installment.IsSponsored = Convert.ToBoolean(reader["IsSponsored"]);
                    installment.SponsorArticleID = Convert.ToInt32(reader["SponsorArtilceID"]);
                    installment.DiscountArticleID = Convert.ToInt32(reader["DiscountArtilceID"]);
                    installment.IsSponsored = Convert.ToBoolean(reader["IsSponsored"]);
                    installment.GymID = Convert.ToInt32(reader["GymID"]);
                    installment.CreditPeriod = Convert.ToInt32(reader["CreditPeriod"]);
                    installment.MemberCreditPeriod = Convert.ToInt32(reader["MemberCreditPeriod"]);
                    installmentsList.Add(installment);
                    count++;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }


            if (installmentsList.Count > 0)
            {
                foreach (var item in installmentsList)
                {
                    GetOrderShareAction adonAction = new GetOrderShareAction(item.Id);
                    item.AddOnList = adonAction.Execute(EnumDatabase.Exceline, _gymCode);

                    foreach (var addon in item.AddOnList)
                    {
                        if (item.IsSponsored)
                        {
                            if ((item.Discount >= 0 && addon.Price <= 0 && addon.ItemName == "Rabatt") || (addon.ArticleId == item.DiscountArticleID))
                            {
                                addon.Description = "Rabatt - " + item.BranchName;
                            }

                            if ((item.SponsoredAmount >= 0 && addon.Price <= 0 && addon.ItemName == "Sponset Beløp") || (addon.ArticleId == item.SponsorArticleID))
                            {
                                if (item.MinVisits > 0)
                                {
                                    addon.Description = "Sponset Beløp - " + item.SponsorName + " - " + addon.NoOfVisits.ToString() + " - besøk ";
                                }
                                else
                                {
                                    addon.Description = "Sponset Beløp - " + item.SponsorName;
                                }
                            }
                        }

                        if (addon.IsAddedFromShop)
                        {
                            item.HasAddonFromShop = true;
                        }
                    }

                    if (!item.IsSponsored)
                    {
                        if (item.Discount > 0 && (!item.AddOnList.Exists(x => x.ArticleId == item.DiscountArticleID)))
                        {

                            ContractItemDC discountAddon = new ContractItemDC();
                            discountAddon.Id = -1;
                            discountAddon.Price = item.Discount * -1;
                            discountAddon.UnitPrice = 0;
                            discountAddon.ArticleId = 0;
                            discountAddon.ItemName = "Rabatt";
                            discountAddon.IsStartUpItem = false;
                            discountAddon.Quantity = 1;
                            discountAddon.StockLevel = 0;
                            discountAddon.Discount = 0;
                            discountAddon.IsActivityArticle = false;
                            discountAddon.Description = "Rabatt - " + item.BranchName;
                            discountAddon.Priority = 0;
                            discountAddon.IsBookingArticle = false;
                            discountAddon.IsSponsored = false;
                            item.AddOnList.Add(discountAddon);
                        }

                        if (item.SponsoredAmount > 0 && (!item.AddOnList.Exists(x => x.ArticleId == item.SponsorArticleID)))
                        {

                            ContractItemDC sponsAddon = new ContractItemDC();
                            sponsAddon.Id = -1;
                            sponsAddon.Price = item.SponsoredAmount * -1;
                            sponsAddon.UnitPrice = 0;
                            sponsAddon.ArticleId = 0;
                            sponsAddon.ItemName = "Sponset Beløp";
                            sponsAddon.IsStartUpItem = false;
                            sponsAddon.Quantity = 1;
                            sponsAddon.StockLevel = 0;
                            sponsAddon.Discount = 0;
                            sponsAddon.IsActivityArticle = false;
                            if (item.MinVisits > 0)
                            {
                                sponsAddon.Description = "Sponset Beløp - " + item.SponsorName + " - " + item.NoOfVisits.ToString() + " - besøk ";
                            }

                            else
                            {
                                sponsAddon.Description = "Sponset Beløp - " + item.SponsorName;
                            }

                            sponsAddon.Priority = 0;
                            sponsAddon.IsBookingArticle = false;
                            sponsAddon.IsSponsored = false;
                            item.AddOnList.Add(sponsAddon);
                        }

                    }

                }

            }
            return installmentsList;
        }


    }
}
