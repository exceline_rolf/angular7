﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetSponsoredMemberListByTimePeriodAction : USDBActionBase<List<int>>
    {
        private DataTable _dtSponsorMembers;
        private int _sponsorId;
        public GetSponsoredMemberListByTimePeriodAction(int sponsorId,Dictionary<int,List<DateTime>> sponsorMembers)
        {
            _dtSponsorMembers = GetActiveTimesDataTable(sponsorMembers);
            _sponsorId = sponsorId;
        }

        private DataTable GetActiveTimesDataTable(Dictionary<int, List<DateTime>> sponsorMembers)
        {
            _dtSponsorMembers = new DataTable();
            _dtSponsorMembers.Columns.Add(new DataColumn("MemberId", typeof(int)));
            _dtSponsorMembers.Columns.Add(new DataColumn("StartDate", typeof(DateTime)));
            _dtSponsorMembers.Columns.Add(new DataColumn("EndDate", typeof(DateTime)));

            if (sponsorMembers != null && sponsorMembers.Any())
            {
                foreach (KeyValuePair<int, List<DateTime>> sMember in sponsorMembers)
                {
                    DataRow dr = _dtSponsorMembers.NewRow();
                    dr["MemberId"] = sMember.Key;
                    dr["StartDate"] = sMember.Value[0];
                    if(sMember.Value[1] == DateTime.MinValue)
                    {
                        dr["EndDate"] = DBNull.Value;
                    }
                    else
                    {
                      dr["EndDate"] = sMember.Value[1];
                    }
                    _dtSponsorMembers.Rows.Add(dr);
                }
            }
            return _dtSponsorMembers;
        }

        protected override List<int> Body(System.Data.Common.DbConnection connection)
        {
            List<int> sponsoredMemberList = new List<int>();
            int memberId;
            string storedProcedureName = "USExceGMSManageMembershipGetSponsoredMemberListByTimePeriod";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsorId", DbType.Int32, _sponsorId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsorList", SqlDbType.Structured, _dtSponsorMembers));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["MemberId"]).Trim()))
                    {
                        memberId = Convert.ToInt32(reader["MemberId"]);
                        sponsoredMemberList.Add(memberId);
                    }
                }
                return sponsoredMemberList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
