﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Common;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveMemberAction : USDBActionBase<string>
    {
        private OrdinaryMemberDC _memberDC = new OrdinaryMemberDC();
        private DataTable _dataTable = null;
        private string _user = string.Empty;

        public SaveMemberAction(OrdinaryMemberDC memberDc,string user)
        {
            _memberDC = memberDc;
            _user = user;
            if (_memberDC.InfoCategoryList != null)
                _dataTable = GetInfoCategoryItemLst(_memberDC.InfoCategoryList);

        }

        private DataTable GetInfoCategoryItemLst(List<CategoryDC> infoCategoryList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("CategoryId", Type.GetType("System.Int32")));


            foreach (CategoryDC infoCategotyItem in infoCategoryList)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["CategoryId"] = infoCategotyItem.Id;

                _dataTable.Rows.Add(_dataTableRow);
            }

            return _dataTable;
        }



        protected override string Body(DbConnection connection)
        {
            string memberId = "-1";
            const string storedProcedureName = "USExceGMSManageMembershipAddMember";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@firstname", DbType.String, _memberDC.FirstName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastname", DbType.String, _memberDC.LastName));
                if (_memberDC.Role.Equals(MemberRole.COM))
                {

                   // cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastname", DbType.String,  _memberDC.CompanyName));
                    if(!string.IsNullOrEmpty(_memberDC.AccountingEmail))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@accountingEmail", DbType.String, _memberDC.AccountingEmail));
                    if (_memberDC.ContactPersonId > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@contactPersonId", DbType.Int32, _memberDC.ContactPersonId));
                    //cmd.Parameters.Add(DataAcessUtils.CreateParam("@department", DbType.String, _memberDC.Department));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@birthdate", DbType.DateTime, null));
                    if(!string.IsNullOrEmpty(_memberDC.VatNumber))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@vatNumber", DbType.String, _memberDC.VatNumber));
                }
        
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entFromEMP", DbType.Int32, _memberDC.EntNo));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@empComapnyName", DbType.String, _memberDC.EmpCompanyName));
                     if (!_memberDC.BirthDate.Equals(DateTime.MinValue))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@birthdate", DbType.DateTime, _memberDC.BirthDate));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ref", DbType.String, _memberDC.Ref));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@empNo", DbType.String, _memberDC.EmployeeNo));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@invoiceChage", DbType.Boolean, _memberDC.InvoiceChage));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@smsInvoice", DbType.Boolean, _memberDC.SmsInvoice));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@isFollowUp", DbType.Boolean, _memberDC.IsFollowUp));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@accountno", DbType.String, _memberDC.AccountNo));

                    if (_memberDC.PinCode != 0 && _memberDC.PinCode != -1)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@pinCode", DbType.Int32, _memberDC.PinCode));
                    if (_memberDC.GuardianId > 0)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@guardianId", DbType.Int32, _memberDC.GuardianId));
                    if (_memberDC.IntroducedById > 0)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@inroducedById", DbType.Int32, _memberDC.IntroducedById));
                    if (_memberDC.GroupId > 0)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@groupId", DbType.Int32, _memberDC.GroupId));
                    if(_memberDC.InstructorId > 0)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@instructorId", DbType.Int32, _memberDC.InstructorId));
                    if (!string.IsNullOrEmpty(_memberDC.ImagePath))
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@imagePath", DbType.String, _memberDC.ImagePath));
                    if (_memberDC.SponserId > 0)
                    {
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponserId", DbType.Int32, _memberDC.SponserId));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsorStartDate", DbType.DateTime, _memberDC.SponsorStartDate == DateTime.MinValue ? DateTime.Now : _memberDC.SponsorStartDate));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsorEndDate", DbType.DateTime, _memberDC.SponsorEndDate == DateTime.MinValue ? null :_memberDC.SponsorEndDate));
                        if (_memberDC.EmployeeCategoryForSponser != null)
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponserCategoryId", DbType.Int32, _memberDC.EmployeeCategoryForSponser.Id));
                    }
                }

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@personNo", DbType.String, _memberDC.PersonNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fax", DbType.String, _memberDC.FaxNo));
                if(!string.IsNullOrEmpty(_memberDC.Email))
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@email", DbType.String, _memberDC.Email));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address1", DbType.String, _memberDC.Address1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address2", DbType.String, _memberDC.Address2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address3", DbType.String, _memberDC.Address3));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@zip", DbType.String, _memberDC.PostCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@postplace", DbType.String, _memberDC.PostPlace));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@role", DbType.String, _memberDC.Role.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gender", DbType.String, _memberDC.Gender));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@description", DbType.String, _memberDC.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _memberDC.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastModifiedUser", DbType.String, _memberDC.ModifiedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Int32, _memberDC.MemberStatuse));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryId", DbType.Int32, _memberDC.MemberCategory.Id));

                if (!string.IsNullOrEmpty(_memberDC.MemberCardNo))
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberCardNo", DbType.String, _memberDC.MemberCardNo));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isSendAdvertisement", DbType.Boolean, _memberDC.IsSendAdvertisement));
                if (!string.IsNullOrEmpty(_memberDC.CountryId))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@countryId", DbType.String, _memberDC.CountryId));
                }
                if (!string.IsNullOrEmpty(_memberDC.Mobile))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@mobile", DbType.String, _memberDC.Mobile));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@mobilePrefix", DbType.String, _memberDC.MobilePrefix));
                }

                if (!string.IsNullOrEmpty(_memberDC.WorkTeleNo))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@telework", DbType.String, _memberDC.WorkTeleNo));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@teleworkPrefix", DbType.String, _memberDC.WorkTeleNoPrefix));
                }

                if (!string.IsNullOrEmpty(_memberDC.PrivateTeleNo))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@telehome", DbType.String, _memberDC.PrivateTeleNo));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@telehomePrefix", DbType.String, _memberDC.PrivateTeleNoPrefix));
                }    

                cmd.Parameters.Add(_memberDC.InfoCategoryList != null
                                       ? DataAcessUtils.CreateParam("@informationCategoryItems", SqlDbType.Structured,
                                                                    _dataTable)
                                       : DataAcessUtils.CreateParam("@informationCategoryItems", SqlDbType.Structured,
                                                                    null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gmStartDate", DbType.DateTime, _memberDC.GroupMembershipStartDate));

                if(_memberDC.DueDate > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@DueDay", DbType.Int32, _memberDC.DueDate));

                if (!string.IsNullOrEmpty(_memberDC.GATCardNo))
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GatCardNo", DbType.String, _memberDC.GATCardNo));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Ssn", DbType.String, _memberDC.Ssn));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LanguageId", DbType.Int32, _memberDC.LanguageId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserId", DbType.Int32, _memberDC.UserId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@School", DbType.String, _memberDC.School));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastPaidSemesterFee", DbType.String, _memberDC.LastPaidSemesterFee));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AgressoId", DbType.Int32, _memberDC.AgressoId));


                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 100;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);

                cmd.ExecuteNonQuery();
                memberId = Convert.ToString(output.Value);

              
              
            }
            catch (Exception ex)
            {
                throw ex;
                
            }
            return memberId;
        }
    }
}
