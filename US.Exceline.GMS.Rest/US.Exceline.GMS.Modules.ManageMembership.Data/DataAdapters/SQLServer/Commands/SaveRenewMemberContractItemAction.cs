﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveRenewMemberContractItemAction : USDBActionBase<bool>
    {
        private int _memberContractId = -1;

        private int _branchId = -1;
        private ContractItemDC _contractItem;

        public SaveRenewMemberContractItemAction(ContractItemDC item, int memberContractId, int branchId)
        {
            _memberContractId = memberContractId;

            _contractItem = item;
            _branchId = branchId;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;
            string spName = "USExceGMSManageMembershipSaveRenewContractItems";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberContractID", System.Data.DbType.Int32, _memberContractId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@articleId", System.Data.DbType.Int32, _contractItem.ArticleId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsStartupItem", System.Data.DbType.Boolean, _contractItem.IsStartUpItem));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Price", System.Data.DbType.Decimal, _contractItem.Price));
                command.Parameters.Add(DataAcessUtils.CreateParam("@units", System.Data.DbType.Decimal, _contractItem.Quantity));
                command.Parameters.Add(DataAcessUtils.CreateParam("@noofOrders", System.Data.DbType.Decimal, _contractItem.NumberOfOrders));
                //if (_contractItem.StartDate.HasValue)
                //    command.Parameters.Add(DataAcessUtils.CreateParam("@startdate", System.Data.DbType.DateTime, _contractItem.StartDate));
                //if (_contractItem.EndDate.HasValue)
                //    command.Parameters.Add(DataAcessUtils.CreateParam("@enddate", System.Data.DbType.DateTime, _contractItem.EndDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchID", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@isbookingArticle", System.Data.DbType.Boolean, _contractItem.IsBookingArticle));


                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 100;
                output.Direction = ParameterDirection.Output;
                command.Parameters.Add(output);
                command.ExecuteNonQuery();

                int result1 = Convert.ToInt32(output.Value);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
