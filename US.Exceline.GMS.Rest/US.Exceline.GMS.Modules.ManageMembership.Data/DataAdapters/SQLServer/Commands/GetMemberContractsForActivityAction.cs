﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "6/19/2012 11:49:23
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberContractsForActivityAction : USDBActionBase<List<MemberContractDC>>
    {
        private int _memberId;
        private int _activityId;
        private int _branchId;

        public GetMemberContractsForActivityAction(int memberId,int activityId,int branchId)
        {
            this._memberId = memberId;
            this._activityId = activityId;
            this._branchId = branchId;
        }

        protected override List<MemberContractDC> Body(DbConnection connection)
        {
            List<MemberContractDC> memberContractList = new List<MemberContractDC>();
            string storedProcedureName = "USExceGMSManageMembershipGetMemberContractsForActivity";
            DbDataReader reader = null;
            
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", System.Data.DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@activityId", System.Data.DbType.Int32, _activityId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _branchId));

                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    MemberContractDC memberContract = new MemberContractDC();
                    memberContract.Id = Convert.ToInt32(reader["MemberContractId"]);
                    memberContract.MemberId = Convert.ToInt32(reader["MemberId"]);
                    if (reader["NumberofInstallments"] != DBNull.Value)
                        memberContract.NoOfInstallments = Convert.ToInt32(reader["NumberofInstallments"]);
                    if (reader["ContractStartDate"] != DBNull.Value)
                        memberContract.ContractStartDate = Convert.ToDateTime(reader["ContractStartDate"]);
                    if (reader["ContractEndDate"] != DBNull.Value)
                        memberContract.ContractEndDate = Convert.ToDateTime(reader["ContractEndDate"]);
                    if (reader["ToTime"] != DBNull.Value)
                        memberContract.ToTime = Convert.ToDateTime(reader["ToTime"]);
                    if (reader["FromTime"] != DBNull.Value)
                        memberContract.FromTime = Convert.ToDateTime(reader["FromTime"]);
                    if (reader["Package"] != DBNull.Value)
                        memberContract.ContractName = Convert.ToString(reader["Package"]);
                    if (reader["PackageType"] != DBNull.Value)
                        memberContract.ContractTypeName = Convert.ToString(reader["PackageType"]);
                    if (reader["IsATG"] != DBNull.Value)
                        memberContract.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    if (reader["TotalSum"] != DBNull.Value)
                        memberContract.TotalSum = Convert.ToDecimal(reader["TotalSum"]);
                    if (reader["ActivityName"] != DBNull.Value)
                        memberContract.ActivityName = Convert.ToString(reader["ActivityName"]);
                    if (reader["ArticleId"] != DBNull.Value)
                        memberContract.ArticleId = Convert.ToInt32(reader["ArticleId"]);
                    if (reader["ArticleName"] != DBNull.Value)
                        memberContract.ArticleName = Convert.ToString(reader["ArticleName"]);

                    if (reader["IsCanceled"] != DBNull.Value)
                    {
                        bool isCanceled = Convert.ToBoolean(reader["IsCanceled"]);
                        if (isCanceled)
                            memberContract.Status = "Closed";
                    }


                    memberContractList.Add(memberContract);

                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            return memberContractList;
        }
    }
}
