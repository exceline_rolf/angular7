﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class MergeOrdersAction : USDBActionBase<List<InstallmentDC>>
    {
        private List<InstallmentDC> _updatedOrders = new List<InstallmentDC>();
        private List<int> _removedOrders = new List<int>();
        private int _memberContractId = -1;
        private string _gymCode = string.Empty;
        private string _user = string.Empty;
        public MergeOrdersAction(List<InstallmentDC> updatedOrder, List<int> removedOrders, int memberContractId, string gymCode, string user)
        {
            _updatedOrders = updatedOrder;
            _removedOrders = removedOrders;
            _memberContractId = memberContractId;
            _gymCode = gymCode;
            _user = user;
        }

        protected override List<InstallmentDC> Body(System.Data.Common.DbConnection connection)
        {
            DbTransaction transaction = null;
            try
            {
                transaction = connection.BeginTransaction();
                //Update the deleted orders
                DeleteInstallmentAction deleteAction = new DeleteInstallmentAction(_removedOrders, _memberContractId, _user);
                deleteAction.RunOnTransaction(transaction);

                //Update the merged order
                foreach (InstallmentDC ins in _updatedOrders)
                {
                    UpdateMemberInstallment updateAction = new UpdateMemberInstallment(ins);
                    updateAction.RunOnTransaction(transaction);
                }
                transaction.Commit();
                GetInstallmentsAction ordersAction = new GetInstallmentsAction(_memberContractId, _gymCode);
                return ordersAction.Execute(EnumDatabase.Exceline, _gymCode);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
