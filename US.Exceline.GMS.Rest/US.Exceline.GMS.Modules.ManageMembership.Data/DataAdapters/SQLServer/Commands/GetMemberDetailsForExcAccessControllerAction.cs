﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberDetailsForExcAccessControllerAction : USDBActionBase<List<ExceACCMember>>
    {
        private readonly string _cardNo = string.Empty;
        private int _branchId;
        public GetMemberDetailsForExcAccessControllerAction(string cardNo,int branchId)
        {
            _cardNo = cardNo;
            _branchId = branchId;
        }
        protected override List<ExceACCMember> Body(DbConnection connection)
        {
            var memberList = new List<ExceACCMember>();
            string spName = "USExceGMSGetAuthorizedMemberPerDay_1_8";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@CardNo", System.Data.DbType.String, _cardNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.String, _branchId));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExceACCMember accessMember = new ExceACCMember();
                    accessMember.Id = Convert.ToInt32(reader["ID"]);
                    accessMember.MemberCardNo = Convert.ToString(reader["MemberCardNo"]);
                    accessMember.Name = Convert.ToString(reader["Name"]);
                    accessMember.CustId = Convert.ToString(reader["CustId"]);
                    accessMember.HomeGym = Convert.ToString(reader["HomeGym"]);
                    accessMember.Gender = Convert.ToString(reader["Gender"]);
                    if (reader["Born"] != DBNull.Value)
                        accessMember.Born = Convert.ToDateTime(reader["Born"]).ToString("yyyy.MM.dd");
                    if (reader["LastVisitDate"] != DBNull.Value)
                        accessMember.LastVisitDate = Convert.ToDateTime(reader["LastVisitDate"]).ToString("yyyy.MM.dd");
                    accessMember.CreditBalance = Convert.ToDecimal(reader["CreditBalance"]);
                    accessMember.GuestCardNo = Convert.ToString(reader["GuestCardNo"]);
                    accessMember.MemberContractNo = Convert.ToString(reader["MemberContractNo"]);
                    accessMember.MemberContractId = Convert.ToInt32(reader["memberContractId"]);
                    if (reader["ContractStartDate"] != DBNull.Value)
                        accessMember.ContractStartDate = Convert.ToDateTime(reader["ContractStartDate"]).ToString("yyyy.MM.dd");
                    if (reader["ContractEndDate"] != DBNull.Value)
                        accessMember.ContractEndDate = Convert.ToDateTime(reader["ContractEndDate"]).ToString("yyyy.MM.dd");

                    accessMember.TemplateNo = Convert.ToString(reader["TemplateNo"]);
                    accessMember.TemplateName = Convert.ToString(reader["TemplateName"]);
                    accessMember.ContractType = Convert.ToString(reader["ContractType"]);
                    accessMember.AvailableVisits = Convert.ToInt32(reader["AvailableVisits"]);
                    accessMember.AccessProfileId = Convert.ToInt32(reader["AccessProfileID"]);
                    accessMember.ShopAccBalance = Convert.ToDecimal(reader["AccountBalance"]);
                    if (reader["AntiDopingSignedDate"] != DBNull.Value)
                        accessMember.AntiDopingDate = Convert.ToDateTime(reader["AntiDopingSignedDate"]).ToString("yyyy.MM.dd");
                    accessMember.IsFreezed = Convert.ToString(reader["IsFreezed"]);
                    accessMember.IsContractATG = Convert.ToString(reader["IsContractATG"]);
                    accessMember.MemberATGStatus = Convert.ToString(reader["MemberATGStatus"]);
                    accessMember.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    accessMember.ActivityName = reader["ActivityName"].ToString();
                    accessMember.MemberMessages = Convert.ToString(reader["MemberMessages"]);
                    if (reader["FreezeEndDate"] != DBNull.Value)
                        accessMember.FreezeToDate = Convert.ToDateTime(reader["FreezeEndDate"]).ToString("yyyy.MM.dd");
                    accessMember.Mobile = Convert.ToString(reader["Mobile"]);
                    accessMember.UnPaidInvoiceCount = Convert.ToInt32(reader["UnPaidInvoiceCount"]);
                    accessMember.MinutesBetweenSwipes = Convert.ToInt32(reader["AccMinutesBetweenAccess"]);
                    accessMember.MinimumUnpaidInvoices = Convert.ToInt32(reader["MinimumUnpaidInvoices"]);
                    accessMember.UnPaidDueBalance = Convert.ToDecimal(reader["UnPaidDueBalance"]);
                    accessMember.MinimumOnAccountBalance = Convert.ToDecimal(reader["MinimumOnAccountBalance"]);
                    accessMember.IsSendSms = Convert.ToBoolean(reader["IsSendSms"]);
                    accessMember.MinimumPunches = Convert.ToInt32(reader["MinimumPunches"]);


                    accessMember.IsDisplayName = Convert.ToBoolean(reader["IsDisplayName"]);
                    accessMember.IsDisplayCustomerNumber = Convert.ToBoolean(reader["IsDisplayCustomerNumber"]);
                    accessMember.IsDisplayContractTemplate = Convert.ToBoolean(reader["IsDisplayContractTemplate"]);
                    accessMember.IsDisplayHomeGym = Convert.ToBoolean(reader["IsDisplayHomeGym"]);
                    accessMember.IsDisplayContractEndDate = Convert.ToBoolean(reader["IsDisplayContractEndDate"]);
                    accessMember.IsDisplayBirthDate = Convert.ToBoolean(reader["IsDisplayBirthDate"]);
                    accessMember.IsDisplayAge = Convert.ToBoolean(reader["IsDisplayAge"]);
                    accessMember.IsDisplayLastVisit = Convert.ToBoolean(reader["IsDisplayLastVisit"]);
                    accessMember.IsDisplayCreditBalance = Convert.ToBoolean(reader["IsDisplayCreditBalance"]);
                    accessMember.IsDisplayPicture = Convert.ToBoolean(reader["IsDisplayPicture"]);
                    accessMember.IsDisplayLastAccess = Convert.ToBoolean(reader["IsDisplayLastAccess"]);
                    accessMember.IsDisplayNotification = Convert.ToBoolean(reader["IsDisplayNotification"]);

                    memberList.Add(accessMember);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return memberList;
        }
    }
}
