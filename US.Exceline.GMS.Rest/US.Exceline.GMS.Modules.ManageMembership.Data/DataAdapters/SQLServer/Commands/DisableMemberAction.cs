﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;



namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class DisableMemberAction : USDBActionBase<bool>
    {
       private int _memberId;
       private bool _deletedStatus;
       private string _comment = string.Empty;
       private string _user = string.Empty;
       public DisableMemberAction(int memberId, bool deletedStatus, string comment, string user)
       {
           _memberId = memberId;
           _deletedStatus = deletedStatus;
           _comment = comment;
           _user = user;
       }

       protected override bool Body(DbConnection connection)
       {
           bool result = false;
           string StoredProcedureName = "USExceGMSManageMembershipDisableMember";
           try
           {
               DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@deletedStatus", DbType.Boolean, _deletedStatus));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, _comment));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));

               cmd.ExecuteNonQuery();
               result = true;
           }
           catch (Exception)
           {
               throw;
           }
           return result;
       }
    }
}
