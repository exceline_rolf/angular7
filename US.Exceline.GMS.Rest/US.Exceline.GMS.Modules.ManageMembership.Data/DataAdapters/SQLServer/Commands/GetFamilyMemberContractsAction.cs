﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetFamilyMemberContractsAction : USDBActionBase<List<ContractSummaryDC>>
    {
        private int _memberId = -1;
        private int _branchId = -1;
        private string _gymCode = string.Empty;
        private bool _isFreezDetailNeeded = false;

        public GetFamilyMemberContractsAction(int memberId, int branchID, string gymCode, bool isFreezDetailNeeded)
        {
            this._memberId = memberId;
            _branchId = branchID;
            _gymCode = gymCode;
            _isFreezDetailNeeded = isFreezDetailNeeded;
        }

        protected override List<ContractSummaryDC> Body(DbConnection connection)
        {
            List<ContractSummaryDC> contractList = new List<ContractSummaryDC>();
            string storedProcedureName = "USExceGMSGetFamilyMemberContracts";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("MemberId", System.Data.DbType.Int32, _memberId));
                DbDataReader reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    ContractSummaryDC contract = new ContractSummaryDC();
                    contract.Id = Convert.ToInt32(reader["ID"]);
                    contract.ContractNo = Convert.ToString(reader["ContractNo"]);
                    contract.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    contract.TemplateName = Convert.ToString(reader["TemplateName"]);
                    contract.ActivityName = Convert.ToString(reader["ActivityName"]);
                    if (reader["CreatedDate"] != DBNull.Value)
                        contract.SignedDate = Convert.ToDateTime(reader["CreatedDate"]);
                    if (reader["ContractEndDate"] != DBNull.Value)
                        contract.EndDate = Convert.ToDateTime(reader["ContractEndDate"]);
                    if (reader["RenewedDate"] != DBNull.Value)
                        contract.RenewedDate = Convert.ToDateTime(reader["RenewedDate"]);
                    contract.NoOfOrders = Convert.ToInt32(reader["NumberofInstallments"]);
                    contract.StartUpItemsPrice = Convert.ToDecimal(reader["StartUpItemPrice"]);
                    contract.MonthlyitemPrice = Convert.ToDecimal(reader["EveryMonthItemPrice"]);
                    contract.TotalPrice = Convert.ToDecimal(reader["PackagePrice"]);
                    contract.Status = Convert.ToString(reader["Status"]);
                    contract.AccessProfileId = Convert.ToInt32(reader["AccessProfileId"]);             
                    contractList.Add(contract);
                }

                if (reader != null)
                    reader.Close();

                List<ContractFreezeItemDC> freezItems;

                foreach (ContractSummaryDC summary in contractList)
                {
                    GetContractAccessTimesAction action = new GetContractAccessTimesAction(summary.AccessProfileId, _branchId);
                    summary.AccesTimes = action.Execute(EnumDatabase.Exceline, _gymCode);

                    // Get  Freez  periods 
                    if (_isFreezDetailNeeded)
                    {
                        freezItems = new List<ContractFreezeItemDC>();
                        GetContractFreezeItemsAction getFreez = new GetContractFreezeItemsAction(summary.Id);
                        freezItems = getFreez.Execute(EnumDatabase.Exceline, _gymCode);
                        summary.ContractFreezList = freezItems;
                    }
                }
                return contractList;
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }
    }
}
