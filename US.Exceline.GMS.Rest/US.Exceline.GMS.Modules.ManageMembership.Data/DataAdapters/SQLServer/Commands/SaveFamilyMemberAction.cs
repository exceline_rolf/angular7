﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    class SaveFamilyMemberAction : USDBActionBase<bool>
    {
         private int _memberId;
         private int _familyMemberId;
         private int _memberContractId;

        public SaveFamilyMemberAction(int memberId, int familyMemberId, int memberContractId)
        {
            _memberId=memberId;
            _familyMemberId = familyMemberId;
            _memberContractId = memberContractId;
        }

        protected override bool Body(DbConnection connection)
        {
            bool _isSwitched = false;
            string StoredProcedureName = "USExceGMSManageMembershipFamilyMember";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                if(_familyMemberId!=-1)//used for Remove the family member
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@familyId", DbType.Int32, _familyMemberId));
                if(_memberContractId!=0)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", DbType.Int32, _memberContractId));
                cmd.ExecuteNonQuery();
               _isSwitched = true;
            }

            catch (Exception ex)
            {
                throw ex ;
            }
            return _isSwitched;
        }
    

}
}
