﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US_DataAccess;


namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class MembercardVisitAction : USDBActionBase<bool>
    {
        int _memberID;
        string _user;

        public MembercardVisitAction(int memberId, string user)
        {
            _memberID = memberId;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            var returnval = true;
            const string storedProcedureName = "USExceGMSManageMembershipLogMemeberVisitEvent";

            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.ExecuteReader();
            }

            catch (Exception ex)
            {
                returnval = false;
                throw ex;
            }

            return returnval;
        }

    }
}
