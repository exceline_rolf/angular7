﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class RemoveContractBookingAction : USDBActionBase<bool>
    {
        private int _memberContractID = -1;
        private string _user = string.Empty;

        public RemoveContractBookingAction(int memberContractID, string user)
        {
            _memberContractID = memberContractID;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            return true;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            try
            {
                DbCommand command = new SqlCommand();
                command.Transaction = transaction;
                command.Connection = transaction.Connection;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandText = "USExceGMSRemoveContractBooking";

                command.Parameters.Add(DataAcessUtils.CreateParam("@contractId", System.Data.DbType.Int32, _memberContractID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                command.ExecuteNonQuery();
                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}
