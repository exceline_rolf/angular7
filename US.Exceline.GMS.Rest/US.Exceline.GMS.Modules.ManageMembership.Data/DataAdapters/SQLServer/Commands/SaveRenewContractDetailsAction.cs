﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/8/2012 5:38:41 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveRenewContractDetailsAction : USDBActionBase<bool>
    {
        private RenewSummaryDC _renewSummary = new RenewSummaryDC();
        private int _branchId = -1;
        private string _gymCode = string.Empty;
        private string _user = string.Empty;

        public SaveRenewContractDetailsAction(RenewSummaryDC renewSummary, int branchId, string gymCode,string user)
        {
            _renewSummary = renewSummary;
            _branchId = branchId;
            _gymCode = gymCode;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSManageMembershipUpdateRenewMemberContract";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lockInUntilDate", DbType.DateTime, _renewSummary.LockInUntilDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@renewContractDetailsId", DbType.Int32, _renewSummary.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@noOfMonths", DbType.Int32, _renewSummary.NoOfMonths));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@noOfOrders", DbType.Int32, _renewSummary.NoOfOrders));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@renewEffectiveDate", DbType.DateTime, _renewSummary.RenewEffectiveDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@priceGuarantyUntilDate", DbType.DateTime, _renewSummary.PriceGuarantyUntilDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lockinPeriod", DbType.Int32, _renewSummary.LockinPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@templateId", DbType.Int32, _renewSummary.TemplateId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@priceGuarantyPeriod", DbType.Int32, _renewSummary.PriceGuarantyPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", DbType.Int32, _renewSummary.MemberContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                cmd.ExecuteNonQuery();
                if(_renewSummary != null)
                {
                foreach(ContractItemDC item in _renewSummary.ItemList)
                {
                    SaveRenewMemberContractItemAction action = new SaveRenewMemberContractItemAction(item, _renewSummary.MemberContractId, _branchId);
                    result = action.Execute(EnumDatabase.Exceline, _gymCode);
                }
                }
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
