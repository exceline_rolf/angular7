﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateBRISMemberDataAction : USDBActionBase<bool>
    {
        private OrdinaryMemberDC _member = null;
        public UpdateBRISMemberDataAction(OrdinaryMemberDC member)
        {
            _member = member;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSUpdateBRISMemberData";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", DbType.Int32, _member.Id));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Ssn", DbType.String, _member.Ssn));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Language", DbType.String, _member.Language));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BridID", DbType.Int32, _member.UserId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@School", DbType.String, _member.School));
                command.Parameters.Add(DataAcessUtils.CreateParam("@LastPaidSemesterFee", DbType.String, _member.LastPaidSemesterFee));
                command.Parameters.Add(DataAcessUtils.CreateParam("@AgressoId", DbType.Int32, _member.AgressoId));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
