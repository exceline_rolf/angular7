﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
   public class GetSwitchPayersAction : USDBActionBase<List<OrdinaryMemberDC>>
    {
       private int _memberId;
       public GetSwitchPayersAction(int branchId,int memberId)
       {
           _memberId=memberId;
       }

        protected override List<OrdinaryMemberDC> Body(DbConnection connection)
        {
            List<OrdinaryMemberDC> switchPayerList = new List<OrdinaryMemberDC>();
            string StoredProcedureName = "USExceGMSManageMembershipGetSwichPayers";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    OrdinaryMemberDC payer = new OrdinaryMemberDC();
                    payer.PayerId = Convert.ToInt32(reader["PayerId"]);
                    payer.PayerActivatedDate = Convert.ToDateTime(reader["ActivatedDateTime"]);
                    payer.PayerName = Convert.ToString(reader["PayerName"]);
                    switchPayerList.Add(payer);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return switchPayerList;
                    
        }
    }
}
