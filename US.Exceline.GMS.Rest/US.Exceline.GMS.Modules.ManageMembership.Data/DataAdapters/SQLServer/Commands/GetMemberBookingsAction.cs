﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberBookingsAction : USDBActionBase<List<MemberBookingDC>>
    {
        private int _memberId;
        private readonly string _user = string.Empty;

        public GetMemberBookingsAction(int memberId, string user)
        {
            _memberId = memberId;
            _user = user;
        }

        protected override List<MemberBookingDC> Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSManageMembershipGetMemberBookings";
            List<MemberBookingDC> memberBookingsList = new List<MemberBookingDC>();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _user));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    MemberBookingDC memberBooking = new MemberBookingDC();
                    memberBooking.ScheduleItemId = Convert.ToInt32(reader["ID"]);
                    memberBooking.ActiveTimeId = Convert.ToInt32(reader["ActiveTimeId"]);
                    memberBooking.Date = Convert.ToDateTime(reader["StartDate"]);
                    memberBooking.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    memberBooking.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    memberBooking.ResourceName  = Convert.ToString(reader["Name"]);
                    memberBooking.CategoryName = Convert.ToString(reader["CategoryName"]);
                    memberBooking.ActivityName = Convert.ToString(reader["ActivityName"]);
                    memberBooking.MemberContractNo = reader["MemberContractNo"].ToString();
                    if (reader["ArrivalDateTime"] != DBNull.Value)
                    memberBooking.ArrivalDateTime = Convert.ToDateTime(reader["ArrivalDateTime"]);

                    if (reader["ReferenceId"] != DBNull.Value)
                    memberBooking.ReferenceId = Convert.ToInt32(reader["ReferenceId"]);
                    memberBooking.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);

                    if(reader["Comment"] != DBNull.Value)
                    {
                    memberBooking.Comment = Convert.ToString(reader["Comment"]);
                    }
                    memberBooking.Article = Convert.ToString(reader["Article"]);
                    if (reader["IsSMSRemindered"] != DBNull.Value)
                         memberBooking.IsSmsRemindered = Convert.ToBoolean(reader["IsSMSRemindered"]);
                    memberBooking.BranchName = Convert.ToString(reader["GymName"]);
                    memberBookingsList.Add(memberBooking);
                  
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }


            string StoredProcedureName2 = "USExceGMSManageMembershipGetMemberBookingResources";
            List<MemberBookingResourcesDC> memberBookingResList = new List<MemberBookingResourcesDC>();
            try
            {
                DbCommand cmd2 = CreateCommand(CommandType.StoredProcedure, StoredProcedureName2);
                cmd2.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                DbDataReader reader2 = cmd2.ExecuteReader();
                while (reader2.Read())
                {
                    MemberBookingResourcesDC memberBooking = new MemberBookingResourcesDC();
                    memberBooking.SheduleItemId = Convert.ToInt32(reader2["ActiveTimeId"]);
                    memberBooking.ResourceId = Convert.ToInt32(reader2["ID"]);
                    memberBooking.ResourceName = Convert.ToString (reader2["Name"]);
                    memberBookingResList.Add(memberBooking);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            int i = 0;
            foreach(MemberBookingDC memberBooking in memberBookingsList)
            {
                i++;
                memberBooking.Number = i.ToString();
                var otherRes = new List<string>();

                foreach(MemberBookingResourcesDC memBookingRes in  memberBookingResList)
                {
                    if (memberBooking.ScheduleItemId == memBookingRes.SheduleItemId)
                    {
                        memberBooking.MemberBookingResources.Add(memBookingRes);
                      //  memberBooking.OtherResource += memBookingRes.ResourceName + ",";
                        otherRes.Add(memBookingRes.ResourceName);
                    
                    }
                }
                memberBooking.OtherResource = string.Join(",", otherRes);
            }

            return memberBookingsList;
        }
    }
}
