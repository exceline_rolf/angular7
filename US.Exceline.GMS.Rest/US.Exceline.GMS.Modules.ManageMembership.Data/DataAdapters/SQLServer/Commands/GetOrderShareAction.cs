﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetOrderShareAction : USDBActionBase<List<ContractItemDC>>
    {
        private int _installmentID = -1;
        public GetOrderShareAction(int installmentID)
        {
            _installmentID = installmentID;
        }

        protected override List<ContractItemDC> Body(System.Data.Common.DbConnection connection)
        {
            string _storedProcedureName = "USExceGMSManageMembershipGetAddonsToInstallments";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, _storedProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentId", System.Data.DbType.Int32, _installmentID));
                DbDataReader reader = command.ExecuteReader();
                List<ContractItemDC>  AddOnList = new List<ContractItemDC>();
                while (reader.Read())
                {
                    ContractItemDC addon = new ContractItemDC();
                    addon.Id = Convert.ToInt32(reader["ItemId"]);
                    addon.Price = Convert.ToDecimal(reader["AddOnPrice"]);
                    addon.EmployeePrice = Convert.ToDecimal(reader["EmployeePrice"]);
                    addon.UnitPrice = Convert.ToDecimal(reader["Unitprice"]);
                    addon.ArticleId = Convert.ToInt32(reader["ArticleID"]);
                    addon.ItemName = reader["AddonName"].ToString();
                    addon.IsStartUpItem = Convert.ToBoolean(reader["IsStartUpItem"]);
                    addon.Quantity = Convert.ToInt32(reader["Quantity"]);
                    addon.StockLevel = Convert.ToInt32(reader["StockLevel"]);
                    addon.Discount = Convert.ToDecimal(reader["Discount"]);
                    addon.IsActivityArticle = Convert.ToBoolean(reader["IsActivityArticle"]);
                    addon.Description = Convert.ToString(reader["Description"]);
                    addon.Priority = Convert.ToInt32(reader["Priority"]);
                    addon.IsBookingArticle = Convert.ToBoolean(reader["IsBookingArticle"]);
                    addon.IsSponsored = Convert.ToBoolean(reader["IsSponsored"]);
                    addon.NoOfVisits = Convert.ToInt32(reader["NoOfVisits"]);
                    addon.VoucherNo = Convert.ToString(reader["VoucherNo"]);
                    addon.IsAddedFromShop = Convert.ToBoolean(reader["IsAddedFromShop"]);

                    if (reader["VoucherExpireDate"] != DBNull.Value)
                         addon.ExpiryDate = Convert.ToDateTime(reader["VoucherExpireDate"]);
                    addon.CategoryCode = Convert.ToString(reader["CategoryCode"]);

                    if (addon.IsSponsored && addon.IsActivityArticle)
                        addon.IsReadOnly = true;
                    addon.IsSelected = true;
                    AddOnList.Add(addon);
                }

                    reader.Close();
                    return AddOnList;
            }
            catch
            {
                throw;
            }
        }
    }
}
