﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateMemberAction : USDBActionBase<string>
    {
        private OrdinaryMemberDC _memberDC;
        private DataTable _dataTable;
        private string _user = string.Empty;

        public UpdateMemberAction(OrdinaryMemberDC memberDc,string user)
        {
            _memberDC = memberDc;
            _memberDC.BranchId = 1;
            _user = user;

            if (_memberDC.InfoCategoryList != null && _memberDC.InfoCategoryList.Count > 0)
                _dataTable = GetInfoCategoryItemLst(_memberDC.InfoCategoryList);
        }

        private DataTable GetInfoCategoryItemLst(IEnumerable<CategoryDC> infoCategoryList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("CategoryId", Type.GetType("System.Int32")));


            foreach (CategoryDC infoCategotyItem in infoCategoryList)
            {
                DataRow dataTableRow = _dataTable.NewRow();
                dataTableRow["CategoryId"] = infoCategotyItem.Id;

                _dataTable.Rows.Add(dataTableRow);
            }

            return _dataTable;
        }


        protected override string Body(DbConnection connection)
        {

            string memberId = "-1";
            const string storedProcedureName = "USExceGMSManageMembershipUpdateMember";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberDC.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberRole", DbType.String, _memberDC.Role.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@firstName", DbType.String, _memberDC.FirstName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastName", DbType.String, _memberDC.LastName));
                if (_memberDC.Role.Equals(MemberRole.COM))
                {
                   // cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastName", DbType.String, _memberDC.CompanyName));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@birthdate", DbType.DateTime, null));

                    if (!string.IsNullOrEmpty(_memberDC.AccountingEmail))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@accountingEmail", DbType.String, _memberDC.AccountingEmail));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@empComapnyName", DbType.String, _memberDC.EmpCompanyName));
                    if (!string.IsNullOrEmpty(_memberDC.ImagePath))
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@imagePath", DbType.String, _memberDC.ImagePath));

                    if (!(_memberDC.BirthDate.Equals(DateTime.MinValue) || _memberDC.BirthDate == new DateTime(1900, 1, 1)))
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@birthdate", DbType.DateTime, _memberDC.BirthDate));
                }

                if (!string.IsNullOrEmpty(_memberDC.CountryId))
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CountryId", DbType.String, _memberDC.CountryId));

                if (!string.IsNullOrEmpty(_memberDC.Email))
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@email", DbType.String, _memberDC.Email));

                if (!string.IsNullOrEmpty(_memberDC.Mobile))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@mobile", DbType.String, _memberDC.Mobile));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@mobilePrefix", DbType.String, _memberDC.MobilePrefix));
                }

                if (!string.IsNullOrEmpty(_memberDC.WorkTeleNo))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@telework", DbType.String, _memberDC.WorkTeleNo));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@teleworkPrefix", DbType.String, _memberDC.WorkTeleNoPrefix));
                }

                if (!string.IsNullOrEmpty(_memberDC.PrivateTeleNo))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@telehome", DbType.String, _memberDC.PrivateTeleNo));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@telehomePrefix", DbType.String, _memberDC.PrivateTeleNoPrefix));
                }   
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address1", DbType.String, _memberDC.Address1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address2", DbType.String, _memberDC.Address2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fax", DbType.String, _memberDC.FaxNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@zip", DbType.String, _memberDC.PostCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@postplace", DbType.String, _memberDC.PostPlace));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gender", DbType.String, _memberDC.Gender));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastModifiedUser", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@age", DbType.Int32, _memberDC.Age));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _memberDC.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Int32, _memberDC.MemberStatuse));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberCardNo", DbType.String,!string.IsNullOrEmpty(_memberDC.MemberCardNo)? _memberDC.MemberCardNo:null));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryid", DbType.Int32, _memberDC.MemberCategory.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isSendAdvertisement", DbType.Boolean, _memberDC.IsSendAdvertisement));
                if (!string.IsNullOrEmpty(_memberDC.VatNumber))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@vatNumber", DbType.String, _memberDC.VatNumber));
                }
                if (_memberDC.InfoCategoryList != null && _memberDC.InfoCategoryList.Count > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@informationCategoryItems", SqlDbType.Structured, _dataTable));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@informationCategoryItems", SqlDbType.Structured, null));
                }

              

                DbParameter para = new SqlParameter();
                para.DbType = DbType.String;
                para.ParameterName = "@outId";
                para.Size = 100;
                para.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para);


                cmd.ExecuteNonQuery();
                memberId = Convert.ToString(para.Value);
                
            }
            catch
            {
                throw;
            }
            return memberId;
        }
    }
}
