﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetTimeMachineArticleAction : USDBActionBase<ArticleDC>
    {        

        public GetTimeMachineArticleAction()
        {            
        }

        protected override ArticleDC Body(System.Data.Common.DbConnection connection)
        {
            ArticleDC article = new ArticleDC();
            try
            {
                const string spName = "USExceGMSGetTimeMachineArticle";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, spName);
                DbDataReader reader = cmd.ExecuteReader();
                
                while (reader.Read())
                {
                    article.Id = Convert.ToInt32(reader["ArticleId"]);
                    article.Description = reader["ArticleName"].ToString();
                    article.CategoryId = Convert.ToInt32(reader["CategoryId"]);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return article;            
        }
    }
}
