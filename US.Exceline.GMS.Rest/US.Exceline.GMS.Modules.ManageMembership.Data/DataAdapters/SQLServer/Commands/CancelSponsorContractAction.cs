﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class CancelSponsorContractAction : USDBActionBase<bool>
    {
        private string _canceledBy = string.Empty;
        private string _comment = string.Empty;
        private int _contrcatId = -1;

        public CancelSponsorContractAction(int contractId, string user, string comment)
        {
            _canceledBy = user;
            _contrcatId = contractId;
            _comment = comment;
        }

        protected override bool Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSManageMembershipCancelSponserContract";
            bool _isCanceled = false;

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractId", DbType.Int32, _contrcatId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@canceldBy", DbType.String, _canceledBy));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, _comment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@closeDate", DbType.DateTime, DateTime.Now));
                cmd.ExecuteNonQuery();
                _isCanceled = true;
            }

            catch 
            {
                throw;
            }
            return _isCanceled;
        }
    }
}
