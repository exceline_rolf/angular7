﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/17/2012 3:23:41 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using US_DataAccess;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateMemContractSettingRefAction : USDBActionBase<bool>
    {
        private int _memContarctId = -1;
        private int _settingId = -1;

        public UpdateMemContractSettingRefAction(int memContarctId, int settingId)
        {
            _memContarctId = memContarctId;
            _settingId = settingId;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSManageMembershipUpdateMemContractSettingRef";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemContarctId", DbType.Int32, _memContarctId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SettingId", DbType.Int32, _settingId));
                cmd.ExecuteNonQuery();
                result = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public bool UpdateMemContractSettingRef(DbTransaction transaction)
        {
            string spName = "USExceGMSManageMembershipUpdateMemContractSettingRef";
            bool result = false;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemContarctId", DbType.Int32, _memContarctId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SettingId", DbType.Int32, _settingId));
                command.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
