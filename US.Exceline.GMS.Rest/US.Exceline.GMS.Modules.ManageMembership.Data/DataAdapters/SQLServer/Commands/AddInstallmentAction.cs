﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class AddInstallmentAction : USDBActionBase<int>
    {
        private InstallmentDC _installment = null;
        private string _user = string.Empty;

        public AddInstallmentAction(InstallmentDC installment, string user)
        {
            _installment = installment;
            _user = user;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipAddInstallment";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@installmentList", SqlDbType.Structured, GetInstallmentTable()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberContractId", SqlDbType.Int, _installment.MemberContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", SqlDbType.VarChar, _user));
                cmd.ExecuteNonQuery();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        private DataTable GetInstallmentTable()
        {
            try
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add(new DataColumn("MemberId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("MemberContractId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("Text", typeof(string)));
                dataTable.Columns.Add(new DataColumn("InstallmentDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("DueDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("PaidDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("Amount", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("InvoiceDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("DebtCollectionDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("PrintedDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("Submitted", typeof(string)));
                dataTable.Columns.Add(new DataColumn("AddonPrice", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("InstallmentNo", typeof(int)));
                dataTable.Columns.Add(new DataColumn("KID", typeof(string)));
                dataTable.Columns.Add(new DataColumn("InstallmentType", typeof(string)));
                dataTable.Columns.Add(new DataColumn("TransferDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("Balance", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("TSDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("TEDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("IsATG", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("Debit", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("DeleteRequest", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("Treat", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("IsOtherPay", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("OrderNo", typeof(string)));
                dataTable.Columns.Add(new DataColumn("ActivityPrice", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("AdonText", typeof(string)));
                dataTable.Columns.Add(new DataColumn("IsInvoiceFee", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("ContractNo", typeof(int)));
                dataTable.Columns.Add(new DataColumn("ServiceAmount", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("ItemAmount", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("SponsoredAmount", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("TemplateId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("CreatedUser", typeof(string)));
                dataTable.Columns.Add(new DataColumn("PayerId", typeof(int)));
                //dataTable.Columns.Add(new DataColumn("EstimatedInvoicedDate", typeof(DateTime)));

                DataRow dataTableRow = dataTable.NewRow();
                dataTableRow["MemberId"] = _installment.MemberId;
                dataTableRow["SponsoredAmount"] = _installment.SponsoredAmount;

                dataTableRow["MemberContractId"] = _installment.MemberContractId;
                dataTableRow["Text"] = _installment.Text;
                dataTableRow["InstallmentDate"] = _installment.InstallmentDate;
                dataTableRow["DueDate"] = _installment.DueDate;
                if (_installment.PaidDate == new DateTime(1, 1, 1))
                {
                    dataTableRow["PaidDate"] = DBNull.Value; // installment.PaidDate;
                }
                else if (_installment.PaidDate == null)
                {
                    dataTableRow["PaidDate"] = DBNull.Value;
                }
                else
                {
                    dataTableRow["PaidDate"] = _installment.PaidDate;
                }
                dataTableRow["InvoiceDate"] = _installment.BillingDate;

                if (_installment.DebtCollectionDate == new DateTime(1, 1, 1))
                {
                    dataTableRow["DebtCollectionDate"] = DBNull.Value;
                }
                else if (_installment.DebtCollectionDate == null)
                {
                    dataTableRow["DebtCollectionDate"] = DBNull.Value;
                }
                else
                {
                    dataTableRow["DebtCollectionDate"] = _installment.DebtCollectionDate;
                }

                dataTableRow["PrintedDate"] = DBNull.Value;
                if (_installment.TransferDate == new DateTime(1, 1, 1))
                {
                    dataTableRow["TransferDate"] = DBNull.Value;
                }
                else
                {
                    dataTableRow["TransferDate"] = _installment.TransferDate;
                }
                dataTableRow["Amount"] = _installment.Amount;
                dataTableRow["Submitted"] = _installment.Submitted;
                dataTableRow["AddonPrice"] = _installment.AdonPrice;
                dataTableRow["InstallmentNo"] = _installment.InstallmentNo;
                dataTableRow["KID"] = string.Empty;
                dataTableRow["InstallmentType"] = _installment.InstallmentType;
                dataTableRow["Balance"] = _installment.Balance;
                if (_installment.TrainingPeriodStart == null)
                {
                    dataTableRow["TSDate"] = DBNull.Value;
                }
                else
                {
                    dataTableRow["TSDate"] = _installment.TrainingPeriodStart;
                }

                if (_installment.TrainingPeriodEnd == null)
                {
                    dataTableRow["TEDate"] = DBNull.Value;
                }
                else
                {
                    dataTableRow["TEDate"] = _installment.TrainingPeriodEnd;
                }
                dataTableRow["IsATG"] = _installment.IsATG;
                dataTableRow["Debit"] = _installment.IsDebit;
                dataTableRow["DeleteRequest"] = _installment.IsDeleteRequest;
                dataTableRow["Treat"] = _installment.IsTreat;

                if (_installment.PayerId != -1)
                {
                    if (_installment.MemberId != _installment.PayerId)
                        dataTableRow["IsOtherPay"] = true;
                    else
                        dataTableRow["IsOtherPay"] = false;
                }
                else
                {
                    dataTableRow["IsOtherPay"] = true;
                }

                dataTableRow["OrderNo"] = _installment.OrderNo;
                dataTableRow["ActivityPrice"] = _installment.ActivityPrice; ;
                dataTableRow["AdonText"] = _installment.AdonText;
                dataTableRow["IsInvoiceFee"] = _installment.IsInvoiceFeeAdded;
                dataTableRow["ContractNo"] = _installment.MemberContractNo;
                dataTableRow["ServiceAmount"] = _installment.ServiceAmount;
                dataTableRow["ItemAmount"] = _installment.ItemAmount;
                dataTableRow["TemplateId"] = _installment.TemplateId;
                dataTableRow["CreatedUser"] = _installment.CreatedUser;
                dataTableRow["PayerId"] = _installment.PayerId;
                //dataTableRow["EstimatedInvoicedDate"] = _installment.EstimatedInvoiceDate; 
                dataTable.Rows.Add(dataTableRow);
                return dataTable;
            }
            catch 
            {
                throw;
            }
        }
    }
}
