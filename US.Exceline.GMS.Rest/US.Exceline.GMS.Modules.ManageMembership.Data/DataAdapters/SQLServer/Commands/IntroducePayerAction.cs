﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class IntroducePayerAction : USDBActionBase<bool>
    {
        private int _memberId;
        private int _introduceId;

        public IntroducePayerAction(int memberId, int introduceId)
        {
            _memberId = memberId;
            _introduceId = introduceId;
        }

        protected override bool Body(DbConnection connection)
        {
            bool _isSwitched = false;
            string StoredProcedureName = "USExceGMSManageMembershipIntroducePayer";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@introduceId", DbType.Int32, _introduceId));
                cmd.ExecuteNonQuery();
                _isSwitched = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _isSwitched;
        }
    }
}
