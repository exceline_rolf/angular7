﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberContractSummaryAction : USDBActionBase<List<ContractSummaryDC>>
    {
        private readonly int _memberId = -1;
        private readonly int _branchId = -1;
        private readonly string _gymCode = string.Empty;
        private readonly bool _isFreezDetailNeeded;
        private readonly string _user = string.Empty;
        private readonly bool _isFreezdView;

        public GetMemberContractSummaryAction(int memberId, int branchId, string gymCode, bool isFreezDetailNeeded, string user, bool isFreezdView)
        {
            _memberId = memberId;
            _branchId = branchId;
            _gymCode = gymCode;
            _isFreezDetailNeeded = isFreezDetailNeeded;
            _user = user;
            _isFreezdView = isFreezdView;
        }

        protected override List<ContractSummaryDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ContractSummaryDC> memberContractList = new List<ContractSummaryDC>();
            string spName = "USExceGMSManageMembershipGetMemberContractsSummary";
            DbDataReader reader = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@isFreezdView", DbType.Boolean, _isFreezdView));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ContractSummaryDC contract = new ContractSummaryDC();
                    contract.Id = Convert.ToInt32(reader["ID"]);
                    contract.MemberID = Convert.ToInt32(reader["MemberID"]);
                    contract.ContractNo = Convert.ToString(reader["ContractNo"]);
                    contract.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    contract.TemplateName = Convert.ToString(reader["TemplateName"]);
                    contract.ActivityName = Convert.ToString(reader["ActivityName"]);
                    if (reader["CreatedDate"] != DBNull.Value)
                        contract.SignedDate = Convert.ToDateTime(reader["CreatedDate"]);
                    if (reader["ContractEndDate"] != DBNull.Value)
                        contract.EndDate = Convert.ToDateTime(reader["ContractEndDate"]);
                    if (reader["ContractStartDate"] != DBNull.Value)
                        contract.StartDate = Convert.ToDateTime(reader["ContractStartDate"]);
                    if (reader["RenewedDate"] != DBNull.Value)
                        contract.RenewedDate = Convert.ToDateTime(reader["RenewedDate"]);
                    if (reader["ClosedDate"] != DBNull.Value)
                        contract.Closedate = Convert.ToDateTime(reader["ClosedDate"]);
                    contract.NoOfOrders = Convert.ToInt32(reader["NumberofInstallments"]);
                    contract.StartUpItemsPrice = Convert.ToDecimal(reader["StartUpItemPrice"]);
                    contract.MonthlyitemPrice = Convert.ToDecimal(reader["EveryMonthItemPrice"]);
                    contract.TotalPrice = Convert.ToDecimal(reader["PackagePrice"]);
                    contract.Status = Convert.ToString(reader["Status"]);
                    contract.AccessProfileId = Convert.ToInt32(reader["AccessProfileId"]);
                    contract.BranchId = Convert.ToInt32(reader["BranchId"]);
                    contract.RemainingVisits = Convert.ToInt32(reader["Visits"]);
                    contract.ContractTypeCode = Convert.ToString(reader["ContractTypeCode"]);
                    contract.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    contract.IsGroupContract = Convert.ToBoolean(reader["IsGroupContract"]);
                    contract.IsFreezed = Convert.ToBoolean(reader["IsFreezed"]);
                    contract.ServiceAmount = Convert.ToDecimal(reader["ServiceAmount"]);
                    contract.GymName = Convert.ToString(reader["GymName"]);
                    contract.IsGroup = Convert.ToBoolean(reader["IsGroup"]);
                    memberContractList.Add(contract);
                }

                if (reader != null)
                    reader.Close();


                List<ContractFreezeItemDC> freezItems;

                foreach (ContractSummaryDC summary in memberContractList)
                {
                    GetContractAccessTimesAction action = new GetContractAccessTimesAction(summary.AccessProfileId, _branchId);
                    summary.AccesTimes = action.Execute(EnumDatabase.Exceline, _gymCode);

                    // Get  Freez  periods 
                    if (_isFreezDetailNeeded)
                    {
                        freezItems = new List<ContractFreezeItemDC>();
                        GetContractFreezeItemsAction getFreez = new GetContractFreezeItemsAction(summary.Id);
                        freezItems = getFreez.Execute(EnumDatabase.Exceline, _gymCode);
                        summary.ContractFreezList = freezItems;
                    }

                }
                return memberContractList;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
