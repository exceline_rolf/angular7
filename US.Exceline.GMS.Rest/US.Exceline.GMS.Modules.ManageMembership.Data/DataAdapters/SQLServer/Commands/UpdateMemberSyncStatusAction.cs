﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateMemberSyncStatusAction : USDBActionBase<bool>
    {
        private string _message = string.Empty;
        private int _userId = -1;
        private string _response = string.Empty;
        private string _status = string.Empty;
             
        public UpdateMemberSyncStatusAction(int userID, string message, string response, string status)
        {
            _message = message;
            _userId = userID;
            _response = response;
            _status = status;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceBRISAddMemberSyncStatus";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@UserID", System.Data.DbType.Int32, _userId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Message", System.Data.DbType.String, _message));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Status", System.Data.DbType.String, _status));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BRISResponse", System.Data.DbType.String, _response));

                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
