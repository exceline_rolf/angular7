﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetTemplatesforPriceUpdateAction : USDBActionBase<List<PackageDC>>
    {
        protected override List<PackageDC> Body(System.Data.Common.DbConnection connection)
        {
            List<PackageDC> packageList = new List<PackageDC>();
            string StoredProcedureName = "USExceGMSGetTemplatesforPriceUpdate";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    PackageDC package = new PackageDC();
                    package.ContractTypeValue = new US.GMS.Core.DomainObjects.Common.CategoryDC();
                    package.PackageId = Convert.ToInt32(reader["Id"]);
                    package.TemplateNumber = Convert.ToString(reader["TemplateNo"]);
                    package.PackageName = reader["Name"].ToString();
                    package.BranchId = Convert.ToInt32(reader["BranchId"]);

                    package.ContractTypeValue.Id = Convert.ToInt32(reader["PackageTypeId"].ToString());
                    package.ContractTypeValue.Code = Convert.ToString(reader["PackageType"].ToString());
                    package.ContractTypeValue.Name = Convert.ToString(reader["PackageTypeName"].ToString());

                    package.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    package.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    package.PackagePrice = Convert.ToDecimal(reader["PackagePrice"]);
                    package.NumOfInstallments = Convert.ToInt32(reader["NumberOfInstallments"]);
                    package.RatePerInstallment = Convert.ToDecimal(reader["RatePerInstallment"]);
                    package.EnrollmentFee = Convert.ToDecimal(reader["EnrollmentFee"]);
                    package.NumOfVisits = Convert.ToInt32(reader["NumberOfVisits"]);
                    package.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    package.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    package.ActivityName = Convert.ToString(reader["ActivityName"]);
                    package.SortingNo = Convert.ToInt32(reader["Priority"]);
                    package.NumOfVisits = Convert.ToInt32(reader["NumberOfVisits"]);
                    package.NoOfMonths = Convert.ToInt32(reader["NoOfMonths"]);
                    packageList.Add(package);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return packageList;
        }
    }
}
