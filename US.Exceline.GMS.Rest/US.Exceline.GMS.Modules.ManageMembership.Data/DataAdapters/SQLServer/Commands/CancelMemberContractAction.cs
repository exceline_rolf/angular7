﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class CancelMemberContractAction : USDBActionBase<bool>
    {
        private int _membercontractId;
        private string _canceledBy = string.Empty;
        private string _comment = string.Empty; 
        private int _minNumber;
        private int _targetInstallmentId;
        private List<int> _installmentIdList = new List<int>();

        public CancelMemberContractAction(int memberContractId, string canceledBy, string comment, 
            List<int> installmentIds, int minNumber, int targetInstallmentId)
        {
            _membercontractId = memberContractId;
            _canceledBy = canceledBy;
            _comment = comment;
            _installmentIdList = installmentIds;
            _minNumber = minNumber;
            _targetInstallmentId = targetInstallmentId;
        }

        protected override bool Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSManageMembershipCancelContract";
            bool _isCanceled = false;

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContrctId", DbType.Int32, _membercontractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@canceldBy", DbType.String, _canceledBy));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, _comment));
                cmd.ExecuteNonQuery();
                _isCanceled = true;
            }

            catch
            {
                throw;
            }

            foreach (int id in _installmentIdList)
            {
                string storedProcedureName = "USExceGMSManageMembershipCloseContractInstallments";

                try
                {
                    DbCommand command = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                    command.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", DbType.Int32, _membercontractId));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@installmentId", DbType.Int32, id));                    
                    command.ExecuteNonQuery();
                    _isCanceled = true;
                }

                catch
                {
                    throw;
                }
            }


            try
            {
                string spName = "USExceGMSManageMembershipSetCloseInstallment";

                DbCommand dcommand = CreateCommand(CommandType.StoredProcedure,spName);
                dcommand.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", DbType.Int32, _membercontractId));
                dcommand.Parameters.Add(DataAcessUtils.CreateParam("@minimumNumberOfInstallments", DbType.Int32, _minNumber));
                dcommand.Parameters.Add(DataAcessUtils.CreateParam("@targetinstallmentId", DbType.Int32, _targetInstallmentId));
                dcommand.ExecuteNonQuery();
                _isCanceled = true;                
            }

            catch 
            {
                throw;
            }

            return _isCanceled;
        }
    }
}
