﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberContractAction : USDBActionBase<MemberContractPrintInfo>
    {
        private  int _contractId = -1;
        private String _gymcode = String.Empty;

        public GetMemberContractAction(int contractId, String gymcode)
        {
            _contractId = contractId;
            _gymcode = gymcode;
        }

        protected override MemberContractPrintInfo Body(DbConnection connection)
        {
            MemberContractPrintInfo member = new MemberContractPrintInfo();
            const string storedProcedureName = "USC_ADP_LDSP_MemberContract";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@itemID", DbType.String, _contractId.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gymCode", DbType.String, _gymcode));

                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    member.GymOrganizationNo = Convert.ToString(reader["GymOrganizationNo"]);
                    member.GymAddress1 = Convert.ToString(reader["GymAddress1"]);
                    member.GymPostalAddress = Convert.ToString(reader["GymPostalAddress"]);
                    member.GymName = Convert.ToString(reader["GymName"]);
                    member.JuridicalName = Convert.ToString(reader["JuridicalName"]);
                    member.GymAccountNo = Convert.ToString(reader["GymAccountNo"]);
                    member.GymCompanyId = Convert.ToInt32(reader["GymCompanyId"]);
                    member.MemberContractNo = Convert.ToString(reader["MemberContractNo"]);
                    member.SignedDate = Convert.ToString(reader["SignedDate"]);
                    member.Template = Convert.ToString(reader["Template"]);
                    member.StartLockPeriod = Convert.ToString(reader["StartLockPeriod"]);
                    member.StopLockPeriod = Convert.ToString(reader["StopLockPeriod"]);
                    member.StrtUpItemPrice = Convert.ToDouble(reader["StrtUpItemPrice"]);
                    member.MonthlyServicePrice = Convert.ToString(reader["MonthlyServicePrice"]);
                    member.MonthlyAddonsPrice = Convert.ToDouble(reader["MonthlyAddonsPrice"]);
                    member.TotPriceLockPeriod = Convert.ToDouble(reader["TotPriceLockPeriod"]);
                    member.ContractCondition = Convert.ToString(reader["ContractCondition"]);
                    member.FirstDeductionDate = Convert.ToString(reader["FirstDeductionDate"]);
                    member.MaxmumATGAmount = Convert.ToDouble(reader["MaxmumATGAmount"]);
                    member.MemberNo = Convert.ToString(reader["MemberNo"]);
                    member.FirstName = Convert.ToString(reader["FirstName"]);
                    member.LastName = Convert.ToString(reader["LastName"]);
                    member.Address1 = Convert.ToString(reader["Address1"]);
                    member.Address2 = Convert.ToString(reader["Address2"]);
                    member.PostalAddress = Convert.ToString(reader["PostalAddress"]);
                    member.TelMobile = Convert.ToString(reader["TelMobile"]);
                    member.TelPrivate = Convert.ToString(reader["TelPrivate"]);
                    member.TelWork = Convert.ToString(reader["TelWork"]);
                    member.Birthdate = Convert.ToString(reader["Birthdate"]);
                    member.BankAccountNo = Convert.ToString(reader["BankAccountNo"]);
                    member.ID = Convert.ToString(reader["ID"]);
                    member.GymCode = Convert.ToString(reader["GymCode"]);
                    member.BranchID = Convert.ToString(reader["BranchID"]);
                    member.DocType = Convert.ToString(reader["DocType"]);
                    member.Today = Convert.ToString(reader["Today"]);
                    member.ContractItemsCount = Convert.ToInt32(reader["ContractItemsCount"]);
                    member.ParentId = Convert.ToInt32(reader["ParentId"]);
                }
            }

            catch (Exception ex)
            {
                throw new NotImplementedException("", ex);
            }
            return member;
        }
    }
}

