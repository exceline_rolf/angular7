﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetSMSNotificationsForMemberAction : USDBActionBase<List<MemberSMSNotificationDC>>
    {
        private readonly int _memberId;
        private readonly int _branchId;
        public GetSMSNotificationsForMemberAction(int memberId, int branchId)
        {
            _memberId = memberId;
            _branchId = branchId;
        }

        protected override List<MemberSMSNotificationDC> Body(DbConnection connection)
        {
            List<MemberSMSNotificationDC> memberSMSNotificationList = new List<MemberSMSNotificationDC>();
            const string spName = "USExceGMSGetSMSNotificationsByMemberId";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memeberId", DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    MemberSMSNotificationDC memberSMSNotifiction = new MemberSMSNotificationDC();
                    memberSMSNotifiction.SentDateTime = Convert.ToDateTime(reader["ActionDate"]);
                    memberSMSNotifiction.CreatedDateTime = Convert.ToDateTime(reader["CreatedDate"]);
                    memberSMSNotifiction.Category = Convert.ToString(reader["Category"]);
                    memberSMSNotifiction.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    memberSMSNotifiction.Content = Convert.ToString(reader["Description"]);
                    memberSMSNotifiction.Status = Convert.ToString(reader["Status"]);
                    memberSMSNotificationList.Add(memberSMSNotifiction);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return memberSMSNotificationList;
        }
    }
}
