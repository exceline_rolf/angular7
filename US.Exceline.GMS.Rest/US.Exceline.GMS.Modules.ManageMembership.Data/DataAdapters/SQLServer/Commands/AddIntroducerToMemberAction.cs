﻿using System.Data.Common;
using System.Data;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class AddIntroducerToMemberAction : USDBActionBase<bool>
    {
        private readonly int _memberId = -1;
        private readonly int _introducerId = -1;

        public AddIntroducerToMemberAction(int memberId, int introducerId)
        {
            _memberId = memberId;
            _introducerId = introducerId;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result;
            const string storedProcedureName = "USExceGMSManageMembershipAddIntroducerToMember";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberID", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@introducerId", DbType.Int32, _introducerId));
                cmd.ExecuteNonQuery();
                result = true;
            }
            catch
            {
                throw;
            }
            return result;
        }
    }
}
