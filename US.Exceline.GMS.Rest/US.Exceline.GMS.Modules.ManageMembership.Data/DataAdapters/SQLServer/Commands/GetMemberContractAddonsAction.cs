﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberContractAddonsAction:USDBActionBase<List<ContractItemDC>>
    {
        private int _memberContractId = -1;

        public GetMemberContractAddonsAction(int memberContractId)
        {
            _memberContractId = memberContractId;
        }

        protected override List<ContractItemDC> Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSManageMembershipGetAddonsToMemberContract";
            List<ContractItemDC> memberContractAddons = new List<ContractItemDC>();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure,StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberContractId", DbType.Int32, _memberContractId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ContractItemDC contractItem = new ContractItemDC()
                    {
                        ItemId = Convert.ToInt32(reader["AddonId"]),
                        ItemName = reader["AddonName"].ToString(),
                        UnitPrice = Convert.ToDecimal(reader["AddonPrice"])                       
                    };

                    memberContractAddons.Add(contractItem);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return memberContractAddons;
        }
    }
}
