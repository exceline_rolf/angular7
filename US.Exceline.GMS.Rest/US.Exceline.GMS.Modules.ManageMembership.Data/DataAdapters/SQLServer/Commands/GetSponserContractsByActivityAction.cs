﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/21/2012 3:01:51 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetSponserContractsByActivityAction : USDBActionBase<List<MemberContractDC>>
    {
        private int _sponserId = -1;
        private int _branchId = -1;
        private int _activityId = -1;

        public GetSponserContractsByActivityAction(int sponserId, int activityId, int branchId)
        {
            _sponserId = sponserId;
            _branchId = branchId;
            _activityId = activityId;
        }

        protected override List<MemberContractDC> Body(System.Data.Common.DbConnection connection)
        {
            List<MemberContractDC> sponserContractList = new List<MemberContractDC>();
            string StoredProcedureName = "USExceGMSManageMembershipGetSponserContractsByActivity";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _sponserId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityId", DbType.Int32, _activityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    MemberContractDC memberContract = new MemberContractDC();

                    memberContract.Id = Convert.ToInt32(reader["ID"]);
                    if (reader["ContractStartDate"] != DBNull.Value)
                        memberContract.ContractStartDate = Convert.ToDateTime(reader["ContractStartDate"]);
                    if (reader["ContractEndDate"] != DBNull.Value)
                        memberContract.ContractEndDate = Convert.ToDateTime(reader["ContractEndDate"]);
                    if (reader["ContractSettingId"] != DBNull.Value)
                        memberContract.SettingId = Convert.ToInt32(reader["ContractSettingId"]);
                    memberContract.ContractName = Convert.ToString(reader["Name"]);
                  
                    sponserContractList.Add(memberContract);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sponserContractList;
        }
    }
}
