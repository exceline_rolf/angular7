﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateMemberContractItems : USDBActionBase<bool>
    {
        private List<ContractItemDC> _startUpItemList = new List<ContractItemDC>();
        private List<ContractItemDC> _monthlyItems = new List<ContractItemDC>();
        private int _memberContractId = -1;
        private string _user = string.Empty;

        public UpdateMemberContractItems(int membercontractId, List<ContractItemDC> startUpItems, List<ContractItemDC> monthlyItems, string user)
        {
            _startUpItemList = startUpItems;
            _monthlyItems = monthlyItems;
            _memberContractId = membercontractId;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            return true;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            try
            {
                DataTable itemsTable = GetDataTable(_startUpItemList, _monthlyItems);

                DbCommand command = new SqlCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "USExceGMSUpdateContractItems";
                command.Transaction = transaction;
                command.Connection = transaction.Connection;
                command.Parameters.Add(DataAcessUtils.CreateParam("@ContractItems", SqlDbType.Structured, itemsTable));
                command.Parameters.Add(DataAcessUtils.CreateParam("@membercontractId", DbType.Int32, _memberContractId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                command.ExecuteNonQuery();
                return true;
            }
            catch
            {
                throw;
            }
        }

        private DataTable GetDataTable(List<ContractItemDC> startUpItems, List<ContractItemDC> monthlyItems)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("MemberContractId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("ArticleNo", typeof(int)));
            dataTable.Columns.Add(new DataColumn("IsStartUpItem", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Price", typeof(decimal)));
            dataTable.Columns.Add(new DataColumn("Quantity", typeof(int)));
            dataTable.Columns.Add(new DataColumn("NoofOrders", typeof(int)));
            dataTable.Columns.Add(new DataColumn("StartDate", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("EndDate", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("IsBookingItem", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("StartOrder", typeof(int)));
            dataTable.Columns.Add(new DataColumn("EndOrder", typeof(int)));
            dataTable.Columns.Add(new DataColumn("IsMemberFee", typeof(bool)));

            dataTable.Columns.Add(new DataColumn("VoucherNo", typeof(string)));
            dataTable.Columns.Add(new DataColumn("VoucherExpireDate", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("UnitPrice", typeof(decimal)));

            
            foreach (ContractItemDC adon in startUpItems)
            {
                DataRow dataTableRow = dataTable.NewRow();
                dataTableRow["MemberContractId"] = _memberContractId;
                dataTableRow["ArticleNo"] = adon.ArticleId;
                dataTableRow["IsStartUpItem"] = true;
                dataTableRow["Price"] = adon.Price;
                dataTableRow["Quantity"] = adon.Quantity;
                dataTableRow["NoofOrders"] = adon.NumberOfOrders;
                dataTableRow["StartDate"] = DBNull.Value;
                dataTableRow["EndDate"] = DBNull.Value;
                dataTableRow["IsBookingItem"] = false;
                dataTableRow["StartOrder"] = adon.StartOrder;
                if(adon.EndOrder > 0)
                    dataTableRow["EndOrder"] = adon.EndOrder;
                dataTableRow["IsMemberFee"] = adon.IsMemberFee;

                dataTableRow["VoucherNo"] = adon.VoucherNo;
                if (adon.ExpiryDate.HasValue)
                    dataTableRow["VoucherExpireDate"] = adon.ExpiryDate;
                else
                    dataTableRow["VoucherExpireDate"] = DBNull.Value;

                dataTableRow["UnitPrice"] = adon.UnitPrice;

                dataTable.Rows.Add(dataTableRow);
            }

            foreach (ContractItemDC adon in monthlyItems)
            {
                DataRow dataTableRow = dataTable.NewRow();
                dataTableRow["MemberContractId"] = _memberContractId;
                dataTableRow["ArticleNo"] = adon.ArticleId;
                dataTableRow["IsStartUpItem"] = false;
                dataTableRow["Price"] = adon.Price;
                dataTableRow["Quantity"] = adon.Quantity;
                dataTableRow["NoofOrders"] = adon.NumberOfOrders;
                dataTableRow["StartDate"] = DBNull.Value;
                dataTableRow["EndDate"] = DBNull.Value;
                dataTableRow["IsBookingItem"] = adon.IsBookingArticle;
                dataTableRow["StartOrder"] = adon.StartOrder;
                if (adon.EndOrder > 0)
                    dataTableRow["EndOrder"] = adon.EndOrder;
                dataTableRow["IsMemberFee"] = adon.IsMemberFee;
                dataTableRow["VoucherNo"] = adon.VoucherNo;
                if (adon.ExpiryDate.HasValue)
                    dataTableRow["VoucherExpireDate"] = adon.ExpiryDate;
                else
                    dataTableRow["VoucherExpireDate"] = DBNull.Value;
                dataTableRow["UnitPrice"] = adon.UnitPrice;
                dataTable.Rows.Add(dataTableRow);
            }
            return dataTable;
        }
    }
}
