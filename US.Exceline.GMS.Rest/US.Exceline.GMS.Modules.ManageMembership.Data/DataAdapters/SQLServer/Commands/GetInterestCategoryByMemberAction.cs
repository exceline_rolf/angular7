﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetInterestCategoryByMemberAction : USDBActionBase<List<CategoryDC>>
    {
        private readonly int _memberId;

        public GetInterestCategoryByMemberAction(int memberId, int branchId)
        {
            _memberId = memberId;
        }

        protected override List<CategoryDC> Body(DbConnection connection)
        {
            List<CategoryDC> interestCategoryList = new List<CategoryDC>();
            const string storedProcedureName = "USExceGMSManageMembershipGetInterestCategoryByMember";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    CategoryDC interestCategory = new CategoryDC();
                    interestCategory.Id = Convert.ToInt32(reader["categoryId"]);
                    interestCategoryList.Add(interestCategory);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return interestCategoryList;
        }
    }
}
