﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractSummariesByEmployeeAction : USDBActionBase<List<ContractSummaryDC>>
    {
        private readonly int _employeeId = -1;
        private readonly int _branchId = -1;
        private readonly DateTime _createdDate;

        public GetContractSummariesByEmployeeAction(int employeeID, int branchId, DateTime createdDate)
        {
            _employeeId = employeeID;
            _branchId = branchId;
            _createdDate = createdDate;
        }

        protected override List<ContractSummaryDC> Body(DbConnection connection)
        {
            var memberContractList = new List<ContractSummaryDC>();
            const string spName = "USExceGMSGetContractSummariesByEmployee";
            DbDataReader reader = null;
            try
            {
                var command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@employeeId", System.Data.DbType.Int32, _employeeId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@createdDate", System.Data.DbType.DateTime, _createdDate));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var contract = new ContractSummaryDC();
                    contract.Id = Convert.ToInt32(reader["ID"]);
                    contract.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
                    contract.ContractNo = Convert.ToString(reader["ContractNo"]);
                    contract.BranchId = Convert.ToInt32(reader["BranchId"]);
                    memberContractList.Add(contract);
                }
                if (reader != null)
                    reader.Close();
                return memberContractList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
