﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 2013/04/03
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetCreditNotesDetails : USDBActionBase<CreditNoteDC>
    {
        private int _arItemNo = -1;
        private string _gymCode = string.Empty;
        public GetCreditNotesDetails(int aritemNo, string gymCode)
        {
            _arItemNo = aritemNo;
            _gymCode = gymCode;
        }
        protected override CreditNoteDC Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSManageMembershipGetCreditNotesDetails";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNo", DbType.String, _arItemNo));
                DbDataReader reader = cmd.ExecuteReader();

                CreditNoteDC creditNote = new CreditNoteDC();
                creditNote.Details = new List<CreditNoteDetailsDC>();
                creditNote.Paymentlist = new List<InvoicePaymentSummaryDC>();
                while (reader.Read())
                {
                    CreditNoteDetailsDC creditNoteDetails = new CreditNoteDetailsDC();
                    creditNoteDetails.Id = Convert.ToInt32(reader["ID"]);;
                    creditNoteDetails.CreatedDate = Convert.ToDateTime(reader["Date"]);
                    creditNoteDetails.User = Convert.ToString(reader["EditUser"]);
                    creditNoteDetails.Note = Convert.ToString(reader["Note"]);
                    creditNoteDetails.OrderlineId = Convert.ToInt32(reader["OrderLineId"]);
                    creditNoteDetails.Amount = Convert.ToDecimal(reader["Amount"]);
                    creditNoteDetails.ArticleText = Convert.ToString(reader["ArticleText"]);
                    creditNoteDetails.ArticleId = Convert.ToInt32(reader["ArticleID"]);
                    creditNoteDetails.CreditedCount = Convert.ToInt32(reader["CreditedQuantity"]);
                    creditNoteDetails.IsEnabled = false;
                    creditNote.GymId = Convert.ToInt32(reader["GymID"]);
                    var action = new GetCreditNotePaymentSummaryAction(_arItemNo);
                    creditNote.Paymentlist = action.Execute(EnumDatabase.Exceline, _gymCode);

                    creditNote.Details.Add(creditNoteDetails);
                }
                return creditNote;
            }
            catch
            {
                throw;
            }
        }
    }
}
