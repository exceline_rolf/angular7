﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateMemberImagePathAction : USDBActionBase<bool>
    {
        private readonly int _memberId;
        private readonly string _imagePath;
        public UpdateMemberImagePathAction(int memberId, string imagePath)
        {
            _memberId = memberId;
            _imagePath = imagePath;
        }
        protected override bool Body(DbConnection connection)
        {
            bool result;
            const string storedProcedureName = "USExceGMSManageMembershipUpdateMemberImagePath";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ImagePath", DbType.String, _imagePath));

                DbParameter para = new SqlParameter();
                para.DbType = DbType.Boolean;
                para.ParameterName = "@outId";
                para.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para);


                cmd.ExecuteNonQuery();
                result = Convert.ToBoolean(para.Value);

            }
            catch
            {
                throw;
            }
            return result;
        }
    }
}
