﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
   public class SaveContractGroupMembersAction : USDBActionBase<int>
    {
        private int _groupId = -1;
        private List<ExcelineMemberDC> _groupMembers;
        private int _memberContractId = -1;
       private string _user = string.Empty;
       
        public SaveContractGroupMembersAction(int groupId, List<ExcelineMemberDC> groupMembers, int memberContractId, string user)
        {
            _groupId = groupId;
            _groupMembers = groupMembers;
            _memberContractId = memberContractId;
            _user = user;
        }
        public int RunOnTransaction(DbTransaction transaction)
        {
            int result = -1;
            try
            {
                DbCommand command = new SqlCommand();
                command.Transaction = transaction;
                command.Connection = transaction.Connection;

                command.CommandText = "USExceGMSManageMembershipSaveContractGroupMembers";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(DataAcessUtils.CreateParam("@GroupContractId", DbType.Int32, _memberContractId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ContractGroupMember", SqlDbType.Structured, GetDataTable(_groupMembers)));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));

                command.ExecuteNonQuery();
                result = 1;
            }
            catch 
            {
                throw;
            }
            return result;
        }


        private DataTable GetDataTable(List<ExcelineMemberDC> groupMembers)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("GroupId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("GroupContractId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("GroupStartDate", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("GroupEndDate", typeof(DateTime)));

            foreach (ExcelineMemberDC member in groupMembers)
            {
                DataRow dataTableRow = dataTable.NewRow();
                dataTableRow["ID"] = member.Id;
                dataTableRow["GroupId"] = _groupId;
                dataTableRow["GroupContractId"] = _memberContractId;
                dataTableRow["GroupStartDate"] = member.GmStartDate;
                if (member.GmEndDate.HasValue)
                {
                    dataTableRow["GroupEndDate"] = member.GmEndDate;
                }else
                {
                    dataTableRow["GroupEndDate"] = DBNull.Value;
                }
                dataTable.Rows.Add(dataTableRow);
            }
            return dataTable;
        }

        protected override int Body(DbConnection connection)
        {
            return 1;
        }
    }
}
