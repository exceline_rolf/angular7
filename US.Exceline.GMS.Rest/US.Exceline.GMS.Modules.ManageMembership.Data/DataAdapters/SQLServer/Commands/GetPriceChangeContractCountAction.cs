﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetPriceChangeContractCountAction : USDBActionBase<int>
    {
        private DateTime _duedate;
        private decimal _minimumAmount = 0;
        private bool _lockInPeriod = false;
        private bool _priceGuarantyPeriod = false;
        private int _templateId = -1;
        private List<int> _gyms = new List<int>();
        private bool _changeToNextTemplate = false;

        public GetPriceChangeContractCountAction(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, bool changeToNextTemplate)
        {
            _duedate = dueDate;
            _lockInPeriod = lockInPeriod;
            _priceGuarantyPeriod = priceGuarantyPeriod;
            _templateId = templateID;
            _gyms = gyms;
            _minimumAmount = minimumAmount;
            _changeToNextTemplate = changeToNextTemplate;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSGetPriceUpdateContractCount";
            int contractCount = 0;
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DueDate", System.Data.DbType.Date, _duedate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lockInPeriod", System.Data.DbType.Boolean, _lockInPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@priceGuarantyPeriod", System.Data.DbType.Boolean, _priceGuarantyPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MinimumAmount", System.Data.DbType.Decimal, _minimumAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ChangeToNextTemplate", System.Data.DbType.Boolean, _changeToNextTemplate));
                if (_templateId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateID", System.Data.DbType.Int32, _templateId));
                }
                var parameter = new SqlParameter
                {
                    ParameterName = "@Branches",
                    SqlDbType = SqlDbType.Structured,
                    Value = GetIds(_gyms)
                };
                cmd.Parameters.Add(parameter);
                contractCount = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception)
            {
                throw;
            }
            return contractCount;
        }

        private DataTable GetIds(List<int> gyms)
        {
            DataTable branches = new DataTable();
            DataColumn col = new DataColumn("ID", typeof(Int32));
            branches.Columns.Add(col);
            foreach (var id in gyms)
            {
                branches.Rows.Add(id);
            }
            return branches;
        }
    }
}
