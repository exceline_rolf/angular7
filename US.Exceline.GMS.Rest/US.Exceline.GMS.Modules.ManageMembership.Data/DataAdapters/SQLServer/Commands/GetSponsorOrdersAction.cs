﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetSponsorOrdersAction : USDBActionBase<List<InstallmentDC>>
    {
        private List<int> _sposors = new List<int>();
        private string _gymCode = string.Empty;
        public GetSponsorOrdersAction(List<int> sponsors, string gymCode)
        {
            _sposors = sponsors;
            _gymCode = gymCode;
        }

        private DataTable GetMemberIdLst(List<int> memberIdLst)
        {
            DataTable _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ID", Type.GetType("System.Int32")));
            foreach (var item in _sposors)
            {
                    DataRow dataTableRow = _dataTable.NewRow();
                    dataTableRow["ID"] = item;
                    _dataTable.Rows.Add(dataTableRow);
            }
            return _dataTable;
        }

        protected override List<InstallmentDC> Body(System.Data.Common.DbConnection connection)
        {
        
            List<InstallmentDC> installmentsList = new List<InstallmentDC>();
            string spName = "USExceGMSManageMembershipGetSPOrders";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@sponsors", SqlDbType.Structured, GetMemberIdLst(_sposors)));
                DbDataReader reader = command.ExecuteReader();
                int count = 1;
                while (reader.Read())
                {
                    InstallmentDC installment = new InstallmentDC();
                    installment.InstallmentId = count;
                    installment.Id = Convert.ToInt32(reader["ID"]);
                    installment.TableId = Convert.ToInt32(reader["ID"]);
                    installment.MemberId = Convert.ToInt32(reader["MemberId"]);
                    installment.MemberContractId = Convert.ToInt32(reader["MemberContractId"]);
                    installment.MemberContractNo = Convert.ToString(reader["MemberContractNo"]);
                    installment.Text = Convert.ToString(reader["Text"]);
                    installment.InstallmentDate = Convert.ToDateTime(reader["InstallmentDate"]);
                    installment.DueDate = Convert.ToDateTime(reader["DueDate"]);               
                    installment.Amount = Convert.ToDecimal(reader["Amount"]);
                    if(reader["AddOnPrice"] != DBNull.Value)
                        installment.AdonPrice=Convert.ToDecimal(reader["AddOnPrice"]);
                    if(reader["Discount"] != DBNull.Value)
                        installment.Discount=Convert.ToDecimal(reader["Discount"]);
                    if(reader["SponsoredAmount"] != DBNull.Value)
                        installment.SponsoredAmount=Convert.ToDecimal(reader["SponsoredAmount"]);
                    installment.Comment=reader["Comment"].ToString();
                    if(reader["OriginalMatuarity"] != DBNull.Value)
                        installment.OriginalDueDate=Convert.ToDateTime(reader["OriginalMatuarity"]);
                    installment.Balance = Convert.ToDecimal(reader["Balance"]);
                    if (reader["InvoiceGeneratedDate"] != DBNull.Value)
                    installment.InvoiceGeneratedDate = Convert.ToDateTime(reader["InvoiceGeneratedDate"]);
                    installment.IsInvoiced = Convert.ToBoolean(reader["InvoiceGenerated"]);
                    installment.InstallmentNo = Convert.ToInt32(reader["InstallmentNo"]);
                    if(reader["TSDate"] != DBNull.Value)
                      installment.TrainingPeriodStart = Convert.ToDateTime(reader["TSDate"]);
                    if (reader["TEDate"] != DBNull.Value)
                        installment.TrainingPeriodEnd = Convert.ToDateTime(reader["TEDate"]);
                    installment.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    installment.IsOtherPay = Convert.ToBoolean(reader["IsOtherPay"]);
                    installment.ContractName = Convert.ToString(reader["TemplateName"]);
                    installment.ActivityName = Convert.ToString(reader["ActivityName"]);
                    installment.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    installment.OriginalCustomer = Convert.ToString(reader["OriginalCustomer"]);
                    installment.EstimatedOrderAmount = Convert.ToDecimal(reader["EstimatedOrderAmount"]);
                    installment.OrderNo = Convert.ToString(reader["OrderNo"]);
                    installment.ActivityPrice = Convert.ToDecimal(reader["ActivityPrice"]);
                    if(reader["EstimatedInvoiceDate"] != DBNull.Value)
                       installment.EstimatedInvoiceDate = Convert.ToDateTime(reader["EstimatedInvoiceDate"]);
                    installment.AdonText = Convert.ToString(reader["AdonText"]);
                    installment.PayerName = Convert.ToString(reader["PayerName"]);
                    if(reader["PayerID"] != DBNull.Value)
                       installment.PayerId = Convert.ToInt32(reader["PayerID"]);
                    installment.IsSpOrder = Convert.ToBoolean(reader["IsSpOrder"]);
                    installment.IsInvoiceFeeAdded = Convert.ToBoolean(reader["IsInvoiceFeeAdded"]);
                    installment.BranchName = Convert.ToString(reader["BranchName"]);
                    installment.ServiceAmount = Convert.ToDecimal(reader["ServiceAmount"]);
                    installment.ItemAmount = Convert.ToDecimal(reader["ItemAmount"]);
                    installment.InstallmentType = Convert.ToString(reader["InstallmentType"]);
                    installment.MemberName = Convert.ToString(reader["MemberName"]);
                    installmentsList.Add(installment);
                    count++;
                }
                reader.Close();
            }
            catch 
            {
                throw;
            }
            if (installmentsList.Count > 0)
            {
                foreach (var item in installmentsList)
                {
                    if (item.IsSpOrder)
                    {
                        GetSponsorInstallmentDetailsAction spAction = new GetSponsorInstallmentDetailsAction(item.Id);
                        item.SponsorItemList = spAction.Execute(EnumDatabase.Exceline, _gymCode);

                        GetOrderShareAction adonAction = new GetOrderShareAction(item.Id);
                        item.AddOnList = adonAction.Execute(EnumDatabase.Exceline, _gymCode);
                    }
                    else
                    {
                        GetOrderShareAction adonAction = new GetOrderShareAction(item.Id);
                        item.AddOnList = adonAction.Execute(EnumDatabase.Exceline, _gymCode);
                    }
                }
            }
            return installmentsList;
        }
    }
}
