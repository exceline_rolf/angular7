﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.ManageMembership.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "5/8/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Collections.ObjectModel;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveMemberVisitAction : USDBActionBase<int>
    {
        private EntityVisitDC _memberVisit;
        private string _user = string.Empty;

        public SaveMemberVisitAction(EntityVisitDC memberVisit, string user)
        {
            _memberVisit = memberVisit;
            _user = user;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSManageMembershipSaveMemberVisit";
            int result = -1; 
            try
            {                
                    DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberVisit.EntityId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _memberVisit.BranchId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@InTime", DbType.DateTime, _memberVisit.InTime));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@VisitDate", DbType.DateTime, _memberVisit.VisitDate ));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@CountVisit", DbType.Boolean, _memberVisit.CountVist));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                    if (_memberVisit.MemberContractId != -1 && _memberVisit.MemberContractId != 0)
                       cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractID", DbType.Int32, _memberVisit.MemberContractId));

                    if (_memberVisit.ArticleID != -1 && _memberVisit.ArticleID != 0)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleId", DbType.Int32, _memberVisit.ArticleID));

                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@VisitType", DbType.String, _memberVisit.VisitType));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _memberVisit.ItemName));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", DbType.Int32, _memberVisit.ActivityId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNo", DbType.Int32, _memberVisit.InvoiceRerefence));

                    if (_memberVisit.TerminalId > 0)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@TerminalId", DbType.Int32, _memberVisit.TerminalId));
                   
                    DbParameter output = new SqlParameter();
                    output.DbType = DbType.Int32;
                    output.ParameterName = "@outId";
                    output.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(output);
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt32(output.Value);         
            }
            catch (Exception ex)
            {
                result = -1;
                throw ex;               
            }
            return result;
        }

    }

}
