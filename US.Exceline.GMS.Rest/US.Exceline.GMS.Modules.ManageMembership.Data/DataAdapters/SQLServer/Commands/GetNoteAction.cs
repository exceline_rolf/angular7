﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetNoteAction : USDBActionBase<OrdinaryMemberDC>
    {
       private int _memberId = -1;
       public GetNoteAction(int memberId)
       {
           _memberId = memberId;
       }

       protected override OrdinaryMemberDC Body(DbConnection connection)
       {
           string storedProcedureName = "USExceGMSManageMembershipGetNote";
           DbDataReader reader = null;
           OrdinaryMemberDC member = new OrdinaryMemberDC();
           try
           {
               DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
               command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));

               reader = command.ExecuteReader();
               while (reader.Read())
               {
                   member.Note=Convert.ToString(reader["Note"]);

               }
               reader.Close();
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               if (reader != null)
                   reader.Close();
           }
           return member;
       }
    }
}
