﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/7/2012 4:27:21 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetSponsorEmployeeCategoryAction : USDBActionBase<List<EmployeeCategoryDC>>
    {
        private int _branchId = 0;
        private int _sponsorId = -1;

        public GetSponsorEmployeeCategoryAction(int branchId, string user, int sponsorId)
        {
            _branchId = branchId;
            _sponsorId = sponsorId;
        }
        protected override List<EmployeeCategoryDC> Body(System.Data.Common.DbConnection connection)
        {
            List<EmployeeCategoryDC> _employeeCategoryList = new List<EmployeeCategoryDC>();
            string storedProcedureName = "USExceGMSManageMembershipGetEmployeeCategory";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponsorId", DbType.Int32, _sponsorId));
              
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    EmployeeCategoryDC _employeeCategory = new EmployeeCategoryDC();
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["SponserMonthlyAmount"]).Trim()))
                    {
                        _employeeCategory.MonthlyAmount = Convert.ToDecimal(reader["SponserMonthlyAmount"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["SponserVisitAmount"]).Trim()))
                    {
                        _employeeCategory.AmountPerVisit = Convert.ToDecimal(reader["SponserVisitAmount"]);
                    }
                    if (_employeeCategory.MonthlyAmount > 0)
                    {
                        _employeeCategory.IsAmountEnable = true;
                        _employeeCategory.IsPercentageEnable = false;
                    }
                    else
                    {
                        _employeeCategory.IsAmountEnable = false;
                        _employeeCategory.IsPercentageEnable = true;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["CategoryName"]).Trim()))
                    {
                        _employeeCategory.LevelName = Convert.ToString(reader["CategoryName"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["BranchId"]).Trim()))
                    {
                        _employeeCategory.BranchId = Convert.ToInt32(reader["BranchId"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["SponsoringPercentage"]).Trim()))
                    {
                        _employeeCategory.SponsoringPercentage = Convert.ToDecimal(reader["SponsoringPercentage"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["ID"]).Trim()))
                    {
                        _employeeCategory.Id = Convert.ToInt32(reader["ID"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["MinVisits"]).Trim()))
                    {
                        _employeeCategory.MinVisits = Convert.ToInt32(reader["MinVisits"]);
                    }
                    if (!(reader["IsDefaultCategory"] is DBNull) )
                    {
                    _employeeCategory.IsDefaultCategory = Convert.ToBoolean(reader["IsDefaultCategory"]);
                    } else
                    {
                        _employeeCategory.IsDefaultCategory = false;
                    }
                    _employeeCategoryList.Add(_employeeCategory);
                }
                return _employeeCategoryList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
    }
}
