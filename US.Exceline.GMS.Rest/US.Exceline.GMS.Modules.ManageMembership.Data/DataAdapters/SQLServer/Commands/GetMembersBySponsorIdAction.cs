﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMembersBySponsorIdAction: USDBActionBase<List<ExcelineMemberDC>>
    {
        private int _branchId = 0;
        private int _sponsorId = -1;
      
        public GetMembersBySponsorIdAction(int sponsorId, int branchId, string user)
        {
            _branchId = branchId;
            _sponsorId = sponsorId;
        }

        protected override List<ExcelineMemberDC> Body(DbConnection connection)
        {
            List<ExcelineMemberDC> sponsoredMembersList = new List<ExcelineMemberDC>();
            string StoredProcedureName = "USExceGMSManageMembershipGetMembersBySponsorId";
             try
             {
                 DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                 cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                 cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponsorId", DbType.Int32, _sponsorId));
                
                 DbDataReader reader = cmd.ExecuteReader();

                 while (reader.Read())
                 {
                     ExcelineMemberDC member = new ExcelineMemberDC();
                     member.Id = Convert.ToInt32(reader["ID"]);
                     member.CustId = Convert.ToString(reader["CustId"]);
                     member.Name= Convert.ToString(reader["Name"]);
                     sponsoredMembersList.Add(member);
                 }
             }
             catch 
             {
                 throw;
             }
            return sponsoredMembersList;
        }
    }
}
