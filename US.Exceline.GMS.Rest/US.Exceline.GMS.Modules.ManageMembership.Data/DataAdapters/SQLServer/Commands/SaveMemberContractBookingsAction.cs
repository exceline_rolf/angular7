﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveMemberContractBookingsAction : USDBActionBase<int>
    {
        private List<ContractBookingDC> _contractBookingList;
        private int _memberContractId = -1;
        private DateTime _contractStartDate;
        private DateTime _contractEndDate;
        private int _branchId = -1;
        private string _user = string.Empty;
        private int _memberId = -1;

        public SaveMemberContractBookingsAction(List<ContractBookingDC> bookingsList, int memberContractId, DateTime contractStartDate, DateTime contractEndDate, int branchId, string user, int memberId)
        {
            _contractBookingList = bookingsList;
            _memberContractId = memberContractId;
            _contractStartDate = contractStartDate;
            _contractEndDate = contractEndDate;
            _branchId = branchId;
            _user = user;
            _memberId = memberId;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            return 1;
        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            string spName = "USExceGMSManageMembershipSaveContractBookings";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Transaction = transaction;
                command.Connection = transaction.Connection;
                command.Parameters.Add(DataAcessUtils.CreateParam("@Bookings", SqlDbType.Structured, GetDataTable(_contractBookingList)));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ContractStartDate", DbType.Date, _contractStartDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ContractEndDate", DbType.Date, _contractEndDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberContractId", DbType.Int32, _memberContractId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                command.ExecuteNonQuery();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private DataTable GetDataTable(List<ContractBookingDC> bookings)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("BookingId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("ResourceId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Day", typeof(string)));
            dataTable.Columns.Add(new DataColumn("BookingAmount", typeof(decimal)));
            dataTable.Columns.Add(new DataColumn("BookingStartTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("BookingEndTime", typeof(DateTime)));

            int i = 1;
            foreach (ContractBookingDC contractBooking in bookings)
            {
                DataRow dataTableRow = dataTable.NewRow();
                dataTableRow["ID"] = i;
                dataTableRow["BookingId"] = contractBooking.BookingId;
                dataTableRow["ResourceId"] = contractBooking.ResourceId;
                dataTableRow["Day"] = contractBooking.Day;
                dataTableRow["BookingAmount"] = contractBooking.BookingAmount;
                dataTableRow["BookingStartTime"] = contractBooking.BookingStartTime;
                dataTableRow["BookingEndTime"] = contractBooking.BookingEndTime;
                dataTable.Rows.Add(dataTableRow);
                i++;
            }
            return dataTable;
        }
    }
}
