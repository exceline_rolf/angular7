﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class CheckBookingAvailabilityAction : USDBActionBase<bool>
    {
        private int _resourceID = -1;
        private string _day = string.Empty;
        private DateTime _startTime;
        private DateTime _endTime;

        public CheckBookingAvailabilityAction(int resourceId, string day, DateTime startTime, DateTime endTime)
        {
            _resourceID = resourceId;
            _day = day;
            _startTime = startTime;
            _endTime = endTime;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;
            string spName = "USExceGMSManageMembershipCheckBookingAvailability";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ResourceID", System.Data.DbType.Int32, _resourceID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Day", System.Data.DbType.String, _day));
                command.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", System.Data.DbType.DateTime, _startTime));
                command.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", System.Data.DbType.DateTime, _endTime));
                result = Convert.ToBoolean(command.ExecuteScalar());
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
