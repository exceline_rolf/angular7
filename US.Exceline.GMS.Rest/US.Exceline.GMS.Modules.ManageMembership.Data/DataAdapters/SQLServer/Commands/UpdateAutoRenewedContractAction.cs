﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateAutoRenewedContractAction : USDBActionBase<bool>
    {

        private int _renewedTemplateId = -1;
        private DateTime _effectiveDate = DateTime.Now;
        private int _memberContractId = -1;
        private DateTime _memberContractDate = DateTime.Now;

        public UpdateAutoRenewedContractAction(int renewedTemplateId, DateTime effectiveDate, int memberContractId, DateTime memberContractDate)
        {
            _renewedTemplateId = renewedTemplateId;
            _effectiveDate = effectiveDate;
            _memberContractId = memberContractId;
            _memberContractDate = memberContractDate;

        }
        protected override bool Body(DbConnection connection)
        {

            bool result = false;
            try
            {
                const string storedProcedureName = "USExceGMSManageMembershipUpdateAutoRenewedContract";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@renewedtemplateId", DbType.Int32, _renewedTemplateId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@effectiveDate", DbType.DateTime, _effectiveDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", DbType.Int32, _memberContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractDate", DbType.DateTime, _memberContractDate));
                cmd.ExecuteNonQuery();
                result = true;

            }
            catch
            {
                result = false;

            }
            return result;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            bool result = false;
            try
            {
                const string storedProcedureName = "USExceGMSManageMembershipUpdateAutoRenewedContract";

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Transaction = transaction;
                cmd.Connection = transaction.Connection;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@renewedtemplateId", DbType.Int32, _renewedTemplateId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@effectiveDate", DbType.DateTime, _effectiveDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", DbType.Int32, _memberContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractDate", DbType.DateTime, _memberContractDate));
                cmd.ExecuteNonQuery();

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;

            }
            return result;
        }
    }
}
