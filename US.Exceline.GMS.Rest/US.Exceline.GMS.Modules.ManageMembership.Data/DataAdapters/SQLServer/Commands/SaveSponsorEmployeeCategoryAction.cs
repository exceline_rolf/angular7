﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/8/2012 5:38:15 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveSponsorEmployeeCategoryAction : USDBActionBase<bool>
    {
        private readonly EmployeeCategoryDC _employeeCategory = new EmployeeCategoryDC();
        private string _user = string.Empty;

        public SaveSponsorEmployeeCategoryAction(EmployeeCategoryDC employeeCategory, string user)
        {
            _employeeCategory = employeeCategory;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result;
            const string storedProcedureName = "USExceGMSManageMembershipSaveEmployeeCategory";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _employeeCategory.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@levelId", DbType.Int32, _employeeCategory.LevelId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MonthlyAmount", DbType.Decimal, _employeeCategory.MonthlyAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VisitAmount", DbType.Decimal, _employeeCategory.AmountPerVisit));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _employeeCategory.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _employeeCategory.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsorId", DbType.Int32, _employeeCategory.SponsorId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MinVisits", DbType.Int32, _employeeCategory.MinVisits));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsoringPercentage", DbType.Int32, _employeeCategory.SponsoringPercentage));

                DbParameter outputPara = new SqlParameter();
                outputPara.DbType = DbType.Int32;
                outputPara.ParameterName = "@OutID";
                outputPara.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputPara);
                cmd.ExecuteNonQuery();
                result = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public bool SaveEmployeeCategory(DbTransaction transaction)
        {
            const string spName = "USExceGMSManageMembershipSaveEmployeeCategory";
            bool result;
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Connection = transaction.Connection;
                command.Transaction = transaction;

                command.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _employeeCategory.Id));
                command.Parameters.Add(DataAcessUtils.CreateParam("@levelId", DbType.Int32, _employeeCategory.LevelId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MonthlyAmount", DbType.Decimal, _employeeCategory.MonthlyAmount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VisitAmount", DbType.Decimal, _employeeCategory.AmountPerVisit));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _employeeCategory.LevelName));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _employeeCategory.BranchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SponsorId", DbType.Int32, _employeeCategory.SponsorId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MinVisits", DbType.Int32, _employeeCategory.MinVisits));
                command.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SponsoringPercentage", DbType.Decimal, _employeeCategory.SponsoringPercentage));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsDefaultCategory", DbType.Decimal, _employeeCategory.IsDefaultCategory));

                DbParameter outputPara = new SqlParameter();
                outputPara.DbType = DbType.Int32;
                outputPara.ParameterName = "@OutID";
                outputPara.Direction = ParameterDirection.Output;
                command.Parameters.Add(outputPara);
                command.ExecuteNonQuery();
                result = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
