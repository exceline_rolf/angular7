﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateContractMembersAction : USDBActionBase<bool>
    {
        private List<int> _membersList = new List<int>();
        private int _memberContractId = -1;
        private int _groupId = -1;
        public UpdateContractMembersAction(int groupID, List<int> memberIds, int membercontractID)
        {
            _membersList = memberIds;
            _memberContractId = membercontractID;
            _groupId = groupID;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            return true;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            try
            {
                DbCommand command = new SqlCommand();
                command.Transaction = transaction;
                command.Connection = transaction.Connection;
                command.CommandText = "USExceGMSUpdateContractMembers";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberIds", SqlDbType.Structured, GetDataTable(_membersList)));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberContractId", DbType.Int32, _memberContractId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@groupID", DbType.Int32, _groupId));
                
                command.ExecuteNonQuery();
                return true;
            }
            catch
            {
                throw;
            }
        }

        private DataTable GetDataTable(List<int> memberIds)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));

            for (int i = 0; i < memberIds.Count; i++)
            {
                DataRow dataTableRow = dataTable.NewRow();
                dataTableRow["ID"] = memberIds[i];
                dataTable.Rows.Add(dataTableRow);
            }
            return dataTable;
        }
    }
}
