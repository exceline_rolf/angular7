﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    class GetMemberIntegrationSettingsAction : USDBActionBase<List<MemberIntegrationSettingDC>>
    {
        private readonly int _branchId;
        private readonly int _memberId;

        public GetMemberIntegrationSettingsAction(int branchId, int memberId)
        {
            _branchId = branchId;
            _memberId = memberId;
        }

        protected override List<MemberIntegrationSettingDC> Body(DbConnection connection)
        {
            List<MemberIntegrationSettingDC> memberIntegrationSettingList = new List<MemberIntegrationSettingDC>();
            const string storedProcedureName = "USExceGMSGetMemberIntegrationSettings";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberId));
                
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    MemberIntegrationSettingDC memberIntegrationSetting = new MemberIntegrationSettingDC();
                    memberIntegrationSetting.Id = Convert.ToInt32(reader["Id"]);
                    memberIntegrationSetting.BranchId = Convert.ToInt32(reader["BranchId"]);
                    memberIntegrationSetting.MemberId = Convert.ToInt32(reader["MemberId"]);
                    memberIntegrationSetting.Token = Convert.ToString(reader["Token"]);
                    memberIntegrationSetting.UserId = Convert.ToString(reader["UserId"]);
                    memberIntegrationSetting.FacilityUserId = Convert.ToString(reader["FacilityUserId"]);
                    memberIntegrationSetting.SystemName = Convert.ToString(reader["SystemName"]);
                    memberIntegrationSettingList.Add(memberIntegrationSetting);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return memberIntegrationSettingList;
        }
    }
}
