﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.ManageMembership.Data
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class RenewContractByIdAction : USDBActionBase<bool>
    {
        private int _branchId = -1;
        private int _memberContractId = -1;


        public RenewContractByIdAction(int memberContractId, int branchId)
        {
            _memberContractId = memberContractId;
            _branchId = branchId;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;
            try
            {
                string storedProcedure = "USExceGMSManageMembershipRenewContractById";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", System.Data.DbType.Int32, _memberContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _branchId));
                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 100;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                result = Convert.ToBoolean(output.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


    }
}
