﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.ManageMembership.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "5/8/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberVisitsAction : USDBActionBase<EntityVisitDetailsDC>
    {
        private int _memberid;
        private int _branchId;
        private DateTime _selectedDate;
        private readonly string _user = string.Empty;

        public GetMemberVisitsAction(int memberId, DateTime selectedDate, int branchId, string user)
        {
            _memberid = memberId;
            _branchId = branchId;
            _selectedDate = selectedDate;
            _user = user;
        }

        protected override EntityVisitDetailsDC Body(DbConnection connection)
        {
            var visitDetails = new EntityVisitDetailsDC();
            visitDetails.VisitList = new List<EntityVisitDC>();

            try
            {
                const string storedProcedure = "USExceGMSManageMembershipGetMemberVisits";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberid));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@selectedDate", DbType.DateTime, _selectedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _user));
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    if(!visitDetails.TrailDate.HasValue)
                    {
                        if (reader["TrailDate"] != DBNull.Value)
                            visitDetails.TrailDate = Convert.ToDateTime(reader["TrailDate"]);
                    }
 
                    EntityVisitDC visit = new EntityVisitDC();
                    visit.Id = Convert.ToInt32(reader["Id"]);
                    visit.EntityId = _memberid;
                    visit.InTime  = Convert.ToDateTime(reader["InTime"]);
                    visit.VisitDate = Convert.ToDateTime(reader["VisitDate"]);
                    if(reader["ContractId"] != DBNull.Value)
                      visit.MemberContractId = Convert.ToInt32(reader["ContractId"]);
                    visit.ContractNo = Convert.ToString(reader["MemberContractNo"]);
                    visit.VisitType = Convert.ToString(reader["VisitType"]);
                    if(reader["ArticleId"] != DBNull.Value)
                      visit.ArticleID = Convert.ToInt32(reader["ArticleId"]);
                    visit.ItemName = Convert.ToString(reader["Description"]);
                    visit.GymName = Convert.ToString(reader["GymName"]);
                    visit.AccessControlType = Convert.ToString(reader["AccessControlType"]);
                    visit.BranchId = Convert.ToInt32(reader["BranchId"]);
                    visitDetails.VisitList.Add(visit);                
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return visitDetails;
        }
    }
}
