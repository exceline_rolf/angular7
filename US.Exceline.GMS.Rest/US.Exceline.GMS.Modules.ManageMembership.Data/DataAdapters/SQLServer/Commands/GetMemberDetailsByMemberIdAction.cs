﻿using System;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;
using US.GMS.Core.SystemObjects;
using System.IO;
using US.GMS.Data.DataAdapters.SQLServer.Commands;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
   public class GetMemberDetailsByMemberIdAction : USDBActionBase<OrdinaryMemberDC>
    {
       private int _memberId;
       private int _branchId;
       private string _memberRole = string.Empty;
       private string _gymCode = string.Empty;
       private string _user = string.Empty;

       public GetMemberDetailsByMemberIdAction(int branchId, string user, int memberId, string memberRole,string gymCode)
       {
           _memberId = memberId;
           _branchId = branchId;
           _memberRole = memberRole;
           _gymCode = gymCode;
           _user = user;
       }

        protected override OrdinaryMemberDC Body(DbConnection connection)
        {
            OrdinaryMemberDC ordinaryMember = new OrdinaryMemberDC();
            const string storedProcedureName = "USExceGMSManageMembershipGetMemberDetailsByMemberID";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberRole", DbType.String, _memberRole));
               
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ordinaryMember.CustId = Convert.ToString(reader["CustId"]);
                    ordinaryMember.EntNo = Convert.ToInt32(reader["EntNo"]);
                    ordinaryMember.Id = Convert.ToInt32(reader["MemberId"]);                    
                    ordinaryMember.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    ordinaryMember.BranchId = Convert.ToInt32(reader["BranchId"]);
                    ordinaryMember.FirstName = reader["FirstName"].ToString();
                    ordinaryMember.LastName = reader["LastName"].ToString();
                    ordinaryMember.Name = _memberRole =="MEM" ? string.Format("{0} {1}", ordinaryMember.FirstName, ordinaryMember.LastName) : string.Format("{0} {1}", ordinaryMember.LastName, ordinaryMember.FirstName);
                    ordinaryMember.FaxNo = reader["FaxNo"].ToString().Trim();
                    ordinaryMember.GuardianId = Convert.ToInt32(reader["ParentId"]);
                    ordinaryMember.GuardianCustId = reader["ParentCustId"].ToString();
                    ordinaryMember.GuardianName = reader["ParentName"].ToString();
                    ordinaryMember.GuardianBranchId = Convert.ToInt32(reader["ParentBranchId"]);
                    string roleType = reader["RoleId"].ToString();
                    if (!string.IsNullOrEmpty(roleType.Trim()))
                    {
                        try
                        {
                            ordinaryMember.Role = (MemberRole)Enum.Parse(typeof(MemberRole), roleType);
                        }
                        catch
                        {
                            ordinaryMember.Role = MemberRole.NONE;
                        }
                    }

                    if (MemberRole.ALL == (MemberRole)Enum.Parse(typeof(MemberRole), roleType) || MemberRole.MEM == (MemberRole)Enum.Parse(typeof(MemberRole), roleType))
                    {
                        ordinaryMember.HasCalender = true;
                    }
                    ordinaryMember.BirthDate = Convert.ToDateTime(reader["BirthDate"]);
                    if (!(ordinaryMember.BirthDate == DateTime.MinValue || ordinaryMember.BirthDate == new DateTime(1900, 1, 1)))
                    ordinaryMember.Age = (DateTime.Now.Year - ordinaryMember.BirthDate.Year);
                    ordinaryMember.Address1 = reader["Address1"].ToString();
                    ordinaryMember.Address2 = reader["Address2"].ToString();
                    ordinaryMember.Address3 = reader["Address3"].ToString();
                    ordinaryMember.PostCode = reader["ZipCodde"].ToString();
                    ordinaryMember.PostPlace = reader["ZipName"].ToString();

                    ordinaryMember.EmpCompanyName = reader["EmpCompanyName"].ToString();
                    ordinaryMember.Ref = reader["Ref"].ToString();
                    ordinaryMember.EmployeeNo = reader["EmployeeNo"].ToString();
                    ordinaryMember.SponserId = Convert.ToInt32(reader["SponserId"]);
                    ordinaryMember.SponserName = Convert.ToString(reader["SponserName"]);
                    ordinaryMember.SponserCustId = Convert.ToString(reader["SponserCustId"]);
                    ordinaryMember.IsSponsor = Convert.ToBoolean(reader["IsSponser"]);


                    ordinaryMember.Mobile = Convert.ToString(reader["Mobile"]).Trim();
                    ordinaryMember.MobilePrefix = Convert.ToString(reader["MobilePrefix"]).Trim();
                    ordinaryMember.WorkTeleNo = Convert.ToString(reader["TeleWork"]).Trim();
                    ordinaryMember.WorkTeleNoPrefix = Convert.ToString(reader["WorkTeleNoPrefix"]).Trim();
                    ordinaryMember.PrivateTeleNo = Convert.ToString(reader["TeleHome"]).Trim();
                    ordinaryMember.PrivateTeleNoPrefix = Convert.ToString(reader["PrivateTeleNoPrefix"]).Trim();
                    ordinaryMember.Email = reader["Email"].ToString();
                    ordinaryMember.AccountingEmail = reader["AccountingEmail"].ToString();
                    ordinaryMember.IntroducedByName = reader["IntroduceByName"].ToString();
                    ordinaryMember.PayerCustId = reader["PayerCustId"].ToString();
                    ordinaryMember.PayerName = reader["PayerName"].ToString();
                    ordinaryMember.PayerId = Convert.ToInt32(reader["PayerId"]);
                    ordinaryMember.GroupNameForMember = reader["GroupName"].ToString();
                    if(reader["CreatedDateTime"] != DBNull.Value)
                      ordinaryMember.RegisterDate = Convert.ToDateTime(reader["CreatedDateTime"]);

                    ordinaryMember.GroupId = Convert.ToInt32(reader["GroupId"]);
                    ordinaryMember.MemberStatuse = Convert.ToInt32(reader["ActiveStatus"]);
                    ordinaryMember.StatusName = reader["StatusName"].ToString();
                    ordinaryMember.AccountNo = reader["AccountNumber"].ToString();
                    string gender = reader["Gender"].ToString();
                    ordinaryMember.ImagePath = reader["ImagePath"].ToString();
                    ordinaryMember.MemberCardNo = reader["MemCardNo"].ToString();

                    if (!string.IsNullOrEmpty(gender.Trim()))
                    {
                        try
                        {
                            ordinaryMember.Gender = (Gender)Enum.Parse(typeof(Gender), gender);
                        }
                        catch {}
                    }

                    if (!string.IsNullOrEmpty(ordinaryMember.ImagePath))
                    {
                        ordinaryMember.ProfilePicture = GetMemberProfilePicture(ordinaryMember.ImagePath);
                    }
                    ordinaryMember.MemberCategory.Name = Convert.ToString(reader["CategoryName"]);
                    ordinaryMember.MemberCategory.Code = Convert.ToString(reader["CategoryCode"]);
                    ordinaryMember.MemberCategory.Id = Convert.ToInt32(reader["CategoryId"]);
                    ordinaryMember.FamilyMemberCustId = Convert.ToString(reader["FamilyMemberCustId"]);
                    ordinaryMember.FamilyMemberName = Convert.ToString(reader["FamilyMemberName"]);
                    ordinaryMember.FamilyMemberId = Convert.ToInt32(reader["FamilyId"]);
                    ordinaryMember.ContractIdForFamily = Convert.ToInt32(reader["MemberContractId"]);
                    if (reader["CreditPeriod"] != DBNull.Value)
                        ordinaryMember.CreditPeriod = Convert.ToInt32(reader["CreditPeriod"]);
                    if (reader["IntroduceDate"] != DBNull.Value)
                        ordinaryMember.IntroduceDate = Convert.ToDateTime(reader["IntroduceDate"]);

                    if (reader["TrailDate"] != DBNull.Value)
                        ordinaryMember.TrailDate = Convert.ToDateTime(reader["TrailDate"]);

                    ordinaryMember.DueDate = Convert.ToInt32(reader["DueDay"]);
                    ordinaryMember.InvoiceChage = Convert.ToBoolean(reader["InvoiceCharge"]);
                    ordinaryMember.GuardianMobile = Convert.ToString(reader["ParentMobile"]);
                    ordinaryMember.GuardianEmail = Convert.ToString(reader["ParentEmail"]);
                    ordinaryMember.VatNumber = Convert.ToString(reader["VatNumber"]);
                    ordinaryMember.InfoGymId = Convert.ToString(reader["InfoGymId"]);

                    if (reader["Ssn"] != DBNull.Value)
                        ordinaryMember.Ssn = Convert.ToString(reader["Ssn"]);

                    if (reader["Language"] != DBNull.Value)
                        ordinaryMember.Language = Convert.ToString(reader["Language"]);

                    if (reader["UserId"] != DBNull.Value)
                        ordinaryMember.UserId = Convert.ToInt32(reader["UserId"]);

                    if (reader["CountryId"] != DBNull.Value)
                        ordinaryMember.CountryId = reader["CountryId"].ToString();
                    ordinaryMember.CountryName = reader["CountryName"].ToString();
                    ordinaryMember.AgressoId = Convert.ToInt32(reader["AgressoId"]);
                    try
                    {
                        GetInfoCategotysForMemberAction action = new GetInfoCategotysForMemberAction(ordinaryMember.Id, _branchId);
                        ordinaryMember.InfoCategoryList = action.Execute(EnumDatabase.Exceline,_gymCode);
                    }
                    catch
                    {
                       
                    }

                    try
                    {
                        GetFollowUpPopUpDetailsAction action = new GetFollowUpPopUpDetailsAction(_memberId);
                        ordinaryMember.FollowUpDetailsList = action.Execute(EnumDatabase.Exceline, _gymCode);
                    }
                    catch
                    {

                    }
                }
            }

            catch (Exception ex)
            {
                throw new NotImplementedException("", ex);
            }
            return ordinaryMember;
        }

        private byte[] GetMemberProfilePicture(string p)
        {
            if (File.Exists(p))
            {
                byte[] byteArray = System.IO.File.ReadAllBytes(p);
                return byteArray;
            }
            return new byte[0];
        }

       
    }
}
