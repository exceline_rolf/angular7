﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "6/6/2012 14:51:51
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveContractFreezeItemAction : USDBActionBase<int>
    {
        private ContractFreezeItemDC _freezeItem;
        private List<ContractFreezeInstallmentDC> _freezeInstallments;
        private string _user;

        public SaveContractFreezeItemAction(ContractFreezeItemDC freezeItem,List<ContractFreezeInstallmentDC> freezeInstallments,string user)
        {
            _freezeItem = freezeItem;
            _freezeInstallments = freezeInstallments;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSManageMembershipSaveFreezeContractDetails";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", DbType.Int32, _freezeItem.MemberContractid));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryId", DbType.Int32, _freezeItem.CategoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", DbType.DateTime, _freezeItem.FromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@toDate", DbType.DateTime, _freezeItem.ToDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@note", DbType.String,_freezeItem.Note));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@freezeDays", DbType.Int32, _freezeItem.FreezeDays));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@shiftCount", DbType.Int32, _freezeItem.ShiftCount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@shiftStartInstallmentId", DbType.Int32, _freezeItem.ShiftStartInstallmentId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractEndDate", DbType.DateTime, _freezeItem.ContractEndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@freezeMonths", DbType.Int32, _freezeItem.FreezeMonths));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@extendedStatus", DbType.Boolean, _freezeItem.IsExtended));


                DataTable freezeInstallmentsTable = new DataTable();
                DataColumn col = null;
                col = new DataColumn("InstallmentId", typeof(Int32));
                freezeInstallmentsTable.Columns.Add(col);
                col = new DataColumn("OriginalDueDate", typeof(DateTime));
                freezeInstallmentsTable.Columns.Add(col);
                col = new DataColumn("NewDueDate", typeof(DateTime));
                freezeInstallmentsTable.Columns.Add(col);
                col = new DataColumn("TrainingPeriodStart", typeof(DateTime));
                freezeInstallmentsTable.Columns.Add(col); 
                col = new DataColumn("TrainingPeriodEnd", typeof(DateTime));
                freezeInstallmentsTable.Columns.Add(col);
                col = new DataColumn("ExtendedTrainingPeriodDays", typeof(int));
                freezeInstallmentsTable.Columns.Add(col);
                col = new DataColumn("InstallmentAmount", typeof(decimal));
                freezeInstallmentsTable.Columns.Add(col);
                col = new DataColumn("EstimatedInvoiceDate", typeof(DateTime));
                freezeInstallmentsTable.Columns.Add(col);

                foreach (var freezeInstallment in _freezeInstallments)
                {
                    freezeInstallmentsTable.Rows.Add(freezeInstallment.InstallmentId, freezeInstallment.InstallmentDueDate, freezeInstallment.NewInstallmentDueDate, freezeInstallment.TrainingPeriodStart, freezeInstallment.TrainingPeriodEnd, freezeInstallment.ExtendTrainingPeriodDays, freezeInstallment.InstallmentAmount, freezeInstallment.EstimatedInvoiceDate);
                }


                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@freezeInstallments";
                parameter.SqlDbType = System.Data.SqlDbType.Structured;
                parameter.Value = freezeInstallmentsTable;
                cmd.Parameters.Add(parameter);

                object obj = cmd.ExecuteScalar();
                int freezeItemId = Convert.ToInt32(obj);
                return freezeItemId;
                //return 1;
            }
            catch (Exception Ex)
            {
                // TODO Anfe: Implement propper error handeling. The code in the catch block needs to do something with the Exception.Ex
                string doNothingString = Ex.Message;
                Console.WriteLine(doNothingString);
                return -1;
            }
        }
    }
}
