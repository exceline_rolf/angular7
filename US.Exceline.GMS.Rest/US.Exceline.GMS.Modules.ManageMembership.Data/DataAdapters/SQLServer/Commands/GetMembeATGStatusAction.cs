﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMembeATGStatusAction : USDBActionBase<List<MemberATGStatusInfo>>
    {
        private int _memberId = -1;
        public GetMembeATGStatusAction(int memberId)
        {
            _memberId = memberId;
        }

        protected override List<MemberATGStatusInfo> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipGetMemberATGStatus";
            DbDataReader reader = null;
            List<MemberATGStatusInfo> statusInfolist = new List<MemberATGStatusInfo>();

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", System.Data.DbType.Int32, _memberId));
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    MemberATGStatusInfo info = new MemberATGStatusInfo();
                    info.Id = Convert.ToInt32(reader["Id"]);
                    info.MemberID = Convert.ToInt32(reader["MemberID"]);
                    info.AccountNumber = Convert.ToString(reader["AccountNo"]);
                    info.AtgStatus = Convert.ToInt32(reader["ATGStatus"]);
                    if(reader["LastUpdatedDate"] != DBNull.Value)
                       info.LastUpdatedDateTime = Convert.ToDateTime(reader["LastUpdatedDate"]);
                    statusInfolist.Add(info);
                    info.SelectedStatus = GetSelectedStatusIndex(info.AtgStatus);
                    info.GymCount = Convert.ToInt32(reader["GymCount"]);
                }
                return statusInfolist;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private int GetSelectedStatusIndex(int status)
        {
            switch (status)
            {
                case 1:
                    return 0;
                case 2:
                    return 1;
                case 5:
                    return 2;
                case 6:
                    return 3;
                default:
                    return -1;
            }
        }
    }
}
