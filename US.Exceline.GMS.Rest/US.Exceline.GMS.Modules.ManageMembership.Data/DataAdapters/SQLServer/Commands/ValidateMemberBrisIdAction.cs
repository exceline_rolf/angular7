﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateMemberBrisIdAction : USDBActionBase<bool>
    {
        private int _brisId;
        public ValidateMemberBrisIdAction(int brisId)
        {
            _brisId = brisId;
        }
        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            const string storedProcedureName = "USExceGMSManageMembershipValidateBrisId";
            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BrisId", DbType.Int32, _brisId));

                result = Convert.ToBoolean(cmd.ExecuteScalar());
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
