﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class IsFreezeAllowedAction : USDBActionBase<int>
    {
        private int _memberContractId;
        private int _memberId;

        public IsFreezeAllowedAction(int memberContractId, int memberId, int branchId)
        {
            this._memberContractId = memberContractId;
            this._memberId = memberId;
        }

        protected override int Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSManageMembershipCheckAllowedToFreeze";
            int id = -1;
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractNo", DbType.Int32, _memberContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@outBit";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);

                cmd.ExecuteNonQuery();
                if (output.Value != null)
                {
                    id = Convert.ToInt32(output.Value);
                }
                else
                {
                    id = -1;
                }
                return id;
            } 
            catch(Exception ex)
            {
                throw ex;
            }
            return id;
        }
    }
}
