﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateInstallmentsAndContractAction : USDBActionBase<bool>
    {
        private decimal _installmentAmount = 0;
        private int _updatingInstallmentNo = -1;
        private MemberContractDC _memberContract = null;
        public UpdateInstallmentsAndContractAction(decimal installmentAmount, int updatingInstallmentNo, MemberContractDC memberContract)
        {
            _installmentAmount = installmentAmount;
            _updatingInstallmentNo = updatingInstallmentNo;
            _memberContract = memberContract;
        }
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipUpdateInstallmentsWithContract";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure,spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberContractId", System.Data.DbType.Int32, _memberContract.Id));
                command.Parameters.Add(DataAcessUtils.CreateParam("@TemplateId", System.Data.DbType.Int32,_memberContract.ContractId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Price", System.Data.DbType.Decimal,_memberContract.ContractPrize));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentPrice", System.Data.DbType.Decimal, _installmentAmount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@UpdatingInstallmentNo", System.Data.DbType.Int32, _updatingInstallmentNo));
                
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
