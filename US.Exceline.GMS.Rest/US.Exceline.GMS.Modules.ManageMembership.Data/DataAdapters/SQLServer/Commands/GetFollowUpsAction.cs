﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : GMA
// Created Timestamp : "10/22/2013 6:07:12 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.Utils;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Linq;
using System.Data.SqlClient;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetFollowUpsAction : USDBActionBase<List<FollowUpDC>>
    {
        private readonly int _memberId;
        private readonly string _user = string.Empty;
        private int _followUpId;

        public GetFollowUpsAction(int memberId,int followUpId, string user)
        {
            _memberId = memberId;
            _user = user;
            _followUpId = followUpId;
        }

        protected override List<FollowUpDC> Body(DbConnection connection)
        {
            List<FollowUpDC> followUpList = new List<FollowUpDC>();
            const string storedProcedureName = "USExceGMSGetFollowUps";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                if(_followUpId > 0)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@followUpId", DbType.Int32, _followUpId));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _user));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    FollowUpDC followUp = new FollowUpDC();
                    followUp.FollowUpDetailList = new List<FollowUpDetailDC>();
                    followUp.Id = Convert.ToInt32(reader["Id"]);
                    followUp.MemberId = Convert.ToInt32(reader["MemberId"]);
                    followUp.FollowUpTemplateId = Convert.ToInt32(reader["FollowUpTemplateId"]);
                    followUp.BranchId = Convert.ToInt32(reader["BranchId"]);
                    followUpList.Add(followUp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return GetFollowUpDetails(followUpList);
        }

        private List<FollowUpDC> GetFollowUpDetails(List<FollowUpDC> followUpList)
        {
            const string storedProcedureName = "USExceGMSGetFollowUpDetails";
            List<FollowUpDC> finalFollowup = new List<FollowUpDC>();
            try
            {
                

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _user));
                var parameter = new SqlParameter
                {
                    ParameterName = "@followUpIds",
                    SqlDbType = SqlDbType.Structured,
                    Value = GetIds(followUpList)
                };

                cmd.Parameters.Add(parameter);

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    FollowUpDetailDC exeTask = new FollowUpDetailDC();
                    exeTask.Id = Convert.ToInt32(reader["Id"]);
                    exeTask.Name = reader["Name"].ToString();
                    exeTask.TaskCategoryId = Convert.ToInt32(reader["TaskCategoryId"]);
                    exeTask.TaskCategoryName = Convert.ToString(reader["TaskCategoryName"]);
                    exeTask.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    exeTask.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    if (reader["StartTime"] != DBNull.Value)
                    {
                        exeTask.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    }
                    if (reader["EndTime"] != DBNull.Value)
                    {
                        exeTask.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    }

                    if (Convert.ToDateTime(reader["FoundDate"]) != DateTime.MinValue)
                        exeTask.FoundDate = Convert.ToDateTime(reader["FoundDate"]);

                    if (Convert.ToDateTime(reader["ReturnDate"]) != DateTime.MinValue)
                        exeTask.ReturnDate = Convert.ToDateTime(reader["ReturnDate"]);

                    if (Convert.ToDateTime(reader["DueDate"]) != DateTime.MinValue)
                        exeTask.DueDate = Convert.ToDateTime(reader["DueDate"]);

                    exeTask.DueTime = reader["DueTime"] != DBNull.Value
                                          ? (DateTime?)Convert.ToDateTime(reader["DueTime"])
                                          : null;

                    exeTask.NumOfDays = Convert.ToInt32(reader["NumOfDays"]);
                    exeTask.PhoneNo = reader["PhoneNo"].ToString();
                    exeTask.AutomatedSMS = reader["Sms"].ToString();
                    if (!String.IsNullOrEmpty(reader["ExtenedInfo"].ToString()))
                    {
                        ExtendedFieldInfoDC fieldInfo = new ExtendedFieldInfoDC();
                        fieldInfo = (ExtendedFieldInfoDC)XMLUtils.DesrializeXMLToObject(reader["ExtenedInfo"].ToString(), typeof(ExtendedFieldInfoDC));
                        exeTask.ExtendedFieldsList = fieldInfo.ExtendedFieldsList;
                    }
                    exeTask.CreatedDateTime = Convert.ToDateTime(reader["CreatedDateTime"]);
                    exeTask.LastModifiedDateTime = Convert.ToDateTime(reader["LastModifiedDateTime"]);
                    exeTask.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    exeTask.LastModifiedUser = Convert.ToString(reader["LastModifiedUser"]);
                    exeTask.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    exeTask.IsShowInCalandar = Convert.ToBoolean(reader["IsShowInCalandar"]);
                    exeTask.FollowUpId = Convert.ToInt32(reader["FollowUpId"]);
                    exeTask.AssignedEmpId = Convert.ToInt32(reader["AssignedEmpId"]);
                    exeTask.AssignedEmpName = Convert.ToString(reader["AssignedEmpName"]);
                    exeTask.EntRoleId = Convert.ToInt32(reader["EntRoleId"]);
                    exeTask.RoleType = Convert.ToString(reader["RoleType"]);
                    if (reader["CompletedDate"] != DBNull.Value)
                    {
                        exeTask.CompletedDate = Convert.ToDateTime(reader["CompletedDate"]);
                    }
                    exeTask.CompletedEmpId = Convert.ToInt32(reader["CompletedEmpId"]);
                    exeTask.CompletedEmpName = Convert.ToString(reader["CompletedEmpName"]);
                    if (reader["PlannedDate"] != DBNull.Value)
                    {
                        exeTask.PlannedDate = Convert.ToDateTime(reader["PlannedDate"]);
                    }
                    exeTask.IsFixed = Convert.ToBoolean(reader["IsFixed"]);
                    exeTask.AutomatedEmail = reader["Email"].ToString();
                    exeTask.Description = reader["Description"].ToString();
                    exeTask.IsNextFollowUpValue = Convert.ToBoolean(reader["IsNextFollowUp"]);
                    exeTask.Comment = reader["Comment"].ToString();
                    exeTask.CompletedStatus = Convert.ToBoolean(reader["CompletedStatus"]);
                    exeTask.IsPopUp = Convert.ToBoolean(reader["IsPopUp"]);
                    exeTask.FollowUpId = Convert.ToInt32(reader["FollowUpId"]);

                    FollowUpDC follwoUp = followUpList.FirstOrDefault(X => X.Id == exeTask.FollowUpId);
                    follwoUp.FollowUpDetailList.Add(exeTask);
                    finalFollowup.Add(follwoUp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return finalFollowup;
        }

        private DataTable GetIds(List<FollowUpDC> folloups)
        {
            DataTable branches = new DataTable();
            DataColumn col = new DataColumn("ID", typeof(Int32));
            branches.Columns.Add(col);
            foreach (var followup in folloups)
            {
                branches.Rows.Add(followup.Id);
            }
            return branches;
        }
    }
}
