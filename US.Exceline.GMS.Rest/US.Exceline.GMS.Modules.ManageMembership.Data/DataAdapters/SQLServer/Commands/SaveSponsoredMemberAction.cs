﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/8/2012 5:38:41 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveSponsoredMemberAction : USDBActionBase<bool>
    {
        private SponsoredMemberDC _sponsoredMember = new SponsoredMemberDC();
        private string _user = string.Empty;

        public SaveSponsoredMemberAction(SponsoredMemberDC sponsoredMember, string user)
        {
            _sponsoredMember = sponsoredMember;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSManageMembershipSaveSponsoredMember";

            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ID", DbType.Int32, _sponsoredMember.Id));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SponsorId", DbType.Int32, _sponsoredMember.SponsorId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                if (_sponsoredMember.StartDate != DateTime.MinValue)
                {
                    command.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _sponsoredMember.StartDate));
                }
                if (_sponsoredMember.EndDate != DateTime.MinValue)
                {
                    command.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _sponsoredMember.EndDate));
                }
                try
                {
                    if (_sponsoredMember.SponsoredLevel.Id > 0)
                    {
                        command.Parameters.Add(DataAcessUtils.CreateParam("@SponsoringLevelId", DbType.Int32, _sponsoredMember.SponsoredLevel.Id));
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(_sponsoredMember.SponsoredLevel.LevelName))
                        {
                            command.Parameters.Add(DataAcessUtils.CreateParam("@SponsoringLevelName", DbType.String, _sponsoredMember.SponsoredLevel.LevelName));
                        }
                    }
                }
                catch
                {
                }
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _sponsoredMember.MemberId));

                command.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public bool SaveSponsoredMember(DbTransaction transaction)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSManageMembershipSaveSponsoredMember";

            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.Parameters.Add(DataAcessUtils.CreateParam("@ID", DbType.Int32, _sponsoredMember.Id));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SponsorId", DbType.Int32, _sponsoredMember.SponsorId));
                if (_sponsoredMember.StartDate != DateTime.MinValue)
                {
                    command.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _sponsoredMember.StartDate));
                }
                if (_sponsoredMember.EndDate != DateTime.MinValue)
                {
                    command.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _sponsoredMember.EndDate));
                }
                try
                {
                    if (_sponsoredMember.SponsoredLevel.Id > 0)
                    {
                        command.Parameters.Add(DataAcessUtils.CreateParam("@SponsoringLevelId", DbType.Int32, _sponsoredMember.SponsoredLevel.Id));
                    }
                    else
                    {
                        command.Parameters.Add(DataAcessUtils.CreateParam("@SponsoringLevelName", DbType.String, _sponsoredMember.SponsoredLevel.LevelName));
                    }
                }
                catch
                {
                }
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _sponsoredMember.MemberId));
                command.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
