﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.Economy;
using US.Common.Notification.Core.DomainObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.SystemObjects
{
    interface IManageMembershipDataAdapter
    {
        #region manage member

        int AddBlancInstallment(int MemberId, int BranchId, string GymCode, string User);
        string SaveMember(OrdinaryMemberDC member, string gymCode, string user);
        bool ValidateMemberBrisId(int brisId, string gymCode);
        string UpdateMember(OrdinaryMemberDC member, string gymCode, string user);
        bool UpdateMemberImagePath(int memberId, string imagePath, string gymCode);
        bool DisableMember(int memberId, bool activeState, string comment, string gymCode, string user);
        bool MembercardVisit(int memberId, string user, string gymCode);
        List<int> GetMemberCount(string gymCode, int branchId);
        List<MemberForMemberlist> GetMemberList(FilterMemberList parameters, string user, string gymCode);
        List<ExcelineMemberDC> GetMemberDetails(int branchId, string seachText, int statuse, MemberSearchType searchType, MemberRole memberRole,string user,string gymCode, int hit, bool isHeaderClick, bool isAscending, string sortName);
        List<OrdinaryMemberDC> GetMembersByStatus(int statusId, string systemName, string user, string gymCode);
        List<PackageDC> GetContractsList(int branchId, int contractType, string gymCode);
        ContractSaveResultDC SaveMemberContract(MemberContractDC memberContract, string gymCode, string user);
        bool DeleteInstallment(List<int> installmentIdList, int memberContractId, string gymCode, string user);
        bool ResignContract(ContractResignDetailsDC resignDetails, string gymCode, string user);
        MemberContractDC GetMemberContractDetails(int ContractId, int branchId, string gymCode);
        List<ContractItemDC> GetItemsForContractTemplate(int contractId, int branchId, string gymCode);
        EntityVisitDetailsDC GetMemberVisits(int memberId, DateTime selectedDate, int branchId, string gymCode, string user);
        int SaveMemberVisit(EntityVisitDC memberVisit, string gymCode, string user);
        bool DeleteMemberVisit(int memberVisitId, int memberContractId, string gymCode);
        List<OrdinaryMemberDC> GetGroupMemebersByGroup(int branchId, string searchText, bool IsActive, int groupId, string gymCode);
        bool AddFamilyMember(int memberId, int familyMemberId, int memberContractId, string gymCode);
        List<InstallmentDC> UpdateInstallmentsWithContract(decimal installmentAmount, int updatingInstallmentNo, MemberContractDC memberContract, string gymCode);
        bool SaveSponsor(int memberId, int sponsorId, int branchId, string user, string gymCode);
        string SaveMemberParent(OrdinaryMemberDC memberParent, int branchId, string user, string gymCode);
        OrdinaryMemberDC GetMemberDetailsByMemberId(int branchId, string user, int memberId, string memberRole, string gymCode);
        List<ExcePaymentInfoDC> GetMemberPaymentsHistory(int memberId, string paymentType, string gymCode, int hit, string user);
        List<CategoryDC> GetInterestCategoryByMember(int memberId, int branchId, string gymCode);
        bool SaveInterestCategoryByMember(List<CategoryDC> InterestCategoryList, int memberId, int branchId, string gymCode);
        List<OrdinaryMemberDC> GetSwitchPayers(int branchId, int memberId, string gymCode);
        List<ExcelineMemberDC> GetGroupMembers(int groupId, string gymCode);
        List<ExcelineMemberDC> GetFamilyMembers(int memberId, string gymCode);
        List<ContractBookingDC> GetContractBookings(int membercontractId, string gymCode);
        List<InstallmentDC> GetMemberOrders(int memberId, string type, string gymCode, string user);
        OrdinaryMemberDC GetEconomyDetails(int memberId, string gymCode);
        List<ExcelineMemberDC> GetListOfGuardian(int guardianId, string gymCode);
        OrdinaryMemberDC GetMemberInfoWithNotes(int branchId, int memberId, string gymCode);
        List<ExcelineMemberDC> GetIntroducedMembers(int memberId, int branchId, string gymCode);
        bool UpdateIntroducedMembers(int memberId, DateTime? creditedDate, string creditedText, bool isDelete, string gymCode);
        Dictionary<int, string> GetMemberStatus(string gymCode, bool getAllStatuses);

        #endregion

        #region contract
        bool SaveMemberInstallments(List<InstallmentDC> installmentList, bool initialGenInstmnts, DateTime startDate, DateTime endDate, int memberContractId, int membercontractNo, int branchId, string gymCode, int renewedTemplateId);
        CreditNoteDC GetCreditNoteDetails(int arItemNo, string gymCode);
        string GetInvoicePathByARitemNo(int arItemNo, string gymCode);
        List<InstallmentDC> GetInstallments(int memberContractId, string gymCode);
        bool UpdateMemberInstallment(InstallmentDC installment, string gymCode);
        bool UpdateMemberAddonInstallments(List<InstallmentDC> installments, string gymCode);
        int SwitchPayer(int memberId, int payerId, DateTime activatedDate, bool isSave, string gymCode);
        bool RenewMemberContract(MemberContractDC renewedContract, List<InstallmentDC> installmentList, string gymCode, int branchId, bool isAutoRenew, string user);
        bool IntroducePayer(int memberId, int introduceId, string gymCode);
        bool CancelMemberContract(int memberContractId, string canceledBy, string comment, List<int> installmentIds, int minNumber, int tergetInstallmentId, string gymCode);
        List<MemberContractDC> GetMemberContractsForActivity(int memberId, int activityId, int branchId, string gymCode);
        List<ClassDetailDC> GetClassByMemberId(int branchId, int memberId, DateTime? classDate, string user, string gymCode);
        List<ContractSummaryDC> GetFamilyMemberContracts(int memberId, int branchId, string gymCode, bool isFreezDetailNeeded);
        int UpdateContract(MemberContractDC contract, string gymCode, string user);
        double ChangePrePaidAccount(string increase, float amount, int memberId, int branchId, string gymCode, string user);
        bool SaveEconomyDetails(OrdinaryMemberDC member, string gymCode, string user);
        string SaveMemberInfoWithNotes(OrdinaryMemberDC member, string gymCode);
        PackageDC GetContractTemplateDetails(string type, int templateID, string gymCode);
        #endregion

        #region Sponser Contraat
        bool RemoveSponsorshipOfMember(int sponsoredRecordId, string gymCode, string user);
        List<DiscountDC> GetGroupDiscountByType(int branchId, string user, int discountTypeId, string gymCode, int sponsorId);
        List<ExcelineMemberDC> GetMembersBySponsorId(int sponsorId, int branchId, string gymcode);

        List<EmployeeCategoryDC> GetSponsorEmployeeCategoryList(int branchId, string gymcode, int sponsorId);
        OrdinaryMemberDC GetEmployeeCategoryBySponsorId(int branchId, string gymcode, int sponsorId);
        SponsorSettingDC GetSponsorSetting(int branchId, string user, int sponsorId, string gymCode);
        bool SaveSponsorSetting(SponsorSettingDC sponsorSetting, int branchId, string user, string gymCode);
        bool DeleteDiscount(int discountID, string gymCode);

        bool DeleteEmployeeCategory(int categoryID, string gymCode);
        List<MemberContractDC> GetContractsForSponsership(int activityId, int branchId, DateTime endDate, DateTime startDate, string gymCode);
        int SaveSponserContract(SponsorSettingDC sponserContract, int branchId, string user, string gymCode);
        List<OrdinaryMemberDC> GetMembersByRoleType(int branchId, string user, int status, MemberRole roleType, string searchText, string gymCode);
        List<MemberContractDC> GetSponserContractsByActivity(int sponserId, int activityId, int branchId, string gymCode);
        bool AddSponserShipForMember(int memContractId, int sponserContractId, int employeeCategotyID, int actionType, string employeeRef, string gymCode);
        bool CancelSponsorContract(int contractId, string user, string comment, string gymCode);
        #endregion

        #region Manage Discounts
        List<SponsoredMemberDC> GetSponsoredMemberList(int branchId, int sponsorId, string gymcode);
        List<int> GetSponsoredMemberListByTimePeriod(int sponsorId, Dictionary<int, List<DateTime>> sponsorMembers, string gymcode);
        bool AddMemberToGroup(int memberId, int groupId, string user, string gymCode);
        List<DiscountDC> GetDiscountList(int branchId, DiscountType discountType, string gymCode);
        int SaveDiscount(List<DiscountDC> discountList, string user, int branchId, int contractSettingID, string gymCode);
        bool RemoveMemberFromGroup(int memberId, int groupId, string user, string gymCode);
        List<DiscountDC> GetDiscountsForActivity(int branchId, int activityId, string gymCode);
        ShopSalesDC GetMemberPerchaseHistory(int memberId, DateTime fromDate, DateTime toDate, string gymCode, int branchId, string user);
        #endregion

        #region Freeze Contract

        int SaveContractFreezeItem(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> freezeInstallments, string user, string gymCode);
        List<ContractFreezeItemDC> GetContractFreezeItems(int memberContractId, string gymCode);
        List<ContractFreezeInstallmentDC> GetContractFreezeInstallments(int freezeItemId, string gymCode);
        List<InstallmentDC> GetInstallmentsToFreeze(int memberContractId, string gymCode);
        int ExtendContractFreezeItem(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> freezeInstallments, string user, string gymCode);
        int UnfreezeContract(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> unfreezeInstallments, string user, string gymCode);
        int IsFreezeAllowed(int memberContractId, int memberId, string gymCode, int branchId);
        #endregion

        bool DeleteCreditNote(int creditNoteId, string gymCode);
        int SaveNote(int branchId, string use, int memberId, string note, string gymCode);
        bool RenewContract(int memberContractId, int branchId, string gymCode);
        List<RenewContractDC> GetContractsForAutoRenew(int branchId, string gymCode);
        bool SaveRenewContractDetails(RenewSummaryDC renewSummary, int branchId, string gymCode, string user);
        OrdinaryMemberDC GetNote(int branchId, string user, int memberId, string gymCode);
        List<ExcelineMemberDC> GetContractMembers(int memberContractId, string gymCode);
        bool UpdateContractOrder(List<InstallmentDC> installmentList, string gymCode, string user);
        int SaveContractGroupMembers(int groupId, List<ExcelineMemberDC> groupMemberList, int contractId, string gymCode, string user);
        bool CheckBookingAvailability(int resourceID, string day, DateTime startTime, DateTime endTime, string gymCode);
        int AddInstallment(InstallmentDC installment, string gymCode, string user);
        InstallmentDC GetMemberContractLastInstallment(int memberContractId, string gymCode);
        string GetMemberSearchCategory(int branchId, string gymCode);
        bool SaveRenewMemberContractItem(List<ContractItemDC> itemList, int memberContractId, int branchId, string gymCode);
        bool SaveDocumentData(DocumentDC document, string username, int branchId);
        List<DocumentDC> GetDocumentData(bool isActive, string custId, string searchText, int documentType, string gymCode, int branchId);
        bool DeleteDocumentData(int docId, string gymCode, int branchId);
        bool SetPDFFileParth(int id, string fileParth, string flag, int branchId, string GymCode, string user);
        CustomerBasicInfoDC GetMemberBasicInfo(int memberId, string gymcode);
        List<ContractSummaryDC> GetContractSummaries(int memberId, int branchId, string gymCode, bool isFreezDetailNeeded, string user, bool isFreezdView);
        int GetVisitCount(int memberId, DateTime startDate, DateTime endDate, string gymCode);
        List<InstallmentDC> MergeOrders(List<InstallmentDC> updatedOrders, List<int> removedOrder, int memberContractId, string gymCode, string user);

        List<FollowUpTemplateTaskDC> GetFollowUpTask(string gymCode);
        
        bool SaveFollowUpPopup(int followUpId,String gymCode);
        List<FollowUpDC> GetFollowUps(int memberId,int followUpId, string gymCode, string user);
        int SaveFollowUp(List<FollowUpDC> followUpList, string gymCode, string user);
        List<FollowUpDetailDC> GetFollowUpDetailByEmpId(int empId, string gymCode);

        List<MemberBookingDC> GetMemberBookings(int memberId, string gymCode, string user);
        List<ExcelineMemberDC> GetOtherMembersForBooking(int scheduleId, string gymCode);
        List<ExceACCMember> GetMemberDetailsForCard(string cardNo, string accessType, string gymCode);
        List<InstallmentDC> GetSponsorOrders(List<int> sponsors, string gymCode);
        bool SetContractSequenceID(List<PackageDC> contractList, int branchID, string gymCode);

        bool AddUpdateMemberIntegrationSettings(MemberIntegrationSettingDC memberIntegrationSettingsDetail, string gymCode);
        List<MemberIntegrationSettingDC> GetMemberIntegrationSettings(int branchId, int memberId, string gymCode);
        int CheckContractGymChange(int memberID, int memberContractID, int oldBranchID, int newBranchID, string gymCode);
        int ValidateChangeGymInMember(int currentBranchID, int newBranchID, int branchID, int memberID, string user, string gymCode);
        ArticleDC GetTimeMachineArticle(string gymCode);
        bool UpdateShopItemAndGatPurchase(GatpurchaseShopItem gatPurchaseShopItem);
        int ValidateMemberWithStatus(int memberId, int statusId, string user, string gymCode);
        List<ExceACCMember> GetMemberDetailsForExcAccessController(string cardNo,int branchId,string gymCode);
        int GetPriceChangeContractCount(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode, bool changeToNextTemplate);
        bool UpdatePrice(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode, bool changeToNextTemplate, string user);

        List<ExcelineContractDC> GetPriceUpdateContracts(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode, bool changeToNextTemplate, int hit);

        List<PackageDC> GetTemplatesForPriceUpdate(string gymCode);

        bool InfoGymUpdateExceMember(string infoGymId, int memberId, string user);

        List<ContractSummaryDC> GetContractSummariesByEmployee(int employeeID, int branchId, DateTime createdDate, string gymCode);
        DateTime? ValidateSponsoringGeneration(string gymCode);
        bool UpdateBRISMemberData(OrdinaryMemberDC member, string gymCode);

        string GetCountryCode(string landCode, string gymCode);
        bool UpdateAgressoId(int memberID, int aggressoID, string gymCode);
        bool UpdateBRISStatus(int brisID, string message, string response, string status, string gymCode);
        bool RemoveContractResign(int memberContractId, string user, string gymCode);
        bool UpdateInvoicePdfPath(int arItemNo, string invoiceFilePath, string gymCode);

        bool ImageChangedAddEvent(USNotificationTree notification, string gymcode, string user);
        List<MemberForMemberlist> GetVisitedMembers(string user, string gymCode);

        MemberContractPrintInfo GetMemberContract(int contractId, string gymcode);

        List<MonthlyServicesForContract> GetMonthlyService(int contractId, string gymcode);

        bool RegisterChangeOfInvoiceDueDate(AlterationInDueDateData changeData, String user, String gymCode);
    }
}
